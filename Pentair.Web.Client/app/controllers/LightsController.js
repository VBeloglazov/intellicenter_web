﻿'use strict';
var PWC = PWC || {};
PWC.controller('LightsController', [
    '$scope', '$location', '$filter', 'PropertyService', 'PropertyModel',
    function($scope, $location, $filter, propertyService, propertyModel) {

        $scope.Property = propertyService;
        $scope.Model = propertyModel;
        $scope.$parent.Layout = "layout-base";

        $scope.IntelliHelp = false;
        $scope.MagicHelp = false;
        $scope.currentSyncSetSwingState = "OFF";
        $scope.currentSyncSetSwingMessage = "Light Group Sequence in Progress..";


        $scope.ToggleIntelliHelp = function() {
            $scope.IntelliHelp = !$scope.IntelliHelp;
        };


        $scope.ToggleMagicHelp = function() {
            $scope.MagicHelp = !$scope.MagicHelp;
        };


        $scope.GoToDashboard = function() {
            $location.path("/property");
        };

        $scope.ShowDimmer = function() {

        };

        $scope.current = 10;
        $scope.changer = function() {
            $scope.current = $scope.current + 10;
        }

        $scope.changer = function () {
            $scope.current = $scope.current + 10;
            $scope.Property.SetValue(lightGroup.OBJNAM, 'ACT', usage.key)
        }
        
        $scope.SetSelectedColor = function (objname, color) {
            $scope.Property.SetValue(objname, 'ACT', color);
            $scope.Property.SetValue(objname, 'USE', color);
        };

        $scope.Lights = $filter('objectsBySection')(propertyModel.Circuits, 'l');   

        $scope.$on('NotifyList', function (objname, src, obj) {

            //console.log("---- processing notify light ..." + src);
            angular.forEach($scope.Lights, function (light) {
                if (light.OBJNAM === obj.objname) {
                    console.log(" procesing  object  : " + light.OBJNAM);
                    if (obj.params.USE) { // CHECK IF EXIST
                        light.USE = obj.params.USE;
                        //console.log(" setting parameter " + values.params.USE);
                    }
                    if (obj.params.ACT) { // CHECK IF EXIST
                        light.ACT = obj.params.ACT;
                        //console.log(" setting parameter " + obj.params.ACT);
                    }
                }
            });
            //console.log("--- processing light ---");
            //console.log("Lightcontroller processing  object  : " + obj);
            if (obj && obj.objnam) {
                if (obj.objnam.indexOf('GRP') >= 0) {                    
                    if (obj.params.SYNC) { // set current state 'ON' or 'OFF'
                         $scope.currentSyncSetSwingState = obj.params.SYNC;
                    }
                    if (obj.params.SET) { // set current state 'ON' or 'OFF'
                        $scope.currentSyncSetSwingState = obj.params.SET;
                    }
                    if (obj.params.SWIM) { // set current state 'ON' or 'OFF'
                        $scope.currentSyncSetSwingState = obj.params.SWIM;
                    }

                    console.log("Lightcontroller group status =  ", $scope.currentSyncSetSwingState);
                }
            }

        });

        
        $scope.getSyncSetSwingMessage = function(name) {
            
            if ($scope.currentSyncSetSwingState === "OFF"){
                return name;
            }
            else{
                return $scope.currentSyncSetSwingMessage;
            } 
        }

        $scope.SetLightGroupStage = function (obj, param) {

            if (obj.CHILD === "")
                return;  // nothing to do no attached items

            if ($scope.currentSyncSetSwingState === "ON")
                return;// do nothing is already under progress
            
            // sent command
            $scope.Property.SetLightStage(obj.OBJNAM, param, 'ON');

            // set current state    
            $scope.currentSyncSetSwingState = "ON";

            // Wait message            
           // $scope.currentSyncSetSwingMessage = "LIGHT GROUP "+ obj.SNAME.toUpperCase() + " , "+ param + " IN  PROGRESS...";

            // now for any register change
            $scope.Property.registertLightGroupStatus(obj.OBJNAM);
        }
        $scope.DropListGroupCircuit = function () {
            var token = ""
            if ($scope.groupIsInProgress() === false) // execute if not in progress
                token = 'dropdown';
            return token;
        }

        $scope.setStatusCircuit = function (obj, checkbox) {
            if (obj && obj.OBJNAM && obj.STATUS) {
                if (obj.STATUS === "OFF")                                     
                    $scope.Property.setTokenCircuit(obj, 'STATUS', "OFF")    
                else
                    $scope.Property.setTokenCircuit(obj, 'STATUS', "ON")
                
            }
        }        

        $scope.setStatusGroupCircuit = function (obj) {
            if ($scope.groupIsInProgress() === true) {
                // is in progress... do not process
                return;
            }
            if (obj && obj.OBJNAM && obj.STATUS) {
                if (obj.STATUS === "OFF")
                    $scope.Property.setTokenCircuit(obj, 'STATUS', "OFF")
                else
                    $scope.Property.setTokenCircuit(obj, 'STATUS', "ON")

            }
        }

        $scope.ToggleGroupCircuit = function(obj,value){
            if ($scope.groupIsInProgress()===false) // execute if not in progress
                $scope.Property.ToggleCircuit(obj, value)
        }

        // WILL TOGGLE 
        $scope.SetGroupCircuit = function (obj) {
            if ($scope.groupIsInProgress() === false) { // execute if not in progress
               // $scope.Property.ToggleCircuit(obj, value)
                if (obj.STATUS === "ON")
                    $scope.Property.setTokenCircuit(obj, 'STATUS', "OFF")
                else
                    $scope.Property.setTokenCircuit(obj, 'STATUS', "ON")
            }
        }

        $scope.SetLightColor = function (obj, param, key) {
            // set param
            $scope.Property.SetValue(obj, param, key);        //Property.SetValue(light.OBJNAM,'ACT',usage.key)
            // now retrivev value 
            $scope.Property.GetLightColor(obj);
        };

        // set color and turn on the group
        $scope.SetLightColorGroupOn = function (obj, param, key) {
            // set param
            $scope.Property.SetValue(obj, param, key);        
            // now retrivev value 
            $scope.Property.GetLightColor(obj);
            // set on the light
            $scope.Property.ToggleCircuitGroup(obj, 'ON')
        };

        $scope.getColorGroup = function () {    // is in progress
            if ($scope.currentSyncSetSwingState === 'ON')
                return 'lightgray';
            else
                return 'black';
        }

        $scope.isGroupEmpty= function (group) {
            if (group.CHILD === "") // no childs available   
                return "true";
            else
                return "false";

        }

        $scope.groupIsInProgress = function () {
            if ( $scope.currentSyncSetSwingState === 'ON')
                return true
            else
                return false
        }

        // check if are in progress
        $scope.isState_SyncSetSwing = function (state) {
            if (state === $scope.currentSyncSetSwingState)
                return true;
            else
                return false;
        }

        $scope.getIconGroupLight = function (obj,type){
            var icon = "";

            var enabled = false;
            if (obj.CHILD === "")
                enabled  = true;  // nothing to do no attached items

            if (($scope.currentSyncSetSwingState  === "ON") || (enabled)){ // IS IN PROGRESS                
                switch(type)  {                  
                    case "SYNC": icon = "groupsyncswimdisabled btn-sync-disabled"; break;
                    case "SET": icon = "groupsyncswimdisabled btn-set-disabled"; break;
                    case "SWIM": icon = "groupsyncswimdisabled btn-swim-disabled"; break;
                }
            }else{ // IS OFF             
                switch (type) {
                    case "SYNC": icon = "groupsyncswim btn-sync-enabled"; break;
                    case "SET": icon = "groupsyncswim btn-set-enabled"; break;
                    case "SWIM": icon = "groupsyncswim btn-swim-enabled"; break;
                }
            }

            return icon;
        }

        
        $scope.SetLightColorAndTurnOn = function (obj, param, key) {
            // set param
            $scope.Property.SetValue(obj.OBJNAM, param, key);        //Property.SetValue(light.OBJNAM,'ACT',usage.key)
            // now retrivev value 
            $scope.Property.GetLightColor(obj.OBJNAM);
            // set on the light
            $scope.Property.ToggleCircuit(obj, 'ON')
                        
        };

        $scope.$on('ObjectUpdated', function (objname, values) {


            //console.log("**********  processing light  ObjectUpdated---" + values);
            //$scope.Lights = $filter('objectsBySection')(propertyModel.Circuits, 'l');
            //$scope.Lights.forEach(function (featureCircuit) {
            //});
            /*
            for (var ligth = 0; ligth < $scope.Lights.length; ligth++) {
                //if ($scope.Lights.deleteFlag == true) {
                if ($scope.Lights.OBJNAM === params.objname) {
                    console.log("--- match  ---: " + objname);
                    
                     $scope.Property.SetValue(objname, 'ACT', color);
                }
                //}
            }
            */
            angular.forEach($scope.Lights, function (light) {
                
                
                if (light.OBJNAM === values.objnam) {
                    console.log(" procesing  object  : " , light.OBJNAM);
                    if (values.params.USE) { // CHECK IF EXIST
                        light.USE = values.params.USE;  
                        //console.log(" setting parameter " + values.params.USE);
                    }
                    if (values.params.ACT) { // CHECK IF EXIST
                        light.ACT = values.params.ACT;
                        //console.log(" setting parameter " + values.params.ACT);
                    }

                    /*
                    angular.forEach(values.params, function (param) {                        
                        // check if params exist
                        if (light.USE && param.USE) {
                            light.USE = param.USE;
                            console.log(" setting parameter " + param.USE);
                            
                        }
                    });
                    */
                }
                /*
                propertyService.SetParams(remote.OBJNAM, params).then(function () {
                    $scope.bodyStyle.overflow = 'none';
                });
                */
            });
            //console.log("--- processing light ---");
        });

    }
]);
