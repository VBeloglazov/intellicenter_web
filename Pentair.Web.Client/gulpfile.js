var gulp = require('gulp');
var webserver = require('gulp-webserver');
var sourcemaps = require('gulp-sourcemaps');
var sass = require('gulp-sass');
var testserver = require('karma').Server;
var util = require('gulp-util');
var ngmin = require('gulp-ngmin');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var cleanCSS = require('gulp-clean-css');
var notify = require("gulp-notify");
var minifyHtml = require('gulp-minify-html');
var ngTemplate = require('gulp-ng-template');
var fs = require('fs');
var mkdirp = require('mkdirp');
var replace = require('gulp-replace');
var rmdir = require('rmdir');

var pkg = require('./package.json');

var rootSrc = './';
var appSrc = 'app/';

var buildPath = 'dist/';

var scripts = [
  'js/vendor.js',
  'js/app.js',
  'js/templates.js'
];

gulp.task('html', function() {
  gulp.src(appSrc + '**/*.html');
});

gulp.task('js', function() {
  gulp.src(appSrc + '**/*.js');
});

gulp.task('sass', function () {
  return gulp.src('./content/css/*.scss')
    .pipe(sass({ includePaths : ['./content/css'] }))
    .pipe(gulp.dest('./content/css'));
});

gulp.task('sass2', function () {
  return gulp.src('./content/css/pages/*.scss')
    .pipe(sass({ includePaths : ['./content/css/pages'] }))
    .pipe(gulp.dest('./content/css/pages'));
});

gulp.task('test', function (done) {
  new testserver({
    configFile: __dirname + '/../Pentair.Tests/Scripts/karma.conf.js',
    singleRun: true
  }, done).start();
});

gulp.task('watch', function() {
  gulp.watch(appSrc + '**/*.js', ['js']);
  gulp.watch(appSrc + '**/*.html', ['html']);
  gulp.watch('./content/css/*.scss', ['sass']);
  gulp.watch('./content/css/pages/*.scss', ['sass2']);
});

gulp.task('webserver', function() {
  gulp.src(rootSrc)
    .pipe(webserver({
      livereload: true,
      open: true
    }));
});

gulp.task('createDistFolder', function() {
  rmdir('dist/', function (err, dirs, files) { });
  mkdirp('dist/', function(err) {});
});

gulp.task('moveToDist', (util.env && util.env.clean !== undefined) ? ['moveToDistMinified'] : ['moveToDistUnminified'], function () {
  util.noop();
});

gulp.task('moveToDistUnminified', function() {
  gulp.src(['./**', '!./{dist,dist/**}', 
    '!./{node_modules,node_modules/**}']).pipe(gulp.dest(buildPath + '/')); 
});

gulp.task('moveToDistMinified', function() {
  gulp.src('Web.config').pipe(gulp.dest(buildPath));
  gulp.src('content/fonts/avenir/**').pipe(gulp.dest(buildPath + '/fonts/avenir'));
  gulp.src('content/fonts/avenir-black/**').pipe(gulp.dest(buildPath + '/fonts/avenir-black'));
  gulp.src('content/fonts/glyphicons**').pipe(gulp.dest(buildPath + '/fonts'));
  gulp.src('content/fonts/**').pipe(gulp.dest(buildPath + '/css'));
  gulp.src('content/img/**').pipe(gulp.dest(buildPath + '/img'));
  gulp.src(['content/css/bootstrap.min.css','content/css/select.min.css','content/css/angular-ui-notification.min.css',
    'content/fonts/iconstyles.css',
    'content/css/main.css','content/css/pages/installations.css','content/css/pages/dashboard.css',
    'content/css/pages/login.css','content/css/pages/configurations.css','content/css/pages/manageUsers.css',
    'content/css/pages/settings.css'])
    .pipe(concat('app.css'))
    .pipe(cleanCSS())
    .pipe(gulp.dest(buildPath + '/css'))
    .pipe(notify({ message: 'css build complete...' }));
  gulp.src(['scripts/jquery-2.1.1.js','scripts/jquery-ui.v1.10.3.min.js','scripts/angular.js',
    'scripts/angular-route.js','scripts/angular-cookies.js','scripts/angular-translate.js','scripts/angular-local-storage.min.js',
    'scripts/angular-animate.js','scripts/loading-bar.min.js','scripts/checklist-model.js','scripts/angular-round-progress.js',
    'scripts/ui-bootstrap-tpls-0.13.3.js','scripts/select.min.js','scripts/angular-dateParser.js','scripts/angular-timePicker.js',
    'scripts/ui-grid.js','scripts/d3-v3.5.17.min.js','scripts/d3-legend.js','scripts/d3-gantt.js','scripts/moment.js',
    'scripts/sortable.min.js','scripts/angular-ui-notification.min.js'])
    .pipe(concat('vendor.js'))
    .pipe(uglify())
    .pipe(gulp.dest(buildPath + '/js'))
    .pipe(notify({ message: 'vendor js build complete...' }));
  gulp.src(['app/i18n/TranslatorProvider.js','app/app.js','app/services/**/*.js','app/models/**/*.js','app/controllers/**/*.js',
    'app/utility/**/*.js','app/filters/**/*.js','app/directives/**/*.js'])
    .pipe(concat('app.js'))
    .pipe(ngmin())
    .pipe(uglify({mangle:false})) // got an error when I tried to mangle it
    .pipe(gulp.dest(buildPath + '/js'))
    .pipe(notify({ message: 'app js build complete...' }));
  gulp.src('app/**/*.html')
    .pipe(minifyHtml({empty: true, quotes: true}))
    .pipe(ngTemplate({
      moduleName: 'PentairWebClient',
      standalone: true,
      filePath: 'js/templates.js',
      prefix: '/app/'
    }))
    .pipe(replace(/.*angular.module.*\r?\n/g, "PWC.run(['$templateCache', function($templateCache) {"))
    .pipe(ngmin())
    .pipe(uglify())
    .pipe(gulp.dest(buildPath))
    .pipe(notify({ message: 'template build complete...' }));
  fs.readFile('index.html', 'utf8', function (err,data) {
    if (err) {
      return console.log(err);
    }
    // remove existing scripts except ones embeded
    var result = data.replace(/.*<script src.*\r?\n/g, '');
    // add new concat scripts
    result = result.replace(/(^\s+)<!-- SCRIPTS -->\r?\n/m, function($, $1) {
      return $ + scripts.map(function(script) {
        return $1 + '<script src="'+script+'"></script>';
      }).join('') + '\n';
    });
    // remove existing css
    result = result.replace(/.*<link rel="stylesheet".*\r?\n/g, '');
    // add new concat css
    result = result.replace(/(^\s+)<!-- CSS -->\r?\n/m, function($, $1) {
      return $  + '<link rel="stylesheet" href="css/app.css">';
    });
    fs.writeFile(buildPath + '/index.html', result, 'utf8', function (err) {
      if (err) return console.log(err);
    });
  });
  fs.writeFile(buildPath + '/version.txt', 'Version: ' + pkg.version + '\nBuild Date: ' + new Date(), function(err) {
    if (err) throw err;
  });
});

gulp.task('default', ['sass', 'sass2', 'watch', 'webserver']);
gulp.task('runTests', ['test']);
gulp.task('build', ['sass', 'sass2', 'createDistFolder','moveToDist']);