﻿'use strict';
describe('SystemPersonalityController', function () {
    var propertyService;
    var $location;
    var $scope;
    var systemPersonalityController;
    var $controller;
    beforeEach(module('PentairWebClient'));


    beforeEach(inject(function (_$controller_, _$rootScope_, _PropertyService_, _$location_) {
        $scope = _$rootScope_.$new();
        propertyService = _PropertyService_;
        $location = _$location_;
        $controller = _$controller_;

        systemPersonalityController = $controller("SystemPersonalityController", { $scope: $scope });
        spyOn($location, 'path');
    }));


    describe('GoToConfigPage', function () {
        it('should take the user to the config page', function () {
            $scope.GoToConfigPage();
            expect($location.path).toHaveBeenCalledWith('/configure');
        });
    });

    describe('Refresh model', function() {
        it('should call the property service to rebuild the model', function() {
            $scope.RefreshSystemPersonality();
        });
    });

    describe('Display I5P details', function() {
        it('should return an array of properties', function() {
            var result = $scope.ModuleCapabilities("I5P");
            expect(result.length).toEqual(8);
        });
    });

    describe('Display I5X details', function () {
        it('should return an array of properties', function () {
            var result = $scope.ModuleCapabilities("I5X");
            expect(result.length).toEqual(2);
        });
    });

    describe('Display I5PS details', function () {
        it('should return an array of properties', function () {
            var result = $scope.ModuleCapabilities("I5PS");
            expect(result.length).toEqual(3);
        });
    });

    describe('Display I8PS details', function () {
        it('should return an array of properties', function () {
            var result = $scope.ModuleCapabilities("I8PS");
            expect(result.length).toEqual(4);
        });
    });

    describe('Display I8P details', function () {
        it('should return an array of properties', function () {
            var result = $scope.ModuleCapabilities("I8P");
            expect(result.length).toEqual(1);
        });
    });

    describe('Display I10D details', function () {
        it('should return an array of properties', function () {
            var result = $scope.ModuleCapabilities("I10D");
            expect(result.length).toEqual(7);
        });
    });

    describe('Display I10PS details', function () {
        it('should return an array of properties', function () {
            var result = $scope.ModuleCapabilities("I10PS");
            expect(result.length).toEqual(4);
        });
    });

    describe('Display VALVEXP details', function () {
        it('should return an array of properties', function () {
            var result = $scope.ModuleCapabilities("VALVEXP");
            expect(result.length).toEqual(1);
        });
    });

    describe('Display ANGLEXP details', function () {
        it('should return an array of properties', function () {
            var result = $scope.ModuleCapabilities("ANGLEXP");
            expect(result.length).toEqual(1);
        });
    });
});
