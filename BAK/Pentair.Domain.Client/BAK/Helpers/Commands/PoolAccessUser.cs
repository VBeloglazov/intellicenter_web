﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Pentair.Domain.Client.Helpers.Commands
{
    public class PoolAccessUser : KeyValueCommand
    {
        public PoolAccessUser(
            string command,
            string messageID,
            Dictionary<string, string> param
            )
            : base(command, messageID, param)
        {
        }

    } // class PoolAccessUser
}
