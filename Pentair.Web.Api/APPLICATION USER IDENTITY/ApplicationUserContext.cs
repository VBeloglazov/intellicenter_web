﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

using Microsoft.AspNet.Identity.EntityFramework;

namespace Pentair.Web.Api
{
    public class ApplicationUserContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationUserContext()
            : base("ApiContext")
        {
        } // ApplicationUserContext()

        public static ApplicationUserContext Create()
        {
            return new ApplicationUserContext();

        } // Create()

    } // class ApplicationUserContext
}

