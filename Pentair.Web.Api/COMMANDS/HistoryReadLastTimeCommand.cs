﻿using System.Collections.Generic;

using Newtonsoft.Json;

namespace Pentair.Web.Api
{
    public class HistoryReadLastTimeCommand : CommandBase
    {
        public HistoryReadLastTimeCommand(
            string messageID
            )
            : base(CommandString.ReadLastHistoryTime, messageID)
        {
        } // HistoryReadLastTimeCommand()

    } // class HistoryReadLastTimeCommand
}
