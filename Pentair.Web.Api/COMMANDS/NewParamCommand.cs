﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Pentair.Web.Api
{
    public class NewParamCommand : CommandBase
    {
        [JsonProperty("objectList")]
        public List<NewParamObject> ObjectList { get; private set; }

        public NewParamCommand(
            string command,
            string messageID,
            List<NewParamObject> objectList
            )
            : base(command, messageID)
        {
            this.ObjectList = objectList;
        }

    } // class NewParamCommand
}
