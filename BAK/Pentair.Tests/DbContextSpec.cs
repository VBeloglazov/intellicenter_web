﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Data.Entity.Core.Common.CommandTrees.ExpressionBuilder;
using System.Linq;
using Pentair.Domain.Client.Entity;
using Pentair.Domain.Client.Helpers.Entity;
using Pentair.Domain.Client.Infrastructure;

namespace Pentair.Tests
{
    #region FakeDb Set
    public class FakeDbSet<T> : IDbSet<T>, IQueryable<T> where T : class
    {
        readonly ObservableCollection<T> _data;
        readonly IQueryable _query;

        public FakeDbSet()
        {
            _data = new ObservableCollection<T>();
            _query = _data.AsQueryable();
        }

        public virtual T Find(params object[] keyValues)
        {
            throw new NotImplementedException("Derive from FakeDbSet<T> and override Find");
        }

        public T Add(T item)
        {
            _data.Add(item);
            return item;
        }

        public T Remove(T item)
        {
            _data.Remove(item);
            return item;
        }

        public T Attach(T item)
        {
            _data.Add(item);
            return item;
        }

        public T Detach(T item)
        {
            _data.Remove(item);
            return item;
        }

        public T Create()
        {
            return Activator.CreateInstance<T>();
        }

        public TDerivedEntity Create<TDerivedEntity>() where TDerivedEntity : class, T
        {
            return Activator.CreateInstance<TDerivedEntity>();
        }

        public ObservableCollection<T> Local
        {
            get { return _data; }
        }

        Type IQueryable.ElementType
        {
            get { return _query.ElementType; }
        }

        System.Linq.Expressions.Expression IQueryable.Expression
        {
            get { return _query.Expression; }
        }

        IQueryProvider IQueryable.Provider
        {
            get { return _query.Provider; }
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return _data.GetEnumerator();
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return _data.GetEnumerator();
        }
    }
    #endregion

    #region FakeProperty
    // Fake Properties
    public class FakePropertySet : FakeDbSet<Property>
    {
        public override Property Find(params object[] keyValues)
        {
            return this.SingleOrDefault(d => d.PropertyId == (int)keyValues.Single());
        }
    }

    // Fake ControlObject
    public class FakeControlObjectSet : FakeDbSet<ControlObject>
    {
        public override ControlObject Find(params object[] keyValues)
        {
            return this.SingleOrDefault(d => d.OBJNAM == (string)keyValues.Single());
        }
    }
    #endregion

    #region Fake LogSet
    public class FakeLogSet : FakeDbSet<MessageLog>
    {
        public override MessageLog Find(params object[] keyValues)
        {
            return this.SingleOrDefault(d => d.Id == (int)keyValues.Single());
        }
    }
    #endregion

    #region Fake ApplicationUserSet

    public class FakeApplicationUser : FakeDbSet<ApplicationUser>
    {
        public override ApplicationUser Find(params object[] keyValues)
        {
            return this.SingleOrDefault(d => d.Id == (string) keyValues.Single());
        }
    }
    #endregion

    #region FakeContext
    public class FakeContext : ISimulatorContext, IApiContext
    {
        public Database Database { get; private set; }
        public IDbSet<Property> Properties { get; set; }
        public IDbSet<Association> Associations { get; set; }
        public IDbSet<Installation> Installations { get; set; }
        public IDbSet<ControlObject> ControlObjects { get; set; }
        public IDbSet<Connection> Connections { get; set; }
        public IDbSet<Subscription> Subscriptions { get; set; } 
        public IDbSet<MessageLog> MessageLogs { get; set; }
        public IDbSet<UserInvitation> UserInvitations { get; set; }
        public IDbSet<ApplicationUser> ApplicationUsers { get; set; }
        public IDbSet<Event> Events { get; set; }
        public IDbSet<EventType> EventTypes { get; set; }
        public IDbSet<UserNotification> UserNotifications { get; set; }
        public IDbSet<ObjectParameterType> ObjectParameterTypes { get; set; }
        public IDbSet<ObjectView> ObjectViews { get; set; }
        public IDbSet<NotificationRecipient> NotificationRecipients { get; set; }
        public IDbSet<NotificationMessage> NotificationMessages { get; set; }
        public IDbSet<InstallationView> InstallationViews { get; set; } 
        public FakeContext()
        {
            Database = new SimulatorContext().Database;
            Properties = new FakeDbSet<Property>();
            Associations = new FakeDbSet<Association>();
            Installations = new FakeDbSet<Installation>();
            Connections = new FakeDbSet<Connection>();
            ControlObjects = new FakeDbSet<ControlObject>();
            Subscriptions = new FakeDbSet<Subscription>();
            MessageLogs = new FakeLogSet();
            UserInvitations = new FakeDbSet<UserInvitation>();
            ApplicationUsers = new FakeDbSet<ApplicationUser>();
        }

        public void Dispose()
        {
            
        }

        public int SaveChanges()
        {
            return 0;
        }

        public void RemoveRange(IList<Row> r)
        {
        }

    }

    #endregion
}
