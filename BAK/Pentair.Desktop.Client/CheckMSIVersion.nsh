;
!macro  CheckMSIVersion
!define MSINSTALLER_URL "http://download.microsoft.com/download/9/e/1/9e14751c-f897-4bbd-af7a-890d9a0f5430/WindowsInstaller-KB884016-v2-x86.exe"
  GetDllVersion "$SYSDIR\MSI.dll" $R0 $R1
  IntOp $R2 $R0 / 0x00010000
  IntOp $R3 $R0 & 0x0000FFFF
 
  IntCmp $R2 3 0 InstallMSI RightMSI
  IntCmp $R3 0 RightMSI InstallMSI RightMSI
 
  RightMSI:
    Goto ExitMacro
 
  InstallMSI:
    MessageBox MB_OK|MB_ICONEXCLAMATION \
"Windows Installer 3.0 was not found.This is required for installation. \
Setup will install the Windows Installer. This may take a while."

     NSISDL::download /TIMEOUT=30000 ${MSINSTALLER_URL} "$TEMP\windowsinstaller.exe" 
	 Pop $0
	 
	${If} $0 == "cancel"
	GOTO GiveupMSI
	${ElseIf} $0 != "success"
	GOTO GiveupMSI
	${EndIf}
  
	ExecWait '$TEMP\windowsinstaller.exe' $1
	${IF} $1 == 0
	GOTO ExitMacro
	${ElseIF} $1 == 3010 
	GOTO ExitMacro
	${ENDIF}
		
	GiveupMSI:
	Delete "$TEMP\windowsinstaller.exe" 
	Abort "Installation cancelled by user."
	
  ExitMacro:
	Delete "$TEMP\windowsinstaller.exe" 
 
!macroend
