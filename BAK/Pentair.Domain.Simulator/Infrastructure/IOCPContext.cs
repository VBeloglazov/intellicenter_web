﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pentair.Domain.Simulator.Entity;
using Pentair.Domain.Simulator.Migrations;

namespace Pentair.Domain.Simulator.Infrastructure
{
    public interface IOcpContext
    {
        IDbSet<Property> Properties { get; }
        IDbSet<Body> Bodies { get; }
        IDbSet<Circuit> Circuits { get; } 
        IDbSet<Chemistry> Chemistries { get; }
        IDbSet<Heater> Heaters { get; }
        IDbSet<MessageLog> MessageLogs { get; }
        int SaveChanges();
    }
}
