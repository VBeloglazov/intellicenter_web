﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pentair.Domain.Client.BusinessLogic
{

    public enum NotificationResults
    {
        Success,
        UnknownError
    };

    public class Notifications
    {
        /// <summary>
        /// Send a Delete User Notification to the person you are
        /// removing from your pool
        /// </summary>
        /// <param name="userEmail">email address of person</param>
        /// <param name="property">name of pool property</param>
        /// <returns></returns>
        public NotificationResults SendDeleteUserNotification(string userEmail, string property)
        {
            var result = NotificationResults.UnknownError;

            return result;
        }
    }
}
