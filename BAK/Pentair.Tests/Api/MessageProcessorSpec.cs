﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;
using System.Windows.Documents;
using Machine.Specifications;
using Machine.Fakes;
using Microsoft.AspNet.Identity.EntityFramework;
using Moq;
using Newtonsoft.Json;
using Pentair.Domain.Client.Entity;
using Pentair.Domain.Client.Helpers;
using Pentair.Domain.Client.Helpers.Commands;
using Pentair.Domain.Client.Infrastructure;
using Pentair.Domain.Client.BusinessLogic;
using Pentair.Web.Api.WebSockets;
using Pentair.Web.Api;
using It = Machine.Specifications.It;

namespace Pentair.Tests.Api
{
    class MessageProcessorSpec
    {
        #region Database Records

        private static string property1 = "8e69e5ff-4cf3-41a5-8a28-adcd5978afbb";
        private static string user1 = "668c6b40-97e9-4064-8e2c-e3bcf732095b";

        public class TestContent
        {
            private readonly ApplicationUserManager _identity;

            public IApiContext Data { get; private set; }
            public IQueryable<ApplicationUser> Users { get { return _identity.Users; } }

            public TestContent()
            {
                _identity = new ApplicationUserManager(
                    new UserStore<ApplicationUser>(
                        new ApplicationUserContext()));

                Data = new FakeContext();
            }
        }

        #endregion

        #region Tests
        public class WhenEditingUserSecurity
        {
            private static readonly Guid user = new Guid(user1);
            private static readonly Guid property = new Guid(property1);

            //private Establish context = () =>
            //{
            //    ua = new MockRepository(MockBehavior.Strict)
            //        .Of<IUserAssociations>()
            //        .First(s => s.ChangeUserAccess("test@bhst.com", "ADMIN", user, property) == UserManagementResult.Success);
            //    um = new MockRepository(MockBehavior.Strict)
            //    .Of<UserStore<ApplicationUser>>(new ApplicationUserContext())
                
            //    subject = new MessageProcessor(um, new TestContent().Data);
            //    message = JsonConvert.SerializeObject(new PoolAccessUser(
            //        new Dictionary<string, string>()
            //        {
            //            {"EMAIL", "test@bhst.com"},
            //            {"PERMISSIONGROUP", "ADMIN"}
            //        },
            //        "",
            //        CommandString.EditUser));
            //};

            //private Because of = () => result = subject.EditUser(message, new Guid(property1));

            //private It editUserShouldReturn = () => result.Equals(
            //    JsonConvert.SerializeObject(new StatusResponse()
            //    {
            //    command = CommandString.ApiResponse,
            //    status = CommandString.SuccessMessage,
            //    messageDetail = "Changed",
            //    messageID = ""
            //    }));
        }


        #endregion
    }
}
