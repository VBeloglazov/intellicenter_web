﻿using System;
using System.Collections.Generic;

using Newtonsoft.Json;

namespace Pentair.Web.Api
{
    public class ExtendedParamObject
    {
        [JsonProperty("changes", Required = Required.Default)]
        public List<ParamObject> Changes { get; private set; }

        [JsonProperty("deleted", Required = Required.Default)]
        public List<string> Deleted { get; private set; }

        [JsonProperty("created", Required = Required.Default)]
        public List<ParamObject> Created { get; private set; }

        public ExtendedParamObject(
            List<ParamObject> changes = null,
            List<string> deleted = null,
            List<ParamObject> created = null)
        {
            this.Changes = changes;
            this.Deleted = deleted;
            this.Created = created;
        }

    } // class ExtendedParamObject
}