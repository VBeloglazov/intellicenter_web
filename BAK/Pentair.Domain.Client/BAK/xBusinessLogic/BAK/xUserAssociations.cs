﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Security.Cryptography.X509Certificates;

using Microsoft.AspNet.Identity;

using Pentair.Domain.Client.BusinessLogic.Classes;
using Pentair.Domain.Client.DataLayer;
using Pentair.Domain.Client.Helpers;
using Pentair.Domain.Client.Helpers.Commands;
using Pentair.Domain.Client.Infrastructure;

namespace Pentair.Domain.Client.BusinessLogic
{
    public enum UserManagementResult
    {
        UserNotFound,
        LastAdminError,
        PropertyNotFound,
        UserNotAssociatedWithProperty,
        InviteNotFound,
        Success,
        UnknownError
    };

    /// <summary>
    /// User Associations implements the business logic for managing
    /// the associations between users and pool access.
    /// </summary>
    public class UserAssociations
    {
        private readonly ApiContext _dBase;
        private int _totalCount;
        private int _totalAdmin;

        public UserAssociations(
            ApiContext db)
        {
            _dBase = db;
            _totalCount = 0;
            _totalAdmin = 0;
        }

        public int TotalCount { get { return _totalCount; } }
        public int TotalAdmins { get { return _totalAdmin; } }

        /// <summary>
        /// UserInstallationList retrieves a list of pools that the
        /// user has access to.
        /// </summary>
        /// <param name="userId">UID of user to retrieve a list of pools for</param>
        /// <returns>A list of UserInstallations objects</returns>
        public List<UserInstallations> UserInstallationListWithDetails(Guid userId)
        {
            var keys = new string[]
            {
                "ADDRESS",
                "CITY",
                "STATE",
                "COUNTRY",
                "TIMZON",
                "NAME",
                "ZIP"

                // VB Task 104
                , "PHONE"
                , "EMAIL"
            };

            var InstallationDetails = (from o in _dBase.ControlObjects
                                       join q in _dBase.ControlObjects
                                           .Where(x => keys.Contains(x.Key))
                                           .GroupBy(x => new
                                           {
                                               x.INSTALLATION,
                                               x.OBJNAM,
                                               x.Key
                                           })
                                           .Select(x => new
                                           {
                                               item = x.Key,
                                               Revision = x.Max(z => z.Revision)
                                           })
                                           on new { o.INSTALLATION, o.OBJNAM, o.Key, o.Revision } equals
                                           new { q.item.INSTALLATION, q.item.OBJNAM, q.item.Key, q.Revision }
                                       where o.OBJNAM == "_5451"
                                       select o).ToList();

            var userList = new List<UserInstallations>();

            var data =
                from a in _dBase.Associations
                join i in _dBase.Installations on a.InstallationId equals i.InstallationId
                where a.UserId == userId
                select
                new
                {
                    InstallationId = i.InstallationId,
                    AccessToken = a.AccessToken,
                    PoolName = i.Name,
                    LastStatusChange = i.UpdateDateTime,
                    TotalAdmins =
                        a.Installation.Associations.Count(
                            x => x.AccessToken.Equals(StaticData.Admin) && x.InstallationId.Equals(a.InstallationId))
                };

            foreach (var i in data)
            {
                var installation = InstallationDetails.Where(x => x.INSTALLATION == i.InstallationId);
                userList.Add(new UserInstallations()
                {
                    InstallationId = i.InstallationId,
                    AccessToken = i.AccessToken,
                    PoolName = i.PoolName,
                    OnLine = Utilities.IsConnectionValid(i.InstallationId),
                    LastStatusChange = i.LastStatusChange,
                    Address = installation.Where(x => x.Key == "ADDRESS").Select(x => x.Value).FirstOrDefault() ?? "",
                    City = installation.Where(x => x.Key == "CITY").Select(x => x.Value).FirstOrDefault() ?? "",
                    State = installation.Where(x => x.Key == "STATE").Select(x => x.Value).FirstOrDefault() ?? "",
                    Zip = installation.Where(x => x.Key == "ZIP").Select(x => x.Value).FirstOrDefault() ?? "",
                    Country = installation.Where(x => x.Key == "COUNTRY").Select(x => x.Value).FirstOrDefault() ?? "",
                    TimeZone = installation.Where(x => x.Key == "TIMZON").Select(x => x.Value).FirstOrDefault() ?? "",
                    OwnerName = installation.Where(x => x.Key == "NAME").Select(x => x.Value).FirstOrDefault() ?? "",
                    TotalAdmins = i.TotalAdmins

                    // VB Task 104
                    ,
                    Phone = installation.Where(x => x.Key == "PHONE").Select(x => x.Value).FirstOrDefault() ?? ""
                    ,
                    Email = installation.Where(x => x.Key == "EMAIL").Select(x => x.Value).FirstOrDefault() ?? ""

                });
            }

            return userList;
        }


        /// <summary>
        /// UserInstallationList retrieves a list of pools that the
        /// user has access to.
        /// </summary>
        /// <param name="userId">UID of user to retrieve a list of pools for</param>
        /// <returns>A list of UserInstallations objects</returns>
        /// <summary>
        /// Build a list of users associated with this pool
        /// </summary>
        /// <param name="propertyId">Pool Id</param>
        /// <param name="users">ApplicatioinUserContext for retrieving user data</param>
        /// <param name="pageSize">result page size</param>
        /// <param name="page">result page</param>
        /// <param name="sortField">sortation field</param>
        /// <param name="sortDirection">sort direction</param>
        /// <param name="search">search term</param>
        /// <param name="filter">status filter</param>
        /// <returns></returns>
        public List<AssociatedUser> UserAssociationList(
            int propertyId,
            IQueryable<ApplicationUser> users,
            int pageSize,
            int page,
            string sortField = "email",
            SortDirection sortDirection = SortDirection.Ascending,
            string search = "",
            string filter = "all")
        {

            var rawList = new List<AssociatedUser>();
            var acceptedUsers = AcceptedUsers(propertyId, users);
            switch (filter.ToLower())
            {
                case "all":
                    rawList.AddRange(acceptedUsers);
                    rawList.AddRange(PendingUsers(propertyId, filter));
                    break;
                case "accepted":
                    rawList.AddRange(acceptedUsers);
                    break;
                default:
                    rawList.AddRange(PendingUsers(propertyId, filter));
                    break;
            }

            var filteredList = new List<AssociatedUser>();

            if (!string.IsNullOrEmpty(search))
            {
                filteredList = (from user in rawList
                                where user.EmailAddress.ToLower().Contains(search.ToLower())
                                select user).ToList();
            }
            else
            {
                filteredList.AddRange(rawList);
            }
            _totalCount = filteredList.Count;
            _totalAdmin = acceptedUsers.Where(x => x.AccessToken == StaticData.Admin).Count();
            return OrderedList(filteredList, sortField, sortDirection, page, pageSize);
        }

        /// <summary>
        /// Remove a user association from a pool
        /// </summary>
        /// <param name="user">user to remove</param>
        /// <param name="property">property to remove</param>
        public UserManagementResult RemoveUserFromInstallation(Guid user, int property)
        {
            var result = UserManagementResult.UnknownError;

            var admins = (from x in _dBase.Associations
                          where x.AccessToken.Equals(StaticData.Admin) &&
                      x.InstallationId.Equals(property)
                          select x).Count();
            var install = (_dBase.Associations
                .Where(x => x.InstallationId == property &&
                            x.UserId == user)).FirstOrDefault();
            if (null != install)
            {
                if (install.AccessToken == StaticData.Admin &&
                    admins < 2)
                {
                    result = UserManagementResult.LastAdminError;
                }
                else
                {
                    _dBase.Associations.Remove(install);
                    result = UserManagementResult.Success;
                }
            }
            else
            {
                result = UserManagementResult.UserNotAssociatedWithProperty;
            }
            _dBase.SaveChanges();
            return result;
        }

        /// <summary>
        /// Change the access rights for a remote pool user
        /// </summary>
        /// <param name="email">users email address</param>
        /// <param name="token">new access rights token</param>
        /// <param name="user">User Id</param>
        /// <param name="property">Property Id</param>
        /// <returns></returns>
        public UserManagementResult ChangeUserAccess(string email, string token, Guid user, int property)
        {
            var result = UserManagementResult.UserNotFound;


            var invite = _dBase.UserInvitations
           .FirstOrDefault(x => x.InstallationId == property &&
                                x.EmailAddress == email);
            if (null != invite)
            {
                invite.AccessToken = token;
                result = UserManagementResult.Success;
            }

            var association = _dBase.Associations
                .FirstOrDefault(x => x.InstallationId == property &&
                                     x.UserId == user);
            if (null != association)
            {
                var admins = (from x in _dBase.Associations
                              where x.AccessToken.Equals(StaticData.Admin) &&
                          x.InstallationId.Equals(property)
                              select x).Count();

                if (token != StaticData.Admin &&
                    admins < 2)
                {
                    result = UserManagementResult.LastAdminError;
                }
                else
                {
                    association.AccessToken = token;
                    result = UserManagementResult.Success;
                    _dBase.SaveChanges();
                }

            }
            return result;
        }

        /// <summary>
        /// Remove the invitation for this user
        /// </summary>
        /// <param name="user">Email address of user invite</param>
        /// <param name="property">Property ID of invite</param>
        /// <returns></returns>
        public UserManagementResult RemoveUserInvite(string user, int property)
        {
            var result = UserManagementResult.UnknownError;

            var invite = _dBase.UserInvitations
                .FirstOrDefault(x => x.InstallationId == property &&
                                     x.EmailAddress == user);
            if (null == invite)
            {
                result = UserManagementResult.InviteNotFound;
            }

            else
            {
                _dBase.UserInvitations.Remove(invite);
                result = UserManagementResult.Success;
            }
            _dBase.SaveChanges();

            return result;
        }

        public PoolInformation PoolInfo(int property)
        {
            var info = new PoolInformation();
            var pool = _dBase.Installations.FirstOrDefault(x => x.InstallationId == property);
            info.PoolName = null == pool ? "Unknown" : pool.Name;
            return info;
        }

        private IEnumerable<AssociatedUser> AcceptedUsers(
            int propertyId,
            IQueryable<ApplicationUser> users)
        {
            List<AssociatedUser> userList = (from a in _dBase.Associations
                                             join x in _dBase.Installations on a.InstallationId equals x.InstallationId
                                             where a.InstallationId == propertyId
                                             select new AssociatedUser()
                                             {
                                                 UserId = a.UserId,
                                                 AccessToken = a.AccessToken,
                                                 Status = "ACCEPTED"
                                             }
                ).ToList();

            var om = new ObjectManager(_dBase, propertyId);

            foreach (var i in userList)
            {
                i.SecurityName = om.ParamValue(i.AccessToken, "SNAME");

                // This test is necessary for unit testing ... until we figure out
                // how to provide an IQueryable<ApplicationUser> object.
                if (null != users)
                {
                    var user = (from u in users where (u.Id == i.UserId.ToString()) select u).FirstOrDefault();
                    if (null != user)
                    {
                        i.UserName = user.UserName;
                        i.EmailAddress = user.Email;
                    }
                }
            }

            return userList;

        } // AcceptedUsers()

        private List<AssociatedUser> OrderedList(List<AssociatedUser> users, string sort, SortDirection direction, int page, int length)
        {
            var start = 0 == length ? 0 : page * length;
            var lines = 0 == length ? users.Count() : length;

            if (lines > 100)
            {
                lines = 100;
            }
            var sortedList = new List<AssociatedUser>();
            if (direction == SortDirection.Ascending)
            {
                switch (sort.ToLower())
                {
                    case "name":
                        sortedList = (from item in users.OrderBy(x => x.UserName) select item).ToList();
                        break;
                    case "level":
                        sortedList = (from item in users.OrderBy(x => x.SecurityName) select item).ToList();
                        break;
                    case "status":
                        sortedList = (from item in users.OrderBy(x => x.Status) select item).ToList();
                        break;
                    default:
                        sortedList = (from item in users.OrderBy(x => x.EmailAddress) select item).ToList();
                        break;
                }
            }
            else
            {
                switch (sort.ToLower())
                {
                    case "name":
                        sortedList = (from item in users.OrderByDescending(x => x.UserName) select item).ToList();
                        break;
                    case "level":
                        sortedList = (from item in users.OrderByDescending(x => x.SecurityName) select item).ToList();
                        break;
                    case "status":
                        sortedList = (from item in users.OrderByDescending(x => x.Status) select item).ToList();
                        break;
                    default:
                        sortedList = (from item in users.OrderByDescending(x => x.EmailAddress) select item).ToList();
                        break;
                }
            }
            return sortedList.GetRange(start, lines > sortedList.Count - start ? sortedList.Count - start : lines);
        }

        private List<AssociatedUser> PendingUsers(
            int propertyId,
            string filter)
        {
            var invites = (from i in _dBase.UserInvitations
                           where i.InstallationId == propertyId &&
                                 i.Status != "ACCEPTED"
                           select i).ToList();

            foreach (var invite in invites)
            {
                if (invite.Status == "UNKNOWN" &&
                    invite.CreateDateTime.AddHours(StaticData.InviteValidAge) < DateTime.Now)
                {
                    invite.Status = "EXPIRED";
                }
            }

            var filteredInvites = new List<UserInvitation>();

            switch (filter.ToLower())
            {
                case "expired":
                    filteredInvites = (from invite in invites where invite.Status == "EXPIRED" select invite).ToList();
                    break;
                case "bounced":
                    filteredInvites = (from invite in invites where invite.Status == "BOUNCED" select invite).ToList();
                    break;
                case "noresponse":
                    filteredInvites = (from invite in invites where invite.Status == "UNKNOWN" select invite).ToList();
                    break;
                default:
                    filteredInvites.AddRange(invites);
                    break;
            }

            var userList = new List<AssociatedUser>();

            var om = new ObjectManager(_dBase, propertyId);

            userList = (from invitee in filteredInvites
                        let access = (from o in _dBase.ControlObjects
                                      where o.INSTALLATION == propertyId &&
                                            o.OBJNAM == invitee.AccessToken
                                      select o).FirstOrDefault()
                        select new AssociatedUser()
                        {
                            UserId = invitee.InvitationId,
                            UserName = invitee.EmailAddress,
                            EmailAddress = invitee.EmailAddress,
                            AccessToken = invitee.AccessToken,
                            SecurityName = om.ParamValue(invitee.AccessToken, "SNAME"),
                            Status = invitee.Status
                        }).ToList();

            return userList;

        } // PendingUsers()
    }
}
