﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Pentair.Domain.Simulator.Entity
{
    public class Circuit
    {
        [Key]
        [MaxLength(16)]
        public string OBJNAM { get; set; }
        public UInt32 OBJREV { get; set; }
        [MaxLength(16)]
        public string OBJTYP { get; set; }
        [MaxLength(16)]
        public string STATIC { get; set; }
        [MaxLength(16)]
        public string SUBTYP { get; set; }
        [MaxLength(128)]
        public string HNAME { get; set; }
        [MaxLength(128)]
        public string SNAME { get; set; }
        public int RECENT { get; set; }
        public int FREQ { get; set; }
        public int CUSTOM { get; set; }
        public int LISTORD { get; set; }
        [MaxLength(16)]
        public string SHOMNU { get; set; }
        public int ALPHA { get; set; }
        [MaxLength(16)]
        public string SERVICE { get; set; }
        [MaxLength(16)]
        public string STATUS { get; set; }
        public int TIMOUT { get; set; }
        [MaxLength(16)]
        public string SOURCE { get; set; }
        [MaxLength(128)]
        public string USE { get; set; }
        [MaxLength(16)]
        public string RLY { get; set; }
        [MaxLength(16)]
        public string FREEZE { get; set; }
        [MaxLength(16)]
        public string MANUAL { get; set; }
        public int TIME { get; set; }
        [MaxLength(16)]
        public string ACT { get; set; }
        [MaxLength(16)]
        public string UNITS { get; set; }
        public int SINDEX { get; set; }
        [MaxLength(128)]
        public string INVALID { get; set; }
        public int COUNT { get; set; }
    }
}