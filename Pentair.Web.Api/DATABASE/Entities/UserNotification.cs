﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Pentair.Web.Api
{
    // TODO: 2b removed
    /*
    public enum NotificationType
    {
        Email,
        SMS
    }

    public class UserNotification
    {
        [Key]
        [Column(Order = 1)]
        public Guid UserId { get; set; }

        [Key]
        [Column(Order = 2)]
        public int InstallationId { get; set; }

        [Key]
        [Column(Order = 3)]
        public int EventId { get; set; }

        [ForeignKey("InstallationId")]
        public virtual Installation Installation { get; set; }

        [ForeignKey("EventId")]
        public virtual EventType Event { get; set; }

        public TimeSpan Duration { get; set; }

        public int NotificationType { get; set; }

        [Column(TypeName = "DateTime2")]
        public DateTime NotificationSent { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(TypeName = "DateTime2")]
        public DateTime CreateDateTime { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [Column(TypeName = "DateTime2")]
        public DateTime UpdateDateTime { get; set; }
    
        public void CopyFrom(
            UserNotification s)
        {
            this.UserId = s.UserId;
            this.InstallationId = s.InstallationId;
            this.EventId = s.EventId;
            this.Installation = s.Installation;
            this.Event = s.Event;
            this.Duration = s.Duration;
            this.NotificationType = s.NotificationType;
            this.NotificationSent = s.NotificationSent;

        } // CopyFrom()

    } // class UserNotification
     */
}

