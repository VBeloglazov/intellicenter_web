﻿'use strict';
describe('DashboardController', function () {
    var PropertyService;
    var $location;
    var $scope;
    var ConfigController;
    var $controller;
    beforeEach(module('PentairWebClient'));

    beforeEach(inject(function (_$controller_, _$rootScope_, _PropertyService_, _$location_) {
        $scope = _$rootScope_.$new();
        PropertyService = _PropertyService_;
        $location = _$location_;
        $controller = _$controller_;

        ConfigController = $controller("ConfigController", { $scope: $scope });
        spyOn($location, 'path');

    }));

    
    describe('GoToDashboard', function () {
        it('should take the user the main page', function () {
            $scope.GoToDashboard();
            expect($location.path).toHaveBeenCalledWith('/property');
        });
    });
    
    describe('GoToSystemProtectionPage', function () {
        it('should take the user the System Protection page', function () {
            $scope.GoToSystemProtectionPage();
            expect($location.path).toHaveBeenCalledWith('/systemProtection');
        });
    });
    describe('GoToSystemInformationPage ', function () {
        it('should take the user the System Information page', function () {
            $scope.GoToSystemInformationPage ();
            expect($location.path).toHaveBeenCalledWith('/systemInformation');
        });
    });
});
