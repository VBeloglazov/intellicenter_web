﻿using Machine.Specifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pentair.Tests
{
    public class Class1
    {
        [Subject("DummyTest")]
        public class When_running_web_api_tests_they_should_pass
        {
            static int dummy;

            Establish context = () =>
                dummy = 0;

            Because of = () =>
                dummy += 1;

            It should_pass_the_test = () =>
                dummy.Equals(1);
        }
    }
}
