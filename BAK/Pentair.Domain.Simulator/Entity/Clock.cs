﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pentair.Domain.Simulator.Entity
{
    public class Clock
    {
        [Key]
        public int ID { get; set; }
        public string Name { get; set; }
        public UInt32 LastRevision { get; set; }
        public string ZipCode { get; set; }
        public string CityState { get; set; }
        public float Longitude { get; set; }
        public float Latitude { get; set; }
        public int UtcOffset { get; set; }
        public int ClockOffset { get; set; }
        public int OffsetFromUtc { get; set; }
        public bool Acceleration { get; set; }
        public DateTime LocalDateTime { get; set; }
        public DateTime UtcDateTime { get; set; }
        public DateTime LocalSunrise { get; set; }
        public DateTime LocalSunset { get; set; }

        // Foreign Keys
        public int TypeID { get; set; }
        public int DaylightSavingTimeModeID { get; set; }
        public int ClockModeID { get; set; }
        public int ClockSourceID { get; set; }
    }
}