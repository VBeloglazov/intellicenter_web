namespace Pentair.Domain.Client.ClientMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lastPinged : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Installations", "LastPinged", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Installations", "LastPinged");
        }
    }
}
