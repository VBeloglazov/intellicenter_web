﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pentair.Domain.Simulator.Entity
{
    public class SystemConfig
    {
        [Key]
        public int ID { get; set; }
        public string Name { get; set; }
        public UInt32 LastRevision { get; set; }
        public int TypeID { get; set; }
        public int UnitID { get; set; }

        public virtual ICollection<CircuitGroup> CircuitGroups { get; set; }
        public virtual ICollection<Heater> Heaters { get; set; }
        public virtual ICollection<HeaterAffect> HeaterAffects { get; set; }
        public virtual ICollection<HeaterGroup> HeaterGroups { get; set; }
        public virtual ICollection<Pump> Pumps { get; set; }
        public virtual ICollection<PumpCircuit> PumpCircuits { get; set; }
        public virtual ICollection<Schedule> Schedules { get; set; }
        public virtual ICollection<Valve> Valves { get; set; }
        public virtual ICollection<ValveGroup> ValveGroups { get; set; }
        public virtual ICollection<ValveSemaphore> ValveSemaphores { get; set; }
    }
}