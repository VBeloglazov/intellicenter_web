USE [PathfinderIntellicenter]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (
	SELECT * FROM sys.objects
	WHERE object_id = OBJECT_ID(N'dbo.pr_GetOwnerAddressList')
	AND type in (N'P', N'PC')
)
DROP PROCEDURE dbo.pr_GetOwnerAddressList
GO


-- =============================================
-- Author			:	Wayne Hui
-- Create date		:	03/11/2019
-- Description		:	Gets the owner address and their installations
-- Input			:	N/A
-- Output			:	Email, Installation, Address, City, State, Zip
--
-- Modified By			Date		Description
---- =============================================
CREATE PROCEDURE [dbo].[pr_GetOwnerAddressList]
AS
BEGIN

	SET NOCOUNT OFF

	----Grab all control objects items for the address
	IF OBJECT_ID('tempdb.dbo.#AddressList', 'U') IS NOT NULL DROP TABLE #AddressList
		SELECT DISTINCT 
			Installation, [Key], Value
		INTO #AddressList
		FROM [dbo].[ControlObjects] CO 
		WHERE
			[Key] IN ( 'ADDRESS', 'CITY', 'STATE', 'ZIP' )


	----Return all installations and their owner address
	SELECT
		AU.Email
		, I.Name AS Installation
		, I.InstallationID
		, AL1.Value AS [Address]
		, AL2.Value AS City
		, AL3.Value AS [State]
		, AL4.Value AS Zip
	FROM 
		[dbo].[Installations] I
		LEFT JOIN [dbo].[AspNetUsers] AU
			ON AU.Id = I.OwnerUserId
		LEFT JOIN #AddressList AL1 
			ON AL1.INSTALLATION = I.InstallationId
			AND AL1.[Key] = 'ADDRESS'
		LEFT JOIN #AddressList AL2 
			ON AL2.INSTALLATION = I.InstallationId
			AND AL2.[Key] = 'CITY'
		LEFT JOIN #AddressList AL3 
			ON AL3.INSTALLATION = I.InstallationId
			AND AL3.[Key] = 'STATE'
		LEFT JOIN #AddressList AL4 
			ON AL4.INSTALLATION = I.InstallationId
			AND AL4.[Key] = 'ZIP'		
	ORDER BY
		Email, Name, [State], City, [Address]

                                                     
END
GO

--GRANT EXECUTE ON [dbo].[pr_GetOwnerAddressList] TO PUBLIC
--GO
