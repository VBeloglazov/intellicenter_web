-- =============================================================
--
-- Author:		Vladimir Beloglazov
-- Create date: 01/26/2018
--
-- Creates:
--
--		Table Type:			None
--
--		Stored Procedure:	PReRegisterProperty
--
-- =============================================================

USE PathfinderIntelliCenter
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (
	SELECT * FROM sys.objects
	WHERE object_id = OBJECT_ID(N'dbo.PReRegisterProperty')
	AND type in (N'P', N'PC')
)
DROP PROCEDURE dbo.PReRegisterProperty
GO

CREATE PROCEDURE dbo.PReRegisterProperty
	 @installationId AS int
	,@userId AS uniqueidentifier
	,@newSecret AS nvarchar(255)

AS
BEGIN

	SET NOCOUNT OFF

	DECLARE @httpStatusCode int
	DECLARE @secretRet nvarchar(255)

	IF NOT EXISTS (
		SELECT *
		FROM dbo.Installations
		WHERE InstallationId = @installationId
    )
		BEGIN
			-- Property ID does not exist, it cannot be re-assigned
			SET @httpStatusCode = 404 -- NotFound
			SET @secretRet = ''
		END
	ELSE
		BEGIN
			IF EXISTS (
				SELECT *
				FROM dbo.Connections
				WHERE InstallationId = @installationId
				AND UserId = CAST(0x0 AS uniqueidentifier)
			)
				BEGIN
                    -- If the pool is connected, do not allow reassignment
					SET @httpStatusCode = 403 -- Forbidden
					SET @secretRet = ''
				END
			ELSE
				BEGIN
					IF NOT EXISTS (
						SELECT * FROM Associations
						WHERE InstallationId = @installationId
						AND UserId = @userId
					)
						BEGIN
							-- If the user is not associated with the pool do not allow reassignment
							SET @httpStatusCode = 303 -- NotModified
							SET @secretRet = ''
						END
					ELSE
						BEGIN
							-- Reset pool secret
							UPDATE Installations
							SET [Secret] = @newSecret
							WHERE InstallationId = @installationId

							SET @httpStatusCode = 200 -- OK
							SET @secretRet = @newSecret
						END
				END

		END

	-- Result set
	SELECT @httpStatusCode AS HttpStatusCode, @secretRet AS NewSecret

END
GO
