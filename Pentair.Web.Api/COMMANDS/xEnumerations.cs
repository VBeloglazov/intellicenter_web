﻿using System.Collections.Generic;

namespace Pentair.Domain.Client.Helpers.Commands
{
    public enum EndpointType
    {
        Unknown = 0, // To catch errors
        THW,
        Client,
        Server
    };

    public enum SortDirection
    {
        Ascending,
        Decending
    }

    public enum ParameterType
    {
        History,
        Saved,
        Both
    }

    public class ResponseString
    {
        public const string
            OK = "200",
            Created = "201",
            NotModified = "304",
            BadRequest = "400",
            Unauthorized = "401",
            Forbidden = "403",
            NotFound = "404",
            InternalServerError = "500";
    }

    /// <summary>
    /// Commands received from a client or issued to the THW
    /// </summary>
    public class CommandString
    {
        public const string
            GetRegisteredProperties = "GETREGISTEREDPROPERTIES",
            GetParamList = "GETPARAMLIST",
            SetParamList = "SETPARAMLIST",
            WriteParamList = "WRITEPARAMLIST",
            RequestParamList = "REQUESTPARAMLIST",
            ReleaseParamList = "RELEASEPARAMLIST",
            ClearParam = "CLEARPARAM",
            ClearHistoryMessage = "CLEARHISTORYMESSAGE",
            GetQuery = "GETQUERY",
            SendQuery = "SENDQUERY",
            SetCommand = "SETCOMMAND",
            AcknowledgeRegistration = "ACKNOWLEDGEREGISTRATIONREQUEST",
            GetParamListResponse = "SENDPARAMLIST",
            CreateObject = "CREATEOBJECT",
            ObjectCreated = "OBJECTCREATED",
// 2017-10-04            ReadParam = "READPARAM",
            NotifyList = "NOTIFYLIST",
            Registration = "REQUESTREGISTRATION",
            Authenticate = "REQUESTAUTHENTICATION",
            QueryPermissionLevel = "QUERYPERMISSIONLEVEL",
            InviteUser = "INVITEUSER",
            ResendInvite = "RESENDINVITE",
            DeleteUser = "DELETEUSER",
            UserList = "USERLIST",
            UserNotificationList = "USERNOTIFICATIONLIST",
            UserNotificationEdit = "EDITNOTIFICATION",
            EditUser = "EDITUSER",
            SystemInfoList = "SYSTEMINFO",
            SystemInfoEdit = "SAVESYSTEMINFO",
            ApiResponse = "APIRESPONSE",
            SuccessMessage = "OK",
            ErrorMessage = "ERROR",
            NotFoundMessage = "NOTFOUND",
            Ping = "PING",
            Invalid = "INVALIDREQUEST",

            ReadHistoryMessage = "READHISTORYMESSAGE",
            WriteHistoryMessage = "WRITEHISTORYMESSAGE",
// 2017-11-29            ErrorHistoryMessage = "ERRORHISTORYMESSAGE",
            ReadLastHistoryTime = "READLASTHISTORYTIME",
            WriteLastHistoryTime = "WRITELASTHISTORYTIME",

            ReadStatusMessage = "READSTATUSMESSAGE",
            WriteStatusMessage = "WRITESTATUSMESSAGE",

            ServerRemoveThwClients = "SERVERREMOVETHWCLIENTS",

            // 2017-11-29
            WriteHistoryMessageResponse = "WRITEHISTORYMESSAGERESPONSE"
        ;

    } // class CommandString

    public enum CommandType
    {
        GetRegisteredProperties,
        GetParamListResponse,
        WriteParamList,
        NotifyList,
        CreateObject,
        ObjectCreated,
// 2017-10-04        ReadParam,
        Registration,
        Authenticate,
        QueryPermissionLevel,
        GetParamList,
        SetParamList,
        RequestParamList,
        ReleaseParamList,
        ClearParam,
        ClearHistoryMessage, 
        GetQuery,
        SendQuery,
        SetCommand,
        RegistrationAcknowledgement,
        InviteUser,
        ResendInvite,
        DeleteUser,
        UserList,
        UserNotificationList,
        UserNotificationEdit,
        EditUser,
        SystemInfoList,
        SystemInfoEdit,
        ApiResponse,
        SuccessMessage,
        ErrorMessage,
        Invalid,
        Ping,

        ReadHistoryMessage,
        WriteHistoryMessage,
        ErrorHistoryMessage,
        ReadLastHistoryTime,
        WriteLastHistoryTime,

        ReadStatusMessage,
        WriteStatusMessage,

        ServerRemoveThwClients

    } // enum CommandType

    public class CommandList
    {
        public static Dictionary<string, CommandType> Commands = new Dictionary<string, CommandType>()
        {
            {CommandString.GetRegisteredProperties, CommandType.GetRegisteredProperties},
            {CommandString.GetParamListResponse, CommandType.GetParamListResponse},
            {CommandString.WriteParamList, CommandType.WriteParamList},
            {CommandString.NotifyList, CommandType.NotifyList},
            {CommandString.CreateObject, CommandType.CreateObject},
// 2017-10-04            {CommandString.ReadParam, CommandType.ReadParam},
            {CommandString.ObjectCreated, CommandType.ObjectCreated},
            {CommandString.Registration, CommandType.Registration},
            {CommandString.Authenticate, CommandType.Authenticate},
            {CommandString.GetParamList, CommandType.GetParamList},
            {CommandString.SetParamList, CommandType.SetParamList},
            {CommandString.RequestParamList, CommandType.RequestParamList},
            {CommandString.ReleaseParamList, CommandType.ReleaseParamList},
            {CommandString.ClearParam, CommandType.ClearParam},
            {CommandString.ClearHistoryMessage, CommandType.ClearHistoryMessage}, 
            {CommandString.GetQuery, CommandType.GetQuery},
            {CommandString.SendQuery, CommandType.SendQuery},
            {CommandString.SetCommand, CommandType.SetCommand},
            {CommandString.AcknowledgeRegistration, CommandType.RegistrationAcknowledgement},
            {CommandString.QueryPermissionLevel, CommandType.QueryPermissionLevel},
            {CommandString.InviteUser, CommandType.InviteUser},
            {CommandString.ResendInvite, CommandType.ResendInvite},
            {CommandString.DeleteUser, CommandType.DeleteUser},
            {CommandString.UserList, CommandType.UserList},
            {CommandString.UserNotificationList, CommandType.UserNotificationList},
            {CommandString.UserNotificationEdit, CommandType.UserNotificationEdit},
            {CommandString.EditUser, CommandType.EditUser},
            {CommandString.SystemInfoList, CommandType.SystemInfoList},
            {CommandString.SystemInfoEdit, CommandType.SystemInfoEdit},
            {CommandString.SuccessMessage, CommandType.SuccessMessage},
            {CommandString.ErrorMessage, CommandType.ErrorMessage},
            {CommandString.Ping, CommandType.Ping},

            {CommandString.ReadHistoryMessage, CommandType.ReadHistoryMessage},
            {CommandString.WriteHistoryMessage, CommandType.WriteHistoryMessage},
// 2017-11-29            {CommandString.ErrorHistoryMessage, CommandType.ErrorHistoryMessage},
            {CommandString.ReadLastHistoryTime, CommandType.ReadLastHistoryTime},
            {CommandString.WriteLastHistoryTime, CommandType.WriteLastHistoryTime},

            {CommandString.ReadStatusMessage, CommandType.ReadStatusMessage},
            {CommandString.WriteStatusMessage, CommandType.WriteStatusMessage},

            {CommandString.ServerRemoveThwClients, CommandType.ServerRemoveThwClients}
        };
    
    } // class CommandList

    // TODO: In use?
    public class WebSocketActions
    {
        public const string 
            Open = "Open",
            Close = "Close",
            Send = "Send THW",
            Receive = "Receive THW",
            Error = "Error",
            SendClient = "Send Client",
            ReceiveClient = "Receive Client",
            ReceiveClientInvalid = "Receive Client Invalid"
        ;
    }
}