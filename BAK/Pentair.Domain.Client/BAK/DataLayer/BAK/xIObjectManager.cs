﻿using System.Collections.Generic;
using Pentair.Domain.Client.Entity;
using Pentair.Domain.Client.Helpers.Commands;
using Pentair.Domain.Client.Helpers.Entity;

namespace Pentair.Domain.Client.DataLayer
{
    // TODO: VB 2b removed (not in use)
    /*
    public interface IObjectManager
    {
        /// <summary>
        /// ChangesSince returtns a collection of ParamObject items
        /// representing all chnages that have occurred since the time stamp
        /// </summary>
        /// <param name="revision">time stamp</param>
        /// <returns></returns>
        IEnumerable<ParamObject> ChangesSince(string revision);

        /// <summary>
        /// BuildResponse builds a collection of ParamObject items
        /// from the collection of ControlObject items passed in.
        /// </summary>
        /// <param name="objects">ControlOBject input list</param>
        /// <param name="parms">List of parameters to include</param>
        /// <returns></returns>
        IEnumerable<ParamObject> BuildResponse(IEnumerable<ObjectView> objects, string[] parameters);

        /// <summary>
        /// GetObjectsByType returns a collection of ParamObject items
        /// whose OBJTYP patches the string passed in.
        /// </summary>
        /// <param name="type">OBJTYP value to search for</param>
        /// <returns></returns>
        IEnumerable<ParamObject> GetObjectsByType(string type);

        /// <summary>
        /// GetChildren returns a collection of ParamObject children
        /// of the object whose name is passed in.
        /// </summary>
        /// <param name="objectName">Object name</param>
        /// <returns></returns>
        IEnumerable<ParamObject> GetChildren(string objectName);

        /// <summary>
        /// ParamValue returns the value of the parameter
        /// whose parameter name and object name are passed in.
        /// </summary>
        /// <param name="objnam">Object name</param>
        /// <param name="key">Parameter name</param>
        /// <returns></returns>
        string ParamValue(string objnam, string key);
    }
    */
}
