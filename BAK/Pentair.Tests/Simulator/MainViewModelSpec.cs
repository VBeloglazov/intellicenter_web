﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Machine.Fakes;
using Machine.Specifications;
using Newtonsoft.Json;
using Pentair.Desktop.Simulator.Models;
using Pentair.Domain.Client.Entity;
using Pentair.Domain.Client.Helpers.Commands;
using Pentair.Domain.Client.Infrastructure;

namespace Pentair.Tests.Simulator
{
    class MainViewModelSpec
    {
        #region Database Records

        private static readonly IOcpContext OcpFakeContext = new FakeContext()
        {
            ParameterTypes =
            {
                new ParameterType()
                {
                    History = false,
                    OBJTYP = "BODY",
                    PARAM = "STATUS",
                    Saved = true
                }
            },

            ControlObjects =
            {
                new ControlObject()
                {
                    INSTALLATION = "Config",
                    OBJNAM = "C1355",
                    OBJTYP = "BODY",
                    SUBTYP = "BODY",
                    SNAME = "Pool Circuit",
                    RECENT = "0",
                    FREQ = 0,
                    CUSTOM = 0,
                    LISTORD = 1,
                    SHOMNU = "s",
                    STATUS = "ON",
                    TIMOUT = "0",
                    TIME = "0",
                    SINDEX = 0,
                    COUNT = 0
                },
                new ControlObject()
                {
                    INSTALLATION = "Config",
                    OBJNAM = "C1356",
                    OBJTYP = "BODY",
                    SUBTYP = "SPA",
                    SNAME = "Spa Circuit",
                    RECENT = "0",
                    FREQ = 0,
                    CUSTOM = 0,
                    LISTORD = 1,
                    SHOMNU = "s",
                    STATUS = "OFF",
                    TIMOUT = "0",
                    TIME = "0",
                    SINDEX = 0,
                    COUNT = 0
                },
                new ControlObject()
                {
                    INSTALLATION = "Config",
                    OBJNAM = "C5433",
                    OBJTYP = "CIRCUIT",
                    SUBTYP = "INTELLI",
                    SNAME = "Night Spa",
                    RECENT = "0",
                    FREQ = 0,
                    CUSTOM = 0,
                    LISTORD = 1,
                    SHOMNU = "l",
                    STATUS = "OFF",
                    TIMOUT = "0",
                    TIME = "0",
                    SINDEX = 0,
                    COUNT = 0
                },
                new ControlObject()
                {
                    INSTALLATION = "Config",
                    OBJNAM = "C5435",
                    OBJTYP = "CIRCUIT",
                    SUBTYP = "MAGIC2",
                    SNAME = "Night Laminar",
                    RECENT = "0",
                    FREQ = 0,
                    CUSTOM = 0,
                    LISTORD = 2,
                    SHOMNU = "l",
                    STATUS = "OFF",
                    TIMOUT = "0",
                    TIME = "0",
                    SINDEX = 0,
                    COUNT = 0
                },
                new ControlObject()
                {
                    INSTALLATION = "Config",
                    OBJNAM = "C5436",
                    OBJTYP = "CIRCUIT",
                    SUBTYP = "GENERIC",
                    SNAME = "Water Fall",
                    RECENT = "0",
                    FREQ = 0,
                    CUSTOM = 0,
                    LISTORD = 1,
                    SHOMNU = "f",
                    STATUS = "OFF",
                    TIMOUT = "0",
                    TIME = "0",
                    SINDEX = 0,
                    COUNT = 0
                },
                new ControlObject()
                {
                    INSTALLATION = "Config",
                    OBJNAM = "C5437",
                    OBJTYP = "CIRCUIT",
                    SUBTYP = "FRZ",
                    SNAME = "Freeze Mode",
                    RECENT = "0",
                    FREQ = 0,
                    CUSTOM = 0,
                    LISTORD = 1,
                    SHOMNU = "s",
                    STATUS = "ON",
                    TIMOUT = "0",
                    TIME = "0",
                    SINDEX = 0,
                    COUNT = 0
                }
            }
        };
        #endregion

        #region Tests
        public class WhenTheClientRequestsAllBodyObjectsThenSendThem : WithFakes
        {
            private static MainViewModel _subject;
            private const string Config = "Config";

            private Establish _ofEstablish = () =>
            {
                _subject = new MainViewModel("wss://localhost:44300/api/websocket");
            };

            private Because _ofBecause = () => _subject.SetConfiguration("Config");

            private It _shouldInstantiateModels = () => _subject.Configuration.ShouldEqual("Config");
        }

        #endregion
    }
}
