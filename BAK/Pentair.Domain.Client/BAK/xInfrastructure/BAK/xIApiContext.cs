﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Data.Entity.Core.Common.CommandTrees.ExpressionBuilder;
using System.Linq;
using Pentair.Domain.Client.Entity;
using Pentair.Domain.Client.Helpers.Entity;

namespace Pentair.Domain.Client.Infrastructure
{
    /* TODO: 2b removed 2017-12-13
    public interface IApiContext : ICommonDataContext
    {
        IDbSet<Association> Associations { get; } 
        IDbSet<Installation> Installations { get; }
        IDbSet<Connection> Connections { get; }
        IDbSet<UserInvitation> UserInvitations { get; }
        IDbSet<Event> Events { get; }
        IDbSet<EventType> EventTypes { get; }
        IDbSet<UserNotification> UserNotifications { get; }
        IDbSet<NotificationRecipient> NotificationRecipients { get; }
        IDbSet<NotificationMessage> NotificationMessages { get; }
        IDbSet<InstallationView> InstallationViews { get; }
    }
    */
}
