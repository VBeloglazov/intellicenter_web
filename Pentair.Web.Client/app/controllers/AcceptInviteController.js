﻿'use strict';
var PWC = PWC || {};
PWC.controller('AcceptInviteController', [
    '$scope', '$location', '$routeParams', '$route', 'PropertyService', 'Enumerations', 'AppService', 'AuthService',
    function ($scope, $location, $routeParams, $route, propertyService, enumerations, appService, authService) {
        var inviteInfo = $route.current.locals.inviteInfo.data;
        var autoAcceptData = {
            Email: authService.authentication.userName,
            Password: "TokenProvided",
            ConfirmPassword: "TokenProvided",
            InviteId: $routeParams.inviteId
        };


        $scope.InvalidInvite = false;
        $scope.EulaRefused = false;
        $scope.ShowEula = false;
        $scope.ShowExistingAcct = false;
        $scope.passwordType = "password";   // existing account password type
        $scope.$parent.Layout = "layout-base";

        if (inviteInfo.error) {
            $scope.InvalidInvite = true;
            $scope.InvaidInviteMessage = inviteInfo.error;
        }

        //AutoAccept if logged in as invited user
        if (authService.authentication.isAuth && inviteInfo.Email === authService.authentication.userName) {
            appService.PostData("Api/Account/AcceptInvite", autoAcceptData).then(function (inviteResponse) {
                propertyService.FetchInstallations();
                propertyService.SelectedInstallation = inviteResponse.data.InstallationId;
                propertyService.Connect();
            }).catch(function () {
                authService.logOut();
            });
        } else {
            authService.resetAuthData(true);
        }

        $scope.Property = propertyService;
        $scope.InviteError = false;
        $scope.ExistingInviteError = false;
        $scope.ExistingInviteLoginError = false;

        $scope.attachedcompleted = false;

        $scope.AcceptInviteData = {
            email: inviteInfo.Email,
            password: "",
            confirmPassword: "",
            propertyName: inviteInfo.PropertyName,
            inviteId: $routeParams.inviteId
        }

        $scope.togglePasswordType = function () {            
            
            $scope.passwordType = ($scope.passwordType === 'password') ? 'text' : 'password';
        }

        $scope.getEyeIcon = function () {
            var icon = ($scope.passwordType === 'password') ? 'icon-theeye-off' : 'icon-theeye';
            return icon;
        }

        $scope.LoginData = {
            email: "",
            password: "",
            inviteId: $routeParams.inviteId
        };

        $scope.ToggleEula = function () {
            $scope.ShowEula = !$scope.ShowEula;
        }

        $scope.ToggleExistingAcct = function () {
            $scope.ShowExistingAcct = !$scope.ShowExistingAcct;
        };

        $scope.RefusedEula = function () {
            $scope.ShowEula = false;
            $scope.EulaRefused = true;
        }


        $scope.ConnectNow = function (LD) {

            var data = {
                userName: LD.email,
                password: LD.password,
                rememberMe: false
            };
            
              authService.login(data).then(function () {
                    $location.path('/installations');
                    $scope.passwordType = 'password';   // reset password type
                },
                    function (err) {
                        $scope.message = err.error_description;
                        $scope.loginData.password = "";
                    });
                //clear url params
                $location.search({});            
        };

        $scope.AttachToExistingAcct = function () {
            var loginData = {
                userName: $scope.LoginData.email,
                password: $scope.LoginData.password,
                rememberMe: false
            };
            authService.login(loginData).then(function (loginResponse) {
                var data = {
                    Email: $scope.LoginData.email,
                    Password: $scope.LoginData.password,
                    ConfirmPassword: $scope.LoginData.password,
                    InviteId: $scope.LoginData.inviteId
                };
                appService.PostData("Api/Account/AcceptInvite", data).then(function (inviteResponse) {
                    console.log("ATTACH TO EXISTING ACC [1]:  AcceptInvite succesfull...");
                    
                    propertyService.SelectedInstallation = inviteResponse.data.InstallationId;
                    $scope.attachedcompleted = true;
                    $scope.ExistingInviteLoginError = false;        
                    /*
                    propertyService.FetchInstallations();
                    propertyService.SelectedInstallation = inviteResponse.data.InstallationId;                    
                    propertyService.Connect();                    
                    */
                }).catch(function () {
                    $scope.ExistingInviteError = true;
                    console.log("ATTACH TO EXISTING ACC:  error ");
                });
            }).catch(function () {
                console.log("ATTACH TO EXISTING ACC:  login error ");
                $scope.ExistingInviteLoginError = true;
            });
        }

        $scope.AcceptInvite = function () {
            $scope.ShowEula = false;

            var data = {
                Email: $scope.AcceptInviteData.email,
                Password: $scope.AcceptInviteData.password,
                ConfirmPassword: $scope.AcceptInviteData.confirmPassword,
                InviteId: $scope.AcceptInviteData.inviteId
            };


            appService.PostData("Api/Account/AcceptInvite", data).then(function (inviteResponse) {
                authService.authentication.lastUserName = inviteResponse.data.UserName;
                authService.authentication.accountCreated = true;

                $location.path('/login');
            }).catch(function () {
                $scope.InviteError = true;
            });
        }

        $scope.validatePassword = function () {
            var re = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*).{6,}$");
            var result = false;
            if ($scope.AcceptInviteData.password) {
                result = $scope.AcceptInviteData.password.match(re) ? true : false;
            }
            return result;
        }

        $scope.verifyConfirmationPassword = function () {
            return !$scope.AcceptInvitationForm.confirmPassword.$invalid &&
                    $scope.validatePassword() &&
                   $scope.AcceptInviteData.password === $scope.AcceptInviteData.confirmPassword;
        }

        $scope.validateForm = function () {
            return $scope.validatePassword() &&
                $scope.verifyConfirmationPassword();
        }

        $scope.checkSizePassword = function (item) {
            if (item.length >= 6)
                return true;
            else
                return false;
        }

        $scope.checkUppercasePassword = function (item) {
            var status = false;
            if (item.length <= 0)
                return status;

            for (var i = 0; i < item.length; i++) {
                if (item[i] >= 'A' && item[i] <= 'Z')
                    status = true;
            }
            return status;
        }

        $scope.checkMatchPassword = function (item1, item2) {
            if ((item1 === item2) && (item1.length >= 6))
                return true;
            else
                return false;
        }

    }
]);