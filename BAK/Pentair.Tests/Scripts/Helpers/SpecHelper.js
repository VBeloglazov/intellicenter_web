﻿var SpecHelper = (function () {
    var body1 = {
        OBJNAM: "BPool",
        SNAME: "Pool",
        TEMP: "85^F",
        LOTMP: "80^F",
        HITMP: "110^F",
        OBJTYP: "BODY",
        LISTORD: "1",
        SUBTYP: "POOL",
        SHARE: "BSpa",
        FILTER: 'CPoolCircuit',
        HEATER: 'H1000',
        HEATERS: {
            VIRTUAL: {},
            GENERIC: {
                HEATER: {},
                HCOMBO: {}
            },
            SECONDARY: {
                HEATER: {},
                HCOMBO: {}
            }
        }
    }
    var body2 = {
        OBJNAM: "BSpa",
        SNAME: "Spa",
        TEMP: "100^F",
        LOTMP: "90^F",
        HITMP: "110^F",
        SHARE: "BSpa",
        LISTORD: "2",
        OBJTYP: "BODY",
        SUBTYP: "SPA",
        FILTER: 'CSpaCircuit',
        HEATER: 'H1001',
        HEATERS: {
            VIRTUAL: heater3,
            GENERIC: {
                HEATER: heater1,
                HCOMBO: hcombo1
            },
            SECONDARY: {
                HEATER: heater2,
                HCOMBO: hcombo2
            }
        }
    }

    var body3 = {
        OBJNAM: "BPool2",
        SNAME: "Separate Pool",
        TEMP: "85^F",
        LOTMP: "80^F",
        HITMP: "110^F",
        OBJTYP: "BODY",
        SHARE: "",
        LISTORD: "3",
        SUBTYP: "POOL",
        FILTER: 'CPoolCircuit',
        HEATER: 'H1000',
        HEATERS: {
            VIRTUAL: {},
            GENERIC: {
                HEATER: {},
                HCOMBO: {}
            },
            SECONDARY: {
                HEATER: {},
                HCOMBO: {}
            }
        }
    }
    var body4 = {
        OBJNAM: "BSpa2",
        SNAME: "Separate Spa",
        TEMP: "100^F",
        LOTMP: "90^F",
        HITMP: "110^F",
        LISTORD: "4",
        OBJTYP: "BODY",
        SUBTYP: "SPA",
        FILTER: 'CSpaCircuit',
        HEATER: 'H1001',
        HEATERS: {
            VIRTUAL: heater3,
            GENERIC: {
                HEATER: heater1,
                HCOMBO: hcombo1
            },
            SECONDARY: {
                HEATER: heater2,
                HCOMBO: hcombo2
            }
        }
    }

    var light1 = {
        OBJNAM: "CSpaLight",
        SNAME: "Spa Light",
        SUBTYP: "LIGHT",
        SHOMNU: "l",
        OBJTYP: "CIRCUIT",
        STATUS: "ON",
        LISTORD: "1"
    }
    var light2 = {
        OBJNAM: "CPoolLight",
        SNAME: "Pool Light",
        SUBTYP: "LIGHT",
        OBJTYP: "CIRCUIT",
        SHOMNU: "le",
        STATUS: "OFF",
        LISTORD: "2"
    }
    var light3 = {
        OBJNAM: "Intelli",
        SNAME: "Pool Light",
        OBJTYP: "CIRCUIT",
        SUBTYP: "INTELLI",
        SHOMNU: "le",
        STATUS: "OFF",
        LISTORD: "2"
    }
    var light4 = {
        OBJNAM: "Magic2",
        SNAME: "Pool Light",
        OBJTYP: "CIRCUIT",
        SUBTYP: "MAGIC2",
        SHOMNU: "le",
        STATUS: "OFF",
        LISTORD: "2"
    }
    var light5 = {
        OBJNAM: "Magic1",
        OBJTYP: "CIRCUIT",
        SNAME: "Pool Light",
        SUBTYP: "MAGIC1",
        SHOMNU: "le",
        STATUS: "OFF",
        LISTORD: "2"
    }
    var feature1 = {
        OBJNAM: "CSlide",
        SNAME: "Pool Slide",
        SUBTYP: "GENERIC",
        SHOMNU: "f",
        OBJTYP: "CIRCUIT",
        STATUS: "OFF",
        LISTORD: "1"
    }
    var feature2 = {
        OBJNAM: "CWaterFall",
        SNAME: "Waterfall",
        OBJTYP: "CIRCUIT",
        SUBTYP: "GENERIC",
        SHOMNU: "f",
        STATUS: "ON",
        LISTORD: "2"
    }
    var setting1 = {
        OBJNAM: "CFreezeMode",
        SNAME: "Freeze Mode",
        OBJTYP: "CIRCUIT",
        SUBTYP: "FRZ",
        SHOMNU: "s",
        STATUS: "OFF",
        LISTORD: "1"
    }
    var setting2 = {
        OBJNAM: "CSpaRemote",
        SNAME: "Spa Remote",
        SUBTYP: "GENERIC",
        SHOMNU: "s",
        OBJTYP: "CIRCUIT",
        STATUS: "OFF",
        LISTORD: "2"
    }

    var bodyFilter1 = {
        OBJNAM: "CPoolCircuit",
        SNAME: "Pool Circuit",
        SUBTYP: "BODY",
        SHOMNU: "s",
        OBJTYP: "CIRCUIT",
        STATUS: "OFF",
        LISTORD: "1"
    }
    var bodyFilter2 = {
        OBJNAM: "CSpaCircuit",
        SNAME: "Spa Circuit",
        SUBTYP: "BODY",
        SHOMNU: "s",
        OBJTYP: "CIRCUIT",
        STATUS: "OFF",
        LISTORD: "1"
    }

    var heater1 = {
        OBJNAM: "H1000",
        SNAME: "Body heater",
        OBJTYP: "HEATER",
        SUBTYP: "GENERIC",
        STATUS: "OFF"
    }

    var heater2 = {
        OBJNAM: "H1001",
        OBJTYP: "HEATER",
        SNAME: "Spa heater",
        SUBTYP: "SOLAR",
        STATUS: "OFF"
    }
    var heater3 = {
        OBJNAM: "H1002",
        SNAME: "HCOMBO heater",
        OBJTYP: "HEATER",
        SUBTYP: "HCOMBO",
        STATUS: "OFF"
    }
    var hcombo1 = {
        OBJNAM: 'G1000',
        USE: 'H1000',
        OBJTYP: "HCOMBO",
        ACT: 'PRIOR',
        HEATER: 'H1002',
        LISTORD: 2
    }

    var hcombo2 = {
        OBJNAM: 'G1001',
        OBJTYP: "HCOMBO",
        USE: 'H1001',
        ACT: 'PRIOR',
        HEATER: 'H1002',
        LISTORD: 1
    }
    var sensor1 = { OBJNAM: "A1000", OBJTYP: "SENSE", SUBTYP: "AIR", PROBE: "96.234" };

    var installations = [
                 {
                     "InstallationId": "35E24665-BD62-48B4-8C12-5E26E57E5A6D",
                     "PoolName": "hewitts pool 1",
                     "OnLine": false,
                     "AccessToken": 'TESTtoken1'
                 },
                 {
                     "InstallationId": "c077a376-e4f5-40b9-a495-ace172297e1c",
                     "PoolName": "hewitts pool 4",
                     "OnLine": true,
                     "AccessToken": 'TESTtoken2'
                 },
                 {
                     "InstallationId": "f467e58f-307f-43f4-a9dc-df7bc3828a88",
                     "PoolName": "hewitts pool 2",
                     "OnLine": false,
                     "AccessToken": 'TESTtoken3'
                 }
    ];

    var schedule1 = {
        OBJNAM: 'S1000',
        OBJTYP: 'SCHED',
        SNAME: 'Schedule 1',
        LISTORD: '1',
        CIRCUIT: 'B1000',
        SINGLE: 'ON',
        DAY: 'M',
        START: 'ABSTIM',
        TIME: '11:23',
        STOP: 'ABSTIM',
        TIMOUT: '13:23',
        GROUP: '1',
        HEATER: 'H1000',
        HITMP: '100',
        LOTMP: '80',
        SMTSRT: 'OFF'
    }

    var schedule2 = {
        OBJNAM: 'S1001',
        OBJTYP: 'SCHED',
        SNAME: 'Schedule 2',
        LISTORD: '2',
        CIRCUIT: 'light1',
        HNAME: '',
        DAY: 'M1',
        START: 'ABSTIM',
        TIME: '12:23',
        STOP: 'ABSTIM',
        TIMOUT: '14:23',
        GROUP: '1',
        STATUS: 'ON',
        HEATER: '',
        HITMP: '',
        LOTMP: '',
        SMTSRT: 'ON'
    }

    var heatModes = {
        GENERIC: [
            {
                name: 'Heat Off',
                value: 'NONE'
            },
            {
                name: 'Heater',
                value: 'GENERIC'
            }
        ],
        HTPMP: [
           {
               name: 'Heat Off',
               value: 'NONE'
           },
           {
               name: 'Heater',
               value: 'GENERIC'
           },
           {
               name: 'Heat Pump Pref.',
               value: 'PREF'
           },
           {
               name: 'Heat Pump Only',
               value: 'ONLY'
           }
        ],
        SOLAR: [
           {
               name: 'Heat Off',
               value: 'NONE'
           },
           {
               name: 'Heater',
               value: 'GENERIC'
           },
           {
               name: 'Solar Preferred',
               value: 'PREF'
           },
           {
               name: 'Solar Only',
               value: 'ONLY'
           }
        ]
    };
    this.mocks = {
        Bodies: [body1, body2],
        Lights: [light1, light2],
        Features: [feature1, feature2],
        Settings: [setting1, setting2],
        Circuits: [light1, light2, feature1, feature2, setting1, setting2],
        ColorLights: [light3, light4, light5],
        AllTypes: [body1, body2, light1, light2, light3, light4, light5, feature1, feature2, setting1, setting2, bodyFilter1, bodyFilter2, heater1, heater2, heater3, sensor1, hcombo1, hcombo2],
        BodyFilterCircuits: [bodyFilter1, bodyFilter2],
        Heaters: [heater1, heater2, heater3],
        HeaterCombos: [hcombo1, hcombo2],
        Sensors: [sensor1],
        Installations: installations,
        Schedules: [ schedule1, schedule2 ],
        HeatModes: heatModes
    };

    this.JSONdata = {
        Circuits: '{"objectList":[{"objnam":"C1355","params":{"OBJTYP":"CIRCUIT","STATIC":null,"SUBTYP":"BODY","HNAME":null,"SNAME":"Pool Circuit","RECENT":"0","FREQ":"0","CUSTOM":"0","LISTORD":"1","SHOMNU":"s","ALPHA":"0","SERVICE":null,"STATUS":"ON","TIMOUT":"0","SOURCE":null,"USE":null,"RLY":null,"FREEZE":null,"MANUAL":null,"TIME":"0","ACT":null,"UNITS":null,"SINDEX":"0","INVALID":null,"COUNT":"0"}},{"objnam":"C1356","params":{"OBJTYP":"CIRCUIT","STATIC":null,"SUBTYP":"BODY","HNAME":null,"SNAME":"Spa Circuit","RECENT":"0","FREQ":"0","CUSTOM":"0","LISTORD":"1","SHOMNU":"s","ALPHA":"0","SERVICE":null,"STATUS":"OFF","TIMOUT":"0","SOURCE":null,"USE":null,"RLY":null,"FREEZE":null,"MANUAL":null,"TIME":"0","ACT":null,"UNITS":null,"SINDEX":"0","INVALID":null,"COUNT":"0"}},{"objnam":"C5433","params":{"OBJTYP":"CIRCUIT","STATIC":null,"SUBTYP":"INTELLI","HNAME":null,"SNAME":"Night Spa","RECENT":"0","FREQ":"0","CUSTOM":"0","LISTORD":"1","SHOMNU":"le","ALPHA":"0","SERVICE":null,"STATUS":"ON","TIMOUT":"0","SOURCE":null,"USE":null,"RLY":null,"FREEZE":null,"MANUAL":null,"TIME":"0","ACT":null,"UNITS":null,"SINDEX":"0","INVALID":null,"COUNT":"0"}},{"objnam":"C5435","params":{"OBJTYP":"CIRCUIT","STATIC":null,"SUBTYP":"MAGIC2","HNAME":null,"SNAME":"Night Laminar","RECENT":"0","FREQ":"0","CUSTOM":"0","LISTORD":"2","SHOMNU":"le","ALPHA":"0","SERVICE":null,"STATUS":"OFF","TIMOUT":"0","SOURCE":null,"USE":null,"RLY":null,"FREEZE":null,"MANUAL":null,"TIME":"0","ACT":null,"UNITS":null,"SINDEX":"0","INVALID":null,"COUNT":"0"}},{"objnam":"C5436","params":{"OBJTYP":"CIRCUIT","STATIC":null,"SUBTYP":"GENERIC","HNAME":null,"SNAME":"Water Fall","RECENT":"0","FREQ":"0","CUSTOM":"0","LISTORD":"1","SHOMNU":"f","ALPHA":"0","SERVICE":null,"STATUS":"OFF","TIMOUT":"0","SOURCE":null,"USE":null,"RLY":null,"FREEZE":null,"MANUAL":null,"TIME":"0","ACT":null,"UNITS":null,"SINDEX":"0","INVALID":null,"COUNT":"0"}},{"objnam":"C5437","params":{"OBJTYP":"CIRCUIT","STATIC":null,"SUBTYP":"FRZ","HNAME":null,"SNAME":"Freeze Mode","RECENT":"0","FREQ":"0","CUSTOM":"0","LISTORD":"1","SHOMNU":"s","ALPHA":"0","SERVICE":null,"STATUS":"ON","TIMOUT":"0","SOURCE":null,"USE":null,"RLY":null,"FREEZE":null,"MANUAL":null,"TIME":"0","ACT":null,"UNITS":null,"SINDEX":"0","INVALID":null,"COUNT":"0"}}],"command":"NOTIFYLIST","messageID":"ID1"}',
        Bodies: '{"objectList":[{"objnam":"B1000","params":{"OBJREV":"0","OBJTYP":"BODY","STATIC":null,"SUBTYP":"POOL","LISTORD":"1","HNAME":null,"SNAME":"Pool","PRIM":"C1355","SEC":null,"SHARE":null,"PARENT":null,"FILTER":"C1355","PUMP":null,"MASTER":null,"HTSRC":null,"HEATER":"H1000","VOL":"0","STATUS":null,"TEMP":"75^F","LOTMP":"72^F","HITMP":"78^F","BADGE":"0"}},{"objnam":"B1001","params":{"OBJREV":"0","OBJTYP":"BODY","STATIC":null,"SUBTYP":"SPA","LISTORD":"2","HNAME":null,"SNAME":"Spa","PRIM":"C1356","SEC":null,"SHARE":null,"PARENT":null,"FILTER":"C1356","PUMP":null,"MASTER":null,"HTSRC":null,"HEATER":"H1001","VOL":"0","STATUS":null,"TEMP":"108^F","LOTMP":"100^F","HITMP":"109^F","BADGE":"0"}},' +
    '{"objnam":"B1002","params":{"OBJREV":"0","OBJTYP":"BODY","STATIC":null,"SUBTYP":"POOL","LISTORD":"1","HNAME":null,"SNAME":"Pool","PRIM":"C1355","SEC":null,"SHARE":"B1003","PARENT":null,"FILTER":"C1355","PUMP":null,"MASTER":null,"HTSRC":null,"HEATER":"H1000","VOL":"0","STATUS":null,"TEMP":"75^F","LOTMP":"72^F","HITMP":"78^F","BADGE":"0"}},{"objnam":"B1003","params":{"OBJREV":"0","OBJTYP":"BODY","STATIC":null,"SUBTYP":"SPA","LISTORD":"2","HNAME":null,"SNAME":"Spa","PRIM":"C1356","SEC":null,"SHARE":"B1002","PARENT":null,"FILTER":"C1356","PUMP":null,"MASTER":null,"HTSRC":null,"HEATER":"H1001","VOL":"0","STATUS":null,"TEMP":"108^F","LOTMP":"100^F","HITMP":"109^F","BADGE":"0"}}],"command":"NOTIFYLIST","messageID":"ID2"}',
        Heaters: '{"objectList":[{"objnam":"H1000","params":{"OBJREV":"0","OBJTYP":"HEATER","STATIC":"ON","LISTORD":"1","HNAME":null,"SNAME":"Body heater","RLY":null,"SUBTYP":"GENERIC","BODY":"B1000","SHARE":"B1001","COMUART":null,"PARENT":null,"COOL":null,"START":"0","STOP":"0","DLY":"0","STATUS":"OFF","PERMIT":"ON","TIMOUT":"20","READY":"TRUE"}},{"objnam":"H1001","params":{"OBJREV":"0","OBJTYP":"HEATER","STATIC":"ON","LISTORD":"1","HNAME":null,"SNAME":"Spa heater","RLY":null,"SUBTYP":"GENERIC","BODY":"B1001","SHARE":"B1001","COMUART":null,"PARENT":null,"COOL":null,"START":"0","STOP":"0","DLY":"0","STATUS":"OFF","PERMIT":"ON","TIMOUT":"20","READY":"TRUE"}}],"command":"NOTIFYLIST","messageID":"ID3"}',
        AirSensors: '{"objectList":[{"objnam":"A1000","params":{"OBJREV":"0","OBJTYP":"SENSE","SUBTYP":"AIR","LISTORD":"1","HNAME":null,"SNAME":null,"PRIM":null,"STATUS":"ERR","CALIB":"1.234","SOURCE":"95","PROBE":"96.234"}}],"command":"NOTIFYLIST","messageID":"ID5"}',
        Notification: '{"objectList":[{"objnam":"C5437","params":{"OBJTYP":"CIRCUIT","STATIC":null,"SUBTYP":"FRZ","HNAME":null,"SNAME":"Freeze Mode","RECENT":"0","FREQ":"0","CUSTOM":"0","LISTORD":"1","SHOMNU":"s","ALPHA":"0","SERVICE":null,"STATUS":"OFF","TIMOUT":"0","SOURCE":null,"USE":null,"RLY":null,"FREEZE":null,"MANUAL":null,"TIME":"0","ACT":null,"UNITS":null,"SINDEX":"0","INVALID":null,"COUNT":"0"}}],"command":"NOTIFYLIST","messageID":"ID9"}',
        PendingSubscriptions: '{"objectList":[],"command":"NOTIFYLIST","messageID":"ID4"}',
        UserList: '{"objectList":[{"objnam":"jhewitt@bhst.com","params":{"name":"jhewitt@bhst.com","level":"Guest","status":"PENDING"}}],"command":"USERLIST", "messageID":"ID8"}',
        DeleteUserResponse: '{"command":"USERLIST", "messageID":"ID10","messageDetail":"jhewitt@bhst.com"}',
        Schedules: '{"objectList":[{"objnam":"S1000","params":{"OBJNAM":"S1000","OBJTYP":"SCHED","LISTORD":"","CIRCUIT":"B1000","HNAME":null,"SNAME":null,"SINGLE":"ON","DAY":"T","START":"ABSTIM","TIME":"02:20","STOP":"ABSTIM","TIMOUT":"04:20","GROUP":"1","STATUS":"ON","HEATER":"H1000","HITMP":"72","LOTMP":"78"}}],"command":"NOTIFYLIST","messageID":"ID11"}',
        SecurityTokens: '{"objectList":[{"objnam":"U0000","params":{"OBJTYP":"PERMIT","OBJNAM":"U0000","SNAME":"GUEST"}},{"objnam":"UFFFE","params":{"OBJTYP":"PERMIT","OBJNAM":"UFFFE","SNAME":"ADMIN"}}],"command":"NOTIFYLIST","messageID":"ID12"}',
        NotificationRecipients: '{"data":[{"RecipientId":1,"InstallationId":1,"Name":"Test Guy","EmailAddress1":"test@email.com","EmailAddress2":"","Phone1":"312-555-2231","Phone2":"","CreateDateTime":"2015-11-10T12:48:15.857","UpdateDateTime":"2015-11-10T12:48:15.857"}]}',
        StatusMessageFlags: '{ "command": "SendQuery", "messageID": "10A86ef1-88f1-8091-7cdf1b", "response": "200", "answer": [ { "CIRCUIT": "fhlmv", "PUMP": "svioubpawfe", "ICHLOR": "dvifloce", "ICHEM": "acbdfgijklnpehomqrs", "HEATER": "hlwrosftbpg" } ] }',
        SystemInfo: '{"InstallationId":1,"PoolName":"shared pool","AccessToken":"UFFFE","SecurityName":null,"Address":null,"Location":null,"City":null,"State":null,"Zip":null,"Phone":null,"OwnerName":"dhoward@pathf.com","Email":null,"MAC":null,"TotalAdmins":1,"OnLine":true,"OwnerId":"00000000-0000-0000-0000-000000000000"}',
        Chemistry: '{"objectList":[{"objnam":"I2572","params":{"OBJTYP":"CHEM","SUBTYP":"ICHLOR","SNAME":"Pool Chlorinator","BODY":"B1002","SHARE":"B1003","LISTORD":null}},{"objnam":"I2573","params":{"OBJTYP":"CHEM","SUBTYP":"ICHEM","SNAME":"Pool Chemistry","BODY":"B1002","SHARE":"B1003","LISTORD":null}}],"command":"NOTIFYLIST","messageID":"245bf5d0-8562-41af-a45c-78b4bbb92c11","response":"200","timeNow":0,"timeSince":0}',
        PageSubscriptions: {
            SystemInformation: '{"objectList":[{"objnam":"UFFFE","params":{"ENABLE":"OFF","OBJTYP":"PERMIT","SHOMNU":"abcdefhlmnprstu"}},{"objnam":"_C105","params":{"OBJTYP":"SYSTIM","SOURCE":"LOCAL"}},{"objnam":"_5451","params":{"ADDRESS":"Address","CITY":"Chicago","STATE":"IL","ZIP":"60654","NAME":"John Doe","TIMZON":"-5","MODE":"ENGLISH","OBJTYP":"SYSTEM"}},{"objnam":"_C10C","params":{"CLK24A":"AMPM","LOCX":"41.895485","LOCY":"87.634880","OBJTYP":"SYSTIM","OFFSET":"-5","SNAME":"Chicago, IL","SRIS":"06:15:00","SSET":"17:38:00","TIMZON":"-5","ZIP":"60654"}},{"objnam":"_CFEA","params":{"MANOVR":"OFF","OBJTYP":"FEATR"}}],"command":"NOTIFYLIST","messageID":"7e54e8bf-a02d-434f-a781-6a15cc7f558a","response":"200","timeNow":0,"timeSince":0}',
            Chemistry:'{"objectList":[{"objnam":"I2572","params":{"SALT":"2450"}},{"objnam":"I2573","params":{"ALK":"70","CALC":"250","CYACID":"1200","ORPSET":"740","ORPTNK":"10","ORPVAL":"780","PHSET":"8.5","PHTNK":"70","PHVAL":"8.94","SALT":"2450"}}],"command":"NOTIFYLIST","messageID":"245bf5d0-8562-41af-a45c-78b4bbb92c11","response":"200","timeNow":0,"timeSince":0}'
        }
    }

    this.createDirective = function (compile, template, scope) {
        var elm = angular.element(template);
        compile(elm)(scope);
        scope.$apply();
        return elm;
    }

    this.messageQueue = {
        CircuitsRequest: { ID1: 'AllCircuitsRequest' },
        BodiesRequest: { ID2: 'AllBodiesRequest' },
        HeaterRequest: { ID3: 'AllHeatersRequest' },
        SubscriptionRequest: { ID4: 'PendingSubScriptionRequest' },
        AirSensorRequest: { ID5: 'AirSensorRequest' },
        DelayRequest: { ID6: 'DelayRequest' },
        InviteUserMessage: { ID7: 'InviteUserMessage' },
        UserListMessage: { ID8: 'UserListMessage' },
        EditUserMessage: { ID9: 'EditUserMessage' },
        DeleteUserMessage: { ID10: 'ManageUserResult' }
    }

    this.ResetBody2Props = function () {
        body2.HEATERS = {
            VIRTUAL: heater3,
            GENERIC: {
                HEATER: heater1,
                HCOMBO: hcombo1
            },
            SECONDARY: {
                HEATER: heater2,
                HCOMBO: hcombo2
            }
        }
    }

    return this;
})();

