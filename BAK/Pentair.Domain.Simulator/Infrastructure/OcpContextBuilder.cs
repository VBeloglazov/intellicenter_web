﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pentair.Domain.Simulator.Infrastructure
{
    public class OcpContextBuilder
    {
        public object GetNewContext()
        {
            return new OcpContext();
        }
    }
}