﻿using System;
using System.Collections.Generic;
using Machine.Fakes;
using Machine.Specifications;
using Newtonsoft.Json;
using Pentair.Desktop.Simulator;
using Pentair.Desktop.Simulator.BusinessLogic;
using Pentair.Domain.Client.Entity;
using Pentair.Domain.Client.Helpers;
using Pentair.Domain.Client.Helpers.Commands;
using Pentair.Domain.Client.Infrastructure;

namespace Pentair.Tests.Simulator
{
    [Subject("ParseParameters")]
    public class WhenIAskForAllObjects
    {
        #region Json Commands

        private const string GetBodyObjects =
            @"{'command':'GetParamList','objectList':[{'objnam':'ALL','keys':['OBJNAM : OBJREV : OBJTYP : STATIC : SUBTYP : RECENT : FREQ : CUSTOM : LISTORD : HNAME : SNAME : READY : PRIM : SEC : COMUART : BODY : SHARE : PARENT : COOL : START : PHTYP : ORPTYP : PHMOD : ORPMOD? OBJTYP=BODY']}]}";

        private const string GetSpaObjects =
            @"{'command':'GetParamList','objectList':[{'objnam':'ALL','keys':['START : STOP : DLY : PERMIT : FILTER : PUMP : MASTER : HTSRC : HEATER : VOL : SHOMNU : ALPHA : TIMZON : FEATR : USAGE : CYCTIM : DAY : GROUP : HITMP : LOTMP : SETTMP : PRIMFLO : FLOWDLY : PHPRIOR : MODE ? OBJTYP=BODY & SUBTYP=SPA']}]}";

        private const string GetMoreObjects1 =
            @"{'command':'GetParamList','objectList':[{'objnam':'ALL','keys':['HTMODE : SERVICE : PASSWRD : ENABLE : SENSE : PRIMTIM : PRESS : SYSTIM : BAKFLO : BAKTIM : RNSTIM : VACFLO : VACTIM : CIRCUIT : RPM : FLOW : PWR : PRIOR : SPEED : BOOST : IPADY : SUBNET : PHFED1 : CHLOR ? OBJTYP=BODY & SUBTYP=SPA']}]}";

        private const string GetMoreObjects2 =
            @"{'command':'GetParamList','objectList':[{'objnam':'ALL','keys':['DFGAT : DNSSERV : WAPNET : WAPPW : PANID : LEGACY : STATUS : TEMP : EMAIL : MIN : MAX : CHEM : SYSTEM : PHONE : URL : LOGO : BADGE : TIMOUT : SOURCE : ZIP : LOCX : LOCY : DLSTIM : USE : CLK24A : STATUS1 ? OBJTYP=BODY & SUBTYP=SPA']}]}";

        private const string GetMoreObjects3 =
            @"{'command':'GetParamList','objectList':[{'objnam':'ALL','keys':['OFFSET : ACCEL : ABSTIM : SRIS : SSET : MENU : RESET : PARTY : ON : OFF : RLY : FREEAE : VER : SERNUM : MNFDAT : NORMAL : DIMMER : MANUAL : TIME : BTT : RADIO : AIR : PORT : CNFG : PANEL ? OBJTYP=BODY & SUBTYP=SPA']}]}";

        private const string GetMoreObjects4 =
            @"{'command':'GetParamList','objectList':[{'objnam':'ALL','keys':['RUNON : ASSIGN : DFLT : ACT : POSIT : VALVE : MODULE : COUNT : UNITS : SUPER : SALT : DEVICE : SALTLO : VERYLO : CURRENT : VOLT : PHSET : ORPSET : PHTNK : ORPTNK : CALC : CYACIT : ALK : SINDEX ? OBJTYP=BODY & SUBTYP=SPA']}]}";

        private const string GetMoreObjects5 =
            @"{'command':'GetParamList','objectList':[{'objnam':'ALL','keys':['PHFED : ORPFED : PHVAL : ORPVAL : PHTIM : ORPTIM : PHVOL : ORPVOL : SALT1 : NOFLO : PHHI : PHLO : ORPHI : ORPLO : PHCHK : ORPCHK : PROE : LOCKOUT : PHLIM : ORPLIM : INVALID : COMLNK : DEVICE1 : CALIB : ALARM ? OBJTYP=BODY & SUBTYP=SPA']}]}";

        private const string GetBodyTemperature =
            @"{'command':'GetParamList','objectList':[{'objnam':'ALL','keys':['CALIB, SOURCE, PROBE']}]}";

        private const string GetCompound =
            @"{'command':'GetParamList','objectList':[{'objnam':'ALL','keys':['OBJNAM : LISTORD : SHOMNU ? (OBJTYP=BODY & SUBTYP=SPA) & STATUS=OFF']}]}";

        private const string SetPoolOff =
            @"{'command':'SetParamList','objectList':[{'objnam':'C1355','params':{'STATUS' : 'OFF'}}]}";

        private const string SetMore1 =
            @"{'command':'SetParamList','objectList':[{'objnam':'C1355','params':{'OBJREV' : '2', 'STATIC' : 'ON', 'RECENT' : '1', 'FREQ' : '2', 'CUSTOM' : '3', 'LISTORD' : '4', 'HNAME' : 'ONE', 'SNAME' : 'TWO', 'READY' : 'NO', 'PRIM' : 'C1234', 'SEC' : 'C1235'}}]}";

        private const string SetMore2 =
            @"{'command':'SetParamList','objectList':[{'objnam':'C1355','params':{'COMUART' : 'ONE', 'BODY' : 'Pool', 'SHARE' : 'C1234', 'PARENT' : 'B1000', 'COOL' : 'H1001', 'START' : '232', 'STOP' : '233', 'DLY' : '1', 'PERMIT' : 'NO', 'FILTER' : 'C1235', 'PUMP' : 'C1236'}}]}";

        private const string SetMore3 =
            @"{'command':'SetParamList','objectList':[{'objnam':'C1355','params':{'MASTER' : 'C2001', 'HTSRC' : 'C3342', 'HEATER' : 'H1222', 'VOL' : '1', 'SHOMNU' : 's', 'ALPHA' : '232', 'TIMZON' : '233', 'FEATR' : 'C2331', 'USAGE' : 'NO', 'CYCTIM' : '1', 'DAY' : 'M1'}}]}";

        private const string SetMore4 =
            @"{'command':'SetParamList','objectList':[{'objnam':'C1355','params':{'GROUP' : '3', 'HITMP' : '75.2', 'LOTMP' : '68.5', 'SETTMP' : '78.5', 'PRIMFLO' : 'C1234', 'HTMODE' : 'CONCURENT', 'SERVICE' : 'HELP', 'PASSWRD' : 'ThisIsALongPassword', 'ENABLE' : 'NO', 'SENSE' : '1', 'PRIMTIM' : '365'}}]}";

        private const string SetMore5 =
            @"{'command':'SetParamList','objectList':[{'objnam':'C1355','params':{'PRESS' : '3', 'SYSTIM' : '7', 'BAKFLO' : '23', 'BAKTIM' : '3', 'RNSTIM' : '3', 'VACFLO' : '97', 'VACTIM' : '2', 'CIRCUIT' : 'C1000', 'RPM' : '3000', 'FLOW' : '1', 'PWR' : '365'}}]}";

        private const string SetMore6 =
            @"{'command':'SetParamList','objectList':[{'objnam':'C1355','params':{'PRIOR' : 'C3000', 'SPEED' : '5600', 'BOOST' : 'C3221', 'IPADY' : 'HELP', 'SUBNET' : '/23', 'DFGAT' : '192.168.20.0', 'DNSSERV' : '196.126.21.0', 'WAPNET' : 'LOCAL', 'WAPPW' : 'ThisIsALongPassword', 'PANID' : '1', 'LEGACY' : '365'}}]}";

        private const string SetMore7 =
            @"{'command':'SetParamList','objectList':[{'objnam':'C1355','params':{'STATUS' : 'BAD', 'TEMP' : 'NOW', 'EMAIL' : 'jhewitt@bhst.com', 'MIN' : '54', 'MAX' : '123', 'CHEM' : 'C4435', 'SYSTEM' : 'SYSTEM', 'PHONE' : '(222)333-444', 'URL' : 'WWW.PENTAIR.COM', 'LOGO' : '1', 'BADGE' : '365'}}]}";

        private const string SetMore8 =
            @"{'command':'SetParamList','objectList':[{'objnam':'C1355','params':{'TIMOUT' : 'BAD', 'SOURCE' : 'NOW', 'ZIP' : '12345', 'LOCX' : '54.5', 'LOCKY' : '123.55', 'DLSTIM' : 'C4435', 'USE' : 'SYSTEM', 'CLK24A' : 'YES', 'OFFSET' : '1', 'ACCEL' : 'FAST', 'ABSTIM' : '7/23/2014 12:00:00.00'}}]}";

        private const string SetMore10 =
            @"{'command':'SetParamList','objectList':[{'objnam':'C1355','params':{'SRIS' : '1/1/2013 01:01:01.01', 'SSET' : '07/23/2014 12:32:00.00', 'MENU' : 'ONE', 'RESET' : 'NEVER', 'PARTY' : 'NOW', 'ON' : 'ON', 'OFF' : 'OFF', 'RLY' : 'C21234', 'FREEZE' : '23', 'VER' : '1.5', 'SERNUM' : '1234567890 Ser.'}}]}";

        private const string SetMore11 =
            @"{'command':'SetParamList','objectList':[{'objnam':'C1355','params':{'RUNON' : 'FOREVER', 'ASSIGN' : 'B1002', 'DFLT' : '1', 'ACT' : 'NEVER', 'POSIT' : '1', 'VALVE' : 'C1222', 'MODULE' : 'A', 'COUNT' : '2', 'UNITS' : 'F', 'SUPER' : '5', 'SALT' : 'HI'}}]}";

        private const string SetMore12 =
            @"{'command':'SetParamList','objectList':[{'objnam':'C1355','params':{'MNFDAT' : '1/1/2013 01:01:01.01', 'NORMAL' : '1', 'DIMMER' : '1', 'MANUAL' : 'NEVER', 'TIME' : 'NOW', 'BAT' : '2', 'RADIO' : '2', 'AIR' : '2', 'PORT' : '23', 'CNFG' : 'ThisCanBeALongLine', 'PANEL' : 'Main'}}]}";

        private const string SetMore13 =
            @"{'command':'SetParamList','objectList':[{'objnam':'C1355','params':{'DEVICE' : 'ONE', 'SALTLO' : 'YES', 'VERYLOW' : 'YES', 'CURRENT' : 'A', 'VOLT' : 'LOW', 'PHSET' : '2', 'ORPSET' : '2', 'PHTNK' : '2', 'ORPTNK' : '23', 'CALC' : '3', 'CYACID' : '3', 'CHLOR' : 'HELP'}}]}";

        private const string SetMore14 =
            @"{'command':'SetParamList','objectList':[{'objnam':'C1355','params':{'ALK' : '34', 'SINDEX' : '5', 'PHFED' : '3', 'ORPFED' : '5', 'PHVAL' : '5', 'ORPVAL' : '2', 'PHTIM' : '2', 'ORPTIM' : '2', 'PHVOL' : '23', 'ORPVOL' : '3', 'SALT1' : '3', 'PHFED1' : 'HELP'}}]}";

        private const string SetMore15 =
            @"{'command':'SetParamList','objectList':[{'objnam':'C1355','params':{'NOFLO' : 'C3324', 'PHHI' : '5', 'PHLO' : '3', 'ORPHI' : '5', 'ORPLO' : '5', 'PHCHK' : '2', 'ORPCHK' : '2', 'PROB' : '2', 'LOCKOUT' : '23', 'PHLIM' : '3', 'ORPLIM' : '3', 'MODE' : 'MANUAL'}}]}";

        private const string SetMore16 =
            @"{'command':'SetParamList','objectList':[{'objnam':'C1355','params':{'INVALID' : 'C3324', 'COMLNK' : '5', 'DEVICE1' : '3', 'CALIB' : '5', 'ALARM' : '5', 'PHTYP' : '2', 'ORPTYP' : '2', 'PHMOD' : '2', 'ORPMOD' : '23', 'FLOWDLY' : '3', 'PHPRIOR' : '3', 'STATUS1' : 'BAD'}}]}";

        private const string GetPoolStatus =
            @"{'command':'GetParamList','objectList':[{'objnam':'C1355','keys':['STATUS']}]}";

        private const string ScheduleSpaNotifications =
            @"{'command':'RequestParamList','objectList':[{'objnam':'C1356','keys':['STATUS', 'HITMP', 'LOTMP']}]}";

        #endregion

        #region Database Records

        private static readonly ISimulatorContext SimulatorFakeContext = new FakeContext()
        {
            Properties =
            {
                new Property()
                {
                    Configuration = PropertyConfigurations.SharedPoolAndSpa,
                    LastChange = 0,
                    LastNotify = 0,
                    LocalPropertyId = 1,
                    Name = "test",
                    PropertyId = 1,
                    ProtocolVersion = "0",
                    RemoteHost = "//LocalHost"
                }
            },
            ControlObjects =
            {
                new ControlObject()
                {
                    INSTALLATION = 1,
                    OBJNAM = "C1355",
                    Key = StaticData.ObjectType,
                    Value = StaticData.BodyType,
                },
                new ControlObject()
                {
                    INSTALLATION = 1,
                    OBJNAM = "C1355",
                    Key = StaticData.SubType,
                    Value = "POOL",
                },
                new ControlObject()
                {
                    INSTALLATION = 1,
                    OBJNAM = "C1355",
                    Key = StaticData.SystemName,
                    Value = "Pool Circuit",
                },
                new ControlObject()
                {
                    INSTALLATION = 1,
                    OBJNAM = "C1355",
                    Key = StaticData.Status,
                    Value = StaticData.On,
                },
                new ControlObject()
                {
                    INSTALLATION = 1,
                    OBJNAM = "C1356",
                    Key = StaticData.ObjectType,
                    Value = StaticData.BodyType,
                },
                new ControlObject()
                {
                    INSTALLATION = 1,
                    OBJNAM = "C1356",
                    Key = StaticData.SubType,
                    Value = "SPA",
                },
                new ControlObject()
                {
                    INSTALLATION = 1,
                    OBJNAM = "C1356",
                    Key = StaticData.SystemName,
                    Value = "Spa Circuit",
                },
                new ControlObject()
                {
                    INSTALLATION = 1,
                    OBJNAM = "C1356",
                    Key = StaticData.Status,
                    Value = StaticData.On,
                },
                new ControlObject()
                {
                    INSTALLATION = 1,
                    OBJNAM = "C1356",
                    Key = StaticData.HiTemp,
                    Value = "30",
                },
                new ControlObject()
                {
                    INSTALLATION = 1,
                    OBJNAM = "C1356",
                    Key = StaticData.LowTemp,
                    Value = "20",
                },
                new ControlObject()
                {
                    INSTALLATION = 1,
                    OBJNAM = "C5433",
                    Key = StaticData.ObjectType,
                    Value = StaticData.CircuitType,
                },
                new ControlObject()
                {
                    INSTALLATION = 1,
                    OBJNAM = "C5433",
                    Key = StaticData.SubType,
                    Value = StaticData.IntellibrightSubtype,
                },
                new ControlObject()
                {
                    INSTALLATION = 1,
                    OBJNAM = "C5433",
                    Key = StaticData.SystemName,
                    Value = "Night Spa",
                },
                new ControlObject()
                {
                    INSTALLATION = 1,
                    OBJNAM = "C5435",
                    Key = StaticData.ObjectType,
                    Value = StaticData.CircuitType,
                },
                new ControlObject()
                {
                    INSTALLATION = 1,
                    OBJNAM = "C5435",
                    Key = StaticData.SubType,
                    Value = "MAGIC2",
                },
                new ControlObject()
                {
                    INSTALLATION = 1,
                    OBJNAM = "C5435",
                    Key = StaticData.SystemName,
                    Value = "Night Laminar",
                },
                new ControlObject()
                {
                    INSTALLATION = 1,
                    OBJNAM = "C5436",
                    Key = StaticData.ObjectType,
                    Value = StaticData.CircuitType,
                },
                new ControlObject()
                {
                    INSTALLATION = 1,
                    OBJNAM = "C5436",
                    Key = StaticData.SubType,
                    Value = StaticData.GenericSubtype,
                },
                new ControlObject()
                {
                    INSTALLATION = 1,
                    OBJNAM = "C5436",
                    Key = StaticData.SystemName,
                    Value = "Water Fall",
                },
                new ControlObject()
                {
                    INSTALLATION = 1,
                    OBJNAM = "C5437",
                    Key = StaticData.ObjectType,
                    Value = StaticData.CircuitType,
                },
                new ControlObject()
                {
                    INSTALLATION = 1,
                    OBJNAM = "C5437",
                    Key = StaticData.SubType,
                    Value = StaticData.LightSubtype,
                },
                new ControlObject()
                {
                    INSTALLATION = 1,
                    OBJNAM = "C5437",
                    Key = StaticData.SystemName,
                    Value = "Light",
                }
            }
        };
        #endregion

        #region Tests
        //public class WhenTheClientRequestsAllBodyObjectsThenSendTheData : WithFakes
        //{
        //    private static BusinessLogic _subject;
        //    private static List<ParamObject> _response;

        //    private Establish context = () =>
        //    {
        //        _subject = new BusinessLogic(SimulatorFakeContext as FakeContext, 1);
        //    };

        //    private Because of = () =>
        //    {
        //        _response = _subject.GetParamList(JsonConvert.DeserializeObject<GetParamList>(GetBodyObjects));
        //    };

        //    private It should_be_all_body_objects = () => _response.Count.ShouldEqual(2);
        //}

        //public class WhenTheClientRequestsAllSpaObjectsThenSendTheData : WithFakes
        //{
        //    private static BusinessLogic _subject;
        //    private static List<ParamObject> _response;

        //    private Establish context = () =>
        //    {
        //        _subject = new BusinessLogic(SimulatorFakeContext as FakeContext, 1);
        //    };

        //    private Because of = () =>
        //    {
        //        _response = _subject.GetParamList(JsonConvert.DeserializeObject<GetParamList>(GetSpaObjects));
        //    };

        //    private It should_be_spa_objects = () => _response.Count.ShouldEqual(1);
        //}

        // TODO Test for query manager
        //public class WhenTheClientRequestsAllMore1ThenSendTheData : WithFakes
        //{
        //    private static BusinessLogic _subject;
        //    private static List<ParamObject> _response;

        //    private Establish context = () =>
        //    {
        //        _subject = new BusinessLogic(SimulatorFakeContext as FakeContext, 1);
        //    };

        //    private Because of = () =>
        //    {
        //        _response = _subject.GetParamList(JsonConvert.DeserializeObject<GetParamList>(GetMoreObjects1));
        //    };

        //    private It should_be_more1_objects = () => _response.Count.ShouldEqual(1);
        //}

        //public class WhenTheClientRequestsAllMore2ThenSendTheData : WithFakes
        //{
        //    private static BusinessLogic _subject;
        //    private static List<ParamObject> _response;

        //    private Establish context = () =>
        //    {
        //        _subject = new BusinessLogic(SimulatorFakeContext as FakeContext, 1);
        //    };

        //    private Because of = () =>
        //    {
        //        _response = _subject.GetParamList(JsonConvert.DeserializeObject<GetParamList>(GetMoreObjects2));
        //    };

        //    private It should_be_more2_objects = () => _response.Count.ShouldEqual(1);
        //}

        // TODO Test for query manager
        //public class WhenTheClientRequestsAllMore3ThenSendTheData : WithFakes
        //{
        //    private static BusinessLogic _subject;
        //    private static List<ParamObject> _response;

        //    private Establish context = () =>
        //    {
        //        _subject = new BusinessLogic(SimulatorFakeContext as FakeContext, 1);
        //    };

        //    private Because of = () =>
        //    {
        //        _response = _subject.GetParamList(JsonConvert.DeserializeObject<GetParamList>(GetMoreObjects3));
        //    };

        //    private It should_be_more3_objects = () => _response.Count.ShouldEqual(1);
        //}

        //public class WhenTheClientRequestsAllMore4ThenSendTheData : WithFakes
        //{
        //    private static BusinessLogic _subject;
        //    private static List<ParamObject> _response;

        //    private Establish context = () =>
        //    {
        //        _subject = new BusinessLogic(SimulatorFakeContext as FakeContext, 1);
        //    };

        //    private Because of = () =>
        //    {
        //        _response = _subject.GetParamList(JsonConvert.DeserializeObject<GetParamList>(GetMoreObjects4));
        //    };

        //    private It should_be_more4_objects = () => _response.Count.ShouldEqual(1);
        //}

        //public class WhenTheClientRequestsAllMore5ThenSendTheData : WithFakes
        //{
        //    private static BusinessLogic _subject;
        //    private static List<ParamObject> _response;

        //    private Establish context = () =>
        //    {
        //        _subject = new BusinessLogic(SimulatorFakeContext as FakeContext, 1);
        //    };

        //    private Because of = () =>
        //    {
        //        _response = _subject.GetParamList(JsonConvert.DeserializeObject<GetParamList>(GetMoreObjects5));
        //    };

        //    private It should_be_more5_objects = () => _response.Count.ShouldEqual(1);
        //}

        //public class WhenTheClientRequestsBodyObjectsThatAreOnThenSendTheData : WithFakes
        //{
        //    private static BusinessLogic _subject;
        //    private static List<ParamObject> _response;

        //    private Establish context = () =>
        //    {
        //        _subject = new BusinessLogic(SimulatorFakeContext as FakeContext, 1);
        //    };

        //    private Because of = () =>
        //    {
        //        _response = _subject.GetParamList(JsonConvert.DeserializeObject<GetParamList>(GetBodyTemperature));
        //    };

        //    private It should_be_body_temperatures = () => _response.Count.ShouldEqual(6);
        //}

        //public class WhenTheClientRequestsBodyTemperaturesThenSendTheData : WithFakes
        //{
        //    private static BusinessLogic _subject;
        //    private static List<ParamObject> _response;

        //    private Establish context = () =>
        //    {
        //        _subject = new BusinessLogic(SimulatorFakeContext as FakeContext, 1);
        //    };

        //    private Because of = () =>
        //    {
        //        _response = _subject.GetParamList(JsonConvert.DeserializeObject<GetParamList>(GetCompound));
        //    };

        //    private It should_be_all_temperatures = () => _response[0].parameters.Count.ShouldEqual(3);
        //}

        //public class WhenTheClientRequestsToTurnThePoolOnMakeSureItIsON : WithFakes
        //{
        //    private static BusinessLogic _subject;
        //    private static List<ParamObject> _status;

        //    private Establish context = () =>
        //    {
        //        _subject = new BusinessLogic(SimulatorFakeContext as FakeContext, 1);
        //    };

        //    private Because of = () =>
        //    {
        //        _subject.SetParamList(JsonConvert.DeserializeObject<SetParamList>(SetPoolOff));
        //        _status = _subject.GetParamList(JsonConvert.DeserializeObject<GetParamList>(GetPoolStatus));
        //    };

        //    private It pool_should_be_off = () => _status[0].parameters.ContainsValue("OFF").ShouldBeTrue();
        //}

        //public class WhenTheClientRequestsToSetMore1 : WithFakes
        //{
        //    private static BusinessLogic _subject;
        //    private static List<ParamObject> _status;

        //    private Establish context = () =>
        //    {
        //        _subject = new BusinessLogic(SimulatorFakeContext as FakeContext, 1);
        //    };

        //    private Because of = () =>
        //    {
        //        _subject.SetParamList(JsonConvert.DeserializeObject<SetParamList>(SetMore1));
        //        _status = _subject.GetParamList(JsonConvert.DeserializeObject<GetParamList>(GetPoolStatus));
        //    };

        //    private It more_1_should_be = () => _status[0].parameters.Count.ShouldEqual(1);
        //}

        //public class WhenTheClientRequestsToSetMore2 : WithFakes
        //{
        //    private static BusinessLogic _subject;
        //    private static List<ParamObject> _status;

        //    private Establish context = () =>
        //    {
        //        _subject = new BusinessLogic(SimulatorFakeContext as FakeContext, 1);
        //    };

        //    private Because of = () =>
        //    {
        //        _subject.SetParamList(JsonConvert.DeserializeObject<SetParamList>(SetMore2));
        //        _status = _subject.GetParamList(JsonConvert.DeserializeObject<GetParamList>(GetPoolStatus));
        //    };

        //    private It more_2_should_be = () => _status[0].parameters.Count.ShouldEqual(1);
        //}

        //public class WhenTheClientRequestsToSetMore3 : WithFakes
        //{
        //    private static BusinessLogic _subject;
        //    private static List<ParamObject> _status;

        //    private Establish context = () =>
        //    {
        //        _subject = new BusinessLogic(SimulatorFakeContext as FakeContext, 1);
        //    };

        //    private Because of = () =>
        //    {
        //        _subject.SetParamList(JsonConvert.DeserializeObject<SetParamList>(SetMore3));
        //        _status = _subject.GetParamList(JsonConvert.DeserializeObject<GetParamList>(GetPoolStatus));
        //    };

        //    private It more_3_should_be = () => _status[0].parameters.Count.ShouldEqual(1);
        //}

        //public class WhenTheClientRequestsToSetMore4: WithFakes
        //{
        //    private static BusinessLogic _subject;
        //    private static List<ParamObject> _status;

        //    private Establish context = () =>
        //    {
        //        _subject = new BusinessLogic(SimulatorFakeContext as FakeContext, 1);
        //    };

        //    private Because of = () =>
        //    {
        //        _subject.SetParamList(JsonConvert.DeserializeObject<SetParamList>(SetMore4));
        //        _status = _subject.GetParamList(JsonConvert.DeserializeObject<GetParamList>(GetPoolStatus));
        //    };

        //    private It more_4_should_be = () => _status[0].parameters.Count.ShouldEqual(1);
        //}

        //public class WhenTheClientRequestsToSetMore5 : WithFakes
        //{
        //    private static BusinessLogic _subject;
        //    private static List<ParamObject> _status;

        //    private Establish context = () =>
        //    {
        //        _subject = new BusinessLogic(SimulatorFakeContext as FakeContext, 1);
        //    };

        //    private Because of = () =>
        //    {
        //        _subject.SetParamList(JsonConvert.DeserializeObject<SetParamList>(SetMore5));
        //        _status = _subject.GetParamList(JsonConvert.DeserializeObject<GetParamList>(GetPoolStatus));
        //    };

        //    private It more_5_should_be = () => _status[0].parameters.Count.ShouldEqual(1);
        //}

        //public class WhenTheClientRequestsToSetMore6 : WithFakes
        //{
        //    private static BusinessLogic _subject;
        //    private static List<ParamObject> _status;

        //    private Establish context = () =>
        //    {
        //        _subject = new BusinessLogic(SimulatorFakeContext as FakeContext, 1);
        //    };

        //    private Because of = () =>
        //    {
        //        _subject.SetParamList(JsonConvert.DeserializeObject<SetParamList>(SetMore6));
        //        _status = _subject.GetParamList(JsonConvert.DeserializeObject<GetParamList>(GetPoolStatus));
        //    };

        //    private It more_6_should_be = () => _status[0].parameters.Count.ShouldEqual(1);
        //}

        //public class WhenTheClientRequestsToSetMore7 : WithFakes
        //{
        //    private static BusinessLogic _subject;
        //    private static List<ParamObject> _status;

        //    private Establish context = () =>
        //    {
        //        _subject = new BusinessLogic(SimulatorFakeContext as FakeContext, 1);
        //    };

        //    private Because of = () =>
        //    {
        //        _subject.SetParamList(JsonConvert.DeserializeObject<SetParamList>(SetMore7));
        //        _status = _subject.GetParamList(JsonConvert.DeserializeObject<GetParamList>(GetPoolStatus));
        //    };

        //    private It more_7_should_be = () => _status[0].parameters.Count.ShouldEqual(1);
        //}

        //public class WhenTheClientRequestsToSetMore8 : WithFakes
        //{
        //    private static BusinessLogic _subject;
        //    private static List<ParamObject> _status;

        //    private Establish context = () =>
        //    {
        //        _subject = new BusinessLogic(SimulatorFakeContext as FakeContext, 1);
        //    };

        //    private Because of = () =>
        //    {
        //        _subject.SetParamList(JsonConvert.DeserializeObject<SetParamList>(SetMore8));
        //        _status = _subject.GetParamList(JsonConvert.DeserializeObject<GetParamList>(GetPoolStatus));
        //    };

        //    private It more_8_should_be = () => _status[0].parameters.Count.ShouldEqual(1);
        //}

        //public class WhenTheClientRequestsToSetMore10 : WithFakes
        //{
        //    private static BusinessLogic _subject;
        //    private static List<ParamObject> _status;

        //    private Establish context = () =>
        //    {
        //        _subject = new BusinessLogic(SimulatorFakeContext as FakeContext, 1);
        //    };

        //    private Because of = () =>
        //    {
        //        _subject.SetParamList(JsonConvert.DeserializeObject<SetParamList>(SetMore10));
        //        _status = _subject.GetParamList(JsonConvert.DeserializeObject<GetParamList>(GetPoolStatus));
        //    };

        //    private It more_10_should_be = () => _status[0].parameters.Count.ShouldEqual(1);
        //}

        //public class WhenTheClientRequestsToSetMore11 : WithFakes
        //{
        //    private static BusinessLogic _subject;
        //    private static List<ParamObject> _status;

        //    private Establish context = () =>
        //    {
        //        _subject = new BusinessLogic(SimulatorFakeContext as FakeContext, 1);
        //    };

        //    private Because of = () =>
        //    {
        //        _subject.SetParamList(JsonConvert.DeserializeObject<SetParamList>(SetMore11));
        //        _status = _subject.GetParamList(JsonConvert.DeserializeObject<GetParamList>(GetPoolStatus));
        //    };

        //    private It more_11_should_be = () => _status[0].parameters.Count.ShouldEqual(1);
        //}

        //public class WhenTheClientRequestsToSetMore12 : WithFakes
        //{
        //    private static BusinessLogic _subject;
        //    private static List<ParamObject> _status;

        //    private Establish context = () =>
        //    {
        //        _subject = new BusinessLogic(SimulatorFakeContext as FakeContext, 1);
        //    };

        //    private Because of = () =>
        //    {
        //        _subject.SetParamList(JsonConvert.DeserializeObject<SetParamList>(SetMore12));
        //        _status = _subject.GetParamList(JsonConvert.DeserializeObject<GetParamList>(GetPoolStatus));
        //    };

        //    private It more_12_should_be = () => _status[0].parameters.Count.ShouldEqual(1);
        //}

        //public class WhenTheClientRequestsToSetMore13 : WithFakes
        //{
        //    private static BusinessLogic _subject;
        //    private static List<ParamObject> _status;

        //    private Establish context = () =>
        //    {
        //        _subject = new BusinessLogic(SimulatorFakeContext as FakeContext, 1);
        //    };

        //    private Because of = () =>
        //    {
        //        _subject.SetParamList(JsonConvert.DeserializeObject<SetParamList>(SetMore13));
        //        _status = _subject.GetParamList(JsonConvert.DeserializeObject<GetParamList>(GetPoolStatus));
        //    };

        //    private It more_13_should_be = () => _status[0].parameters.Count.ShouldEqual(1);
        //}

        //public class WhenTheClientRequestsToSetMore14 : WithFakes
        //{
        //    private static BusinessLogic _subject;
        //    private static List<ParamObject> _status;

        //    private Establish context = () =>
        //    {
        //        _subject = new BusinessLogic(SimulatorFakeContext as FakeContext, 1);
        //    };

        //    private Because of = () =>
        //    {
        //        _subject.SetParamList(JsonConvert.DeserializeObject<SetParamList>(SetMore14));
        //        _status = _subject.GetParamList(JsonConvert.DeserializeObject<GetParamList>(GetPoolStatus));
        //    };

        //    private It more_14_should_be = () => _status[0].parameters.Count.ShouldEqual(1);
        //}

        //public class WhenTheClientRequestsToSetMore15 : WithFakes
        //{
        //    private static BusinessLogic _subject;
        //    private static List<ParamObject> _status;

        //    private Establish context = () =>
        //    {
        //        _subject = new BusinessLogic(SimulatorFakeContext as FakeContext, 1);
        //    };

        //    private Because of = () =>
        //    {
        //        _subject.SetParamList(JsonConvert.DeserializeObject<SetParamList>(SetMore15));
        //        _status = _subject.GetParamList(JsonConvert.DeserializeObject<GetParamList>(GetPoolStatus));
        //    };

        //    private It more_15_should_be = () => _status[0].parameters.Count.ShouldEqual(1);
        //}

        //public class WhenTheClientRequestsToSetMore16 : WithFakes
        //{
        //    private static BusinessLogic _subject;
        //    private static List<ParamObject> _status;

        //    private Establish context = () =>
        //    {
        //        _subject = new BusinessLogic(SimulatorFakeContext as FakeContext, 1);
        //    };

        //    private Because of = () =>
        //    {
        //        _subject.SetParamList(JsonConvert.DeserializeObject<SetParamList>(SetMore16));
        //        _status = _subject.GetParamList(JsonConvert.DeserializeObject<GetParamList>(GetPoolStatus));
        //    };

        //    private It more_16_should_be = () => _status[0].parameters.Count.ShouldEqual(1);
        //}

        // TODO Test for query handling
        //public class WhenTheClientRequestsToTurnTheSpaOffCheckConfigurations : WithFakes
        //{
        //    private static BusinessLogic _subject;
        //    private static WriteParamList _status;
        //    private static readonly int _property = 1;
        //    private static readonly Guid _client = new Guid();
        //    private Establish context = () =>
        //    {
        //        _subject = new BusinessLogic(SimulatorFakeContext as FakeContext, 1);
        //    };

        //    private Because of = () =>
        //    {
        //        _subject.RequestParamList(JsonConvert.DeserializeObject<RequestParamList>(ScheduleSpaNotifications), _client, _property);
        //        _status = _subject.GetConfigChanges(0);
        //    };

        //    private It scheduleNotiications = () => _status.objectList[0].parameters.Count.ShouldEqual(2);
        //}

        //public class WhenTheClearParamListIsCalledNotificationsAreCanceledForClient : WithFakes
        //{
        //    private static BusinessLogic _subject;
        //    private static NotifyList _status;
        //    private static NotifyList _clearStatus; 
        //    private const int Config = 1;
        //    private static readonly int _property = 1;
        //    private static readonly Guid _client = new Guid();

        //    private Establish context = () =>
        //    {
        //        _subject = new BusinessLogic(SimulatorFakeContext as FakeContext, Config);
        //    };

        //    private Because of = () =>
        //    {
        //        _subject.RequestParamList(JsonConvert.DeserializeObject<RequestParamList>(ScheduleSpaNotifications),
        //            _client, _property);
        //        _status = _subject.GetNotifications();
        //        _subject.ClearParamList(_client, _property);
        //        _clearStatus = _subject.GetNotifications();
        //    };

        //    private It clearNotifications = () =>
        //        _clearStatus.ShouldNotBeNull();
        //}

        //public class WhenTheClientCreatesNewObjects : WithFakes
        //{
        //    private static BusinessLogic _subject;
        //    private static string _result;

        //    private Establish context = () =>
        //    {
        //        _subject = new BusinessLogic(SimulatorFakeContext as FakeContext, 1);
        //    };

        //    private Because of = () =>
        //    {
        //        _result = _subject.CreateObject(new KeyObjectType() {objtyp="SCHED", keys = new List<string>()});
        //    };

        //    private It addsNewObject = () => _result.ShouldEqual("S1000");
        //}
        #endregion
    }
}
