USE [PathfinderIntellicenter]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[PostalCodes]
(
	[CountryName] nvarchar(100) NOT NULL,
	[PostalCode] nvarchar(20) NOT NULL,
	[CityName] nvarchar(100) NOT NULL,
	[ProvinceAbbr] nvarchar(20) NOT NULL,
	[TimeZone] nvarchar(20) NOT NULL,
	[UTC] decimal(4,1) NULL,
	[DST] nvarchar(1) NULL,
	[Latitude] decimal(12,6) NULL,
	[Longitude] decimal(12,6) NULL,
 CONSTRAINT [PK_PostalCodes] PRIMARY KEY CLUSTERED 
(
	[CountryName] ASC,
	[PostalCode] ASC,
	[CityName] ASC,
	[ProvinceAbbr] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] 
GO

CREATE NONCLUSTERED INDEX [IX_PostalCodes_Zip] ON [dbo].[PostalCodes]
(
	[PostalCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]


/* Create Linked Server for ODBC
	Microsoft OLD DB Provider for ODBC Drivers:	SQLITEZIPCODES
	Data Source:								SQLiteZipCodesDS

	----To load files > 250MB, use below after creating the table
	sqlcmd -S 23.253.87.25 -d TESTDB -U <username> -P <passsword> -i C:\Wayne\PostalCodesData.sql -o C:\Wayne\PostalCodesOut.txt

*/

--select * 
--into #ZipCodes
--from openquery(SQLiteZipCodes, 'select * from postalCodes')
	
--insert into dbo.PostalCodes
--select 
--	CountryName, PostalCode, CityName, ProvinceAbbr, TimeZone
--	, CAST(CAST(UTC AS NVARCHAR) AS DECIMAL(12,6)) AS UTC
--	, DST
--	, CAST(CAST(Latitude AS NVARCHAR) AS DECIMAL(12,6)) AS Latitude
--	, CAST(CAST(Longitude AS NVARCHAR) AS DECIMAL(12,6)) AS Longitude
--from #ZipCodes
