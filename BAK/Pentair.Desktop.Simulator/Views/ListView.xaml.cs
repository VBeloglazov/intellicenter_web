﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;
using Newtonsoft.Json;
using Pentair.Desktop.Simulator.Models;
using Pentair.Domain.Client.Entity;
using Pentair.Domain.Client.Helpers;
using Pentair.Domain.Client.Helpers.Commands;

namespace Pentair.Desktop.Simulator.Views
{
    /// <summary>
    /// Interaction logic for Dashboard.xaml
    /// </summary>
    public partial class ListView : UserControl
    {
        internal event DataElementChanged OnDataElementChanged;
        internal delegate void DataElementChanged(EditData e);

        internal enum KeyFilter
        {
            AlphaNumeric,
            Alpha,
            Numeric
        }

        private TextBox _textBox;
        private bool _editFlag = false;

        public ListView()
        {
            InitializeComponent();
        }

        private void DgPreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            var cell = sender as DataGridCell;
            if (null != cell && !cell.IsEditing && !cell.IsReadOnly)
            {
                if (!cell.IsFocused)
                {
                    cell.Focus();
                }
                var dg = FindVisualParent<DataGrid>(cell);
                if (null != dg)
                {
                    if (DataGridSelectionUnit.FullRow != dg.SelectionUnit)
                    {
                        if (!cell.IsSelected)
                        {
                            cell.IsSelected = true;
                        }
                    }
                    else
                    {
                        var row = FindVisualParent<DataGridRow>(cell);
                        if (null != row && !row.IsSelected)
                        {
                            row.IsSelected = true;
                        }
                    }
                }
            }
        }

        private void DgOnDataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            Dispatcher.BeginInvoke(DispatcherPriority.Render, new Action(InitializeColumns));
        }

        private void InitializeColumns()
        {

        }

        private void DgOnBeginEdit(object sender, MouseButtonEventArgs e)
        {
            var control = sender as DataGrid;
            TabControl parent = null;
            if (null != control)
            {
                parent = Helpers.GetListView(control);
                if (null != parent)
                {
                    var grid = parent.SelectedContent is KeyValuePair<string, DataModel> ? (KeyValuePair<string, DataModel>) parent.SelectedContent : new KeyValuePair<string, DataModel>();
                    var view = control.SelectedItem as DataRowView;
                    var obj = null == view ? "" : view.Row.ItemArray[0].ToString();
                    switch (grid.Key)
                    {
                        case StaticData.BodyType:
                            break;
                        case StaticData.CircuitType:
                            var ce = new CircuitEditor(new EditModel(obj, StaticData.CircuitType, grid.Value.PropertyId));
                            ce.ShowDialog();
                            break;
                        case StaticData.StatusMessageType:
                            var me = new EditModel(obj, StaticData.StatusMessageType, grid.Value.PropertyId);
                            var se = new MessageEditor(me);
                            se.ShowDialog();
                            if (me.Changes)
                            {
                                grid.Value.Sockets.SendMessage(SendStatus(me));
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        private string SendStatus(
            EditModel data)
        {
            var ItemsDeleted = new List<string>();
            var ItemsCreated = new List<string>();
            var ItemsChanged = new List<ParamObject>();
            if (data.DeleteFlag)
            {
                ItemsDeleted.Add(data.objnam);
            }
            else 
            {
                if (data.CreateFlag)
                {
                    ItemsCreated.Add(data.objnam);
                }

                ItemsChanged.Add(
                    new ParamObject(
                        objNam: data.objnam,
                        parameters: new Dictionary<string, object>()
                        {
                            { StaticData.ObjectType, data.Object.OBJTYP },
                            { StaticData.SystemName, data.Object.SNAME },
                            { StaticData.Parent, data.Object.PARENT },
                            { StaticData.SIndex, data.Object.SINDEX },
                            { StaticData.Mode, data.Object.MODE },
                            { StaticData.ShowMenu, data.Object.SHOMNU }
                        }
                    )
                );
            }
            
            var msg = new WriteStatusMessage(
                messageID: String.Empty,
                response: ResponseString.OK,
                timeNow: Globals.CurrentTime().ToString(),
                timeSince: Globals.CurrentTime().ToString(),
                objectList: new List<ExtendedParamObject>()
                {
                    new ExtendedParamObject(
                        created: ItemsCreated,
                        changes: ItemsChanged,
                        deleted: ItemsDeleted
                    )
                }
            );

            return JsonConvert.SerializeObject(msg);
        
        } // SendStatus()

        List<ControlObject> GetObject(string objType, DataRowView view)
        {
            var obj = new List<ControlObject>();

            if (null != view)
            {
                for (var i = 0; i < view.Row.ItemArray.Count(); i++)
                {
                    if (view.DataView.Table.Columns[i].ColumnName != StaticData.ObjectName)
                    {
                        obj.Add(new ControlObject()
                        {
                            CreateDateTime = DateTime.Now,
                            Deleted = false,
                            OBJNAM = view.Row[StaticData.ObjectName].ToString(),
                            Key = view.DataView.Table.Columns[i].ColumnName,
                            Value = view.DataView.Table.Rows[0][i].ToString()
                        });
                    }
                }
            }
            else
            {
                obj.Add(new ControlObject()
                {
                    CreateDateTime = DateTime.Now,
                    Deleted = false,
                    OBJNAM = "",
                    Key = StaticData.ObjectType,
                    Value = objType
                });
            }

            return obj;
        }

        private void DgOnRowEditEnding(object sender, DataGridRowEditEndingEventArgs e)
        {
        }

        private void DgOnCellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {
            _textBox = e.EditingElement as TextBox;
            _editFlag = true;
            var item = e.Row.DataContext as ControlObject;
            if (null != _textBox)
            {
                if (null != OnDataElementChanged)
                {
                    OnDataElementChanged(new EditData(item.OBJNAM, _textBox.Text, e.Column));
                }
            }
        }

        public void OnDataValueChanged(object sender, PropertyChangedEventArgs e)
        {
            for (int r = 0; r < DgObjects.Items.Count; r++)
            {
                var obj = DgObjects.Items[r] as ControlObject;
                if (null != obj && obj.OBJNAM == e.PropertyName)
                {
                    if (!_editFlag)
                    {
                        DgObjects.Items.Refresh();
                    }
                    DgObjects.SelectedItem = DgObjects.Items[r];
                    _editFlag = false;
                }
            }
        }

        private static T FindVisualParent<T>(UIElement element) where T : UIElement
        {
            UIElement parent = element;
            while (parent != null)
            {
                T correctlyTyped = parent as T;
                if (correctlyTyped != null)
                {
                    return correctlyTyped;
                }

                parent = VisualTreeHelper.GetParent(parent) as UIElement;
            }
            return null;
        }
    }

    public class EditData
    {
        public DataGridColumn Column { get; private set; }
        public string Value { get; private set; }
        public string Key { get; private set; }
        internal EditData(string key, string value, DataGridColumn column)
        {
            Value = value;
            Column = column;
            Key = key;
        }
    }
}
