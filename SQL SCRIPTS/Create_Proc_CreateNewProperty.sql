-- =============================================================
--
-- Author:		Vladimir Beloglazov
-- Create date: 02/07/2018
--
-- Creates:
--
--		Table Type:			None
--
--		Stored Procedure:	PCreateNewProperty
--
-- 2018-03-16 VB: [Installations] redesign, accept same name for different properties.
--
-- =============================================================

USE PathfinderIntelliCenter
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (
	SELECT * FROM sys.objects
	WHERE object_id = OBJECT_ID(N'dbo.PCreateNewProperty')
	AND type in (N'P', N'PC')
)
DROP PROCEDURE dbo.PCreateNewProperty
GO

CREATE PROCEDURE dbo.PCreateNewProperty
	 @name			AS nvarchar(255)
	,@userId		AS uniqueidentifier
	,@newSecret		AS nvarchar(255)
	,@accessToken	AS nvarchar(max)

AS
BEGIN

	SET NOCOUNT OFF

	BEGIN TRY

		IF EXISTS (
			SELECT 1
			FROM dbo.Installations
			WHERE [Name] = @name AND OwnerUserId = @userId -- UK
		)
			BEGIN

				-- The user is already an owner of property having the same name,
				-- duplication is not allowed

				-- Result set
				SELECT 	 409				AS HttpStatusCode -- Conflict
						,-1					AS InstallationId
						,NULL				AS NewSecret

			END
		ELSE
			BEGIN

				BEGIN TRANSACTION

				-- Create new property and assign it to the user as an owner
				DECLARE @InstallationNewId TABLE (InstallationId int)
				INSERT INTO dbo.Installations (
					 [Name]
					,OwnerUserId
					,[Secret]
					,LastChange
					,LastHistory
				)
				OUTPUT INSERTED.InstallationId INTO @InstallationNewId
				VALUES(
					 @name							-- Name
					,@userId						-- OwnerUserId
					,@newSecret						-- Secret
					,0								-- LastChange
					,0								-- LastHistory
				)

				DECLARE @newInstallationId int
				SET @newInstallationId = (SELECT TOP 1 InstallationId FROM @InstallationNewId)

				-- Update existing Association
				UPDATE dbo.Associations
				SET AccessToken = @accessToken
				WHERE UserId = @userId
				AND InstallationId = @newInstallationId

				-- Create new Association
				INSERT INTO dbo.Associations (UserId, InstallationId, AccessToken)
				SELECT @userId, @newInstallationId, @accessToken
				WHERE NOT EXISTS (
					SELECT 1 FROM dbo.Associations
					WHERE UserId = @userId
					AND InstallationId = @newInstallationId
				)

				-- Result set
				SELECT 	 201				AS HttpStatusCode -- Created
						,InstallationId		AS InstallationId
						,[Secret]			AS NewSecret
				FROM dbo.Installations
				WHERE InstallationId = @newInstallationId

			END

	END TRY
	BEGIN CATCH

		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION

		DECLARE @err_message nvarchar(255)
		SET @err_message = ERROR_MESSAGE()
		RAISERROR (@err_message, 11, 1) -- severity 11-19 will cause returning exception to caller

	END CATCH

	IF @@TRANCOUNT > 0
		COMMIT TRANSACTION

END
GO
