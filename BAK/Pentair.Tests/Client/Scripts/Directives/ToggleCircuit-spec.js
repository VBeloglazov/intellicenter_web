﻿describe('Toggle Circuit', function () {
    var $scope, $compile;

    var baseTemplate = '<toggle-circuit toggle-onclick="ToggleCircuit(obj,value)" target-circuit="circuit" /></toggle-circuit>';
    var disabledTemplate = '<toggle-circuit toggle-onclick="ToggleCircuit(obj,value)"disabled="isDisabled" target-circuit="circuit"></toggle-circuit>';

    beforeEach(module('PentairWebClient'));

    beforeEach(inject(function ($rootScope, _$compile_) {
        // Get an isolated scope
        $scope = $rootScope.$new();
        $compile = _$compile_;
        $scope.circuit = SpecHelper.mocks.Circuits[0];
        $scope.ToggleCircuit = jasmine.createSpy('ToggleCircuit');
    }));

    describe('when state is null', function () {
        beforeEach(function () {
            $scope.circuit.STATUS = null;
        });
        it('changes model to ON when clicked', function () {
            $scope.ToggleCircuit = jasmine.createSpy();
            var elm = SpecHelper.createDirective($compile, baseTemplate, $scope);

            elm.triggerHandler('click');
            expect($scope.ToggleCircuit).toHaveBeenCalledWith($scope.circuit, 'ON');
        });
    });

    describe('when state is ON', function () {
        beforeEach(function () {
            $scope.circuit.STATUS = "ON";
        });

        it('changes model to OFF when clicked', function () {
            var elm = SpecHelper.createDirective($compile, baseTemplate, $scope);
            elm.triggerHandler('click');
            expect($scope.ToggleCircuit).toHaveBeenCalledWith($scope.circuit, 'OFF');
        });
    });

    describe('when state is OFF', function () {
        beforeEach(function () {
            $scope.circuit.STATUS = "OFF";
        });

        it('changes model to ON when clicked', function () {
            var elm = SpecHelper.createDirective($compile, baseTemplate, $scope);
            elm.triggerHandler('click');
            expect($scope.ToggleCircuit).toHaveBeenCalledWith($scope.circuit, 'ON');
        });
    });

    describe('when model changes', function () {
        var elm;

        beforeEach(function () {
            $scope.circuit.STATUS = "OFF";
            elm = SpecHelper.createDirective($compile, baseTemplate, $scope);
        });

        it('shout toggle the switch ON when model is updated', function () {
            var switchCtrl = $(elm[0].firstChild);
            expect(switchCtrl.hasClass("off")).toEqual(true);
            expect($scope.circuit.STATUS).toEqual("OFF");

            $scope.$apply(function () {
                $scope.circuit.STATUS = "ON";
            });

            expect(switchCtrl.hasClass("on")).toEqual(true);
        });

        describe('when circuit is in Delay Mode', function () {
            it('should toggle the switch ON when the status is DLYOFF ', function () {
                var switchCtrl = $(elm[0].firstChild);
                expect(switchCtrl.hasClass("off")).toEqual(true);
                expect($scope.circuit.STATUS).toEqual("OFF");

                $scope.$apply(function () {
                    $scope.circuit.STATUS = "DLYOFF";
                });
                expect(switchCtrl.hasClass("on")).toEqual(true);

            });

            it('should toggle the switch OFF when the status is DLYON ', function () {
                var switchCtrl = $(elm[0].firstChild);
                $scope.$apply(function () {
                    $scope.circuit.STATUS = "ON";
                });
                expect(switchCtrl.hasClass("on")).toEqual(true);
                expect($scope.circuit.STATUS).toEqual("ON");

                $scope.$apply(function () {
                    $scope.circuit.STATUS = "DLYON";
                });
                expect(switchCtrl.hasClass("on")).toEqual(false);

            });
            it('should toggle the switch OFF when the status is null ', function () {
                var switchCtrl = $(elm[0].firstChild);
                $scope.$apply(function () {
                    $scope.circuit.STATUS = "";
                });
                $scope.$apply(function () {
                    $scope.circuit.STATUS = "DLYON";
                });
                expect(switchCtrl.hasClass("on")).toEqual(false);

            });
        });
    });


    describe('when toggle is disabled', function () {
        it('model does not change on click', function () {
            $scope.circuit.STATUS = "OFF";
            $scope.isDisabled = true;
            var elm = SpecHelper.createDirective($compile, disabledTemplate, $scope);
            elm.triggerHandler('click');
            expect($scope.circuit.STATUS).toEqual("OFF");
        });
    });


});