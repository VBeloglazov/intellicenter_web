﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pentair.Domain.Simulator.Entity
{
    public class ValveGroup
    {
        [Key]
        public int ID { get; set; }
        public string Name { get; set; }
        public UInt32 LastRevision { get; set; }
        public int Position { get; set; }
        public int PositionPriority { get; set; }

        // Foreign Keys
        public int TypeID { get; set; }
        public int ParentValveID { get; set; }
        public int CurrentCircuitID { get; set; }
        public int? SystemConfigID { get; set; }
    }
}