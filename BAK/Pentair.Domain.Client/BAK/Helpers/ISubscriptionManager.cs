﻿using System;
using System.Collections.Generic;

namespace Pentair.Domain.Client.Helpers
{
    interface ISubscriptionManager
    {
        /// <summary>
        /// Add the specified settings to the list of items this client is
        /// currently monitoring
        /// </summary>
        /// <param name="keys">List of objnam, param pairs</param>
        /// <param name="client">client uid</param>
        /// <param name="property">property uid</param>
        void Add(List<KeyValuePair<string, string>> keys, Guid client, Guid property);

        /// <summary>
        /// Remove the specified settings from the list of items this client is
        /// currently monitoring
        /// </summary>
        /// <param name="keys">List of objnam, param pairs</param>
        /// <param name="client">client uid</param>
        /// <param name="property">property uid</param>
        void Remove(List<KeyValuePair<string, string>> keys, Guid client, Guid property);

        /// <summary>
        /// Remove all subscriptions for the supplied client
        /// </summary>
        /// <param name="client">client uid to remove subscriptions for</param>
        void RemoveAll(Guid client);

        /// <summary>
        /// Remove all subscriptions for the supplied client
        /// </summary>
        /// <param name="client">client uid to remove subscriptions for</param>
        /// <param name="property">property uid to remove subscriptions for</param>
        void RemoveAll(Guid client, Guid property);

        /// <summary>
        /// Get a list of subscriptions at the supplied property for a client
        /// </summary>
        /// <param name="client">client uid</param>
        /// <param name="property">property uid</param>
        /// <returns></returns>
        List<KeyValuePair<string, string>> ClientSubscriptions(Guid client, Guid property);
    }
}
