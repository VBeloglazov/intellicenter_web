﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using Pentair.Domain.Client.Entity;
using Pentair.Domain.Client.Helpers.Entity;

namespace Pentair.Domain.Client.Infrastructure
{
    // TODO 2b deleted 2017-12-09
    /*
    public interface ISimulatorContext : ICommonDataContext, IDisposable
    {
        IDbSet<Property> Properties { get; }
        IDbSet<ObjectParameterType> ObjectParameterTypes { get; }
        IDbSet<ObjectView> ObjectViews { get; } 
    }
    */
}
