USE [PathfinderIntellicenter]
GO

/****** Object:  Table [dbo].[ControlObjectsHistory]    Script Date: 2/13/2019 9:40:31 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (
	SELECT * FROM sys.objects
	WHERE object_id = OBJECT_ID(N'dbo.ControlObjectsHistory')
	AND type in (N'U')
)
DROP TABLE dbo.ControlObjectsHistory
GO


CREATE TABLE [dbo].[ControlObjectsHistory](
	[Installation] [int] NOT NULL,
	[Revision] [bigint] NOT NULL,
	[ObjNam] [nvarchar](16) NOT NULL,
	[Key] [nvarchar](16) NOT NULL,
	[Value] [nvarchar](256) NULL,
	[CreateDateTime] [datetime2](7) NOT NULL DEFAULT GETUTCDATE(),
	[UpdateDateTime] [datetime2](7) NOT NULL DEFAULT GETUTCDATE(),
 CONSTRAINT [PK_ControlObjectsHistory] PRIMARY KEY CLUSTERED 
(
	[Installation] ASC,
	[Revision] ASC,
	[ObjNam] ASC,
	[Key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


