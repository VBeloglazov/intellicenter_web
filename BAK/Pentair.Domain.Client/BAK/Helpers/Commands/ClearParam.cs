﻿namespace Pentair.Domain.Client.Helpers.Commands
{
    public class ClearParam : CommandBase
    {
        public ClearParam(
            string messageID
            )
            : base(CommandString.ClearParam, messageID)
        {
        }

    } // class ClearParam
}