using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using SERVICE_SHARED;

namespace Pentair.Web.Api
{
    public partial class WebSocketClient
    {
        private static List<WebSocketClient> Clients = new List<WebSocketClient>();
        private static object ClientsLock = new object();

        /// <summary>
        /// Thread safe.
        /// </summary>
        protected static void AddClient(
            WebSocketClient clientToAdd)
        {
            lock (WebSocketClient.ClientsLock)
            {
                WebSocketClient.Clients.Add(clientToAdd);

                LogManager.LogInformationAsync(
                    source: clientToAdd.EventSourceName,
                    eventBriefDescription: "WebSocket Client added",
                    installationId: clientToAdd.InstallationId,
                    userId: clientToAdd.UserId,
                    connectionId: clientToAdd.ConnectionId,
                    eventFullDescription:
                        String.Format("Connection Type: {0}\nTotal Clients: {1}",
                            clientToAdd.TypeEndpoint,
                            WebSocketClient.Clients.Count
                        )
                );

            } // lock(WebSocketClient.ClientsLock)

        } // AddClient()

        /// <summary>
        /// Thread safe.
        /// Removes client from the list and from the database.
        /// </summary>
        protected virtual void RemoveClient(
            WebSocketClient clientToRemove)
        {
            //
            // Remove from the list
            //

            lock (WebSocketClient.ClientsLock)
            {
                WebSocketClient.Clients.Remove(clientToRemove);

                LogManager.LogInformationAsync(
                    source: clientToRemove.EventSourceName,
                    eventBriefDescription: "WebSocket Client removed",
                    installationId: clientToRemove.InstallationId,
                    userId: clientToRemove.UserId,
                    connectionId: clientToRemove.ConnectionId,
                    eventFullDescription:
                        String.Format("Connection Type: {0}\nTotal Clients: {1}",
                            clientToRemove.TypeEndpoint,
                            WebSocketClient.Clients.Count
                        )
                );

            } // lock(WebSocketClient.ClientsLock)

            //
            // Remove from the database
            //

            if (clientToRemove.Connection != null)
            {
                Guid removedConnectionId;

                switch (clientToRemove.TypeEndpoint)
                {
                    case EndpointType.THW:
                        using (DbContextPentair db = DbContextPentair.New())
                        {
                            removedConnectionId = db.RemoveConnectionThw(
                                clientToRemove.ConnectionId,
                                this.InstallationId
                            );
                        }
                        break;

                    case EndpointType.Client:
                        using (DbContextPentair db = DbContextPentair.New())
                        {
                            removedConnectionId = db.RemoveConnectionClient(
                                clientToRemove.ConnectionId
                            );
                        }
                        break;

                    default:
                        removedConnectionId = Guid.Empty;
                        break;
                }

                if (removedConnectionId == clientToRemove.ConnectionId)
                {
                    if (WebApiApplication.LogVerbose)
                    {
                        LogManager.LogInformationAsync(
                            source: clientToRemove.EventSourceName,
                            eventBriefDescription: "[Connection] record removed",
                            installationId: clientToRemove.InstallationId,
                            userId: clientToRemove.UserId,
                            connectionId: clientToRemove.ConnectionId,
                            eventFullDescription:
                                String.Format("Connection Type: {0}", clientToRemove.TypeEndpoint)
                        );
                    }
                }
                else
                {
                    LogManager.LogErrorAsync(
                        source: clientToRemove.EventSourceName,
                        eventBriefDescription: "Removing [Connection] record failed",
                        installationId: clientToRemove.InstallationId,
                        userId: clientToRemove.UserId,
                        connectionId: clientToRemove.ConnectionId,
                        eventFullDescription:
                        String.Format("[Connection] record not found for Id = {0}", clientToRemove.ConnectionId)
                    );
                }
            }
            else
            {
                // Not necessarily error. It could be THW just connected and AddConnection() failed before adding
                // a new record to the [Connections]. Then we remove client not having the record.

                LogManager.LogWarningAsync(
                    source: clientToRemove.EventSourceName,
                    eventBriefDescription: "Removing [Connection] record failed",
                    installationId: clientToRemove.InstallationId,
                    userId: clientToRemove.UserId,
                    connectionId: clientToRemove.ConnectionId,
                    eventFullDescription:
                        "Client's Connection object is NULL. [Connections] not in sync with WebSocketClient.Clients"
                );
            }

        } // RemoveClient()

        /// <summary>
        /// Thread safe.
        /// Terminates client connections.
        /// </summary>
        public static void RemoveThwClients(
            int installationId,
            bool removeThw,
            string caller,
            string reason)
        {           
            lock (WebSocketClient.ClientsLock)
            {
                WebSocketClient.Clients.ForEach((client) =>
                    {
                        if (client.InstallationId == installationId &&
                            (removeThw || (!removeThw && client.TypeEndpoint == EndpointType.Client)))
                        {
                            client.Disconnect(
                                isHardStop: true,
                                caller: caller,
                                reason: reason
                            );
                        }
                    }
                );

            } // lock(WebSocketClient.ClientsLock)

        } // RemoveThwClients()

        /// <summary>
        /// Thread safe.
        /// </summary>
        public static WebSocketClient FindClientByConnectionId(
            Guid connectionId)
        {
            lock (WebSocketClient.ClientsLock)
            {
                return
                    WebSocketClient.Clients.Find(x =>
                        x.ConnectionId == connectionId
                    );

            } // lock(WebSocketClient.ClientsLock)

        } // FindClientByConnectionId()

        /// <summary>
        /// Thread safe.
        /// </summary>
        public static WebSocketClient FindHostByInstallationId(
            int installationId)
        {
            lock (WebSocketClient.ClientsLock)
            {
                return
                    WebSocketClient.Clients.Find(x =>
                        x.InstallationId == installationId &&
                        x.TypeEndpoint == EndpointType.THW
                    );

            } // lock(WebSocketClient.ClientsLock)

        } // FindHostByInstallationId()

    } // class WebSocketClient
}