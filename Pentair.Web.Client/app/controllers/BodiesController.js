﻿'use strict';
var PWC = PWC || {};
PWC.controller('BodiesController', [
    '$scope', '$rootScope', '$location', '$filter', 'PropertyService', 'PropertyModel', 'Enumerations',
    function($scope, $rootScope, $location, $filter, propertyService, propertyModel, enumerations) {
        $scope.Busy = false;
        $scope.AdminSecurity = false;
        $scope.Property = propertyService;
        $scope.Model = propertyModel;
        $scope.DashboardTabs = propertyModel.dashboardTabs;
        $scope.$parent.Layout = "layout-base";
        $scope.sysPref = propertyModel.Objects._5451;
        $scope.enumerations = enumerations;
        $scope.shareBody = false;
        //$scope.Property.getUpdateClock();   // UPDATE CLOCK

        $scope.showLastTemp = function(body) {
            if(!$scope.sysPref)
                return false;
                        
            //console.log("========== body.Status = " + body.STATUS + "   sysPref.TEMPNC = " + $scope.sysPref.TEMPNC);
            return (enumerations.bodyOffStatusList.indexOf(body.STATUS) >= 0 && $scope.sysPref.TEMPNC === 'ON');            
        };

        $scope.getLastTemp = function(body) {
            var value = (body.parent.LSTTMP) || '--';
                        
            //console.log("========== body.parent.LSTTMP = " + body.parent.LSTTMP);
            return value            
        };

        $scope.isPresentIChlorBody = function (body) {
            if (body.CHEMISTRY.ICHLOR != undefined)
                return true;
            else
                return false;
        };
        
        $scope.getSaltValue = function (body) {
            var value = "0";
            if (body.CHEMISTRY.ICHLOR)
                value = body.CHEMISTRY.ICHLOR.SALT;
            else {
                if (body.CHEMISTRY.ICHEM)
                    value = body.CHEMISTRY.ICHEM.SALT;
            }

            return value;
        };        
       
        $scope.isMenuEnabled = function (flag) {
            if ($scope.model.ACL.SHOMNU.indexOf(flag) >= 0)
                return "";              // normal 
            else
                return "disabledItem";  // format to disabled item
        }

        $scope.isCoolingActive = function (SHOMNU,COOL) {
            
            //console.log("============== isCoolingActive BODY SHOWMENU: ", SHOMNU, "     BODY COOL : ",COOL)            

            var status = false
            if (SHOMNU && (SHOMNU.indexOf('e') > -1)) {
                status  = true
            }


            if (COOL && COOL==="ON"){                
                status = true
            }
            
            return status
        };

        // allow to display heat set active
        $scope.isHeatActive = function (SHOMNU, currentHeatMode) {
            //console.log("============ isHeatActive BODY SHOWMENU: ", SHOMNU, "     Current heat Mode: ", currentHeatMode)
            var status = false

            // checking heating 
            if (SHOMNU && (SHOMNU.indexOf('h') > -1)) {
                status = true
            }

            // checking if heater is off..
            if (currentHeatMode === "00000") {
                status = false
            }
            else {
                // here enters if no flags available and current heatmode is different than null then enable heat is active.. 
                if(!status)
                    status = true
            }

            return status
        };

        // check if body is active
        $scope.getbodyIcon = function (body) {
            var running = false;
            switch (body.SUBTYP) {
                case 'SPA':
                case 'POOL':
                    if (body.STATUS === "ON")
                        running = true;  // body is active
                    break;
            }

            var bodyname = body.SUBTYP;

            /* uncomment next 2 lines when animation image is ready.
            if (running === true)
                bodyname = bodyname + "on"; // is running
             */


            return bodyname.toLowerCase();
        }

        $scope.getBodyIconImage = function (body) {
            var name = "";
            name = body.SNAME; //removing parent so is passed on the caller. body.parent.SNAME   
            if (body.HTMODE) { //HTSRCTYP) {
                if (body.STATUS === "ON") {
                    switch (body.HTMODE) { //HTSRCTYP) {
                        case "1": name = name + " Flame"; break;
                        case "2": name = name + " Solar"; break;
                        case "3": name = name + " Flake"; break;
                            // FLAME ICONS
                        case "4": name = name + " Flame"; break;    // ultra
                        case "5": name = name + " Flame"; break;    // hybrid
                        case "6": name = name + " Flame"; break;    // mastertemp
                        case "7": name = name + " Flame"; break;    // max-e temp

                        case "0": default: name = name + " None"; break;
                    }
                }
            }
            return name;
        }

        $scope.getBodyStatusIconName = function (body) {
            if (body === undefined) {
                return ""   // is null 
            }
            var name = body.SNAME;
            if (body.HTMODE) {
                if (body.STATUS === "ON") {
                    var SRC = body.HTSRC;
                    switch (body.HTMODE) {
                        case "0":   // off
                            name = ""; break;
                        case "1":   // gas on
                            name = "flameblinking"; break;
                        case "2":   // solar on
                            name = "sunblinking"; break;
                        case "3":   // heatpump on, ultratemp on
                            name = "flameblinking"; break;
                        case "4":   // ultra temp on
                            name = "flameblinking"; break;
                        case "5":   // hybrid on
                            name = "flameblinking"; break;
                        case "6":   // master temp on
                            name = "flameblinking"; break;
                        case "7":   // MAX-E temp on
                            name = "flameblinking"; break;
                        case "10":  // solar cooling
                            name = "flakeblinking"; break;
                        case "11":  // ultratemp cooling
                            name = "flakeblinking"; break;
                            /*
                            angular.forEach(body.HEATERS, function (item) {
                                if (body.HTSRC === item.HNAME) {
                                }
                            });
                            */
                        default:
                            name = ""; break;
                    }

                }
            }
            return name;
        };

        $scope.isSharePresent = function (body) {                       
            //return propertyModel.ShareBody;            
            if (body.parent === undefined)   // check if parent exist
                return false;   
            
            //console.log("-+++++++++++++++++++++ BODY PAGES, BODY = " + body.parent.SHARE);
            if (body.parent.SHARE === "00000")
                return false;
            else
                return true;
          
        };
        
        $scope.supportGasConnected = function () {      // checking version 1.43. or larger      
            var version = $scope.Model.getOCPVersion(); // checking version for new format using tMODE for heatMode 
            return (version >= $scope.enumerations.CONNECTEDGAS_VERSION)   // check minimum connected version
        }

        $scope.isBodyCooling = function (body) {
            if (!body.SUBTYP)   // check if parent exist
                return false;

            if ((body.SUBTYP === "POOL") ||
                 (body.SUBTYP === "SPA")) {
                // console.log("-+++++++++++++++++++++ BODY TYPE = POOL" );
                return true;
            }
            else {
                return false;
            }

        };
        $scope.setSelectedHeaterToBody = function (OBJBODY, TOKEN, VALUE,MODE) {
            // checking version for new format using tMODE for heatMode 
            var version = $scope.Model.getOCPVersion();
            if (version >= $scope.enumerations.CONNECTEDGAS_VERSION) {  // check minimum connected version
                console.log("Heater setting: ", " objname( ", OBJBODY, " ) Token( ", "MODE", " ) value( ", MODE, " )");
                $scope.Property.SetValue(OBJBODY, "MODE", MODE);
            }
            console.log("Heater setting: ", " objname( ", OBJBODY, " ) Token( ", TOKEN, " ) value( ", VALUE, " )");
            $scope.Property.SetValue(OBJBODY, TOKEN, VALUE);
        }

        $scope.isBodyPresent = function (body) {                                    
            if (!body.SUBTYP)   // check if parent exist
                return false;

            
            if ((body.SUBTYP === "POOL") ||
                 (body.SUBTYP === "SPA")) {
               // console.log("-+++++++++++++++++++++ BODY TYPE = POOL" );
                return true;
            }
            else {               
                return false;
            }

        };
        
        (function() {
            buildBodies();
        }());

        // LISTENING BROADCAST 
        $scope.$on('UpdatedHeaters', function (objname) {

            console.log("BodiesController:: processing broadcast UpdatedHeaters: ", objname);
            buildBodies();
        });

        $scope.$on('ObjectDeleted', function (objname) {

            console.log("BodiesController:: processing broadcast ObjectDeleted : ", objname);

            buildBodies();
        });

        function buildBodies() {
            console.log("CONTRUCTOR : ---------------------------building bodies  -----------------------")
            var result = [];
            for (var key in propertyModel.SharedBodies) {
                var value = propertyModel.SharedBodies[key];
                var relationShip = {
                    parent: {},
                    child: {}
                };
                if (!propertyModel.Objects[value]) {
                    $rootScope.configurationIssue = false;  // fix bug : pop up message "configuration on the hardware"
                    relationShip = {
                        parent: propertyModel.Objects[key],
                        child: { SNAME: '' }
                    }
                }
                else if (propertyModel.Objects[key].LISTORD < propertyModel.Objects[value].LISTORD) {
                    relationShip = {
                        parent: propertyModel.Objects[key],
                        child: propertyModel.Objects[value]
                    };

                } else {
                    relationShip = {
                        parent: propertyModel.Objects[value],
                        child: propertyModel.Objects[key]
                    };
                }
                relationShip.parent.HEATERS = getHeaters(relationShip.parent.OBJNAM);
                relationShip.child.HEATERS = getHeaters(relationShip.child.OBJNAM);
                relationShip.CHEMISTRY = getChemistries(relationShip.parent.OBJNAM);
                result.push(relationShip);
            }

            $scope.SharedBodies = result;

            $scope.SingleBodies = propertyModel.SingleBodies.map(function (item, index) {
                var obj = propertyModel.Objects[item];
                obj.CHEMISTRY = getChemistries(obj.OBJNAM);
                obj.HEATERS = getHeaters(obj.OBJNAM);
                return obj;
            });

        }
        
        function getHeaters(bodyName) {
                 
            var filterHeaters = [];
            angular.forEach(propertyModel.Heaters, function (heater) {
                if (heater.BODY) { // check if BODY exists.
                    if (heater.BODY.indexOf(bodyName) >= 0) {
                        var h1 = heater;
                                                
                        if ($scope.supportGasConnected()) { // CHECK NEW SUPPORTED GAS HEATERS IS AVAILABLE
                            switch (h1.SUBTYP) {
                                case "HCOMBO":                                                                        
                                    
                                    h1.DISPLAY = "Hybrid-Gas Only Mode"
                                    h1.MODE = '7';
                                    h1.ORDER = 9;
                                    filterHeaters.push(h1);

                                    var h2 = angular.copy(heater);
                                    h2.DISPLAY = "Hybrid-Heat Pump Only Mode"
                                    h2.MODE = '8';
                                    h2.ORDER = 10;
                                    filterHeaters.push(h2);

                                    var h3 = angular.copy(heater);
                                    h3.DISPLAY = "Hybrid-Hybrid Mode"
                                    h3.MODE = '9';
                                    h3.ORDER = 11;
                                    filterHeaters.push(h3);

                                    var h4 = angular.copy(heater);
                                    h4.DISPLAY = "Hybrid-Dual Mode"
                                    h4.MODE = '10';
                                    h4.ORDER = 12;
                                    filterHeaters.push(h4);

                                    break;
                                default:
                                    h1.MODE = '0';
                                    filterHeaters.push(h1);
                                    break;
                            }
                        }
                        else {
                            h1.MODE = '0';
                            filterHeaters.push(h1);
                        }
                        
                    }
                }
            });

            filterHeaters.unshift({ SNAME: 'OFF', OBJNAM: '00000', DISPLAY: "OFF", ORDER:0 });

            if ($scope.supportGasConnected()) {
                filterHeaters.sort(function (a, b) {
                    var keyA = a.ORDER,
                        keyB = b.ORDER;
                    if (keyA < keyB) return -1;
                    if (keyA > keyB) return 1;
                    return 0;
                });
            }
            
            return filterHeaters;

        };
        
        $scope.getHeaterName = function (subtype) {

            var dontchange = "Don't Change";

            switch (subtype) {
                case 'GENERIC':     return "Heater"                    
                case 'HTPMP':       return "Heat Pump Only"                    
                case 'SOLAR':       return "Solar Only"
                case 'ULTRA':       return "UltraTemp Only"
                case 'HCOMBO':      return "Hybrid"
                case 'HXSLR':       return 'Solar Preferred'
                case 'HXHTP':       return 'Heat Pump Preferred'
                case 'HXULT':       return 'UltraTemp Preferred' 
                case 'HOLD': return dontchange;
                case 'MASTER':      return 'MasterTemp'
                case 'MAX':         return 'Max-E-Therm'
                

                default:            return "Heater"
            }
        };

        $scope.getHeaterItemName = function (heater) {
            var name = heater.SNAME
            
            // check if DISPLAY exits
            if (heater.DISPLAY) {
                name = heater.DISPLAY
            }

            if (heater.OBJNAM === "00000"){
                name = "OFF"
                return name
            }
             
            return name
        };

        $scope.isBodyScheduled = function (body) {
            var scheduled = ""
            if (body ===null )
                return scheduled;
                                                
            propertyModel.Schedules.forEach(function (schedule) {
                if (schedule.ACT === "ON") {  // checking if is active                    
                    if ((schedule.CIRCUIT === "C0006") && (body.SUBTYP === "POOL")) {   // is POOL body and circuit C0006
                        scheduled = "(Scheduled) \n";
                    }
                    if ((schedule.CIRCUIT === "C0001") && (body.SUBTYP === "SPA")) {   // is SPA body and circuit C0001
                        scheduled = "(Scheduled) \n";
                    }
                }
            });

            return scheduled;
        }

        $scope.getSolarSensor = function () {   // return solar sensor temperature
            var value = ""
            if (propertyModel.SolarSensors) {
                propertyModel.SolarSensors.forEach(function (obj) {
                    if (obj.OBJNAM === "SSS11") {  // checking if is active                    
                        if ((obj.SUBTYP === "SOLAR") && (obj.PROBE)) {   // is POOL body and circuit C0006                            
                            value = " (" + obj.PROBE + '\xB0' + ")"; // " add degrees char \xB0 = °";
                        }
                    }
                });
            }
            return value;
        }        

        $scope.checkExceptionUltraTemp = function (body, name) {// checking dependency for ultra all names on heaters selection are default by Heat Pump
            var ultra = false
            angular.forEach(body.HEATERS, function (obj) {
                if (obj.SUBTYP === 'ULTRA') 
                    ultra = true                                        
            });
            if (ultra)
                name = name.replace("Heat Pump","UltraTemp")
            return name        

        }
        $scope.getHeaterNameSelected = function (body, heaterID, currentHeatMode) {
            var name = "Off";
            var scheduled = "";            
            if ($scope.supportGasConnected()) {
                var mode = 1; //Off  
                if (body.MODE) {    // checking if exists
                    mode = body.MODE
                }
                //name = $scope.enumerations.getHeaterModeName(mode);
                name = $scope.enumerations.getHeaterModeNameShort(mode);
                
                name = $scope.checkExceptionUltraTemp(body, name) // checking dependency for ultra all names on heaters selection are default by Heat Pump

                scheduled = $scope.isBodyScheduled(body);

                name = "Heat Mode: " + scheduled + name

                if (name.search("Solar") >= 0)    // check is solar value
                    name = name + $scope.getSolarSensor();

                return name;
            }

            angular.forEach(body.HEATERS, function (obj) {
                if (obj.HNAME == heaterID) {
                    if (obj.SNAME) {                        
                        name = obj.SNAME                        
                    }
                    // overwrite name with fix display name
                    if (obj.DISPLAY) {                        
                        name = obj.DISPLAY
                    }
                }
            });
            
            scheduled = $scope.isBodyScheduled(body);

            name = "Heat Mode: " + scheduled + name

            if (name.search("Solar") >= 0)    // check is solar value
                name = name + $scope.getSolarSensor();


            return name;
        }

        //!showLastTemp(body.parent) ? 'CURRENT_TEMP' : 'LAST_TEMP'


        function getChemistries(bodyName) {
            var chemistry = {
                ICHEM: null,
                ICHLOR: null
            };
            /*
            var chemistries = $filter('objectsByParamValue')(propertyModel.Chemistry, ['BODY', 'SHARE'], bodyName);
            chemistry.ICHLOR = $filter('objectsBySubType')(chemistries, 'ICHLOR')[0];
            chemistry.ICHEM = $filter('objectsBySubType')(chemistries, 'ICHEM')[0];
            */
            var panelType = 'SHARE';
            if (propertyModel.PANID) { // "SINGLE",    // NONE, SINGLE, SHARE, DUAL
                //console.log("========== paneltype " + propertyModel.PANID);
                panelType = propertyModel.PANID;
            }
            /*
            var allChemistries = $filter('filterOutByObjectType')(propertyModel.Objects, 'CHEM');

            var chemistries;
            switch (panelType) {
                case 'SINGLE':
                    chemistries = $filter('objectsByParamValue')(allChemistries, ['BODY', 'SINGLE'], bodyName);
                    break;

                case 'DUAL':
                case 'SHARE':
                default:
                    chemistries = $filter('objectsByParamValue')(allChemistries, ['BODY', 'SHARE'], bodyName);
                    break;
            }

            chemistry.ICHLOR = $filter('objectsBySubType')(chemistries, 'ICHLOR')[0];
            chemistry.ICHEM = $filter('objectsBySubType')(chemistries, 'ICHEM')[0];
            */

            angular.forEach($scope.Model.Chemistry, function (obj) {
                if (((obj.BODY === bodyName) || (panelType === 'SHARE')) &&                
                     (obj.OBJTYP === 'CHEM')
                    ) {
                    switch (obj.SUBTYP) {
                        case 'ICHEM':
                            chemistry.ICHEM = propertyModel.Objects[obj.OBJNAM];
                            break;
                        case 'ICHLOR':
                            chemistry.ICHLOR = propertyModel.Objects[obj.OBJNAM];
                            break;
                    }
                }
            });

            
            return chemistry;
        }

        $scope.setSelectHeater = function (parentOBJNAM, type, objNam) {                       
                   
            $scope.Property.SetValue(parentOBJNAM, 'HEATER', objNam)
            // retrieve params
            var params1 = "HEATER";
            propertyService.GetParamListByObject(params1, parentOBJNAM);
        };


        $scope.$on('CircuitUpdate', function (objname, item, obj) {
            //console.log("CircuitUpdate:" + objname)
            
            // process singlebodies ONLY
            
            angular.forEach($scope.SingleBodies, function (body) {
                angular.forEach(body.HEATERS, function (circuit) {
                    /*         
                    if (circuit.BODY === item.objectList.objnam) { // check match
                        if (item.objectList){
                            if (item.objectList.params.HEATER) {
                                circuit.params.HEATER = item.objectList.params.HEATER;
                            }
                        }
                    }
                    */
               });
            });            
        });
    }
]);
