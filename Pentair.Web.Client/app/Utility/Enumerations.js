var PWC = PWC || {};
PWC.factory('Enumerations', function($filter) {
    var factory = {};
    factory.MaxTimeOut = 60;
    factory.MaxReconnectAttempts = 5;  //500;
    factory.CONNECTEDGAS_VERSION = 1.043;   // Connected gas minimum version ICWEB
    factory.DefaultInstallation = { PoolName: 'Select A Installation', InstallationId: '0' };
    factory.DefaultScheduledCircuit = { SNAME: 'Select A Circuit', OBJNAM: '0' };
    factory.HeatOffCircuit = { SNAME: 'Heat Mode Off', OBJNAM: 'NONE' };
    factory.DefaultSelectedTimezone = { ZONE: 0, VALUE: 'Greenwich Mean Time' };
    factory.DefaultSelectedCircuitType = { VALUE: 'GENERIC', LABEL: 'Generic' };
    factory.bodyOffStatusList = ['OVRON', 'OFF', 'DLYON'];
    factory.bodyOnStatusList = ['OVROFF', 'ON', 'DLYOFF'];
    factory.LiveParams = [
        { objtyp: 'body', liveParams: ["TEMP", "HITMP", "LOTMP", "HEATER", "STATUS", "HTMODE", "HTSRC", "VOL", "MANUAL", "LSTTMP", "HNAME","MODE"/*,"HTSRCTYP"*/] },
        { objtyp: 'circuit', liveParams: ["STATUS", "MODE", "LISTORD", "USAGE", /*"ACT", removed by ram suggestion*/
        "FREEZE", "LIMIT", "USE", "MANUAL", "FEATR", "DNTSTP", "CHILD", "HNAME", "SNAME", "RLY",
        "OBJNAM", "OBJTYP", "SHOMNU", "TIME", "TIMOUT", "SOURCE", "SUBTYP", "BODY"] },        
        { objtyp: 'heater', liveParams: ["STATUS", "SUBTYP", "PERMIT", "TIMOUT", "READY", "HTMODE", "SHOMNU", "COOL", "COMUART", "BODY", "HNAME", "START", "STOP", "HEATING","BOOST","TIME","DLY"] },
        { objtyp: 'hcombo', liveParams: ["LISTORD"] },
        { objtyp: 'REMOTE', liveParams: ["ENABLE"] },
        { objnam: '_A135', liveParams: ["OBJTYP", "SUBTYP", "PROBE", "STATUS"] },
        { objnam: '_C10C', liveParams: ["OBJTYP", "DAY", "MIN", "OFFSET", "SNAME", "TIMZON", "SRIS", "SSET", "CLK24A", "LOCX", "LOCY", "ZIP", "DLSTIM"] },
        { objnam: '_C105', liveParams: ["OBJTYP", "SOURCE"] },
        { objnam: '_5451', liveParams: ["OBJTYP", "MODE", "AVAIL", "TEMPNC", "VACFLO", "VACTIM", "START", "STOP", "HEATING", "VALVE", "TIMZON", "VER","MANHT"] },      //"MANUAL"
        { objnam: '_CFEA', liveParams: ["OBJTYP", "MANOVR"] },
        { objnam: '_57A7', liveParams: ["PARTY","BADGE" /*,"ACT","TIME"*/]},
        { subtyp: 'ichlor', liveParams: ["SALT", "PRIM", "SEC", "SUPER", "TIMOUT"] },
        { subtyp: 'DIMMER', liveParams: ["LIMIT"] },
        { subtyp: 'GLOWT', liveParams: ["LIMIT"] },
        { subtyp: 'CIRCGRP', liveParams: ["CHILD", "SELECT"] },
        { subtyp: 'LITSHO', liveParams: ["CHILD"] },
        { subtyp: 'ichem', liveParams: ["PHSET", "ORPSET", "PHTNK", "ORPTNK", "CALC", "CYACID", "ALK", "SALT", "ORPVAL", "PHVAL", "SINDEX", "PHLO", "PHHI", "ORPLO", "ORPHI", "QUALTY", "CHLOR"] },
        { objtyp: 'chem', liveParams: ["OBJNAM", "OBJTYP", "SUBTYP", "LISTORD", "COMUART", "COMPSPI", "ASSIGN", "BODY", "SHARE", "DEVCONN", "PRIM", "SEC", "TIMOUT", "SUPER", "ABSMAX", "SNAME", "CHLOR"] },
        { objtyp: 'PUMP', liveParams: ["RPM","GPM","PWR","CIRCUIT","PRIM","MIN","MAX","SNAME","SUBTYP","STATIC","PRIMFLO"] },
        { objtyp: 'PMPCIRC', liveParams: ["CIRCUIT","SPEED","BOOST","SELECT","LISTORD","PARENT","STATIC"] },
        { objtyp: 'STATUS', liveParams: [/*"PARENT","MODE","COUNT","PARTY","SINDEX", "SNAME", "TIME",*/ "SHOMNU"] }
    ];
    factory.CLOCK_LISTS = {
        MODE: [
            { name: '12-hour clock', id: 'AMPM' },
            { name: '24-hour clock', id: 'HR24' }
        ],
        SOURCE: [
            { name: 'Manual', id: 'LOCAL' },
            { name: 'Internet', id: 'URL' }
        ],
        PREFERENCES: [
            { name: 'English', id: 'ENGLISH' },
            { name: 'Metric', id: 'METRIC' }
        ]
    };

    factory.AllLightsOnObj = "_A111";

    factory.AllLightsOffObj = "_A110";

    factory.DaysOfWeek = [
        { id: 'U', text: 'Sun' },
        { id: 'M', text: 'Mon' },
        { id: 'T', text: 'Tue' },
        { id: 'W', text: 'Wed' },
        { id: 'R', text: 'Thu' },
        { id: 'F', text: 'Fri' },
        { id: 'A', text: 'Sat' }
    ];
    
    factory.heaterSubtypes = ['HTPMP', 'SOLAR', 'GENERIC', 'ULTRA', "HXSLR", "HXHTP", "HCOMBO", "MASTER", "MAX"]; //factory.heaterSubtypes = ['HTPMP', 'SOLAR', 'GENERIC', 'ULTRA', "HXSLR", "HXHTP", "HCOMBO"];

    factory.heaterSubtypesNoPref = ['GENERIC', 'SOLAR', 'HTPMP', 'ULTRA', 'MASTER', 'MAX','HCOMBO'];
 
    //NOTE: we grab this from getCircuitTypes THW call now
    // factory.circuitSubtypes = [ 'GENERIC', 'SPILLWAY','MASTER', 'CHEM', 'GLOW', 'GLOWT', 'MAGIC1', 'MAGIC2', 'SAML', 'PHOTON', 'DIMMER', 'CLRCASC'];

    factory.valveSubtypes = ['LEGACY'/*, 'INTELLI'*/];

    //factory.circuitSubtypes = [ 'BOOST', 'BYPASS', 'CHEM', 'CIRCGRP', 'COLORRW', 'DECR', 'DIMMER', 'ENABLE', 'FLOOR',
    //    'FRZ', 'GENERIC', 'GLOW', 'GLOWT', 'INCR', 'INTAKE', 'INTELLI', 'LIGHT', 'MAGIC1', 'MAGIC2', 'MASTER', 'PHOTON',
    //    'POOL', 'RETURN', 'SAML', 'SPA', 'SPILL'];


    factory.chemSubtypes = ['ICHEM','ICHLOR'];

    factory.featureCircuitSubtypes = ['GENERIC', 'SPILL'];
    
    factory.pumpSubtypes = ['SINGLE', 'DUAL', 'SPEED', 'FLOW', 'VSF'];

    // IS4 wouldn't be a subtype here because it can only be set by the THW.
    factory.remoteSubtypes = ['IS4', 'IS10', 'SPACMD', 'QT'];


    factory.getRemoteSubtypeObjects = function() {
        return this.remoteSubtypes.map(function(remoteSubtype){
            var remoteSubtypeObject = {};
            remoteSubtypeObject.actualValue = remoteSubtype;
            remoteSubtypeObject.dictionaryKey = remoteSubtype;
            remoteSubtypeObject.dictionaryValue = $filter('translate')(remoteSubtypeObject.dictionaryKey);
            return remoteSubtypeObject;
        });
    };

    // check if this is a special circuit 'X____' start with X
    factory.isCircuitSpecial = function (circuit) {

        var status = false

        if (circuit) {
            var prefix = circuit[0]
            if ((prefix === 'X') || (prefix === 'x'))
                status = true
        }

        return status
    };

    factory.getHeaterSubtypeObjects = function() {
        return this.heaterSubtypes.map(function(heaterSubtype) {
            var heaterSubtypeObject = {};
            heaterSubtypeObject.actualValue = heaterSubtype;
            heaterSubtypeObject.dictionaryKey = heaterSubtype;
            if(heaterSubtype === 'GENERIC') 
                heaterSubtypeObject.dictionaryKey = 'HEATER_GENERIC';
            heaterSubtypeObject.dictionaryValue = $filter('translate')(heaterSubtypeObject.dictionaryKey);
            return heaterSubtypeObject;
        });
    };

    factory.getHeaterSubtypeObjectsNoPref = function () {
        return this.heaterSubtypesNoPref.map(function (heaterSubtype) {
            var heaterSubtypeObject = {};
            heaterSubtypeObject.actualValue = heaterSubtype;
            heaterSubtypeObject.dictionaryKey = heaterSubtype;
            if (heaterSubtype === 'GENERIC')
                heaterSubtypeObject.dictionaryKey = 'HEATER_GENERIC';
            heaterSubtypeObject.dictionaryValue = $filter('translate')(heaterSubtypeObject.dictionaryKey);
            return heaterSubtypeObject;
        });
    };

    factory.getValveSubtypeObjects = function() {
        return this.valveSubtypes.map(function(valveSubtypes) {
            var valveSubtypeObject = {};
            valveSubtypeObject.actualValue = valveSubtypes;
            valveSubtypeObject.dictionaryKey = valveSubtypes;
            valveSubtypeObject.dictionaryValue = $filter('translate')(valveSubtypeObject.dictionaryKey);
            return valveSubtypeObject;
        });
    };

    factory.getPumpSubtypeObjects = function() {
        return this.pumpSubtypes.map(function(pumpSubtype) {
            var pumpSubtypeObject = {};
            pumpSubtypeObject.actualValue = pumpSubtype;
            pumpSubtypeObject.dictionaryKey = pumpSubtype;
            pumpSubtypeObject.dictionaryValue = $filter('translate')(pumpSubtypeObject.dictionaryKey);
            return pumpSubtypeObject;
        });
    };

    factory.getChemSubtypeObjects = function() {
        return this.chemSubtypes.map(function(chemSubtype) {
            var chemSubtypeObject = {};
            chemSubtypeObject.actualValue = chemSubtype;
            chemSubtypeObject.dictionaryKey = chemSubtype;
            chemSubtypeObject.dictionaryValue = $filter('translate')(chemSubtypeObject.dictionaryKey);
            return chemSubtypeObject;
        });
    };

    //NOTE: we grab this from getCircuitTypes THW call now
    // factory.getCircuitSubtypeObjects = function() {
    //     return this.circuitSubtypes.map(function(circuitSubtype) {
    //         var circuitSubtypeObject = {};
    //         circuitSubtypeObject.actualValue = circuitSubtype;
    //         circuitSubtypeObject.dictionaryKey = circuitSubtype;
    //         if(circuitSubtype === 'POOL') circuitSubtypeObject.dictionaryKey = 'CFG_POOL_FILTER';
    //         if(circuitSubtype === 'SPA') circuitSubtypeObject.dictionaryKey = 'CFG_SPA';
    //         circuitSubtypeObject.dictionaryValue = $filter('translate')(circuitSubtypeObject.dictionaryKey);
    //         return circuitSubtypeObject;
    //     });
    // };

    factory.getFeatureCircuitSubtypeObjects = function() {
        return this.featureCircuitSubtypes.map(function(circuitSubtype) {
            var circuitSubtypeObject = {};
            circuitSubtypeObject.actualValue = circuitSubtype;
            circuitSubtypeObject.dictionaryKey = circuitSubtype;
            circuitSubtypeObject.dictionaryValue = $filter('translate')(circuitSubtypeObject.dictionaryKey);
            if(circuitSubtypeObject.actualValue === 'SPILL') {
                circuitSubtypeObject.dictionaryValue = 'Spillway';
            }
            return circuitSubtypeObject;
        });
    };

    factory.GetLiveParams = function(request) {
        var result = [];
        if (!PWC.isEmptyObject(request)) {
            angular.forEach(request, function(value, key) {
                angular.forEach(factory.LiveParams, function(list) {
                    var match = list[angular.lowercase(key)];
                    if (match && angular.lowercase(match) === angular.lowercase(value)) {
                        result = result.concat(list.liveParams);
                    }
                });
            });
        }
        return result;
    };

    factory.StatusMessageTypeFlags = {
        Circuit: [
            //{ key: 'f', value: 'SMT-Circuit-f' }
            { key: 'v', value: 'SMT-Circuit-v' },
            { key: 'h', value: 'SMT-Circuit-h' }
        ],
        Pump: [
            { key: 's', value: 'SMT-Pump-s' },
            { key: 'v', value: 'SMT-Pump-v' },
            { key: 'i', value: 'SMT-Pump-i' },
            { key: 'o', value: 'SMT-Pump-o' },
            { key: 'p', value: 'SMT-Pump-p' },
            { key: 'w', value: 'SMT-Pump-w' },
            { key: 'a', value: 'SMT-Pump-a' },            
            { key: 'e', value: 'SMT-Pump-e' }
        ],

        IntelliChlor: [
            { key: 'f', value: 'SMT-IntelliChlor-f'},
            { key: 'l', value: 'SMT-IntelliChlor-l'},
            { key: 'o', value: 'SMT-IntelliChlor-o'},
            { key: 'c', value: 'SMT-IntelliChlor-c' },
            { key: 'a', value: 'SMT-IntelliChlor-a' },
            { key: 'e', value: 'SMT-IntelliChlor-e'}

        ],
        IntelliChem: [
           { key: 'a', value:  'SMT-IntelliChem-a' },
           { key: 'c', value: 'SMT-IntelliChem-c' },
           { key: 'b', value: 'SMT-IntelliChem-b' },
           { key: 'd', value: 'SMT-IntelliChem-d' },
           { key: 'e', value: 'SMT-IntelliChem-e' },
           { key: 'f', value: 'SMT-IntelliChem-f' },
           { key: 'g', value: 'SMT-IntelliChem-g' },
           { key: 'i', value: 'SMT-IntelliChem-i' },
           { key: 'j', value: 'SMT-IntelliChem-j' },
           { key: 'k', value: 'SMT-IntelliChem-k' },
           { key: 'l', value: 'SMT-IntelliChem-l' },
           { key: 'n', value: 'SMT-IntelliChem-n' },
           { key: 'p', value: 'SMT-IntelliChem-p' },
           { key: 'o', value: 'SMT-IntelliChem-o' },
           { key: 'q', value: 'SMT-IntelliChem-q' },
           { key: 'r', value: 'SMT-IntelliChem-r' },
           { key: 's', value: 'SMT-IntelliChem-s' }
        ],
        HeaterUltraT: [
            { key: 'b', value: 'SMT-HeaterUltraT-b' },
            { key: 'p', value: 'SMT-HeaterUltraT-p' },
            { key: 'g', value: 'SMT-HeaterUltraT-g' },
            { key: 'a', value: 'SMT-HeaterUltraT-a' },
            { key: 'c', value: 'SMT-HeaterUltraT-c' },
            { key: 'h', value: 'SMT-HeaterUltraT-h' },
            { key: 'l', value: 'SMT-HeaterUltraT-l' },
            { key: 'w', value: 'SMT-HeaterUltraT-w' },
            { key: 'r', value: 'SMT-HeaterUltraT-r' },            
            { key: 'o', value: 'SMT-HeaterUltraT-o' },         
            { key: 's', value: 'SMT-HeaterUltraT-s' },
            { key: 'f', value: 'SMT-HeaterUltraT-f' },
            { key: 't', value: 'SMT-HeaterUltraT-t' },                                    
            { key: 'e', value: 'SMT-HeaterUltraT-e' }
        ],        

        HeaterHComboT: [            
            { key: 'a', value: 'SMT-HeaterHComboT-a'},
            { key: 'b', value: 'SMT-HeaterHComboT-b' },
            { key: 'c', value: 'SMT-HeaterHComboT-c' },
            { key: 'd', value: 'SMT-HeaterHComboT-d' },
            { key: 'e', value: 'SMT-HeaterHComboT-e' },
            { key: 'f', value: 'SMT-HeaterHComboT-f' },
            { key: 'g', value: 'SMT-HeaterHComboT-g' },
            { key: 'h', value: 'SMT-HeaterHComboT-h' },
            { key: 'i', value: 'SMT-HeaterHComboT-i' },
            { key: 'j', value: 'SMT-HeaterHComboT-j' },
            { key: 'k', value: 'SMT-HeaterHComboT-k' },
            { key: 'l', value: 'SMT-HeaterHComboT-l' },
            { key: 'm', value: 'SMT-HeaterHComboT-m' },
            { key: 'n', value: 'SMT-HeaterHComboT-n' },
            { key: 'o', value: 'SMT-HeaterHComboT-o' },
            { key: 'p', value: 'SMT-HeaterHComboT-p' },
            { key: 'q', value: 'SMT-HeaterHComboT-q' },
            { key: 'r', value: 'SMT-HeaterHComboT-r' },
            { key: 's', value: 'SMT-HeaterHComboT-s' },
            { key: 't', value: 'SMT-HeaterHComboT-t' },
            { key: 'u', value: 'SMT-HeaterHComboT-u' },
            { key: 'v', value: 'SMT-HeaterHComboT-v' },
        ],
        HeaterMasterT: [
            { key: 'a', value: 'SMT-HeaterMasterT-a'},
            { key: 'b', value: 'SMT-HeaterMasterT-b' },
            { key: 'c', value: 'SMT-HeaterMasterT-c' },
            { key: 'd', value: 'SMT-HeaterMasterT-d' },
            { key: 'e', value: 'SMT-HeaterMasterT-e' },
            { key: 'f', value: 'SMT-HeaterMasterT-f' },
            { key: 'g', value: 'SMT-HeaterMasterT-g' },
            { key: 'h', value: 'SMT-HeaterMasterT-h' },
            { key: 'i', value: 'SMT-HeaterMasterT-i' },
            { key: 'j', value: 'SMT-HeaterMasterT-j' },
            { key: 'k', value: 'SMT-HeaterMasterT-k' },
            { key: 'l', value: 'SMT-HeaterMasterT-l' },
            { key: 'm', value: 'SMT-HeaterMasterT-m' },
            { key: 'n', value: 'SMT-HeaterMasterT-n' },
            { key: 'o', value: 'SMT-HeaterMasterT-o' },

        ],
    };
    
    factory.heatModes = {        
        'HTPMP': 'Heat Pump Only',
        'SOLAR': 'Solar Only',
        'GENERIC': 'Heater',
        'ULTRA': 'UltraTemp Only',        
        'HCOMBO': 'Hybrid', 
        'HXSLR': 'Solar Preferred',        
        'HXHTP': 'Heat Pump Preferred',
        'HXULT': 'UltraTemp Preferred',
    };

    factory.timeModes = [
        { key: 'ABSTIM', value: 'Manual'},
        { key: 'SRIS',   value: 'Sunrise'},
        { key: 'SSET',   value:  'Sunset'}        
    ];
    

    factory.getHeaterModeName = function (mode) {
        var heatmode = "OFF"
        var range = Number(mode);    
        if ((range >= 0) && (range <= 12)) {
                heatmode = factory.ConnectedGasheatModes[mode];
        }

        return heatmode;
    }

    factory.ConnectedGasheatModes = {
        '0': 'Dont Change',
        '1': 'OFF',
        '2': 'Heater',
        '3': 'Solar Only',        
        '4': 'Solar Preferred',
        '5': 'Heat Pump Only',
        '6': 'Heat Pump Preferred',
        '7': 'Hybrid-Gas Only Mode',
        '8': 'Hybrid-Heat Pump Only Mode',
        '9': 'Hybrid-Hybrid Mode',
        '10': 'Hybrid-Dual Mode',
        '11': 'MasterTemp',
        '12': 'Max-E-Therm',        
    };


    factory.getHeaterModeNameShort = function (mode) {
        var heatmode = "OFF"
        var range = Number(mode);
        if ((range >= 0) && (range <= 12))
            heatmode = factory.ConnectedGasheatModesShort[mode];

        return heatmode;
    }

    factory.ConnectedGasheatModesShort = {
        '0': 'Dont Change',
        '1': 'OFF',
        '2': 'Heater',
        '3': 'Solar Only',
        '4': 'Solar Preferred',
        '5': 'Heat Pump',
        '6': 'Heat Pump (Pref)',
        '7': 'Hybrid (Gas)',
        '8': 'Hybrid Heat Pump',
        '9': 'Hybrid (Hybrid)',
        '10': 'Hybrid (Dual)',
        '11': 'MasterTemp',
        '12': 'Max-E-Therm',
    };

    // to be used as heater type selection
    factory.heatModesSelect = {
        'HTPMP': 'Heat Pump',
        'SOLAR': 'Solar',
        'GENERIC': 'Heater',
        'ULTRA': 'UltraTemp',
        'HCOMBO': 'Hybrid',     //'Heater Group',
        'HXSLR': 'Solar Preferred',
        'HXHTP': 'Heat Pump Preferred',
        'HXULT': 'UltraTemp Preferred',
        "MASTER": "MasterTemp",
        "MAX":    "Max-E-Therm",
    };

    factory.heatersPrefer = {
        types: [
                { key: 'HXSLR', value: 'Solar Preferred' },
                { key: 'HXHTP', value: 'Heat Pump Preferred' },
                { key: 'HXULT', value: 'UltraTemp Preferred' }
        ]
    };

    factory.heatersParams = {
        types: [
                { key:'HTPMP', value: 'Heat Pump Only'},
                { key:'SOLAR', value: 'Solar Only'},
                { key:'GENERIC', value: 'Heater'},
                { key:'ULTRA', value: 'UltraTemp Only'},
                { key:'HCOMBO', value: 'Hybrid'},   //'Heater Group'},
                { key:'HXSLR', value: 'Solar Preferred'},
                { key:'HXHTP', value: 'Heat Pump Preferred' },
                { key:'HXULT', value: 'UltraTemp Preferred' },
                { key:'HOLD', value: "Don't change"},
        ]
    };                                                    

    factory.heaterStatus = {
        'ON': 'On',
        'OFF': 'Off',
        'NONE': 'Not Available',
        'INC': 'Heating',
        'DEC': 'Cooling',
        'DLYON': 'Cool Down Delay',
    };

    factory.lightCapabilities = {
        "r": { key: "ROTATE" },
        "p": { key: "STOP" },
        "c": { key: "SYNC" },
        "s": { key: "SWIM" },
        "h": { key: "HOLD", color: true },
        "m": { key: "MODE" },
        "t": { key: "RESET" },
        "u": { key: "THUMP" },
        "d": { key: "SAMMOD" },
        "Q": { key: "RECALL", color: true },
        "y": { key: "PARTY", color: true },
        "M": { key: "ROMAN", color: true },
        "b": { key: "CARIB", color: true },
        "a": { key: "AMERCA", color: true },
        "S": { key: "SSET", color: true },
        "o": { key: "ROYAL", color: true },
        "w": { key: "WHITER", color: true },
        "g": { key: "GREENR", color: true },
        "l": { key: "BLUER", color: true },
        "T": { key: "MAGNTAR", color: true },
        "e": { key: "REDR", color: true },
        "i": { key: "WHITE", color: true },
        "G": { key: "LITGRN", color: true },
        "n": { key: "GREEN", color: true },
        "q": { key: "AQUA", color: true },
        "B": { key: "BLUE", color: true },
        "A": { key: "MAGNTA", color: true },
        "R": { key: "RED", color: true },
        "L": { key: "LITRED", color: true }
    };


    factory.lightColorGroup = {
        types: [
                { key: 'WHITER', value: 'White' },
                { key: 'LITGRN', value: 'Light Green' },
                { key: 'GREEN', value: 'Green' },
                { key: 'CYAN', value: 'Cyan' },
                { key: 'BLUE', value: 'Blue' },
                { key: 'LAVANDER', value: 'Lavander' },
                { key: 'MAGNTA', value: 'Magenta' },
                { key: 'MAGNTAR', value: 'Light Magenta' },                                
        ]
    };

    factory.coloredlights = ["INTELLI", "GLOW", "MAGIC2", "CLRCASC"];

    factory.noncoloredlights = ["LIGHT", "GLOWT", "DIMMER"];

    // factory.countries = ["United States", "Canada", "Mexico", "Afghanistan", "Albania", "Algeria", "Andorra", "Angola", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegovina", "Botswana", "Brazil", "Brunei", "Bulgaria", "Burkina Faso", "Burma", "Burundi", "Cambodia", "Cameroon", "Cape Verde", "Central African Republic", "Chad", "Chile", "China", "Colombia", "Comoros", "Congo, Democratic Republic", "Congo, Republic of the", "Costa Rica", "Cote d'Ivoire", "Croatia", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Fiji", "Finland", "France", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Greece", "Greenland", "Grenada", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, North", "Korea, South", "Kuwait", "Kyrgyzstan", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macedonia", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Mauritania", "Mauritius", "Micronesia", "Moldova", "Mongolia", "Morocco", "Monaco", "Mozambique", "Namibia", "Nauru", "Nepal", "Netherlands", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Norway", "Oman", "Pakistan", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Poland", "Portugal", "Qatar", "Romania", "Russia", "Rwanda", "Samoa", "San Marino", " Sao Tome", "Saudi Arabia", "Senegal", "Serbia and Montenegro", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "Spain", "Sri Lanka", "Sudan", "Suriname", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Togo", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Yemen", "Zambia", "Zimbabwe"];
    factory.countries = ["United States", "Canada"];//factory.countries = ["United States", "Canada", "Mexico"];

    return factory;
});
