﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pentair.Web.Api
{
    public class InstallationsPutModel
    {
        [Required]
        public int PropertyId { get; set; }
        [Required]
        public String Address { get; set; }
        [Required]
        public String City { get; set; }
        [Required]
        public String State { get; set; }
        [Required]
        public String PostalCode { get; set; }

    }

    public class InstallationsNameChangeModel
    {
        [Required]
        public int PropertyId { get; set; }
        [Required]
        public string Name { get; set; }
        public string OwnerName { get; set; }
    }

    public class InstallationRemovePoolsModel
    {
        [Required]
        public List<String> PropertyIds { get; set; }
    }
}