﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Pentair.Web.Api
{
    public class ObjectCreatedResponse : ResponseCommand
    {
        [JsonProperty("objnam", Required = Required.Always)]
        public string ObjNam { get; private set; }

        public ObjectCreatedResponse(
            string messageID,
            string response,
            string objNam
            )
            : base(CommandString.ObjectCreated, messageID, response)
        {
            this.ObjNam = objNam;

        } // ObjectCreatedResponse()

        /// <summary>
        /// Always returns an object, valid or not. If invalid, check the DeserializationError property value.
        /// </summary>
        public static new ObjectCreatedResponse Deserialize(
            string serializedMessage)
        {
            ObjectCreatedResponse command = null;

            try
            {
                command = JsonConvert.DeserializeObject<ObjectCreatedResponse>(serializedMessage);
                command.IsValid = true;
            }
            catch (Exception e)
            {
                // Deserialization as ObjectCreatedResponse failed

                StringBuilder sbError = new StringBuilder();

                sbError.Append("Cannot deserialize as ObjectCreatedResponse. " + e.Message);

                // Try to get at least the base message data
                ResponseCommand baseCommand = ResponseCommand.Deserialize(serializedMessage);

                if (!baseCommand.IsValid)
                {
                    sbError.Append("\n" + baseCommand.DeserializationError);
                }

                command = new ObjectCreatedResponse(
                    messageID: baseCommand.MessageID,
                    response: ResponseString.BadRequest,
                    objNam: String.Empty)
                {
                    IsValid = false,
                    DeserializationError = sbError.ToString()
                };
            }

            return command;

        } // Deserialize()

    } // class ObjectCreatedResponse
}
