﻿using System;
using System.Collections.Generic;

using Newtonsoft.Json;

namespace Pentair.Domain.Client.Helpers.Commands
{
    public class ParamObject
    {
        [JsonProperty("objnam")]
        public string ObjNam { get; private set; }

        [JsonProperty("params")]
        public Dictionary<string, string> Parameters { get; private set; }

        public ParamObject(
            string objNam,
            Dictionary<string, string> parameters)
        {
            this.ObjNam = objNam;
            this.Parameters = parameters;
        }

    } // class ParamObject
}