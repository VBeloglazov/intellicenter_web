﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Windows;

namespace Pentair.Desktop.Simulator.Views
{
    /// <summary>
    /// Interaction logic for HelpAbout.xaml
    /// </summary>
    public partial class HelpAbout : Window
    {
        public HelpAbout()
        {
            InitializeComponent();
            var theApp = System.Reflection.Assembly.GetExecutingAssembly();
            var version = FileVersionInfo.GetVersionInfo(theApp.Location).FileVersion.Split(new char[] {'.'});
            VersionNumber.Content = string.Format("{0}.{1} Build {2}", version[0], version[1], version[2]);
        }

        private void Ok_OnClick(object sender, RoutedEventArgs e)
        {
            Hide();
        }
    }
}
