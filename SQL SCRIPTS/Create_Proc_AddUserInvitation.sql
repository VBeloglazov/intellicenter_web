-- =============================================================
--
-- Author:		Vladimir Beloglazov
-- Create date: 02/13/2018
--
-- Creates:
--
--		Table Type:			None
--
--		Stored Procedure:	PAddUserInvitation
--
-- 2018-04-20 VB: @installationId can be NULL.
--
-- =============================================================

USE PathfinderIntelliCenter
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (
	SELECT * FROM sys.objects
	WHERE object_id = OBJECT_ID(N'dbo.PAddUserInvitation')
	AND type in (N'P', N'PC')
)
DROP PROCEDURE dbo.PAddUserInvitation
GO

CREATE PROCEDURE dbo.PAddUserInvitation
	 @invitationId		AS uniqueidentifier
	,@userEmail			AS nvarchar(max)
	,@installationId	AS int = NULL
	,@accessToken		AS nvarchar(16)

AS
BEGIN

	SET NOCOUNT OFF

	INSERT INTO dbo.UserInvitations (
		 InvitationId
		,EmailAddress
		,InstallationId
		,AccessToken
		,[Status]
		,EmailId
	)
	VALUES (
		 @invitationId      -- InvitationId
		,@userEmail         -- EmailAddress
		,@installationId    -- InstallationId
		,@accessToken       -- AccessToken
		,'UNKNOWN'          -- Status
		,NULL               -- EmailId
	)

END
GO
