﻿describe('IndexController', function () {
    var IndexController;
    var AuthService;
    var $scope;
    var $location;
    var $controller;
    var SocketService;
    beforeEach(angular.mock.module('PentairWebClient'));

    beforeEach(inject(function (_$controller_, _AuthService_, _$rootScope_, _$location_, _SocketService_, _PropertyModel_) {
        AuthService = _AuthService_;
        _PropertyModel_.Ready = true;
        $scope = _$rootScope_.$new();
        $location = _$location_;
        $controller = _$controller_;
        SocketService = _SocketService_;
        IndexController = $controller("IndexController", { $scope: $scope });
    }));

    it('should define scope.authentication', function () {
        expect($scope.Authentication).toBeDefined();
    });

    describe('Scope.LogOut', function () {
        beforeEach(function () {
            spyOn(AuthService, 'logOut');
            spyOn(SocketService, 'closeSocket');
        });

        it('should call AuthService.Logout', function () {
            $scope.LogOut();
            expect(AuthService.logOut).toHaveBeenCalled();
            expect(SocketService.closeSocket).toHaveBeenCalled();
        });
    });

    describe('ToggleAccountMenu', function () {
        it('should toggle DisplayAccountMenu to true', function () {
            $scope.DisplayAccountMenu = false;
            $scope.ToggleAccountMenu();
            expect($scope.DisplayAccountMenu).toBe(true);
        });
    });

    describe('Go to change Password page', function () {
        it('should should navigate to the change password page', function () {
            spyOn($location, 'path');
            spyOn($scope, 'ToggleAccountMenu');
            $scope.GoTo('/changePassword');
            expect($scope.ToggleAccountMenu).toHaveBeenCalled();
            expect($location.path).toHaveBeenCalledWith('/changePassword');
        });
    });
    describe('Go to change Password page', function () {
        it('should should navigate to the remove pool page', function () {
            spyOn($location, 'path');
            spyOn($scope, 'ToggleAccountMenu');
            $scope.GoTo('manageProperties');
            expect($scope.ToggleAccountMenu).toHaveBeenCalled();
            //     expect($location.path).toHaveBeenCalledWith('/removePool');
        });
    });



});