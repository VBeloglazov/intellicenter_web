﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Newtonsoft.Json;
using Pentair.Desktop.Simulator.Models;
using Pentair.Desktop.Simulator.Views;
using Pentair.Domain.Client.Entity;
using Pentair.Domain.Client.Helpers.Commands;
using Pentair.Domain.Client.Infrastructure;

namespace Pentair.Desktop.Simulator
{
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Private Properties

        private readonly MainViewModel viewModel;

        #endregion

        #region Constructor

        public MainWindow()
        {
            InitializeComponent();
            var host = ConfigurationManager.AppSettings.Get("PentairHost");
            viewModel = new MainViewModel(this);
            viewModel.OnPropertyChange += SetPropertyName;
            HostLog.LogIdentifier = LogId.Client;
            HostLog.DataContext = viewModel.RemoteLogModel = new LogModel();
            HostLog.OnClearLogItems += OnClearLogItems;
            ServerLog.LogIdentifier = LogId.Server;
            ServerLog.DataContext = viewModel.ServerLogModel = new LogModel();
            ServerLog.OnClearLogItems += OnClearLogItems;
            BrowserLog.LogIdentifier = LogId.Discovery;
            BrowserLog.DataContext = viewModel.BrowserLogModel = new LogModel();
            BrowserLog.OnClearLogItems += OnClearBrowserLogItems;
            foreach (var item in viewModel.Tabs)
            {
                ControlObjects.Items.Add(item);
            }

            UserView.DataContext = viewModel.User;

            DataContext = viewModel;
            AddHandler(UserView.InviteUserEvent, new RoutedEventHandler(InviteUserEvent));
            AddHandler(RegistrationView.RegisterPropertyEvent, new RoutedEventHandler(PropertyRegistrationEvent));
            AddHandler(RegistrationView.GetRegisteredPropertiesEvent, new RoutedEventHandler(RegisteredPropertiesEvent));
        }

        // Dispose of all events
        public void Dispose()
        {

        }

        #endregion

        #region Public Methods

        public void SetPropertyName(string name)
        {
            Application.Current.Dispatcher.Invoke(
                new Action(() => Title = string.Format("Pentair THW Simulator ({0})", name)));
        }

        #endregion

        #region Private Methods

        private void RefreshDisplay()
        {
            ControlObjects.Items.Clear();
            foreach (var item in viewModel.Tabs)
            {
                ControlObjects.Items.Add(item);
            }
        }

        #endregion

        #region Overrides

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            base.OnClosing(e);
        }

        #endregion

        #region Event Handlers

        public void ConfigurationSelect_Changed(object sender, RoutedEventArgs e)
        {
            var selectedIndex = ConfigurationSelector.SelectedIndex + 1;

            if (null == viewModel.SystemConfig.Property || 
                selectedIndex != viewModel.SystemConfig.Property.LocalPropertyId)
            {
                viewModel.CloseConnections();
                viewModel.ChangeProperty(selectedIndex);
                RefreshDisplay();
            }
        }

        private void OnClearLogItems(LogId log)
        {
            viewModel.ClearLogItems(log);
        }

        private void OnClearBrowserLogItems(LogId log)
        {
            viewModel.ClearLogItems(log);
            viewModel.Bonjour.BrowseServices("_http._tcp");
        }

        private void RegisteredPropertiesEvent(object sender, RoutedEventArgs e)
        {
            var control = e.Source as RegistrationView;
            if (null != control)
            {
                var request = new RegisteredPropertiesMessage(
                    command: CommandString.GetRegisteredProperties,
                    messageID: Guid.Empty.ToString(),
                    userID: control.UserName,
                    password: control.Password
                );

                if (viewModel.HostEnabled)
                {
                    viewModel.MessageProc.SendMessage(JsonConvert.SerializeObject(request));
                }
                else
                {
                    var host = Globals.Hosts[(string) control.SelectedHost.SelectedValue];
                    viewModel.OpenAndSend(JsonConvert.SerializeObject(request), host);
                }
            }
        }

        private void PropertyRegistrationEvent(object sender, RoutedEventArgs e)
        {
           var settings = (RegistrationView)e.Source;
            Property property = null;
            var hasMoved = BusinessLogic.Helpers.BoolValue(settings.HasMovedCheckbox.IsChecked);
            int id;
            Int32.TryParse(settings.PropertyId, out id);

            if (settings.Configuration == PropertyConfigurations.Import)
            {
                if (!viewModel.ParseConfiguration(settings.ConfigFile))
                {
                    MessageBox.Show("No file selected", "Error creating custom configuration", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
            }

            var remoteHost = Globals.Hosts[(string)settings.SelectedHost.SelectedValue];

            using (var db = new SimulatorContext())
            {
                if (settings.HasMoved) {
                    property = (from item in db.Properties
                        where item.PropertyId == id && item.RemoteHost == remoteHost
                        select item).FirstOrDefault();
                }
                else
                {
                    id = -1;
                }
                if (null == property)
                {
                    property = new Property()
                    {
                        RemoteHost = remoteHost,
                        Name = settings.PropertyName,
                        Configuration = (PropertyConfigurations) settings.SelectedConfiguration.SelectedIndex,
                        PropertyId = id
                    };
                }
            }

            viewModel.RegisteringProperty = property;

            string guid = Guid.Empty.ToString();
            const string messageCommand = CommandString.Registration;
            var message = string.Format(
                "{{\"command\": \"{0}\",\"messageID\": \"{1}\",\"userName\": \"{2}\",\"password\": \"{3}\",\"name\": \"{4}\",\"hasMoved\": \"{5}\",\"createNew\": \"{6}\",\"propertyID\": \"{7}\"}}",
                messageCommand,
                guid,
                settings.UserName,
                RegistrationView.Password,
                settings.PropertyName,
                BusinessLogic.Helpers.BoolValue(settings.HasMovedCheckbox.IsChecked) ? "yes" : "no",
                BusinessLogic.Helpers.BoolValue(settings.CreateUserCheckbox.IsChecked) ? "yes" : "no",
                settings.PropertyId);

            var host = Globals.Hosts[(string)settings.SelectedHost.SelectedValue];
            viewModel.OpenAndSend(message, host);
            ((MainViewModel)DataContext).Host.SendMessage(message);
        }

        private void InviteUserEvent(object sender, RoutedEventArgs e)
        {
            var settings = (UserView)e.Source;
            var msg = new PoolAccessUser(
                command: CommandString.InviteUser,
                messageID: "",
                param: new Dictionary<string, string>()
                {
                    {"EMAIL", settings.Email.Text},
                    {"PERMISSIONGROUP", settings.PermissionGroup.Text}
                }
            );

            ((MainViewModel)DataContext).Host.SendMessage(JsonConvert.SerializeObject(msg));
        }

        private void OnExitClick(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void OnToggleRemoteMode(object sender, RoutedEventArgs e)
        {
            ((MainViewModel)DataContext).SetRemoteMode();
        }

        private void OnToggleBrowsability(object sender, RoutedEventArgs e)
        {
            ((MainViewModel)DataContext).SetBrowsability();
        }

        private void OnHelpAbout(object sender, RoutedEventArgs e)
        {
            var location = Mouse.GetPosition(this);
            var helpDialog = new HelpAbout()
            {
                Top = this.Top + location.X,
                Left = this.Left + location.Y
            };
            helpDialog.ShowDialog();
        }

        #endregion
    }
}
