'use strict';
var PWC = PWC || {};

PWC.factory('PropertyModel', ['$location', '$filter', 'Enumerations', function ($location, $filter, enumerations) {
    var property = {
        Objects: {},
        Panels: [],
        Modules: [],
        Relays: [],
        Bodies: [],
        SharedBodies: {},
        SingleBodies: [],
        CircuitGroups: [],
        VirtualCircuitGroups: [],
        FreezeCircuit: {},
        Circuits: [],
        Chemistry: [],
        Heaters: [],
        Valves: [],
        Pumps: [],
        PumpAffects: [],
        HeaterCombos: [],
        AirSensors: [],
        WaterSensors: [],
        SolarSensors: [],
        FeatureMenu: [],
        StatusMessages: [],
        Singleton: {},
        ACL: {},
        Users: [],
        UserNotifications: {},
        SecurityEnabled: false,
        GuestEnabled: false,
        PendingSubscriptionRequest: {
            command: "RequestParamList",
            objectList: []
        },
        SystemInfo: {},
        NotificationRecipients: [],
        Schedules: [],
        Security: [],
        Installations: [],
        ConnectionStatus: 'Not Connected',
        Ready: false,        
        SelectedInstallation: enumerations.DefaultInstallation.InstallationId,
        DefaultInstallation: enumerations.DefaultInstallation,
        Busy: false,
        InviteStatus: {},
        EditSession: false,
        DeleteUserError: false,
        ListCount: 0,
        RequestedObjects: [],
        PropertyStatusMessageFlags: {},
        SystemInformation: {},
        TimeSince: {
            WriteParam: 0,
            NotifyList: 0
        },
        SecurityInitiated: false,
        hardwareDefinition: {},
        circuitNames: [],
        circuitTypes: [],
        Remotes: [],
        Alerts: [], 
        History: [],
        covers: [],
        PanelType: "SINGLE",    // NONE, SINGLE, SHARE, DUAL
        ShareBody: false,
        mode: "ENGLISH",        // format 
        freezeStatus: "0",        // freeze status
        serviceStatus: "",        // service status
        firmwareUpdateStatus: "", // new firmware update status  : YES OR NO
        firmwareUpdateVersion: "",// new firmware update version
        webApiVersion: "",        // web API version (server version)
        camaraEnabled:false,
        communication: []         // store communication status
    };

    property.Clear = function (resetSelectedInstallation) {
        property.Panels = [];
        property.Modules = [];
        property.Relays = [];
        property.Bodies = [];
        property.SharedBodies = {};
        property.SingleBodies = [];
        property.Circuits = [];
        property.Objects = {};
        property.FeatureMenu = [];
        property.StatusMessages = [];
        property.Heaters = [];
        property.FreezeCircuit = {};
        property.Chemistry = [];
        property.AirSensors = [];
        property.WaterSensors = [];
        property.SolarSensors = [];
        property.HeaterCombos = [];
        property.VirtualCircuitGroups = [];
        property.CircuitGroups = [];
        property.Security = [];
        property.ACL = [];
        property.Users = [];
        property.UserNotifications = [];
        property.Schedules = [];
        property.PendingSubscriptionRequest.objectList = [];
        property.RequestedObjects = [];
        property.Ready = false;
        property.SecurityEnabled = false;
        property.GuestEnabled = false;
        property.SecurityInitiated = false;
        property.PropertyStatusMessageFlags = {};
        property.SystemInformation = {};
        property.SystemInfo = {};
        property.NotificationRecipients = [];
        property.hardwareDefinition = [];
        property.circuitNames = [];
        property.circuitTypes = [];
        property.Remotes = [];
        property.Pumps = [];
        property.PumpAffects = [];
        property.Alerts = [];
        property.History = [];
        property.covers = [];

        property.lostcommunication = [];    // lost communication with chemistry devices

        property.ConnectedChem = false;
        property.ConnectedChlor = false;

        if (resetSelectedInstallation)
            property.SelectedInstallation = enumerations.DefaultInstallation.InstallationId;
        property.Busy = false;


        property.EditSession = false;
        property.DeleteUserError = false;
        property.webApiVersion = "";
    };

    property.getSubscribableObjects = function (page) {
        page = page || $location.path();
        var subscribableObjects = [];
        switch (page.toLowerCase()) {
            case 'pools':
            case '/property':
                subscribableObjects = property.getBodyPageSubscribables();
                break;
            case 'lights':
                subscribableObjects = property.getLightsPageSubscribables();
                break;
            case 'home':
                subscribableObjects = property.getHomePageSubscribables();
                break;
            case 'features':
                subscribableObjects = property.getFeaturesPageSubscribables();
                break;
            case 'schedules':
                subscribableObjects = property.getSchedulePageSubscribables();
                break;
            case 'lightgroups':
                subscribableObjects = property.getLightsPageSubscribables();
                break;
            case 'general':
                subscribableObjects = [{OBJNAM: '_C105'}, {OBJNAM: '_5451'}, {OBJNAM: '_C10C'}, {OBJNAM: '_CFEA'}];
                break;
            case 'chemistry':
                subscribableObjects = $filter('objectsBySubType')(property.Chemistry, ['ICHEM', 'ICHLOR']);
                break;
            case 'pumps':
                subscribableObjects = property.getPumpPageSubscribables();
                break;
            case 'vacation':
                subscribableObjects = property.getSchedulePageSubscribables();
                break;
        }

        //always request Remotes
        subscribableObjects = subscribableObjects.concat(property.Remotes);
        subscribableObjects = subscribableObjects.concat(property.Alerts); // all the individual status'es
        subscribableObjects = subscribableObjects.concat([{ OBJNAM: '_57A7' }]); // status master        
        return subscribableObjects;
    };

    property.getBodyPageSubscribables = function () {
        var subscribableObjects = [{OBJNAM: '_5451'}, {OBJNAM: '_A135'}];
        subscribableObjects = subscribableObjects.concat(
            property.Bodies,
            property.Heaters,
            property.HeaterCombos
        );
        return subscribableObjects;
    }

    property.getHomePageSubscribables = function () {
        var subscribableObjects = $filter('objectsBySection')(property.Circuits, ['l']);
        // Filter out any duplicates.
        angular.forEach($filter('objectsBySection')(property.Circuits, ['f']), function (subscribableObject) {
            var found = false
            angular.forEach(subscribableObjects, function (object, subscribableObject) {
                if (object.OBJNAM === subscribableObject.OBJNAM) {
                    found = true;
                }
            })
            if (!found) {
                subscribableObjects = subscribableObjects.concat(subscribableObject);
            }
        });
        var chemistryPageObjects = $filter('objectsBySubType')(property.Chemistry, ['ICHEM', 'ICHLOR']);
        subscribableObjects = subscribableObjects.concat(property.getBodyPageSubscribables());
        subscribableObjects = subscribableObjects.concat(property.getSchedulePageSubscribables());
        subscribableObjects = subscribableObjects.concat(chemistryPageObjects);

        return subscribableObjects;
    }

    property.getSchedulePageSubscribables = function () {
        var subscribableObjects = $filter('objectsBySection')(property.Circuits, ['e']);
        var circuitGroups = $filter('objectsBySection')(property.CircuitGroups, ['e']);
        var singletons = [{OBJNAM: '_C105'}, {OBJNAM: '_C10C'},{OBJNAM:'_5451'}];
        subscribableObjects = subscribableObjects.concat(circuitGroups);
        subscribableObjects = subscribableObjects.concat(singletons);
        return subscribableObjects;
    }

    property.getPumpPageSubscribables = function () {
        var subscribableObjects = property.Pumps;
        var singletons = [{ OBJNAM: 'X0052' }, { OBJNAM: 'X0042' }]; // special circuit solar status, freeze status
        subscribableObjects = subscribableObjects.concat(property.PumpAffects);
        subscribableObjects = subscribableObjects.concat(singletons);
        return subscribableObjects;
    }


    property.getLightsPageSubscribables = function () {
        var subscribableObjects = $filter('objectsBySection')(property.Circuits, ['l']);
        var circuitGroups = $filter('objectsBySection')(property.CircuitGroups, ['l']);
        subscribableObjects = subscribableObjects.concat(circuitGroups);
        return subscribableObjects;
    }

    property.getFeaturesPageSubscribables = function () {
        var subscribableObjects = $filter('objectsBySection')(property.Circuits, ['f']);
        return subscribableObjects;
    }

    property.getInstallationObject = function (selectedInstallation) {
        var result = null;
        for (var i = 0; i < property.Installations.length; i++) {
            if (property.Installations[i].InstallationId === selectedInstallation) {
                result = property.Installations[i];
            }
        }
        return result;
    }

    property.isSubscribable = function (objNam) {
        var result = false;
        if (objNam) {
            var subscribables = property.getSubscribableObjects();
            angular.forEach(subscribables, function (obj) {
                if (objNam === obj.OBJNAM) {
                    result = true;
                }
            });
        }
        return result;
    }

    property.addPendingSubscription = function (obj) {
        if (property.isSubscribable(obj.objnam)) {
            property.PendingSubscriptionRequest.objectList.push({
                objnam: obj.objnam,
                keys: enumerations.GetLiveParams({
                    objTyp: obj.params.OBJTYP,
                    objnam: obj.objnam,
                    subTyp: obj.params.SUBTYP
                })
            });
        }
    };

    property.supportConnectedHeaters = function () {   // check if connected heater
        var version = property.getOCPVersion()
        return (version >= enumerations.CONNECTEDGAS_VERSION)   // check minimum connected version
    }
    property.getOCPVersion = function () {   // return OCP version #
        var icversionfull = "";
        var version = 1.042;    // default

        if (!property.Objects ||
            !property.Objects._5451 ||
            !property.Objects._5451.VER) // check if 5451 object already exist.
            return version;

        icversionfull = property.Objects._5451.VER.split(',');  // THW Version: IC: 1.043 , ICWEB:2019-06-18 0.90.02
        if (icversionfull[0]) {   // THW Version: IC: 1.043 
            var icversion = icversionfull[0].split(' ');      // find the last space ' '  on   "THW Version: IC: 1.040"           
            if (icversion[1])   // 1.043 
                version = Number(icversion[1]);
        }
        return version;
    }

    property.PermanantSubscriptions = function (Token) {
        var subscriptionRequest = [{
            objnam: 'UFFFE',
            keys: ['ENABLE', 'OBJTYP', "PASSWRD"]
        }];

        if (Token === 'UFFFE') {
            subscriptionRequest[0].keys.push('SHOMNU');
        } else {
            subscriptionRequest.push({
                objnam: Token,
                keys: ['SHOMNU']
            });
        }

        return subscriptionRequest;
    };

    property.isSingleOrSharedBody = function() {
        var isEnable = false;
        if ((property.PANID === "SINGLE") ||
             (property.PANID === "SHARE"))
            isEnable = true;
        return isEnable;
    };

    property.isSharedBody = function () {
        var isEnable = false;
        if ((property.PANID === "SHARE"))
            isEnable = true;
        return isEnable;
    };

    property.getSingleOrSharedPoolID = function() {
        var poolID = '00000';
        angular.forEach(property.Bodies, function(body) {
           if(body.SUBTYP === 'POOL') {
               poolID = body.OBJNAM;
           }
        });
        return poolID;
    };

    property.dashboardTabs = {
        home: {heading: 'HOME', page: 'HOME', disabled: false, active: true, url: '/app/templates/home.html'},
        pools: {heading: 'POOL(s)', page: 'POOLS', disabled: false, active: false, url: '/app/templates/bodies.html'},
        features: {
            heading: 'FEATURES',
            page: 'FEATURES',
            disabled: false,
            active: false,
            url: '/app/templates/features.html'
        },
        lights: {heading: 'LIGHTS', page: 'LIGHTS', disabled: false, active: false, url: '/app/templates/lights.html'},
        chemistry: {
            heading: 'CHEMISTRY',
            page: 'CHEMISTRY',
            disabled: false,
            active: false,
            url: '/app/templates/chemistry.html'
        },
        pumps: {heading: 'PUMPS', page: 'PUMPS', disabled: false, active: false, url: '/app/templates/pumps.html'},
        history: {heading: 'HISTORY', page: 'HISTORY', disabled: false, active: false, url: '/app/templates/history.partial.html'}
    };

    property.settingsTabs = {
        lightgroups: {
            heading: 'LIGHT GROUPS',
            page: 'LIGHTGROUPS',
            disabled: false,
            active: false,
            url: '/app/templates/lightGroups.html'
        },
        schedules: {
            heading: 'SCHEDULES',
            page: 'SCHEDULES',
            disabled: false,
            active: true,
            url: '/app/templates/Schedules.html'
        },
        vacation: {
            heading: 'VACATION',
            page: 'VACATION',
            disabled: false,
            active: false,
            url: '/app/templates/vacation.html'
        }
    };

    property.configurationsTabs = {
        // TBD LATER ADD HOME BUTTON 
        /*
        home: {
            heading: 'HOME',
            page: 'HOME',
            disabled: false,
            active: false,
            url: '/app/templates/home.html'
        },
        */
       
        general: {
            heading: 'GENERAL SETTINGS',
            page: 'GENERAL',
            disabled: false,
            active: true,
            url: '/app/templates/systemInformation.html'
        },
        circuits: {
            heading: 'CIRCUITS',
            page: 'CIRCUITS',
            disabled: false,
            active: false,
            url: '/app/templates/configure-circuits.partial.html'
        },
        bodies: {
            heading: 'BODIES',
            page: 'BODIES',
            disabled: false,
            active: false,
            url: '/app/templates/configure-bodies.partial.html'
        },
        chemistryControl: {
            heading: 'CHEMISTRY CONTROL',
            page: 'CHEMISTRYCONTROL',
            disabled: false,
            active: false,
            url: '/app/templates/configure-chemistry.partial.html'
        },
        
        otherEquipment: {
            heading: 'OTHER EQUIPMENT',
            page: 'OTHEREQUIPMENT',
            disabled: false,
            active: false,
            url: '/app/templates/configure-other-equipment.partial.html'
        },
        
        pumps: {
            heading: 'PUMPS', 
            page: 'PUMPS', 
            disabled: false,
            active: false, 
            url: '/app/templates/configure-pumps.partial.html'
        },
        remotes: {
            heading: 'REMOTES',
            page: 'REMOTES',
            disabled: false,
            active: false,
            url: '/app/templates/configure-remote.partial.html'
        },
    };
    

    property.configreportTabs = {

        general: {
            heading: 'REPORT AN ISSUE',
            page: 'REPORT',
            disabled: false,
            active: true,
            url: '/app/templates/reportInformation.html'
        },

    };

    property.systemMenu = {
        managePools: {heading: 'HOME', disabled: false, connected: true, location: '/property'},
        settings: {heading: 'SETTINGS', disabled: false, connected: true, location: '/settings'},
        users: {heading: 'MANAGE USERS', disabled: false, connected: true, location: '/manageUsers'},
        installations: { heading: 'MANAGE SYSTEMS', connected: false, disabled: false, location: '/installations' },

        
        spaSideRemotes: {
            heading: 'ENABLE SPA-SIDE REMOTES',
            disabled: false,
            connected: true,
            action: 'disableSpaSideRemotes' //'enableSpaSideRemotes'
        },

        //  to be included when is complete this feature:  report a issue.
        // reportissue: { heading: 'REPORT AN ISSUE', connected: false, disabled: false, location: '/systemReportIssue' },
        
        aboutVersion: {
            heading: 'ABOUT',
            disabled: false,
            connected: true,
            action: 'displayAboutVersion'
        },


        logout: { heading: "LOGOUT", connected: true, disabled: false, location: '/login' },
        
        usersguide: {
            heading:"MANUALS", //"USER'S GUIDE",
            connected: true,
            disabled: false,
            action: 'openUsersGuideNone',
            submenu: [
                    { heading: 'Quick Reference Guide', disabled: false, connected: true, action: 'openQuickReferenceGuide' },
                    { heading: 'Installation Guide', disabled: false, connected: true, action: 'openInstallationGuide' } ,
                    { heading: 'Users Guide', disabled: false, connected: true, action: 'openUsersGuide' } ,
                    { heading: 'Wireless Remote', disabled: false, connected: true, action: 'openWirelessRemote' } ,
                    { heading: 'Indoor Control Panel Installation Guide', disabled: false, connected: true, action: 'openIndoorControlPanelInstallationGuide' } ,
                    { heading: 'Personality and Expansion Card Installation Guide', disabled: false, connected: true, action: 'openPersonalityandExpansionCardInstallationGuide' } ,
                    { heading: 'Remote Access Setup Instructions', disabled: false, connected: true, action: 'openRemoteAccessSetupInstructions' },
                    { heading: 'Temperature Sensor Install Guide', disabled: false, connected: true, action: 'openTemperatureSensorInstallGuide' },
            ]
        }

    };
    return property;

}]);
