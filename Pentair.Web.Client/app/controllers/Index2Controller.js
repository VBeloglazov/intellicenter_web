﻿'use strict';
var PWC = PWC || {};

PWC.controller('Index2Controller', ['$scope', '$location', 'PropertyModel',
    function ($scope, $location, propertyModel) {
        $scope.DisplayAccountMenu = true;

        $scope.LogOut = function () {
            $scope.ToggleAccountMenu();
        }

        $scope.ToggleAccountMenu = function () {
            $scope.DisplayAccountMenu = !$scope.DisplayAccountMenu;
        }
    }]);