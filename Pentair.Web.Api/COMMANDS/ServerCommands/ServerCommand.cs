﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Pentair.Web.Api
{
    public class ServerCommand : CommandBase
    {
        public ServerCommand(
            string command,
            string messageID
            )
            : base(command, messageID)
        {
        } // ServerCommand()

        /// <summary>
        /// Must be overriden by subclasses.
        /// </summary>
        public virtual void Handle()
        {
            throw new NotImplementedException("Handle method is not implemented");

        } // Handle()

        /// <exception cref="Exception" />
        public static ServerCommand ServerCommandFactory(
            CommandType commandType,
            string serializedMessage)
        {
            ServerCommand serverCommand;

            switch (commandType)
            {
                case CommandType.ServerRemoveThwClients:
                    serverCommand = ServerCommandRemoveThwClients.Deserialize(serializedMessage);
                    break;

                default:
                    throw new Exception("Unsupported server command type: " + commandType);

            } // switch (commandType)

            return serverCommand;

        } // ServerCommandFactory()

        /// <summary>
        /// Always returns an object, valid or not. If invalid, check the DeserializationError property value.
        /// </summary>
        public static new ServerCommand Deserialize(
            string serializedMessage)
        {
            ServerCommand command = null;

            try
            {
                command = JsonConvert.DeserializeObject<ServerCommand>(serializedMessage);
                command.IsValid = true;
            }
            catch (Exception e)
            {
                // Deserialization as ServerCommand failed

                StringBuilder sbError = new StringBuilder();

                sbError.Append("Cannot deserialize as ServerCommand. " + e.Message);

                // Try to get at least the base message data
                CommandBase baseCommand = CommandBase.Deserialize(serializedMessage);

                if (!baseCommand.IsValid)
                {
                    sbError.Append("\n" + baseCommand.DeserializationError);
                }

                command = new ServerCommand(
                    command: baseCommand.Command,
                    messageID: baseCommand.MessageID)
                {
                    IsValid = false,
                    DeserializationError = sbError.ToString()
                };
            }

            return command;

        } // Deserialize()

    } // class ServerCommand
}
