USE [PathfinderIntellicenter]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (
	SELECT * FROM sys.objects
	WHERE object_id = OBJECT_ID(N'dbo.pr_GetAlertNote')
	AND type in (N'P', N'PC')
)
DROP PROCEDURE dbo.pr_GetAlertNote
GO


-- =============================================
-- Author			:	Wayne Hui
-- Create date		:	01/29/2019
-- Description		:	Shows the alert notifications
-- Input			:	AlertType, AlertDescription
-- Output			:	AlertMode, AlertMessage, Note 
--
-- Modified By			Date		Description
---- =============================================
CREATE PROCEDURE [dbo].[pr_GetAlertNote]
     @AlertType				NVARCHAR(30)
     , @AlertDescription	NVARCHAR(256)
AS
BEGIN

	SET NOCOUNT OFF

------TEST
--DECLARE	 
--	@AlertType			NVARCHAR(30)
--	, @AlertDescription	NVARCHAR(256)

--SET @AlertType = 'Warning'
--SET @AlertDescription = 'VSF: Communication Lost'

---END TEST

DECLARE	
	@DeviceName	NVARCHAR(30)
	, @AlertMessage	NVARCHAR(256)
	, @AlertNote NVARCHAR(max)


	----Parse out the device name and alert message
	SELECT 
		@DeviceName = REPLACE(SUBSTRING(@AlertDescription, 0, CHARINDEX(' ', @AlertDescription)), ':', '')
		, @AlertMessage = SUBSTRING(@AlertDescription, CHARINDEX(':', @AlertDescription)+2, LEN(@AlertDescription)) 


	----Base on device name and alert message, get the note. If it is a warning, do not display the alert type
	IF ((SELECT COUNT(*) FROM [dbo].[AlertNotes]	WHERE DeviceName = @DeviceName AND AlertMessage = @AlertMessage) > 0)
	BEGIN
		SELECT 
			CASE WHEN AlertType = 'Error' THEN AlertType ELSE @AlertType END AS AlertMode
			, @AlertDescription AS AlertMessage
			, Note
		FROM [dbo].[AlertNotes]
		WHERE
			DeviceName = @DeviceName
			AND AlertMessage = @AlertMessage
	END
	ELSE
	BEGIN
		SELECT 
			@AlertType AS AlertMode
			, @AlertDescription AS AlertMessage 
			, NULL AS Note
	END	
                                                     
END
GO

GRANT EXECUTE ON [dbo].[pr_GetAlertNote] TO PUBLIC
GO
