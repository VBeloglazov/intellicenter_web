﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Pentair.Web.Api")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("Pentair.Web.Api")]
[assembly: AssemblyCopyright("Copyright ©  2014")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("f3eacca2-263b-44ce-935d-3ca9f6fe3e07")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers
// by using the '*' as shown below:
//
// 20171208 0.0.0.1 Order added for CommandBase, ResponseCommand and ParamCommandResponse JSON properties.
// 20171212 0.0.0.2 CreateDatabase fix.
// 20171213 0.0.0.3 Removing what is not in use.
//
// 20171213 0.0.0.4 Removing MessageLog.
//
// 20180104 0.0.0.5 BIG REFACTORING:
//                  Pentair.Domain.Client project removed;
//                  Different EF contexts instead of single ApiContext;
//                  UserInstallationListWithDetails/UserSingleInstallationWithDetails as SQL.
//
// 20180109 0.0.0.6 Preventing [Installations] duplicates;
//                  Keep adding THW connection when initial cleanup failed to close already disposed socket.
//
// 20180112 0.0.0.7 SQL exception should not stop pong;
//                  If Disconnect() failed, it still must clean [Connections].
//
// 20180119 0.0.0.8 "Manage Users" fixed, database EF calls replaced with raw SQL.
//
// 20180213 1.0.0.0 EF raw SQL instead of EF API - everywhere, SPs - somewhere;
//                  Retired: Installation.Connected, Installation.LastPinged (temporarily remain in DB and Installation).
//
// 20180213 1.0.0.1 AddUserInvitation fix.
//
// 20180215 1.0.0.2 ControlObjects collation CS -> CI fix;
//                  Send ClearParamResponse or ReleaseParamListResponse back to client if even it was nothing to cancel.
//
// 20180215 1.0.0.3 UpdateControlObjects: Filter input duplications out;
//                  UpdateControlObjects: Delete not presented if only timeSince = 0, separate list for "Deleted".
//
// 20180216 1.0.0.4 History having Revision = 0 is error;
//                  Better diagnostics for "Process ResponseCommand failed".
//
// 20180223 1.0.0.5 Handling null return when query for LastHistoryTime;
//                  Routing back to client failed is not an error when client already disconnected;
//                  Web.config updated with Production Weather account credentials.
//
// 20180226 1.0.0.6 Report THW error as warning.
//
// 20180301 1.0.0.7 Added: API to query weather provider for current conditions;
//                  Report THW authentication error as warning;
//                  Watch for null objects when process RequestParamList/ReleaseParamList.
//
// 20180302 1.0.0.8 EditUser bug fixes.
//
// 20180306 1.0.0.9 Send THW error or even malformed response back to client "as is".
//
// 20180308 1.0.0.10 Multiple user management fixes.
//
// 20180312 1.0.0.11 Eliminate “POST /service/Api/Installations/NameChange/”;
//                   Handling WriteParamResponse from THW, update both [ControlObjects] and [Installations];
//                   DbContextPentair: No need to cast Cast<> results again;
//                   HTTP /Service/api/websocket/<id> cleaning.
//
// 20180315 1.0.0.12 Added: Diagnostics for RegisteredProperties() and ReRegisterProperty().
//
// 20180322 1.0.0.13 Accept the same name for different properties belonging to different users;
//                   [Installations] redesign: Added OwnerUserIdRemove, removed retired columns;
//                   Removed: DatabaseCreationManager class.
//
// 20180409 1.0.0.14 Fixed: Getting/Saving notification recipients;
//                   Fixed: Delete user association -> e-mail notification;
//                   Added: E-mail alert notifications to recipients (2b tested with THW);
//                   Removed: [MessageLogs], [ManufacturingInfo].
//
// 20180411 1.0.0.15 PGetInstallationById -> PGetInstallationById_GIT.
// 20180411 1.0.0.16 AlertOn, AlertOff fix (uppercase).
//
// 20180423 1.0.0.17 E-mail manager replaced: Pathfinder's Mandrill API replaced with Rackspace's SMTP account;
//                   Separate [Installations] update (which can fail) from [ControlObjects] one;
//                   Fixed: "SQL syntax" error when processing ClearHistoryMessage;
//                   Fixed: ResendInvite() - replaced with SP;
//                   Fixed: Multiple fixed for Rename User, Delete User, etc.;
//                   Fixed: Updating [Installations].LastHistory - replaced with SP.
//
// 20180425 1.0.0.18 Fixed: db.GetConnection wrong parameter;
//                   Added: Handling RequestParamList having ObjList = null.
//
// 20180927 1.0.0.19 Fixed: Send invitation to already existing users as well.
//
// 20181008 1.0.0.20 Replaced temporary Rackspace e-mail account with permanent
//                   no-reply.intellicenter@email3.pentair.com (smtp.mandrillapp.com).
//
// TODO:
//                   When THW tries to open duplicated connection, close existing connection and open new one
//                   (instead of returning 403);
//
//                   Handling WriteParamList can try to update [Installations].Name, which can fail.
//                   If it happened, the name remains updated on the THW and not updated on the server.
//                   A solution needed to keep it in sync.
//
//                   Maybe? Delete [UserNotifications] and all the code related. Clients?
//
//                   Maybe? AuthenticationManager, AccountController: UserId string -> Guid.
//
// 20190221 1.0.0.21 Added Note to email alert and changed invite subject
//                   Added ControlObjectsHistory table so ControlObjects only contains current state
//                   Commented out unused Entity objects and changed Entity calls to stored procedures
//                   Fixed RemoveAssociation method
//
// 20190709 1.0.0.22 Fixed API log file locking issue
//                   Added AccountController.GetVersionNumber()
//
// 20200630 1.0.0.23 Fixed empty alert emails (WebSocketClient_THW.cs)                 
//
[assembly: AssemblyVersion("1.0.0.23")]
[assembly: AssemblyFileVersion("1.0.0.23")]
