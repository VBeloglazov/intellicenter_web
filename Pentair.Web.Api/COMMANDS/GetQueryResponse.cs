﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Pentair.Web.Api
{
    public class GetQueryResponse : ResponseCommand
    {
        [JsonProperty("queryName", Required = Required.Always)]
        public string QueryName { get; private set; }

        [JsonProperty("answer", Required = Required.Always)]
        public object Answer { get; private set; }

        public GetQueryResponse(
            string command,
            string messageID,
            string response,
            string queryName,
            object answer
            )
            : base(command, messageID, response)
        {
            this.QueryName = queryName;
            this.Answer = answer;

        } // GetQueryResponse()

        /// <summary>
        /// Always returns an object, valid or not. If invalid, check the DeserializationError property value.
        /// </summary>
        public static new GetQueryResponse Deserialize(
            string serializedMessage)
        {
            GetQueryResponse command = null;

            try
            {
                command = JsonConvert.DeserializeObject<GetQueryResponse>(serializedMessage);
                command.IsValid = true;
            }
            catch (Exception e)
            {
                // Deserialization as GetQueryResponse failed

                StringBuilder sbError = new StringBuilder();

                sbError.Append("Cannot deserialize as GetQueryResponse. " + e.Message);

                // Try to get at least the base message data
                ResponseCommand baseCommand = ResponseCommand.Deserialize(serializedMessage);

                if (!baseCommand.IsValid)
                {
                    sbError.Append("\n" + baseCommand.DeserializationError);
                }

                command = new GetQueryResponse(
                    command: baseCommand.Command,
                    messageID: baseCommand.MessageID,
                    response: ResponseString.BadRequest,
                    queryName: String.Empty,
                    answer: String.Empty)
                {
                    IsValid = false,
                    DeserializationError = sbError.ToString()
                };
            }

            return command;

        } // Deserialize()

    } // class GetQueryResponse
}
