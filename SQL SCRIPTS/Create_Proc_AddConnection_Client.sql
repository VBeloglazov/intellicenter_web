-- =============================================================
--
-- Author:		Vladimir Beloglazov
-- Create date: 01/26/2018
--
-- Creates:
--
--		Table Type:			None
--
--		Stored Procedure:	PAddConnectionClient
--
-- =============================================================

USE PathfinderIntelliCenter
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (
	SELECT * FROM sys.objects
	WHERE object_id = OBJECT_ID(N'dbo.PAddConnectionClient')
	AND type in (N'P', N'PC')
)
DROP PROCEDURE dbo.PAddConnectionClient
GO

-- -------------------------------------------------------------
--
-- Returns [Connections] record just created.
--
-- -------------------------------------------------------------
CREATE PROCEDURE dbo.PAddConnectionClient
	 @hostMachine AS nvarchar(max)
	,@installationId AS int
	,@userId AS uniqueidentifier

AS
BEGIN

	SET NOCOUNT OFF

	-- Add [Connections] record, return new record
	INSERT INTO dbo.Connections (HostMachine, InstallationId, UserId)
	OUTPUT INSERTED.*
	VALUES (@hostMachine, @installationId, @userId)

END
GO
