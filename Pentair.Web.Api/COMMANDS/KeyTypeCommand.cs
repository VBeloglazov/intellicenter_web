﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Pentair.Web.Api
{
    public class KeyTypeCommand : CommandBase
    {
        [JsonProperty("objectList")]
        public List<KeyObjectType> ObjectList { get; set; }

        public KeyTypeCommand(
            string command,
            string messageID,
            List<KeyObjectType> objectList
            )
            : base(command, messageID)
        {
            this.ObjectList = objectList;
        }

    } // class KeyTypeCommand
}
