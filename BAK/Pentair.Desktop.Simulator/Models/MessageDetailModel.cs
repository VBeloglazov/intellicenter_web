﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Pentair.Domain.Client.Helpers;
using Pentair.Domain.Client.Helpers.Commands;

namespace Pentair.Desktop.Simulator.Models
{
    public class MessageDetailModel
    {
        /// <summary>
        /// Collection of log entries
        /// </summary>
        public ObservableCollection<MessageDetail> Messages { get; private set; }
        public bool Valid { get; private set; }
        /// <summary>
        /// Constructor
        /// </summary>
        public MessageDetailModel(string message)
        {
            Valid = true;
            var cmd = WebSocketMessage.ParseMessageType(message, EndpointType.Server);
            Messages = new ObservableCollection<MessageDetail>();
            switch (cmd.CommandType)
            {
                case CommandType.GetParamList:
                    var getParams = JsonConvert.DeserializeObject<GetParamList>(message);
                    var getParamItem = new MessageDetail { Command = cmd.CommandType.ToString() };
                    foreach (var circuit in getParams.ObjectList)
                    {
                        getParamItem.Object = circuit.ObjNam;
                        foreach (var entry in circuit.Keys)
                        {
                            getParamItem.Parameter = entry;
                            Messages.Add(getParamItem);
                            getParamItem = new MessageDetail();
                        }
                        getParamItem = new MessageDetail();
                    }
                    break;
                case CommandType.RequestParamList:
                    var requestParams = JsonConvert.DeserializeObject<RequestParamList>(message);
                    var requestParamItem = new MessageDetail { Command = cmd.CommandType.ToString() };
                    foreach (var circuit in requestParams.ObjectList)
                    {
                        requestParamItem.Object = circuit.ObjNam;
                        foreach (var entry in circuit.Keys)
                        {
                            requestParamItem.Parameter = entry;
                            Messages.Add(requestParamItem);
                            requestParamItem = new MessageDetail();
                        }
                        requestParamItem = new MessageDetail();
                    }
                    break;
                case CommandType.GetParamListResponse:
                    var sendParams = JsonConvert.DeserializeObject<GetParamListResponse>(message);
                    var sendParamItem = new MessageDetail { Command = cmd.CommandType.ToString() };
                    if (0 == sendParams.ObjectList.Count)
                    {
                        sendParamItem.Parameter = "Empty list";
                        Messages.Add(sendParamItem);
                    }
                    else
                    {
                        foreach (var circuit in sendParams.ObjectList)
                        {
                            sendParamItem.Object = circuit.ObjNam;
                            foreach (var entry in circuit.Parameters)
                            {
                                sendParamItem.Parameter = entry.Key + " : " + entry.Value;
                                Messages.Add(sendParamItem);
                                sendParamItem = new MessageDetail();
                            }
                            sendParamItem = new MessageDetail();
                        }
                    }
                    break;
                case CommandType.WriteParamList:
                    var writeParams = JsonConvert.DeserializeObject<WriteParamListResponse>(message);
                    var writeParamItem = new MessageDetail { Command = cmd.CommandType.ToString() };
                    foreach (var item in writeParams.ObjectList)
                    {
                        foreach (var circuit in item.Changes)
                        {
                            writeParamItem.Object = circuit.ObjNam;
                            foreach (var entry in circuit.Parameters)
                            {
                                writeParamItem.Parameter = entry.Key + " : " + entry.Value;
                                Messages.Add(writeParamItem);
                                writeParamItem = new MessageDetail();
                            }
                        }
                        writeParamItem = new MessageDetail();
                    }
                    break;
                case CommandType.SetParamList:
                    var setParams = JsonConvert.DeserializeObject<SetParamListResponse>(message);
                    var setParamItem = new MessageDetail { Command = cmd.CommandType.ToString() };
                    if (null != setParams && null != setParamItem && null != setParams.ObjectList)
                    {
                        foreach (var circuit in setParams.ObjectList)
                        {
                            setParamItem.Object = circuit.ObjNam;
                            foreach (var entry in circuit.Parameters)
                            {
                                setParamItem.Parameter = entry.Key + " : " + entry.Value;
                                Messages.Add(setParamItem);
                                setParamItem = new MessageDetail();
                            }
                            setParamItem = new MessageDetail();
                        }
                    }
                    break;
                case CommandType.NotifyList:
                    var notifyList = JsonConvert.DeserializeObject<NotifyList>(message);
                    var notifyListItem = new MessageDetail { Command = cmd.CommandType.ToString() };
                    foreach (var circuit in notifyList.ObjectList)
                    {
                        notifyListItem.Object = circuit.ObjNam;
                        foreach (var entry in circuit.Parameters)
                        {
                            notifyListItem.Parameter = entry.Key + " : " + entry.Value;
                            Messages.Add(notifyListItem);
                            notifyListItem = new MessageDetail();
                        }
                        notifyListItem = new MessageDetail();
                    }
                    break;
                case CommandType.InviteUser:
                    var inviteList = JsonConvert.DeserializeObject<KeyValueCommand>(message);
                    var inviteItem = new MessageDetail { Command = cmd.CommandType.ToString() };
                    foreach (var item in inviteList.Parameters)
                    {
                        inviteItem.Parameter = item.Key + " : " + item.Value;
                        Messages.Add(inviteItem);
                        inviteItem = new MessageDetail();
                    }
                    break;
                case CommandType.CreateObject:
                    var createList = JsonConvert.DeserializeObject<CreateObjectFromParam>(message);
                    var createItem = new MessageDetail() { Command = cmd.CommandType.ToString() };
                    foreach (var item in createList.ObjectList)
                    {
                        createItem.Object = item.ObjTyp;
                        foreach (var entry in item.Parameters)
                        {
                            createItem.Parameter = entry.Key + " : " + entry.Value;
                            Messages.Add(createItem);
                            createItem = new MessageDetail();
                        }
                    }
                    break;
                case CommandType.ObjectCreated:
                    var notifyCreateList = JsonConvert.DeserializeObject<ObjectCreatedResponse>(message);
                    var notifyCreate = new MessageDetail
                    {
                        Command = cmd.CommandType.ToString(),
                        Parameter = notifyCreateList.ObjNam
                    };
                    Messages.Add(notifyCreate);
                    break;
                case CommandType.SuccessMessage:
                case CommandType.ErrorMessage:
                case CommandType.ClearParam:
                    Messages.Add(new MessageDetail() { Command = cmd.CommandType.ToString() });
                    break;
                default:
                    Valid = false;
                    break;
            }
        }
    }

    public class MessageDetail
    {
        public string Command { get; set; }
        public string Object { get; set; }
        public string Parameter { get; set; }
    }
}
