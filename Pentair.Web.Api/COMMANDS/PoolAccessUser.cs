﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Pentair.Web.Api
{
    public class PoolAccessUser : KeyValueCommand
    {
        public PoolAccessUser(
            string command,
            string messageID,
            Dictionary<string, string> param
            )
            : base(command, messageID, param)
        {
        }

    } // class PoolAccessUser
}
