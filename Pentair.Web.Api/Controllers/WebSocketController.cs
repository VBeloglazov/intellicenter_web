﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

using Microsoft.AspNet.Identity;
using Microsoft.Web.WebSockets;

using Newtonsoft.Json;

using SERVICE_SHARED;

namespace Pentair.Web.Api
{
    public class WebSocketController : ApiController
    {
        // VB Be careful with 503 ServiceUnavailable response: it might make LB to exclude the server from the pool.

        private const string EVENT_SOURCE = "WebSocketController";

        /// <summary>
        /// THW connection: GET /Service/api/websocket
        /// </summary>
        [AllowAnonymous]
        public HttpResponseMessage Get()
        {
            long ticksStart, ticksEnd;

            NativeMethods.QueryPerformanceCounter(out ticksStart);

            try
            {
                if (HttpContext.Current.IsWebSocketRequest || HttpContext.Current.IsWebSocketRequestUpgrading)
                {
                    if (EsbClientConnection.IsEsbRequired && !EsbClientConnection.IsEsbConnected)
                    {
                        return Request.CreateErrorResponse(
                            HttpStatusCode.ServiceUnavailable, // 503
                            "Service temporarily unavailable"
                        );
                    }

                    WebSocketClient_THW client = new WebSocketClient_THW();

                    HttpContext.Current.AcceptWebSocketRequest(client);

                    return Request.CreateResponse(HttpStatusCode.SwitchingProtocols);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest);
                }
            }
            catch (Exception e)
            {
                LogManager.LogErrorAsync(
                    source: EVENT_SOURCE,
                    eventBriefDescription: "THW incoming connection rejected",
                    installationId: 0,
                    userId: Guid.Empty,
                    connectionId: Guid.Empty,
                    eventFullDescription:
                        ExceptionManager.CustomizeException(e).ErrorMessage(
                            errorHeader: null,
                            addStackTrace: true
                        )
                );

                return Request.CreateResponse(HttpStatusCode.Unauthorized);
            }
            finally
            {
                NativeMethods.QueryPerformanceCounter(out ticksEnd);

                double elapsedSec = (double)(ticksEnd - ticksStart) / WebApiApplication.counterFrequency;

                WebApiApplication.PerformanceCountersMgr.SetCounterValue(
                    WebApiApplication.PerformanceCountersMgr.CounterLatencyConnect,
                    (long)(elapsedSec * 1000.0 + 0.5)
                );
            }
        
        } // Get()

        /// <summary>
        /// Client connection: /Service/api/websocket/<id>
        /// </summary>
        [Authorize]
        public HttpResponseMessage Get(
            string id)
        {
            long ticksStart, ticksEnd;

            NativeMethods.QueryPerformanceCounter(out ticksStart);

            try
            {
                if (HttpContext.Current.IsWebSocketRequest || HttpContext.Current.IsWebSocketRequestUpgrading)
                {
                    int installationId = -1;
                    if (int.TryParse(id, out installationId))
                    {
                        Connection selectedPropertyConnection = null;

                        using (DbContextPentair db = DbContextPentair.New())
                        {
                            selectedPropertyConnection = db.GetConnectionThw(installationId);

                        } // using (DbContextConnectionsAndInstallations db)

                        if (selectedPropertyConnection == null)
                            return Request.CreateErrorResponse(HttpStatusCode.Gone, "Property offline or not registered");

                        if (selectedPropertyConnection.HostMachine != WebApiApplication.HOSTNAME &&
                            !EsbClientConnection.IsEsbConnected)
                        {
                            LogManager.LogErrorAsync(
                                source: EVENT_SOURCE,
                                eventBriefDescription: "ESB not connected",
                                installationId: installationId,
                                userId: Guid.Empty,
                                connectionId: Guid.Empty,
                                eventFullDescription:
                                    String.Format("Incoming Client cannot be served: This server is {0} and THW host is {1}",
                                        WebApiApplication.HOSTNAME,
                                        selectedPropertyConnection.HostMachine
                                    )
                            );

                            return Request.CreateErrorResponse(HttpStatusCode.ServiceUnavailable, "Service temporarily unavailable");
                        }

                        WebSocketClient_Client client = new WebSocketClient_Client(
                            installationId,
                            User.Identity.GetOwinUserId(),
                            User.Identity.Name
                        );

                        HttpContext.Current.AcceptWebSocketRequest(client);

                        return Request.CreateResponse(HttpStatusCode.SwitchingProtocols);
                    }
                }

                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Bad Request");
            }
            catch (Exception e)
            {
                LogManager.LogErrorAsync(
                    source: EVENT_SOURCE,
                    eventBriefDescription: "Client incoming connection rejected",
                    installationId: 0,
                    userId: Guid.Empty,
                    connectionId: Guid.Empty,
                    eventFullDescription:
                        ExceptionManager.CustomizeException(e).ErrorMessage(
                            errorHeader: null,
                            addStackTrace: true
                        )
                );

                return Request.CreateResponse(HttpStatusCode.Unauthorized);
            }
            finally
            {
                NativeMethods.QueryPerformanceCounter(out ticksEnd);

                double elapsedSec = (double)(ticksEnd - ticksStart) / WebApiApplication.counterFrequency;

                WebApiApplication.PerformanceCountersMgr.SetCounterValue(
                    WebApiApplication.PerformanceCountersMgr.CounterLatencyConnect,
                    (long)(elapsedSec * 1000.0 + 0.5)
                );
            }

        } // Get(string)

    } // class WebSocketController
}
