USE [PathfinderIntellicenter]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (
	SELECT * FROM sys.objects
	WHERE object_id = OBJECT_ID(N'dbo.pr_GetPostalCodeInfo')
	AND type in (N'P', N'PC')
)
DROP PROCEDURE dbo.pr_GetPostalCodeInfo
GO

-- =============================================
-- Author			:	Wayne Hui
-- Create date		:	03/08/2019
-- Parameters		:
--	@PostalCode		:	Postal code to search
-- Description		:	Get the postal code city/state information
--
-- Modified By			Date		Description
--	Wayne Hui			03/25/2020	Added NOT FOUND
-- =============================================
CREATE PROCEDURE [dbo].[pr_GetPostalCodeInfo]
	@PostalCode		NVARCHAR(20)
AS
BEGIN

	SET NOCOUNT OFF

----TEST
--DECLARE @PostalCode		NVARCHAR(20)

--SET @PostalCode	= '27560'

----END TEST

	IF ( SELECT COUNT(*) FROM [dbo].[PostalCodes] WHERE PostalCode = @PostalCode ) > 0
	BEGIN
		SELECT 
			[CountryName]
			,[CityName]
			,[ProvinceAbbr]
			,[TimeZone]
			,[UTC]
			,[DST]
			,[Latitude]
			,[Longitude]
			,[PostalCode]
		FROM 
			[dbo].[PostalCodes]
		WHERE
			PostalCode = @PostalCode
	END
	ELSE
	BEGIN
		SELECT 
			CAST('' AS NVARCHAR(100)) AS [CountryName]
			, CAST('' AS NVARCHAR(100)) AS [CityName]
			, CAST('' AS NVARCHAR(20)) AS [ProvinceAbbr]
			, CAST('' AS NVARCHAR(20)) AS [TimeZone]
			, CAST(0 AS DECIMAL(4,1)) AS [UTC]
			, CAST('' AS NVARCHAR(1)) AS [DST]
			, CAST(0 AS DECIMAL(12,6)) AS [Latitude]
			, CAST(0 AS DECIMAL(12,6)) AS [Longitude]
			, CAST('NOT FOUND' AS NVARCHAR(20)) AS [PostalCode]
	END


END

GO


--GRANT EXECUTE ON [dbo].[pr_GetPostalCodeInfo] TO PUBLIC
--GO
