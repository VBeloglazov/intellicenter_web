﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pentair.Domain.Client.Helpers.Entity
{
    public class InstallationView
    {
        [Key]
        [Column(Order = 1)]
        public int INSTALLATION { get; set; }

        [Key]
        [Column(Order = 2)]
        [MaxLength(16)]
        public string OBJNAM { get; set; }
        public string ADDRESS { get; set; }
        public string CITY { get; set; }
        public string STATE { get; set; }
        public string ZIP { get; set; }
        public string COUNTRY { get; set; }
        public string TIMZON { get; set; }
        public string NAME { get; set; }
    }
}
