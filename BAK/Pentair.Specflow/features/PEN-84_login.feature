﻿Feature: Login (PEN-84)
As a user
I want to login Remotely
So that I am connected to my system

#this background needs a little thought
#Background the user is on a network
#And there is no IntelliCenter on the Network

#Scenario: REMOTE LOGIN -- Valid
#  Given the user submits a valid login
#  Then the user goes to the status screen

Scenario: REMOTE LOGIN -- Valid
  Given the user goes to the login page 
  And the user enters "user@pentair.com" into the "userName" field
  And the user enters "Password" into the "password" field
  And the user clicks the "submit" button
  Then the user goes to the status screen

Scenario: REMOTE LOGIN -- Invalid
Given the user goes to the login page
  And the user enters "user@pentair.com" into the "userName" field
  And the user enters "??ImGuessingThePassword!!" into the "password" field
  And the user clicks the "submit" button
  Then an error message is displayed

Scenario: REMEMBER LOGIN
Given the user goes to the login page 
  And the user enters "user@pentair.com" into the "userName" field
  And the user enters "Password" into the "password" field
  And the remember checkbox was checked
  And the user clicks the "submit" button
  When the user restarts the app
  Then the user goes to the status screen

Scenario: DO NOT REMEMBER LOGIN 
Given the user goes to the login page
  And the user enters "user@pentair.com" into the "userName" field
  And the user enters "Password" into the "password" field
  And the remember checkbox was NOT checked
  And the user clicks the "submit" button
  When the user restarts the app
  Then the user goes to the login screen

Scenario: DO NOT REMEMBER BAD CREDENTIALS
Given the user goes to the login page
 And the user enters "user@pentair.com" into the "userName" field
  And the user enters "??ImGuessingThePassword!!" into the "password" field
  And the remember checkbox was checked
  And the user clicks the "submit" button
  Then the bad login credentials are NOT remembered

Scenario: LOGOUT
Given the user is logged in
  When the user clicks the "logout" button 
  Then the user has logged out

Scenario: LOGOUT REMEMBERED
Given no user is logged in 
  When the user restarts the app
  Then the user goes to the login screen
