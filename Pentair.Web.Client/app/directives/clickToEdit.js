﻿var PWC = PWC || {};
PWC.directive('clickToEdit', function () {
    return {
        restrict: 'EA',
        replace: true,
        scope: {
            model: '=clickToEdit',
            ngChange: '&',
            minValue: '@',
            maxValue: '@',
            maxLength: '@',
            disabled: '='
        },
        template: '<div><span class="duration">{{durationModel ? durationModel : "--"}}</span><input ng-numbers-only ng-blur="onExit()" ng-enter="onExit()" ng-show="showInput" ng-model="duration" maxlength="{{maxLength}}" max-value="{{maxValue}}" min-value="{{minValue}}" type="text"></div>',
        link: function (scope, element, attrs) {
            if (!attrs.disabled)
                attrs.disabled = false;
            var oldVal = 0;
            var maxValue = Number(attrs.maxValue || 0);
            var minValue = Number(attrs.minValue || 0);
            scope.maxLength = Number(attrs.maxLength || 0);
            scope.showInput = false;

            //if input box is hidden show box and set focus
            element.on('click', function () {
                if (scope.enabled && angular.isDefined(scope.showInput) && scope.showInput == false) {
                    oldVal = Number((scope.durationModel).NUMBER);
                    scope.duration = oldVal;
                    scope.showInput = true;
                    scope.$apply(scope.showInput);
                    element[0].lastChild.focus();
                }
            });

            scope.onExit = function () {
                scope.showInput = false;
                var duration = scope.duration;

                if (duration < minValue && minValue > 0) {
                    duration = minValue;
                }
                if (duration > maxValue && maxValue > 0) {
                    duration = maxValue;
                }
                if (oldVal != duration) {
                    scope.onTextInput({ obj: scope.notificationObject, value: Math.round(duration) });
                    oldVal = duration;
                }
            }

            //update UI value if Model changes
            scope.$watch('durationModel', function () {
                if (scope.durationModel) {
                    oldVal = Number(scope.durationModel);
                    scope.duraton = oldVal;
                    scope.enabled = oldVal !== 0;
                } else {
                    scope.enabled = false;
                }
            });
        }
    };
});
