-- =============================================================
--
-- Author:		Vladimir Beloglazov
-- Create date: 01/29/2018
--
-- Creates:
--
--		Table Type:			None
--
--		Stored Procedure:	PScheduleNotifications
--
-- =============================================================

USE PathfinderIntelliCenter
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (
	SELECT * FROM sys.objects
	WHERE object_id = OBJECT_ID(N'dbo.PScheduleNotifications')
	AND type in (N'P', N'PC')
)
DROP PROCEDURE dbo.PScheduleNotifications
GO

CREATE PROCEDURE dbo.PScheduleNotifications
	@subscriptions AS TSubscription READONLY

AS
BEGIN

	SET NOCOUNT OFF

	INSERT INTO Subscriptions (ClientId, PropertyId, ObjNam, ObjParm)
	SELECT DISTINCT * FROM @subscriptions S
	WHERE NOT EXISTS (
		SELECT 1 FROM Subscriptions
		WHERE ClientId = S.ClientId
		AND PropertyId = S.PropertyId
		AND ObjNam = S.ObjNam
		AND ObjParm = S.ObjParm
	)
 
END
GO
