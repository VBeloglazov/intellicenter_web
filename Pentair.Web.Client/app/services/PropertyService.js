var PWC = PWC || {};


PWC.factory('PropertyService', [
    '$filter', '$location', '$q', '$rootScope', '$timeout', 'SocketService', 'Enumerations', 'LocalStorageService', 'AppService', 'PropertyModel', 'MessageProcessor', 'AuthService',
    function($filter, $location, $q, $rootScope, $timeout, socketService, enumerations, localStorageService, appService, propertyModel, messageProcessor, authService) {
        'use strict';
        var factory = {};
        var socket = socketService.socket;
        var reconnecting = {
            attempts: 0,
            multiple: 1,
            active: false
        };
        var autoRetry = true;

        //public methods
        factory.ToggleCircuit = function(circuit, value) {
            if (circuit && value)
                return factory.SetValue(circuit.OBJNAM, 'STATUS', value);
        };

        
        factory.setTokenCircuit = function (circuit, token, value) {
            console.log("circuit name : " + circuit.OBJNAM+ " STATUS = " +value)
            if (circuit && value)
                return factory.SetValue(circuit.OBJNAM, token, value);
        };


        //public methods group
        factory.ToggleCircuitGroup = function (circuit, value) {
            if (circuit && value)
                return factory.SetValue(circuit, 'STATUS', value);
        };

        //public methods
        factory.ToggleBody = function(circuit, value) {
            if (circuit && value) {
                var objnam = circuit.OBJNAM;
                var args = {}
                args[objnam] = value;
                var message = {
                    command: "SETCOMMAND",
                    method: "SETBODYSTATE",
                    arguments: args
                };
                return socketService.sendMessage(message);
            }
        };

        factory.SetParamList = function(objectList) {
          var message = {
              command: 'SetParamList',
              objectList: objectList
          };
          return socketService.sendMessage(message);
        };

        factory.getActiveStatusMessages = function() {
            var message = {
                command: 'GetQuery',
                queryName: 'GetActiveStatusMessages',
                arguments: ''
            };
            return socketService.sendMessage(message);
        };

        factory.GetHistory = function(start, end) {
                var message = {
                    command: 'ReadHistoryMessage',
                    timeStart: start,
                    timeEnd: end
                };
                return socketService.sendMessage(message);
        };



        factory.DismissAllAlerts = function() {
            var message = {
                command: "SETCOMMAND",
                method: "CLEARSTATUSMESSAGES",
                arguments: {}
            };
            return socketService.sendMessage(message);
        };

        factory.IncreaseValue = function(obj, prop, max) {
            if (PWC.isValidObject(obj, [prop, 'OBJNAM'])) {
                var target = obj[prop];
                var value = Number(target) || Number(target.replace(/\D/g, ''));
                value++;
                if (value <= max)
                    return factory.SetValue(obj.OBJNAM, prop, value);
            }
        };

        factory.DecreaseValue = function(obj, prop, min) {
            if (PWC.isValidObject(obj, [prop, 'OBJNAM'])) {
                var target = obj[prop];
                var value = Number(target) || Number(target.replace(/\D/g, ''));
                value--;
                if (value >= min)
                    return factory.SetValue(obj.OBJNAM, prop, value);
            }
        };

        factory.SetValue = function(objName, param, value) {
            if (objName && param) {
                return sendSetParamList(objName, param, value);
            }
        };

        factory.GetHeatModes = function(body) {
            return $filter('heatModes')(body);
        };

        factory.SetHeatModes = function(objNam, heatMode) {
            var body = propertyModel.Objects[objNam];
            if (!PWC.isEmptyObject(body) && heatMode) {
                return sendSetParamList(objNam, "HEATER", $filter('modeToHeater')(body, heatMode));
            }
        };

        factory.SetAllLightsOff = function() {
            return sendSetParamList(enumerations.AllLightsOffObj, "STATUS", "OFF");
        };

        factory.SetAllLightsOn = function () {            
            return sendSetParamList(enumerations.AllLightsOnObj, "STATUS", "ON");
        };

        factory.GetParamListByOBJTYP = function(params, objType) {
            if (params && objType) {
                var message = {
                    command: "GETPARAMLIST",
                    condition: "OBJTYP=" + objType,
                    objectList: [{
                        objnam: "ALL",
                        keys: [params]
                    }]
                };
                return socketService.sendMessage(message);
            }
        };

        factory.GetParamListBySUBTYP = function(params, objType, subType) {
            if (params && objType && subType) {
                var message = {
                    command: "GETPARAMLIST",
                    condition: "OBJTYP=" + objType + " & " + "SUBTYP=" + subType,
                    objectList: [{
                        objnam: "ALL",
                        keys: [params]
                    }]
                };
                return socketService.sendMessage(message);
            }
        };

        factory.GetParamListByOBJTYPOnly = function (params, objTyp, objnam) {
            if (params && objTyp) {
                var message = {
                    command: "GETPARAMLIST",
                    condition: "PARENT=" + objnam + " & " + "OBJTYP=" + objTyp,
                    objectList: [{
                        objnam: "ALL",
                        keys: [params]
                    }]
                };
                return socketService.sendMessage(message);
            }
        };

        factory.GetParamListByObject = function(params, objName) {
            if (params && objName) {
                var message = {
                    command: "GETPARAMLIST",
                    condition: "",
                    objectList: [{
                        objnam: objName,
                        keys: [params]
                    }]
                };
                return socketService.sendMessage(message);
            }
        };

        factory.GetPostalCodeData = function(zipCode) {
            var message = {
                command: "ReadPostalCodeInfoMessage",
                postalCode: zipCode
            };
            return socketService.sendMessage(message);
        };

        
        factory.GetCityCodeData = function (city,state) {
            var message = {
                command: "ReadCityNameInfoMessage",                
                City:city,
                State:state
            };
            return socketService.sendMessage(message);
        };

        factory.SetACLSHOMNU = function() {
          this.GetParamListByObject('SHOMNU',propertyModel.ACL.OBJNAM).then(function(result) {
              propertyModel.ACL.SHOMNU = result.objectList[0].params.SHOMNU;
              authService.authorization.SHOMNU = propertyModel.ACL.SHOMNU;
              authService.authorization.securityEnabled = propertyModel.SecurityEnabled;
          });
        };

        factory.SetLightStage = function (objectName, mode,state) {
            if (objectName && mode && state) {
                var message = {
                    command: "SETPARAMLIST",
                    objectList: []
                };

                if (mode === 'SWIM') {
                    var newObj = {
                        objnam: objectName,
                        params: { SWIM : state }
                    };
                    message.objectList.push(newObj);
                }
                if (mode === 'SYNC') {
                    var newObj = {
                        objnam: objectName,
                        params: { SYNC: state }
                    };
                    message.objectList.push(newObj);
                }
                if (mode === 'SET') {
                    var newObj = {
                        objnam: objectName,
                        params: { SET: state }
                    };
                    message.objectList.push(newObj);
                }


                if (message.objectList.length > 0)
                    return socketService.sendMessage(message);
            }
        };
        //deprecate
        factory.SetLightMode = function(lightTypes, mode) {
            if (typeof lightTypes === 'object' && lightTypes.length > 0 && mode) {
                var message = {
                    command: "SETPARAMLIST",
                    objectList: []
                };
                for (var i = 0; i < propertyModel.Circuits.length; i++) {
                    var circuit = propertyModel.Circuits[i];
                    if (lightTypes.indexOf(circuit.SUBTYP) !== -1) {

                        var newObj = {
                            objnam: circuit.OBJNAM,
                            params: { ACT: mode }
                        };

                        message.objectList.push(newObj);
                    }
                }

                if (message.objectList.length > 0)
                    return socketService.sendMessage(message);
            }
        };

        factory.CancelDelay = function(objNam) {
            if (objNam) {
                sendSetParamList(objNam, 'DLYCNCL', '');
            }
        };

        factory.GetDelayByOBJNAM = function(objName) {
            if (objName) {
                var message = {
                    command: "GETPARAMLIST",
                    condition: "",
                    objectList: [{
                        objnam: objName,
                        keys: ["TIMOUT : SOURCE"]
                    }]
                };
                return socketService.sendMessage(message);
            }
        };


        factory.logsCommands = function (command) {
            if (command) {
                var message = {
                    command: "GETQUERY",
                    queryName: command                    
                };
                return socketService.sendMessage(message);
            }
        };

        factory.InviteUser = function(email, group) {
            if (email && group) {
                var message = {
                    command: "INVITEUSER",
                    params: {
                        EMAIL: email,
                        PERMISSIONGROUP: group
                    }
                };
                return socketService.sendMessage(message);
            }
        };

        factory.ResendInvite = function(user) {
            if (user) {
                var message = {
                    command: "RESENDINVITE",
                    params: {
                        ID: user.id
                    }
                };
                return socketService.sendMessage(message);
            }
        };

        factory.CreateNewObject = function(objtyp, params) {
            var message = {
                command: "CREATEOBJECT",
                objectList: [{
                    objtyp: objtyp,
                    params: params
                }],
            };
            return socketService.sendMessage(message);
        };

        factory.SetParams = function(objNam, params) {
            if (objNam && !PWC.isEmptyObject(params)) {
                var message = {
                    command: "SETPARAMLIST",
                    objectList: [{
                        objnam: objNam,
                        params: params
                    }]
                };
                return socketService.sendMessage(message);
            }
        };

        factory.SetParamsList = function(objectList) {
            if (objNam && !PWC.isEmptyObject(params)) {
                var message = {
                    command: "SETPARAMLIST",
                    objectList: objectList
                };
                return socketService.sendMessage(message);
            }
        };

        factory.DeleteUser = function(email, status, notify) {
            if (email && status) {
                var message = {
                    command: "DELETEUSER",
                    params: {
                        EMAIL: email,
                        STATUS: status,
                        NOTIFY: notify
                    }
                };
                return socketService.sendMessage(message);
            }
        };

        factory.EditUser = function(email, level) {
            if (email && level) {
                var message = {
                    command: "EDITUSER",
                    params: {
                        EMAIL: email,
                        PERMISSIONGROUP: level
                    }
                };
                return socketService.sendMessage(message);
            }
        };

        factory.GetUserList = function(filter, searchTerm, sortField, sortDirection) {
            var message = {
                command: "USERLIST",
                params: {
                    FILTER: filter,
                    SORTFIELD: sortField,
                    SORTDIRECTION: sortDirection,
                    SEARCHTERM: searchTerm
                }
            };
            return socketService.sendMessage(message);
        };

        factory.GetSecurityTokens = function() {
            return factory.GetParamListByOBJTYP("OBJNAM : SNAME : ENABLE : PASSWRD : SHOMNU : TIMOUT", "PERMIT").then(messageProcessor.ProcessSecurityTokens);
        };

        factory.SaveSecurityTokens = function(guestParms, adminParms) {
            factory.SetParams('U0000', guestParms);
            factory.SetParams('UFFFE', adminParms).then(function() {
                factory.GetSecurityTokens();
            });
        };

        factory.SaveSecurityTokens = function(permitParamsList) {
          var defer = $q.defer();
          angular.forEach(permitParamsList, function(permitParamsItem, idx, array) {
              var tempParams = {
                  SNAME: permitParamsItem.SNAME,
                  ENABLE: permitParamsItem.ENABLE,
                  PASSWRD: permitParamsItem.PASSWRD,
                  SHOMNU: permitParamsItem.SHOMNU,
                  TIMOUT: permitParamsItem.TIMOUT
              };
              if(permitParamsItem.OBJNAM && permitParamsItem.OBJNAM === 'UFFFE')
                  delete tempParams.SHOMNU;
              // send password for guess
              if (!permitParamsItem.OBJNAM || (permitParamsItem.OBJNAM !== 'UFFFE' && permitParamsItem.OBJNAM !== 'U0000'))
                  delete tempParams.ENABLE;
              if(permitParamsItem.OBJNAM && permitParamsItem.OBJNAM === 'U0000')
                  delete tempParams.PASSWRD;
              if(permitParamsItem.OBJNAM && (permitParamsItem.OBJNAM === 'UFFFE' || permitParamsItem === 'U0000'))
                  delete tempParams.SNAME;
              if(idx === array.length-1) {
                if(permitParamsItem.OBJNAM) {
                  factory.SetParams(permitParamsItem.OBJNAM, tempParams).then(function () {
                    factory.GetSecurityTokens().then(function () {
                      defer.resolve();
                    });
                  });
                } else {
                  //TODO
                  factory.CreateNewObject('PERMIT', tempParams).then(function() {
                      factory.GetSecurityTokens().then(function () {
                        defer.resolve();
                      });
                  });
                }
              } else {
                if(permitParamsItem.OBJNAM) {
                  factory.SetParams(permitParamsItem.OBJNAM, tempParams);
                } else {
                  factory.CreateNewObject('PERMIT', tempParams);
                }
              }
          });
          return defer.promise;
        };

        factory.ReleaseTimeUpdates = function() {
            var subscription = [{
                objnam: "_C10C",
                keys: ["DAY", "MIN", "OFFSET", "SNAME", "TIMZON", "SRIS", "SSET"]
            }, {
                objnam: "_C105",
                keys: ["SOURCE"]
            }];
            return socketService.unSubscribe(subscription);
        };

        //factory.GetPanels = function () {
        //    return $q.all([
        //        factory.GetParamListByOBJTYP("OBJNAM : LISTORD : SUBTYP : HNAME : SNAME", "PANEL").then(messageProcessor.ProcessPanels),
        //        factory.GetParamListByOBJTYP("OBJNAM : OBJTYP : SUBTYP : SNAME : LISTORD : PORT : PARENT : READY", "MODULE").then(messageProcessor.ProcessModules),
        //        factory.GetParamListByOBJTYP("OBJNAM : OBJTYP : SUBTYP : SNAME : PANEL : PARENT : RUNON : CIRCUIT : STATUS : SOURCE : LISTORD", "RLY").then(messageProcessor.ProcessRelays)
        //    ]);
        //}

        factory.GetUserNotifications = function() {
            var message = {
                command: "USERNOTIFICATIONLIST"
            };
            return socketService.sendMessage(message);
        };
        factory.ReadStatusMessage = function() {
            var message = {
                command: "READSTATUSMESSAGE",
                timeSince: "0"
            };
            return socketService.sendMessage(message);
        };

        factory.SaveUserNotification = function(obj, value) {
            var params = {
                id: obj.id,
                duration: value,
                type: obj.type
            };
            var message = {
                command: "EDITNOTIFICATION",
                objectList: [{
                    objnam: obj.name,
                    params: params
                }]
            };
            return socketService.sendMessage(message).then(function() {
                propertyModel.UserNotifications[obj.name].duration = value;
            });

        };

        //TODO:No need for extra deferred object
        factory.FetchInstallations = function() {
            var deferred = $q.defer();
            appService.Installations().then(function(result) {
                propertyModel.Installations = result.data;
                deferred.resolve();
            });
            return deferred.promise;
        };

        factory.FetchInstallationsWithDetails = function(pageSize, page, term, filter) {
            appService.InstallationsWithDetails(pageSize, page, term, filter).then(function(result) {
                propertyModel.Installations = result.data;
                $rootScope.$broadcast('PropertyListUpdates', result.data);
            });
        }

        factory.RefreshLightGroups = function() {
            propertyModel.CircuitGroups = [];
            return factory.GetParamListBySUBTYP("OBJNAM : OBJTYP : SNAME : SHOMNU : STATUS : USAGE : SUBTYP : SOURCE : TIMOUT : FREEZE : CHILD", "CIRCUIT", "LITSHO", "TIME", "DNTSTP","USE")
        }

        factory.GetSchedules = function () {
            
            propertyModel.Schedules = [];
            return factory.GetParamListByOBJTYP("OBJNAM : OBJTYP : LISTORD : CIRCUIT : SNAME : DAY : SINGLE : START : " +
                "TIME : STOP : TIMOUT : GROUP : HEATER : COOLING : LOTMP : SMTSRT : VACFLO : STATUS : DNTSTP : ACT : MODE", "SCHED")
        };

        factory.GetLightColor = function (obj) {                      
            return factory.GetParamListByObject("ACT : USE", obj);          
        };
        
        factory.GetParamsCircuit = function (obj) {            
            return factory.GetParamListByObject("SNAME : SUBTYP : FREEZE :TIME :DNTSTP : FEATR", obj);
        };

        factory.GetValvesParams = function (obj) {
            return factory.GetParamListByObject("ACT : USE", obj);
        };

        factory.GetAlarmsParams = function (obj) {
            return factory.GetParamListByObject("SHOMNU", obj);
        };

        // TODO This needs to be redone.
        factory.GetSystemInfo = function() {
            var message = {
                command: "SYSTEMINFO"
            };
            return socketService.sendMessage(message);
        };

        // TODO This needs to be redone.
        factory.SetSystemInfo = function(installation, params) {
            var message = {
                command: "SAVESYSTEMINFO",
                objectList: [{
                    objnam: installation,
                    params: params
                }]
            };
            return socketService.sendMessage(message);
        };

        factory.Reset = function() {
            if (!PWC.isEmptyObject(socket) && socket.readyState.toString() === '1') {
                socketService.clearSubscriptions();
                socket.close();
            } else {
                propertyModel.Clear();
            }
        };

        factory.ChangeConnection = function(selection) {
            factory.Reset();
            factory.Connect(selection);
        };

        // set cookie on with duration in days
        factory.setCookieDays = function(cname,cvalue,exdays){
            var d = new Date()
            // adding exdays in m
            d.setTime(d.getTime() + (exdays*24*60*60*1000))
            var expires = "expires="+ d.toUTCString();
            document.cookie = cname + "=" + cvalue + ";" + expires + "path=/";
        }

        // set cookie name and value
        factory.setCookie = function(cname,cvalue){            
            document.cookie = cname + "=" + cvalue + ";" 
        }

        // retrieve cookie
        factory.getCookie = function(cname){
            var name = cname + "=";
            var decodedCookie = decodeURIComponent(document.cookie)
            var ca = decodedCookie.split(";")
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i]
                while (c.charAt(0) == ' ') {
                    c=c.substring(1)
                }
                if (c.indexOf(name) == 0)
                    return c.substring(name.length,c.length)
            }
            
            return ""
        }

        // set cookie on/off
        factory.setCookieOn = function (cookie, status) {
            if (status === true)
                factory.setCookie(cookie, "ON")
            else
                factory.setCookie(cookie,"OFF")
        }

        factory.isCookieOn = function (cookie, default_value) {
            // check if cookie is available
            var c = factory.getCookie(cookie)
            if (c === "")
                return default_value;   // return default value
            // check if is ON status
            if ( c === "ON" )
                return true
            else
                return false
        }


        factory.Connect = function(selection) {
            if (selection)
                propertyModel.SelectedInstallation = selection;
            if (propertyModel.SelectedInstallation && '0' !== propertyModel.SelectedInstallation) {

                propertyModel.Busy = true;

                factory.Reset();

                if (!reconnecting.active) {
                    propertyModel.ConnectionStatus = 'Connecting...';
                }

                //clear existing subscriptions and wait for close event if socket is connected
                if (!PWC.isEmptyObject(socket) && socket.readyState < 3) {
                    socket.addEventListener('close', function() {
                        connectToProperty();
                    });

                } else {
                    connectToProperty();
                }
            }
        };

        factory.Disconnect = function() {
            if (socket.close) {
                autoRetry = false;
                socket.close();
            }
        };

        factory.GetCompatableLightCircuits = function(circuit) {
            var message = {
                command: 'GetQuery',
                queryName: 'GetValidLightGroupings',
                arguments: circuit
            };
            return socketService.sendMessage(message);
        };

        factory.CreateGroup = function(args) {
            var message = {
                command: 'SetCommand',
                method: 'CreateGroup',
                arguments: args
            };
            return socketService.sendMessage(message);
        };

        factory.EditGroup = function(args) {
            var message = {
                command: 'SetCommand',
                method: 'EditGroup',
                arguments: args
            };
            return socketService.sendMessage(message);
        }

        factory.DeleteCoverCircuit = function (list) {
            var message = {
                command: 'SetParamList',                                
                objectList: [list]
            };
            return socketService.sendMessage(message);
        }

        factory.DeleteObject = function(objNames) {
            if (!objNames || !objNames.length) {
                return;
            }
            var message = {
                command: 'SetCommand',
                method: 'DeleteObject',
                arguments: objNames
            };
            return socketService.sendMessage(message);
        }

        factory.ProcessSystemInfo = function(info) {
            if (info && info.data) {
                var newObject = {
                    InstallationId: info.data.InstallationId,
                    PropertyName: info.data.PoolName,
                    OwnerName: info.data.OwnerName,
                    Address: info.data.Address
                }
                propertyModel.SystemInfo = newObject;
                $rootScope.$broadcast('SystemInfoUpdated', newObject);
            }
        }

        factory.GetPropertyStatusMessageFlags = function() {
            factory.GetQuery('GetStatusMessageFlags').then(function(result) {
                processStatusMessageFlags(result);
            });
        };

        factory.GetNotificationRecipients = function() {
            appService.GetData('Api/NotificationUsers/' + propertyModel.SelectedInstallation).then(function(result) {
                processNotificationRecipients(result);
            });
        };

        factory.SaveOwnerInfo = function(info) {
            var params = {
                PROPNAME: info.Name,
                NAME: info.OwnerName,
                PHONE: info.OwnerPhone,
                EMAIL: info.OwnerEmail
            };

            factory.SetParams("_5451", params).then(function() {
                factory.GetUserSettings();
            });
        };

        factory.SaveLocationData = function(params) {
            factory.SetParams('_5451', params).then(function() {
                factory.GetUserSettings();
            });
        };

        factory.GetSystemInformation = function() {
            appService.GetData('Api/Installations/' + propertyModel.SelectedInstallation).then(function(result) {
                processSystemInformation(result);
            });
        };

        factory.GetUserSettings = function() {
            return factory.GetParamListByObject("OBJNAM : OBJTYP : MODE : AVAIL : NAME : ADDRESS : CITY : STATE : ZIP : COUNTRY : TIMZON : PHONE : PHONE2 : EMAIL : EMAIL2 : TEMPNC : START : STOP : VACFLO : VACTIM : VALVE : HEATING : MANHT : PROPNAME : SERVICE ", "_5451").then(function (message) {
                if (message) {
                    var result = message.objectList[0];
                    messageProcessor.ProcessSystemConfig(result);
                }
            });
        };

        factory.SubscribeToPage = function (path) {
            console.log("----------------------------------------------- TAB SELECTED (PAGE) : ",path)
            // subcribe page items
            // subcribe the clock 
            factory.getUpdateClock();       // UPDATE CLOCK
            factory.getUpdateHeatMode();    // HEATER STATUS FOR FLAME, SUN AND FLAKES
            factory.getFreezeStatus();      // FREEZE STATUS
            factory.getServiceStatus();     // SERVICE STATUS
            factory.getSolarStatus();       // SOLAR STATUS
            factory.getFreezeCircuitStatus();//FREEZE STATUS
            
            factory.registeringAlltLightGroup();  // LIGHTH Group status STATUS
            factory.registeringAllGroupCircuits();// Circuit Group status STATUS
            // factory.getRemotesList();       // REMOTES LIST 
            factory.GetSchedules();         // SCHEDULES


            var subscriptionRequests = propertyModel.PermanantSubscriptions(propertyModel.ACL.OBJNAM);
            var subscribableObjects = propertyModel.getSubscribableObjects(path);

            angular.forEach(subscribableObjects, function(subscribableObject) {
                subscriptionRequests.push({
                    objnam: subscribableObject.OBJNAM,
                    keys: enumerations.GetLiveParams({
                        objTyp: subscribableObject.OBJTYP,
                        objnam: subscribableObject.OBJNAM,
                        subTyp: subscribableObject.SUBTYP
                    })
                });
            });

            switch (path) {
                case 'GENERAL':
                    console.log("---- PROCESSING GENERAL  ---")
                    var notificationRecipients = factory.GetNotificationRecipients();
                    var statusMessageFlags = factory.GetPropertyStatusMessageFlags();
                    var userSettings = factory.GetUserSettings();
                    var systemInformation = factory.GetSystemInformation();
                    var security = factory.GetSecurityTokens();
                    var hardware = GetOverrides();
                    var pageSubscriptions = socketService.subscribe(subscriptionRequests);
                    return $q.all([notificationRecipients, statusMessageFlags, userSettings, systemInformation, security, hardware, pageSubscriptions]);
                    break;
                    /*
                case 'SCHEDULES':
                    
                    console.log("---- PROCESSING SCHEDULES  ---")
                    var schedules = factory.GetSchedules();                    
                    var clockInfo = getClockValue();
                    var pageSubscriptions = socketService.subscribe(subscriptionRequests);
                    return $q.all([schedules, clockInfo, pageSubscriptions]);                                                                               
                    break;
                    */

                default:
                    return socketService.subscribe(subscriptionRequests);
            }

        };

        factory.GetQuery = function(queryName) {
            var message = {
                command: 'GetQuery',
                queryName: queryName,
                arguments: ''
            };
            return socketService.sendMessage(message);
        };

        factory.GetQueryArgs = function (queryName,queryArgs) {
            var message = {
                command: 'GetQuery',
                queryName: queryName,
                arguments: queryArgs,
            };
            return socketService.sendMessage(message);
        };

        factory.SetCommand = function(method, args) {
            var message = {
                command: 'SetCommand',
                method: method
            }

            if (args) {
                message.arguments = args;
            }

            return socketService.sendMessage(message);
        };

        factory.RequestParamList = function (objectList) {
            console.log("------ REQUEST PARAM LIST ---:::",objectList);
            var message = {
                command: 'RequestParamList',
                objectList: objectList
            };
            return socketService.sendMessage(message);
        };

        factory.GetHardwareDefinition = function(OBJNAM) {
            return factory.GetQuery('GetHardwareDefinition').then(function(result) {
                var message = {
                    command: 'RequestParamList',
                    objectList: []
                };
                propertyModel.hardwareDefinition = result.answer;
                messageProcessor.processHardwareDefinition(result);
                propertyModel.hardwareDefinition.map(function(hPanel) {
                    hPanel.params.OBJLIST.map(function(hModule) {
                        if (hModule.params.CIRCUITS) {
                            hModule.params.CIRCUITS.map(function(hCircuit) {
                                // do the call here
                                message.objectList.push({ objnam: hCircuit.objnam, keys: ['FEATR', 'DNTSTP', 'MANUAL', 'CIRCUIT'] });
                                // append maunual / featr / dntstp here
                                //hCircuit.params.DNTSTP = 'ON';
                                return hCircuit;
                            });
                            return hModule;
                        } else {
                            return hModule;
                        }
                    });
                    return hPanel;
                });

                return socketService.sendMessage(message).then(function(result) {
                    if (result && result.objectList) {
                        angular.forEach(result.objectList, function(value) {
                            propertyModel.hardwareDefinition.map(function(hPanel) {
                                hPanel.params.OBJLIST.map(function(hModule) {
                                    if (hModule.params.CIRCUITS) {
                                        hModule.params.CIRCUITS.map(function(hCircuit) {
                                            // append maunual / featr / dntstp here
                                            if (hCircuit.objnam === value.objnam) {
                                                if (value.params) {
                                                    hCircuit.params.DNTSTP = value.params.DNTSTP || null;
                                                    hCircuit.params.FEATR = value.params.FEATR || null;
                                                    hCircuit.params.MANUAL = value.params.MANUAL || null;
                                                }
                                            }
                                            return hCircuit;
                                        });
                                        return hModule;
                                    } else {
                                        return hModule;
                                    }
                                });
                                return hPanel;
                            });
                        });
                    }
                });
            });
        };

        // icweb retrieve "real current configuration" on the Target Hardware.
        factory.GetHardwareDefinitionPrecise = function (OBJNAM) {

            // adding arguments to the query
            var args = {
                Modality: 'Precise',                                    
            };

            return factory.GetQueryArgs('GetHardwareDefinition', args).then(function (result) {
                var message = {
                    command: 'RequestParamList',                    
                    objectList: []
                };
                propertyModel.hardwareDefinition = result.answer;
                messageProcessor.processHardwareDefinition(result);
                propertyModel.hardwareDefinition.map(function (hPanel) {
                    hPanel.params.OBJLIST.map(function (hModule) {
                        if (hModule.params.CIRCUITS) {
                            hModule.params.CIRCUITS.map(function (hCircuit) {
                                // do the call here
                                message.objectList.push({ objnam: hCircuit.objnam, keys: ['FEATR', 'DNTSTP', 'MANUAL', 'CIRCUIT'] });
                                // append maunual / featr / dntstp here
                                //hCircuit.params.DNTSTP = 'ON';
                                return hCircuit;
                            });
                            return hModule;
                        } else {
                            return hModule;
                        }
                    });
                    return hPanel;
                });

                return socketService.sendMessage(message).then(function (result) {
                    if (result && result.objectList) {
                        angular.forEach(result.objectList, function (value) {
                            propertyModel.hardwareDefinition.map(function (hPanel) {
                                hPanel.params.OBJLIST.map(function (hModule) {
                                    if (hModule.params.CIRCUITS) {
                                        hModule.params.CIRCUITS.map(function (hCircuit) {
                                            // append maunual / featr / dntstp here
                                            if (hCircuit.objnam === value.objnam) {
                                                if (value.params) {
                                                    hCircuit.params.DNTSTP = value.params.DNTSTP || null;
                                                    hCircuit.params.FEATR = value.params.FEATR || null;
                                                    hCircuit.params.MANUAL = value.params.MANUAL || null;
                                                }
                                            }
                                            return hCircuit;
                                        });
                                        return hModule;
                                    } else {
                                        return hModule;
                                    }
                                });
                                return hPanel;
                            });
                        });
                    }
                });
            });
        };

        factory.getUpdateClock = function () {
            console.log("======= request clock ============");
            var message = {
                command: 'RequestParamList',
                objectList: [{
                    objnam: '_C10C',
                    keys: ['MIN', 'DAY', 'CLK24A']
                }]
            };            

            socketService.sendMessage(message);                        
        };

        factory.getFreezeStatus = function () {
            console.log("======= request freeze status ============");
            var message = {
                command: 'RequestParamList',
                objectList: [{
                    objnam: '_5451',
                    keys: ['FRZ']
                }]
            };

            socketService.sendMessage(message);
        };

        factory.getSolarStatus = function () {
            console.log("======= request solar status ============");
            var message = {
                command: 'RequestParamList',
                objectList: [{
                    objnam: 'X0052',
                    keys: ['STATUS']
                }]
            };

            socketService.sendMessage(message);
        };

        factory.getFreezeCircuitStatus = function () {
            console.log("======= request Freeze Circuit status ============");
            var message = {
                command: 'RequestParamList',
                objectList: [{
                    objnam: '_5451',
                    keys: ['FRZ']
                }]
            };

            socketService.sendMessage(message);
        };


        factory.getServiceStatus = function () {
            console.log("======= request service status ============");
            var message = {
                command: 'RequestParamList',
                objectList: [{
                    objnam: '_5451',
                    keys: ['SERVICE']
                }]
            };

            socketService.sendMessage(message);
        };

        factory.registeringAlltLightGroup = function () {
            
            var LightsGroup = $filter('lightGroupCircuits')(propertyModel.Circuits, 'l');
            angular.forEach(LightsGroup, function (obj) {
                console.log("*===== REGISTERING ", obj.OBJNAM);                
                var message = {
                    command: 'RequestParamList',
                    objectList: [{
                        objnam: obj.OBJNAM,
                        keys: ['SYNC', 'SET', 'SWIM']
                    }]
                };

                socketService.sendMessage(message);
            });
        }

        factory.registeringAllGroupCircuits = function () {

            var circuitGroups = $filter('GroupCircuits')(propertyModel.CircuitGroups, 'l');            
            angular.forEach(circuitGroups, function (obj) {
                console.log("*===== REGISTERING ", obj.OBJNAM);
                var message = {
                    command: 'RequestParamList',
                    objectList: [{
                        objnam: obj.OBJNAM,
                        keys: ['STATUS']
                    }]
                };

                socketService.sendMessage(message);
            });
        }

        factory.registertLightGroupStatus = function (obj) {
            console.log("======= request group light status: SYNC,SET,SWIM ============",obj);
            var message = {
                command: 'RequestParamList',
                objectList: [{
                    objnam: obj,
                    keys: ['SYNC','SET','SWIM']
                }]
            };

            socketService.sendMessage(message);
        };

        factory.registerScheduleItem = function (obj) {

            if (obj.OBJTYP === "SCHED") {  // process only schedule type

                if (!obj.OBJNAM) {  // check if object exist
                    console.log("*===== REGISTERING SCHED IS NULL !!!");
                    return;
                }

                console.log("*===== REGISTERING SCHED ", obj.OBJNAM);
                var message = {
                    command: 'RequestParamList',
                    objectList: [{
                        objnam: obj.OBJNAM,
                        keys: ['ACT']
                    }]
                };

                socketService.sendMessage(message);
            }
        }

        factory.registertScheduleStatus = function () {
            console.log("======= request Schedule status ACT ============");
            var items = propertyModel.Schedules;
            if (items.length === 0)
                console.log("Total Schedules ", items.length);

            angular.forEach(items, function (obj) {
                factory.registerScheduleItem(obj);
                /*
                console.log("*===== REGISTERING SCHED ", obj.OBJNAM);
                var message = {
                    command: 'RequestParamList',
                    objectList: [{
                        objnam: obj.OBJNAM,
                        keys: ['ACT']
                    }]
                };

                socketService.sendMessage(message);
                */
            });
        };

        factory.registertAllPumpStatus = function (pumps) {
            angular.forEach(pumps, function (pump) {
                if (pump.OBJNAM)
                    factory.registertPumpStatus(pump.OBJNAM);
            });
        };

        factory.registertPumpStatus = function (obj) {
            console.log("======= request Pump status: RPM GPM PWR: ============", obj);
            var message = {
                command: 'RequestParamList',
                objectList: [{
                    objnam: obj,
                    keys: ['RPM', 'GPM', 'PWR','STATUS']
                }]
            };

            socketService.sendMessage(message);
        };
        // 
        factory.getUpdateAlarms= function () {
            console.log("======= request alarms ============");
            var message = {
                command: 'RequestParamList',
                objectList: [{
                    objnam: '_57A7',
                    keys: ['BADGE']
                }]
            };

            socketService.sendMessage(message);
        };

        // request new firmware version Update notification

        factory.getNotificationNewFirmwareAvailable = function () {
            //console.log("======= request new Firmware Update is Available ============");
            var message = {
                command: 'RequestParamList',
                objectList: [{
                    objnam: '_5451',
                    keys: ['UPDATE','IN']
                }]
            };

            socketService.sendMessage(message);
        };


        factory.getUpdateHeatMode = function () {
            console.log("======= request Heat Mode ============");
            var message = {
                command: 'RequestParamList',
                    objectList: [{  objnam: 'B1101',
                                    keys: ['HTMODE','HTSRC',"HEATER","STATUS","MANUAL","MODE"]
                                 },
                                {
                                    objnam: 'B1202',
                                    keys: ['HTMODE', 'HTSRC', "HEATER", "STATUS", "MANUAL", "MODE"]
                                },
                                {
                                    objnam: 'B1101 B1202',
                                    keys: ['HTMODE', 'HTSRC', "HEATER", "STATUS", "MANUAL", "MODE"]
                                },
                                {
                                    objnam: 'SSS11',
                                    keys: ['PROBE']
                                }
                                ]
            };

            socketService.sendMessage(message);

        };


        factory.getCircuitsFull = function() {
            // we were lacking on some fields for circuits from GetConfiguration so doing this in addition
            var message = {
                command: "GETPARAMLIST",
                condition: "OBJTYP=CIRCUIT",
                objectList: [{
                    objnam: "ALL",
                    keys: [enumerations.LiveParams.filter(function(LiveParam) {
                        if (LiveParam.objtyp && LiveParam.objtyp === 'circuit') {
                            return LiveParam;
                        }
                    })[0].liveParams.join(':')]
                }]
            };
            return socketService.sendMessage(message).then(function(result) {
                for (var i = 0; i < result.objectList.length; i++) {
                    var circuit = result.objectList[i];
                    messageProcessor.ProcessCircuit(circuit);
                }
                return result;
            });
        };

        factory.getCircuitNames = function() {
            return factory.GetQuery('GetCircuitNames').then(function(result) {
                propertyModel.circuitNames = result.answer;
            });
        };

        factory.getCircuitTypes = function() {
            return factory.GetQuery('GetCircuitTypes').then(function(result) {
                propertyModel.circuitTypes = result.answer;
            });
        };

        factory.getRemotesList = function () {
            propertyModel.RemoteList = [];
            return factory.GetParamListByOBJTYP("OBJNAM : OBJTYP : OBJREV : STATIC : LISTORD : SUBTYP : SNAME : HNAME : ENABLE", "REMOTE")
                .then(function (result) {
                    propertyModel.RemoteList = result.objectList;                    

                    $rootScope.$broadcast('RemotesUpdatedBroadcast', result.objectList);
                })
        };

        factory.getRemotes = function() {
            propertyModel.remotes = [];
            return factory.GetParamListByOBJTYP("OBJNAM : OBJTYP : OBJREV : STATIC : LISTORD : SUBTYP : SNAME : HNAME : ENABLE", "REMOTE")
                .then(function(result) {
                    propertyModel.remotes = result.objectList;
                })
        };

        factory.subscribeToObject = function(subscribableObject) {
            return socketService.subscribe([{
                    objnam: subscribableObject.OBJNAM,
                    keys: enumerations.GetLiveParams({
                        objTyp: subscribableObject.OBJTYP,
                        objnam: subscribableObject.OBJNAM,
                        subTyp: subscribableObject.SUBTYP
                    })
                }]);
        };

        factory.getUpdatedBody = function(OBJNAM) {
            var message = {
                command: 'RequestParamList',
                objectList: [{
                    objnam: OBJNAM,
                    keys: ['VOL', 'PRIM', 'SEC', 'ACT1', 'ACT2', 'ACT3', 'ACT4', 'MANUAL', 'SNAME']
                }]
            };

            var p = $q.defer();

            socketService.sendMessage(message).then(function(result) {
                var resultBody = result.objectList[0].params;
                propertyModel.Bodies.map(function(body) {
                    if (body.OBJNAM === OBJNAM) {
                        body.SNAME = resultBody.SNAME;
                        body.PRIM = resultBody.PRIM;
                        body.SEC = resultBody.SEC;
                        body.ACT1 = resultBody.ACT1;
                        body.ACT2 = resultBody.ACT2;
                        body.ACT3 = resultBody.ACT3;
                        body.ACT4 = resultBody.ACT4;
                        body.VOL = resultBody.VOL;
                        body.MANUAL = resultBody.MANUAL;
                    }
                });
                return p.resolve(propertyModel.Bodies);
            });

            return p.promise;
        };
        
        factory.getChemistries = function() {
            var message = {
                command: 'GetParamList',
                condition: 'OBJTYP=CHEM',
                objectList: [{
                    objnam: 'INCR',
                    keys: enumerations.LiveParams.filter(function(LiveParam) {
                        if (LiveParam.objtyp && LiveParam.objtyp === 'chem') {
                            return LiveParam;
                        }
                    })[0].liveParams
                }]
            };
            return socketService.sendMessage(message);
        };

        factory.getAddressMap = function(objtyp, subtyp) {
            var message = {
                command: 'GetQuery',
                queryName: 'AddressMap',
                arguments: {
                    'OBJTYP': objtyp,
                    'SUBTYP': subtyp
                }
            };
            return socketService.sendMessage(message);
        };

        factory.editObjectAffect = function(objnam, listItems) {
            var message = {
                command: 'SetCommand',
                method: 'EditObjectAffect',
                arguments: {
                    OBJNAM: objnam,
                    objectList: listItems
                }
            };
            return socketService.sendMessage(message);
        };
        
        factory.createObjectAffect = function(objnam, listItems) {
            var message = {
                command: 'SetCommand',
                method: 'CreateObjectAffect',
                arguments: {
                    OBJNAM: objnam,
                    objectList: listItems
                }
            };
            return socketService.sendMessage(message);
        };
        
        factory.createOneObjectAffect = function (objnam, affect) {
            var newObj = {
                "CIRCUIT": affect
            };
            var message = {
                command: 'SetCommand',
                method: 'CreateObjectAffect',
                arguments: {
                    OBJNAM: objnam,
                    objectList: [newObj]
                }
            };
            return socketService.sendMessage(message);
        };

        factory.getNameQuotationMark = function (name) {
            var token = name;
            return token;
        };

        factory.getObjectAffect = function (affect) {

            var newObj = factory.getNameQuotationMark("CIRCUIT") + ":";

            newObj = newObj + factory.getNameQuotationMark(affect);

            return newObj;
        };

        // multiple items
        factory.createCircuitObjectAffects = function (objnam, circuits) {
            var params = [];
            var index = 0;


            angular.forEach(circuits, function (affect) {
                var newObj = {
                    "CIRCUIT": affect.objnam
                };

                params[index] = newObj;
                index = index + 1;
            });

            var message = {
                command: 'SetCommand',
                method: 'CreateObjectAffect',
                arguments: {
                    OBJNAM: objnam,                    
                    objectList: params
                }
            };
            return socketService.sendMessage(message);
        };


        factory.getRemotes = function() {
            var message = {
                command: 'GetQuery',
                queryName: 'GetRemotes',
                arguments: ''
            };
            return socketService.sendMessage(message);
        };

        factory.editRemote = function(objnam, params) {
            var message = {
              command: 'SetCommand',
              method: 'EditRemote',
              arguments: [
                  {
                      objnam: objnam,
                      params: params
                  }
              ]
            };
            return socketService.sendMessage(message);
        };

        factory.createRemote = function(obj) {
            var message = {
                command: 'SetCommand',
                method: 'CreateRemote',
                arguments: [
                    {
                        objtyp: obj.objtyp,
                        params: obj.params
                    }

                ]
            };
            return socketService.sendMessage(message);
        };

        factory.deleteRemote = function(objnam) {
            var message = {
                command: 'SetCommand',
                method: 'DeleteRemote',
                arguments: [
                    {
                        objnam: objnam
                    }

                ]
            };
            return socketService.sendMessage(message);
        };

        /** CreateObject is used to create new objects on the target hardware.  All known parameters of the new object are supplied 
         *   in the initial payload.  The response to the CreateObject command is the ObjectCreated which contains a response and the 
         *   object name assigned by the target hardware.
         **/
        factory.createObject = function(object) {
            var message = {
                command: 'CreateObject',
                objectList: [object]
            }
            return socketService.sendMessage(message);
        }

        factory.getCircuitAffects = function() {
            var message = {

            };
            return socketService.sendMessage(message);
        };

        factory.getCovers = function() {
          propertyModel.covers = [];
          return factory.GetParamListBySUBTYP("SNAME : HNAME : OBJNAM : OBJTYP : SUBTYP : PARENT : STATIC : STATUS : NORMAL : BODY ", "EXTINSTR", "COVER")
            .then(function(result) {
                propertyModel.covers = result.objectList;
                angular.forEach(propertyModel.covers, function (cover) {
                    // now retrieve each cover 
                    factory.getCoversCircuitsAffected(cover);

                });
            });
        };

        factory.getCoversCircuitsAffected = function (cover) {

            factory.GetParamListByOBJTYPOnly("OBJNAM : LISTORD : OBJTYP : PARENT : CIRCUIT", "CHILD",cover.objnam)
              .then(function (result) {
                  var localcircuits = [];
                  var index = 0;
                  angular.forEach(result.objectList, function (circuit) {
                      console.log("======= COVER parent :"+circuit.params.PARENT+
                                  " CIRCUIT:" + circuit.params.CIRCUIT);
                      var item = {
                          objnam: circuit.params.CIRCUIT,
                          objparent: circuit.params.PARENT,
                          objID : circuit.params.OBJNAM
                      };                      
                      localcircuits[index] = item;
                      index = index + 1;                      
                  });
                  cover.circuits = [];
                  cover.circuits = localcircuits;
                  //propertyModel.covers = result.objectList;
              });            
        };


        //END
        // Private Methods
        function GetOverrides() {
            return factory.GetParamListByObject("OBJNAM : OBJTYP : MANOVR", "_CFEA").then(messageProcessor.ProcessFeatureObject);
        }

        function processSystemInformation(info) {
            propertyModel.SystemInformation = {};
            if (info && info.data) {
                propertyModel.SystemInformation = info.data;
            }
            $rootScope.$broadcast('SystemInformationUpdated', info);
        }

        function processNotificationRecipients(info) {
            propertyModel.NotificationRecipients = [];
            if (info && info.data) {
                propertyModel.NotificationRecipients = info.data;
            }
            $rootScope.$broadcast('NotificationRecipientUpdated', info);
        };

        function processStatusMessageFlags(message) {
            propertyModel.PropertyStatusMessageFlags = {};
            if (message.answer) {
                var flags = message.answer;
                propertyModel.PropertyStatusMessageFlags = {
                    Circuit: flags.CIRCUIT.split(''),
                    Pump: flags.PUMP.split(''),
                    IntelliChlor: flags.ICHLOR.split(''),
                    IntelliChem: flags.ICHEM.split(''),                                                            
                    HeaterUltraT: flags.ULTRA.split(''),
                    HeaterMasterT: flags.MASTER.split(''),
                    HeaterHComboT: flags.HCOMBO.split(''),
                };
            }
            $rootScope.$broadcast('PropertyStatusMessageFlagsUpdated', message);
        };

        function getClockValue() {
            return factory.GetParamListByObject("OBJNAM : OBJTYP : SOURCE : MIN : CLK24A : TIMZON", "_C010").then(messageProcessor.ProcessLocation);
        }

        function saveTimeSince(message) {
            if (message.timeNow && message.timeSince) {
                switch (message.command) {
                    case 'NOTIFYLIST':
                        if (message.timeSince === propertyModel.TimeSince.WriteParam) {
                            propertyModel.TimeSince.NotifyList = message.timeNow;
                        } else {
                            // TODO: re-request Notifications
                            console.warn('TODO');
                        }
                        break;
                }
            }
        }

        function sendSetParamList(objNam, prop, value) {
            if (objNam && prop && !angular.isUndefined(value)) {

                var newParams = {};
                newParams[prop] = value.toString();

                var newObj = {
                    objnam: objNam,
                    params: newParams
                };

                var message = {
                    command: "SETPARAMLIST",
                    objectList: [newObj]
                };
                return socketService.sendMessage(message);
            }
        }

        function selectedInstallationIsValid() {
            return (propertyModel.SelectedInstallation && 0 !== propertyModel.SelectedInstallation);
        }

        function connectToProperty() {
            saveCookies();
            if (selectedInstallationIsValid()) {
                socket = socketService.newSocket(propertyModel.SelectedInstallation);
                propertyModel.SystemInfo[propertyModel.SelectedInstallation] = {};

                if (!PWC.isEmptyObject(socket)) {
                    wireUpSocketEvents();
                } else {
                    reconnect();
                }
            }
            return socket;
        }

        function saveCookies() {
            sessionStorage.selectedInstallation = JSON.stringify(propertyModel.SelectedInstallation || '0');
            if (localStorageService.get('saveSession'))
                localStorageService.set('selectedInstallation', sessionStorage.selectedInstallation);
        }

        function handleMessage(message) {
            if (message) {
                var msgObj = message;
                if(typeof message === 'string' && message === 'pong') {
                    return;
                }
                if (typeof message === 'string') {
                    msgObj = JSON.parse(message);
                }
                if (PWC.isValidObject(msgObj, ['command'])) {
                    saveTimeSince(message);
                    messageProcessor.ProcessNotification(msgObj);
                    if (msgObj.command.toUpperCase() === "NOTIFYLIST") {
                        setPropertyReady();
                    }
                } else {
                    console.log("Failed to convert JSON to Object");
                }
            }
        }

        function wireUpSocketEvents() {
            socket.addEventListener('open', function() {
                propertyModel.ConnectionStatus = 'Downloading...';
                socketService.clearSubscriptions();
                initConnection();
                propertyModel.Busy = false;
            });

            socket.addEventListener('error', function() {
                propertyModel.Clear();
                $rootScope.$apply();
                reconnect();
            });

            socket.addEventListener('close', function() {
                propertyModel.ConnectionStatus = 'Not Connected';
                if (autoRetry) {
                    reconnect();
                }
                propertyModel.Clear(!autoRetry);
                $rootScope.$apply();
                autoRetry = true;
            });

            socket.addEventListener('message', function(e) {
                handleMessage(e.data);
                $rootScope.$apply();
            });
        }

        function initConnection() {
            saveUserSecurityToken();
            factory.SubscribeToPage();
            factory.ReadStatusMessage();
        }

        function saveUserSecurityToken() {
            var result = false;
            propertyModel.ACL = {};
            var installation = propertyModel.getInstallationObject(propertyModel.SelectedInstallation);
            if (installation && installation.AccessToken) {
                var newObject = {
                    OBJNAM: installation.AccessToken,
                    SHOMNU: ""
                };
                propertyModel.ACL = newObject;
                // propertyModel.Objects[newObject.OBJNAM] = newObject;
                result = true;
            }
            return result;
        }

        function reconnect() {
            reconnecting.active = true;
            if (propertyModel.SelectedInstallation && reconnecting.attempts < enumerations.MaxReconnectAttempts && authService.authentication.isAuth) {
                if (propertyModel.getInstallationObject(propertyModel.SelectedInstallation).OnLine) {
                    propertyModel.ConnectionStatus = 'Connection Error';
                } else {
                    propertyModel.ConnectionStatus = 'Property OffLine!';
                }
                
                propertyModel.ConnectionStatus = propertyModel.ConnectionStatus + ' Attempting to Re-Connect(' + reconnecting.attempts + ')...';
                console.log("=============================================================================================================================")
                console.log("========================            R E C O N N E C T                                                ========================")
                console.log("========================  " , propertyModel.ConnectionStatus)

                $timeout(function() {
                    if (PWC.isEmptyObject(socket) || socket.readyState === 3) {
                        reconnecting.multiple++;
                        reconnecting.attempts++;
                        factory.Connect();
                        if (reconnecting.multiple === enumerations.MaxTimeOut) {
                            reconnecting.multiple = 1;
                        }
                    }
                }, reconnecting.multiple * 1000);
            } else {
                propertyModel.ConnectionStatus = 'Not Connected';
                console.log("========================  " + propertyModel.ConnectionStatus)
                $location.path('/login');   // GO TO LOGIN
            }
        }

        function resetReconnectionObject() {
            reconnecting = {
                attempts: 0,
                multiple: 1,
                active: false
            };
        }

        function setPropertyReady() {
            resetReconnectionObject();
            if (propertyModel.SecurityInitiated && !propertyModel.Ready) {
                factory.SetACLSHOMNU();
                factory.getCircuitNames();
                factory.getCircuitTypes();
                factory.getRemotes();

                factory.GetParamListBySUBTYP('STATUS:SUBTYP:OBJTYP', 'CIRCUIT', 'FRZ').then(function(result) {
                    var freezeCircuit = {};
                    if (result.objectList && result.objectList.length > 0 && result.objectList[0].params) {
                        freezeCircuit = result.objectList[0].params;
                        freezeCircuit.OBJNAM = result.objectList[0].objnam;
                    }
                    propertyModel.FreezeCircuit = freezeCircuit;
                });
                
                factory.GetQuery('GetConfiguration').then(function(result) {
                    if (result.answer) {
                        messageProcessor.ProcessConfiguration(result);
                        factory.GetHardwareDefinition().then(function() {
                            factory.getActiveStatusMessages().then(function(result) {
                                messageProcessor.ProcessAlerts(result);
                            });
                        });
                        propertyModel.Ready = true;
                        $location.path('/property');
                    }
                });
            }
            propertyModel.ConnectionStatus = 'Connected';
        }

        factory.handleMessage = handleMessage;
        //END
        return factory;
    }
]);
