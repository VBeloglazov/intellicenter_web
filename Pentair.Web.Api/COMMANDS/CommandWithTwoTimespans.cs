﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Pentair.Web.Api
{
    public class CommandWithTwoTimespans : CommandBase
    {
        [JsonProperty("timeNow")]
        public string TimeNow { get; private set; }

        [JsonProperty("timeSince")]
        public string TimeSince { get; private set; }

        public CommandWithTwoTimespans(
            string command,
            string messageID,
            string timeNow,
            string timeSince
            )
            : base(command, messageID)
        {
            this.TimeNow = timeNow;
            this.TimeSince = timeSince;
        }

    } // class CommandWithTwoTimespans
}
