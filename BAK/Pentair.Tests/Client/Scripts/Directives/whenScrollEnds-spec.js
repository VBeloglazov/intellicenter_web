﻿describe('When Scroll Ends', function () {
    var $scope, $compile, $document, $window;

    var baseTemplate = ' <div style="min-height:400px; max-height:400px; overflow-y:scroll;" whenscrollends="scrollEnded()"></div>';

    beforeEach(module('PentairWebClient'));

    beforeEach(inject(function ($rootScope, _$compile_, _$document_, _$window_) {
        // Get an isolated scope
        $scope = $rootScope.$new();
        $compile = _$compile_;
        $document = _$document_;
        $window = _$window_;
        $scope.scrollEnded = jasmine.createSpy('scrollEnded');
    }));


    describe('When scrolling', function () {
        beforeEach(function () {
            $window.document.body.style.maxHeight = '5px';
        });

        it('should invoke the callback method when you reach the bottom of the page', function () {
            var elm = SpecHelper.createDirective($compile, baseTemplate, $scope);
            $document.find('body').append(elm)

            elm.triggerHandler('scroll');

            expect($scope.scrollEnded).toHaveBeenCalled();
        });
    });


});