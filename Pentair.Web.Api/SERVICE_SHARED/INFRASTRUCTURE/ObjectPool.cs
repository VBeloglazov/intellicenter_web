﻿/*
--------------------------------------------------------------------------------

    ObjectPool.cs

    Copyright © Pentair Water Pool and Spa, Inc. 2014

--------------------------------------------------------------------------------
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SERVICE_SHARED
{
    public abstract class ObjectPool<T> : IDisposable
        where T : IDisposable
    {
        private readonly string EVENT_SOURCE;
        protected virtual string EventSourceName { get { return this.EVENT_SOURCE; } }

        private static string WHO_THIS = typeof(ObjectPool<T>).ToString();
        private static string WHO_DISPOSE = WHO_THIS + ".Dispose";
        private static string WHO_COUNT = WHO_THIS + ".Count";
        private static string WHO_USAGE = WHO_THIS + ".Usage";
        private static string WHO_COUNTASYNC = WHO_THIS + ".CountAsync";
        private static string WHO_USAGEASYNC = WHO_THIS + ".UsageAsync";
        private static string WHO_PUSH = WHO_THIS + ".Push";
        private static string WHO_PUSHASYNC = WHO_THIS + ".PushAsync";
        private static string WHO_POP = WHO_THIS + ".Pop";
        private static string WHO_POPASYNC = WHO_THIS + ".PopAsync";
        private static string WHO_DESTROYPOOL = WHO_THIS + ".ActionEachItem";

        private object lockPool = new object();

        private Stack<T> pool = null;

        public int Capacity { get; private set; }
        private bool IsFixedSize { get; set; }

        /// <exception cref="Exception" />
        public int Count
        {
            get
            {
                int count = -1;

                LockWithTimeout.DoWithLock(
                    lockObj: this.lockPool,
                    timeoutMSec: 100,
                    action: () =>
                    {
                        count = this.pool.Count;
                    }
                );

                return count;
            }

        } // Count

        public int Usage
        {
            get
            {
                int usage = -1;

                LockWithTimeout.DoWithLock(
                    lockObj: this.lockPool,
                    timeoutMSec: 100,
                    action: () =>
                    {
                        usage = this.Capacity - this.pool.Count;
                    }
                );

                return usage;
            }

        } // Usage

        /// <exception cref="Exception" />
        public abstract T CreateNewObject();

        #region Construction

        public ObjectPool(
            string eventSourceNamePrefix,
            int capacity,
            bool isFixedSize)
        {
            this.EVENT_SOURCE = eventSourceNamePrefix + "ObjectPool";

            this.pool = new Stack<T>(capacity);

            this.Capacity = capacity;
            this.IsFixedSize = isFixedSize;

        } // ObjectPool()

        #endregion Construction

        #region IDisposable implementation

        volatile bool disposed = false;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);

        } // Dispose()

        /// <exception cref="Exception" />
        protected virtual void Dispose(bool disposing)
        {
            // TODO: Consider something like below:
            //
            // enum State { Executing, Done };
            // private int state = (int)State.Executing;
            //
            // private void Complete()
            // {
            //   if (Interlocked.CompareAndExchange (ref state, (int)State.Done, (int)State.Executing) ==
            //       (int)State.Executing)
            //   {
            //     ...
            //   }
            // }
            //
            // The first time Complete executes, it will compare the state variable against State.Executing
            // and if they are equal replace state with the value State.Done.
            // The next thread that executes this code will compare state against State.Executing,
            // which will not be true, and CompareAndExchange will return State.Done, failing the if statement.
            //
            if (this.disposed)
            {
                return;
            }

            this.disposed = true;

            if (disposing)
            {
                // Free managed objects

                LockWithTimeout.DoWithLock(
                    lockObj: this.lockPool,
                    timeoutMSec: 100,
                    action: () =>
                    {
                        Task.WhenAll(
                            this.pool.Select(t => { t.Dispose(); return Task.FromResult(0); })
                        )
                        .Wait();

                        this.pool.Clear();
                    }
                );
            }

            // Free unmanaged objects
            // N/A

        } // Dispose(bool disposing)

        #endregion IDisposable implementation

        #region Public Interface

        /// <exception cref="ArgumentNullException" />
        /// <exception cref="Exception" />
        public virtual void Push(
            T item,
            string caller)
        {
            LockWithTimeout.DoWithLock(
                lockObj: this.lockPool,
                timeoutMSec: 100,
                action: () =>
                {
                    PushItem(item);
                }
            );

        } // Push()

        /// <summary>
        /// Always returns object, doesn't return null.
        /// </summary>
        /// <exception cref="Exception" />
        public virtual T Pop(
            string caller)
        {
            T retT = default(T);

            LockWithTimeout.DoWithLock(
                lockObj: this.lockPool,
                timeoutMSec: 100,
                action: () =>
                {
                    retT = PopItem();
                }
            );

            return retT;

        } // Pop()

        // TODO: 2016-03-03 VB: It throws now, test Relay/Weather services! (Discovery has been tested 2017-08-01)
        /// <exception cref="Exception" />
        public void DoEachItem(
            Action<T> actionEachItem)
        {
            LockWithTimeout.DoWithLock(
                lockObj: this.lockPool,
                timeoutMSec: 100,
                action: () =>
                {
                    if (this.pool.Count != this.Capacity)
                    {
                        throw new Exception(
                            String.Format("DoEachItem for pool of {0} failed: Not all elements are pushed back!\n\nCapacity: {1}\nCount: {2}",
                                typeof(T).Name,
                                this.Capacity,
                                this.pool.Count
                            )
                        );
                    }

                    Task[] tasks = new Task[this.pool.Count];
                    int i = 0;
                    foreach (T t in this.pool)
                    {
                        tasks[i++] = Task.Run(() => actionEachItem(t));
                    }
                    Task.WaitAll(tasks);
                }
            );

        } // DoEachItem()

        #endregion Public Interface

        #region Implementation

        /// <summary>
        /// Always returns object, doesn't return null.
        /// Not thread safe.
        /// </summary>
        /// <exception cref="IndexOutOfRangeException" />
        /// <exception cref="Exception" />
        private T PopItem()
        {
            T retT;

            if (this.pool.Count > 0)
            {
                retT = this.pool.Pop();
            }
            else
            {
                if (this.IsFixedSize)
                {
                    throw new IndexOutOfRangeException(
                        String.Format("PopItem() failed - Fixed size pool of <{0}> exosted. Capacity: {1}",
                            typeof(T).Name,
                            this.Capacity
                        )
                    );
                }

                this.Capacity++;

                LogManager.LogWarningAsync(
                    source: this.EventSourceName,
                    eventBriefDescription: "(PopItem) Flexible size pool exosted",
                    installationId: 0,
                    userId: Guid.Empty,
                    connectionId: Guid.Empty,
                    eventFullDescription:
                        String.Format("Item of type {0} added, new capacity: {1}",
                            typeof(T).Name,
                            this.Capacity
                        )
                );

                retT = CreateNewObject();
            }

            return retT;

        } // PopItem()

        /// <summary>
        /// Not thread safe.
        /// </summary>
        /// <exception cref="IndexOutOfRangeException" />
        /// <exception cref="Exception" />
        private void PushItem(T item)
        {
            if (this.pool.Count < this.Capacity)
            {
                this.pool.Push(item);
            }
            else
            {
                throw new IndexOutOfRangeException(
                    String.Format("PushItem() failed - Capacity reached: {0}",
                        this.Capacity
                    )
                );
            }

        } // PushItem()

        protected void DestroyPool(
            Action<T> actionEachItem)
        {
            LockWithTimeout.DoWithLock(
                lockObj: this.lockPool,
                timeoutMSec: 100,
                action: () =>
                {
                    if (this.pool.Count != this.Capacity)
                    {
                        // TODO: Throw instead? Caller could report more details...
                        LogManager.LogErrorAsync(
                            source: this.GetType().Name,
                            eventBriefDescription: "Destroying pool - not all items are pushed back",
                            installationId: 0,
                            userId: Guid.Empty,
                            connectionId: Guid.Empty,
                            eventFullDescription:
                            String.Format("Pool item type: {0}\nCapacity: {1}\nCount: {2}",
                                typeof(T).Name,
                                this.Capacity,
                                this.pool.Count
                            )
                        );
                    }

                    Task[] tasks = new Task[this.pool.Count];
                    int i = 0;
                    foreach (T t in this.pool)
                    {
                        tasks[i++] = Task.Run(() => actionEachItem(t));
                    }
                    Task.WaitAll(tasks);

                    this.pool.Clear();
                }
            );

        } // DestroyPool()

        #endregion Implementation

    } // class ObjectPool<T>
}
