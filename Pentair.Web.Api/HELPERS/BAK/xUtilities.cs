﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pentair.Web.Api
{
    public static class Utilities
    {
        public static bool IsValidConnectionThw(
            int installationId)
        {
            Connection connection = null;

            using (ConnectionsContext db = new ConnectionsContext())
            {
                connection = db.GetConnectionThw(installationId);
            }

            return (connection != null);

        } // IsValidConnectionThw

    } // IsValidConnectionThw()
}
