﻿using System;
using FluentAutomation;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using TechTalk.SpecFlow;

namespace Pentair.Specflow
{
    
    [Binding]
    public class LoginSteps : FluentTest
    {

        public LoginSteps()
        {
            SeleniumWebDriver.Bootstrap(SeleniumWebDriver.Browser.Chrome);
            FluentSettings.Current.WaitTimeout = TimeSpan.FromSeconds(10);
            FluentSettings.Current.ScreenshotOnFailedAssert = true;
            FluentSettings.Current.ScreenshotOnFailedAction = true;
            FluentSettings.Current.ScreenshotPath = "C:\\screenshots";
        }

 
        [Given(@"the remember checkbox was checked")]
        public void GivenTheRememberCheckboxWasChecked()
        {
            var remember = I.Find("#rememberMe");
            if (remember.Element.Value == "")
            {
                I.Click("#rememberMe");
            }
        }

        [Given(@"the remember checkbox was NOT checked")]
        public void GivenTheRememberCheckboxWasNOTChecked()
        {
            var remember = I.Find("#rememberMe");
            if (remember.Element.Value != "")
            {
                I.Click("#rememberMe");
            }
        }

        [When(@"the user restarts the app")]
        public void WhenTheUserRestartsTheApp()
        {
            I.Open("http://localhost:5911");
        }

        [When(@"the user clicks the ""(.*)"" button")]
        public void WhenTheUserClicksTheButton(string p0)
        {
            string id = "#" + p0;
            I.Click(id);
        }

        [Then(@"the user goes to the status screen")]
        public void ThenTheUserGoesToTheStatusScreen()
        {
            I.Assert.Url("http://localhost:5911/#/properties");
        }

        [Then(@"an error message is displayed")]
        public void ThenAnErrorMessageIsDisplayed()
        {
            I.Assert.Class("ng-hide").Not.On("#warning");
        }

        [Then(@"the user goes to the login screen")]
        public void ThenTheUserGoesToTheLoginScreen()
        {
            I.Assert.Url("http://localhost:5911/#/login");
        }

        [Then(@"the bad login credentials are NOT remembered")]
        public void ThenTheBadLoginCredentialsAreNOTRemembered()
        {
            ThenTheUserGoesToTheLoginScreen();
            I.Assert.Value("").In("#username");
            I.Assert.Value("").In("#password");
        }

        [Then(@"the user has logged out")]
        public void ThenTheUserHasLoggedOut()
        {
            WhenTheUserRestartsTheApp();
            ThenTheUserGoesToTheLoginScreen();
        }

        [Given(@"the user enters ""(.*)"" into the ""(.*)"" field")]
        public void GivenTheUserEntersIntoTheField(string p0, string p1)
        {
            I.Enter(p0).In("#" + p1);
        }

        [Given(@"the user clicks the ""(.*)"" button")]
        public void GivenTheUserClicksTheButton(string p0)
        {
            string id = "#" + p0;
            I.Click(id);
        }

        [Given(@"the user goes to the login page")]
        public void GivenTheUserGoesToTheLoginPage()
        {
            I.Open("http://localhost:5911");        
        }

        [Given(@"the user is logged in")]
        public void GivenTheUserIsLoggedIn()
        {
            I.Open("http://localhost:5911").Enter("user@pentair.com").In("#userName").Enter("Password").In("#password").Click(".loginSubmitBtn");
        }

        [Given(@"no user is logged in")]
        public void GivenNoUserIsLoggedIn()
        {
            I.Open("http://localhost:5911");
            GivenTheUserClicksTheButton("logout");

        }

    }
}
