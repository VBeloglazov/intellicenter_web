﻿using System;
using System.Linq;

using Newtonsoft.Json;

namespace Pentair.Web.Api
{
    public class WebSocketMessage
    {
        /// <summary>
        /// Test for an acknowledgement type message
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public static bool IsAnAcknowledgementMessage(string message)
        {
            return (IsErrorAcknowledgement(message) ||
                    IsSuccessAcknowledgement(message));
        }

        /// <summary>
        /// Test for a success acknowledgement message
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public static bool IsSuccessAcknowledgement(string message)
        {
            return message == CommandString.SuccessMessage;
        }

        /// <summary>
        /// Test for an error ackowledgement message
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public static bool IsErrorAcknowledgement(string message)
        {
            if (message.Length >= CommandString.ErrorMessage.Length)
            {
                return message.Substring(0, CommandString.ErrorMessage.Length) == CommandString.ErrorMessage;
            }
            return false;
        }

        /// <summary>
        /// Parse and return the message type.
        /// VB: It does JsonConvert.DeserializeObject(CommandBase) !
        /// </summary>
        /// <param name="message"></param>
        /// <param name="endpoint"></param>
        /// <exception cref="Exception" />
        public static MessageType ParseMessageType(
            string message,
            EndpointType endpoint)
        {
            MessageType type;

            if (String.IsNullOrEmpty(message))
            {
                throw new Exception("Cannot parse empty or null message");
            }

            var obj = JsonConvert.DeserializeObject<CommandBase>(message);
            var cmd = obj.Command.ToUpper();

            if (!CommandList.Commands.ContainsKey(cmd))
            {
                throw new Exception("Unknown command: " + obj.Command);
            }

            type = new MessageType(endpoint, CommandList.Commands[cmd], obj.MessageID);

            return type;

        } // ParseMessageType()

    } // class WebSocketMessage

    // TODO: VB This creature somewhat duplicates the CommandBase - get rid of it!
    public class MessageType
    {
        public EndpointType Origin { get; private set; }
        public CommandType CommandType { get; private set; }
        public string MessageID { get; private set; }

        public MessageType(EndpointType origin, CommandType cmd, string id)
        {
            Origin = origin;
            CommandType = cmd;
            MessageID = id;
        }
    }
}