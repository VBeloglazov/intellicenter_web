﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pentair.Web.Api
{
    // TODO: VB It partially duplicates UserInstallation -- better solution?
    public class InstallationWithDetails
    {
        // [Installations]
        public int InstallationId { get; set; }
        public string Name { get; set; }
        public Guid OwnerUserId { get; set; }
        public string Secret { get; set; }
        public long LastChange { get; set; }
        public long LastHistory { get; set; }
        public DateTime CreateDateTime { get; set; }
        public DateTime UpdateDateTime { get; set; }

        // [Connections]
        public bool IsConnected { get; set; }

        // TODO: VB Move to [Installations]
        // [ControlObjects]
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Country { get; set; }
        public string OwnerName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }

    } // class InstallationWithDetails
}