using System.Data.Entity.Migrations;
using System.Diagnostics.CodeAnalysis;
using Pentair.Domain.Client.Infrastructure;

namespace Pentair.Domain.Client.SimulatorMigrations
{
    [ExcludeFromCodeCoverage]
    internal sealed class Configuration3 : DbMigrationsConfiguration<SimulatorContext>
    {
        public Configuration3()
        {
            AutomaticMigrationsEnabled = false;
            AutomaticMigrationDataLossAllowed = false;
            MigrationsDirectory = @"SimulatorMigrations";
        }
    }
}