﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Pentair.Domain.Client.Helpers.Commands
{
    // TODO: This is actually request now, rename! And maybe another base?
    public class HistoryWriteCommandResponse : ResponseCommandWithTwoTimespans
    {
        [JsonProperty("messageNo", Required = Required.Always)]
        public int MessageNo { get; private set; }

        [JsonProperty("objectList", Required = Required.Default)]
        public List<Dictionary<string, List<HistoryItem>>> ObjectList { get; private set; }

        public HistoryWriteCommandResponse(
            string messageID,
            string response,
            int messageNo,
            string timeNow,
            string timeSince,
            List<Dictionary<string, List<HistoryItem>>> objectList
            )
            : base(CommandString.WriteHistoryMessage, messageID, response, timeNow, timeSince)
        {
            this.MessageNo = messageNo;
            this.ObjectList = objectList;

        } // HistoryWriteCommandResponse()

        /// <summary>
        /// Always returns an object, valid or not. If invalid, check the DeserializationError property value.
        /// </summary>
        public static new HistoryWriteCommandResponse Deserialize(
            string serializedMessage)
        {
            HistoryWriteCommandResponse command = null;

            try
            {
                command = JsonConvert.DeserializeObject<HistoryWriteCommandResponse>(serializedMessage);
                command.IsValid = true;
            }
            catch (Exception e)
            {
                // Deserialization as HistoryWriteCommandResponse failed

                StringBuilder sbError = new StringBuilder();

                sbError.Append("Cannot deserialize as HistoryWriteCommandResponse. " + e.Message);

                // Try to get at least the base message data
                ResponseCommandWithTwoTimespans baseCommand = ResponseCommandWithTwoTimespans.Deserialize(serializedMessage);

                if (!baseCommand.IsValid)
                {
                    sbError.Append("\n" + baseCommand.DeserializationError);
                }

                command = new HistoryWriteCommandResponse(
                    messageID: baseCommand.MessageID,
                    response: ResponseString.BadRequest,
                    messageNo: -1,
                    timeNow: String.Empty,
                    timeSince: String.Empty,
                    objectList: null)
                {
                    IsValid = false,
                    DeserializationError = sbError.ToString()
                };
            }

            return command;

        } // Deserialize()

    } // class HistoryWriteCommandResponse
}
