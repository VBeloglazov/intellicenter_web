Feature: Feature Control (PEN-97)
As a Pool Owner
I want to turn on and off features
So That I can enjoy all the features of my pool automation

  Background user is connected to a pool system
  And user is viewing pool details

Scenario: DISPLAY ON FEATURE STATUS
  When a feature switch is displayed
  And the associated feature is ON
  Then the feature switch is set to ON
 
Scenario: DISPLAY OFF FEATURE STATUS
  When a feature switch is displayed
  And the associated feature is OFF
  Then the feature switch is set to OFF

Scenario: TOGGLE FEATURE SWITCH
  When the user toggles a feature switch
  Then the state of the on-screen switch is changed
  And the appropriate message is sent to the THW

Scenario: VIEW FEATURE STATUS CHANGES
  When the state of a feature changes
  Then the state of the on-screen switch is changed