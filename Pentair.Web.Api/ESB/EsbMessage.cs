﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Newtonsoft.Json;

namespace Pentair.Web.Api
{
    public partial class EsbClientConnection
    {
        private class EsbMessage
        {
            [JsonProperty(Required=Required.AllowNull)]
            public string DestinationHostName { get; private set; }

            [JsonProperty(Required=Required.Always)]
            public EndpointType DestinationType { get; private set; }

            [JsonProperty(Required = Required.Always)]
            public string SenderHostName { get; private set; }

            [JsonProperty(Required = Required.Always)]
            public int InstallationId { get; private set; }

            [JsonProperty(Required = Required.Always)]
            public Guid ClientConnectionId { get; private set; }

            [JsonProperty(Required = Required.Always)]
            public string SerializedMessage { get; private set; }

            public EsbMessage(
                string destHostName,     // Can be null for broadcast
                EndpointType destType,
                string sender,
                int installationId,
                Guid clientConnectionId, // Can be Guid.Empty for destType = EndpointType.THW
                string message)
            {
                this.DestinationHostName = destHostName;
                this.DestinationType = destType;
                this.SenderHostName = sender;
                this.InstallationId = installationId;
                this.ClientConnectionId = clientConnectionId;
                this.SerializedMessage = message;

            } // EsbMessage()

            public override string ToString()
            {
                return
                    String.Format("DestHostName: {0}, DestType: {1}, Sender: {2}, InstallationId: {3}, ClientConnectionId: {4}, Message: {5}",
                        this.DestinationHostName,
                        this.DestinationType,
                        this.SenderHostName,
                        this.InstallationId,
                        this.ClientConnectionId,
                        this.SerializedMessage
                    );

            } // ToString()

        } // class EsbMessage

    } // class EsbClientConnection
}