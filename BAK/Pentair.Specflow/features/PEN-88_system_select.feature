Feature: System Select (PEN-88)
As a Pool Owner
I want to switch between systems with my login
So That I do not have to login and logout to change pool access

Scenario: ONE POOL USER
  Given a user logged in via REMOTE  
  And who is authorized to only one pool
  When the user logs in
  Then the pool details page will be displayed

Scenario: MULTI POOL USER
  Given a user logged in via REMOTE 
  And who is authorized to more than one pool 
  When the user logs in
  Then the select a pool page will be displayed

Scenario: SELECT A POOL
  Given a user logged in
  And the the user is on the select a pool page
  #this when could be rephrased or broken into two steps but it's very clear as-is
  When the user selects a pool and submits their choice
  Then the pool details page will be displayed

Scenario: OFFLINE POOL
  Given a user logged in
  And the the user is on the select a pool page
  When the user selects a pool that is offline
  Then a non-modal error message is displayed

Scenario: REMEMBER LAST CONNECTION
  Given a user logged in
  And user has previously connected to a pool
  When the select a pool screen is displayed
  Then the default selection in the picker is the last pool they successfully connected to
