USE [PathfinderIntellicenter]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (
	SELECT * FROM sys.objects
	WHERE object_id = OBJECT_ID(N'dbo.pr_GetCityInfo')
	AND type in (N'P', N'PC')
)
DROP PROCEDURE dbo.pr_GetCityInfo
GO

-- =============================================
-- Author			:	Wayne Hui
-- Create date		:	09/06/2019
-- Parameters		:
--	@PostalCode		:	City to search
-- Description		:	Get the city information
--
-- Modified By			Date		Description
--	Wayne Hui			03/25/2020	Added NOT FOUND
-- =============================================
CREATE PROCEDURE dbo.pr_GetCityInfo
	@City		NVARCHAR(100)
	, @State	NVARCHAR(20) 
AS
BEGIN

	SET NOCOUNT OFF

----TEST
--DECLARE 
--	@City	NVARCHAR(100)
--	, @State	NVARCHAR(20) = NULL

--SET @City	= 'Emeryville'
--SET @State	= ''

----END TEST

	IF ( 
		SELECT COUNT(*) FROM [dbo].[PostalCodes]
		WHERE
			CityName = @City
			AND [ProvinceAbbr] = CASE WHEN @State IS NULL OR LEN(@State) = 0 THEN [ProvinceAbbr] ELSE @State END
		) > 0
	BEGIN
		SELECT 
			[CountryName]
			, [CityName]
			,[ProvinceAbbr]
			,[TimeZone]
			,[UTC]
			,[DST]
			,[Latitude]
			,[Longitude]
			,[PostalCode]
		FROM 
			[dbo].[PostalCodes]
		WHERE
			CityName = @City
			AND [ProvinceAbbr] = CASE WHEN @State IS NULL OR LEN(@State) = 0 THEN [ProvinceAbbr] ELSE @State END
	END
	ELSE
	BEGIN
		SELECT 
			CAST('' AS NVARCHAR(100)) AS [CountryName]
			, 'NOT FOUND' AS [CityName]
			, CAST('' AS NVARCHAR(20)) AS [ProvinceAbbr]
			, CAST('' AS NVARCHAR(20)) AS [TimeZone]
			, CAST(0 AS DECIMAL(4,1)) AS [UTC]
			, CAST('' AS NVARCHAR(1)) AS [DST]
			, CAST(0 AS DECIMAL(12,6)) AS [Latitude]
			, CAST(0 AS DECIMAL(12,6)) AS [Longitude]
			, CAST('' AS NVARCHAR(20)) AS [PostalCode]
	END

END

GO


--GRANT EXECUTE ON [dbo].[pr_GetCityInfo] TO PUBLIC
--GO
