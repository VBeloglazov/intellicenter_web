﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pentair.Domain.Simulator.Entity
{
    public class Pump
    {
        [Key]
        public int ID { get; set; }
        public string Name { get; set; }
        public UInt32 LastRevision { get; set; }
        public int ListPosition { get; set; }
        public string UserName { get; set; }
        public string CommAddress { get; set; }
        public bool PrimeEnable { get; set; }
        public int PrimeSensitivity { get; set; }
        public int PrimeDelay { get; set; }
        public int MaxPrimeFlow { get; set; }
        public int MaxPrimeTime { get; set; }
        public int SystemPrimeTime { get; set; }
        public int BackFlow { get; set; }
        public int BackTime { get; set; }
        public int RinseTime { get; set; }
        public int VacuumFlow { get; set; }
        public int VacuumTime { get; set; }
        public int MinSpeed { get; set; }
        public int MaxSpeed { get; set; }
        public int FilterPressure { get; set; }
        public string Password { get; set; }
        public int CurrentRPM { get; set; }
        public int CurrentFlow { get; set; }
        public int CurrentWatts { get; set; }
        public int CurrentPressure { get; set; }
        public string Status { get; set; }

        // Foreign Keys
        public int TypeId { get; set; }
        public int PumpSubTypeID { get; set; }
        public int RelayID { get; set; }
        public int BodyAssignmentID { get; set; }
        public int SharedBodyID { get; set; }
        public int CurrentCircuitID { get; set; }
        public int PumpListOrderID { get; set; }
        public int? SystemConfigID { get; set; }
    }
}