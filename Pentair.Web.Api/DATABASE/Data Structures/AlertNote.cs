﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pentair.Web.Api
{
    // TODO: Just a temporary solution to get the note based on the alert message
    public class AlertNote
    {
        // [Notes]
        public string AlertMode { get; set; }
        public string AlertMessage { get; set; }
        public string Note { get; set; }

    } // class AlertNote
}