﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pentair.Domain.Simulator.Entity
{
    public class Valve
    {
        [Key]
        public int ID { get; set; }
        public string Name { get; set; }
        public UInt32 LastRevision { get; set; }
        public int ListPosition { get; set; }
        public string UserName { get; set; }
        public bool Delay { get; set; }
        public int Timeout { get; set; }
        public int Time { get; set; }
        public int Status { get; set; }

        // Foreign Keys
        public int TypeID { get; set; }
        public int ValveSubTypeID { get; set; }
        public int RelayID { get; set; }
        public int CommObjectID { get; set; }
        public int CurrentCircuitID { get; set; }
        public int? SystemConfigID { get; set; }
    }
}