;--------------------------------------------------------------------------
; Setup.nsi
;
; This script installs the Pentair application and also remember the directories, 
; has uninstall support and (optionally) installs start menu shortcuts.
;
; It will install Setup.nsi into a directory that the user selects.
;--------------------------------------------------------------------------

; Include the required nsh script files
;-----------------------------------
!include "CheckMSIVersion.nsh"
!include "FileAssociation.nsh"
!include "LogicLib.nsh"
!include "MUI2.nsh"
!include "DotNetChecker.nsh"
!include nsDialogs.nsh
!include "FileFunc.nsh"
!include "Registry.nsh"

; The name of the installer
;-----------------------------------
Name "Pentair Simulator"

; Clear the nullsoft branding text
;-----------------------------------
BrandingText " "

XPStyle on

; The controls for the custom dialog page
;-----------------------------------
Var FileTypeAssociationDialog
Var YesRadioButton
Var NoRadioButton
Var YesRadioButton_State
Var NoRadioButton_State

Var CurrentRegValue
Var WindowsVersion

;Define Installer Properties
;-----------------------------------
VIProductVersion "1.6.0.0"
VIAddVersionKey /LANG=1033-English "ProductName" "Simulator"
VIAddVersionKey /LANG=1033-English "CompanyName" "Pentair"
VIAddVersionKey /LANG=1033-English "LegalTrademarks" ""
VIAddVersionKey /LANG=1033-English "LegalCopyright" "� Contractors Register, Inc."
VIAddVersionKey /LANG=1033-English "FileDescription" "Pentair Simulator"
VIAddVersionKey /LANG=1033-English "FileVersion" "1.6"

; The file to write
;-----------------------------------
OutFile "SimulatorSetup.exe"

; The default installation directory
;-----------------------------------
InstallDir "$PROGRAMFILES\Pentair"

; Registry key to check for directory (so if you install again, it will 
; overwrite the old one automatically)
;-----------------------------------
InstallDirRegKey HKLM "Software\Pentair" "Install_Dir"

; Request application privileges for Windows Vista
RequestExecutionLevel admin

;The icons for the installer and uninstaller
;-----------------------------------
!define MUI_ICON Pentair.ico
!define MUI_UNICON Pentair.ico

!define MUI_ABORTWARNING
!define PROC 0 

;The class and title for the application
;-----------------------------------
!define WNDCLASS "WindowsForms10.Window.8.app.0.202c666"                  
!define WNDTITLE "Pentair Simulator"

;Define a function that will kick off the application after the installation
;-----------------------------------
!define MUI_FINISHPAGE_RUN
!define MUI_FINISHPAGE_RUN_CHECKED
!define MUI_FINISHPAGE_RUN_TEXT "Start Pentair"
!define MUI_FINISHPAGE_RUN_FUNCTION "LaunchApplication"

;Installer Pages
;--------------------------------
  !insertmacro MUI_PAGE_WELCOME
  ; License page text 
  ;-------------------------------- 
  !insertmacro MUI_PAGE_INSTFILES
  !insertmacro MUI_PAGE_FINISH
  
;Uninstaller Pages
;--------------------------------
  !insertmacro MUI_UNPAGE_WELCOME
  !insertmacro MUI_UNPAGE_CONFIRM
  !insertmacro MUI_UNPAGE_INSTFILES
  !insertmacro MUI_UNPAGE_FINISH
 
; Language 
;--------------------------------
 !insertmacro MUI_LANGUAGE "English"
 
; The install section
;-----------------------------------
Section "Setup (required)"
   
  SectionIn RO

  AddSize 350000
  
  ; check windows MSI installer 3.0
  !insertmacro CheckMSIVersion
  
  AddSize 350000

  ; Set output path to the installation directory.
  SetOutPath $INSTDIR

    Call CheckBonjourInstall
    Pop $0
    IntCmp $0 1 +2
    Abort
      
  ; check .NET Version
  Call CheckAndDownloadDotNet451

  ; Check for the C++ Runtime assemblies
  ReadRegStr $1 HKLM "SOFTWARE\Microsoft\VisualStudio\10.0\VC\VCRedist\x86" "Installed"
  StrCmp $1 1 installed
  ClearErrors
  ; Breakpoint
  ;ExecWait "Notepad.exe"
  ExecWait '"$TEMP\vcredist_x86.exe" /q /l $INSTDIR/vcredist.log'
  IfErrors 0 +2
  Abort

  installed:

  ; Include the files for the install package
  File "Setup.nsi"
  File "PentairSimulator.exe"
  File "PentairSimulator.exe.config"
  File "EntityFramework.dll"
  File "EntityFramework.SqlServer.dll"
  File "Microsoft.AspNet.Identity.Core.dll"
  File "Microsoft.AspNet.Identity.EntityFramework.dll"
  File "Microsoft.AspNet.Identity.Owin.dll"
  File "Microsoft.Owin.Cors.dll"
  File "Microsoft.Owin.dll"
  File "Microsoft.Owin.Security.Cookies.dll"
  File "Microsoft.Owin.Security.dll"
  File "Microsoft.Owin.Security.OAuth.dll"
  File "Microsoft.WebSockets.dll"
  File "Newtonsoft.Json.dll"
  File "Ninject.dll"
  File "Owin.dll"
  File "Pentair.Domain.Client.dll" 
  File "Pentair.Web.Api.dll"
  File "SuperSocket.Common.dll"
  File "SuperSocket.Facility.dll"
  File "SuperSocket.SocketBase.dll"
  File "SuperSocket.SocketEngine.dll"
  File "SuperWebSocket.dll"
  File "System.Web.Cors.dll"
  File "System.Web.Http.dll"
  File "System.Web.Http.Owin.dll"
  File "System.Web.Http.WebHost.dll"
  File "WebSocket4Net.dll"
  File "log4net.dll"
  File "dropdatabase.sql"
  
  ; Write the installation path into the registry
  WriteRegStr HKLM "SOFTWARE\Pentair" "Install_Dir" "$INSTDIR"
  
  ; Write the uninstall keys for Windows
 ${GetSize} "$INSTDIR" "/S=0K" $0 $1 $2
 IntFmt $0 "0x%08X" $0
 WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Pentair" "EstimatedSize" "$0"

  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Pentair" "DisplayName" "Pentair"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Pentair" "Publisher" "Pentair"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Pentair" "DisplayVersion" "2.75.5"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Pentair" "UninstallString" '"$INSTDIR\Pentairuninstall.exe"'
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Pentair" "NoModify" 1
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Pentair" "NoRepair" 1
  ; Drop the local database tables
  ExecWait 'sqlcmd -S (local) -i dropdatabase.sql'
  WriteUninstaller "Pentairuninstall.exe"
  
 SectionEnd

; Start Menu shortcuts section
;-----------------------------------
Section "Start Menu Shortcuts"

  CreateDirectory "$SMPROGRAMS\Pentair"
  CreateShortCut "$SMPROGRAMS\Pentair\Pentair Simulator.lnk" "$INSTDIR\PentairSimulator.exe" "" "$INSTDIR\PentairSimulator.exe" 0
  CreateShortCut "$SMPROGRAMS\Pentair\Pentair Uninstall.lnk" "$INSTDIR\Pentairuninstall.exe" "" "$INSTDIR\Pentairuninstall.exe" 0
  CreateShortCut "$DESKTOP\Pentair Simulator.lnk" "$INSTDIR\PentairSimulator.exe" ""

SectionEnd


; Uninstall Section
;-----------------------------------
Section "Uninstall"
  SetShellVarContext all

  ; Remove registry keys
  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Pentair"
  DeleteRegKey HKLM "SOFTWARE\Pentair"
  
  ReadRegStr $CurrentRegValue HKCR .pdf ""
  
  ${If} $CurrentRegValue == "Pentair.PDF"
     !insertmacro APP_UNASSOCIATE "pdf" "Pentair.PDF"
  ${EndIf}     
  
 !insertmacro APP_UNASSOCIATE "ptx" "Pentair.PTX"
 !insertmacro APP_UNASSOCIATE "itu" "Pentair.ITU"



  ; Remove all the files 
  Delete "$INSTDIR\Setup.nsi"
  Delete "$INSTDIR\PentairSimulator.exe"
  Delete "$INSTDIR\PentairSimulatoruninstall.exe"
  Delete "$INSTDIR\PentairSimulator.exe.config"
  Delete "$INSTDIR\Antlr3.Runtime.dll"
  Delete "$INSTDIR\EntityFramework.dll.dll"
  Delete "$INSTDIR\EntityFramework.SqlSrever.dll"
  Delete "$INSTDIR\FizzWare.NBuilder.dll.dll"
  Delete "$INSTDIR\Microsoft.AspNet.Identity.Core.dll"
  Delete "$INSTDIR\Microsoft.AspNet.IdentityFramework.dll"
  Delete "$INSTDIR\Microsoft.AspNet.Identity.Owin.dll"
  Delete "$INSTDIR\Microsoft.Owin.Cors.dll"
  Delete "$INSTDIR\Microsoft.Owin.dll"
  Delete "$INSTDIR\Microsoft.Owin.Host.SystemWeb.dll"
  Delete "$INSTDIR\Microsoft.Owin.Security.Cookies.dll"
  Delete "$INSTDIR\Microsoft.Owin.Security.dll"
  Delete "$INSTDIR\Microsoft.Woin.Security.OAuth.dll"
  Delete "$INSTDIR\Microsoft.WebSockets.dll"
  Delete "$INSTDIR\Newtonsoft.Json.dll"
  Delete "$INSTDIR\Ninject.dll"
  Delete "$INSTDIR\Owin.dll"
  Delete "$INSTDIR\Pentair.Domain.Client.dll" 
  Delete "$INSTDIR\Pentair.Web.Api.dll"
  Delete "$INSTDIR\SuperSocket.Common.dll"
  Delete "$INSTDIR\SuperSocket.Facility.dll"
  Delete "$INSTDIR\SuperSocket.SocketBase.dll"
  Delete "$INSTDIR\SuperSocket.SocketEngine.dll"
  Delete "$INSTDIR\SuperWebSocket.dll"
  Delete "$INSTDIR\System.Det.Http.Formatting.dll"
  Delete "$INSTDIR\System.Web.Cors.dll"
  Delete "$INSTDIR\System.Web.Http.dll"
  Delete "$INSTDIR\System.Web.Http.Owin.dll"
  Delete "$INSTDIR\System.Web.Http.WebHost.dll"
  Delete "$INSTDIR\System.Web.Optimiztion.dll"
  Delete "$INSTDIR\WebGrease.dll"
  Delete "$INSTDIR\WebSocket4Net.dll"
  DELETE "$INSTDIR\dropdatabase.sql"

 
  ; Remove shortcuts, if any
  Delete "$SMPROGRAMS\Pentair\*.*"
  Delete "$DESKTOP\Pentair.lnk"

  ; Remove directories used
  RMDir /r "$PROGRAMFILES\Pentair"
  RMDir /r "$SMPROGRAMS\Pentair"
  RMDir /r "$DESKTOP\Pentair"
SectionEnd

; On install init
;-----------------------------------
Function .onInit
  
  ; Prevent multiple instances of installer
  System::Call 'kernel32::CreateMutexA(i 0, i 0, t "myMutex") i .r1 ?e'
  Pop $R0
 
  StrCmp $R0 0 +3
  MessageBox MB_OK|MB_ICONEXCLAMATION "The installer is already running."
  Abort

  ; shut down app before running the install
  FindWindow $0 "${WNDCLASS}" "${WNDTITLE}"
  
   StrCmp $0 0 continueInstall
     goto oninitAbort  
  
 oninitAbort:
 
    MessageBox MB_ICONSTOP|MB_OK "Installation cannot proceed when the application is already running. Please close it and try again."
    Abort

 continueInstall:
 
  ; Kill any running instances of Pentair running in the background
  Strcpy $0 "PentairSimulator.exe"
  KillProc::KillProcesses
 
  System::Call "kernel32::GetCurrentProcess() i .s"
  System::Call "kernel32::IsWow64Process(i s, *i .r0)"
  
  IntCmp $0 0 is32
  
  Strcpy $WindowsVersion "64"
  goto exitfunction
    
  is32:
    Strcpy $WindowsVersion "32"
    
  exitfunction:

FunctionEnd

; On Uninstall init
;-----------------------------------
Function un.onInit
  ; Use the 32 bit registry entries
  SetRegView 32

  ; Prevent multiple instances of the installer
  System::Call 'kernel32::CreateMutexA(i 0, i 0, t "myMutex") i .r1 ?e'
  Pop $R0
 
  StrCmp $R0 0 +3
  MessageBox MB_OK|MB_ICONEXCLAMATION "The UnInstaller is already running."
  Abort
  
  ; shut down app before uninstall
  FindWindow $0 "${WNDCLASS}" "${WNDTITLE}"
  
  StrCmp $0 0 continueUnInstall
  goto unoninitAbort

unoninitAbort:
    MessageBox MB_ICONSTOP|MB_OK "The application is running. Please close it and try uninstalling again."
    Abort
 
 continueUnInstall:
   ; Kill any running instances of Pentair running in the background
  Strcpy $0 "PentairSimulator.exe"
  KillProc::KillProcesses
 
  System::Call "kernel32::GetCurrentProcess() i .s"
  System::Call "kernel32::IsWow64Process(i s, *i .r0)"
FunctionEnd

; Function to create custom file type association page
;-----------------------------------
Function nsDialogsPage

        !insertmacro MUI_HEADER_TEXT "Default application" "Do you want Pentair to be the default application for PDF Documents?" 

    nsDialogs::Create 1018
    Pop $FileTypeAssociationDialog
    
    ${If} $FileTypeAssociationDialog == error
      Abort
    ${EndIf}
        
        ${NSD_CreateRadioButton} 0 10u 100% 10u "&Yes"
        Pop $YesRadioButton
        
        ${NSD_Check} $YesRadioButton
            
    ${NSD_CreateRadioButton} 0 30u 100% 10u "&No"
        Pop $NoRadioButton 
        
        ${NSD_Uncheck} $NoRadioButton
        
        nsDialogs::Show
    
FunctionEnd

Function nsDialogsPageLeave

    ${NSD_GetState} $YesRadioButton $YesRadioButton_State
    ${NSD_GetState} $NoRadioButton $NoRadioButton_State

FunctionEnd

;Function to create custom directory page, not to be displayed during an upgrade
;----------------------------------
Function dirPre

    ReadRegStr $0 HKLM "Software\Pentair" "Install_Dir"

    ${If} $0 != ""
        Abort
    ${EndIf}

FunctionEnd  

Function LaunchApplication
  ExecShell "" "$INSTDIR\PentairSimulator.exe"
FunctionEnd

 Function CheckAndDownloadDotNet451
    # Let's see if the user has the .NET Framework 4.5 installed on their system or not
    # Remember: you need Vista SP2 or 7 SP1.  It is built in to Windows 8, and not needed
    # In case you're wondering, running this code on Windows 8 will correctly return is_equal
    # or is_greater (maybe Microsoft releases .NET 4.5 SP1 for example)
 
    # Set up our Variables
    Var /GLOBAL dotNET451IsThere
    Var /GLOBAL dotNET_CMD_LINE
    Var /GLOBAL EXIT_CODE
 
        # We are reading a version release DWORD that Microsoft says is the documented
        # way to determine if .NET Framework 4.51 is installed
    DetailPrint "Checking for Version 4.51 Dot Net Assemblies"

    ReadRegDWORD $dotNET451IsThere HKLM "SOFTWARE\Microsoft\NET Framework Setup\NDP\v4\Full" "Release"
    IntCmp $dotNET451IsThere 378675 is_equal is_less is_greater
 
    is_equal:
        Goto done_compare_not_needed
    is_greater:
        # Useful if, for example, Microsoft releases .NET 4.5 SP1
        # We want to be able to simply skip install since it's not
        # needed on this system
        Goto done_compare_not_needed
    is_less:
        Goto done_compare_needed
 
    done_compare_needed:
        DetailPrint "Installing Microsoft Dot Net"

        #.NET Framework 4.51 install is *NEEDED*
 
        # Microsoft Download Center EXE:
        # Web Bootstrapper: http://go.microsoft.com/fwlink/?LinkId=225704
        # Full Download: http://go.microsoft.com/fwlink/?LinkId=225702
 
        # Setup looks for components\dotNET45Full.exe relative to the install EXE location
        # This allows the installer to be placed on a USB stick (for computers without internet connections)
        # If the .NET Framework 4.5 installer is *NOT* found, Setup will connect to Microsoft's website
        # and download it for you
 
        # Reboot Required with these Exit Codes:
        # 1641 or 3010
 
        # Command Line Switches:
        # /showrmui /passive /norestart
 
        # Silent Command Line Switches:
        # /q /norestart
 
 
        # Let's see if the user is doing a Silent install or not
        IfSilent is_quiet is_not_quiet
 
        is_quiet:
            StrCpy $dotNET_CMD_LINE "/q /norestart"
            Goto LookForLocalFile
        is_not_quiet:
            StrCpy $dotNET_CMD_LINE "/showrmui /passive /norestart"
            Goto LookForLocalFile
 
        LookForLocalFile:
            # Let's see if the user stored the Full Installer
            IfFileExists "$EXEPATH\components\dotNET451Full.exe" do_local_install do_network_install
 
            do_local_install:
                # .NET Framework found on the local disk.  Use this copy
 
                ExecWait '"$EXEPATH\components\dotNET451Full.exe" $dotNET_CMD_LINE' $EXIT_CODE
                Goto is_reboot_requested
 
            # Now, let's Download the .NET
            do_network_install:
 
                Var /GLOBAL dotNetDidDownload
                NSISdl::download "http://go.microsoft.com/fwlink/p/?LinkId=310158" "$TEMP\dotNET451Web.exe" $dotNetDidDownload
 
                StrCmp $dotNetDidDownload success fail
                success:
                    ExecWait '"$TEMP\dotNET451Web.exe" $dotNET_CMD_LINE' $EXIT_CODE
                    Goto is_reboot_requested
 
                fail:
                    MessageBox MB_OK|MB_ICONEXCLAMATION "Unable to download .NET Framework.  Pentair will be installed, but will not function without the Framework!"
                    Goto done_dotNET_function
 
                # $EXIT_CODE contains the return codes.  1641 and 3010 means a Reboot has been requested
                is_reboot_requested:
                    ${If} $EXIT_CODE = 1641
                    ${OrIf} $EXIT_CODE = 3010
                        SetRebootFlag true
                    ${EndIf}
 
    done_compare_not_needed:
        # Done dotNET Install
        Goto done_dotNET_function
 
    #exit the function
    done_dotNET_function:
 
    FunctionEnd

;--------------------------------
; CheckSqlInstall
;
; Written by Jim Hewitt (jhewitt@bhst.com)
;
Function CheckSqlInstall

    ClearErrors
 
    ReadRegStr $0 HKLM "SOFTWARE\Microsoft\MSSQLServer\MSSQLServer\CurrentVersion" "CurrentVersion"
    IfErrors SQLServerVersionError SQLServerFound
 
    SQLServerFound:
        ;Check the first digit of the version; must be 11
        StrCpy $1 $0 2
        StrCmp $1 "11" SQLServer2012Found SQLServerVersionError
 
    SQLServer2012Found:
        Push 1
        Goto ExitCheckMinSQLVersion
 
    SQLServerVersionError:
        MessageBox MB_OK|MB_ICONEXCLAMATION  "This product requires a minimum SQLServer version of 11.00.2100.60 (SQLServer 2012), found $1 installable from https://pathfinder.atlassian.net/wiki/display/PEN/06+Developer+Information; detected version $0. Setup will abort."
        Push 0
        Goto ExitCheckMinSQLVersion
 
    ExitCheckMinSQLVersion:

FunctionEnd

;--------------------------------
; CheckBonjour
;
; Written by Jim Hewitt (jhewitt@bhst.com)
;
; For this function Apple Bonjour must be installed on the computer.
Function CheckBonjourInstall
 
    ClearErrors
        ${registry::KeyExists} "HKEY_LOCAL_MACHINE\SOFTWARE\Apple Inc.\Bonjour" $0
        IfErrors ServiceNotFound ServiceFound
 
    ServiceNotFound:
        MessageBox MB_OK|MB_ICONEXCLAMATION  "This product requires the Bonjour Discovery Service installable from https://pathfinder.atlassian.net/wiki/display/PEN/06+Developer+Information. Setup will abort."
        Push 0
        Goto ExitCheckBonjour

    ServiceFound:
        Push 1
        Goto ExitCheckBonjour

    ExitCheckBonjour:
 
FunctionEnd

Function StrTok
  Exch $R1
  Exch 1
  Exch $R0
  Push $R2
  Push $R3
  Push $R4
  Push $R5
 
  ;R0 fullstring
  ;R1 tokens
  ;R2 len of fullstring
  ;R3 len of tokens
  ;R4 char from string
  ;R5 testchar
 
  StrLen $R2 $R0
  IntOp $R2 $R2 + 1
 
  loop1:
    IntOp $R2 $R2 - 1
    IntCmp $R2 0 exit
 
    StrCpy $R4 $R0 1 -$R2
 
    StrLen $R3 $R1
    IntOp $R3 $R3 + 1
 
    loop2:
      IntOp $R3 $R3 - 1
      IntCmp $R3 0 loop1
 
      StrCpy $R5 $R1 1 -$R3
 
      StrCmp $R4 $R5 Found
    Goto loop2
  Goto loop1
 
  exit:
  ;Not found!!!
  StrCpy $R1 ""
  StrCpy $R0 ""
  Goto Cleanup
 
  Found:
  StrLen $R3 $R0
  IntOp $R3 $R3 - $R2
  StrCpy $R1 $R0 $R3
 
  IntOp $R2 $R2 - 1
  IntOp $R3 $R3 + 1
  StrCpy $R0 $R0 $R2 $R3
 
  Cleanup:
  Pop $R5
  Pop $R4
  Pop $R3
  Pop $R2
  Exch $R0
  Exch 1
  Exch $R1
 
FunctionEnd