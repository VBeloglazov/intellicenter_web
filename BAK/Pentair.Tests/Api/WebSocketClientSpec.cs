﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Web.Razor.Parser;
using FizzWare.NBuilder;
using Machine.Fakes;
using Machine.Specifications;
using Microsoft.AspNet.Identity.EntityFramework;
using Pentair.Domain.Client.Entity;
using Pentair.Domain.Client.Helpers;
using Pentair.Domain.Client.Infrastructure;
using Pentair.Web.Api;
using Pentair.Web.Api.WebSockets;

namespace Pentair.Tests.Api
{
    [Subject("WebSocketClient")]
    public class WebSocketClientSpec : WithFakes
    {
        #region Database Records

        private static int property1 = 1;
        private static int property2 = 2;
        private static string user1 = "668c6b40-97e9-4064-8e2c-e3bcf732095b";
        private static string user2 = "768c6b40-97e9-4064-8e2c-e3bcf732095b";

        public class TestContent
        {
            private ApplicationUserManager identity;

            public IApiContext Data { get; private set; }
            public IQueryable<ApplicationUser> Users { get { return identity.Users; } }

            public TestContent()
            {
                var users = new FakeContext()
                {
                    ApplicationUsers =
                    {
                        new ApplicationUser()
                        {
                            Id = user1
                        }
                    }
                };

                identity = new ApplicationUserManager(
                    new UserStore<ApplicationUser>(
                        new ApplicationUserContext()));

                Data = new FakeContext()
                {
                    Associations =
                    {
                        new Association()
                        {
                            UserId = new Guid(user1),
                            InstallationId = property1,
                            AccessToken = StaticData.Admin
                        },
                        new Association()
                        {
                            UserId = new Guid(user2),
                            InstallationId = property1,
                            AccessToken = StaticData.Guest
                        },
                        new Association()
                        {
                            UserId = new Guid(user1),
                            InstallationId = property2,
                            AccessToken = StaticData.Admin
                        },
                        new Association()
                        {
                            UserId = new Guid(user2),
                            InstallationId = property2,
                            AccessToken = StaticData.Admin
                        }
                    },
                    Installations =
                    {
                        new Installation()
                        {
                            Name = "Property1",
                            InstallationId = property1,
                            Connected = true
                        },
                        new Installation()
                        {
                            Name = "property2",
                            InstallationId = property2,
                            Connected = false
                        }
                    },
                    ControlObjects =
                    {
                        new ControlObject()
                        {
                            OBJNAM = StaticData.Guest,
                            //OBJTYP = "PERMIT",
                            //SNAME = "Guest",
                            //SHOMNU = "slb"
                        },
                        new ControlObject()
                        {
                            OBJNAM = StaticData.Admin,
                            //OBJTYP = "PERMIT",
                            //SNAME = "Admin",
                            //SHOMNU = "slb"
                        }

                    },
                    UserInvitations =
                    {
                        new UserInvitation()
                        {
                            InvitationId = new Guid(user1),
                            EmailAddress = "jhewitt@bhst.com",
                            InstallationId = property1,
                            AccessToken = StaticData.Admin,
                            Status = "UNKNOWN",
                            EmailId = ""
                        }
                    }
                };
            }
        }

        #endregion

        #region Tests

        //public class WhenAnEmptyMessageIsPassedFromAProperty : WithFakes
        //{
        //    private static WebSocketClient subject;


        //    private Establish context = () => subject = new WebSocketClient();

        //    private Because of = () => subject.OnMessage("");

        //    private It PropertyIsNotYetAuthorized = () => subject.Id.ShouldEqual(new Guid("00000000000000000000000000000000"));
        //}

        //public class WhenAnEmptyMessageIsPassedFromAClient
        //{
        //    private static WebSocketClient subject;


        //    private Establish context = () => subject = new WebSocketClient(new Guid(property1), new Guid(user1));

        //    private Because of = () => subject.OnMessage("");

        //    private It PropertyIsNotYetAuthorized = () => subject.Id.ToString().ShouldEqual(property1);
        //}

        #endregion
    }
}
