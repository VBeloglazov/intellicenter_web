﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pentair.Domain.Simulator.Entity
{
    public class EmailAlert
    {
        [Key]
        public int ID { get; set; }
        public string Name { get; set; }
        public UInt32 LastRevision { get; set; }
        public UInt32 PriorRevision { get; set; }
        public int ListPosition { get; set; }
        public string Email { get; set; }
        public char PumpMask { get; set; }
        public char ChemMask { get; set; }
        public char SystemMask { get; set; }

        // Foreign Keys
        public int TypeID { get; set; }
    }
}