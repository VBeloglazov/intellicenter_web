﻿-- =============================================================
--
-- Author:		Vladimir Beloglazov
-- Create date: 02/23/2018
--
-- Creates:
--
--		Table Type:			None
--
--		Stored Procedure:	PGetThwLastHistoryTime
--
-- =============================================================

USE PathfinderIntelliCenter
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (
	SELECT * FROM sys.objects
	WHERE object_id = OBJECT_ID(N'dbo.PGetThwLastHistoryTime')
	AND type in (N'P', N'PC')
)
DROP PROCEDURE dbo.PGetThwLastHistoryTime
GO

-- -------------------------------------------------------------
--
-- Returns [ControlObjects].Revision max value or null.
--
-- -------------------------------------------------------------
CREATE PROCEDURE dbo.PGetThwLastHistoryTime
     @installationId AS int
AS
BEGIN

	SET NOCOUNT OFF

	-- Result set
	SELECT Max(Revision)
	FROM ControlObjects
	WHERE INSTALLATION = @installationId
	AND ParamType = 'History'
                                                     
END
GO
