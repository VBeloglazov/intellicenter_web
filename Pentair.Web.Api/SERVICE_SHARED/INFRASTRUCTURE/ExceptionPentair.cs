﻿/*
--------------------------------------------------------------------------------

    ExceptionPentair.cs

    Copyright © Pentair Water Pool and Spa, Inc. 2014

--------------------------------------------------------------------------------
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SERVICE_SHARED
{
    /// <summary>
    /// ExceptionManager.CustomizeException
    /// </summary>
    public class ExceptionPentair : Exception
    {
        private string OriginalStackTrace { get; set; }

        public ExceptionPentair(Exception e)
            : base(e.Message, e.InnerException)
        {
            if (e.Data.Contains("OriginalStackTrace"))
            {
                this.OriginalStackTrace = e.Data["OriginalStackTrace"] as string;
            }
            else
            {
                this.OriginalStackTrace = e.StackTrace;
            }

        } // ExceptionPentair(Exception)

        public ExceptionPentair(string sMessage)
            : base(sMessage)
        {
        } // ExceptionPentair(string)

        public ExceptionPentair(string sMessage, Exception InnerException)
            : base(sMessage, InnerException)
        {
        } // ExceptionPentair(string, Exception)

        /// <summary>
        /// Returns comprehensive exception description,
        /// including the calling stack trace.
        /// </summary>
        public string ErrorMessage(
            string errorHeader,
            bool addStackTrace = true)
        {
            string sError =
                String.Format("{0}Error:\n{1}",
                    !String.IsNullOrWhiteSpace(errorHeader) ? errorHeader + "\n\n" : String.Empty,
                    this.Message
                );

            if (addStackTrace)
            {
                if (!String.IsNullOrEmpty(this.StackTrace))
                {
                    sError += "\n\nStack Trace:\n" + this.StackTrace;
                }
                else
                {
                    if (this.OriginalStackTrace != null)
                    {
                        sError += "\n\nStack Trace:\n" + this.OriginalStackTrace;
                    }
                }
            }

            Exception eInnerException = this.InnerException;
            while (eInnerException != null)
            {
                sError += "\n\nInner Error:\n" + eInnerException.Message;

                if (addStackTrace)
                {
                    if (!String.IsNullOrEmpty(eInnerException.StackTrace))
                    {
                        sError += "\n\nStack Trace:\n" + eInnerException.StackTrace;
                    }
                }

                eInnerException = eInnerException.InnerException;
            }

            return sError;

        } // ErrorMessage()

    } // class ExceptionPentair
}
