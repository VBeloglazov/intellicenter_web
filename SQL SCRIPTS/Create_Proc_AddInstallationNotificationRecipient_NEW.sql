USE [PathfinderIntellicenter]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (
	SELECT * FROM sys.objects
	WHERE object_id = OBJECT_ID(N'dbo.pr_AddInstallationNotificationRecipient')
	AND type in (N'P', N'PC')
)
DROP PROCEDURE dbo.pr_AddInstallationNotificationRecipient
GO


-- =============================================
-- Author			:	Vladimir Beloglazov
-- Create date		:	04/05/2018
-- Parameters		:
--	@installationId	:	Device ID
--	@name			:	recipient name
--	@email			:	email address
--	@phone			:	phone number
-- Description		:	Save the recipient's info
--						Original: PAddInstallationNotificationRecipient
--
-- Modified By			Date		Description
--	Wayne Hui			02/27/2019	Added @phone
-- =============================================
CREATE PROCEDURE [dbo].[pr_AddInstallationNotificationRecipient]
	 @installationId	AS int
	,@name				AS nvarchar(255)
	,@email				AS nvarchar(255)
	,@phone				AS nvarchar(20) = NULL
AS
BEGIN

	SET NOCOUNT OFF

----TEST
--DECLARE
--	@installationId	AS int
--	,@name				AS nvarchar(255)
--	,@email				AS nvarchar(255)
--	,@phone				AS nvarchar(20)
	
--SET @installationId	= 3948
--SET @name			= 'Wayne XY'
--SET @email			= 'wayneh722@gmail.com'
--SET @phone			= null
----END TEST

	DECLARE @RecipientNewId TABLE (RecipientId int)

	INSERT INTO NotificationRecipients (
		 InstallationId
	    ,Name
	    ,EmailAddress1
		,Phone1
    )
	OUTPUT INSERTED.RecipientId INTO @RecipientNewId
    SELECT
	     @installationId	-- InstallationId
        ,@name				-- Name
	    ,@email				-- EmailAddress1
		,@phone				-- Phone1
    WHERE NOT EXISTS (
	    SELECT 1 FROM NotificationRecipients
	    WHERE InstallationId = @installationId
        AND Name = @name
	    AND EmailAddress1 = @email
    )

	-- Result set
	SELECT RecipientId
	FROM @RecipientNewId

END
GO


GRANT EXECUTE ON [dbo].[pr_AddInstallationNotificationRecipient] TO PUBLIC
GO
