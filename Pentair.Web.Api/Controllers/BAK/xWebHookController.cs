﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Newtonsoft.Json;
using Pentair.Domain.Client.Infrastructure;
using Pentair.Web.Api.Helpers;

using SERVICE_SHARED;

namespace Pentair.Web.Api.Controllers
{
    // TODO: VB - 2b removed?

    [AllowAnonymous]
    [RoutePrefix("api/Webhook")]
    public class WebHookController : ApiController
    {
        private const string EVENT_SOURCE = "WebHookController";

        [HttpHead]
        public HttpResponseMessage Header()
        {
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpPost]
        public HttpResponseMessage Post()
        {
            var message = HttpContext.Current.Request.Form["mandrill_events"];

            LogManager.LogWarningAsync(
                source: EVENT_SOURCE,
                eventBriefDescription: "WebHookController - in use!",
                installationId: 0,
                userId: Guid.Empty,
                connectionId: Guid.Empty,
                eventFullDescription: String.Format("Message:\n{0}", message),
                saveEventLog: true
            );

            try
            {
                string validJson = message.Replace("mandrill_events=", "");
                var mandrillEventList = JsonConvert.DeserializeObject<List<MandrillEvent>>(validJson);

                foreach (MandrillEvent mandrillEvent in mandrillEventList)
                {
                    var bouncedStatuses = new[] { "hard_bounce", "soft_bounce" };
                    if (bouncedStatuses.Contains(mandrillEvent.@event) && !String.IsNullOrEmpty(mandrillEvent.msg.email))
                    {
                        using (var db = new ApiContext())
                        {
                            var invite = db.UserInvitations.FirstOrDefault(x => x.EmailId == mandrillEvent.msg._id);
                            if (invite != null)
                            {
                                invite.Status = "BOUNCED";
                                db.SaveChanges();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.LogErrorAsync(
                    source: EVENT_SOURCE,
                    eventBriefDescription: "WebHookController - in use, but failed!",
                    installationId: 0,
                    userId: Guid.Empty,
                    connectionId: Guid.Empty,
                    eventFullDescription:
                        ExceptionManager.CustomizeException(ex).ErrorMessage(
                            errorHeader: null,
                            addStackTrace: true
                        )
                );
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }
    }

    internal class MandrillEvent
    {
        public string @event { get; set; }
        public Msg msg { get; set; }
        public string _id { get; set; }
        public int ts { get; set; }
    }

    internal class Metadata
    {
        public int user_id { get; set; }
    }

    internal class Msg
    {
        public int ts { get; set; }
        public string subject { get; set; }
        public string email { get; set; }
        public string sender { get; set; }
        public List<string> tags { get; set; }
        public string state { get; set; }
        public Metadata metadata { get; set; }
        public string _id { get; set; }
        public string _version { get; set; }
        public string bounce_description { get; set; }
        public int bgtools_code { get; set; }
        public string diag { get; set; }
    }


}
