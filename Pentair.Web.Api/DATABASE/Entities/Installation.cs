﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Pentair.Web.Api
{
    public class Installation
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int InstallationId { get; set; }

        [MaxLength(255)]
        public string Name { get; set; }

        public Guid OwnerUserId { get; set; }

        [MaxLength(255)]
        public string Secret { get; set; }

        public long LastChange { get; set; }

        public long LastHistory { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(TypeName = "DateTime2")]
        public DateTime CreateDateTime { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [Column(TypeName = "DateTime2")]
        public DateTime UpdateDateTime { get; set; }

    } // class Installation
}
