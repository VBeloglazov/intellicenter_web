﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Pentair.Domain.Client.Helpers.Commands
{
    public class GetParamListResponse : ParamCommandResponse
    {
        public GetParamListResponse(
            string messageID,
            string response,
            List<ParamObject> objectList
            )
            : base(CommandString.GetParamListResponse, messageID, response, objectList)
        {
        } // GetParamListResponse()

        /// <summary>
        /// Always returns an object, valid or not. If invalid, check the DeserializationError property value.
        /// </summary>
        public static new GetParamListResponse Deserialize(
            string serializedMessage)
        {
            GetParamListResponse command = null;

            try
            {
                command = JsonConvert.DeserializeObject<GetParamListResponse>(serializedMessage);
                command.IsValid = true;
            }
            catch (Exception e)
            {
                // Deserialization as GetParamListResponse failed

                StringBuilder sbError = new StringBuilder();

                sbError.Append("Cannot deserialize as GetParamListResponse. " + e.Message);

                // Try to get at least the base message data
                ParamCommandResponse baseCommand = ParamCommandResponse.Deserialize(serializedMessage);

                if (!baseCommand.IsValid)
                {
                    sbError.Append("\n" + baseCommand.DeserializationError);
                }

                command = new GetParamListResponse(
                    baseCommand.MessageID,
                    response: ResponseString.BadRequest,
                    objectList: null)
                {
                    IsValid = false,
                    DeserializationError = sbError.ToString()
                };
            }

            return command;

        } // Deserialize()

    } // class GetParamListResponse
}