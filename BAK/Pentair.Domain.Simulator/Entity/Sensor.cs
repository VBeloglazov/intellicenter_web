﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pentair.Domain.Simulator.Entity
{
    public class Sensor
    {
        [Key]
        public int ID { get; set; }
        public string Name { get; set; }
        public UInt32 LastRevision { get; set; }
        public string UserName { get; set; }
        public float Calibration { get; set; }
        public bool Status { get; set; }
        public float Value { get; set; }

        // Foreign Keys
        public int TypeID { get; set; }
    }
}