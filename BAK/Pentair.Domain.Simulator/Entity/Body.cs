﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Microsoft.SqlServer.Server;

namespace Pentair.Domain.Simulator.Entity
{
    public class Body
    {
        [Key]
        [MaxLength(16)]
        public string OBJNAM { get; set; }
        public UInt32 OBJREV { get; set; }
        [MaxLength(16)]
        public string OBJTYP { get; set; }
        [MaxLength(16)]
        public string STATIC { get; set; }
        [MaxLength(16)]
        public string SUBTYP { get; set; }
        public int LISTORD { get; set; }
        [MaxLength(128)]
        public string HNAME { get; set; }
        [MaxLength(128)]
        public string SNAME { get; set; }
        [MaxLength(16)]
        public string PRIM { get; set; }
        [MaxLength(16)]
        public string SEC { get; set; }
        [MaxLength(16)]
        public string SHARE { get; set; }
        [MaxLength(16)]
        public string PARENT { get; set; }
        [MaxLength(16)]
        public string FILTER { get; set; }
        [MaxLength(16)]
        public string PUMP { get; set; }
        [MaxLength(16)]
        public string MASTER { get; set; }
        [MaxLength(16)]
        public string HTSRC { get; set; }
        [MaxLength(16)]
        public string HEATER { get; set; }
        public int VOL { get; set; }
        [MaxLength(16)]
        public string STATUS { get; set; }
        [MaxLength(16)]
        public string TEMP { get; set; }
        [MaxLength(16)]
        public string MIN { get; set; }
        [MaxLength(16)]
        public string MAX { get; set; }
        public int BADGE { get; set; }

    }
}