﻿'use strict';
var PWC = PWC || {};


PWC.directive('circuitGroupPowerButtons', ['PropertyService', function (PropertyService) {
    return {
        restrict: 'EA',
        replace: true,
        scope: {
            targetCircuit: '=',
            disabled: '='
        },
        template: '<div class="switch-toggle" style="width:190px">' +
                      '<button id="{{targetCircuit.OBJNAM+\'_OffBtn\'}}" class="btn off" ng-disabled="disabled" ng-click="powerOff($event)">Off</button>' +
                      '<button id="{{targetCircuit.OBJNAM+\'_OnBtn\'}}" class="btn on" ng-disabled="disabled"  ng-click="powerOn($event)">On</button>' +
                  '</div>',
        link: function (scope, element, attrs) {
            scope.powerOff = function (event) {
                if (!scope.disabled) {
                    PropertyService.SetValue(scope.targetCircuit.OBJNAM, 'STATUS', 'OFF');
                    event.stopPropagation();
                }
            }
            scope.powerOn = function (event) {
                if (!scope.disabled) {
                    PropertyService.SetValue(scope.targetCircuit.OBJNAM, 'STATUS', 'ON');
                    event.stopPropagation();
                }
            }
        }
    };
}]);

