﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Windows;
using Bonjour;
using Pentair.Desktop.Simulator.BusinessLogic;
using Pentair.Domain.Client.Entity;

namespace Pentair.Desktop.Simulator.Services
{
    /// <summary>
    /// 
    /// </summary>
    public class Discovery : IDisposable
    {
        #region Private Properties

        private DNSSDEventManager _eventManager = null;
        private DNSSDService _service = null;
        private DNSSDService _registrar = null;
        private DNSSDService _browser = null;
        private DNSSDService _resolver = null;
        private readonly Socket _socket = null;
        private string _name;
        private const int BufferSize = 1024;
        private readonly byte[] _buffer = new byte[BufferSize];
        private Property _host = null;

        #endregion

        #region Events and Delegates

        public event OnRegister OnRegisterEvent;

        public delegate void OnRegister(Object sender, DiscoveryEventArgs e);

        public event OnMessage OnMessageEvent;

        public delegate void OnMessage(object sender, DiscoveryEventArgs e);

        #endregion

        #region Public Properties

        public ConnectionStatus Status { get; private set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public Discovery()
        {
        }

        public void Dispose()
        {
            if (null != _registrar)
            {
                _registrar.Stop();
            }
            if (null != _browser)
            {
                _browser.Stop();
            }
            if (null != _resolver)
            {
                _resolver.Stop();
            }
            _eventManager.ServiceRegistered -= ServiceRegistered;
            _eventManager.ServiceFound -= ServiceFound;
            _eventManager.ServiceLost -= ServiceLost;
            _eventManager.ServiceResolved -= ServiceResolved;
            _eventManager.QueryRecordAnswered -= QueryAnswered;
            _eventManager.OperationFailed -= OperationFailed;
        }

        #endregion

        #region Public Methods
        public void BrowseServices(string serviceType)
        {
            if (null != _host)
            {
                RegisterService();
            }
        }


        public void ChangeHost(Property host)
        {
            _host = host;
            RegisterService();
        }
        #endregion

        #region Private Methods

        private void RegisterService()
        {
            var broadcastName = string.IsNullOrEmpty(_host.Name) ? "Unassigned" : _host.Name;
            _name = string.Format("Pentair: {0}", broadcastName);
            if (null == _eventManager)

            {
                try
                {
                    _service = new DNSSDService();
                }
                catch
                {
                    throw (new Exception("Discovery service is not available"));
                }

                _eventManager = new DNSSDEventManager();
                _eventManager.ServiceRegistered += ServiceRegistered;
                _eventManager.ServiceFound += ServiceFound;
                _eventManager.ServiceLost += ServiceLost;
                _eventManager.ServiceResolved += ServiceResolved;
                _eventManager.QueryRecordAnswered += QueryAnswered;
                _eventManager.OperationFailed += OperationFailed;
            }
            try
            {
                _registrar = _service.Register(
                    0x0000,
                    0x0000,
                    _name,
                    "_http._tcp",
                    null,
                    null,
                    (ushort)BusinessLogic.Helpers.LocalHostPort,
                    null,
                    _eventManager);
                Status = ConnectionStatus.Enabled;
            }
            catch
            {
                Status = ConnectionStatus.Disabled;
            }
        }
        #endregion

        #region Public call back Methods
        /// <summary>
        /// Register callback
        /// called by DNSServices core in response to a registration request
        /// </summary>
        /// <param name="service"></param>
        /// <param name="flags"></param>
        /// <param name="name"></param>
        /// <param name="regType"></param>
        /// <param name="domain"></param>
        public void ServiceRegistered(
            DNSSDService service,
            DNSSDFlags flags,
            String name,
            String regType,
            String domain
            )
        {
            _name = name;
            var message = String.Empty;
            Status = ConnectionStatus.Connected;

//            _browser = _service.Browse(0, 0, "_http._tcp", null, _eventManager);
            message = String.Format("{0} registered with Bonjour.", _name);

            if (null != OnRegisterEvent)
            {
                OnRegisterEvent(this, new DiscoveryEventArgs(Status, message));
            }
        }

        /// <summary>
        /// Called by DNSServices core in response to a Service Found event
        /// </summary>
        /// <param name="sref"></param>
        /// <param name="flags"></param>
        /// <param name="ifIndex"></param>
        /// <param name="serviceName"></param>
        /// <param name="regType"></param>
        /// <param name="domain"></param>
        public void ServiceFound(
            DNSSDService sref,
            DNSSDFlags flags,
            uint ifIndex,
            String serviceName,
            String regType,
            String domain
            )
        {
            var peer = new PeerData
            {
                InterfaceIndex = ifIndex,
                Name = serviceName,
                Type = regType,
                Domain = domain,
                Address = null
            };

            if (null != OnMessageEvent)
            {
                var message = string.Format("Found service {0} of type {1} available at {2}",
                    serviceName,
                    regType,
                    domain);
                OnMessageEvent(this, new DiscoveryEventArgs(Status, message));
            }
        }

        /// <summary>
        /// Called by DNSServices core in response to a service lost event
        /// </summary>
        /// <param name="sref"></param>
        /// <param name="flags"></param>
        /// <param name="ifIndex"></param>
        /// <param name="serviceName"></param>
        /// <param name="regType"></param>
        /// <param name="domain"></param>
        public void ServiceLost(
            DNSSDService sref,
            DNSSDFlags flags,
            uint ifIndex,
            String serviceName,
            String regType,
            String domain
            )
        {
            var peer = new PeerData
            {
                InterfaceIndex = ifIndex,
                Name = serviceName,
                Type = regType,
                Domain = domain,
                Address = null
            };

            if (null != OnMessageEvent)
            {
                var message = string.Format("Lost service {0} of type {1} available at {2}",
                    serviceName,
                    regType,
                    domain);
                OnMessageEvent(this, new DiscoveryEventArgs(Status, message));
            }
        }

        /// <summary>
        /// Called by DNSServices core in response to a Service Resolved event
        /// </summary>
        /// <param name="sref"></param>
        /// <param name="flags"></param>
        /// <param name="ifIndex"></param>
        /// <param name="fullName"></param>
        /// <param name="hostName"></param>
        /// <param name="port"></param>
        /// <param name="txtRecord"></param>
        public void ServiceResolved(
            DNSSDService sref,
            DNSSDFlags flags,
            uint ifIndex,
            String fullName,
            String hostName,
            ushort port,
            TXTRecord txtRecord
            )
        {
            _resolver.Stop();
            _resolver = null;

            try
            {
                _resolver = _service.QueryRecord(
                    0, 
                    ifIndex, 
                    hostName, 
                    DNSSDRRType.kDNSSDType_A, 
                    DNSSDRRClass.kDNSSDClass_IN, 
                    _eventManager);
            }
            catch
            {
                throw (new Exception("QueryRecord Failed"));
            }
            if (null != OnMessageEvent)
            {
                var message = string.Format("Pentair service has been resolved");
                OnMessageEvent(this, new DiscoveryEventArgs(Status, message));
            }
        }

        /// <summary>
        /// Called by DNSServices core in response to a Query Answered event
        /// </summary>
        /// <param name="service"></param>
        /// <param name="flags"></param>
        /// <param name="ifIndex"></param>
        /// <param name="fullName"></param>
        /// <param name="rrtype"></param>
        /// <param name="rrclass"></param>
        /// <param name="rdata"></param>
        /// <param name="ttl"></param>
        public void QueryAnswered(
            DNSSDService service,
            DNSSDFlags flags,
            uint ifIndex,
            String fullName,
            DNSSDRRType rrtype,
            DNSSDRRClass rrclass,
            Object rdata,
            uint ttl
            )
        {
            _resolver.Stop();
            _resolver = null;
            if (null != OnMessageEvent)
            {
                var message = string.Format("Query resolved");
                OnMessageEvent(this, new DiscoveryEventArgs(Status, message));
            }
        }

        public void OperationFailed
            (
            DNSSDService service,
            DNSSDError error
            )
        {
            if (null != OnMessageEvent)
            {
                var message = string.Format("Error: {0} - {1}", service, error);
                OnMessageEvent(this, new DiscoveryEventArgs(Status, message));
            }
            MessageBox.Show("Operation returned an error code " + error, "Error");
        }

        /// <summary>
        /// Called when there is data to be read on the socket
        /// </summary>
        /// <param name="msg"></param>
        public void OnReadMessage(String msg)
        {
            int rgb = 0;

            for (int i = 0; i < msg.Length && msg[i] != ':'; i++)
            {
                rgb = rgb ^ ((int) msg[i] << (i%3 + 2)*8);
            }
        }

        /// <summary>
        /// Called when there is data to be read on the socket
        /// </summary>
        /// <param name="msg"></param>
        //
        private void OnReadSocket(IAsyncResult ar)
        {
            try
            {
                int read = _socket.EndReceive(ar);

                if (read > 0)
                {
                    String msg = Encoding.UTF8.GetString(_buffer, 0, read);
                    //Invoke(_readMessageCallback, new Object[] {msg});
                }

                _socket.BeginReceive(_buffer, 0, BufferSize, 0, new AsyncCallback(OnReadSocket), this);
            }
            catch
            {
            }
        }
    }

    #endregion

    #region Helper Classes

    public class DiscoveryEventArgs
    {
        public ConnectionStatus Status { get; private set; }
        public string Message { get; private set; }

        public DiscoveryEventArgs(ConnectionStatus status, string message)
        {
            Status = status;
            Message = message;
        }
    }


    /// <summary>
    /// Class to manage data about a peer on the network
    /// </summary>
    public class PeerData
    {
        public uint InterfaceIndex;
        public String Name;
        public String Type;
        public String Domain;
        public IPAddress Address;
        public int Port;

        public override String
            ToString()
        {
            return Name;
        }

        /// <summary>
        /// Override for the Equals operator
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public override bool Equals(object other)
        {
            bool result = false;

            if (other != null)
            {
                if ((object) this == other)
                {
                    result = true;
                }
                else if (other is PeerData)
                {
                    PeerData otherPeerData = (PeerData) other;

                    result = (this.Name == otherPeerData.Name);
                }
            }

            return result;
        }

        /// <summary>
        /// Override for the int operator
        /// </summary>
        /// <returns></returns>
        public override int
            GetHashCode()
        {
            return Name.GetHashCode();
        }
    }

    #endregion
}