﻿'use strict';
describe('DashboardController', function () {
    var PropertyService;
    var PropertyModel;
    var $location;
    var $scope;
    var SystemProtectionController;
    var $controller;
    beforeEach(module('PentairWebClient'));

    beforeEach(inject(function (_$controller_, _$rootScope_, _PropertyService_, _PropertyModel_, _$location_) {
        $scope = _$rootScope_.$new();
        PropertyService = _PropertyService_;
        PropertyModel = _PropertyModel_;
        $location = _$location_;
        $controller = _$controller_;

        PropertyModel.SecurityEnabled = false;
        SystemProtectionController = $controller("SystemProtectionController", { $scope: $scope });
        spyOn($location, 'path');
        spyOn(PropertyService, 'SetValue');
    }));

    describe('default Variables', function () {
        it('should set the following default values', function () {
            PropertyModel.SecurityEnabled = false;
            SystemProtectionController = $controller("SystemProtectionController", { $scope: $scope });
            expect($scope.SecurityValue).toBe('OFF');
        });
         it('should set the following default values if security is enabled', function () {
            PropertyModel.SecurityEnabled = true;
            SystemProtectionController = $controller("SystemProtectionController", { $scope: $scope });
            expect($scope.SecurityValue).toBe('ON');
        });
    });

    describe('GoToConfigPage', function () {
        it('should take the user the main page', function () {
            $scope.GoToConfigPage();
            expect($location.path).toHaveBeenCalledWith('/configure');
        });
    });

    describe('ToggleSecurity', function () {
        it('should hide password box if ON is provided', function () {
            $scope.ShowPassWrdBox.show = false;
            $scope.ToggleSecurity('ON');
            expect($scope.ShowPassWrdBox.show).toBe(true);
            expect($scope.ShowPassWrdBox.password).toBe('');
        });
        it('should hide password box if OFF is provided', function () {
            $scope.ShowPassWrdBox.show = true;
            $scope.ToggleSecurity('OFF');
            expect($scope.ShowPassWrdBox.show).toBe(false);
            expect($scope.ShowPassWrdBox.password).toBe('');
        });

        it('should hide password box if OFF is provided', function () {
            PropertyModel.SecurityEnabled = true;
            $scope.ToggleSecurity('OFF');
            expect($scope.ShowPassWrdBox.show).toBe(false);
            expect($scope.ShowPassWrdBox.password).toBe('');
            expect(PropertyService.SetValue).toHaveBeenCalledWith('UFFFE', 'PASSWRD', '');
        });
    });

    describe('SetSecurity', function () {
        it('should set system passcode via SetValue and hide password box', function () {
            $scope.ShowPassWrdBox.password = 'testPassword';
            $scope.SetSecurity();
            expect(PropertyService.SetValue).toHaveBeenCalledWith('UFFFE', 'PASSWRD', 'testPassword');
            expect($scope.ShowPassWrdBox.show).toBe(false);
        });
    });
});
