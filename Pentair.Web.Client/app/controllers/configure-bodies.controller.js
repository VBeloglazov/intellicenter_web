'use strict';
var PWC = PWC || {};
PWC.controller('ConfigureBodiesController', [
  '$rootScope', '$scope', '$location', '$interpolate', 'PropertyService', 'PropertyModel', '$filter', 'Notification',
  function ($rootScope, $scope, $location, $interpolate, propertyService, propertyModel, $filter, Notification) {
    $scope.$parent.Layout = 'layout-base';
    $scope.property = propertyModel;
    $scope.accordionStateByObjectNames = {};
    $scope.cBodies = $scope.property.Bodies;
    updatePanels();    

    $rootScope.editMode.bodies = {};
    
    $scope.circuitNames = $scope.property.Circuits.map(function (circuit) {
      var circuitName = {};
      circuitName.sName = circuit.SNAME;
      circuitName.objectName = circuit.OBJNAM;
      return circuitName;
    });
    // for unassigned circuits
    $scope.circuitNames.unshift(
      {
        sName: '---',
        objectName: '00000'
      });

    $scope.errorSave = function(errorMessage) {
      Notification.error({message: 'Failed!' + ' ' + errorMessage, delay: 1000});
    };

    $scope.successSave = function() {
      Notification.success({message: 'Saved!', delay: 1000});
    };

    $scope.successCancel = function() {
      Notification.success({message: 'Cancelled!', delay: 1000});
    };

    $scope.combineName = function (obj) {
      return $interpolate('{{hName}} - {{sName}}')(obj);
    };

    $scope.getPanelName = function (obj) {
        return $interpolate('{{sName}}')(obj);
    };

    $scope.getBodyName = function (obj) {
        return $interpolate('{{sName}}')(obj);
    };

    $scope.setEditMode = function (obj) {
      $rootScope.editMode.bodies[obj.objectName] = true;
      //obj.editMode = true;
    };

    $scope.requestUpdateBodies = function () {
        //angular.forEach($scope.cBodies , funtion(body) { } );
        //console.log("-------  request bodies-------- ");
    };

    $scope.cancelEdit = function (index, parentIndex) {
      $scope.panels = angular.copy($scope.panelsBeforeEdit);
      $scope.panels[parentIndex].bodies[index].editMode = false;
      $rootScope.editMode.bodies[$scope.panels[parentIndex].bodies[index].objectName] = false;
      $rootScope.forms.bodies[parentIndex][index].$setPristine();
      $scope.successCancel();
    };

    $scope.saveBody = function (body, parentIndex, index) {
      var params = {};
      params.SNAME = body.sName;
      if(body.capacity < 1) {
        body.capacity = 1;
      }
      if(body.capacity > 2000000) {
        body.capacity = 2000000;
      }
      params.VOL = body.capacity + '';
      if(!body.isPool) {
        params.MANUAL = (body.spaManualHeat) ? 'ON' : 'OFF';
      }
      params.PRIM = body.shortcut1;
      params.SEC = body.shortcut2;
      params.ACT1 = body.shortcut3;
      params.ACT2 = body.shortcut4;
      params.ACT3 = body.shortcut5;
      params.ACT4 = body.shortcut6;

      $scope.panels = [];
      $rootScope.editMode.bodies[body.objectName] = false;
      propertyService.SetParams(body.objectName, params).then(function () {        
        propertyService.GetHardwareDefinition().then(function () {
          propertyService.getUpdatedBody(body.objectName).then(function(results) {
            updatePanels(results);              
            $rootScope.forms.bodies[parentIndex][index].$setPristine();
            $scope.successSave();
          });
        });        
      });
    };

    $scope.showCircuitSNameFromObjectName = function (objectName) {
      var circuitName = $scope.circuitNames.filter(function (circuitName) {
        return circuitName.objectName === objectName;
      });

      if(circuitName[0] != undefined) {
        return circuitName[0].sName;
      }
    };
    
    

    $scope.$on('UpdatedBody', function (objname, params) {
        
        //console.log(" processing broadcast UpdatedBody...");
        angular.forEach($scope.cBodies, function (body) {            
            if (params.objnam == body.objnam) { // check match
                if (params.VOL) {
                    body.capacity = params.VOL;
                    //console.log(" new body capacity " + params.VOL);
                }
            }
        });           
    });

    $scope.$on('NotifyList', function (objname, $body , obj) {

        //console.log("---- processing notify body ..."+$body );
        angular.forEach($scope.panels, function (panel) {
            angular.forEach(panel.bodies, function (localbody) {
            if (obj.objnam === localbody.hName) { // check match
                    if (obj.params.VOL) {
                        localbody.capacity = obj.params.VOL;
                        localbody.VOL = obj.params.VOL;
                        //console.log(" set body capacity " + localbody.capacity);                        
                    }
                    if (obj.params.SNAME) {
                        localbody.sName = obj.params.SNAME;                        
                        //console.log(" set name  " + localbody.sName);
                    }
                }
            });
        });

    });

    $scope.$on('ObjectUpdated', function () {
        //console.log(" processing broadcast ObjectUpdated");
    });


    function updatePanels(results) {
      $scope.panels = $scope.property.hardwareDefinition.map(function (hPanel) {
        var panel = {};
        panel.objectName = hPanel.objnam;
        panel.hName = hPanel.params.HNAME;
        panel.sName = hPanel.params.SNAME;
        panel.bodies = hPanel.params.OBJLIST.map(function (hModule) {
          var bodies = [];
          if(hModule.params.CIRCUITS){ 
            bodies = bodies.concat(hModule.params.CIRCUITS
              .filter(function(filterBody) { 
                // needed to only grab bodies here with the most recent API changes
                if(filterBody.params.OBJTYP && filterBody.params.OBJTYP.indexOf('BODY') >= 0) {
                  return filterBody;
                }
              })
              .map(function (hBody) {
                var body = {};
                body.objectName = hBody.objnam;
                body.sName = hBody.params.SNAME;
                body.listOrder = parseInt(hBody.params.LISTORD);
                body.shortcut1 = hBody.params.PRIM;
                body.shortcut2 = hBody.params.SEC;
                body.shortcut3 = hBody.params.ACT1;
                body.shortcut4 = hBody.params.ACT2;
                body.shortcut5 = hBody.params.ACT3;
                body.shortcut6 = hBody.params.ACT4;
                body.capacity = parseInt(hBody.params.VOL || '0');
                body.isPool =  (hBody.params.SUBTYP !== 'SPA') ? true : false;
                // need to get the manual, hname field from a different object
                // or updated fields
                var configBody = $scope.property.Objects[body.objectName];
                if(configBody) {
                  if(results) {
                    var result = results.filter(function(item) {
                        return item.OBJNAM === body.objectName;
                    })[0];
                    if(result) {
                        configBody.HNAME = result.HNAME;
                        configBody.MANUAL = result.MANUAL;
                    }
                  }
                  body.hName = configBody.HNAME;
                  if(!body.isPool) {
                    body.spaManualHeat = (hBody.params.MANUAL === 'ON');
                  } 
                }
                return body;
              })
            );
          }
          return bodies;
        });
        // flatten the module array
        panel.bodies = [].concat.apply([], panel.bodies);
        panel.bodies.sort(function (a, b) {
          var keyA = a.listOrder, keyB = b.listOrder;
          if (keyA < keyB) return -1;
          if (keyA > keyB) return 1;
          return 0;
        });
        return panel;
      });
      $scope.panelsBeforeEdit = angular.copy($scope.panels);
    }

  }
]);
