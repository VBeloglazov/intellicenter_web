﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pentair.Desktop.Simulator.BusinessLogic
{
    public static class Statics
    {
        public static List<string>SimpleCircuits = new List<string>()
        {
            "GENERIC",
            "LIGHT",
            "MAGIC1",
            "MAGIC2",
            "GLOW",
            "GLOWT",
            "INTELLI",
            "SAML",
            "COLORW",
            "PHOTON",
            "DIMMER",
            "FLOOR",
            "MASTER",
            "CIRCGRP",
            "LITSHO"
        };
    }
}
