﻿using System;
using System.Threading.Tasks;

using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Pentair.Web.Api
{
    public class AuthRepository : IDisposable
    {
        private AuthContext _ctx;

        private readonly UserManager<ApplicationUser> _userManager;

        public AuthRepository()
        {
            _ctx = new AuthContext();
            _userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(_ctx));
        }

        public async Task<IdentityResult> RegisterUser(UserModel userModel)
        {
            ApplicationUser user = new ApplicationUser
            {
                UserName = userModel.UserName.ToLowerInvariant()
            };

            var result = await _userManager.CreateAsync(user, userModel.Password);

            return result;
        }

        /// <exception cref="Exception" />
        public async Task<ApplicationUser> FindUser(
            string userName,
            string password)
        {
            ApplicationUser user = await _userManager.FindAsync(userName, password);

            return user;
        
        } // FindUser()

        public void Dispose()
        {
            _ctx.Dispose();
            _userManager.Dispose();

        }
    }
}
