'use strict';
var PWC = PWC || {};
PWC.controller('ConfigurePumpController', [
  '$rootScope', '$scope', '$location', '$interpolate', 'PropertyService', 'PropertyModel', 'Enumerations', '$q',
  '$filter', 'MessageProcessor', 'Notification',
  function ($rootScope, $scope, $location, $interpolate, propertyService, propertyModel, Enumerations, $q,
            $filter, messageProcessor, Notification) {
    $scope.DataLoading = false;
    $scope.property = propertyModel;
    $scope.propertyService = propertyService;
    $scope.propertyBodies = propertyModel.Bodies;
    $scope.accordionStateByObjectNames = {};
    $scope.NewPumpOBJNAM = "";
    $scope.pumps = propertyModel.Pumps;
    $scope.pumpsEdit = propertyModel.Pumps;
    $scope.circuits = propertyModel.Circuits;
    $scope.pumpAffects = [];
    $scope.pumpsview = [false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false];
    $scope.maxPumps = 16;
    $scope.maxPumpsCircuits = 16;
    $scope.pumpSubtypes = Enumerations.getPumpSubtypeObjects();
    $scope.addresses = Array.apply(null, {length: 16}).map(function(element, index) { return index + 1; });
    $scope.sortableOptionsForPumps = {};
    $scope.flowPriorities = [{readableValue:'HIGHEST FLOW', systemValue:'SPEED'},{readableValue:'CUSTOM', systemValue:'CUSTOM'}];
    $scope.speedPriorities = [{readableValue:'HIGHEST SPEED', systemValue:'SPEED'},{readableValue:'CUSTOM', systemValue:'CUSTOM'}];

      // $rootScope.editMode.pumps = {};
    initBodies();    

    linkPumpAffects();

    // retrieve all circuits including (aux circuits, groups ligth circuits,             
    $scope.AllCircuits = $filter('AllPumpCircuits')(propertyModel.Circuits, propertyModel.Objects);
    

    $scope.errorSave = function(errorMessage) {
      Notification.error({message: 'Failed!' + ' ' + errorMessage, delay: 1000});
    };

    $scope.successSave = function() {
      Notification.success({message: 'Saved!', delay: 1000});
    };

    $scope.successCancel = function() {
      Notification.success({message: 'Cancelled!', delay: 1000});
    };

    $scope.$on('PUMPS_UPDATED', function (objname, item, obj) {
            console.log("---- processing Broadcast : pumps-controllers");
            //$scope.pumps =      propertyModel.Pumps;
            //$scope.pumpsEdit =  propertyModel.Pumps;
    });

    $scope.$on('UPDATE_CIR_GRPS', function (objname, item, obj) {
        console.log("---- processing Broadcast [PUMP CONTROLLER]: Updating circuits groups ------------------------------");
        $scope.AllCircuits = $filter('AllPumpCircuits')(propertyModel.Circuits, propertyModel.Objects);
    });

    $scope.getBodyPump = function (objectName) {
        var filterResult = $scope.propertyBodies.filter(function (propertyBody) {
            if (propertyBody.OBJNAM === objectName) {
                return propertyBody;
            }
        });

        if (filterResult.length > 0) {
            return filterResult[0].SNAME;
        }

        return objectName;
    }

    // TO FIX LEGACY CIRCUIT PUMP NAME BUG: 204 
    $scope.getCircuitPumpName = function (circuit_name) {
        if ((circuit_name === "X0048") || (circuit_name === "X0-148"))
            return "Solar";
        else
            return circuit_name;
    }

    $scope.getStepSize = function (pump) {
        if (pump.SETTMP) {
            return pump.SETTMP
        }
        else {
            return 1
        }
    }

    $scope.setIncreaseValue = function (value){
        $scope.increment = value

    }

    $scope.getIncreaseValue = function () {
        return $scope.increment
    }

    $scope.getDefaultPumpName = function (source, type,pumpIndex) {
        var names = []

        // set default names
        for (var i = 0; i < 16; i++) {
            var number = i + 1
            var name = ""
            if (i > 0)
                name = name + " #" + number.toString()
            var item = { "id": i, 
                         "occupied": false,
                         'name': source + name }
            names.push(item)
        }
        // check if is in use this name
        
        angular.forEach($scope.pumps, function (pump) {
            if (pumpIndex != $scope.pumps.indexOf(pump)) { // discard current position
                if (pump.SUBTYP === type) {
                    for (var i = 0; i < 16; i++) {                        
                            if (names[i].name === pump.SNAME) {
                                names[i].occupied = true
                                break
                            }
                    }
                }
            }
        });
        // search for the first avialable
        var available = 0;
        for (var i = 0; i < 16; i++) {
            if (names[i].occupied === false) {
                available = i
                break
            }
        }
        return names[available].name
    }

    $scope.getAvailablePort = function(pumpIndex){
        var ports = []
        
        for(var i=0;i<16;i++){
            var item = {"id":i+1,"occupied": false};
            ports.push(item)
        }            
                
        angular.forEach($scope.pumps,function(pump){
            var id = parseInt(pump.COMUART);
            if (pumpIndex != $scope.pumps.indexOf(pump)) { // discard current position
                angular.forEach(ports, function (port) {
                    switch (pump.SUBTYP) {
                        case 'VSF':
                        case 'FLOW':
                        case 'SPEED':
                            if (id === port.id) {
                                port.occupied = true
                            }
                            break;
                    }
                });
            }
        });

        var available = 1;
        for(var i=0;i<16;i++){
            if (ports[i].occupied === false) {
                available = ports[i].id
                break;
            }
        }
        return available
    }     

    $scope.changeSubtype = function(pump, subtype,index) {
      pump.SUBTYP = subtype;
      if(pump.SUBTYP === 'SPEED') {
          pump.PRIMFLO = '2500';
          pump.MIN = '450'; 
          pump.MAX = '3450';
          pump.SETTMP = '100';          
          pump.SNAME = $scope.getDefaultPumpName('VS', 'SPEED',index)
          pump.COMUART = $scope.getAvailablePort(index)
          
      }
      if(pump.SUBTYP === 'FLOW') {
        pump.PRIMFLO = '24';
        pump.MIN = '15';
        pump.MAX = '130';
        pump.SETTMP = '1';
        pump.PRIMTIM = '1';        
        pump.SNAME = $scope.getDefaultPumpName('VF', 'FLOW',index)
        pump.COMUART = $scope.getAvailablePort(index)        
      }

      if (pump.SUBTYP === 'VSF') {
          // SPEED SETTINGS
          pump.PRIMFLO = '1000';
          pump.MIN = '450'; 
          pump.MAX = '3450';
          // FLOW SETTINGS 
          pump.PRIMFLOF = '30';
          pump.MINF = '20';
          pump.MAXF = '140';
          pump.SETTMP = '100'
          pump.SETTMPNC = '1';
          pump.SNAME = $scope.getDefaultPumpName('VSF', 'VSF',index)
          pump.COMUART = $scope.getAvailablePort(index)
      }

      if (pump.SUBTYP == "SINGLE") {
          pump.BODY = propertyModel.getSingleOrSharedPoolID();
          pump.SNAME = $scope.getDefaultPumpName('Single Spd ', 'SINGLE',index)
      }

      if (pump.SUBTYP == "DUAL") {
          pump.BODY = propertyModel.getSingleOrSharedPoolID();
          pump.SNAME = $scope.getDefaultPumpName('Dual Spd ', 'DUAL',index)
      }
    };


    $scope.isAddressSelected= function(address){
        if (address > 5)
            return true
        else
            return false
    };
    
    // remove ports already used by other pumps
    $scope.getAvailableAddresses = function(addresses,current){
        var available = []              
        angular.forEach(addresses, function (port) {            
            var portOccupied = false
            angular.forEach($scope.pumps, function (pump) {
                var id = parseInt(pump.COMUART);
                if (port === id)
                    portOccupied = true
            });

            if ((portOccupied === false) || (current === port)) {
                available.push(port)
            }
        });
        return available        
    };
    
    $scope.getReadableFromSystem = function(systemValue, list) {
      var match = list.filter(function(item) {
        return item.systemValue === systemValue;
      });
      if(match.length > 0) {
        return match[0].readableValue;
      }
      return systemValue;
    };

    $scope.removeItems = function() {
      // populate the list of object names we are going to delete
      $scope.objectNamesToDelete = $scope.pumps.filter(function(obj) {
        if (obj.isSelected) {
          return obj;
        }
      }).map(function(obj) {
        return obj.OBJNAM;
      });
      propertyService.DeleteObject($scope.objectNamesToDelete).then(function() {
          angular.forEach($scope.objectNamesToDelete, function(nameToDelete) {
            propertyModel.Pumps = propertyModel.Pumps.filter(function(pump) {
              if(pump.OBJNAM !== nameToDelete) {
                return pump;
              }
            });
          });
          $scope.pumps = propertyModel.Pumps;  
          $scope.retrievePumps();
      });
    };

    $scope.retrievePumps = function () {
        // console retrieve pumpps get hardware definition
        propertyService.GetHardwareDefinitionPrecise().then(function () {
            linkPumpAffects();
            $scope.pumpsEdit = propertyModel.Pumps;            
            $scope.successSave();
        });
    }

    $scope.isValidDualPump = function (pump) {      
      // needs a sname and subtype
      if(!pump.SNAME) {
        return false;
      }
      if(!pump.SUBTYP) {
        return false;
      }
        // for all pump affects, there needs to be a circuit assigned.
      if (pump.pumpAffects) {
          for (var i = 0; i < pump.pumpAffects.length; i++) {
              if (!pump.pumpAffects[i].CIRCUIT) {
                  return false;
              }
          }
      }
      // needs to have one low speed pumpAffect.
      //return $filter('filterPumpAffectLowSpeed')(pump.pumpAffects);            // fix bug webclint bug # 2      
      return true;
    };

    $scope.changeUnitsVSF = function (uoM, pumpAffect,min) {
        //console.log("------changeUnits---- :", uoM);
        if (uoM === 'RPM') {
            pumpAffect.SPEED = 1000;
            pumpAffect.SELECT = 'RPM';
            if (pumpAffect.SPEED < min)
                pumpAffect.SPEED = min
            //console.log("------RPM NEW VALUE :" + pumpAffect.SPEED);
        } else if (uoM === 'GPM') {
            pumpAffect.SPEED = 30;
            if (pumpAffect.SPEED < min)
                pumpAffect.SPEED = min

            pumpAffect.SELECT = 'GPM';
            //console.log("------GPM NEW VALUE :" + pumpAffect.SPEED);
        }
        $scope.propertyService.SetValue(pumpAffect.OBJNAM, "SPEED", pumpAffect.SPEED);
    };
    
    $scope.isValidSpeedPump = function(pump) {
      // needs a sname and subtype
      if(!pump.SNAME) {
        return false;
      }
      if(!pump.SUBTYP) {
        return false;
      }
        // for all pump affects, there needs to be a circuit assigned.
      if (pump.pumpAffects) {
          for (var i = 0; i < pump.pumpAffects.length; i++) {
              if (!pump.pumpAffects[i].CIRCUIT) {
                  return false;
              }
          }
      }
      return true;
    };
    
    $scope.getRangeMin = function (pump) {
        var value = pump.MIN
        return value
    };
    
    $scope.getRangePump = function (pump) {
        var range = {
            min: pump.MIN,
            max: pump.MAX
        }
        return range
    };

    $scope.getValueinRange = function(value,min,max){      
        if(min){
            if (value < min)
                value = min
        }
        if (max){
            if (value > max)
                value = max
        }
        return value
    };

    $scope.addHighSpeedCircuit = function(pump) {
      var pumpAffect = {
        SPEED: '0',
        SELECT: 'NONE',
        BOOST: 'ON'
      };
      pump.pumpAffects.push(pumpAffect);
    };
    
   $scope.addSpeedCircuit = function(pump) {
      var max = 1; // start here because of primer
      angular.forEach(pump.pumpAffects, function(pumpAffect) {
        var tempListOrd = parseInt(pumpAffect.LISTORD);
        max = (tempListOrd > max) ? tempListOrd : max;
      });
      var listOrder = (max + 1) + '';
      var pumpAffect = {
        SPEED: '1000',
        SELECT: 'RPM',
        COMUART: '1',
        objectName: Math.uuid(4), // so that we can use the stepper for speed w/o actually create it yet.
        LISTORD: listOrder
      };
        // Check if minimum needs to be adjusted
      if (pump.MIN) {
          if (pumpAffect.SPEED < pump.MIN) {
              pumpAffect.SPEED = pump.MIN
              pumpAffect.SPEED = pump.MIN
          }
      }
      pump.pumpAffects.push(pumpAffect);
    };

    $scope.addFlowCircuit = function(pump) {
      var max = 1; // start here because of primer
      angular.forEach(pump.pumpAffects, function(pumpAffect) {
        var tempListOrd = parseInt(pumpAffect.LISTORD);
        max = (tempListOrd > max) ? tempListOrd : max;
      });
      var listOrder = (max + 1) + '';
      var pumpAffect = {
        SPEED: '30',
        FLOW: '30',
        SELECT: 'GPM',
        COMUART: '1',
        MIN: pump.MIN,
        MAX: pump.MAX,
        MINF: pump.MINF,
        MAXF: pump.MAXF,
        objectName: Math.uuid(4), // so that we can use the stepper for speed w/o actually create it yet.
        LISTORD: listOrder
      };
      
      // Check if minimum needs to be adjusted
      switch (pump.SUBTYP) {
          case "VF":
              if (pump.MIN) {
                  if (pumpAffect.FLOW < pump.MIN) {
                      pumpAffect.FLOW = pump.MIN
                      pumpAffect.SPEED = pump.MIN
                  }
              }
              break
          case "VSF":
            if (pump.MINF) {
              if (pumpAffect.FLOW < pump.MINF) {
                  pumpAffect.FLOW = pump.MINF
                  pumpAffect.SPEED = pump.MINF
              }
            }
            break
      }

      pump.pumpAffects.push(pumpAffect);
    };

    $scope.deleteSpeedCircuit = function(pump, pumpAffect) {
      var index = pump.pumpAffects.indexOf(pumpAffect);
      // add it to the list so that when we actually save the pump, we make delete calls to object affect if it was already there.
      if(pumpAffect.OBJNAM) {
        if(!pump.pumpAffectsToBeDeleted) {
          pump.pumpAffectsToBeDeleted = [];
        }
        pump.pumpAffectsToBeDeleted.push(pumpAffect.OBJNAM);
      }
      pump.pumpAffects.splice(index, 1);
    };

    $scope.setTwoSpeedBody = function (pump, circuitObjectName) {
        pump.BODY = circuitObjectName;
    };

    $scope.setLowSpeedCircuit = function(pump, circuitObjectName) {
      if($filter('filterPumpAffectLowSpeed')(pump.pumpAffects)) {
        $filter('filterPumpAffectLowSpeed')(pump.pumpAffects)[0].CIRCUIT = circuitObjectName;
      } else {
        var pumpAffect = {
          CIRCUIT: circuitObjectName,
          SPEED: '0',
          SELECT: 'NONE',
          BOOST: 'OFF'
        };
        pump.pumpAffects.push(pumpAffect);
      }
    };

    $scope.setHighSpeedCircuit = function(pumpAffect, circuitObjectName) {
      pumpAffect.CIRCUIT = circuitObjectName;
    };

    $scope.setSingleCircuit = function(pump, circuitObjectName) {
      if (pump.pumpAffects.length > 0) {            
        pump.pumpAffects[0].CIRCUIT = circuitObjectName;
      } else {
        var pumpAffect = {
          CIRCUIT: circuitObjectName,
          SPEED: '0',
          SELECT: 'NONE',
          LISTORD: '1'
        };
        pump.pumpAffects.push(pumpAffect);
      }
    };

    $scope.save = function (pump, index) {          

      $scope.setPristine(index) // disable before anything..
      if (pump.createMode) {
        create(pump, index);
      } else {
        update(pump, index);
      }          
    };

    $scope.getPumpsFree = function () {
        var free = $scope.pumps.length;
        return free;
    };

    // check can add more FeatureCircuits
    $scope.canAddPumps = function () {
        var free = $scope.maxPumps - $scope.pumps.length;
        if (free > 0)
            return true;
        else
            return false;
    };

    $scope.$on('WriteParamList', function (objname, item, obj) {
        console.log("---- processing WriteParamList pump controller..." , item);
        // process group circuits
        angular.forEach($scope.circuitsGroups, function (circuitsGroup) {
            if (item.objnam === circuitsGroup.OBJNAM) { // check match
                
                circuitsGroup.sName = item.params.SNAME;
            }
        });
    });

    $scope.$on('SETTINGSUPDATED', function (objname, item, obj) {
        console.log("---- processing Broadcast ---------    Config-pump-controllers: SETTINGSUPDATED");        
        linkPumpAffects();
    });

    $scope.getRangeString = function (pump) {
        var range = {
            min: pump.MIN,
            max: pump.MAX
        }
        var s = range
        return s
    };

    $scope.areSelectedPumpsToRemove = function () {
        var enabled = false;
        $scope.pumps.map(function (obj) {
            if (obj.isSelected) {
                enabled = true;     // at least 1 is enable (selected).
            }
        });
        return enabled;
    }
      // CIRCUITS
    $scope.getPumpsCicuitsFree = function (pump) {
        var free = 8;
        if (pump.pumpAffects)
           free = pump.pumpAffects.length;
        return free;
    };

    $scope.canAddPumpsCircuits = function (pump) {       
        var free = $scope.maxPumpsCircuits;
        if (pump.pumpAffects)   // check if exist
            free = free - pump.pumpAffects.length;

        if (free > 0)
            return true;
        else
            return false;
    };

    $scope.add = function () {      
     $scope.pumpsEdit = angular.copy($scope.pumps); // saving current configuration
      var pump = {
        OBJNAM: Math.uuid(4),
        editMode: true,
        createMode: true,
        SUBTYP: 'SINGLE',   
        PRIMFLO: '450',
        PRIMTIM: '1',
        SYSTIM: '0',
        MIN: '450',
        MAX: '3450',
        MINF: '15',
        MAXF: '134',
        PRIOR: 'CUSTOM',
        SETTMP: '100',
        SETTMPNC: '1',
        COMUART: '1',
        SNAME :"",
        pumpAffects: []
      };

      pump.SNAME = $scope.getDefaultPumpName('Single Spd ', 'SINGLE')

      $scope.NewPumpOBJNAM = pump.OBJNAM;

      $scope.sortableOptionsForPumps[pump.OBJNAM] = {
        update: function(e, ui) {
          // callback before changed model, don't need to do anything here.
        },
        stop: function(e, ui) {
          var currentElement = $(e.toElement);
          var attempts = 0;
          // we try to grab parentsUntil class row to get the id
          while(!$(currentElement).hasClass("row")) {
            currentElement = $(currentElement).parent();
            if (attempts > 2) { //> 5) {       // retries only 2 times
              break;
            }
          }
          // if we found the row proceed
          if($(currentElement).hasClass("row")) {
            //get the pump's id
            var rowId = $(currentElement).attr('id');
            var pumpId = parseInt(rowId.split('-')[1]);
            $scope.pumps[pumpId].pumpAffects = $scope.pumps[pumpId].pumpAffects.map(function(pumpAffect, idx) {
              pumpAffect.LISTORD = 2 + idx;
              return pumpAffect;
            });
          }
        }
      };

      if (pump.SUBTYP == "SINGLE") {    // ADD DEFAULT 
          pump.BODY = propertyModel.getSingleOrSharedPoolID();
        //  $scope.setSingleCircuit(pump, pump.BODY);
      }
      
        // start with 0 index
      $scope.setDirty($scope.pumps.length )
      // $rootScope.editMode.pumps[pump.OBJNAM] = true;
      $scope.pumps.push(pump);
      $scope.accordionStateByObjectNames[pump.OBJNAM] = {};
      $scope.accordionStateByObjectNames[pump.OBJNAM].open = true;     
    };
    
    // pump: form is touch
    $scope.isDirty = function (index) {
        if ($scope.pumpsview[index] === true)
            return true
        else
            return false
 
    }
      // pump: if form is clear (no touch)
    $scope.isPristine = function (index) {
        if ($scope.pumpsview[index] === false)
            return true
        else
            return false
    }

      // pump: clear form to disable [save]
    $scope.setPristine = function (index) {
        $scope.pumpsview[index] = false        
    };

    $scope.NameIsDuplicated = function (index, pumpName) {
        var status = false;
        for (var i = 0; i < $scope.pumps.length; i++) {
            if (i != index)
                if (pumpName == $scope.pumps[i].SNAME)
                    return true;
        }        
        return status;
    }

    $scope.checkNameEmpty = function (index, pumpName) {
        $scope.pumpsview[index] = true;            // set dirty first        
        if (pumpName === "") {  // disable if name is empty
            $scope.pumpsview[index] = false;
        }

        // checking for duplicated names : do not allow
        if($scope.NameIsDuplicated(index, pumpName))
            $scope.pumpsview[index] = false;                            
    }
      // pump: to enable [save] button form 
    $scope.setDirty = function (index,pump) {
        $scope.pumpsview[index] = true        
    };

    $scope.removePump = function(items,OBJNAM) {
        var newItems = [];
        angular.forEach(items, function (item) {
            
            if (item.OBJNAM !== OBJNAM)
                newItems.push(item);
        });

        return newItems;
    }

    $scope.cancelEdit = function (pump, index) {        
        console.log("total pumps count", propertyModel.Pumps.length);
        // restore original pumps before edit
        $scope.pumps = angular.copy($scope.pumpsEdit);
       // propertyModel.Pumps = angular.copy($scope.pumpsEdit); 
        // get hardware definition
        $scope.retrievePumps();
        console.log("disable cancel button")
        $scope.setPristine(index);      
    };

    $scope.getDictionaryValue = function(actualValue) {
      if (!actualValue)
        return actualValue;
      var match = $scope.pumpSubtypes.filter(function(pumpSubtype) {
        if (pumpSubtype.actualValue === actualValue) {
          return pumpSubtype;
        }
      });

      if (match && match.length > 0 && match[0].dictionaryValue) {
        return match[0].dictionaryValue;
      }
      return actualValue;
    };

    $scope.changePumpPriority = function(pump, priority) {
      pump.PRIOR = priority;
      pump.pumpAffects = $filter('sortByPumpAffectPriority')(pump);
    };

    function initBodies() {
    }

    function linkPumpAffects() {
      // filter the parent of pumpAffects to be that of the selectedPump objnam
      angular.forEach($scope.pumps, function(pump) {
        // this one is for sorting
        $scope.sortableOptionsForPumps[pump.OBJNAM] = {
          update: function(e, ui) {
            // callback before changed model, don't need to do anything here.
          },
          stop: function(e, ui) {
            var currentElement = $(e.toElement);
            var attempts = 0;
            // we try to grab parentsUntil class row to get the id 
            while(!$(currentElement).hasClass("row")) {
              currentElement = $(currentElement).parent();
              if (attempts > 2) { //> 5) {       // retries only 2 times
                break;
              }
            }
            // if we found the row proceed
            if($(currentElement).hasClass("row")) {
              //get the pump's id
              var rowId = $(currentElement).attr('id');
              var pumpId = parseInt(rowId.split('-')[1]);
              $scope.pumps[pumpId].pumpAffects = $scope.pumps[pumpId].pumpAffects.map(function(pumpAffect, idx) {
                pumpAffect.LISTORD = 2 + idx;
                return pumpAffect;
              });
            }
          }
        };
        // end sorting code
        pump.pumpAffects = propertyModel.PumpAffects.filter(function(PumpAffect) {
          return PumpAffect.PARENT === pump.OBJNAM;
        });
        // we need to exclude the priming pump affect for VS
        if (pump.SUBTYP === 'SPEED') {
            /*
          pump.pumpAffects = pump.pumpAffects.filter(function(pumpAffect) {
            if(pumpAffect.LISTORD !== '1' && pumpAffect.BOOST !== 'ON') {
              return pumpAffect;
            }
          });
          */
        }
        pump.pumpAffects.sort(function(a, b) {
          var keyA = parseInt(a.LISTORD),
            keyB = parseInt(b.LISTORD);
          if (keyA < keyB) return -1;
          if (keyA > keyB) return 1;
          return 0;
        });
      });
      $scope.pumpsBeforeEdit = angular.copy($scope.pumps);
    }

      // RUN EDIT [START]
    function runAddEditDeletes(pump) {

        //all pumps affects added here
        var newObjectAffects = pump.pumpAffects.filter(function (pumpAffect) {
            return pumpAffect;  // return each object on the list
        }).map(function (pumpAffect) {
            return {
                CIRCUIT: pumpAffect.CIRCUIT,
                SPEED: pumpAffect.SPEED + '',
                BOOST: pumpAffect.BOOST,
                SELECT: pumpAffect.SELECT
            }
        });
        //NOTE: create if there are any
        if (newObjectAffects && newObjectAffects.length >= 0) {     // 
            var createPromise = propertyService.createObjectAffect(pump.OBJNAM, newObjectAffects).then(function (result) {

                if (result.objectList[0]) {
                    var resultPumpAffects = result.objectList[0].params.OBJLIST;
                    var requestParamList = resultPumpAffects.map(function (resultPumpAffect) {
                        return {
                            objnam: resultPumpAffect.objnam,
                            keys: ["CIRCUIT", "SPEED", "BOOST", "SELECT", "LISTORD", "PARENT"]
                        }
                    });
                }
                propertyService.RequestParamList(requestParamList).then(function (result2) {
                    angular.forEach(result2.objectList, function (item) {
                        messageProcessor.ProcessPumpAffect(item);
                    });
                });
            });
        }

        // if no objects affect then edit 
/*
        if (newObjectAffects && newObjectAffects.length === 0) {     // 
            var createPromise = propertyService.editObjectAffect(pump.OBJNAM, newObjectAffects).then(function (result) {

                if (result.objectList[0]) {
                    var resultPumpAffects = result.objectList[0].params.OBJLIST;
                    var requestParamList = resultPumpAffects.map(function (resultPumpAffect) {
                        return {
                            objnam: resultPumpAffect.objnam,
                            keys: ["CIRCUIT", "SPEED", "BOOST", "SELECT", "LISTORD", "PARENT"]
                        }
                    });
                }
                propertyService.RequestParamList(requestParamList).then(function (result2) {
                    angular.forEach(result2.objectList, function (item) {
                        messageProcessor.ProcessPumpAffect(item);
                    });
                });
            });
        }
*/
        return true;
    };

// RUN EDIT [STOP]
    function runAddEditDeletes_old(pump) {
      var promises = [];
      //NOTE: separate affects that are new or old.
      var newObjectAffects = pump.pumpAffects.filter(function (pumpAffect) {
        if (!pumpAffect.OBJNAM) {
          return pumpAffect;
        }
      }).map(function (pumpAffect) {
        return {
          CIRCUIT: pumpAffect.CIRCUIT,
          SPEED: pumpAffect.SPEED + '',
          BOOST: pumpAffect.BOOST,
          SELECT: pumpAffect.SELECT
        }
      });
      var editObjectAffects = pump.pumpAffects.filter(function (pumpAffect) {
        if (pumpAffect.OBJNAM) {
          return pumpAffect;
        }
      }).map(function (pumpAffect) {
        return {
          CIRCUIT: pumpAffect.CIRCUIT,
          SPEED: pumpAffect.SPEED + '',
          BOOST: pumpAffect.BOOST,
          SELECT: pumpAffect.SELECT,
          LISTORD: pumpAffect.LISTORD + ''
        }
      });
      //NOTE: delete the objects in pumpAffectsToBeDeleted if there are any
      if (pump.pumpAffectsToBeDeleted && pump.pumpAffectsToBeDeleted.length > 0) {
        var deletePromise = propertyService.DeleteObject(pump.pumpAffectsToBeDeleted).then(function () {
          angular.forEach(pump.pumpAffectsToBeDeleted, function (pumpAffectToDelete) {
            propertyModel.PumpAffects = propertyModel.PumpAffects.filter(function (pumpAffect) {
              if (pumpAffect.OBJNAM !== pumpAffectToDelete) {
                return pumpAffect;
              }
            });
          });
        });
        promises.push(deletePromise);
      }
      //NOTE: create if there are any
      if (newObjectAffects && newObjectAffects.length > 0) {
          var createPromise = propertyService.createObjectAffect(pump.OBJNAM, newObjectAffects).then(function (result) {

          if (result.objectList[0]) {
              var resultPumpAffects = result.objectList[0].params.OBJLIST;
              var requestParamList = resultPumpAffects.map(function (resultPumpAffect) {
                  return {
                      objnam: resultPumpAffect.objnam,
                      keys: ["CIRCUIT", "SPEED", "BOOST", "SELECT", "LISTORD", "PARENT"]
                  }
              });
          }
          propertyService.RequestParamList(requestParamList).then(function (result2) {
            angular.forEach(result2.objectList, function (item) {
              messageProcessor.ProcessPumpAffect(item);
            });
          });
        });
        promises.push(createPromise);
      }
      //NOTE: edit if there are any
      if ((pump.SUBTYP === 'SPEED' || pump.SUBTYP === 'FLOW') && editObjectAffects && editObjectAffects.length > 0) {
        // take out the priming pump affect
        editObjectAffects = editObjectAffects.filter(function(editObjectAffect) {
           return editObjectAffect.BOOST !== 'ON' && editObjectAffect.LISTORD !== '1';
        });
      }
      if (editObjectAffects && editObjectAffects.length > 0) {
        //NOTE: edit any existing object affects
          //var editPromise = propertyService.editObjectAffect(pump.OBJNAM, editObjectAffects).then(function () { });       // edit has been deprecated
        var editPromise = propertyService.createObjectAffect(pump.OBJNAM, editObjectAffects).then(function () { });          
        promises.push(editPromise);
      }
      return $q.all(promises);
    }
    //----------------------------- CREATE 
    function create_BACKUP(pump, index) {
      $scope.DataLoading = true;
      var params = {};
      params.SNAME = pump.SNAME;
      params.SUBTYP = pump.SUBTYP;
      params.CIRCUIT = pump.CIRCUIT;
      if(pump.SUBTYP === 'SPEED' || pump.SUBTYP === 'FLOW' || pump.SUBTYP === 'VSF') {
          if (parseInt(pump.MIN) > parseInt(pump.MAX)) {
          pump.MIN = pump.MAX;
        }
          if(parseInt(pump.MAX) < parseInt(pump.MIN) ) {
          pump.MAX = pump.MIN;
        }
        params.PRIMFLO = pump.PRIMFLO + '';
        params.PRIMTIM = pump.PRIMTIM + '';
        params.COMUART = pump.COMUART + '';

        if (pump.SUBTYP === 'VSF') {
            if (parseInt(pump.MINF) > parseInt(pump.MAXF)) {
                pump.MINF = pump.MAXF;
            }
            if (parseInt(pump.MAXF) < parseInt(pump.MINF)) {
                pump.MAXF = pump.MINF;
            }
            params.MINF = pump.MINF + '';
            params.MAXF = pump.MAXF + '';
            params.SETTMPNC = pump.SETTMPNC;
        }

        // params.SYSTIM = pump.SYSTIM + '';
        params.MIN = pump.MIN + '';
        params.MAX = pump.MAX + '';
        params.PRIOR = pump.PRIOR;
        params.SETTMP = pump.SETTMP;
        
      }

      if (pump.SUBTYP === 'SINGLE' || pump.SUBTYP === 'DUAL') {
          params.BODY = pump.BODY;
      }

      // never set CIRCUIT, it is a system defined parameter
      delete params.CIRCUIT;
      delete params.SYSTIM;
          
      propertyService.CreateNewObject('PUMP', params).then(function(result) {
        $scope.setPristine(index) //$rootScope.forms.pumps[index].$setPristine();
        pump.OBJNAM = result.objnam;
        // grab the comuart that got set
        propertyService.GetParamListByObject("COMUART",pump.OBJNAM).then(function(result) {
          pump.COMUART = result.objectList[0].params.COMUART;
        });
        delete pump.createMode;
        $scope.accordionStateByObjectNames[pump.OBJNAM] = {};
        $scope.accordionStateByObjectNames[pump.OBJNAM].open = true;
        
        if(pump.SUBTYP === 'SINGLE') {
            $scope.retrievePumps();
          return;
        }

        var pumpAffects;
        if(pump.SUBTYP === 'DUAL') {
          // loop through each one
          pumpAffects = pump.pumpAffects.map(function(pumpAffect) {
            return {
              CIRCUIT: pumpAffect.CIRCUIT,
              SPEED: '0',
              BOOST: pumpAffect.BOOST,
              SELECT: 'NONE'
            };
          });

          if (pumpAffects.length > 0) {
              propertyService.createObjectAffect(pump.OBJNAM, pumpAffects).then(function (result) {
                  var resultPumpAffects = result.objectList[0].params.OBJLIST;
                  var requestParamList = resultPumpAffects.map(function (resultPumpAffect) {
                      return {
                          objnam: resultPumpAffect.objnam,
                          keys: ["CIRCUIT", "SPEED", "BOOST", "SELECT", "LISTORD", "PARENT"]
                      }
                  });
                  propertyService.RequestParamList(requestParamList).then(function (result2) {
                      angular.forEach(result2.objectList, function (item) {
                          messageProcessor.ProcessPumpAffect(item);
                      });
                      $scope.retrievePumps();
                  });
              });
          }
        }

        if(pump.SUBTYP === 'SPEED' || pump.SUBTYP === 'FLOW' || pump.SUBTYP === 'VSF') {
            pumpAffects = pump.pumpAffects.map(function (pumpAffect) {
            /*
                if (pump.SUBTYP === 'VSF') {
                    var speed1 = pumpAffect.SPEED;
                    if (pumpAffect.SELECT === "GPM")
                        speed1 = pumpAffect.FLOW;

                    return{
                        CIRCUIT: pumpAffect.CIRCUIT,
                        SPEED: speed1 + '',
                        SELECT: pumpAffect.SELECT,
                        LISTORD: pumpAffect.LISTORD
                    };
                }
                else {
                    return {
                        CIRCUIT: pumpAffect.CIRCUIT,
                        SPEED: pumpAffect.SPEED + '',
                        SELECT: pumpAffect.SELECT,
                        LISTORD: pumpAffect.LISTORD
                    };
                }                
                }
                */
                return {
                    CIRCUIT: pumpAffect.CIRCUIT,
                    SPEED: pumpAffect.SPEED + '',
                    SELECT: pumpAffect.SELECT,
                    LISTORD: pumpAffect.LISTORD
                };

          });
          if(pumpAffects.length > 0) {
              propertyService.createObjectAffect(pump.OBJNAM, pumpAffects).then(function (result) {
                  $scope.DataLoading = false;
              var resultPumpAffects = result.objectList[0].params.OBJLIST;
              var requestParamList = resultPumpAffects.map(function(resultPumpAffect) {
                return {
                  objnam: resultPumpAffect.objnam,
                  keys: ["CIRCUIT","SPEED","BOOST","SELECT","LISTORD","PARENT"]
                }
              });
              propertyService.RequestParamList(requestParamList).then(function(result2) {
                angular.forEach(result2.objectList, function(item) {
                  messageProcessor.ProcessPumpAffect(item);
                });
                $scope.retrievePumps();
              });
            });
          } else {
              $scope.retrievePumps();
          }
        }
      });
    }

    //----------------------------- CREATE  MODIFIED
    function create(pump, index) {
        $scope.DataLoading = true;
        var params = {};
        params.SNAME = pump.SNAME;
        params.SUBTYP = pump.SUBTYP;
        params.CIRCUIT = pump.CIRCUIT;
        if (pump.SUBTYP === 'SPEED' || pump.SUBTYP === 'FLOW' || pump.SUBTYP === 'VSF') {
            if (parseInt(pump.MIN) > parseInt(pump.MAX)) {
                pump.MIN = pump.MAX;
            }
            if (parseInt(pump.MAX) < parseInt(pump.MIN)) {
                pump.MAX = pump.MIN;
            }
            params.PRIMFLO = pump.PRIMFLO + '';
            params.PRIMTIM = pump.PRIMTIM + '';
            params.COMUART = pump.COMUART + '';

            if (pump.SUBTYP === 'VSF') {
                if (parseInt(pump.MINF) > parseInt(pump.MAXF)) {
                    pump.MINF = pump.MAXF;
                }
                if (parseInt(pump.MAXF) < parseInt(pump.MINF)) {
                    pump.MAXF = pump.MINF;
                }
                params.MINF = pump.MINF + '';
                params.MAXF = pump.MAXF + '';
                params.SETTMPNC = pump.SETTMPNC;
            }

            // params.SYSTIM = pump.SYSTIM + '';
            params.MIN = pump.MIN + '';
            params.MAX = pump.MAX + '';
            params.PRIOR = pump.PRIOR;
            params.SETTMP = pump.SETTMP;

        }

        if (pump.SUBTYP === 'SINGLE' || pump.SUBTYP === 'DUAL') {
            params.BODY = pump.BODY;
        }

        // never set CIRCUIT, it is a system defined parameter
        delete params.CIRCUIT;
        delete params.SYSTIM;

        propertyService.CreateNewObject('PUMP', params).then(function (result) {
            $scope.setPristine(index) //$rootScope.forms.pumps[index].$setPristine();
            pump.OBJNAM = result.objnam;
            // grab the comuart that got set
            propertyService.GetParamListByObject("COMUART", pump.OBJNAM).then(function (result) {
                pump.COMUART = result.objectList[0].params.COMUART;
            });
            delete pump.createMode;
            $scope.accordionStateByObjectNames[pump.OBJNAM] = {};
            $scope.accordionStateByObjectNames[pump.OBJNAM].open = true;
            var pumpAffects;
            switch (pump.SUBTYP){
                case 'SINGLE':
                    /*
                    propertyService.GetHardwareDefinition().then(function () {
                        linkPumpAffects();
                        $scope.successSave(); });                
                        */
                    break;

                case 'DUAL':
                    pumpAffects = pump.pumpAffects.map(function (pumpAffect) {
                        return {
                            CIRCUIT: pumpAffect.CIRCUIT,
                            SPEED: '0',
                            BOOST: pumpAffect.BOOST,
                            SELECT: 'NONE'
                        };
                    });
                    break;

                case 'SPEED':
                case 'FLOW':
                case 'VSF':
                    pumpAffects = pump.pumpAffects.map(function (pumpAffect) {
                        /*
                            if (pump.SUBTYP === 'VSF') {
                                var speed1 = pumpAffect.SPEED;
                                if (pumpAffect.SELECT === "GPM")
                                    speed1 = pumpAffect.FLOW;
            
                                return{
                                    CIRCUIT: pumpAffect.CIRCUIT,
                                    SPEED: speed1 + '',
                                    SELECT: pumpAffect.SELECT,
                                    LISTORD: pumpAffect.LISTORD
                                };
                            }
                            else {
                                return {
                                    CIRCUIT: pumpAffect.CIRCUIT,
                                    SPEED: pumpAffect.SPEED + '',
                                    SELECT: pumpAffect.SELECT,
                                    LISTORD: pumpAffect.LISTORD
                                };
                            }                
                            }
                            */
                            return {
                                CIRCUIT: pumpAffect.CIRCUIT,
                                SPEED: pumpAffect.SPEED + '',
                                SELECT: pumpAffect.SELECT,
                                LISTORD: pumpAffect.LISTORD
                            };
                        });
                    break;

            }

            
            if ((pumpAffects != undefined ) && (pumpAffects.length > 0)) {
                propertyService.createObjectAffect(pump.OBJNAM, pumpAffects).then(function (result) {
                 if (result.objectList.length > 0){
                        var resultPumpAffects = result.objectList[0].params.OBJLIST;
                    var requestParamList = resultPumpAffects.map(function (resultPumpAffect) {
                        return {
                            objnam: resultPumpAffect.objnam,
                            keys: ["CIRCUIT", "SPEED", "BOOST", "SELECT", "LISTORD", "PARENT"]
                        }
                    });
                    propertyService.RequestParamList(requestParamList).then(function (result2) {
                        angular.forEach(result2.objectList, function (item) {
                            messageProcessor.ProcessPumpAffect(item);
                        });
                    });
                 }
                });
            }
            
            // get hardware definition
            $scope.retrievePumps();
        });
    }

    function update(pump, index) {
      var params = {};
      var objnam = pump.OBJNAM;
      params.SNAME = pump.SNAME;
      params.SUBTYP = pump.SUBTYP;
      params.CIRCUIT = pump.CIRCUIT;
      if(pump.SUBTYP === 'SPEED' || pump.SUBTYP === 'FLOW' || pump.SUBTYP === 'VSF') {
          if ( parseInt(pump.MIN) > parseInt(pump.MAX) ) {
          pump.MIN = pump.MAX;
        }
          if( parseInt(pump.MAX) < parseInt(pump.MIN) ) {
          pump.MAX = pump.MIN;
        }
        params.PRIMFLO = pump.PRIMFLO + '';
        params.PRIMTIM = pump.PRIMTIM + '';
        params.COMUART = pump.COMUART + '';
        params.SYSTIM = pump.SYSTIM + '';

        params.MIN = pump.MIN + '';
        params.MAX = pump.MAX + '';
        if (pump.SUBTYP === 'VSF') {
            if ( parseInt(pump.MINF) > parseInt(pump.MAXF)) {
                pump.MINF = pump.MAXF;
            }
            if ( parseInt(pump.MAXF) < parseInt(pump.MINF) ) {
                pump.MAXF = pump.MINF;
            }
            params.MINF = pump.MINF + '';
            params.MAXF = pump.MAXF + '';
        }
        params.PRIOR = pump.PRIOR;
        
        params.SETTMP = params.SETTMP;
        params.SETTMPNC = pump.SETTMPNC;

      }

      if (pump.SUBTYP === 'SINGLE' || pump.SUBTYP === 'DUAL') {   // SET SINGLE PUMP
          params.BODY = pump.BODY;
      }

      // never set CIRCUIT, it is a system defined parameter
      delete params.CIRCUIT;
      propertyService.SetParams(objnam, params).then(function(result) {
          if (pump.SUBTYP === 'SINGLE' ) {
              if ( (pump.pumpAffects!=null)  &&(pump.pumpAffects[0].OBJNAM)) {
                propertyService.SetValue(pump.pumpAffects[0].OBJNAM, 'CIRCUIT', pump.pumpAffects[0].CIRCUIT).then(function() {
                $Scope.setPristine( index )  //$rootScope.forms.pumps[index].$setPristine();                    
                $scope.retrievePumps();                                                                                                            });
          }
          else
          {
            var pumpAffect = {
              CIRCUIT:pump.pumpAffects[0].CIRCUIT,
              SPEED:pump.pumpAffects[0].SPEED,
              SELECT:pump.pumpAffects[0].SELECT,
              LISTORD:pump.pumpAffects[0].LISTORD
            };
            propertyService.createObjectAffect(pump.OBJNAM, [pumpAffect]).then(function() {
                $scope.setPristine( index ) //$rootScope.forms.pumps[index].$setPristine();
              var resultPumpAffects = result.objectList[0].params.OBJLIST;
              var requestParamList = resultPumpAffects.map(function(resultPumpAffect) {
                return {
                  objnam: resultPumpAffect.objnam,
                  keys: ["CIRCUIT","SPEED","BOOST","SELECT","LISTORD","PARENT"]
                }
              });
              propertyService.RequestParamList(requestParamList).then(function(result2) {
                angular.forEach(result2.objectList, function(item) {
                  messageProcessor.ProcessPumpAffect(item);
                });
                $scope.retrievePumps();
              });
            });
          }
        }
      });


      if (pump.SUBTYP === 'DUAL' || pump.SUBTYP === 'SPEED' || pump.SUBTYP === 'FLOW' || pump.SUBTYP === 'VSF') {
          if ( runAddEditDeletes(pump) === true) {
              propertyService.GetHardwareDefinitionPrecise().then(function () {
                  linkPumpAffects();
                  $scope.successSave();
                  $scope.pumpsEdit = propertyModel.Pumps;
                  $scope.setPristine(index)
              });
          }          
      }

      if (pump.SUBTYP === 'SINGLE') {          
          propertyService.GetHardwareDefinitionPrecise().then(function () {
              linkPumpAffects();
              $scope.successSave();
              $scope.pumpsEdit = propertyModel.Pumps;
              $scope.setPristine(index)
          });
      }

    }   // a
      // b
  }
]);
