﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pentair.Domain.Simulator.Entity
{
    public class Panel
    {
        [Key]
        public int ID { get; set; }
        public string Name { get; set; }
        public UInt32 LastRevision { get; set; }
        public int ListPosition { get; set; }
        public string UserName { get; set; }
        public float Version { get; set; }
        public string SerialNumber { get; set; }
        public DateTime ManufacturDate { get; set; }
        public UInt32 TotalOnTime { get; set; }
        public int BacklightNormal { get; set; }
        public int BacklightDim { get; set; }
        public int DimTimeout { get; set; }
        public int OffTimeout { get; set; }
        public string TouchCalibration { get; set; }
        public float BatteryLevel { get; set; }
        public float RFLevel { get; set; }
        public DateTime FirstDetect { get; set; }
        public DateTime LastDetect { get; set; }

        // Foreign Keys
        public int TypeID { get; set; }
        public int PanelSubTypeID { get; set; }
        public int CommObjectID { get; set; }
        public int DayNightModeID { get; set; }
    }
}