-- =============================================================
--
-- Author:		Vladimir Beloglazov
-- Create date: 02/08/2018
--
-- Creates:
--
--		Table Type:			None
--
--		Stored Procedure:	PRemoveConnectionClient
--
-- =============================================================

USE PathfinderIntelliCenter
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (
	SELECT * FROM sys.objects
	WHERE object_id = OBJECT_ID(N'dbo.PRemoveConnectionClient')
	AND type in (N'P', N'PC')
)
DROP PROCEDURE dbo.PRemoveConnectionClient
GO

-- -------------------------------------------------------------
--
-- Returns just deleted [Connections] record ID.
--
-- -------------------------------------------------------------
CREATE PROCEDURE dbo.PRemoveConnectionClient
	 @connectionId		AS uniqueidentifier

AS
BEGIN

	SET NOCOUNT OFF

    DELETE FROM Connections
	OUTPUT DELETED.ConnectionId
    WHERE ConnectionId = @connectionId
	
END
GO
