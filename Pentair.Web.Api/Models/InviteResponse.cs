﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace Pentair.Web.Api
{
    public class InviteResponse
    {
        public string Email { get; set; }
        public string UserName { get; set; }
        public int InstallationId { get; set; }
    }
}