﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Threading.Tasks;

using SERVICE_SHARED;

namespace Pentair.Web.Api
{
    [Authorize]
    [RoutePrefix("api/NotificationUsers")]
    public class NotificationUsersController : ApiController
    {
        private const string EVENT_SOURCE = "NotificationUsersController";

        [Authorize]
        public List<NotificationRecipient> Get(
            int id)
        {
            List<NotificationRecipient> result = null;

            try
            {
                using (DbContextPentair db = DbContextPentair.New())
                {
                    result = db.GetInstallationNotificationRecipients(id).ToList();

                } // using (DbContextPentair db)
            }
            catch(Exception e)
            {
                LogManager.LogErrorAsync(
                    source: EVENT_SOURCE,
                    eventBriefDescription: "GET api/NotificationUsers/" + id + " failed",
                    installationId: 0,
                    userId: User.Identity.GetOwinUserId(),
                    connectionId: Guid.Empty,
                    eventFullDescription:
                        ExceptionManager.CustomizeException(e).ErrorMessage(
                            errorHeader: null,
                            addStackTrace: true
                        )
                );

                result = null;
            }

            return result;
        
        } // Get()

        [Authorize]
        public IHttpActionResult Delete(
            NotificationRecipientList notificationRecipientList)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                using (DbContextPentair db = DbContextPentair.New())
                {
                    return db.DeleteInstallationNotificationRecipients(notificationRecipientList)
                        ? Ok() as IHttpActionResult
                        : BadRequest() as IHttpActionResult
                    ;

                } // using (DbContextPentair db)
            }
            catch(Exception e)
            {
                LogManager.LogErrorAsync(
                    source: EVENT_SOURCE,
                    eventBriefDescription: "DELETE /service/Api/NotificationUsers/ failed",
                    installationId: 0,
                    userId: User.Identity.GetOwinUserId(),
                    connectionId: Guid.Empty,
                    eventFullDescription:
                        ExceptionManager.CustomizeException(e).ErrorMessage(
                            errorHeader: null,
                            addStackTrace: true
                        )
                );

                return InternalServerError();
            }
        
        } // Delete()

        [Authorize]
        public IHttpActionResult Post(
            NotificationRecipientModel postParameters)
        {
            if (String.IsNullOrEmpty(postParameters.EmailAddress2))
            {
                ModelState["postParameters.EmailAddress2"].Errors.Clear();
            }
            if (String.IsNullOrEmpty(postParameters.Phone1))
            {
                ModelState["postParameters.Phone1"].Errors.Clear();
            }
            if (String.IsNullOrEmpty(postParameters.Phone2))
            {
                ModelState["postParameters.Phone2"].Errors.Clear();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                int hardwareId;
                if (int.TryParse(postParameters.PropertyId, out hardwareId))
                {
                    using (DbContextPentair db = DbContextPentair.New())
                    {
                        return db.AddInstallationNotificationRecipient(postParameters, hardwareId)
                            ? Ok() as IHttpActionResult
                            : BadRequest() as IHttpActionResult
                        ;

                    } // using (DbContextPentair db)
                }
                else
                {
                    return BadRequest();
                }
            }
            catch(Exception e)
            {
                LogManager.LogErrorAsync(
                    source: EVENT_SOURCE,
                    eventBriefDescription: "POST api/NotificationUsers/ failed for " + postParameters.EmailAddress1,
                    installationId: 0,
                    userId: User.Identity.GetOwinUserId(),
                    connectionId: Guid.Empty,
                    eventFullDescription:
                        ExceptionManager.CustomizeException(e).ErrorMessage(
                            errorHeader: null,
                            addStackTrace: true
                        )
                );

                return InternalServerError();
            }
        
        } // Post()

        // TODO: Not in use? 2b removed? 2018-04-05 VB
        /*
        [Authorize]
        public IHttpActionResult Put(
            NotificationRecipientModel postParameters)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                int hardwareId, recipientId;
                if (int.TryParse(postParameters.PropertyId, out hardwareId) &&
                    int.TryParse(postParameters.UserId, out recipientId))
                {
                    using (DbContextPentair db = DbContextPentair.New())
                    {
                        db.UpdateInstallationNotificationRecipient(postParameters, hardwareId, recipientId);

                    } // using (DbContextPentair db)

                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            catch(Exception e)
            {
                LogManager.LogErrorAsync(
                    source: EVENT_SOURCE,
                    eventBriefDescription: "PUT api/NotificationUsers/ failed for " + postParameters.EmailAddress1,
                    installationId: 0,
                    userId: User.Identity.GetOwinUserId(),
                    connectionId: Guid.Empty,
                    eventFullDescription:
                        ExceptionManager.CustomizeException(e).ErrorMessage(
                            errorHeader: null,
                            addStackTrace: true
                        )
                );

                return InternalServerError();
            }

        } // Put()
        */

    } // class NotificationUsersController
}
