﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pentair.Domain.Simulator.Entity
{
    public class RemoteGroup
    {
        [Key]
        public int ID { get; set; }
        public string Name { get; set; }
        public UInt32 LastRevision { get; set; }
        public int ButtonPosition { get; set; }

        // Foreign Keys
        public int TypeID { get; set; }
        public int ParentRemoteID { get; set; }
        public int CurrentCircuitID { get; set; }
    }
}