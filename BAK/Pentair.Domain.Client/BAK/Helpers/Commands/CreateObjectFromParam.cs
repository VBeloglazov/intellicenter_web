﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pentair.Domain.Client.Helpers.Commands
{
    public class CreateObjectFromParam : NewParamCommand
    {
        public CreateObjectFromParam(
            string messageID,
            List<NewParamObject> objectList
            )
            : base(CommandString.CreateObject, messageID, objectList)
        {
        }

    } // class CreateObjectFromParam
}
