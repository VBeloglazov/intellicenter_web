-- =============================================================
--
-- Author:		Vladimir Beloglazov
-- Create date: 04/05/2018
--
-- Creates:
--
--		Table Type:			None
--		Stored Procedure:	None
--		Function:			Split
--
-- =============================================================

USE PathfinderIntelliCenter
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (
	SELECT * FROM sys.objects
	WHERE object_id = OBJECT_ID(N'dbo.FSplit')
	AND type in (N'IF')
)
DROP FUNCTION dbo.FSplit
GO

CREATE FUNCTION dbo.FSplit
(
    @String nvarchar(255),
    @Delimiter nchar(1)
)
RETURNS TABLE
AS
RETURN
(
    WITH Split(stpos,endpos)
    AS(
        SELECT 0 AS stpos, CHARINDEX(@Delimiter,@String) AS endpos
        UNION ALL
        SELECT endpos+1, CHARINDEX(@Delimiter,@String,endpos+1)
            FROM Split
            WHERE endpos > 0
    )
    SELECT 'Item' = SUBSTRING(@String,stpos,COALESCE(NULLIF(endpos,0),LEN(@String)+1)-stpos)
    FROM Split
)
GO