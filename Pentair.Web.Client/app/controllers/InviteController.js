﻿'use strict';
var PWC = PWC || {};
PWC.controller('InviteController', [
    '$scope', '$location', 'PropertyService', 'PropertyModel',
    function ($scope, $location, propertyService, propertyModel) {
        $scope.Property = propertyService;
        $scope.Model = propertyModel;
        $scope.$parent.Layout = "layout-base";

        $scope.InviteSubmitted = false;
        $scope.InviteGroup = propertyModel.Security[0].value;
        $scope.InviteEmail = "";


        $scope.InviteUser = function () {
            propertyService.InviteUser($scope.InviteEmail, $scope.InviteGroup);
            $scope.InviteSubmitted = true;
        }


        $scope.GoToManageUsersPage = function () {
            $location.path("/manageUsers");
        }

        $scope.ResetInvite = function () {
            $scope.InviteGroup = propertyModel.Security[0].value;
            $scope.InviteEmail = "";
            propertyModel.InviteStatus = {};
            $scope.InviteSubmitted = false;
            if ($scope.AddUserForm)
                $scope.AddUserForm.$setPristine();
        }
        
        $scope.ResetInvite();
    }
]);