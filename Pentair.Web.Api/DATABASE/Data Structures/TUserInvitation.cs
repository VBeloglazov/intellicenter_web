﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pentair.Web.Api
{
    /// <summary>
    /// Must match PGetUserInvitation result set.
    /// </summary>
    public class TUserInvitation
    {
        public Guid InvitationId { get; set; }
        public string EmailAddress { get; set; }
        public int? InstallationId { get; set; }
        public string InstallationName { get; set; }
        public string AccessToken { get; set; }
        public string Status { get; set; }
        public string EmailId { get; set; }
        public DateTime CreateDateTime { get; set; }
        public DateTime UpdateDateTime { get; set; }

    } // class TUserInvitation
}