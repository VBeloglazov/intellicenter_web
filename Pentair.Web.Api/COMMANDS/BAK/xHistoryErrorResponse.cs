﻿using System.Collections.Generic;

using Newtonsoft.Json;

namespace Pentair.Domain.Client.Helpers.Commands
{
    /* TODO: 2b deleted
    public class HistoryErrorResponse : CommandBase
    {
        [JsonProperty("response")]
        public string Response { get; private set; }

        [JsonProperty("messageNo")]
        public string MessageNo { get; private set; }

        [JsonProperty("timestamp")]
        public string TimeStamp { get; private set; }

        [JsonProperty("description")]
        public string Description { get; private set; }

        public HistoryErrorResponse(
            string messageID,
            string response,
            int messageNo,
            string timestamp,
            string description
            )
            : base(CommandString.ErrorHistoryMessage, messageID)
        {
            this.Response = response;
            this.MessageNo = messageNo.ToString();
            this.TimeStamp = timestamp;
            this.Description = description;

        } // HistoryErrorResponse()

    } // class HistoryErrorResponse
    */
}
