﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Net;
using System.Threading.Tasks;

using Microsoft.AspNet.Identity.Owin;

namespace Pentair.Web.Api
{
    public class WeatherApi
    {
        #region Public Properties

        public string Url { get; private set; }
        public string UserId { get; private set; }
        public string Secret { get; private set; }

        #endregion Public Properties

        public WeatherApi()
        {
            System.Configuration.Configuration configSettings =
                System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("/service");

            this.Url = configSettings.AppSettings.Settings["weatherUrl"].Value;
            this.UserId = configSettings.AppSettings.Settings["weatherId"].Value;
            this.Secret = configSettings.AppSettings.Settings["weatherSecret"].Value;

        } // WeatherApi()

    } // class WeatherApi

    public class WeatherCurrentConditionsController : ApiController
    {
        [Authorize]
        public async Task<IHttpActionResult> Get(
            string zip)
        {
            if (String.IsNullOrEmpty(zip))
            {
                return StatusCode(HttpStatusCode.BadRequest);
            }

            WeatherApi weather = new WeatherApi();

            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(weather.Url);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // https://api.aerisapi.com/observations/93012?client_id=30V6nDI3y3X3S3u0lUTX4&client_secret=OY79PUrcxZmUlyxe6XINjHNLlNXB4DoGiWJGhXrj
                HttpResponseMessage response = await client.GetAsync(
                    String.Format("/observations/{0}?client_id={1}&client_secret={2}",
                        zip,
                        weather.UserId,
                        weather.Secret
                    )
                );

                if (!response.IsSuccessStatusCode)
                {
                    return StatusCode(response.StatusCode);
                }

                dynamic forecast = await response.Content.ReadAsAsync<dynamic>().ConfigureAwait(false);

                return Ok(forecast);

            } // using (HttpClient client)

        } // Get()

    } // class WeatherCurrentConditionsController

    public class WeatherForecastController : ApiController
    {
        [Authorize]
        public async Task<IHttpActionResult> Get(
            string zip)
        {
            if (String.IsNullOrEmpty(zip))
            {
                return StatusCode(HttpStatusCode.BadRequest);
            }

            WeatherApi weather = new WeatherApi();

            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(weather.Url);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // https://api.aerisapi.com/forecasts/93012?client_id=30V6nDI3y3X3S3u0lUTX4&client_secret=OY79PUrcxZmUlyxe6XINjHNLlNXB4DoGiWJGhXrj&limit=6
                HttpResponseMessage response = await client.GetAsync(
                    String.Format("/forecasts/{0}?client_id={1}&client_secret={2}&limit=6",
                        zip,
                        weather.UserId,
                        weather.Secret
                    )
                );

                if (!response.IsSuccessStatusCode)
                {
                    return StatusCode(response.StatusCode);
                }

                dynamic forecast = await response.Content.ReadAsAsync<dynamic>().ConfigureAwait(false);

                return Ok(forecast);

            } // using (HttpClient client)

        } // Get()
    
    } // class WeatherForecastController
}