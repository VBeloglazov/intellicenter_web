﻿using System;
using System.Collections.Generic;
using System.Windows.Media;

namespace Pentair.Desktop.Simulator.BusinessLogic
{
    #region enumerations
    /// <summary>
    /// Internal enumerations
    /// </summary>

    public enum ConnectionStatus
    {
        Disabled,
        Enabled,
        Authenticating,
        Connected,
        Error
    }
    #endregion
    public static class Helpers
    {
        #region Private Properties
        private static readonly Color connected = Colors.Green;
        private static readonly Color connecting = Colors.Goldenrod;
        private static readonly Color authenticating = Colors.OrangeRed;
        private static readonly Color disabled = Colors.DarkGray;
        private static readonly Color error = Colors.Red;
        #endregion

        #region Public Properties
        public static int MessageId = 0;
        public static int LocalHostPort = 6680;
        #endregion

        #region Public Methods
        public static SolidColorBrush StatusBrush(ConnectionStatus status)
        {
            switch (status)
            {
                case ConnectionStatus.Connected:
                    return new SolidColorBrush(connected);
                    //break;
                case ConnectionStatus.Enabled:
                    return new SolidColorBrush(connecting);
                    //break;
                case ConnectionStatus.Authenticating:
                    return new SolidColorBrush(authenticating);
                    //break;
                case ConnectionStatus.Error:
                    return new SolidColorBrush(error);
                    //break;
                default:
                    return new SolidColorBrush(disabled);
                    //break;
            }
        }

        /// <summary>
        /// Safely convert a nullable bool
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool BoolValue(bool? value)
        {
            if (!value.HasValue)
            {
                value = false;
            }
            return (bool) value;
        }
        #endregion
    }

    public class LogEntry
    {
        public DateTime TimeStamp { get; private set; }
        public string Message { get; private set; }
        public bool Request { get; private set; }
        public char Direction { get; private set; }
        public LogEntry(string message, bool request)
        {
            Message = message;
            Request = request;
            TimeStamp = DateTime.Now;
            Direction = request ? '>' : '<';
        }
    }

    public static class ConnectionList
    {
        public static Dictionary<string, string> Connections = new Dictionary<string, string>()
        {
            {"Local", "wss://localhost:44300/api/websocket"},
            {"Development VM", "wss://pentdev.pathf.com/service/api/websocket"},
            {"Quality Assurance VM", "wss://pentqa.pathf.com/service/api/websocket"},
            {"User Acceptance VM", "wss://pentuat.pathf.com/service/api/websocket"}
        };

        
    }
}
