-- =============================================================
--
-- Author:		Vladimir Beloglazov
-- Create date: 04/05/2018
--
-- Creates:
--
--		Table Type:			None
--
--		Stored Procedure:	PDeleteInstallationNotificationRecipient
--
-- =============================================================

USE PathfinderIntelliCenter
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (
	SELECT * FROM sys.objects
	WHERE object_id = OBJECT_ID(N'dbo.PDeleteInstallationNotificationRecipient')
	AND type in (N'P', N'PC')
)
DROP PROCEDURE dbo.PDeleteInstallationNotificationRecipient
GO

CREATE PROCEDURE dbo.PDeleteInstallationNotificationRecipient
	 @recipientIdList AS nvarchar(255)

AS
BEGIN

	SET NOCOUNT OFF

	DELETE FROM NotificationRecipients
	WHERE RecipientId IN (
		SELECT Item FROM dbo.FSplit(@recipientIdList, ',')
	)

END
GO
