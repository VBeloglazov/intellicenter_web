﻿'use strict';
var PWC = PWC || {};
PWC.controller('InstallationsGridController', [
    '$scope', '$location', '$route', '$filter', '$q', 'PropertyService', 'LocalStorageService', 'AppService', 'PropertyModel', 'uiGridConstants',
    function ($scope, $location, $route, $filter, $q, propertyService, localStorageService, appService, propertyModel, uiGridConstants) {
        var sortInfo = null;
        var searchTerm = "";
        $scope.Property = propertyService;
        $scope.Model = propertyModel;
        $scope.SearchTerm = "";
        $scope.$parent.Layout = "layout-base";
        $scope.ClearSearchTerm = function () {
            $scope.SearchTerm = "";
            searchTerm = "";
            refreshData();
        }

        $scope.SearchInstallations = function () {
            searchTerm = $scope.SearchTerm;
            refreshData();
        }

        $scope.GridViewModel = {
            connect: function (entity) {
                if (entity) {
                    propertyModel.SelectedInstallation = entity.InstallationId;
                    propertyService.Connect();
                }
            }
        };

        $scope.gridOptions = {
            useExternalSorting: true,
            enableHorizontalScrollbar: false,
            enableVerticalScrollbar: false,
            pagingPageSizes: [20, 50, 75],
            pagingPageSize: 20,
            useExternalPaging: true,
            enableColumnMenus: false,
            columnDefs: [
              { name: 'Pool Name', field: "PoolName", cellTemplate: '<a href="" ng-click="getExternalScopes().connect(row.entity)">{{COL_FIELD}}</a>' },
              { name: 'Address', field: "Address", enableSorting: false },
              { name: 'City/State', field: "Location" },
              { name: 'Phone Number', field: "Phone", enableSorting: false }
             // ,{ name: ' ', width:100, cellTemplate: '<a href="" ng-click="getExternalScopes().showMe(row.entity)">{{"CONNECT"|translate}}</a>', enableSorting: false }

            ],
            onRegisterApi: function (gridApi) {
                $scope.gridApi = gridApi;
                $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                    sortInfo = sortColumns[0];
                    fetchConnectfons();
                });
                $scope.gridApi.paging.on.pagingChanged($scope, function (newPage) {
                    fetchConnectfons(newPage);
                });
            }
        };

        function refreshData() {
            fetchConnectfons(0).then(function (result) {
                $scope.gridOptions.pagingCurrentPage = 0;
            });
        }

        function fetchConnectfons(page) {
            if (sortInfo) {
                return appService.Installations($scope.gridOptions.pagingPageSize, page, searchTerm, sortInfo.field, (sortInfo.sort.direction == uiGridConstants.DESC)).then(function (result) {
                    $scope.gridOptions.totalItems = JSON.parse(result.headers('x-pagination'))['TotalCount'];
                    $scope.gridOptions.data = result.data;
                });
            } else {
                return appService.Installations($scope.gridOptions.pagingPageSize, page, searchTerm).then(function (result) {
                    $scope.gridOptions.totalItems = JSON.parse(result.headers('x-pagination'))['TotalCount'];
                    $scope.gridOptions.data = result.data;
                });
            }
        }


        //set up initial grid data
        if ($route.current && $route.current.locals && $route.current.locals.installations) {
            propertyModel.Installations = $route.current.locals.installations.data;
            $scope.gridOptions.totalItems = JSON.parse($route.current.locals.installations.headers('x-pagination'))['TotalCount'];
        }

        $scope.gridOptions.data = propertyModel.Installations;

    }
]);