﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Pentair.Desktop.Simulator.Models;
using Pentair.Domain.Client.Entity;
using Pentair.Domain.Client.Helpers;
using Pentair.Domain.Client.Helpers.Commands;
using Pentair.Domain.Client.Infrastructure;

namespace Pentair.Desktop.Simulator.Views
{
    /// <summary>
    /// Interaction logic for Editor.xaml
    /// </summary>
    public partial class MessageEditor : Window
    {
        #region Private Properties

        private EditModel _model;

        private readonly Dictionary<StatMsgType, string> _statusMessages = new Dictionary<StatMsgType, string>()
        {
            {new StatMsgType(){Status = "FRZ", ObjectType = StaticData.CircuitType, Mode=1}, "Freezing Active"},
            {new StatMsgType(){Status = "VOLT", ObjectType = StaticData.PumpType, Mode=0}, "Over Voltage Alarm"},
            {new StatMsgType(){Status = "CURRENT", ObjectType = StaticData.PumpType, Mode=0}, "Over Current Alarm"},
            {new StatMsgType(){Status = "FAULT", ObjectType = StaticData.PumpType, Mode=0}, "Power Outage Alarm"},
            {new StatMsgType(){Status = "ALARM", ObjectType = StaticData.PumpType, Mode=0}, "Unknown Alarm"},
            {new StatMsgType(){Status = "SYSTEM", ObjectType = StaticData.PumpType, Mode=0}, "Pump Override"},
            {new StatMsgType(){Status = "PRIME", ObjectType = StaticData.PumpType, Mode=0}, "Priming Alarm"},
            {new StatMsgType(){Status = "DRVALM", ObjectType = StaticData.PumpType, Mode=0}, "Drive Temperature Alarm"},
            {new StatMsgType(){Status = "DRVWRN", ObjectType = StaticData.PumpType, Mode=1}, "Drive Temperature Warning"},
            {new StatMsgType(){Status = "FILTER", ObjectType = StaticData.PumpType, Mode=1}, "Filter Warning"},
            {new StatMsgType(){Status = "COMERR", ObjectType = StaticData.PumpType, Mode=1}, "Communication Lost"},
            {new StatMsgType(){Status = "DEVICE", ObjectType = "ICHLOR", Mode=0}, "PCB Error"},
            {new StatMsgType(){Status = "VOLT", ObjectType = "ICHLOR", Mode=0}, "Low Voltage Alarm"},
            {new StatMsgType(){Status = "CURRENT", ObjectType = "ICHLOR", Mode=0}, "High Current Alarm"},
            {new StatMsgType(){Status = "CUTOFF", ObjectType = "ICHLOR", Mode=0}, "Cold Water Cutoff Alarm"},
            {new StatMsgType(){Status = "SALTLO", ObjectType = "ICHLOR", Mode=1}, "Low Salt Warning"},
            {new StatMsgType(){Status = "VERYLO", ObjectType = "ICHLOR", Mode=1}, "Very Low Salt Warning"},
            {new StatMsgType(){Status = "CLEAN", ObjectType = "ICHLOR", Mode=1}, "Clean and Inspect Alarm"},
            {new StatMsgType(){Status = "COMERR", ObjectType = "ICHLOR", Mode=1}, "Communications Lost"},
            {new StatMsgType(){Status = "NOFLO", ObjectType = "ICHEM", Mode=0}, "No Flow"},
            {new StatMsgType(){Status = "PHHI", ObjectType = "ICHEM", Mode=0}, "pH Too High"},
            {new StatMsgType(){Status = "PHLO", ObjectType = "ICHEM", Mode=0}, "pH Too Low"},
            {new StatMsgType(){Status = "ORPLO", ObjectType = "ICHEM", Mode=0}, "ORP Too Low"},
            {new StatMsgType(){Status = "PHCHK", ObjectType = "ICHEM", Mode=0}, "Check pH Chemical Container"},
            {new StatMsgType(){Status = "ORPCHK", ObjectType = "ICHEM", Mode=0}, "Check ORP Chemical Container"},
            {new StatMsgType(){Status = "LOCKOUT", ObjectType = "ICHEM", Mode=0}, "Sanitizer Locked Out"},
            {new StatMsgType(){Status = "PHLIM", ObjectType = "ICHEM", Mode=0}, "pH at Feed Limit"},
            {new StatMsgType(){Status = "ORPLIM", ObjectType = "ICHEM", Mode=0}, "ORP at Feed Limit"},
            {new StatMsgType(){Status = "INVALID", ObjectType = "ICHEM", Mode=0}, "Invalid Settings"},
            {new StatMsgType(){Status = "CALIB", ObjectType = "ICHEM", Mode=0}, "Calibration Failed"},
            {new StatMsgType(){Status = "COMERR", ObjectType = "ICHEM", Mode=0}, "Automation Comm Error"},
            {new StatMsgType(){Status = "ORPHI", ObjectType = "ICHEM", Mode=1}, "ORP Too High"},
            {new StatMsgType(){Status = "PROBE", ObjectType = "ICHEM", Mode=1}, "Probe Check Failed"},
            {new StatMsgType(){Status = "COMLNK", ObjectType = "ICHEM", Mode=1}, "Peripheral Comm Error"},
            {new StatMsgType(){Status = "DEVICE", ObjectType = "ICHEM", Mode=1}, "Memory Errors"},
            {new StatMsgType(){Status = "HITMP", ObjectType = StaticData.HeaterType, Mode=0}, "High Water Temperature"},
            {new StatMsgType(){Status = "LOTMP", ObjectType = StaticData.HeaterType, Mode=0}, "Low Water Temperature"},
            {new StatMsgType(){Status = "FLOW", ObjectType = StaticData.HeaterType, Mode=0}, "Low Water Flow"},
            {new StatMsgType(){Status = "RLY", ObjectType = StaticData.HeaterType, Mode=0}, "Remote Relay"},
            {new StatMsgType(){Status = "OFF", ObjectType = StaticData.HeaterType, Mode=0}, "Water Thermistor open"},
            {new StatMsgType(){Status = "ON", ObjectType = StaticData.HeaterType, Mode=0}, "Water Thermistor shorted"},
            {new StatMsgType(){Status = "OVROFF", ObjectType = StaticData.HeaterType, Mode=0}, "Defrost Thermistor open"},
            {new StatMsgType(){Status = "OVRON", ObjectType = StaticData.HeaterType, Mode=0}, "Defrost Thermistor shorted"},
            {new StatMsgType(){Status = "OUT", ObjectType = StaticData.HeaterType, Mode=0}, "Brownout"},
            {new StatMsgType(){Status = "HI", ObjectType = StaticData.HeaterType, Mode=0}, "High refrigerant pressure"},
            {new StatMsgType(){Status = "LO", ObjectType = StaticData.HeaterType, Mode=0}, "Low refrigerant pressure"}
        };
        #endregion

        #region Constructor
        public MessageEditor(EditModel data)
        {
            _model = data;
            DataContext = this._model;
            _model.objnam = _model.Object.OBJNAM;
            _model.objtyp = _model.Object.OBJTYP;
            _model.sname = _model.Object.SNAME;
            _model.parent = _model.Object.PARENT;
            _model.sindex = _model.Object.SINDEX;
            _model.mode = _model.Object.MODE;
            SelectParents();
            if (!string.IsNullOrEmpty(_model.Object.PARENT))
            {
                _model.parent = _model.Object.PARENT;
                SetParentProperties();
            }
            InitializeComponent();
        }

        #endregion

        #region Events

        private void cancel_click(object sender, RoutedEventArgs e)
        {
            _model.Changes = false;
            this.Close();
        }

        private void delete_click(object sender, RoutedEventArgs e)
        {
            using (var db = new SimulatorContext())
            {
                var eb = new BusinessLogic.BusinessLogic(db, _model.Object.INSTALLATION);

                var o = new ControlObject
                    {
                        INSTALLATION = _model.Object.INSTALLATION,
                        OBJNAM = _model.objnam,
                        Revision = Globals.CurrentTime(),
                        Deleted = true,
                        Key = StaticData.ObjectType,
                        Value = StaticData.StatusMessageType
                    };
                eb.DeleteObject(o);
            }
            _model.Changes = true;
            _model.DeleteFlag = true;
            this.Close();
        }


        private void save_click(object sender, RoutedEventArgs e)
        {
            _model.Changes = true;
            _model.Object.OBJTYP = StaticData.StatusMessageType;
            _model.Object.PARENT = _model.parent;
            _model.Object.SINDEX = _model.sindex;
            _model.Object.SNAME = _model.sname;
            _model.Object.MODE = _model.mode;
            _model.Object.SHOMNU = "OFF";
            using (var db = new SimulatorContext())
            {
                var eb = new BusinessLogic.BusinessLogic(db, _model.Object.INSTALLATION);
                var obj = new NewParamObject(objTyp: StaticData.StatusMessageType);

                var ts = Globals.CurrentTime();
                if (_model.CreateFlag)
                {
                    _model.objnam = eb.CreateObject(obj.ObjTyp);
                    _model.Object.OBJNAM = _model.objnam;
                    db.ControlObjects.Add(new ControlObject()
                    {
                        OBJNAM = _model.objnam,
                        Revision = ts,
                        INSTALLATION = _model.Object.INSTALLATION,
                        Key = StaticData.Parent,
                        Value = _model.parent
                    });
                    db.ControlObjects.Add(new ControlObject()
                    {
                        OBJNAM = _model.objnam,
                        Revision = ts,
                        INSTALLATION = _model.Object.INSTALLATION,
                        Key = StaticData.SIndex,
                        Value = _model.sindex
                    });
                    db.ControlObjects.Add(new ControlObject()
                    {
                        OBJNAM = _model.objnam,
                        Revision = ts,
                        INSTALLATION = _model.Object.INSTALLATION,
                        Key = StaticData.SystemName,
                        Value = _model.sname
                    });
                    db.ControlObjects.Add(new ControlObject()
                    {
                        OBJNAM = _model.objnam,
                        Revision = ts,
                        INSTALLATION = _model.Object.INSTALLATION,
                        Key = StaticData.Mode,
                        Value = _model.mode
                    });
                    db.ControlObjects.Add(new ControlObject()
                    {
                        OBJNAM = _model.objnam,
                        Revision = ts,
                        INSTALLATION = _model.Object.INSTALLATION,
                        Key = StaticData.ShowMenu,
                        Value = "OFF"
                    });
                }

                else
                {

                }

                db.SaveChanges();
            }
            this.Close();
        }

        private void parent_changed(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(_model.parent))
            {
                SetParentProperties();
                _model.Changes = true;
                _model.OnPropertyChanged("Changes");
            }
        }

        private void status_changed(object sender, SelectionChangedEventArgs e)
        {
            if (!string.IsNullOrEmpty(_model.sindex))
            {
                var msg = (from item in _statusMessages
                    where item.Key.ObjectType == _model.MessageType &&
                          item.Key.Status == _model.sindex
                    select item).FirstOrDefault();
                _model.sname = msg.Value;
                _model.mode = msg.Key.Mode.ToString();
                _model.OnPropertyChanged("sname");
                _model.Changes = true;
                _model.OnPropertyChanged("Changes");
            }
        }
        #endregion

        #region Private methods

        private void SelectParents()
        {
            using (var db = new SimulatorContext())
            {
                var parents = (from o in db.ObjectViews
                    where o.INSTALLATION == _model.Object.INSTALLATION &&
                            (o.OBJTYP == StaticData.PumpType ||
                             o.OBJTYP == StaticData.HeaterType || 
                             o.SUBTYP == "ICHEM" ||
                             o.SUBTYP == "ICHLOR" ||
                             (o.OBJTYP == StaticData.CircuitType && o.SUBTYP == "FRZ"))
                    select o.OBJNAM).ToList();
                _model.parents = new ObservableCollection<string>();
                foreach (var item in parents)
                {
                    _model.parents.Add(item);
                }

            }
        }

        private void SetParentProperties()
        {
            using (var db = new SimulatorContext())
            {
                var parent = db.ObjectViews.FirstOrDefault(x => x.INSTALLATION == _model.Object.INSTALLATION && x.OBJNAM == _model.parent);
                if (null != parent)
                {
                    // Get the message type.
                    _model.MessageType = parent.OBJTYP == StaticData.PumpType ||
                               parent.OBJTYP == StaticData.CircuitType ||
                               parent.OBJTYP == StaticData.HeaterType
                        ? parent.OBJTYP
                        : parent.SUBTYP;
                    var sindex = (from o in _statusMessages
                        where o.Key.ObjectType == _model.MessageType
                        select o.Key.Status).ToList();

                    _model.sindexSelector = new ObservableCollection<string>();
                    foreach (var item in sindex)
                    {
                        _model.sindexSelector.Add(item);
                    }
                    _model.parentType = parent.OBJTYP;
                    _model.OnPropertyChanged("sindexSelector");
                    _model.OnPropertyChanged("parentType");
                }
            }
        }
        #endregion

    }

    internal class StatMsgType
    {
        public string ObjectType { get; set; }
        public string Status { get; set; }
        public int Mode { get; set; }
    }
}
