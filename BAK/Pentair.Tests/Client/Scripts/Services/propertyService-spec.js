﻿
'use strict';
describe('PropertyService', function () {
    var SocketService;
    var Enumerations;
    var PropertyService;
    var PropertyModel;
    var $route;
    var mockInstallations;
    var AppService;
    var $rootScope;
    var $q;
    var deferred;
    var heatModesFilter;
    var messageProcessor;
    var $location;
    var mockSocket;
    beforeEach(module('PentairWebClient'));

    beforeEach(inject(function (_$controller_, _$rootScope_, _$q_, _heatModesFilter_, _SocketService_, _Enumerations_, _PropertyService_,
                            _$route_, _AppService_, _$location_, _PropertyModel_, _MessageProcessor_) {
        SocketService = _SocketService_;
        Enumerations = _Enumerations_;
        $route = _$route_;
        PropertyService = _PropertyService_;
        PropertyModel = _PropertyModel_;
        heatModesFilter = _heatModesFilter_;
        messageProcessor = _MessageProcessor_;
        AppService = _AppService_;
        $rootScope = _$rootScope_;
        $location = _$location_;
        $q = _$q_;
        deferred = $q.defer();
        mockSocket = {
            readyState: 0,
            onOpen: jasmine.createSpy(),
            onError: jasmine.createSpy(),
            onClose: jasmine.createSpy(),
            open: jasmine.createSpy(),
            close: jasmine.createSpy(),
            addEventListener: jasmine.createSpy()
        };
        spyOn(PropertyModel, 'Clear');

        spyOn(SocketService, 'newSocket').and.returnValue(mockSocket);
        mockInstallations = {
            data: [
                {
                    "InstallationId": "1f92ba9c-077a-4cac-bdd2-1dee3579911a",
                    "PoolName": "hewitts pool 1",
                    "OnLine": false
                },
                {
                    "InstallationId": "c077a376-e4f5-40b9-a495-ace172297e1c",
                    "PoolName": "Hewitt Pool 4",
                    "OnLine": true
                },
                {
                    "InstallationId": "f467e58f-307f-43f4-a9dc-df7bc3828a88",
                    "PoolName": "Hewitt Pool 2",
                    "OnLine": false
                }
            ]
        };
        $route.current = {
            locals: { 'installations': mockInstallations }
        };

        //spy on console log to avoid log message 
        //Uncomment for debugging 
        spyOn(console, 'log');
    }));

    describe('Default vars', function () {

        it('should have these default values', function () {
            //   expect(PropertyService.Installations).toEqual(mockInstallations);
            expect(PropertyModel.SelectedInstallation).toBe(Enumerations.DefaultInstallation.InstallationId);
            expect(PropertyModel.ConnectionStatus).toBe("Not Connected");
            expect(PropertyModel.Objects).toEqual({});
            expect(PropertyModel.Bodies).toEqual([]);
            expect(PropertyModel.Circuits).toEqual([]);
            expect(PropertyModel.Heaters).toEqual([]);
        });
    });

    describe('toggle circuit', function () {
        var circuit;
        beforeEach(function () {
            spyOn(PropertyService, 'SetValue');
            circuit = SpecHelper.mocks.Circuits[0];
            PropertyService.ToggleCircuit(circuit, "ON");
        });
        it('should send a SetParamList Message to turn Circuit ON', function () {
            expect(PropertyService.SetValue).toHaveBeenCalledWith(circuit.OBJNAM, 'STATUS', 'ON');
        });
    });


    describe('increase Value', function () {
        var body;
        beforeEach(function () {
            spyOn(PropertyService, 'SetValue');
            body = SpecHelper.mocks.Bodies[0];
            body.LOTMP = '98';
        });
        it('should send a SetParamList Message to set Min Value to 99', function () {
            PropertyService.IncreaseValue(body, 'LOTMP', 104);
            expect(PropertyService.SetValue).toHaveBeenCalledWith(body.OBJNAM, 'LOTMP', 99);
        });

        it('should send a SetParamList Message to set Min Value to 99', function () {
            body.LOTMP = '98F';
            PropertyService.IncreaseValue(body, 'LOTMP', 104);
            expect(PropertyService.SetValue).toHaveBeenCalledWith(body.OBJNAM, 'LOTMP', 99);
        });

        it('should NOT send a SetParamList Message when provided with unfound property', function () {
            PropertyService.IncreaseValue(body, '!INVALID', 104);
            expect(PropertyService.SetValue).not.toHaveBeenCalled();
        });
        it('should NOT send a SetParamList Message when provided with null property', function () {
            PropertyService.IncreaseValue(body, null, 104);
            expect(PropertyService.SetValue).not.toHaveBeenCalled();
        });

        it('should NOT send a SetParamList Message when provided with invalid body', function () {
            body = undefined;
            PropertyService.IncreaseValue(body, 'LOTMP', 104);
            expect(PropertyService.SetValue).not.toHaveBeenCalled();
        });

        it('should NOT send a SetParamList Message when provided with null body', function () {
            body = null;
            PropertyService.IncreaseValue(body, 'LOTMP', 104);
            expect(PropertyService.SetValue).not.toHaveBeenCalled();
        });
    });


    describe('decrease Value', function () {
        var body;
        beforeEach(function () {
            spyOn(PropertyService, 'SetValue');
            body = SpecHelper.mocks.Bodies[1];
            body.LOTMP = '99';
        });

        it('should send a SetParamList Message to set Min Value to 98', function () {
            PropertyService.DecreaseValue(body, 'LOTMP', 40);
            expect(PropertyService.SetValue).toHaveBeenCalledWith(body.OBJNAM, 'LOTMP', 98);
        });

        it('should send a SetParamList Message to set Min Value to 99', function () {
            body.LOTMP = '100F';
            PropertyService.DecreaseValue(body, 'LOTMP', 40);
            expect(PropertyService.SetValue).toHaveBeenCalledWith(body.OBJNAM, 'LOTMP', 99);
        });
        it('should NOT send a SetParamList Message when provided with unfound property', function () {
            PropertyService.DecreaseValue(body, '!INVALID', 40);
            expect(PropertyService.SetValue).not.toHaveBeenCalled();
        });
        it('should NOT send a SetParamList Message when provided with null property', function () {
            PropertyService.DecreaseValue(body, null, 40);
            expect(PropertyService.SetValue).not.toHaveBeenCalled();
        });

        it('should NOT send a SetParamList Message when provided with invalid body', function () {
            body = undefined;
            PropertyService.DecreaseValue(body, 'LOTMP', 40);
            expect(PropertyService.SetValue).not.toHaveBeenCalled();
        });

        it('should NOT send a SetParamList Message when provided with null body', function () {
            body = null;
            PropertyService.DecreaseValue(body, 'LOTMP', 40);
            expect(PropertyService.SetValue).not.toHaveBeenCalled();
        });
    });


    describe('Set Value', function () {
        var body;
        beforeEach(function () {
            spyOn(SocketService, 'sendMessage');
            body = SpecHelper.mocks.Bodies[1];
            body.LOTMP = '99';
        });

        it('should send a SetParamList Message to set Min Value to 98', function () {
            var message = {
                "command": "SETPARAMLIST",
                "objectList": [
                    {
                        "objnam": "BSpa",
                        "params": { "LOTMP": "88" }
                    }
                ]
            }
            PropertyService.SetValue(body.OBJNAM, 'LOTMP', 88);
            expect(SocketService.sendMessage).toHaveBeenCalledWith(message);
        });

        it('should NOT send a SetParamList Message When an undefined object is provided', function () {
            PropertyService.SetValue(undefined, 'LOTMP', 88);
            expect(SocketService.sendMessage).not.toHaveBeenCalled();
        });

        it('should NOT send a SetParamList Message When an null object is provided', function () {
            PropertyService.SetValue(null, 'HITMP', 88);
            expect(SocketService.sendMessage).not.toHaveBeenCalled();
        });
    });

    describe('Set Heat Modes', function () {
        var body, newObj, message;
        beforeEach(function () {
            spyOn(SocketService, 'sendMessage');
            body = SpecHelper.mocks.Bodies[0];
            body.HEATERS = {
                HCOMBO: {},
                GENERIC: {
                    HEATER: {},
                    HCOMBO: {}
                },
                SECONDARY: {
                    HEATER: {},
                    HCOMBO: {}
                }
            }

            newObj = {
                objnam: body.OBJNAM,
                params: { HEATER: '000FF' }
            }

            message = {
                command: "SETPARAMLIST",
                objectList: [newObj]
            }

            PropertyModel.Objects[body.OBJNAM] = body;
        });

        it('should set the heat to none', function () {
            newObj.params.HEATER = '000FF';

            PropertyService.SetHeatModes(body.OBJNAM, 'NONE');
            expect(SocketService.sendMessage).toHaveBeenCalledWith(message);
        });

        it('should send a message to set body.heater to Generic heater', function () {
            newObj.params.HEATER = 'H1000';
            body.HEATERS.GENERIC.HEATER = SpecHelper.mocks.Heaters[0];

            PropertyService.SetHeatModes(body.OBJNAM, 'GENERIC');
            expect(SocketService.sendMessage).toHaveBeenCalledWith(message);
        });

        it('should send a message to set  body.heater to Soloar heater', function () {
            newObj.params.HEATER = 'H1001';
            body.HEATERS.SECONDARY.HEATER = SpecHelper.mocks.Heaters[1];

            PropertyService.SetHeatModes(body.OBJNAM, 'ONLY');
            expect(SocketService.sendMessage).toHaveBeenCalledWith(message);
        });

        it('should send a message to set body.heater to HCOMBO heater', function () {

            body.HEATERS.VIRTUAL = SpecHelper.mocks.Heaters[2];
            body.HEATERS.SECONDARY.HCOMBO = SpecHelper.mocks.HeaterCombos[0];
            body.HEATERS.GENERIC.HCOMBO = SpecHelper.mocks.HeaterCombos[1];

            newObj.params.HEATER = 'H1002';

            PropertyService.SetHeatModes(body.OBJNAM, 'PREF');
            expect(SocketService.sendMessage).toHaveBeenCalledWith(message);
        });
    });


    describe('set all lights', function () {
        var message = {
            command: "SETPARAMLIST",
            objectList: [
                {
                    objnam: "",
                    params: {}
                }
            ]
        }
        beforeEach(function () {
            spyOn(SocketService, 'sendMessage');
        });

        it('should send the status of the ALL lights object to OFF', function () {
            message.objectList[0].objnam = Enumerations.AllLightsOffObj;
            message.objectList[0].params.STATUS = "OFF";
            PropertyService.SetAllLightsOff();
            expect(SocketService.sendMessage).toHaveBeenCalledWith(message);
        });

        it('should send the status of the ALL lights object to ON', function () {
            message.objectList[0].objnam = Enumerations.AllLightsOnObj;
            message.objectList[0].params.STATUS = "ON";
            PropertyService.SetAllLightsOn();
            expect(SocketService.sendMessage).toHaveBeenCalledWith(message);
        });
    });


    describe('Get Param List by object type', function () {
        beforeEach(function () {
            spyOn(SocketService, 'sendMessage');
        });

        it('should send request and add an item to the message queue', function () {
            var message = {
                command: "GETPARAMLIST",
                condition: "OBJTYP=GETOBJS",
                objectList: [
                    {
                        objnam: "ALL",
                        keys: ["OBJTYPE"]
                    }
                ]
            };

            PropertyService.GetParamListByOBJTYP("OBJTYPE", "GETOBJS");
            expect(SocketService.sendMessage).toHaveBeenCalledWith(message);
        });

    });


    describe('Get ParamList by Object and Object Sub type', function () {
        beforeEach(function () {
            spyOn(SocketService, 'sendMessage');
        });

        it('should send request', function () {
            var message = {
                command: "GETPARAMLIST",
                condition: "OBJTYP=GETOBJS & SUBTYP=" + "subTypeName",
                objectList: [
                    {
                        objnam: "ALL",
                        keys: ["OBJTYPE"]
                    }
                ]
            };

            PropertyService.GetParamListBySUBTYP("OBJTYPE", "GETOBJS", "subTypeName");
            expect(SocketService.sendMessage).toHaveBeenCalledWith(message);
        });

    });


    describe('set light mode', function () {
        var message = {
            command: "SETPARAMLIST",
            objectList: [
                {
                    objnam: "",
                    params: {}
                }
            ]
        }
        beforeEach(function () {
            spyOn(SocketService, 'sendMessage');
            PropertyModel.Circuits = SpecHelper.mocks.ColorLights;
        });

        it('should send the ACT value of the ALL Intelli Lights to blue', function () {
            message.objectList[0].objnam = 'Intelli';
            message.objectList[0].params.ACT = "BLUE";
            PropertyService.SetLightMode(['INTELLI'], 'BLUE');
            expect(SocketService.sendMessage).toHaveBeenCalledWith(message);
        });

        it('should send the ACT value of the ALL MagicStream Lights to RED', function () {
            message.objectList = [
                {
                    objnam: "Magic2",
                    params: { ACT: 'RED' }
                }, {
                    objnam: "Magic1",
                    params: { ACT: 'RED' }
                }
            ];
            PropertyService.SetLightMode(['MAGIC1', 'MAGIC2'], 'RED');
            expect(SocketService.sendMessage).toHaveBeenCalledWith(message);
        });

    });


    describe('cancel delay', function () {
        beforeEach(function () {
            spyOn(SocketService, 'sendMessage');
        });
        it('should send a message to cancel the delay', function () {
            var message = {
                command: "SETPARAMLIST",
                objectList: [
                    {
                        objnam: "BPool",
                        params: { DLYCNCL: '' }
                    }
                ]
            }
            PropertyService.CancelDelay('BPool');
            expect(SocketService.sendMessage).toHaveBeenCalledWith(message);

        });
    });


    describe('get delay by Object Name', function () {
        beforeEach(function () {
            spyOn(SocketService, 'sendMessage');
        });
        it('should send a message to get an objects TIMEOUT AND SOURCE', function () {
            var message = {
                command: "GETPARAMLIST",
                condition: "",
                objectList: [
                    {
                        objnam: "BPool",
                        keys: ['TIMOUT : SOURCE']
                    }
                ]
            }
            PropertyService.GetDelayByOBJNAM('BPool', 'GetObject');
            expect(SocketService.sendMessage).toHaveBeenCalledWith(message);

        });
    });


    describe('invite user', function () {
        var email = "EMAIL@pentair.com";
        var group = 'ADMIN';
        beforeEach(function () {
            spyOn(SocketService, 'sendMessage').and.returnValue(deferred.promise);
        });
        describe('Send Invite', function () {
            it('should send a message to invite a user', function () {

                var message = {
                    command: "INVITEUSER",
                    params: {
                        EMAIL: email,
                        PERMISSIONGROUP: group
                    }
                }
                PropertyService.InviteUser(email, group);
                expect(SocketService.sendMessage).toHaveBeenCalledWith(message);
            });
        });

        describe('Invite Response', function () {
            email = 'Error@Email.com';
            it('should handle Error', function () {

                PropertyService.InviteUser(email, group);

            });
        });

    });

    describe('Fetch Installations', function () {
        beforeEach(function () {

            spyOn(AppService, 'Installations').and.returnValue({
                then: function (success, error) {
                    success(mockInstallations);
                }
            });
        });

        it('should fetch installations and assign to scope', function () {
            PropertyService.FetchInstallations();
            expect(AppService.Installations).toHaveBeenCalled();
            expect(PropertyModel.Installations).toEqual(mockInstallations.data);
        });

    });


    describe('delete user', function () {
        var email = "user@pentair.com";
        var status = "PENDING";
        var notify = true;
        beforeEach(function () {
            spyOn(SocketService, 'sendMessage').and.returnValue(deferred.promise);
        });
        it('should send a message to delete a user', function () {

            var message = {
                command: "DELETEUSER",
                params: {
                    EMAIL: email,
                    STATUS: status,
                    NOTIFY: notify
                }
            }
            PropertyService.DeleteUser(email, status, notify);
            expect(SocketService.sendMessage).toHaveBeenCalledWith(message);
        });
    });

    describe('CreateNewObject', function () {
        it('should send a message to create a new schedule object and return a promise', function () {
            spyOn(SocketService, 'sendMessage');
            var message = {
                command: "CREATEOBJECTFROMPARAM",
                objectList: [
                    {
                        objtyp: "SCHED",
                        keys: []
                    }
                ]
            }
            PropertyService.CreateNewObject("SCHED");
           // expect(SocketService.sendMessage).toHaveBeenCalledWith(message);
        });
    });

    describe('SetParams', function () {
        it('should accept an object name and a list of params to send a SetParamList message through the socket', function () {
            spyOn(SocketService, 'sendMessage');
            var params = { test: 'value' };
            var message = {
                command: "SETPARAMLIST",
                objectList: [
                    {
                        objnam: "B1000",
                        params: params
                    }
                ]
            }
            PropertyService.SetParams('B1000', params);
            expect(SocketService.sendMessage).toHaveBeenCalledWith(message);
        });
    });

    describe('EditUser', function () {
        var processManageUserDeferred;
        beforeEach(function () {
            processManageUserDeferred = $q.defer();
        });
        it('should accept a email and a level to send EDITUSER message through the socket', function () {
            spyOn(SocketService, 'sendMessage').and.returnValue(deferred.promise);
            spyOn(PropertyService, 'GetUserList');
            spyOn(messageProcessor, 'ProcessManageUserMessage').and.returnValue(processManageUserDeferred.promise);
            var email = 'unit@test.com';
            var level = 'TESTER';
            var message = {
                command: "EDITUSER",
                params: {
                    EMAIL: email,
                    PERMISSIONGROUP: level
                }

            }
            PropertyService.EditUser(email, level);
            deferred.resolve(message);
            processManageUserDeferred.resolve();
            //   $rootScope.$apply();
            expect(SocketService.sendMessage).toHaveBeenCalledWith(message);
            // expect(messageProcessor.ProcessManageUserMessage).toHaveBeenCalledWith(message);
            // expect(PropertyService.GetUserList).toHaveBeenCalledWith(message);
        });
    });


    describe('Reset', function () {
        beforeEach(function () {
            mockSocket.readyState = 1;
            spyOn(SocketService, 'clearSubscriptions').and.returnValue(deferred.promise);

        });
        it('should clear the model', function () {
            PropertyService.Reset();
            expect(PropertyModel.Clear).toHaveBeenCalled();
            expect(SocketService.clearSubscriptions).not.toHaveBeenCalled();

        });
        it('should send a message to clear all subscriptions and close the socket if open, then clear the model', function () {
            PropertyService.Connect('test');
            PropertyService.Reset();

            deferred.resolve();
            //   $rootScope.$apply();

            //expect(SocketService.Close).toHaveBeenCalled();
            expect(SocketService.clearSubscriptions).toHaveBeenCalled();
            expect(PropertyModel.Clear).toHaveBeenCalled();

        });
    });
    describe('Connect', function () {
        var currentInstallationselection;

        beforeEach(function () {
            PropertyService.Reset();
            spyOn(PropertyService, 'Reset');
            currentInstallationselection = 'c077a376-e4f5-40b9-a495-ace172297e1c';

        });


        it('should request new socket if selected installation is valid', function () {
            PropertyModel.SelectedInstallation = currentInstallationselection;
            PropertyService.Connect();

            expect(PropertyService.Reset).toHaveBeenCalled();
            expect(SocketService.newSocket).toHaveBeenCalledWith(currentInstallationselection);
            expect(PropertyModel.ConnectionStatus).toBe("Connecting...");
        });

        xit('should save the user security token', function () {
            PropertyModel.SelectedInstallation = currentInstallationselection;
            PropertyModel.Installations = SpecHelper.mocks.Installations;
            var expectedObject = { OBJNAM: 'TESTtoken2', SHOMNU: '' }

            PropertyService.Connect();

            expect(PropertyModel.ACL).toEqual(expectedObject);
            expect(PropertyModel.Objects.TESTtoken2).toEqual(expectedObject);
        });
        it('should not request new socket if selected installation is INvalid', function () {
            PropertyModel.SelectedInstallation = null;
            mockSocket.readyState = 3;
            PropertyService.Connect();
            expect(SocketService.newSocket).not.toHaveBeenCalled();
            expect(PropertyModel.ConnectionStatus).toBe("Not Connected");
        });

        it('should set the selected installation if a selection is provided', function () {

            var selection = 'Test-ID';
            PropertyService.Connect(selection);

            expect(PropertyModel.SelectedInstallation).toEqual(selection);
            expect(SocketService.newSocket).toHaveBeenCalledWith(selection);
            expect(PropertyModel.ConnectionStatus).toBe("Connecting...");
        });

        it('should add listner to socket.close if Connection is open ', function () {
            PropertyModel.SelectedInstallation = currentInstallationselection;
            PropertyService.Connect();
            //connect twice to simulate an open socket
            PropertyService.Connect();

            expect(mockSocket.addEventListener).toHaveBeenCalledWith('close', jasmine.any(Function));
            expect(SocketService.newSocket.calls.count()).toBe(1);
        });

    });

    describe('Change Connection', function () {
        it('should clear the model, close the socket and connect to new socket', function () {
            spyOn(PropertyService, 'Connect');
            spyOn(PropertyService, 'Reset');

            PropertyService.ChangeConnection('NewProperty');
            expect(PropertyService.Reset).toHaveBeenCalled();
            expect(PropertyService.Connect).toHaveBeenCalledWith('NewProperty');
        });
    });
});

