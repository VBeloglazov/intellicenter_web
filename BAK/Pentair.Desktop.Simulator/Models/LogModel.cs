﻿using System.Collections.ObjectModel;
using Pentair.Desktop.Simulator.BusinessLogic;

namespace Pentair.Desktop.Simulator.Models
{
    public class LogModel
    {
        /// <summary>
        /// Collection of log entries
        /// </summary>
        public ObservableCollection<LogEntry> Messages { get; private set; }

        /// <summary>
        /// Constructor
        /// </summary>
        public LogModel()
        {
            Messages = new ObservableCollection<LogEntry>();
        }

        public void Add(string message, bool direction)
        {
            Messages.Add(new LogEntry(message, direction));
        }
    }
}
