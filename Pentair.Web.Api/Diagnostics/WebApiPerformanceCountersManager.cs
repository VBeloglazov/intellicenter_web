﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

using SERVICE_SHARED;

namespace Pentair.Web.Api
{
    public class WebApiPerformanceCountersManager : PerformanceCountersManager
    {
        const string CATEGORY_NAME = "PENTAIR_WEB_API";
        const string CATEGORY_HELP = "Pentair Web Api metrics";

        const string COUNTER_NAME_TOTALCONNECTED_THW = "Total Connected THWs";
        const string COUNTER_HELP_TOTALCONNECTED_THW = "Number of connected THWs";

        const string COUNTER_NAME_TOTALCONNECTED_CLIENT = "Total Connected Clients";
        const string COUNTER_HELP_TOTALCONNECTED_CLIENT = "Number of connected Clients";

        const string COUNTER_NAME_CONNECTIONSPERSEC = "Connections/sec";
        const string COUNTER_HELP_CONNECTIONSPERSEC = "Connected per second";

        const string COUNTER_NAME_REQUESTSPERSEC = "Requests/sec";
        const string COUNTER_HELP_REQUESTSPERSEC = "Received per second";

        const string COUNTER_NAME_LATENCY_CONNECT = "Connection Latency (msec)";
        const string COUNTER_HELP_LATENCY_CONNECT = "Establishing connection time";

        const string COUNTER_NAME_LATENCY_REQUEST = "Request Latency (msec)";
        const string COUNTER_HELP_LATENCY_REQUEST = "Request processing time";

        #region Public Properties

        public string CounterTotalConnectedThw { get { return COUNTER_NAME_TOTALCONNECTED_THW; } }
        public string CounterTotalConnectedClient { get { return COUNTER_NAME_TOTALCONNECTED_CLIENT; } }
        public string CounterConnectionsPerSec { get { return COUNTER_NAME_CONNECTIONSPERSEC; } }
        public string CounterRequestsPerSec { get { return COUNTER_NAME_REQUESTSPERSEC; } }
        public string CounterLatencyConnect { get { return COUNTER_NAME_LATENCY_CONNECT; } }
        public string CounterLatencyRequest { get { return COUNTER_NAME_LATENCY_REQUEST; } }

        #endregion Public Properties

        /// <exception cref="Exception" />
        public WebApiPerformanceCountersManager()
            : base(
                CATEGORY_NAME,
                CATEGORY_HELP,
                new CounterCreationData[]
                {
                    // Connections Total THWs
                    new CounterCreationData()
                    {
                        CounterName = COUNTER_NAME_TOTALCONNECTED_THW,
                        CounterHelp = COUNTER_HELP_TOTALCONNECTED_THW,
                        CounterType = PerformanceCounterType.NumberOfItems32
                    }

                    // Connections Total Clients
                    , new CounterCreationData()
                    {
                        CounterName = COUNTER_NAME_TOTALCONNECTED_CLIENT,
                        CounterHelp = COUNTER_HELP_TOTALCONNECTED_CLIENT,
                        CounterType = PerformanceCounterType.NumberOfItems32
                    }

                    // Connections per second THW
                    , new CounterCreationData()
                    {
                        CounterName = COUNTER_NAME_CONNECTIONSPERSEC,
                        CounterHelp = COUNTER_HELP_CONNECTIONSPERSEC,
                        CounterType = PerformanceCounterType.RateOfCountsPerSecond32
                    }

                    // Requests per second
                    , new CounterCreationData()
                    {
                        CounterName = COUNTER_NAME_REQUESTSPERSEC,
                        CounterHelp = COUNTER_HELP_REQUESTSPERSEC,
                        CounterType = PerformanceCounterType.RateOfCountsPerSecond32
                    }

                    // Latency Connect
                    , new CounterCreationData()
                    {
                        CounterName = COUNTER_NAME_LATENCY_CONNECT,
                        CounterHelp = COUNTER_HELP_LATENCY_CONNECT,
                        CounterType = PerformanceCounterType.NumberOfItems32
                    }

                    // Latency Request
                    , new CounterCreationData()
                    {
                        CounterName = COUNTER_NAME_LATENCY_REQUEST,
                        CounterHelp = COUNTER_HELP_LATENCY_REQUEST,
                        CounterType = PerformanceCounterType.NumberOfItems32
                    }
                }
            )
        {
        } // WebApiPerformanceCountersManager()

    } // class WebApiPerformanceCountersManager
}
