﻿var PWC = PWC || {};

PWC.controller('LoginController', ['$scope', '$location', '$route', 'AuthService', 'AppService', 'PropertyService', 'PropertyModel',
    function ($scope, $location, $route, authService, appService, propertyService, propertyModel) {

        $scope.AuthService = authService;

        $scope.message = "";
        $scope.validateOldPassword = false;

        $scope.$parent.Layout = "layout-login";
        $scope.changePasswordFlag = true;

        $scope.passwordType = 'password';

        $scope.loginData = {
            userName: "",
            password: "",
            rememberMe: true,
            invalid : false
        };

        $scope.changePasswordData = {
            user: "",
            code: "",
            oldPassword: "",
            newPassword: "",
            confirmPassword: "",
            error: false,
            requestSubmitted: false
        }

        $scope.forgotPasswordData = {
            email: ""
        }

        $scope.togglePassword = function () {
            $scope.passwordType = ($scope.passwordType === 'password') ? 'text' : 'password';
        }


        $scope.getEyeIcon = function () {
            var icon = ($scope.passwordType === 'password') ? 'icon-theeye-off' : 'icon-theeye';
            return icon;
        }
      
        if ($route.current && $route.current.locals && $route.current.locals.resetModel) {
            $scope.changePasswordData.code = $route.current.locals.resetModel.Code;
            $scope.changePasswordData.user = $route.current.locals.resetModel.User;
            $scope.changePasswordFlag = false;
        }

        $scope.validateNewPassword = function () {
            var re = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*).{6,}$");
            var result = false;
            if ($scope.changePasswordData.newPassword) {
                result = $scope.changePasswordData.newPassword.match(re) ? true : false;
            }
            return result;
        }


        $scope.clearOldPassword = function () {
            $scope.changePasswordData.oldPassword = "";
            $scope.validateOldPassword = false;
        }

        $scope.verifyConfirmationPassword = function () {
            return $scope.changePasswordData.newPassword == $scope.changePasswordData.confirmPassword;
        }


        $scope.verifyPassword = function () {
            $scope.errorMessage = null;
            var data = {
                oldPassword: $scope.changePasswordData.oldPassword
            }
            appService.PostData("Api/Account/ValidatePassword", data).then(function () {
                $scope.validateOldPassword = true;
            }, function(err) {
                $scope.errorMessage = err.error;
                $scope.validateOldPassword = false;
            });
        }

        $scope.validateForm = function () {
            if ($scope.changePasswordFlag == true) {
                return $scope.validateNewPassword() &&
                    $scope.validateOldPassword &&
                    $scope.verifyConfirmationPassword();
            } else {
                return $scope.validateNewPassword() &&
                    $scope.verifyConfirmationPassword();
            }
        };

        $scope.login = function () {
            if (!$scope.loginData.userName || !$scope.loginData.password) {
                $scope.loginData.invalid = true;
            }
            else {
                $scope.loginData.invalid = false;
                authService.login($scope.loginData).then(function () {
                    $location.path('/installations');
                    $scope.passwordType = 'password';   // reset password type
                },
                    function (err) {
                        $scope.message = err.error_description;
                        $scope.loginData.password = "";
                    });
                //clear url params
                $location.search({});
            }
        };

/******* LOGIN DEMO
        
Admin account:
Demo.pool@pentair.com
Password = pentair510

Guest account:
intellicenterdemo@gmail.com
Password = Pentair510

********/

        $scope.loginDemo = function () {
            $scope.loginData.userName = "icdemop@gmail.com";   //"icdemop@gmail.com"; //"intellicenterdemo@gmail.com"; //"aaedgardo@gmail.com";
            $scope.loginData.password = "Pentair510"; //"Password"
                    
            $scope.loginData.invalid = false;
            authService.login($scope.loginData).then(function () {
                $location.path('/installations');
            },
                function (err) {
                    $scope.message = err.error_description;
                    $scope.loginData.password = "";
                });
            //clear url params
            $location.search({});
        
        };

        $scope.ChangePassword = function () {
            if ($scope.changePasswordFlag) {
                var changePassword = {
                    oldPassword: $scope.changePasswordData.oldPassword,
                    newPassword: $scope.changePasswordData.newPassword,
                    confirmPassword: $scope.changePasswordData.confirmPassword
                }
                appService.PostData("Api/Account/ChangePassword", changePassword).then(function () {
                    propertyService.Reset();
                    authService.authentication.passwordChanged = true;
                    authService.authentication.lastUserName = authService.authentication.userName;
                    authService.logOut();
                }, function (err) {
                    $scope.changePasswordData.error = true;
                });
            } else {
                var resetPassword = {
                    email: $scope.changePasswordData.user,
                    password: $scope.changePasswordData.newPassword,
                    confirmPassword: $scope.changePasswordData.confirmPassword,
                    code: $scope.changePasswordData.code
                }
                appService.PostData("Api/Account/ResetPassword", resetPassword).then(function () {
                    propertyService.Reset();
                    authService.authentication.passwordChanged = true;
                    authService.authentication.lastUserName = $scope.changePasswordData.user;
                    authService.logOut();
                }, function (err) {
                    $scope.changePasswordData.error = true;
                }
                );
            }
        };
        $scope.forgotPassword = function () {
            $location.path('/passwordAssistance');
        };

        $scope.GoToLoginPage = function () {
            $location.path('/login');
        };

        $scope.passwordAssistance = function () {
            appService.PostData("Api/Account/ForgotPassword", $scope.forgotPasswordData).then(function () {
                $scope.changePasswordData.requestSubmitted = true;
                $scope.changePasswordData.error = false;
            }).catch(function () {
                $scope.forgotPasswordData.email = '';
                $scope.PasswordAssistanceFrom.$setPristine();
                $scope.changePasswordData.error = true;
            });
        };

        if (authService.authentication.lastUserName) {
            $scope.loginData.userName = authService.authentication.lastUserName;
        }
        propertyModel.Clear();
    }
]);