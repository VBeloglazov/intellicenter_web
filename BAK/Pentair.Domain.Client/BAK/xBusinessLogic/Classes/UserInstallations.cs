﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pentair.Domain.Client.BusinessLogic
{
    /// <summary>
    /// UserInstallations is a DTO class used to provide pool information
    /// The access token is only valid when the DTO is used in a user context.
    /// </summary>
    /// 
    
    public class UserInstallations
    {
        #region Public Properties
        public int InstallationId { get; set; }
        public string PoolName { get; set; }
        public string AccessToken { get; set; }
        public string SecurityName { get; set; }
        public string Address { get; set; }
        public string Location { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string Zip { get; set; }
        public string Phone { get; set; }
        public string OwnerName { get; set; }
        public string Email { get; set; }
        public string TimeZone { get; set; }
        public int TotalAdmins { get; set; }
        public bool OnLine { get; set; }
        public DateTime LastStatusChange { get; set; }

        //this is only here until we setup relationships correctly
        public Guid OwnerId { get; set; }
        #endregion

        public bool Contains(string term)
        {
            string[] values = { PoolName, SecurityName, Location, OwnerName, City, State, Zip, Phone, Address };
            return String.IsNullOrEmpty(term) || values.Any(x => !String.IsNullOrEmpty(x) && x.ToLower().Contains(term.ToLower()));
        }

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public UserInstallations()
        {
            InstallationId = -1;
            AccessToken = "";
            TotalAdmins = 0;
            OnLine = false;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="Id">Installation / Property ID</param>
        /// <param name="name">Property name</param>
        /// <param name="token">User access token</param>
        /// <param name="online">Property on line</param>
        public UserInstallations(int Id, string name, string token, bool online)
        {
            InstallationId = Id;
            PoolName = name;
            AccessToken = token;
            OnLine = online;
            TotalAdmins = 0;
        }
        #endregion
    }
}
