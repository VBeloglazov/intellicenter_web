﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Pentair.Domain.Client.Helpers.Commands
{
    // TODO: 2b removed, not in use.
    /*
    public class SendQueryWithDictionaryResponse : ResponseCommand
    {
        [JsonProperty("answer", Required = Required.Always)]
        public Dictionary<string, string> Answer { get; private set; }

        public SendQueryWithDictionaryResponse(
            string messageID,
            string response,
            Dictionary<string, string> answer
            )
            : base(CommandString.SendQuery, messageID, response)
        {
            this.Answer = answer;

        } // SendQueryWithDictionaryResponse()

        /// <summary>
        /// Always returns an object, valid or not. If invalid, check the DeserializationError property value.
        /// </summary>
        public static new SendQueryWithDictionaryResponse Deserialize(
            string serializedMessage)
        {
            SendQueryWithDictionaryResponse command = null;

            try
            {
                command = JsonConvert.DeserializeObject<SendQueryWithDictionaryResponse>(serializedMessage);
                command.IsValid = true;
            }
            catch (Exception e)
            {
                // Deserialization as SendQueryWithDictionaryResponse failed

                StringBuilder sbError = new StringBuilder();

                sbError.Append("Cannot deserialize as SendQueryWithDictionaryResponse. " + e.Message);

                // Try to get at least the base message data
                ResponseCommand baseCommand = ResponseCommand.Deserialize(serializedMessage);

                if (!baseCommand.IsValid)
                {
                    sbError.Append("\n" + baseCommand.DeserializationError);
                }

                command = new SendQueryWithDictionaryResponse(
                    messageID: baseCommand.MessageID,
                    response: ResponseString.BadRequest,
                    answer: null)
                {
                    IsValid = false,
                    DeserializationError = sbError.ToString()
                };
            }

            return command;

        } // Deserialize()

    } // class SendQueryWithDictionaryResponse
    */
}
