﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Pentair.Domain.Simulator;
using Pentair.Domain.Simulator.Migrations;
using Pentair.Domain.Simulator.Entity;

namespace Pentair.Domain.Simulator.Infrastructure
{
    public class OcpContext : DbContext, IOcpContext
    {
        public OcpContext()
        {
        }
        public IDbSet<Property> Properties { get; set; }
        public IDbSet<Body> Bodies { get; set; }
        public IDbSet<Chemistry> Chemistries { get; set; }
        public IDbSet<Circuit> Circuits { get; set; }
        public IDbSet<Heater> Heaters { get; set; } 
        public IDbSet<MessageLog> MessageLogs { get; set; }
    }
}