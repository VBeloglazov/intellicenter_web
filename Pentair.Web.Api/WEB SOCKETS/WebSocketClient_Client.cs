using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;

using Microsoft.AspNet.Identity.EntityFramework;

using Newtonsoft.Json;

using SERVICE_SHARED;

namespace Pentair.Web.Api
{
    public class WebSocketClient_Client : WebSocketClient
    {
        protected override string EventSourceName { get { return "WebSocketClient_THW"; } }

        #region Construction

        /// <summary>
        /// Constructor used by a Client consuming the Pentair service.  
        /// A valid User ID is required to gain access
        /// </summary>
        /// <param name="id">Property Id being requested</param>
        /// <param name="userId">Id of logged in user</param>
        public WebSocketClient_Client(
            int installationId, 
            Guid userId,
            string user
            )
            : base(
                installationId: installationId,
                userId: userId
            )
        {
            lock (this.lockTimerInactivity)
            {
                if (this.timerInactivity.Enabled)
                {
                    this.timerInactivity.Stop();
                }
            }

            AddConnection();

            lock (this.lockTimerInactivity)
            {
                this.timerInactivity.Interval = CONNECTION_INACTIVITY_TIMEOUT_MSEC_AFTER_ADDCONNECTION;
                this.timerInactivity.Start();
            }

        } // WebSocketClient_Client(int, Guid, string)

        #endregion Construction

        #region WebSocketClient overrides

        public override EndpointType TypeEndpoint
        {
            get { return EndpointType.Client; }

        } // TypeEndpoint

        public override void AddConnection()
        {
            try
            {
                using (DbContextPentair db = DbContextPentair.New())
                {
                    this.Connection = db.AddConnectionClient(
                        installationId: this.InstallationId,
                        userId: this.UserId
                    );

                } // using (DbContextPentair db)

                WebSocketClient.AddClient(this);
            }
            catch (Exception e)
            {
                LogManager.LogErrorAsync(
                    source: this.EventSourceName,
                    eventBriefDescription: "Adding Client connection failed",
                    installationId: this.InstallationId,
                    userId: this.UserId,
                    connectionId: this.ConnectionId,
                    eventFullDescription:
                        ExceptionManager.CustomizeException(e).ErrorMessage(
                            errorHeader: null,
                            addStackTrace: true
                        )
                );

                Disconnect(
                    isHardStop: false,
                    caller: "WebSocketClient_Client.AddConnection()",
                    reason: "Error"
                );
            }

        } // AddConnection()

        public override async Task RemoveConnectionAsync()
        {
            try
            {
                // Remove the connection from the list and the database
                RemoveClient(this);

                // Will clean client's subscriptions
                await RouteClearParamRequestAsync(
                    JsonConvert.SerializeObject(
                        new ClearParam(
                            messageID: Guid.Empty.ToString() // Must be empty to let WebSocketClient_THW to distinguish it from Client's
                        )
                    )
                )
                .ConfigureAwait(false);
            }
            catch (Exception e)
            {
                LogManager.LogErrorAsync(
                    source: this.EventSourceName,
                    eventBriefDescription: "Removing Client connection failed",
                    installationId: this.InstallationId,
                    userId: this.UserId,
                    connectionId: this.ConnectionId,
                    eventFullDescription:
                        ExceptionManager.CustomizeException(e).ErrorMessage(
                            errorHeader: null,
                            addStackTrace: true
                        )
                );
            }

        } // RemoveConnectionAsync()

        /// <exception cref="Exception" />
        protected override async Task ProcessRequestAsync(
            string message)
        {
            if (message == "Close Connection")
            {
                Disconnect(
                    isHardStop: false,
                    caller: "WebSocketClient_Client.ProcessRequest()",
                    reason: "Close Connection received"
                );

                return;

            } // if (message == "Close Connection")

            else if (message == "ping")
            {
                SendMessage("pong", WebSocketActions.SendClient);

                return;

            } // if (message.StartsWith("ping"))
            
            else if (!WebSocketMessage.IsAnAcknowledgementMessage(message))
            {
                MessageType msgType;
                try
                {
                    msgType = WebSocketMessage.ParseMessageType(message, EndpointType.THW);
                }
                catch(Exception e)
                {
                    msgType = new MessageType(EndpointType.THW, CommandType.Invalid, null);

                    LogManager.LogErrorAsync(
                        source: this.EventSourceName,
                        eventBriefDescription: "Request processing failed",
                        installationId: this.InstallationId,
                        userId: this.UserId,
                        connectionId: this.ConnectionId,
                        eventFullDescription:
                            String.Format("WebSocket State: {0}\n\n{1}\n\n{2}",
                                base.WebSocketContext.WebSocket.State,
                                message,
                                ExceptionManager.CustomizeException(e).ErrorMessage(null, true)
                            )
                    );

                    return;
                }

                if (WebApiApplication.LogVerbose && msgType.CommandType == CommandType.Invalid)
                {
                    LogManager.LogWarningAsync(
                        source: this.EventSourceName,
                        eventBriefDescription: "Request processing failed",
                        installationId: this.InstallationId,
                        userId: this.UserId,
                        connectionId: this.ConnectionId,
                        eventFullDescription: String.Format("{0}\n\n{1}", WebSocketActions.ReceiveClientInvalid, message)
                    );
                }

                switch (msgType.CommandType)
                {
                    case CommandType.Ping:
                        SendMessage(
                            "{\"command\":\"pong\", \"messageId\":\"" + msgType.MessageID + "\"",
                            WebSocketActions.SendClient
                        );
                        break;

                    case CommandType.ResendInvite:
                        SendMessage(
                            MessageProcessor.ResendInvite(message, this.InstallationId, this.UserId, this.ConnectionId),
                            WebSocketActions.SendClient
                        );
                        break;

                    case CommandType.InviteUser:
                        SendMessage(
                            MessageProcessor.InviteNewUser(message, this.InstallationId, this.UserId, this.ConnectionId),
                            WebSocketActions.SendClient
                        );
                        break;

                    case CommandType.UserList:
                        SendMessage(
                            MessageProcessor.ListUsers(message, this.InstallationId),
                            WebSocketActions.SendClient
                        );
                        break;

                    case CommandType.DeleteUser:
                        SendMessage(
                            MessageProcessor.DeleteUser(message, this.InstallationId, this.UserId, this.ConnectionId),
                            WebSocketActions.SendClient
                        );
                        break;

                    case CommandType.EditUser:
                        SendMessage(
                            MessageProcessor.EditUser(message, this.InstallationId),
                            WebSocketActions.SendClient
                        );
                        break;

                    case CommandType.SystemInfoList:
                        SendMessage(
                            MessageProcessor.SystemInfoList(message, this.InstallationId),
                            WebSocketActions.SendClient
                        );
                        break;

                    case CommandType.Invalid:
                        SendMessage(
                            new InvalidRequestResponse(message).Serialize(),
                            WebSocketActions.SendClient
                        );
                        break;

                    case CommandType.CreateObject:
                        {
                            CreateObjectFromParam formattedMessage = JsonConvert.DeserializeObject<CreateObjectFromParam>(message);
                            await RouteToThwAsync(formattedMessage).ConfigureAwait(false);
                        }
                        break;

                    case CommandType.SetParamList:
                        {
                            SetParamListResponse formattedMessage = SetParamListResponse.Deserialize(message);
                            await RouteToThwAsync(formattedMessage).ConfigureAwait(false);
                        }
                        break;

                    case CommandType.ReadStatusMessage:
                        {
                            ReadStatusMessage formattedMessage = JsonConvert.DeserializeObject<ReadStatusMessage>(message);
                            await RouteToThwAsync(formattedMessage).ConfigureAwait(false);
                        }
                        break;

                    // Commands that can be served from cache.
                    case CommandType.GetParamList:
                        {
                            GetParamList formattedMessage = JsonConvert.DeserializeObject<GetParamList>(message);
                            await RouteToThwAsync(formattedMessage).ConfigureAwait(false);
                        }
                        break;

                    case CommandType.RequestParamList:
                        {
                            // Register for notifications
                            RequestParamList formattedMessage = JsonConvert.DeserializeObject<RequestParamList>(message);

                            if (formattedMessage.ObjectList != null)
                            {
                                ScheduleNotifications(formattedMessage.ObjectList);
                                await RouteToThwAsync(formattedMessage).ConfigureAwait(false);
                            }
                            else
                            {
                                LogManager.LogErrorAsync(
                                    source: this.EventSourceName,
                                    eventBriefDescription: "Registering for notifications failed",
                                    installationId: this.InstallationId,
                                    userId: this.UserId,
                                    connectionId: this.ConnectionId,
                                    eventFullDescription: String.Format("Object List received null\n\n{0}", message)
                                );
                            }
                        }
                        break;

                    // Commands that are served from cache or API.
                    case CommandType.ClearParam:
                        await RouteClearParamRequestAsync(message).ConfigureAwait(false);
                        break;

                    case CommandType.ReleaseParamList:
                        await RouteReleaseParamListAsync(message).ConfigureAwait(false);
                        break;

                    case CommandType.GetQuery:
                        {
                            GetQuery formattedMessage = JsonConvert.DeserializeObject<GetQuery>(message);
                            await RouteToThwAsync(formattedMessage).ConfigureAwait(false);
                        }
                        break;

                    case CommandType.SetCommand:
                        {
                            SetCommand formattedMessage = JsonConvert.DeserializeObject<SetCommand>(message);
                            await RouteToThwAsync(formattedMessage).ConfigureAwait(false);
                        }
                        break;

                    case CommandType.ReadHistoryMessage:
                        {
                            HistoryReadCommand formattedMessage = JsonConvert.DeserializeObject<HistoryReadCommand>(message);
                            GetHistory(formattedMessage);
                        }
                        break;

                    case CommandType.ReadPostalCodeInfoMessage:
                        {
                            PostalCodeCommand formattedMessage = JsonConvert.DeserializeObject<PostalCodeCommand>(message);
                            GetPostalCodeData(formattedMessage);
                        }
                        break;

                    case CommandType.ReadCityNameInfoMessage:
                        {
                            CityCommand formattedMessage = JsonConvert.DeserializeObject<CityCommand>(message);
                            GetCityData(formattedMessage);
                        }
                        break;

                    default:
                        LogManager.LogErrorAsync(
                            source: this.EventSourceName,
                            eventBriefDescription: "Unknown message type",
                            installationId: this.InstallationId,
                            userId: this.UserId,
                            connectionId: this.ConnectionId,
                            eventFullDescription: String.Format("Received from Client: '{0}'", msgType.CommandType)
                        );
                        
                        // TODO: VB Return error to Client?

                        break;

                } // switch (msgType.Command)

            } // if (!WebSocketMessage.IsAnAcknowledgementMessage(message))

        } // ProcessRequestAsync()

        protected override void RemoveClient(
            WebSocketClient clientToRemove)
        {
            base.RemoveClient(clientToRemove);

        } // RemoveClient()

        #endregion WebSocketClient overrides

        #region Implementation

        private async Task RouteToThwAsync(
            CommandBase formattedMessage)
        {
            if (formattedMessage == null)
            {
                LogManager.LogErrorAsync(
                    source: this.EventSourceName,
                    eventBriefDescription: "Cannot route to THW",
                    installationId: this.InstallationId,
                    userId: this.UserId,
                    connectionId: this.ConnectionId,
                    eventFullDescription: "formattedMessage is null"
                );

                return;
            }

            formattedMessage.AppendMessageId(this.ConnectionId);

            string serializedMessage = JsonConvert.SerializeObject(formattedMessage);

            WebSocketClient_THW thw =
                FindHostByInstallationId(this.InstallationId) as WebSocketClient_THW;

            // THW could disconnect and client remains connected
            if (thw != null)
            {
                // THW connected to this server, communicate directly

                thw.SendMessage(serializedMessage, WebSocketActions.Send);
            }
            else
            {
                // THW might be connected to different server, try ESB

                try
                {
                    Installation installation = null;

                    using (DbContextPentair db = DbContextPentair.New())
                    {
                        installation = db.GetInstallation(this.InstallationId);

                        if (installation == null)
                        {
                            throw new Exception(
                                "Installation not found: " + this.InstallationId
                            );
                        }

                        Connection connection = db.GetConnectionThw(this.InstallationId);

                        if ((connection == null))
                            return;

                        if (connection.HostMachine == WebApiApplication.HOSTNAME)
                        {
                            // It connected to this server, but not found in the Clients list: we have data corrupted!
                            throw new Exception(
                                "Data corrupted: according to the DB, THW connected to this server, but not found in the Clients list"
                            );
                        }

                        // Broadcast to make another server to handle the message
                        // TODO: VB This shouldn't be broadcast, we know the addressee server
                        await BroadcastEsbAsync(
                            destHostMachine: connection.HostMachine,
                            destType: EndpointType.THW,
                            installationId: installation.InstallationId,
                            clientConnectionId: this.ConnectionId,
                            serializedMessage: serializedMessage
                        )
                        .ConfigureAwait(false);

                    } // using (DbContextPentair db)
                }
                catch (Exception e)
                {
                    LogManager.LogErrorAsync(
                        source: this.EventSourceName,
                        eventBriefDescription: "Routing to THW failed",
                        installationId: this.InstallationId,
                        userId: this.UserId,
                        connectionId: this.ConnectionId,
                        eventFullDescription:
                            String.Format("{0}\n\nMessage lost:\n{1}",
                                ExceptionManager.CustomizeException(e).ErrorMessage(
                                    errorHeader: null,
                                    addStackTrace: true
                                ),
                                serializedMessage
                            )
                    );
                }
            }

        } // RouteToThwAsync()

        private void ScheduleNotifications(
            List<KeyObject> objList)
        {
            using (DbContextPentair db = DbContextPentair.New())
            {
                db.ScheduleNotifications(
                    clientId: this.ConnectionId,
                    propertyId: this.InstallationId,
                    objList: objList
                );

            } // using (DbContextPentair db)

        } // ScheduleNotifications()

        private async Task RouteClearParamRequestAsync(
            string message)
        {
            ClearParam clearParam = JsonConvert.DeserializeObject<ClearParam>(message);

            // TODO: Combine the rest with RouteReleaseParamListAsync()

            List<KeyObject> lstCancellations;

            using (DbContextPentair db = DbContextPentair.New())
            {
                lstCancellations = db.ClearParam(
                    clientId: this.ConnectionId,
                    propertyId: this.InstallationId
                );

            } // using (DbContextPentair db)

            // Notify the THW

            if (lstCancellations.Any())
            {
                ReleaseParamList releaseParamList = new ReleaseParamList(
                    messageID: clearParam.MessageID,
                    condition: String.Empty,
                    objectList: lstCancellations
                );

                releaseParamList.AppendMessageId(this.ConnectionId);

                await RouteToThwAsync(releaseParamList).ConfigureAwait(false);
            }
            else
            {
                ClearParamResponse clearParamResponse = new ClearParamResponse(
                    messageID: clearParam.MessageID,
                    response: ResponseString.OK
                );

                SendMessage(
                    JsonConvert.SerializeObject(clearParamResponse),
                    WebSocketActions.SendClient
                );
            }

        } // RouteClearParamRequestAsync()

        private async Task RouteReleaseParamListAsync(
            string message)
        {
            #region TODO: 2b deleted after testing
            /*
            // Populate Cancellations
            // Notify the THW

            //
            // 2018-01-25 VB: What if there are no more subscriptions just for SOME of the parameters?
            //                Pathfinder still passes the entire list to the THW!
            //
            // If there are no more subscriptions for these parameters, pass the cancellation on to the THW.
            if (1 > releaseParamList.ObjectList.SelectMany(l => l.Keys, (l, o) =>
                (from t in db.Subscriptions
                 where t.ClientId == this.ConnectionId && t.ObjNam == l.ObjNam && t.ObjParm == o
                 select t).FirstOrDefault())
                .Where(i => null != i).Count())
            {
                await RouteToThwAsync(releaseParamList).ConfigureAwait(false);
            }

            // Clean [Subscriptions]

            IEnumerable<Subscription> subscriptions = releaseParamList.ObjectList.SelectMany(
                keyObject => keyObject.Keys,
                (keyObject, o) =>
                    (from t in db.Subscriptions
                     where t.ClientId == this.ConnectionId && t.ObjNam == keyObject.ObjNam && t.ObjParm == o
                     select t
                     ).FirstOrDefault()
                 ).Where(i => i != null)
            ;

            foreach (Subscription subscription in subscriptions)
            {
                db.Subscriptions.Remove(subscription);
            }
            db.SaveChanges();
            */
            #endregion TODO: 2b deleted after testing

            ReleaseParamList releaseParamListClient = JsonConvert.DeserializeObject<ReleaseParamList>(message);

            // TODO: Combine the rest with RouteClearParamRequestAsync()

            List<KeyObject> lstCancellations;

            using (DbContextPentair db = DbContextPentair.New())
            {
                lstCancellations = db.ReleaseParamList(
                    clientId: this.ConnectionId,
                    propertyId: this.InstallationId,
                    objList: releaseParamListClient.ObjectList
                );

            } // using (DbContextPentair db)

            // Notify the THW

            if (lstCancellations.Any())
            {
                ReleaseParamList releaseParamListThw = new ReleaseParamList(
                    messageID: releaseParamListClient.MessageID,
                    condition: String.Empty,
                    objectList: lstCancellations
                );

                releaseParamListThw.AppendMessageId(this.ConnectionId);

                await RouteToThwAsync(releaseParamListThw).ConfigureAwait(false);
            }
            else
            {
                ReleaseParamListResponse releaseParamListResponse = new ReleaseParamListResponse(
                    messageID: releaseParamListClient.MessageID,
                    response: ResponseString.OK
                );

                SendMessage(
                    JsonConvert.SerializeObject(releaseParamListClient),
                    WebSocketActions.SendClient
                );
            }

        } // RouteReleaseParamListAsync()

        private void GetHistory(
            HistoryReadCommand message)
        {
            try
            {
                long startTime = Convert.ToInt64(message.TimeStart);
                long endTime = Convert.ToInt64(message.TimeEnd);

                using (DbContextPentair db = DbContextPentair.New())
                {
                    // TODO: 2b replaced with SP & tested
                    IEnumerable<ControlObjectsHistory> historyDB = db.GetHistoryRecord(this.InstallationId, startTime, endTime);

                    // Send the data having not more than 25 history records per message
                    int messageNo = 1;
                    int recordsToSend = historyDB.Count();
                    int blocksize = 25;         // by default 
                    if (recordsToSend > 100)    // if count is > 100 then add blocks of 100 messages
                        blocksize = 100;
                    if (recordsToSend > 250)    // if count is > 250 then add blocks of 250 messages
                        blocksize = 250;
                    if (recordsToSend > 500)    // if count is > 500 then add blocks of 500 messages
                        blocksize = 500;
                    if (recordsToSend > 750)    // if count is > 750 then add blocks of 750 messages
                        blocksize = 750;
                    if (recordsToSend > 1000)    // if count is > 1000 then add blocks of 1000 messages
                        blocksize = 1000;
                    if (recordsToSend > 1250)    // if count is > 1250 then add blocks of 1250 messages
                        blocksize = 1250;
                    if (recordsToSend > 1500)    // if count is > 1500 then add blocks of 1500 messages
                        blocksize = 1500;

                    int totalSent = 0;
                    int messageRecordCount = 0;
                    string objName = String.Empty;
                    var objectList = new List<Dictionary<string, List<HistoryItem>>>();

                    foreach (ControlObjectsHistory controlObject in historyDB)
                    {
                        if (controlObject.ObjNam != objName)
                        {
                            objName = controlObject.ObjNam;
                        }

                        Dictionary<string, List<HistoryItem>> objListItem =
                            objectList.FirstOrDefault(o => o.ContainsKey(objName));

                        if (objListItem == null)
                        {
                            objListItem = new Dictionary<string, List<HistoryItem>>();
                            objListItem[objName] = new List<HistoryItem>();
                            objectList.Add(objListItem);
                        }

                        objListItem[objName].Add(
                            new HistoryItem(
                                P: controlObject.Key,
                                V: controlObject.Value,
                                T: controlObject.Revision.ToString()
                            )
                        );

                        messageRecordCount++;
                        // Changed from 25 to 250 records TCP/IP fragmentation will handle messages more than 1500 maximum payload, than normally TCP/IP can tcp can carry.
                        //if (messageRecordCount >= 25 || totalSent + messageRecordCount >= recordsToSend)
                        if ((messageRecordCount >= blocksize) || 
                            ((totalSent + messageRecordCount) >= recordsToSend) 
                            )
                        {
                            if (objectList.Count > 0)
                            {
                                HistoryWriteCommandResponse msg = new HistoryWriteCommandResponse(
                                    messageID: message.MessageID,
                                    response: ResponseString.OK,
                                    messageNo: messageNo++,
                                    timeNow: message.TimeEnd,
                                    timeSince: message.TimeStart,
                                    objectList: objectList
                                );

                                SendMessage(
                                    JsonConvert.SerializeObject(msg), WebSocketActions.SendClient
                                );

                                totalSent += messageRecordCount;

                                // Reset
                                objectList.Clear();
                                messageRecordCount = 0;
                            }
                        }

                    } // foreach(ControlObject controlObject)

                } // using (DbContextPentair db)
            }
            catch (Exception e)
            {
                LogManager.LogErrorAsync(
                    source: this.EventSourceName,
                    eventBriefDescription: "GetHistory failed",
                    installationId: this.InstallationId,
                    userId: this.UserId,
                    connectionId: this.ConnectionId,
                    eventFullDescription:
                        ExceptionManager.CustomizeException(e).ErrorMessage(
                            errorHeader: null,
                            addStackTrace: true
                        )
                );
            }

        } // GetHistory()

        private void GetPostalCodeData(
            PostalCodeCommand message)
        {
            try
            {
                string postalCode = message.PostalCode;
                
                using (DbContextPentair db = DbContextPentair.New())
                {
                    IEnumerable<PostalCodeInfo> postalCodeInfoDB = db.GetPostalCodeInfo(postalCode);

                    var objectList = new List<PostalCodeInfoItem>();

                    foreach (PostalCodeInfo postalCodeInfo in postalCodeInfoDB)
                    {
                        objectList.Add(
                            new PostalCodeInfoItem(
                                country: postalCodeInfo.CountryName,
                                city: postalCodeInfo.CityName,
                                state: postalCodeInfo.ProvinceAbbr,
                                timeZone: postalCodeInfo.TimeZone,
                                utc: postalCodeInfo.UTC,
                                dst: postalCodeInfo.DST,
                                latitude: postalCodeInfo.Latitude,
                                longitude: postalCodeInfo.Longitude,
                                postalCode: postalCodeInfo.PostalCode
                            )
                        );
                    } // foreach(ControlObject controlObject)

                    if (objectList.Count > 0)
                    {
                        PostalCodeInfoResponse msg = new PostalCodeInfoResponse(
                            messageID: message.MessageID,
                            response: ResponseString.OK,
                            objectList: objectList
                        );

                        SendMessage(JsonConvert.SerializeObject(msg), WebSocketActions.SendClient);

                        objectList.Clear();
                    }   // if (objectList.Count > 0)

                } // using (DbContextPentair db)
            }
            catch (Exception e)
            {
                LogManager.LogErrorAsync(
                    source: this.EventSourceName,
                    eventBriefDescription: "GetPostalCodeInfo failed",
                    installationId: this.InstallationId,
                    userId: this.UserId,
                    connectionId: this.ConnectionId,
                    eventFullDescription:
                        ExceptionManager.CustomizeException(e).ErrorMessage(
                            errorHeader: null,
                            addStackTrace: true
                        )
                );
            }

        } // GetPostalCodeInfo()

        private void GetCityData(
            CityCommand message)
        {
            try
            {
                string city = message.City;
                string state = message.State;         

                using (DbContextPentair db = DbContextPentair.New())
                {
                    IEnumerable<PostalCodeInfo> cityInfoDB = db.GetCityInfo(city, state);

                    var objectList = new List<PostalCodeInfoItem>();

                    foreach (PostalCodeInfo postalCodeInfo in cityInfoDB)
                    {
                        objectList.Add(
                            new PostalCodeInfoItem(
                                country: postalCodeInfo.CountryName,
                                city: postalCodeInfo.CityName,
                                state: postalCodeInfo.ProvinceAbbr,
                                timeZone: postalCodeInfo.TimeZone,
                                utc: postalCodeInfo.UTC,
                                dst: postalCodeInfo.DST,
                                latitude: postalCodeInfo.Latitude,
                                longitude: postalCodeInfo.Longitude,
                                postalCode: postalCodeInfo.PostalCode
                            )
                        );
                    } // foreach(ControlObject controlObject)

                    if (objectList.Count > 0)
                    {
                        PostalCodeInfoResponse msg = new PostalCodeInfoResponse(
                            messageID: message.MessageID,
                            response: ResponseString.OK,
                            objectList: objectList
                        );

                        SendMessage(JsonConvert.SerializeObject(msg), WebSocketActions.SendClient);

                        objectList.Clear();
                    }   // if (objectList.Count > 0)

                } // using (DbContextPentair db)
            }
            catch (Exception e)
            {
                LogManager.LogErrorAsync(
                    source: this.EventSourceName,
                    eventBriefDescription: "GetCityInfo failed",
                    installationId: this.InstallationId,
                    userId: this.UserId,
                    connectionId: this.ConnectionId,
                    eventFullDescription:
                        ExceptionManager.CustomizeException(e).ErrorMessage(
                            errorHeader: null,
                            addStackTrace: true
                        )
                );
            }

        } // GetCityInfo()

        #endregion Implementation

    } // class WebSocketClient_Client
}