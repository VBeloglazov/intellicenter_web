﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace Pentair.Desktop.Simulator.Models
{
    interface IDataModel
    {
        /// <summary>
        /// Collection of data object entries
        /// </summary>
        DataTable Data { get; }

        /// <summary>
        /// Rebuild the data collection
        /// </summary>
        /// <param name="data">new data</param>
        void SwapData(IEnumerable<object> data);
    }
}
