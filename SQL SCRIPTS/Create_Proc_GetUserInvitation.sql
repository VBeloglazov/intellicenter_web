-- =============================================================
--
-- Author:		Vladimir Beloglazov
-- Create date: 03/07/2018
--
-- Creates:
--
--		Table Type:			None
--
--		Stored Procedure:	PGetUserInvitation
--
-- 2018-04-20 VB: INNER JOIN -> LEFT JOIN (Invitation's InstallationId can be NULL).
--
-- =============================================================

USE PathfinderIntelliCenter
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (
	SELECT * FROM sys.objects
	WHERE object_id = OBJECT_ID(N'dbo.PGetUserInvitation')
	AND type in (N'P', N'PC')
)
DROP PROCEDURE dbo.PGetUserInvitation
GO

-- -------------------------------------------------------------
--
-- Returns [UserInvitations] record field values.
--
-- -------------------------------------------------------------
CREATE PROCEDURE dbo.PGetUserInvitation
	 @invitationId AS uniqueidentifier
AS
BEGIN

	SET NOCOUNT OFF

	-- Result set
	SELECT
		 UI.InvitationId	AS InvitationId
		,UI.EmailAddress	AS EmailAddress
		,UI.InstallationId	AS InstallationId
		,I.Name				AS InstallationName
		,UI.AccessToken		AS AccessToken
		,UI.[Status]		AS [Status]
		,UI.EmailId			AS EmailId
		,UI.CreateDateTime	AS CreateDateTime
		,UI.UpdateDateTime	AS UpdateDateTime
	FROM dbo.UserInvitations UI
	LEFT JOIN dbo.Installations I ON I.InstallationId = UI.InstallationId
	WHERE UI.InvitationId = @invitationId

END
GO
