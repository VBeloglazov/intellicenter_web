USE [PathfinderIntellicenter]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (
	SELECT * FROM sys.objects
	WHERE object_id = OBJECT_ID(N'dbo.pr_UpdateUserInvitationsStatus')
	AND type in (N'P', N'PC')
)
DROP PROCEDURE dbo.pr_UpdateUserInvitationsStatus
GO


-- =============================================
-- Author			:	Wayne Hui
-- Create date		:	02/14/2019
-- Parameters		:
--  @InstallationId	:	Device ID
--  @InviteExpirationHours	:	Hours before invitation expires
-- Description		:	Update the user invite status
--
-- Modified By			Date		Description
-- =============================================
CREATE PROCEDURE [dbo].[pr_UpdateUserInvitationsStatus]
	@InstallationId				INT
	, @InviteExpirationHours	INT
AS
BEGIN

	SET NOCOUNT OFF

----TEST
--DECLARE 
--	@InstallationId				INT
--	, @InviteExpirationHours	INT

--SET @InstallationId			= 3948
--SET @InviteExpirationHours	= 72

----END TEST


	UPDATE UserInvitations 
	SET [Status] = 'UNKNOWN'
	WHERE 
		InstallationId = @InstallationId
		AND [Status] IS NULL


	UPDATE UserInvitations 
	SET [Status] = 'EXPIRED'
	WHERE 
		InstallationId = @InstallationId
		AND	[Status] <> 'ACCEPTED'
		AND DATEDIFF(hour, CreateDateTime, GETDATE()) > @InviteExpirationHours

END

GO


--GRANT EXECUTE ON [dbo].[pr_UpdateUserInvitationsStatus] TO PUBLIC
--GO
