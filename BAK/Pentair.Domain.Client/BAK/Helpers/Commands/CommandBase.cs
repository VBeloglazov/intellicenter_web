﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Pentair.Domain.Client.Helpers.Commands
{
    public class CommandBase
    {
        [JsonProperty("command", Required = Required.Always, Order = 1)]
        public string Command { get; private set; }

        [JsonProperty("messageID", Required = Required.Always, Order = 2)]
        public string MessageID { get; protected set; }

        [JsonIgnore]
        public bool IsValid { get; set; }

        [JsonIgnore]
        public string DeserializationError { get; set; }

        public CommandBase(
            string command,
            string messageID)
        {
            this.Command = command;
            this.MessageID = messageID;

        } // CommandBase()

        public void AppendMessageId(
            Guid connectionId)
        {
            this.MessageID += ":" + connectionId;

        } // AppendMessageId()

        public virtual string Serialize()
        {
            return JsonConvert.SerializeObject(this);

        } // Serialize()

        /// <summary>
        /// Always returns an object, valid or not. If invalid, check the DeserializationError property value.
        /// </summary>
        public static CommandBase Deserialize(
            string serializedMessage)
        {
            CommandBase command = null;

            try
            {
                command = JsonConvert.DeserializeObject<CommandBase>(serializedMessage);
                command.IsValid = true;
            }
            catch (Exception e)
            {
                // Deserialization as CommandBase failed

                command = new CommandBase(
                    command: String.Empty,
                    messageID: Guid.Empty.ToString())
                {
                    IsValid = false,
                    DeserializationError = "Cannot deserialize as CommandBase. " + e.Message
                };
            }

            return command;

        } // Deserialize()

    } // class CommandBase
}