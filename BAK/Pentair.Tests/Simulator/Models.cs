﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Machine.Specifications;
using Newtonsoft.Json;
using Pentair.Desktop.Simulator.Models;
using Pentair.Domain.Client.Entity;
using Pentair.Domain.Client.Helpers;
using Pentair.Domain.Client.Infrastructure;

namespace Pentair.Tests.Simulator
{
    public class When_I_Ask_For_All_Models
    {
        #region Database Records

        private static readonly ISimulatorContext SimulatorFakeContext = new FakeContext()
        {
            ControlObjects =
            {
                new ControlObject()
                {
                    INSTALLATION = 1,
                    OBJNAM = "C1355",
                    Key = StaticData.ObjectType,
                    Value = StaticData.BodyType,
                },
                new ControlObject()
                {
                    INSTALLATION = 1,
                    OBJNAM = "C1355",
                    Key = StaticData.SubType,
                    Value = "POOL",
                },
                new ControlObject()
                {
                    INSTALLATION = 1,
                    OBJNAM = "C1355",
                    Key = StaticData.SystemName,
                    Value = "Pool Circuit",
                },
                new ControlObject()
                {
                    INSTALLATION = 1,
                    OBJNAM = "C1355",
                    Key = StaticData.Status,
                    Value = StaticData.On,
                },
                new ControlObject()
                {
                    INSTALLATION = 1,
                    OBJNAM = "C1356",
                    Key = StaticData.ObjectType,
                    Value = StaticData.BodyType,
                },
                new ControlObject()
                {
                    INSTALLATION = 1,
                    OBJNAM = "C1356",
                    Key = StaticData.SubType,
                    Value = "SPA",
                },
                new ControlObject()
                {
                    INSTALLATION = 1,
                    OBJNAM = "C1356",
                    Key = StaticData.SystemName,
                    Value = "Spa Circuit",
                },
                new ControlObject()
                {
                    INSTALLATION = 1,
                    OBJNAM = "C1356",
                    Key = StaticData.Status,
                    Value = StaticData.On,
                },
                new ControlObject()
                {
                    INSTALLATION = 1,
                    OBJNAM = "C1356",
                    Key = StaticData.ObjectType,
                    Value = StaticData.BodyType,
                },
                new ControlObject()
                {
                    INSTALLATION = 1,
                    OBJNAM = "C5433",
                    Key = StaticData.ObjectType,
                    Value = StaticData.BodyType,
                },
                new ControlObject()
                {
                    INSTALLATION = 1,
                    OBJNAM = "C5433",
                    Key = StaticData.ObjectType,
                    Value = StaticData.CircuitType,
                },
                new ControlObject()
                {
                    INSTALLATION = 1,
                    OBJNAM = "C5433",
                    Key = StaticData.SubType,
                    Value = StaticData.IntellibrightSubtype,
                },
                new ControlObject()
                {
                    INSTALLATION = 1,
                    OBJNAM = "C5433",
                    Key = StaticData.SystemName,
                    Value = "Night Spa",
                },
                new ControlObject()
                {
                    INSTALLATION = 1,
                    OBJNAM = "C5435",
                    Key = StaticData.ObjectType,
                    Value = StaticData.CircuitType,
                },
                new ControlObject()
                {
                    INSTALLATION = 1,
                    OBJNAM = "C5435",
                    Key = StaticData.SubType,
                    Value = "MAGIC2",
                },
                new ControlObject()
                {
                    INSTALLATION = 1,
                    OBJNAM = "C5435",
                    Key = StaticData.SystemName,
                    Value = "Night Laminar",
                },
                new ControlObject()
                {
                    INSTALLATION = 1,
                    OBJNAM = "C5436",
                    Key = StaticData.ObjectType,
                    Value = StaticData.CircuitType,
                },
                new ControlObject()
                {
                    INSTALLATION = 1,
                    OBJNAM = "C5436",
                    Key = StaticData.SubType,
                    Value = StaticData.GenericSubtype,
                },
                new ControlObject()
                {
                    INSTALLATION = 1,
                    OBJNAM = "C5436",
                    Key = StaticData.SystemName,
                    Value = "Water Fall",
                },
                new ControlObject()
                {
                    INSTALLATION = 1,
                    OBJNAM = "C5437",
                    Key = StaticData.ObjectType,
                    Value = StaticData.CircuitType,
                },
                new ControlObject()
                {
                    INSTALLATION = 1,
                    OBJNAM = "C5437",
                    Key = StaticData.SubType,
                    Value = StaticData.LightSubtype,
                },
                new ControlObject()
                {
                    INSTALLATION = 1,
                    OBJNAM = "C5437",
                    Key = StaticData.SystemName,
                    Value = "Light",
                }
            }
        };
        #endregion

    #region Tests

        //public class WhenTheSimulatorAsksForConfigurationDataToDisplay
        //{
        //    private static DataModel _subject;
        //    private const string Config = "Config";

        //    private Establish context = () =>
        //    {
        //        _subject = new DataModel((from item in SimulatorFakeContext.ControlObjects select item).ToList());
        //    };

        //    private Because of = () => _subject.SwapData((from item in SimulatorFakeContext.ControlObjects select item).ToList());

        //    private It should_be_all_body_objects = () => _subject.Data.Rows.Count.ShouldEqual(6);
        //}
    #endregion
    }
}
