-- =============================================================
--
-- Author:		Vladimir Beloglazov
-- Create date: 03/02/2018
--
-- Creates:
--
--		Table Type:			None
--
--		Stored Procedure:	PChangeUserAccess
--
-- =============================================================

USE PathfinderIntelliCenter
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (
	SELECT * FROM sys.objects
	WHERE object_id = OBJECT_ID(N'dbo.PChangeUserAccess')
	AND type in (N'P', N'PC')
)
DROP PROCEDURE dbo.PChangeUserAccess
GO

-- -------------------------------------------------------------
--
-- Returns int matching one of the UserManagementResult values.
--
-- -------------------------------------------------------------
CREATE PROCEDURE dbo.PChangeUserAccess
	 @propertyId		AS int
	,@userId			AS uniqueidentifier
	,@userEmail			AS nvarchar(max)
	,@tokenNew			AS nvarchar(16)
	,@tokenSuperAdmin	AS nvarchar(16)
	,@tokenAdmin		AS nvarchar(16)

AS
BEGIN

	SET NOCOUNT OFF

	DECLARE @userManagementResult AS int
	SET @userManagementResult = 6 -- UnknownError

	BEGIN TRANSACTION

	BEGIN TRY

		DECLARE @demotingSingleAdmin bit
		SET @demotingSingleAdmin = 0

		SELECT @demotingSingleAdmin = 1
		FROM dbo.Associations
        WHERE InstallationId = @propertyId AND UserId = @userId -- PK
        AND (AccessToken = @tokenSuperAdmin OR AccessToken = @tokenAdmin)
		AND (@tokenNew <> @tokenSuperAdmin AND @tokenNew <> @tokenAdmin)
		AND
		(
			SELECT COUNT(*)
			FROM dbo.Associations
			WHERE InstallationId = @propertyId
			AND (AccessToken = @tokenSuperAdmin OR AccessToken = @tokenAdmin)
		) = 1

		IF @demotingSingleAdmin = 1
			BEGIN
				SET @userManagementResult = 1 -- LastAdminError
			END
		ELSE
			BEGIN
				UPDATE dbo.Associations
				SET  AccessToken = @tokenNew
				WHERE InstallationId = @propertyId AND UserId = @userId -- PK

				UPDATE dbo.UserInvitations
				SET AccessToken = @tokenNew
				WHERE InstallationId = @propertyId
				AND EmailAddress = @userEmail

				SET @userManagementResult = 5 -- Success
			END

		-- Result set
		SELECT @userManagementResult

	END TRY
	BEGIN CATCH

		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION

		DECLARE @err_message nvarchar(255)
		SET @err_message = ERROR_MESSAGE()
		RAISERROR (@err_message, 11, 1) -- severity 11-19 will cause returning exception to caller

	END CATCH

	IF @@TRANCOUNT > 0
		COMMIT TRANSACTION

END
GO
