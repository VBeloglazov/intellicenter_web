﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

using Pentair.Domain.Client.Entity;
using Pentair.Domain.Client.Helpers;
using Pentair.Domain.Client.Helpers.Commands;
using Pentair.Domain.Client.Helpers.Entity;
using Pentair.Domain.Client.Infrastructure;
using System.Data.SqlClient;

namespace Pentair.Domain.Client.DataLayer
{
    /// <summary>
    /// ObjectManager manages the configuration cache for installations
    /// </summary>
    public class ObjectManager
    {
        #region Private Properties

        private readonly ApiContext data;
        private readonly int property;

        #endregion Private Properties

        #region Constructor

        public ObjectManager(
            ApiContext data,
            int property)
        {
            this.data = data;
            this.property = property;
        
        } // ObjectManager()

        #endregion Constructor

        #region Public Methods

        /// <summary>
        /// ParamValue returns the value of the parameter
        /// whose parameter name and object name are passed in.
        /// </summary>
        /// <param name="objnam">Object name</param>
        /// <param name="key">Parameter name</param>
        /// <returns></returns>
        public string ParamValue(
            string objnam,
            string key)
        {
            var o = this.data.ControlObjects.FirstOrDefault(x =>
                x.OBJNAM == objnam &&
                x.Key == key &&
                x.INSTALLATION == this.property
            );

            return (o == null) ? null : o.Value;

        } // ParamValue()

        public void DeleteObject(
            string objnam)
        {
            int sqlResult = 0;
            string sql = @"Delete From dbo.ControlObjects where INSTALLATION = @INSTALLATION  And OBJNAM = @OBJNAM";
            sqlResult = this.data.Database.ExecuteSqlCommand(sql, new SqlParameter("@INSTALLATION", this.property), new SqlParameter("@OBJNAM", objnam));

            //KD
            // It is actually list of Keys for the INSTALLATION/OBJNAM
            //IQueryable<ControlObject> objRecords = this.data.ControlObjects.Where(x =>
            //    x.INSTALLATION == this.property &&
            //    x.OBJNAM == objnam
            //);

            //foreach (ControlObject o in objRecords)
            //{
            //    this.data.ControlObjects.Remove(o);
            //}
            
            //this.data.SaveChanges();

        } // DeleteObject()

        /// <summary>
        /// Updates existing or creates new object.
        /// </summary>
        public void UpdateObjects(
            ParamObject pObject,
            long revision)
        {
            foreach (KeyValuePair<string, string> p in pObject.Parameters)
            {
                UpdateObjectParameter(
                    objName: pObject.ObjNam,
                    key: p.Key,
                    value: p.Value,
                    revision: revision,
                    type: ParameterType.Saved
                );
            }
        
        } // UpdateObjects()

        /// <summary>
        /// Doesn't call SaveChanges()!
        /// </summary>
        public void UpdateHistory(
            List<Dictionary<string, List<HistoryItem>>> historyObjectList)
        {
            foreach (Dictionary<string, List<HistoryItem>> dic in historyObjectList)
            {
                foreach (KeyValuePair<string, List<HistoryItem>> historyObject in dic)
                {
                    string objName = historyObject.Key;
                    List<HistoryItem> objHistoryItems = historyObject.Value;

                    // 2017-10-26 VB: CPU usage here was high. Task.Delay() added to make it better.
                    //
                    foreach (HistoryItem historyItem in objHistoryItems)
                    {
                        UpdateObjectParameter(
                            objName: objName,
                            key: historyItem.Name,
                            value: historyItem.Value,
                            revision: Convert.ToInt64(historyItem.Timestamp),
                            type: ParameterType.History
                        );

                        Task.Delay(1).Wait();

                    } // foreach (HistoryItem)

                } // foreach (KeyValuePair)

            } // foreach (Dictionary)
        
        } // UpdateHistory()

        /// <summary>
        /// Doesn't call SaveChanges()!
        /// </summary>
        public void ClearHistory(string timeStart, string timeEnd)
        {
            int sqlResult = 0;
            string sql = @"Delete From dbo.ControlObjects where INSTALLATION = @INSTALLATION And ParamType = @ParamType";
            sqlResult = this.data.Database.ExecuteSqlCommand(sql, new SqlParameter("@INSTALLATION", this.property), new SqlParameter("@ParamType", "History"));

            //KD
            //var objectDb = this.data.ControlObjects.Where(x => x.INSTALLATION == this.property && x.ParamType == "History");

            //if (objectDb != null)
            //{
            //    foreach (var item in objectDb)
            //    {
            //        this.data.ControlObjects.Remove(item);
            //    }
            //}

        } // ClearHistory()
        #endregion Public Methods

        #region Private Methods

        private void UpdateObjectParameter(
            string objName,
            string key,
            string value,
            long revision,
            ParameterType type)
        {
            ControlObject objectDb = this.data.ControlObjects.FirstOrDefault(x =>
                x.INSTALLATION == this.property &&
                x.Revision == revision &&
                x.OBJNAM == objName &&
                x.Key == key
            );

            if (objectDb != null)
            {
                objectDb.Value = value;
                objectDb.Revision = revision;
                objectDb.Deleted = false;
            }
            else
            {

                var cObject = this.data.ControlObjects.Local.FirstOrDefault(a => a.INSTALLATION == this.property &&
                                                                               a.OBJNAM == objName &&
                                                                               a.Key == key &&
                                                                               a.Revision == revision);

                if (cObject == null)
                {
                    this.data.ControlObjects.Add(
                        new ControlObject()
                        {
                            INSTALLATION = this.property,
                            OBJNAM = objName,
                            Key = key,
                            Revision = revision,
                            Value = value,
                            ParamType = type.ToString()
                        }
                    );
                }
                else
                {
                    cObject.Value = value;
                    cObject.Revision = revision;
                    cObject.Deleted = false;
                }
            }

        } // UpdateObjectParameter()

        #endregion Private Methods

        #region Direct Sql Command Methods
        static public int ClearHistoryByInstallationAndParamType(ApiContext db,
        int installation,
        ParameterType paramType)
        {
            int sqlResult = 0;
            string sql = @"Delete From dbo.ControlObjects where INSTALLATION = @INSTALLATION And ParamType = @ParamType";
            sqlResult = db.Database.ExecuteSqlCommand(sql, new SqlParameter("@INSTALLATION", installation), new SqlParameter("@ParamType", paramType.ToString()));
            return sqlResult;
        } // ClearHistoryByInstallationAndParamType()

        static public int RemoveSubscriptions(ApiContext db,
        Guid clientID)
        {
            int sqlResult = 0;
            string sql = @"Delete From dbo.Subscriptions where ClientId = @ClientId";
            sqlResult = db.Database.ExecuteSqlCommand(sql, new SqlParameter("@ClientId", clientID));
            return sqlResult;
        } // RemoveSubscriptions()

        static public int RemoveManySubscriptions(ApiContext db,
        Guid clientID,
        string objNam,
        string objParm)
        {
            int sqlResult = 0;
            string sql = @"Delete From dbo.Subscriptions where ClientId = @ClientId And OBJNAM = @OBJNAM And ObjParm = @ObjParm";
            sqlResult = db.Database.ExecuteSqlCommand(sql, new SqlParameter("@ClientId", clientID.ToString()), new SqlParameter("@OBJNAM", objNam), new SqlParameter("@ObjParm", objParm));
            return sqlResult;
        }// RemoveManySubscriptions()

        static public int RemoveConnectionsByConnectionId(ApiContext db,
        Guid connectionId)
        {
            int sqlResult = 0;
            string sql = @"Delete From dbo.Connections where ConnectionId = @ConnectionId";
            sqlResult = db.Database.ExecuteSqlCommand(sql, new SqlParameter("@ConnectionId", connectionId.ToString()));
            return sqlResult;
        } // RemoveConnectionsByConnectionId()

        static public int RemoveConnectionsByHostMachine(ApiContext db,
        string hostMachine)
        {
            int sqlResult = 0;
            string sql = @"Delete From dbo.Connections where HostMachine = @HostMachine";
            sqlResult = db.Database.ExecuteSqlCommand(sql, new SqlParameter("@HostMachine", hostMachine));
            return sqlResult;
        }// RemoveConnectionsByHostMachine()

        static public int RemoveAssociations(ApiContext db,
         int installationId,
         Guid userId)
        {
            int sqlResult = 0;
            string sql = @"Delete From dbo.Associations where InstallationId = @InstallationId And UserId = @UserId";
            sqlResult = db.Database.ExecuteSqlCommand(sql, new SqlParameter("@InstallationId", installationId), new SqlParameter("@UserId", userId.ToString()));
            return sqlResult;
        } // RemoveAssociations()

        static public int RemoveUserInvitations(ApiContext db,
        int installationId,
        string emailAddress)
        {
            int sqlResult = 0;
            string sql = @"Delete From dbo.UserInvitations where InstallationId = @InstallationId And EmailAddress = @EmailAddress";
            sqlResult = db.Database.ExecuteSqlCommand(sql, new SqlParameter("@InstallationId", installationId), new SqlParameter("@EmailAddress", emailAddress.ToString()));
            return sqlResult;
        } // RemoveUserInvitations()


        static public int RemoveNotificationRecipients(ApiContext db,
        int recipientId,
        int installationId)
        {
            int sqlResult = 0;
            string sql = @"Delete From dbo.NotificationRecipients where RecipientId = @RecipientId And InstallationId = @InstallationId";
            sqlResult = db.Database.ExecuteSqlCommand(sql, new SqlParameter("@RecipientId", recipientId), new SqlParameter("@InstallationId", installationId));
            return sqlResult;
        } // RemoveNotificationRecipients()

        static public int RemoveUserNotifications(IApiContext db,
        int eventId,
        int installationId,
        Guid userId)
        {
            int sqlResult = 0;
            string sql = @"Delete From dbo.UserNotifications where EventId = @EventId And InstallationId = @InstallationId And UserId = @UserId";
            sqlResult = db.Database.ExecuteSqlCommand(sql, new SqlParameter("@EventId", eventId), new SqlParameter("@InstallationId", installationId), new SqlParameter("@UserId", userId.ToString()));
            return sqlResult;
        } // RemoveUserNotifications()

        static public int DeleteControlObjectByInstallationAndParamType(ApiContext db,
        int installation,
        ParameterType paramType)
        {
            int sqlResult = 0;
            string sql = @"Delete From dbo.ControlObjects where INSTALLATION = @INSTALLATION And ParamType = @ParamType";
            sqlResult = db.Database.ExecuteSqlCommand(sql, new SqlParameter("@INSTALLATION", installation), new SqlParameter("@ParamType", paramType.ToString()));
            return sqlResult;
        } // DeleteControlObjectByInstallationAndParamType()


        static public int DeleteControlObjectByObjectName(ApiContext db,
        string objnam)
        {
            int sqlResult = 0;
            string sql = @"Delete From dbo.ControlObjects where OBJNAM = @OBJNAM";
            sqlResult = db.Database.ExecuteSqlCommand(sql, new SqlParameter("@OBJNAM", objnam));
            return sqlResult;
        } //DeleteControlObjectByObjectName

        static public int DeleteControlObjectByObjectName(ICommonDataContext db,
        string objnam)
        {
            int sqlResult = 0;
            string sql = @"Delete From dbo.ControlObjects where OBJNAM = @OBJNAM";
            sqlResult = db.Database.ExecuteSqlCommand(sql, new SqlParameter("@OBJNAM", objnam));
            return sqlResult;
        } //DeleteControlObjectByObjectName
        #endregion

    } // class ObjectManager
}
