﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Policy;
using System.Web;

namespace Pentair.Domain.Simulator.Entity
{
    public class Chemistry
    {
        [Key]
        [MaxLength(16)]
        public string OBJNAM { get; set; }
        public UInt32 OBJREV { get; set; }
        [MaxLength(16)]
        public string OBJTYP { get; set; }
        public int LISTORD { get; set; }
        [MaxLength(128)]
        public string HNAME { get; set; }
        [MaxLength(128)]
        public string SNAME { get; set; }
        [MaxLength(16)]
        public string SUBTYP { get; set; }
        [MaxLength(16)]
        public string COMUART { get; set; }
        [MaxLength(16)]
        public string BODY { get; set; }
        [MaxLength(16)]
        public string SHARE { get; set; }
        [MaxLength(16)]
        public string READY { get; set; }
        public int PRIM { get; set; }
        public int SEC { get; set; }
        public int SUPER { get; set; }
        [MaxLength(16)]
        public string SALT { get; set; }
        [MaxLength(16)]
        public string DEVICE { get; set; }
        [MaxLength(16)]
        public string SALTLO { get; set; }
        [MaxLength(16)]
        public string VERYLO { get; set; }
        [MaxLength(16)]
        public string CURRENT { get; set; }
        [MaxLength(16)]
        public string VOLT { get; set; }
        [MaxLength(16)]
        public string STATUS { get; set; }
        public int PHSET { get; set; }
        public int ORPSET { get; set; }
        public int PHTNK { get; set; }
        public int ORPTNK { get; set; }
        public int CALC { get; set; }
        public int CYACID { get; set; }
        public int ALK { get; set; }
        public int SINDEX { get; set; }
        public int PHFED { get; set; }
        public int ORPFED { get; set; }
        public int PHVAL { get; set; }
        public int ORPVAL { get; set; }
        public int PHTIM { get; set; }
        public int ORPTIM { get; set; }
        public int PHVOL { get; set; }
        public int ORPVOL { get; set; }
        public int SALT1 { get; set; }
        public int TEMP { get; set; }
        [MaxLength(16)]
        public string NOFLO { get; set; }
        [MaxLength(16)]
        public string PHHI { get; set; }
        [MaxLength(16)]
        public string PHLO { get; set; }
        [MaxLength(16)]
        public string ORPHI { get; set; }
        [MaxLength(16)]
        public string ORPLO { get; set; }
        [MaxLength(16)]
        public string PHCHK { get; set; }
        [MaxLength(16)]
        public string ORPCHK { get; set; }
        [MaxLength(16)]
        public string PROBE { get; set; }
        [MaxLength(16)]
        public string LOCKOUT { get; set; }
        [MaxLength(16)]
        public string PHLIM { get; set; }
        [MaxLength(16)]
        public string ORPLIM { get; set; }
        [MaxLength(16)]
        public string INVALID { get; set; }
        [MaxLength(16)]
        public string COMLINK { get; set; }
        [MaxLength(16)]
        public string DEVICE1 { get; set; }
        [MaxLength(16)]
        public string CALIB { get; set; }
        [MaxLength(16)]
        public string ALARM { get; set; }
        [MaxLength(16)]
        public string PHTYP { get; set; }
        [MaxLength(16)]
        public string ORPTYP { get; set; }
        [MaxLength(16)]
        public string PHMOD { get; set; }
        [MaxLength(16)]
        public string ORPMOD { get; set; }
        [MaxLength(16)]
        public string FLOWDLY { get; set; }
        [MaxLength(16)]
        public string PHPRIOR { get; set; }
        [MaxLength(16)]
        public string MODE { get; set; }
        [MaxLength(16)]
        public string PHFED1 { get; set; }
        [MaxLength(16)]
        public string CHLOR { get; set; }
        [MaxLength(16)]
        public string STATUS1 { get; set; }
    }
}