﻿describe('UserController', function () {
    var PropertyModel;
    var $scope;
    var LightShowController;
    var $controller;
    beforeEach(module('PentairWebClient'));

    beforeEach(inject(function (_$controller_, _$rootScope_, _PropertyModel_) {
        $scope = _$rootScope_.$new();
        PropertyModel = _PropertyModel_

        PropertyModel.Security = [
            { "UFFFE": "ADMIN" },
            { "U0000": "GUEST" }];

        $controller = _$controller_;

        LightShowController = $controller("UserController", { $scope: $scope });
    }));

    describe('EditUser', function () {
        it('should set edit mode', function () {
            $scope.EditUser('U0000');
            expect($scope.Edit).toBe(true);
        });
    });

    describe('EditSave', function () {
        it('should cancel edit mode', function () {
            $scope.EditSave("", "");
            expect($scope.Edit).toBe(false);
        });
    });

    describe('Delete function', function () {
        it('should delete user', function () {
            $scope.RemoveUser('user', 'status', 'false');
            expect($scope.Delete).toBe(false);
        });
    });

    describe('Cancel Edit', function () {
        it('should cancel edit mode', function () {
            $scope.CancelEdit();
            expect($scope.Edit).toBe(false);
        });
    });
});