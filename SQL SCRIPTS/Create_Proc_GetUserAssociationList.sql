USE [PathfinderIntellicenter]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (
	SELECT * FROM sys.objects
	WHERE object_id = OBJECT_ID(N'dbo.pr_GetUserAssociationList')
	AND type in (N'P', N'PC')
)
DROP PROCEDURE dbo.pr_GetUserAssociationList
GO


-- =============================================
-- Author			:	Wayne Hui
-- Create date		:	02/14/2019
-- Parameters		:
--	@InstallationId	:	Device ID
--	@Filter			:	Filtering for the inner query
--	@Search			:	WHERE clause if any
--	@SortField		:	ORDER BY
--	@SortDirection	:	ORDER BY direction
--	@StartRow		:	OFFSET to exclude first n records
--	@NumOfRows		:	FETCH next m records
-- Description		:	Get the user association list
--
-- Modified By			Date		Description
-- =============================================
CREATE PROCEDURE [dbo].[pr_GetUserAssociationList]
	@InstallationId		INT
	, @Filter			NVARCHAR(50)
	, @Search			NVARCHAR(100)
	, @SortField		NVARCHAR(50)
	, @SortDirection	NVARCHAR(5)
	, @StartRow			INT
	, @NumOfRows		INT
AS
BEGIN

	SET NOCOUNT OFF

----TEST
--DECLARE 
--	@InstallationId		INT
--	, @Filter			NVARCHAR(50)
--	, @Search			NVARCHAR(100)
--	, @SortField		NVARCHAR(50)
--	, @SortDirection	NVARCHAR(5)
--	, @StartRow			INT
--	, @NumOfRows		INT

--SET @InstallationId	= 3948
--SET @Filter			= 'all'
--SET @Search			= ''
--SET @SortField		= 'EmailAddress'
--SET @SortDirection	= 'ASC'
--SET @StartRow			= 0
--SET @NumOfRows		= 18

----END TEST

DECLARE @SqlQuery	NVARCHAR(MAX)


	SET @SqlQuery = 
		'SELECT UserId, UserName, EmailAddress, SecurityName, AccessToken, [Status]
		 FROM 
			(
				SELECT
					A.UserId AS UserId
					,U.UserName AS UserName
					,U.Email AS EmailAddress
					,CO.Value AS SecurityName
					,A.AccessToken COLLATE DATABASE_DEFAULT AS AccessToken
					,''ACCEPTED'' AS [Status]
				FROM Associations A
					JOIN AspNetUsers U ON U.Id = A.UserId
					JOIN ControlObjects CO ON CO.INSTALLATION = A.InstallationId
				WHERE 
					A.InstallationId = {0}
					AND ''{1}'' IN (''all'', ''accepted'')
					AND OBJNAM COLLATE DATABASE_DEFAULT = A.AccessToken COLLATE DATABASE_DEFAULT
					AND [Key] = ''SNAME''
				UNION
				SELECT
					InvitationId AS UserId
					,EmailAddress AS UserName
					,EmailAddress AS EmailAddress
					,(SELECT TOP 1 Value FROM ControlObjects WHERE INSTALLATION = {0} AND OBJNAM = [AccessToken] AND [Key] = ''SNAME'') AS SecurityName
					,AccessToken COLLATE DATABASE_DEFAULT AS AccessToken
					,[Status] AS [Status]
				FROM UserInvitations
				WHERE 
					InstallationId = {0}
					AND (
						((''{1}'' NOT IN (''accepted'', ''expired'', ''bounced'', ''noresponse'')) AND ([Status] <> ''ACCEPTED''))
						OR (''{1}'' = ''expired'' AND [Status] = ''EXPIRED'')
						OR (''{1}'' = ''bounced'' AND [Status] = ''BOUNCED'')
						OR (''{1}'' = ''noresponse'' AND ([Status] = ''UNKNOWN''))
					)
			 ) AS RAWLIST
		 {2}
		 ORDER BY {3} {4}
		 OFFSET {5} ROWS
		 FETCH NEXT {6} ROWS ONLY'

	----Replace the input parameters into the query
	SET @SqlQuery = REPLACE(@SqlQuery, '{0}', @InstallationId)
	SET @SqlQuery = REPLACE(@SqlQuery, '{1}', @Filter)
	SET @SqlQuery = REPLACE(@SqlQuery, '{2}', @Search)
	SET @SqlQuery = REPLACE(@SqlQuery, '{3}', @SortField)
	SET @SqlQuery = REPLACE(@SqlQuery, '{4}', @SortDirection)
	SET @SqlQuery = REPLACE(@SqlQuery, '{5}', @StartRow)
	SET @SqlQuery = REPLACE(@SqlQuery, '{6}', @NumOfRows)

	EXEC (@SqlQuery)

END

GO


--GRANT EXECUTE ON [dbo].[pr_GetUserAssociationList] TO PUBLIC
--GO
