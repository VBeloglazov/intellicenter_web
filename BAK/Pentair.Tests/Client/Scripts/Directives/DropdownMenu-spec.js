﻿describe('DropDown Menu', function () {
    var $scope, $compile;

    var withChangeEventTemplate = ' <div dropdown-menu="MockItems" dropdown-model="SelectedItem" dropdown-onchange="OnDropDownChange()" label-field="CUSTOM_LABEL"></div>';
    var customLabelTemplate = ' <div dropdown-menu="CustomLabelMockItems" dropdown-model="SelectedItem" label-field="CUSTOM_LABEL"></div>';
    var customValueTemplate = ' <div dropdown-menu="CustomValueMockItems" dropdown-model="SelectedItem" value-field="CUSTOM_VALUE"></div>';
    var baseTemplate = ' <div default-value="DefaultInstallation"  dropdown-menu="MockItems" dropdown-model="SelectedItem"></div>';
    var disabledTemplate = '<toggle-circuit model="switchState" disabled="isDisabled"></toggle-circuit>';

    var mockItems = [
        { name: 'Option1', value: '1' },
        { name: 'Option2', value: '2' },
        { name: 'Option3', value: '3' }
    ];
    var customValueMockItems = [
        { name: 'CustomOption4', CUSTOM_VALUE: '4', connected: true },
        { name: 'CustomOption5', CUSTOM_VALUE: '5', connected: false },
        { name: 'CustomOption6', CUSTOM_VALUE: '6', connected: true }
    ];
    var customLabelMockItems = [
       { CUSTOM_LABEL: 'CustomOption7', value: '7', connected: true },
       { CUSTOM_LABEL: 'CustomOption8', value: '8', connected: false },
       { CUSTOM_LABEL: 'CustomOption9', value: '9', connected: true }
    ];

    beforeEach(module('PentairWebClient'));

    beforeEach(inject(function ($rootScope, _$compile_) {
        // Get an isolated scope
        $scope = $rootScope.$new();
        $scope.CustomLabelMockItems = customLabelMockItems;
        $scope.CustomValueMockItems = customValueMockItems;
        $scope.DefaultInstallation = { name: 'Select A Installation', property: '0' };
        $scope.MockItems = mockItems;
        $compile = _$compile_;
    }));

    
    describe('null selection', function () {
        var elm;
        var selected;
        beforeEach(function () {
            $scope.SelectedItem = null;

            elm = SpecHelper.createDirective($compile, baseTemplate, $scope);
            selected = elm[0].children[0].textContent;
        });

        it('should set selected Item to Option1', function () {
            expect(elm[0].children[0].textContent).toBe('Select A Installation');
        });

    });

    describe('default selection', function () {
        var elm;
        var selected;
        beforeEach(function () {
            $scope.SelectedItem = '0';
            elm = SpecHelper.createDirective($compile, baseTemplate, $scope);
            selected = elm[0].children[0].textContent;
        });

        it('should set selected Item to Option1', function () {
            expect(elm[0].children[0].textContent).toBe('Select A Installation');
        });

    });

    describe('set selection via model', function () {
        var elm;
        var selected;
        beforeEach(function () {
            $scope.SelectedItem = '1';
            elm = SpecHelper.createDirective($compile, baseTemplate, $scope);
            selected = elm[0].children[0].textContent;
        });

        it('should set selected Item to Option1', function () {
            expect(elm[0].children[0].textContent).toBe('Option1');
        });

        it('should update selected item to Option2', function () {
            $scope.SelectedItem = '2';
            $scope.$apply();
            expect(elm[0].children[0].textContent).toBe('Option2');
        });
    });

    describe('set model via selection', function () {
        var elm;
        var selected;
        beforeEach(function () {
            $scope.SelectedItem = '0';
            elm = SpecHelper.createDirective($compile, baseTemplate, $scope);
        });

        it('should set selected Item to Option2 When clicked', function () {

            elm[0].children[1].children[1].children[0].click();
            expect(elm[0].children[0].textContent).toBe('Option2');
            expect($scope.SelectedItem).toBe('2');
        });

    });

    describe('set active class', function () {
        var elm;
        var selected;
        beforeEach(function () {
            elm = SpecHelper.createDirective($compile, baseTemplate, $scope);
        });

        it('should toggle element class to active when clicked', function () {
            expect(elm.hasClass('active')).toBe(false);
            elm[0].click();
            expect(elm.hasClass('active')).toBe(true);
            elm[0].click();
            expect(elm.hasClass('active')).toBe(false);
        });

    });

    describe('custom Label templates', function () {
        var elm;
        var selected;
        beforeEach(function () {
            $scope.SelectedItem = '8';
            elm = SpecHelper.createDirective($compile,customLabelTemplate, $scope);
            selected = elm[0].children[0].textContent;
        });

        it('should set selected Item to Option1', function () {
            expect(elm[0].children[0].textContent).toBe('CustomOption8');
        });
    });

    describe('custom value templates', function () {
        var elm;
        var selected;
        beforeEach(function () {
            $scope.SelectedItem = '4';
            elm = SpecHelper.createDirective($compile, customValueTemplate, $scope);
            selected = elm[0].children[0].textContent;
        });

        it('should set selected Item to Option1', function () {
            expect(elm[0].children[0].textContent).toBe('CustomOption4');
        });
    });
});