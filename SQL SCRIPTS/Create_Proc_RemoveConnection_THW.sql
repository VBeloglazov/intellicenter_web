-- =============================================================
--
-- Author:		Vladimir Beloglazov
-- Create date: 02/08/2018
--
-- Creates:
--
--		Table Type:			None
--
--		Stored Procedure:	PRemoveConnectionThw
--
-- 2018-03-16 VB: [Installations] redesign.
--
-- =============================================================

USE PathfinderIntelliCenter
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (
	SELECT * FROM sys.objects
	WHERE object_id = OBJECT_ID(N'dbo.PRemoveConnectionThw')
	AND type in (N'P', N'PC')
)
DROP PROCEDURE dbo.PRemoveConnectionThw
GO

-- -------------------------------------------------------------
--
-- Returns just deleted [Connections] record ID.
-- Updates [Installation] record accordingly.
-- Removes [Events] records related to the THW.
--
-- -------------------------------------------------------------
CREATE PROCEDURE dbo.PRemoveConnectionThw
	  @connectionId		AS uniqueidentifier
	 ,@installationId	AS int

AS
BEGIN

	SET NOCOUNT OFF

	BEGIN TRANSACTION

	BEGIN TRY

		-- Validate input
		IF NOT EXISTS (SELECT * FROM dbo.Installations WHERE InstallationId = @installationId)
			THROW 50000, 'Installation not found', 1;

		DELETE FROM dbo.Connections
		OUTPUT DELETED.ConnectionId
		WHERE ConnectionId = @connectionId
	
		-- TODO: What's the idea with events? Who uses the data? Pathfinder was adding InstallationDisconnectEvent = 2
		--		 here, but this way the table will grow unlimited.
		-- Add Event
        --INSERT INTO dbo.[Events] (EventId, InstallationId, UserId)
        --VALUES (@eventId, @installationId, @userId)
		-- HACK
		DELETE FROM dbo.[Events]
		WHERE InstallationId = @installationId

	END TRY
	BEGIN CATCH

		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION

		DECLARE @err_message nvarchar(255)
		SET @err_message = ERROR_MESSAGE()
		RAISERROR (@err_message, 11, 1) -- severity 11-19 will cause returning exception to caller

	END CATCH

	IF @@TRANCOUNT > 0
		COMMIT TRANSACTION

END
GO
