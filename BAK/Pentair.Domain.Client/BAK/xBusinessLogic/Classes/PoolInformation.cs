﻿using Pentair.Domain.Client.Helpers;

namespace Pentair.Domain.Client.BusinessLogic.Classes
{
    public class PoolInformation
    {
        public string PoolName { get; set; }
        public string PentairPhoneContact { get; set; }

        public PoolInformation()
        {
            PentairPhoneContact = StaticData.PentairPhoneContact;
        }
    }

}
