//'use strict';
var PWC = PWC || {};
PWC.directive('historyChart', function($parse) {

  function getMinValue(values) {
      var result = 0.0;
      if (values != undefined) {
          var first = true;
          angular.forEach(values, function (item) {
              if (first){
                  result = item.value
                  first = false;
              }else{
                  if (result > item.value)
                      result = item.value;
            }                
          });
      }
      if (result.toString().indexOf('.', 0) >= 0)
          if (result.toFixed)
              result = result.toFixed(1);

      return result 
  }
  function getMaxValue(values) {
      var result = 0.0;
      if (values != undefined) {
          var first = true;
          angular.forEach(values, function (item) {
              if (first) {
                  result = item.value
                  first = false;
              } else {
                  if (result < item.value)
                      result = item.value;
              }
          });
      }
      if (result.toString().indexOf('.', 0) >= 0)
          if (result.toFixed)
              result = result.toFixed(1);

      return result
  }

  function drawChart(scope, element) {
      var uniqueId = 'a' + Math.uuid(15); // id's can't start with numbers    

    // to display both charts
     // var phOn = scope.xAxis.phOn;  // true
      //var orpOn = scope.xAxis.orpOn;  //true;
      var zoomOn = false;      
    // remove existing content
    element.html('');
    element.append('<div id="' + uniqueId + '"></div>');
    d3.select('#' + uniqueId).attr('class', 'history-chart');
    var historyAnchor = d3.select("#" + uniqueId);
    var legendContainer = historyAnchor.append("div").attr("class", "legend");
    var legend;
    var legendFormat = 'legend-box-chem'
    if ((scope.xAxis.phOn && !scope.xAxis.orpOn) || (!scope.xAxis.phOn && scope.xAxis.orpOn))
        legendFormat = 'legend-box-chem-one'

    if (scope.dataTopic === 'temp') {
      legend = legendContainer
        .append('svg').attr('class', 'legend-box-A');
    } else {
        
        if ((scope.xAxis.phOn && scope.xAxis.orpOn) )   // checking if both charts are selected
            legend = legendContainer.append('svg').attr('class', 'legend-box-chem');
        else
            legend = legendContainer.append('svg').attr('class', 'legend-box-chem-one');
    }

    var ySvg = legendContainer.append('svg')
    if (scope.dataTopic === 'temp') {
        
        ySvg.attr('class', 'legend-axis-A');
    } else {
        if ((scope.xAxis.phOn && scope.xAxis.orpOn))   // checking if both charts are selected
            ySvg.attr('class', 'legend-axis-chem');
        else
            ySvg.attr('class', 'legend-axis-chem-one');
    }
    
    var svg = historyAnchor.append("svg")
      .attr("class", "line-chart").attr("width", 988).attr("height", 350); //362);
    var margin = {
      top: 20,
      right: 15,
      bottom: 15,
      left: 1 //15
    };
    var listNames = [];
    var listColors = [];
    var count = 0

    var parseDate = d3.time.format("%d-%b-%y").parse;
    var width = svg.attr("width") - margin.left - margin.right;
    var height = svg.attr("height") - margin.top - margin.bottom;
    var g = svg.append("g").attr("transform", "translate(" + margin.left + "," + margin.top + ")");
    var t1 = 70
    if (scope.dataTopic === 'temp')
        t1 = 45
    var g2 = ySvg.append("g").attr("transform", "translate(" + t1 + "," + margin.top + ")");
    var timeFormat = d3.time.format("%Y%m%d");
    var x = d3.time.scale().range([0, width]);
    var y = d3.scale.linear().range([height, 0]);
    var y1 = d3.scale.linear().range([height, 0]);
    var line, line1;
    if (scope.dataTopic === 'temp') {
      line = d3.svg.line()
        //.interpolate("basis")     // removing the interpolation plotting
        .x(function(d) {
          return x(d.date);
        })
        .y(function(d) {
          return y(d.yValue);
        }); 
    } else {
      line = d3.svg.line()
        //.interpolate("basis")         // removing the interpolation plotting
        .x(function(d) {
          return x(d.date);
        })
        .y(function(d) {
          return y(d.yValue);
        });
      line1 = d3.svg.line()
        //.interpolate("basis")         // removing the interpolation plotting
        .x(function(d) {
          return x(d.date);
        })
        .y(function(d) {
          return y1(d.yValue);
        });
    }
    //### legend code
    var sampleOrdinal;
    if (scope.dataTopic === 'temp') {
        //--------------------  temperature -------------------
 
        if (scope.xAxis.poolTempOn) {
            listNames[count] = "Pool Temp.";
            listColors[count] = "rgb(0, 68, 111)"
            count++;
        }

        if (scope.xAxis.spaTempOn) {
            listNames[count] = "Spa Temp.";
            listColors[count] = "rgb(0, 167, 13)"
            count++;
        }
        if (scope.xAxis.solarTempOn) {
            listNames[count] = "Solar Temp.";
            listColors[count] = "rgb(242, 94, 25)"
            count++;
        }
        if (scope.xAxis.airTempOn) {
            listNames[count] = "Air Temp.";
            listColors[count] = "rgb(21, 209, 242)"
            count++;
        }
        
        
        // MIN AND MAX VALUES ----------------
        if (count === 1) {
            var min = 0;
            var max = 0;
            
            if (scope.data != undefined) {
                if (scope.xAxis.poolTempOn) {
                    if (scope.data.poolTemp != undefined) {
                        min = getMinValue(scope.data.poolTemp.history);
                        max = getMaxValue(scope.data.poolTemp.history);
                    }
                }
                if (scope.xAxis.spaTempOn) {
                    if (scope.data.spaTemp != undefined) {
                        min = getMinValue(scope.data.spaTemp.history);
                        max = getMaxValue(scope.data.spaTemp.history);
                    }
                }

                if (scope.xAxis.solarTempOn) {
                    if (scope.data.solarTemp != undefined) {
                        min = getMinValue(scope.data.solarTemp);
                        max = getMaxValue(scope.data.solarTemp);
                    }
                }

                if (scope.xAxis.airTempOn) {
                    if (scope.data.poolSideTemp != undefined) {
                        min = getMinValue(scope.data.poolSideTemp);
                        max = getMaxValue(scope.data.poolSideTemp);
                    }
                }
            }

            var format = " "
            
            if ( (scope.dataTopic === 'temp')&& (scope.xAxis.temperatureFormat === "ENGLISH") ) 
                format = '℉';
            else 
                format = '℃'
            
            if (scope.data === undefined) {
                max = " - "
                min = " - "
            }
            listNames[count] = "High " + max + format;
            count++;

            listNames[count] = "Low  " + min + format;
            count++;

        }else if (count === 0){
            listNames[count] = "Select Temp." ;
            count++;

            listNames[count] = "Type to Plot?";
            count++;
        }
 

            //sampleOrdinal = d3.scale.category20().domain(["Pool Temp.", "Spa Temp.", "Solar Temp.", "Air Temp."]).range(["rgb(0, 68, 111)", "rgb(0, 167, 13)", "rgb(242, 94, 25)", "rgb(21, 209, 242)"]);        
        sampleOrdinal = d3.scale.category20().domain(listNames).range(listColors);

        } else {
            //--------------------- chemistry -----------------

        if (scope.xAxis.phOn) {
            listNames[count] = "pH";
            listColors[count] = "rgb(0,0, 255)"
            count++;
        }
        if (scope.xAxis.orpOn) {
            listNames[count] = "ORP";
            listColors[count] = "rgb(255,0, 0)"
            count++;
        }
        if (count === 1) {
            var min = 0;
            var max = 0;
            if (scope.xAxis.phOn) {
                if (scope.data.phLevel != undefined) {
                    min = getMinValue(scope.data.phLevel);
                    max = getMaxValue(scope.data.phLevel);
                }
            }

            if (scope.xAxis.orpOn) {
                if (scope.data.orpLevel != undefined) {
                    min = getMinValue(scope.data.orpLevel);
                    max = getMaxValue(scope.data.orpLevel);
                }
            }

            if (count === 1) {
                listNames[count] = "High:" + max;
                count++;

                listNames[count] = "Low:" + min;
                count++;
            }
        }else if (count === 0){
            listNames[count] = "High: no data";
            count++;

            listNames[count] = "Low: no data";
            count++;
        }

        sampleOrdinal = d3.scale.category20().domain(listNames).range(listColors);
    }

    var verticalLegend = d3.svg.legend().labelFormat("none").cellPadding(5).orientation("vertical").units("").cellWidth(25).cellHeight(22).inputScale(sampleOrdinal).cellStepping(10);

    legend.append("g").attr("transform", "translate(10,10)").attr("class", "legend").call(verticalLegend);
    //### end legend code
    x.domain(scope.xAxis.domain);
    var xAxis = d3.svg.axis().scale(x);
    xAxis.orient("bottom");
    xAxis.innerTickSize(-height);
    xAxis.outerTickSize(0);
    xAxis.ticks(scope.xAxis.ticks.timeScale, scope.xAxis.ticks.timeInterval);
    xAxis.tickFormat("");    

    g.append("g")
      .attr("class", "axis axis--x")
      .attr("transform", "translate(0," + height + ")")
      .call(xAxis);

    var data = scope.data;

    if (!data) {
      return;
    }

    var legendTypes = [];
    if (scope.dataTopic === 'temp') {

        var zeroValue = 0   // celcius
        if (scope.xAxis.temperatureFormat === "ENGLISH")
            zeroValue = 32  // farenheit

        //---------------- POOL ----------------
        if (scope.xAxis.poolTempOn) {

            if (!scope.xAxis.freezeOn) {
                data.poolTemp.history = data.poolTemp.history.filter(function (item) {
                    return item.value != zeroValue;
                });
            }

            legendTypes.push({
                id: 'Pool Temp.',
                values: data.poolTemp.history.map(function (poolTempItem) {                    
                    return {
                        date: moment.unix(parseInt(poolTempItem.time)).toDate(),
                        yValue: parseFloat(poolTempItem.value)
                    }
                })
            });
        }
        //---------------- SPA ----------------
        if (scope.xAxis.spaTempOn){

            if (!scope.xAxis.freezeOn)
                data.spaTemp.history = data.spaTemp.history.filter(function (item) {
                    return item.value != zeroValue;
                });

        legendTypes.push({
            id: 'Spa Temp.',
            values: data.spaTemp.history.map(function (spaTempItem) {                
                return {
                    date: moment.unix(parseInt(spaTempItem.time)).toDate(),
                    yValue: parseFloat(spaTempItem.value)
                }
            })
        });
      }
      //---------------- SOLAR ----------------
        if (scope.xAxis.solarTempOn) {

            if (!scope.xAxis.freezeOn)
                data.solarTemp = data.solarTemp.filter(function (item) {
                    return item.value != zeroValue;
                });

            legendTypes.push({
                id: 'Solar Temp.',
                values: data.solarTemp.map(function (solarTempItem) {                    
                        return {
                            date: moment.unix(parseInt(solarTempItem.time)).toDate(),
                            yValue: parseFloat(solarTempItem.value)
                        }
                })
            });
        }

     //---------------- AIR ----------------
        if (scope.xAxis.airTempOn) {

            if (!scope.xAxis.freezeOn)
                data.poolSideTemp = data.poolSideTemp.filter(function (item) {
                    return item.value != zeroValue;
                });

            legendTypes.push({
                id: 'Air Temp.',
                values: data.poolSideTemp.map(function (poolSideTempItem) {                    
                        return {
                            date: moment.unix(parseInt(poolSideTempItem.time)).toDate(),
                            yValue: parseFloat(poolSideTempItem.value)
                        }
                })
            });
        }
    } else {
        /*
      legendTypes.push({
        id: 'ph',
        values: data.phLevel.map(function(phLevelItem) {
          return {
            date: moment.unix(parseInt(phLevelItem.time)).toDate(),
             // yValue: parseFloat((Math.round( parseFloat(phLevelItem.value) * 10 ) / 10).toFixed(1))
            yValue: parseFloat(Math.round(phLevelItem.value).toFixed(1))
          }
        })
      });

      legendTypes.push({
        id: 'orp',
        values: data.orpLevel.map(function(orpLevelItem) {
          return {
            date: moment.unix(parseInt(orpLevelItem.time)).toDate(),
            yValue: parseInt(orpLevelItem.value)
            
          }
        })
      });
    */
    }

      // value Adjuster , if both 
    var adjustPh = 100;
    if (!scope.xAxis.orpOn)
        adjustPh = 100;

     // not adjusment is need it
    if (scope.xAxis.zoomOn && !scope.xAxis.orpOn)
        adjustPh = 1;


    if (scope.dataTopic === 'chem') {
        //----------------- ph -------------------
        if (scope.xAxis.phOn) {
            if (!scope.xAxis.zeroOn)
                data.phLevel = data.phLevel.filter(function (item) {
                    return item.value != 0; // real zero
                });

            legendTypes.push({
                id: 'ph',
                values: data.phLevel.map(function (phLevelItem) {                    
                        return {
                            date: moment.unix(parseInt(phLevelItem.time)).toDate(),
                            // adjust to 1000 (original range 0 to 10, same  range  0 to 1000                    
                            yValue: parseFloat(Math.round(phLevelItem.value * adjustPh).toFixed(1))
                        }
                })
            });
        }

        if (scope.xAxis.orpOn) {
            if (!scope.xAxis.zeroOn)
                data.orpLevel = data.orpLevel.filter(function (item) {
                    return item.value != 0; // real zero
                });

            legendTypes.push({
                id: 'orp',
                values: data.orpLevel.map(function (orpLevelItem) {
                    if (scope.xAxis.zeroOn || (!scope.xAxis.zeroOn && orpLevelItem.value != 0))// include zero values 
                        return {
                            date: moment.unix(parseInt(orpLevelItem.time)).toDate(),
                            yValue: parseInt(orpLevelItem.value)
                        }
                })
            });
        }
    }



    if (scope.dataTopic === 'temp') {
        var min = d3.min(legendTypes, function(c) {
            return d3.min(c.values, function(d) {
                return d.yValue;
            });
        });
        var max = d3.max(legendTypes, function(c) {
            return d3.max(c.values, function(d) {
                return d.yValue;
            });
        });
  
        if(min === max) {
            max += 1;
            min = min-1;  // center chart
        }

        if (!scope.xAxis.zoomOn) {
            if (scope.xAxis.temperatureFormat === "ENGLISH") {
                //'℉';
                if (min >= 32)
                    min = 32;       // 32 fareheid
                if (max < 105)
                    max = 105;
            }
            else { //'℃'
                if (min >= 0) 
                    min = 0;
                if (max < 40) {
                    max = 40;
                }
            }
        }
        // round to near upper 10th value
        if (!scope.xAxis.zoomOn)    // not zoom
            max = Math.round((max + 9) / 10) * 10;

        y.domain([min, max]);
    } else {

        // ------------------- checking orp         
            var min = d3.min(legendTypes.filter(function (legendType) {
                return legendType.id === 'orp'
            }), function(c) {
                return d3.min(c.values, function(d) {
                    return d.yValue;
                });
            });

            var max = d3.max(legendTypes.filter(function (legendType) {
                return legendType.id === 'orp'
            }), function(c) {
                return d3.max(c.values, function(d) {
                    return d.yValue;
                });
            });
      
        
            var zoomOn = false; // applies only if one chart is off
            if ((scope.xAxis.phOn && !scope.xAxis.orpOn) || (!scope.xAxis.phOn && scope.xAxis.orpOn)) {
                zoomOn = scope.xAxis.zoomOn
            }

            if (!zoomOn) {   // not zoom
                min = 0;
                max = 1000;
            }
            
            if(min === max) {
                max += 1;
                min = min - 1;
            }
                               
            if (scope.xAxis.orpOn)  // check if is enabled
                y.domain([min, max]);
        
        // ------------------- checking ph         
            var min1 = d3.min(legendTypes.filter(function(legendType) {
                return legendType.id === 'ph'
            }), function(c) {
                return d3.min(c.values, function(d) {
                    return d.yValue;
                });
            });

            var max1 = d3.max(legendTypes.filter(function(legendType) {
                return legendType.id === 'ph'
            }), function(c) {
                return d3.max(c.values, function(d) {
                    return d.yValue;
                });
            });
            zoomOn = false; // applies only if one chart is off
            if ((scope.xAxis.phOn && !scope.xAxis.orpOn) || (!scope.xAxis.phOn && scope.xAxis.orpOn)) {
                zoomOn = scope.xAxis.zoomOn
            }


            if (!zoomOn) {   // not zoom
                min1 = 0;
                max1 = 10;
            } else {
                if (min1 === max1) {
                    min1 -= 0.5;
                    max1 = min1 + 1;
                }
            }

            if (scope.xAxis.phOn) { // check if is enabled
                y1.domain([min1, max1]);

                if (!scope.xAxis.orpOn) {  // check if is enabled
                    if (!zoomOn) {
                        min1 = 0;
                        max1 = 1000;
                    }
                    y.domain([min1, max1]);

                }
            }                    
    }

    if (scope.dataTopic === 'temp') {
      var yAxis = d3.svg.axis().scale(y).orient("right");

      yAxis.outerTickSize(0);
      yAxis.ticks(6);
      yAxis.tickFormat(function(d) {

          if (scope.xAxis.temperatureFormat === "ENGLISH")
              return d + '℉';
          else
              return d + '℃'
      });
      yAxis.innerTickSize(0);
      g2.append("g")
        .attr("class", "axis axis--y temp")
        .call(yAxis);
      yAxis.tickFormat("");
      yAxis.innerTickSize(-width);
      yAxis.orient("left");
      g.append("g")
        .attr("class", "axis axis--y temp")
        .call(yAxis);
    } else {
               
        if (scope.xAxis.orpOn &&
            (scope.data.orpLevel.length > 0)) {

         var align = "left";
         if (scope.xAxis.phOn )
                align = "right";

          var yAxis = d3.svg.axis().scale(y).orient(align); //right");
          yAxis.outerTickSize(0);          
          yAxis.ticks(6);
          yAxis.tickFormat(function (d) {                          
              return d + 'mV'
          });
          yAxis.innerTickSize(0);
          g2.append("g")
            .attr("class", "axis axis--y red")
            .call(yAxis);
          yAxis.tickFormat("");
          yAxis.innerTickSize(-width);
          yAxis.orient("left");
          g.append("g")
            .attr("class", "axis axis--y red")
            .call(yAxis);
        }

        if (scope.xAxis.phOn &&
            (scope.data.phLevel.length > 0)) {          
         var align = "left";
         var phunits = 'pH: ';

         if (scope.xAxis.phOn && scope.xAxis.orpOn)
             align = "left";

         if (scope.xAxis.phOn && !scope.xAxis.orpOn) {
             //align = "right";
             phunits = 'pH: ';
         }

          var yAxis1 = d3.svg.axis().scale(y1).orient(align); 
          yAxis1.outerTickSize(0);
          
          yAxis1.tickFormat(function (d) {  // ph units
              if (scope.xAxis.phOn && scope.xAxis.orpOn)
                  return phunits + d + " /"
              else
                  return phunits  +d 
          });

          yAxis1.ticks(6);
          yAxis1.innerTickSize(0); 
          g2.append("g")
            .attr("class", "axis axis--y blue")
            .call(yAxis1);

          yAxis1.tickFormat("");
          yAxis1.innerTickSize(-width);
          yAxis1.orient("left");
          g.append("g")
            .attr("class", "axis axis--y blue")
            .call(yAxis1);
        }
    
    }

    var legendType = g.selectAll(".legendTypes")
      .data(legendTypes)
      .enter().append("g")
      .attr("class", "legendType");

    if (scope.dataTopic === 'temp') {
      legendType.append("path")
        .attr("class", "line")
        .attr("d", function(d) {
          return line(d.values);
        })
        .attr("stroke-width", function(d) {
          if ((scope.xAxis.poolTempOn) && (d.id === 'Pool Temp.'))
              return 2;
          if ((scope.xAxis.spaTempOn) &&  (d.id === 'Spa Temp.'))
              return 2;
          if ((scope.xAxis.solarTempOn) && (d.id === 'Solar Temp.'))
              return 2;
          if ((scope.xAxis.airTempOn) && (d.id === 'Air Temp.'))
              return 2;
          else
            return 2;
        })
        .style("stroke", function(d) {
            if ((scope.xAxis.poolTempOn) && (d.id === 'Pool Temp.'))
              return 'rgba(0, 68, 111, 0.9)';              
            if ((scope.xAxis.spaTempOn) && (d.id === 'Spa Temp.'))
              return 'rgba(0, 167, 13, 0.9)';
            if ((scope.xAxis.solarTempOn) && (d.id === 'Solar Temp.'))
              return 'rgba(242, 94, 25, 0.9)';
            if ((scope.xAxis.airTempOn) && (d.id === 'Air Temp.'))
              return 'rgba(21, 209, 242, 0.9)';
          else
            return 'black';
        });
    } else {
            


  /* ORIGINAL
        // ORP LEVELS
        if(scope.data.orpLevel.length > 0) {
          legendType.append("path")
            .attr("class", "line")
            .attr("d", function(d) {
              return line(d.values);
            })
            .attr("stroke-width", 2)
            .style("stroke", 'rgba(255, 0, 0, 0.9)');
        }
        // PH LEVELS
        if(scope.data.phLevel.length > 0) {
          legendType.append("path")
        .attr("class", "line")
        .attr("d", function(d) {
          return line1(d.values);
        })
        .attr("stroke-width", 2)            
          .style("stroke", 'rgb(21, 209, 242, 0.9)');         
        }        
*/
    }
    var charttype = "line";
    if ((scope.xAxis.phOn && !scope.xAxis.orpOn)  || (!scope.xAxis.phOn && scope.xAxis.orpOn) )    {
        charttype = "line"; //charttype = "area"
    }

    
    if (scope.dataTopic === 'chem') {
        legendType.append("path")
          .attr("class", charttype) //"line")
          //.attr("transform", "null")
          .attr("d", function (d) {
              return line(d.values);
          })
          .attr("stroke-width", function (d) {
              if (scope.xAxis.phOn && (d.id === 'ph'))
                  return 2;
              if (scope.xAxis.orpOn && (d.id === 'orp'))
                  return 2;
              else
                  return 2;
          })
          .style("stroke", function (d) {
              if (scope.xAxis.phOn && (d.id === 'ph'))
                  return 'rgba(0, 0, 255, 0.9)';

              if (scope.xAxis.orpOn && (d.id === 'orp'))
                  return 'rgba(255, 0, 0, 0.9)';
              else
                  return 'black';
          });
    }
   
/*  work in progress for crosshair  
    var tooltip = d3.select("#" + uniqueId)
    .append("div")
    .style("position", "absolute")
    .style("z-index", "10")
    .style("visibility", "hidden")
    .style("background", "#000")
    .text("a simple tooltip");

    d3.select("#" + uniqueId)
      .selectAll("div")
        .data(legendTypes)
      .enter().append("div")
        .style("width", function (d) { return x(d) + "px"; })
        .text(function (d) { return d; })
        .on("mouseover", function (d) { tooltip.text(d); return tooltip.style("visibility", "visible"); })
          .on("mousemove", function () { return tooltip.style("top", (d3.event.pageY - 10) + "px").style("left", (d3.event.pageX + 10) + "px"); })
          .on("mouseout", function () { return tooltip.style("visibility", "hidden"); });
*/
  }


  var directiveDefinitionObject;
  directiveDefinitionObject = {
    restrict: 'E',
    replace: false,
    scope: {
      data: '=chartData',
      xAxis: '=chartXAxis',
      dataTopic: '=chartDataTopic'
    },

    link: function (scope, element, attrs) {

      scope.$watch("data", function () {
          drawChart(scope, element);          
      }, true);

      scope.$watch("xAxis", function () { // we need this because sometimes the data stays the same for weekly and monthly.
          drawChart(scope, element);          
      }, true);

      

    }
  };
  return directiveDefinitionObject;
});