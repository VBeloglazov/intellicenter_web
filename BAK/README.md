

Pentair Web API / Simulator Repository
======================================
Code repository containing source code for Pentair Domain, API, and Simulator projects.

Requirements
-----------
* [Visual Studio 2013](http://www.visualstudio.com/en-us/downloads)
* [SQL Server 2012](http://www.microsoft.com/en-us/server-cloud/products/sql-server/try.aspx#fbid=mYq2R5wNoyz)
* [.NET 4.5.1](http://www.microsoft.com/en-us/download/details.aspx?id=40779)
* [Entity Framework 6](http://www.nuget.org/packages/EntityFramework)
* [MSpec](http://www.nuget.org/packages/machine.specifications)
* [Moq](http://www.nuget.org/packages/Machine.Fakes.Moq/)
* [NBuilder](http://www.nuget.org/packages/nbuilder)
* [Ninject](http://www.nuget.org/packages/Ninject/3.0.1.10)
* [Mspec Test Adapter](http://visualstudiogallery.msdn.microsoft.com/4abcb54b-53b5-4c44-877f-0397556c5c44)
* [NUnit Test Adapter](http://visualstudiogallery.msdn.microsoft.com/6ab922d0-21c0-4f06-ab5f-4ecd1fe7175d)
* [SpecFlow for Visual Studio 2013](http://visualstudiogallery.msdn.microsoft.com/90ac3587-7466-4155-b591-2cd4cc4401bc) 
 
*Pathfinder is using Windows 8.1 for development*


###Code First Migrations with Entity Framework###
To update your local database to the latest schema you must run migrations. 

If you are updating `Pentair.Domain.Client`, run the following from the package management console in Visual Studio:

* `update-database -configuration Pentair.Domain.Client.UserMigrations.Configuration2 -ProjectName Pentair.Domain.Client -StartUpProjectName Pentair.Web.Api`

 `update-database -configuration Pentair.Domain.Client.ClientMigrations.Configuration -ProjectName Pentair.Domain.Client -StartUpProjectName Pentair.Web.Api`  
* `update-database -configuration Pentair.Domain.Client.ClientMigrations.Configuration1 -ProjectName Pentair.Domain.Client -StartUpProjectName Pentair.Web.Api`  

