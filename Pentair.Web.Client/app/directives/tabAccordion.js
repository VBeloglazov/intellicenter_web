'use strict';
var PWC = PWC || {};
///DEPRECATE!

PWC.directive('tabAccordion', [
    function () {
        return {
            restrict: 'EA',
            transclude: true,
            replace: true,
            scope:
            {
                onlyOneExpanded: '@',
                itemSelected: '&'
            },
            template: '<div class="tabAccordion" ng-transclude></div>',
            controller: [
                '$scope', function (scope) {
                    var collapsibleTabs = [];

                    this.openTab = function (tabToOpen, panel) {
                        if (scope.onlyOneExpanded) {
                            angular.forEach(collapsibleTabs, function (tab) {
                                tab.isOpen = false;
                                tab.stateIcon = 'closed';
                            });
                            tabToOpen.isOpen = true;
                            tabToOpen.stateIcon = 'open';
                        }
                        scope.itemSelected({ objNam: panel });
                    }

                    this.addCollapsibleTab = function (collapsibleTab) {
                        collapsibleTabs.push(collapsibleTab);
                    }
                }
            ]
        };
    }
]).directive('collapsibleTab', [
    function () {
        return {
            require: '^tabAccordion',
            restrict: 'EA',
            transclude: true,
            replace: true,
            scope: {
                enclosureName: '@',
                defaultOpen: '@',
                listOrder: '@',
                typeIcon: '@',
                item: '@'
            },
            link: function (scope, element, attrs, tabAccordionController) {
                if (scope.defaultOpen == 'true') {
                    scope.isOpen = true;
                    scope.stateIcon = 'open';
                } else {
                    scope.isOpen = false;
                    scope.stateIcon = 'closed';
                }
                if (scope.listOrder == '1') {
                    scope.staticIcon = 'main';
                } else {
                    scope.staticIcon = 'module';
                }

                tabAccordionController.addCollapsibleTab(scope);

                scope.toggleCollapsibleTab = function (tab) {
                    if (!tab.isOpen) {
                        tabAccordionController.openTab(tab, scope.item);
                    }
                }
            },
            template: '<div class="collapsibleTab" >' +
                        '<div class="tab nav icon {{stateIcon}}" ng-click="toggleCollapsibleTab(this)">' +
                            '<div class="tab static icon {{staticIcon}}" ng-click="toggleCollapsibleTab(this)">' +
                                '{{enclosureName}}' +
                                '<div class="content" ng-show="isOpen" ng-transclude>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                    '</div>'
        };
    }
]);


angular.module("template/accordion/accordion-group.html", []).run(["$templateCache", function ($templateCache) {
    $templateCache.put("template/accordion/accordion-group.html",
      "<div class=\"panel panel-default\" ng-class=\"{'panel-open': isOpen}\">\n" +
      "  <div class=\"panel-heading {{bgColor}}\" ng-click=\"toggleOpen()\">\n" +
      "    <h4 class=\"panel-title\">\n" +
      "      <div tabindex=\"0\" class=\"accordion-toggle\" accordion-transclude=\"heading\">"+
      "         <span ng-class=\"{'text-muted': isDisabled}\">{{heading}}</span>" +
      "      </div>\n" +
      "    </h4>\n" +
      "  </div>\n" +
      "  <div class=\"panel-collapse collapse\" collapse=\"!isOpen\">\n" +
      "	  <div class=\"panel-body\" ng-transclude></div>\n" +
      "  </div>\n" +
      "</div>\n" +
      "");
}]);

angular.module("template/tabs/tab.html", []).run(["$templateCache", function ($templateCache) {
    $templateCache.put("template/tabs/tab.html",
      "<li ng-click=\"select()\" class=\"pointer-cursor\" ng-class=\"{active: active, disabled: disabled}\">\n" +
      "  <a href tab-heading-transclude>{{heading}}</a>\n" +
      "</li>\n" +
      "");
}]);