﻿/*
--------------------------------------------------------------------------------

    ExceptionManager.cs

    Copyright © Pentair Water Pool and Spa, Inc. 2014

--------------------------------------------------------------------------------
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Data.SqlClient;
using System.Data.Entity.Validation;

namespace SERVICE_SHARED
{
    public static class ExceptionManager
    {
        /// <summary>
        /// Customizes the exception.
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        public static ExceptionPentair CustomizeException(Exception e)
        {
            if (e is AggregateException)
            {
                AggregateException eReal = (e as AggregateException).Flatten();
                eReal.Data["OriginalStackTrace"] = e.StackTrace; // Flatten() doesn't preserve the e.StackTrace
                return new ExceptionPentair(eReal);
            }

            if (e is ArgumentNullException)
            {
                ArgumentNullException eReal = e as ArgumentNullException;

                return new ExceptionPentair(
                    String.Format("Parameter '{0}' cannot be null",
                        eReal.ParamName
                    ),
                    eReal
                );
            }

            if (e is ArgumentOutOfRangeException)
            {
                ArgumentOutOfRangeException eReal = e as ArgumentOutOfRangeException;

                return new ExceptionPentair(
                    String.Format("Parameter '{0}' value is out of range{1}",
                        eReal.ParamName,
                        (eReal.ActualValue != null) ? ": '" + eReal.ActualValue.ToString() + "'" : "."
                    ),
                    eReal
                );
            }

            if (e is ObjectDisposedException)
            {
                ObjectDisposedException eReal = e as ObjectDisposedException;

                return new ExceptionPentair(
                    String.Format("Object already disposed: '{0}'",
                        eReal.ObjectName
                    ),
                    eReal
                );
            }

            if (e is SocketException)
            {
                SocketException eReal = e as SocketException;

                return new ExceptionPentair(
                    String.Format("Socket error #{0}: {1}",
                        eReal.ErrorCode.ToString(),
                        eReal.Message
                    ),
                    eReal
                );
            }

            if (e is SqlException)
            {
                SqlException eReal = e as SqlException;

                StringBuilder errorMessage = new StringBuilder();
                foreach (SqlError errSQL in eReal.Errors)
                {
                    errorMessage.AppendLine(errSQL.Number + " - " + errSQL.Message);
                }

                return new ExceptionPentair(
                    String.Format("SQL error #{0}: {1}",
                        eReal.ErrorCode.ToString(),
                        errorMessage.ToString()
                    ),
                    eReal
                );
            }

            // 2017-11-02
            if (e is DbEntityValidationException)
            {
                DbEntityValidationException eReal = e as DbEntityValidationException;

                StringBuilder errorMessage = new StringBuilder();
                foreach (DbEntityValidationResult errEF in eReal.EntityValidationErrors)
                {
                    foreach(DbValidationError errDB in errEF.ValidationErrors)
                    {
                        errorMessage.AppendFormat("Invalid Parameter: {0}, Error: {1}\n",
                            errDB.PropertyName,
                            errDB.ErrorMessage
                        );
                    }
                }

                return new ExceptionPentair(
                    String.Format("Data validation failed:\n{0}",
                        errorMessage.ToString()
                    ),
                    eReal
                );
            }

            // None of above: process as a generic Exception
            return new ExceptionPentair(e);

        } // CustomizeException()

    } // class ExceptionManager
}
