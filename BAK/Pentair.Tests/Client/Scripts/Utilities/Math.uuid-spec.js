﻿describe('Math UUID', function () {

    it('should generate a random Guid of size 8 with no dashes', function () {
        var uuid = Math.uuid(8);
        expect(uuid.length).toEqual(8);
        expect(uuid.match(/-/g)).toEqual(null);

    });
    it('should generate a random Guid of size 8 with 4 dashes', function () {
        var uuid = Math.uuid();
        expect(uuid.length).toEqual(36);
        expect(uuid.match(/-/g).length).toEqual(4);

    });
    it('should generate a random Guid of size 36 with 4 dashes', function () {
        var uuid = Math.uuidFast();
        expect(uuid.length).toEqual(36);
        expect(uuid.match(/-/g).length).toEqual(4);

    });

    it('should generate a random Guid of size 36 with 4 dashes', function () {
        var uuid = Math.uuidCompact();
        expect(uuid.length).toEqual(36);
        expect(uuid.match(/-/g).length).toEqual(4);
    });
});