'use strict';
var PWC = PWC || {};
PWC.controller('PumpsController', [
    '$scope', '$location', '$filter', 'PropertyService', 'PropertyModel','Enumerations',
    function ($scope, $location, $filter, propertyService, propertyModel,enumerations) {
        $scope.property = propertyModel;
        $scope.propertyService = propertyService;
        $scope.pumps = propertyModel.Pumps;
        $scope.pumpAffects = [];

        linkPumpAffects();

        // inputs: pump, circuit
        // outputs: 'inactive', 'active', or 'up-next'
        $scope.getCircuitPriority = function (pump, pumpAffect) {

            if (pump.SUBTYP === 'FLOW' && pumpAffect.isPrimingAffect && pump.CIRCUIT === '00000') {
                var isPriming;
                angular.forEach(pump.pumpAffects, function (pA) {
                    var affectingCircuit = propertyModel.Objects[pA.CIRCUIT];
                    if (affectingCircuit && affectingCircuit.STATUS == 'ON') {
                        isPriming = true;
                    }
                });
                if (isPriming) {
                    return 'active'
                }
            }

            if (!pumpAffect) {
                return 'inactive';
            }

            if (pump.SUBTYP === 'SINGLE' || pump.SUBTYP === 'DUAL') {
                if (pump.CIRCUIT === pumpAffect.CIRCUIT) {
                    return 'active';
                }
            }

            var affectingCircuit = propertyModel.Objects[pumpAffect.CIRCUIT];
            if (pump.SUBTYP === 'SPEED' || pump.SUBTYP === 'FLOW') {
                if (pump.CIRCUIT === pumpAffect.CIRCUIT) {
                    return 'active';
                }

                if (affectingCircuit && (enumerations.bodyOnStatusList.indexOf(affectingCircuit.STATUS) >= 0)) {
                    return 'up-next';
                }
            }
            return 'inactive';
        };

        $scope.specialCircuit = function (CIRCUIT) {    
            var status = enumerations.isCircuitSpecial(CIRCUIT)
            if (CIRCUIT === "X0052" || CIRCUIT ==="X0046") { // STATUS FOR SOLAR SPECIAL CIRCUIT, OR FREEZE CIRCUIT
                if (propertyModel.Objects[CIRCUIT]) {
                    if (
                        (propertyModel.Objects[CIRCUIT].STATUS === "ON") ||
                        (propertyModel.Objects[CIRCUIT].STATUS === "OFF")           // BOTH STATUS 'ON' OR 'OFF'
                        )
                        status = true;
                    else
                        status = false;                   
                }
            }
            return status
        };

        // if this circtuit priming affect 
        $scope.isCircuitPrimingAffect = function (pumpAffect) {
            var status = false
            if (pumpAffect && pumpAffect.isPrimingAffect)
                status = pumpAffect.isPrimingAffect
            return status
        };

        

        // TO FIX LEGACY CIRCUIT PUMP NAME BUG: 204 
        $scope.getCircuitPumpName = function (circuit_name) {
            if ((circuit_name === "X0048") || (circuit_name === "X0-148"))
                return "Solar";
            else
                return circuit_name;
        }



        $scope.setUnitsPumpAffect = function (pumpAffect) {
            console.log("------set current units---- ");
            switch(pumpAffect.SELECT){
                case  'RPM':            
                    pumpAffect.SPEED = 1200;
                    pumpAffect.SELECT = 'RPM';
                    console.log("------RPM NEW VALUE :" , pumpAffect.SPEED);                    
                    $scope.propertyService.SetValue(pumpAffect.OBJNAM, "SELECT", "RPM");    // setting current type
                    break;
                case 'GPM':
                    pumpAffect.SPEED = 30;
                    pumpAffect.SELECT = 'GPM';
                    console.log("------GPM NEW VALUE :" , pumpAffect.SPEED);
                    $scope.propertyService.SetValue(pumpAffect.OBJNAM, "SELECT", "GPM");    // setting current type
                    break;
            }

            $scope.propertyService.SetValue(pumpAffect.OBJNAM, "SPEED", pumpAffect.SPEED);

        };


        $scope.changeUnits = function (uoM, pumpAffect) {
            console.log("------changeUnits---- :", uoM);
      
            if (uoM === 'RPM') {
                pumpAffect.SPEED = 1000;
                pumpAffect.SELECT = 'RPM';
                console.log("------RPM NEW VALUE :" , pumpAffect.SPEED);
            } else if (uoM === 'GPM') {
                pumpAffect.SPEED = 30;
                pumpAffect.SELECT = 'GPM';
                console.log("------GPM NEW VALUE :" + pumpAffect.SPEED);
            }
            
            $scope.propertyService.SetValue(pumpAffect.OBJNAM, "SPEED", pumpAffect.SPEED);

        };

        $scope.toggleAffectedCircuit = function (circuit) {                             
                if (propertyModel.Objects[circuit].STATUS) {
                    if (propertyModel.Objects[circuit].STATUS === "ON") {
                        propertyModel.Objects[circuit].STATUS = "OFF"
                        $scope.propertyService.SetValue(circuit, "STATUS", "OFF");
                    } else {
                        propertyModel.Objects[circuit].STATUS = "ON"
                        $scope.propertyService.SetValue(circuit, "STATUS", "ON");
                    }
                }
        };

        // DO  NOT TOGGLE JUST SET THE VALUE FROM CONTROL
        $scope.setAffectedCircuit = function (circuit, status) {
            if (propertyModel.Objects[circuit].STATUS) {
                if (propertyModel.Objects[circuit].STATUS === "ON") {
                    propertyModel.Objects[circuit].STATUS = "ON"
                    $scope.propertyService.SetValue(circuit, "STATUS", "ON");
                } else {
                    propertyModel.Objects[circuit].STATUS = "OFF"
                    $scope.propertyService.SetValue(circuit, "STATUS", "OFF");
                }
            }
        };


        function linkPumpAffects() {
            // filter the parent of pumpAffects to be that of the selectedPump objnam
            angular.forEach($scope.pumps, function (pump) {

                // convert min and max to ints for the stepper
                pump.MIN = parseInt(pump.MIN);
                pump.MAX = parseInt(pump.MAX);

                pump.pumpAffects = propertyModel.PumpAffects.filter(function (PumpAffect) {
                    PumpAffect.isPrimingAffect = false;
                    if (PumpAffect.LISTORD === '1' && PumpAffect.STATIC === 'ON') {
                        PumpAffect.SNAME = 'Priming';
                        PumpAffect.isPrimingAffect = true;
                    }
                    return PumpAffect.PARENT === pump.OBJNAM;
                });

                //Manually add priming affect for VF pump
                if (pump.SUBTYP == 'FLOW') {
                    pump.pumpAffects.push({SNAME: 'Priming', SPEED: pump.PRIMFLO, LISTORD: 9999,isPrimingAffect:true, SELECT:'GPM'})
                }
            });
        }

    }
]);
