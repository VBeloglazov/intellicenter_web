-- =============================================================
--
-- Author:		Vladimir Beloglazov
-- Create date: 04/17/2018
--
-- Creates:
--
--		Table Type:			None
--
--		Stored Procedure:	PUpdateInstallationLastHistory
--
-- =============================================================

USE PathfinderIntelliCenter
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (
	SELECT * FROM sys.objects
	WHERE object_id = OBJECT_ID(N'dbo.PUpdateInstallationLastHistory')
	AND type in (N'P', N'PC')
)
DROP PROCEDURE dbo.PUpdateInstallationLastHistory
GO

-- -------------------------------------------------------------
--
-- Updates [Installations].Name.
--
-- -------------------------------------------------------------
CREATE PROCEDURE dbo.PUpdateInstallationLastHistory
	 @installationId	AS int
	,@lastHistory		AS bigint

AS
BEGIN

	SET NOCOUNT OFF

	UPDATE dbo.Installations
	SET  LastHistory = @lastHistory
	WHERE InstallationId = @installationId

END
GO
