﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using Pentair.Desktop.Simulator.BusinessLogic;
using Pentair.Domain.Client.Entity;
using Pentair.Domain.Client.Helpers;

namespace Pentair.Desktop.Simulator.Models
{
    public class DataModel : INotifyPropertyChanged, IDataModel
    {
        /// <summary>
        /// Property Changed event handler
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        public int PropertyId { get; private set; }

        /// <summary>
        /// Collection of data object entries
        /// </summary>
        public DataTable Data
        {
            get { return _data; }
        }

        public MessageProcessor Sockets { get; private set; }

        //public List<string> Columns
        //{
        //    get { return _columns; }
        //}

        /// <summary>
        /// View Groupings
        /// </summary>
        private readonly DataTable _data;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="data"></param>
        public DataModel(IEnumerable<object> data, MessageProcessor p)
        {
            _data = new DataTable();
            Sockets = p;
            _data.Columns.Add(new DataColumn(StaticData.ObjectName));
            _data.PrimaryKey = new DataColumn[] {_data.Columns[StaticData.ObjectName]};
            BuildCollection(data);
        }

        private void BuildCollection(IEnumerable<object> data)
        {
            var d = data.ToList();
            foreach (var c in d.OfType<ControlObject>())
            {
                if (_data.Columns.Contains(c.Key))
                {
                    continue;
                }
                _data.Columns.Add(c.Key);
            }

            foreach (var o in d.OfType<ControlObject>())
            {
                var r = _data.Rows.Find(o.OBJNAM);
                if (null == r)
                {
                    r = _data.Rows.Add(o.OBJNAM);
                }
                r[o.Key] = o.Value;
            }
        }

        public void RebuildData(IEnumerable<object> data, int id)
        {
            PropertyId = id;
            _data.Rows.Clear();
            BuildCollection(data);
        }

        /// <summary>
        /// Rebuild the data collection
        /// </summary>
        /// <param name="data">new data</param>
        public void SwapData(IEnumerable<object> data)
        {
            var d = data.ToList();
            var changes = new List<string>();

            foreach (var c in d.OfType<ControlObject>())
            {
                if (_data.Columns.Contains(c.Key))
                {
                    continue;
                }
                _data.Columns.Add(c.Key);
            }

            foreach (var o in d.OfType<ControlObject>())
            {
                var r = _data.Rows.Find(o.OBJNAM);
                if (null == r)
                {
                    r = _data.Rows.Add(o.OBJNAM);
                    if (!changes.Contains(o.OBJNAM))
                    {
                        changes.Add(o.OBJNAM);
                    }
                }
                if (r[o.Key].ToString() != o.Value)
                {
                    r[o.Key] = o.Value;
                    if (!changes.Contains(o.OBJNAM))
                    {
                        changes.Add(o.OBJNAM);
                    }
                }
            }
            if (null != PropertyChanged)
            {
                foreach (var c in changes)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs(c));
                }
            }
        }
    }
}
