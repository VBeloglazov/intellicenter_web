﻿'use strict';
describe('ChangeEmailController', function () {
    var AppService;
    var $location;
    var $scope;
    var InstallationsGridController;
    var $controller;
    var propertyService;
    var $q;
    var deferred;
    beforeEach(module('PentairWebClient'));

    beforeEach(inject(function (_$controller_, _$rootScope_, _$location_, _PropertyService_, _AppService_, _$q_) {
        $scope = _$rootScope_.$new();
        $location = _$location_;
        $controller = _$controller_;
        propertyService = _PropertyService_;
        AppService = _AppService_;

        $q = _$q_;
        InstallationsGridController = $controller("InstallationsGridController", { $scope: $scope });
    }));

    describe('Default Vars', function () {
        it('should have the following default variables', function () {
            expect($scope.SearchTerm).toBe("");
            expect($scope.gridOptions.useExternalSorting).toBe(true);
            expect($scope.gridOptions.enableHorizontalScrollbar).toBe(false);
            expect($scope.gridOptions.enableVerticalScrollbar).toBe(false);
            expect($scope.gridOptions.pagingPageSizes).toEqual([20, 50, 75]);
            expect($scope.gridOptions.pagingPageSize).toBe(20);
            expect($scope.gridOptions.useExternalPaging).toBe(true);
            expect($scope.gridOptions.enableColumnMenus).toBe(false);
            expect($scope.gridOptions.onRegisterApi).toEqual(jasmine.any(Function));
            expect($scope.gridOptions.columnDefs).toEqual([
              { name: 'Pool Name', field: "PoolName", cellTemplate: '<a href="" ng-click="getExternalScopes().connect(row.entity)">{{COL_FIELD}}</a>' },
              { name: 'Address', field: "Address", enableSorting: false },
              { name: 'City/State', field: "Location" },
              { name: 'Phone Number', field: "Phone", enableSorting: false }
            ]);
            expect($scope.gridOptions.columnDefs[0].name).toBe('Pool Name');

        });
    });

    describe('ClearSearchTerm', function () {
        var result = { headers: jasmine.any(Function) }
        var fetchInstallationsDeferred;
        beforeEach(function () {
            fetchInstallationsDeferred = $q.defer();

            $scope.SearchTerm = "NotEmpty";
            spyOn(result, 'headers').and.returnValue('{"TotalCount":745}');
            spyOn(AppService, "Installations").and.returnValue(fetchInstallationsDeferred.promise);
        });
        it('should clear search term and refresh installations', function () {
            $scope.gridOptions.pagingCurrentPage = 2;
            $scope.ClearSearchTerm();
            fetchInstallationsDeferred.resolve(result);

            expect($scope.SearchTerm).toBe("");
            // expect($scope.gridOptions.totalItems).toBe(745);
            // expect($scope.gridOptions.pagingCurrentPage).toEqual(0);

            expect(AppService.Installations).toHaveBeenCalledWith(20, 0, "");
        });
    });




});
