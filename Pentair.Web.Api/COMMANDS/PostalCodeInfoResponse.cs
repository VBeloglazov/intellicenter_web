﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Pentair.Web.Api
{
    public class PostalCodeInfoResponse : ResponseCommand
    {
        [JsonProperty("objectList", Required = Required.Default)]
        public List<PostalCodeInfoItem> ObjectList { get; private set; }

        public PostalCodeInfoResponse(
            string messageID,
            string response,
            List<PostalCodeInfoItem> objectList
            )
            : base(CommandString.WritePostalCodeInfoMessage, messageID, response)
        {
            this.MessageID = messageID;
            this.ObjectList = objectList;
        } // PostalCodeInfoResponse()

        /// <summary>
        /// Always returns an object, valid or not. If invalid, check the DeserializationError property value.
        /// </summary>
        public static new PostalCodeInfoResponse Deserialize(
            string serializedMessage)
        {
            PostalCodeInfoResponse command = null;

            try
            {
                command = JsonConvert.DeserializeObject<PostalCodeInfoResponse>(serializedMessage);
                command.IsValid = true;
            }
            catch (Exception e)
            {
                // Deserialization as PostalCodeInfoResponse failed

                StringBuilder sbError = new StringBuilder();

                sbError.Append("Cannot deserialize as PostalCodeInfoResponse. " + e.Message);

                // Try to get at least the base message data
                ResponseCommand baseCommand = ResponseCommand.Deserialize(serializedMessage);

                if (!baseCommand.IsValid)
                {
                    sbError.Append("\n" + baseCommand.DeserializationError);
                }

                command = new PostalCodeInfoResponse(
                    messageID: baseCommand.MessageID,
                    response: ResponseString.BadRequest,
                    objectList: null)
                {
                    IsValid = false,
                    DeserializationError = sbError.ToString()
                };
            }

            return command;

        } // Deserialize()

    } // class PostalCodeInfoResponse
}
