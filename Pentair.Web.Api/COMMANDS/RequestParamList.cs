﻿using System.Collections.Generic;

namespace Pentair.Web.Api
{
    public class RequestParamList : KeyCommand
    {
        public RequestParamList(
            string messageID,
            string condition,
            List<KeyObject> objectList
            )
            : base(CommandString.RequestParamList, messageID, condition, objectList)
        {
        }

    } // class RequestParamList
}