﻿using System;
using System.Threading;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using Pentair.Desktop.Simulator.BusinessLogic;
using Pentair.Desktop.Simulator.Models;
using Timer = System.Timers.Timer;

namespace Pentair.Desktop.Simulator.Views
{
    public enum LogId
    {
        Server,
        Client,
        Discovery
    }

    /// <summary>
    /// Interaction logic for LogView.xaml
    /// </summary>
    public partial class LogView : UserControl
    {
        #region Events and Delegates
        internal event ClearLogItems OnClearLogItems;
        internal delegate void ClearLogItems(LogId log);
        #endregion

        #region Public Properties
        public LogId LogIdentifier { get; set; }
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public LogView()
        {
            LogIdentifier = LogId.Client;
            InitializeComponent();
            DataContext = new LogModel();
        }
        #endregion

        #region Private Methods

        void DisplayItem(LogEntry item)
        {
            var msg = item.Message;
            if (null != msg)
            {
                var data = new MessageDetailModel(msg);
                if (data.Valid)
                {
                    MessageDetailView.MessageDetail.Items.Clear();
                    foreach (var entry in data.Messages)
                    {
                        MessageDetailView.MessageDetail.Items.Add(entry);
                    }
                    var gv = MessageDetailView.MessageDetail.View as GridView;
                    foreach (var col in gv.Columns)
                    {
                        col.Width = ActualWidth;
                        col.Width = Double.NaN;
                    }
                    MessageDetailView.MessageDetailPopup.IsOpen = true;
                }
            }

        }
        #endregion

        #region Event Handlers
        private void OnPreviewMouseClick(object sender, MouseEventArgs e)
        {
            MessageDetailView.StopTimer();
            MessageDetailView.MessageDetailPopup.IsOpen = false;
            var item = e.Source as TextBlock;
            if (null != item)
            {
                DisplayItem((LogEntry)(item.DataContext));
            }
        }

        private void OnClearContents(object sender, RoutedEventArgs e)
        {
            if (null != OnClearLogItems)
            {
                OnClearLogItems(LogIdentifier);
            }
        }

        private void OnHideDetail(object sender, MouseEventArgs e)
        {
            if (MessageDetailView.MessageDetailPopup.IsOpen)
            {
                MessageDetailView.StartTimer();
            }
        }

        private void OnShowDetail(object sender, MouseEventArgs e)
        {
            MessageDetailView.StopTimer();
            MessageDetailView.MessageDetailPopup.IsOpen = false;
            var item = e.Source as ListViewItem;
            if (null != item)
            {
                DisplayItem((LogEntry)(item.Content));
            }
        }
        #endregion
    }
}
