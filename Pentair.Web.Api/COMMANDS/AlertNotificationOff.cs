﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;

using Newtonsoft.Json;

namespace Pentair.Web.Api
{
    public class AlertNotificationOff : AlertNotificationCommand
    {
        public AlertNotificationOff(
            string messageID,
            int category,
            string objName,
            string objType,
            string parent,
            string alert,
            long timeStamp
            )
            : base(CommandString.AlertOff, messageID, category, objName, objType, parent, alert, timeStamp)
        {
        } // AlertNotificationOff()

        /// <summary>
        /// Always returns an object, valid or not. If invalid, check the DeserializationError property value.
        /// </summary>
        public static new AlertNotificationOff Deserialize(
            string serializedMessage)
        {
            AlertNotificationOff command = null;

            try
            {
                command = JsonConvert.DeserializeObject<AlertNotificationOff>(serializedMessage);
                command.IsValid = true;
            }
            catch (Exception e)
            {
                // Deserialization as AlertNotificationOff failed

                StringBuilder sbError = new StringBuilder();

                sbError.Append("Cannot deserialize as AlertNotificationOff. " + e.Message);

                // Try to get at least the base message data
                AlertNotificationCommand commandBase = AlertNotificationCommand.Deserialize(serializedMessage);

                if (!commandBase.IsValid)
                {
                    sbError.Append("\n" + commandBase.DeserializationError);
                }

                command = new AlertNotificationOff(
                    messageID: commandBase.MessageID,
                    category: (int)commandBase.Mode,
                    objName: commandBase.ObjName,
                    objType: commandBase.ObjType,
                    parent: commandBase.Parent,
                    alert: commandBase.Alert,
                    timeStamp: 0L)
                {
                    IsValid = false,
                    DeserializationError = sbError.ToString()
                };
            }

            return command;

        } // Deserialize()

    } // class AlertNotificationOff
}