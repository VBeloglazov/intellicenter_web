﻿'use strict';
var PWC = PWC || {};

PWC.factory('AuthInterceptorService', ['HOST_NAME', '$q', '$window', '$injector',
    function (HOST_NAME, $q, $window, $injector) {

        var AuthInterceptorServiceFactory = {};

        //Append Bearer token to EVERY Request
        var _request = function (config) {
            var token = $window.sessionStorage.token;


            config.headers = config.headers || {};
            if (token && config.url.indexOf(HOST_NAME) > -1)
                config.headers.Authorization = 'Bearer ' + token;
            return config;
        }

        //Logout on 401
        var _responseError = function (rejection) {
            if (rejection.status === 401) {
                var AuthService = $injector.get('AuthService');
                AuthService.logOut();
            }
            return $q.reject(rejection);
        }

        AuthInterceptorServiceFactory.request = _request;
        AuthInterceptorServiceFactory.responseError = _responseError;

        return AuthInterceptorServiceFactory;
    }]);