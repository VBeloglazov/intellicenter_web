﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Pentair.Domain.Client.Helpers.Commands
{
    public class ExtendedParamListResponse : ResponseCommandWithTwoTimespans
    {
        [JsonProperty("objectList", Required = Required.Default)]
        public List<ExtendedParamObject> ObjectList { get; private set; }

        public ExtendedParamListResponse(
            string command,
            string messageID,
            string response,
            string timeNow,
            string timeSince,
            List<ExtendedParamObject> objectList
            )
            : base(command, messageID, response, timeNow, timeSince)
        {
            this.ObjectList = objectList;

        } // ExtendedParamListResponse()


        /// <summary>
        /// Always returns an object, valid or not. If invalid, check the DeserializationError property value.
        /// </summary>
        public static new ExtendedParamListResponse Deserialize(
            string serializedMessage)
        {
            ExtendedParamListResponse command = null;

            try
            {
                command = JsonConvert.DeserializeObject<ExtendedParamListResponse>(serializedMessage);
                command.IsValid = true;
            }
            catch (Exception e)
            {
                // Deserialization as ExtendedParamListResponse failed

                StringBuilder sbError = new StringBuilder();

                sbError.Append("Cannot deserialize as ExtendedParamListResponse. " + e.Message);

                // Try to get at least the base message data
                ResponseCommandWithTwoTimespans baseCommand = ResponseCommandWithTwoTimespans.Deserialize(serializedMessage);

                if (!baseCommand.IsValid)
                {
                    sbError.Append("\n" + baseCommand.DeserializationError);
                }

                command = new ExtendedParamListResponse(
                    command: baseCommand.Command,
                    messageID: baseCommand.MessageID,
                    response: ResponseString.BadRequest,
                    timeNow: String.Empty,
                    timeSince: String.Empty,
                    objectList: null)
                {
                    IsValid = false,
                    DeserializationError = sbError.ToString()
                };
            }

            return command;

        } // Deserialize()

    } // class ExtendedParamListResponse
}
