﻿var PWC = PWC || {};
PWC.directive('tabInclude',['$compile','SocketService', 'PropertyService', function ($compile,SocketService, PropertyService) {
    'use strict';

    function postLink(scope, iElement, iAttrs, controller) {
        iElement.isolateScope().$watch('active',
            function (newValue) {
                // AC 6-1-17 For some of the tabs with the select attribute, this is now part of the confirm modal.
                // Currently, this applies to the tabs on the system configuration page.
                if (!iAttrs.select) {
                  if (newValue) {
                    SocketService.clearSubscriptions();
                    PropertyService.SubscribeToPage(iAttrs.page).then(function () {
                      scope.$include = iAttrs.tabInclude;
                    });
                  }
                } else {
                  scope.$include = iAttrs.tabInclude;
                }
            }
        );
    }

    return {
        require: ['^tab', '^tabset'],
        restrict: 'A',
        priority: 1,
        compile: function (tElement, tAttrs, transclude) {
            var elem = angular.element('<div></div>');
            elem.attr('ng-include', '$include');

            if (tAttrs.tabOnload) {
                elem.attr('onload', tAttrs.tabOnload);
            }
            if (tAttrs.tabAutoscroll) {
                elem.attr('autoscroll', tAttrs.autoscroll);
            }
            tElement.append(elem);
            return postLink;
        }
    };
}]);

