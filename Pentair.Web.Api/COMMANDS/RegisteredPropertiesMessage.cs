﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Pentair.Web.Api
{
    public class RegisteredPropertiesMessage : CommandBase
    {
        [JsonProperty("userID", Required = Required.Always)]
        public string UserID { get; private set; }

        [JsonProperty("password", Required = Required.Always)]
        public string Password { get; private set; }

        public RegisteredPropertiesMessage(
            string command,
            string messageID,
            string userID,
            string password
            )
            : base(command, messageID)
        {
            this.UserID = userID;
            this.Password = password;

        } // RegisteredPropertiesMessage()

        /// <summary>
        /// Always returns an object, valid or not. If invalid, check the DeserializationError property value.
        /// </summary>
        public static new RegisteredPropertiesMessage Deserialize(
            string serializedMessage)
        {
            RegisteredPropertiesMessage command = null;

            try
            {
                command = JsonConvert.DeserializeObject<RegisteredPropertiesMessage>(serializedMessage);
                command.IsValid = true;
            }
            catch (Exception e)
            {
                // Deserialization as RegisteredPropertiesMessage failed

                StringBuilder sbError = new StringBuilder();

                sbError.Append("Cannot deserialize as RegisteredPropertiesMessage. " + e.Message);

                // Try to get at least the base message data
                CommandBase baseCommand = CommandBase.Deserialize(serializedMessage);

                if (!baseCommand.IsValid)
                {
                    sbError.Append("\n" + baseCommand.DeserializationError);
                }

                command = new RegisteredPropertiesMessage(
                    command: baseCommand.Command,
                    messageID: baseCommand.MessageID,
                    userID: String.Empty,
                    password: String.Empty)
                {
                    IsValid = false,
                    DeserializationError = sbError.ToString()
                };
            }

            return command;

        } // Deserialize()

    } // class RegisteredPropertiesMessage
}
