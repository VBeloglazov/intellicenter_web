﻿using System;
using Pentair.Desktop.Simulator.BusinessLogic;
using SuperWebSocket;

namespace Pentair.Desktop.Simulator.Services
{
    public class LocalClient : WebSocketSession<LocalClient>
    {
        #region Public Properties
        public MessageProcessor MsgProcessor { get; set; }
        public string Id { get { return SessionID; } }
        #endregion

        #region Events and Delegates
        public event ClientSocket.LogMessage LogMessageEvent;
        public delegate void LogMessage(string message, bool request);
        #endregion

        #region Public Methods
        /// <summary>
        /// Process the message received on this session.
        /// </summary>
        /// <param name="message"></param>
        public void ProcessMessage(string message)
        {
            if (message.Contains("ping"))
            {
                Send("pong");
            }
            else
            {
                if (null != LogMessageEvent)
                {
                    LogMessageEvent(message, true);
                }

                string response = MsgProcessor.ProcessMessage(message, Guid.Empty);


                if (null != response)
                {
                    Send(response);

                    if (null != LogMessageEvent)
                    {
                        LogMessageEvent(response, false);
                    }
                }
            }
        }

        /// <summary>
        /// A configuration change has been made, notify the client
        /// </summary>
        /// <param name="sender">MessageProcessor</param>
        /// <param name="e">WriteParam message</param>
        public void ConfigurationUpdate(object sender, string e)
        {
            var logMsg = e;
            try
            {
                Send(e);
            }
            catch (Exception ex)
            {
                logMsg = string.Format("Error: {0}, Original Message: {1}", ex.Message, e);
            }
            finally
            {
                if (null != LogMessageEvent)
                {
                    LogMessageEvent(logMsg, false);
                }
            }
        }

        #endregion
    }
}
