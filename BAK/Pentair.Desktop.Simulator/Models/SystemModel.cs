﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Pentair.Desktop.Simulator.BusinessLogic;
using Pentair.Domain.Client.Entity;

namespace Pentair.Desktop.Simulator.Models
{
    public class SystemModel : ISystemModel
    {
        #region Public Properties

        public Property Property { get; set; }
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public SystemModel()
        {
            Property = null;
        }
        #endregion
    }
}
