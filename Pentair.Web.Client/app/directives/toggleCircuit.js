'use strict';
var PWC = PWC || {};


PWC.directive('toggleCircuit', function () {
    return {
        restrict: 'EA',
        replace: true,
        scope: {
            targetCircuit: '=',
            disabled: '@',
            toggleOnclick: '&'
        },
        template: '<div class="switch-toggle"><button class="btn {{value | lowercase}}">{{value}}</button></div>',
        link: function (scope, element, attrs) {
            if (PWC.isEmptyObject(scope.targetCircuit))
                return;

            if (!attrs.disabled) {
                attrs.disabled = false;
            }
            scope.value = getClassName();

            element.on('click', function (e) {
                e.stopPropagation();
                if (!scope.disabled) {
                    var newValue = getNewValue();
                    var params = { obj: scope.targetCircuit, value: newValue };
                    scope.toggleOnclick(params);
                }
            });

            //Upddate UI Value when model Changes
            scope.$watch('targetCircuit.STATUS', function (newVal, oldVal) {
                if (newVal && newVal !== oldVal)
                    scope.value = getClassName();
            });


            function getClassName() {
                return (scope.targetCircuit.STATUS === 'ON' || scope.targetCircuit.STATUS === 'DLYOFF') ? "ON" : "OFF";
            }

            function getNewValue() {
                return scope.value === "OFF" ? 'ON' : 'OFF';
            }

        }
    };
});

