﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Pentair.Domain.Client.Helpers.Commands
{
    public class ErrorResponse : ResponseCommand
    {
        [JsonProperty("description", Required = Required.Always)]
        public string Description { get; private set; }

        public ErrorResponse(
            string messageID,
            string response,
            string description
            )
            : base(CommandString.ErrorMessage, messageID, response)
        {
            this.Description = description;

        } // ErrorResponse()

        /// <summary>
        /// Always returns an object, valid or not. If invalid, check the DeserializationError property value.
        /// </summary>
        public static new ErrorResponse Deserialize(
            string serializedMessage)
        {
            ErrorResponse command = null;

            try
            {
                command = JsonConvert.DeserializeObject<ErrorResponse>(serializedMessage);
                command.IsValid = true;
            }
            catch (Exception e)
            {
                // Deserialization as ErrorResponse failed

                StringBuilder sbError = new StringBuilder();

                sbError.Append("Cannot deserialize as ErrorResponse. " + e.Message);

                // Try to get at least the base message data
                ResponseCommand baseCommand = ResponseCommand.Deserialize(serializedMessage);

                if (!baseCommand.IsValid)
                {
                    sbError.Append("\n" + baseCommand.DeserializationError);
                }

                command = new ErrorResponse(
                    messageID: baseCommand.Command,
                    response: ResponseString.BadRequest,
                    description: String.Empty)
                {
                    IsValid = false,
                    DeserializationError = sbError.ToString()
                };
            }

            return command;

        } // Deserialize()

    } // class ErrorResponse
}
