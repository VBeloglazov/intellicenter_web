﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Pentair.Web.Api
{
    public class SetCommand : CommandBase
    {
        [JsonProperty("method")]
        public string Method { get; private set; }

        [JsonProperty("arguments")]
        public object Arguments { get; private set; }

        public SetCommand(
            string messageID,
            string method,
            object arguments
            )
            : base(CommandString.SetCommand, messageID)
        {
            this.Method = method;
            this.Arguments = arguments;
        }

    } // class SetCommand
}
