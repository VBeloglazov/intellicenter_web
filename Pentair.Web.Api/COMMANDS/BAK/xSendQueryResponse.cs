﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Pentair.Domain.Client.Helpers.Commands
{
    // TODO: 2b removed, not in use.
    /*
    public class SendQueryResponse : ResponseCommand
    {
        [JsonProperty("answer", Required = Required.Always)]
        public object Answer { get; private set; }

        public SendQueryResponse(
            string command,
            string messageID,
            string response,
            object answer
            )
            : base(command, messageID, response)
        {
            this.Answer = answer;

        } // SendQueryResponse()

        /// <summary>
        /// Always returns an object, valid or not. If invalid, check the DeserializationError property value.
        /// </summary>
        public static new SendQueryResponse Deserialize(
            string serializedMessage)
        {
            SendQueryResponse command = null;

            try
            {
                command = JsonConvert.DeserializeObject<SendQueryResponse>(serializedMessage);
                command.IsValid = true;
            }
            catch (Exception e)
            {
                // Deserialization as SendQueryResponse failed

                StringBuilder sbError = new StringBuilder();

                sbError.Append("Cannot deserialize as SendQueryResponse. " + e.Message);

                // Try to get at least the base message data
                ResponseCommand baseCommand = ResponseCommand.Deserialize(serializedMessage);

                if (!baseCommand.IsValid)
                {
                    sbError.Append("\n" + baseCommand.DeserializationError);
                }

                command = new SendQueryResponse(
                    command: baseCommand.Command,
                    messageID: baseCommand.MessageID,
                    response: ResponseString.BadRequest,
                    answer: String.Empty)
                {
                    IsValid = false,
                    DeserializationError = sbError.ToString()
                };
            }

            return command;

        } // Deserialize()

    } // class SendQueryResponse
    */
}
