'use strict';
var PWC = PWC || {};
PWC.controller('VacationController', [
  '$scope', '$location', '$filter', 'PropertyService', 'AuthService', 'Enumerations', 'PropertyModel', 'Notification',
  function ($scope, $location, $filter, propertyService, authService, enumerations, propertyModel, Notification) {
    var stopTimePristine = true;
    $scope.InitialTimeFormat = getTimeFormat;
    $scope.Model = propertyModel;
    $scope.systemSettings = $scope.Model.Objects._5451;
    $scope.Property = propertyService;
    $scope.Authentication = authService;
    $scope.Enumerations = enumerations;
    $scope.ProgrammableCircuits = [];
    $scope.HeatModes = [];
    $scope.Schedules = [];
    $scope.SelectedSchedule = null;
    $scope.UoM = 'C';
    $scope.minTemp = 5;
    $scope.maxTemp = 40;
    $scope.maxSchedules = 100;
    $scope.ConfirmDelete = false;
    $scope.isVacationTab = true;
    $scope.timeModes = enumerations.timeModes;

    init();

    $scope.open = function (type) {
        if (type === 'start')
            $scope.start.status.opened = true;
        else
            $scope.stop.status.opened = true;
    };

      // $scope.status = {
      //   opened: false
      // };

    $scope.start = {
        status: {
            opened: false
        }
    };

    $scope.stop = {
        status: {
            opened: false
        }
    };

    $scope.getSchedControlID = function (id, index) {
        var value = 1;
        switch (id) {
            case 'OCCURANCE': value = 3000 + index; break;
            case 'ONETIME': value = 3100 + index; break;
            case 'REPEAT': value = 3200 + index; break;
            case 'STARTTIME': value = 3300 + index; break;
            case 'STOPTIME': value = 3400 + index; break;
            case 'DONTSTOP': value = 3500 + index; break;
            case 'DAYONCE': value = 3600 + index; break;
            case 'DAYREPEATS': value = 3600 + index; break;

            default:
                value = 1;

        }
        return value;
    }

    // return how many schedules are free 
    $scope.getSchedulesFree = function () {
        var free = $scope.maxSchedules - $scope.Schedules.length;
        return free;
    };
      // check can add more schedules
    $scope.canAddSchedules = function () {
        var free = $scope.maxSchedules - $scope.Schedules.length;
        if (free > 0)
            return true;
        else
            return false;
    };

    $scope.minStartDate = moment().toDate();
    $scope.minStopDate = moment().add(1, 'days').toDate();

    $scope.errorSave = function(errorMessage) {
      Notification.error({message: 'Failed!' + ' ' + errorMessage, delay: 1000});
    };

    $scope.updatingSchedules = function () {
        Notification.success({ message: 'Updating Schedules!', delay: 3000 });
    };

    $scope.successSave = function() {
      Notification.success({message: 'Saved!', delay: 1000});
    };

    $scope.successCancel = function() {
      Notification.success({message: 'Cancelled!', delay: 1000});
    };

    $scope.dateBlurred = function(isStart) {
      if(isStart && $scope.systemSettings.startDate === undefined) {
        $scope.systemSettings.startDate = moment().toDate();
      } else {
        if($scope.systemSettings.stopDate === undefined) {
          $scope.systemSettings.stopDate = moment().add(1, 'days').toDate();
        }
      }
      $scope.changeVacation();
    };

    $scope.setVacationState = function () {
        var params = {
            VACFLO: $scope.systemSettings.vacMode,            
        };
        console.log("*** SET  START DATE" + params.START + "  STOP DATE: " + params.STOP);

        propertyService.SetParams($scope.systemSettings.OBJNAM, params).then(function (result) {
            $scope.systemSettings.VACFLO = params.VACFLO;            
            return result;
        });
    };

    $scope.changeVacation = function() {
        var params = {
          //VACFLO: $scope.systemSettings.vacMode,
          VACTIM: $scope.systemSettings.vacTimer,
          START: $filter('date')($scope.systemSettings.startDate, 'MM,dd,yy'),
          STOP: $filter('date')($scope.systemSettings.stopDate, 'MM,dd,yy')
        };
        console.log("*** SET  START DATE" + params.START + "  STOP DATE: " + params.STOP);

        propertyService.SetParams($scope.systemSettings.OBJNAM, params).then(function (result) {
            //$scope.systemSettings.VACFLO = params.VACFLO;
            $scope.systemSettings.START = params.START;
            $scope.systemSettings.STOP = params.STOP;
          return result;
        });
    };

    $scope.getScheduleFormatted = function (time, mode) {
        var value = time
        switch (mode) {
            case 'SRIS':
                value = 'SUNRISE'
                break;
            case 'SSET':
                value = 'SUNSET'
                break;
            default:
            case 'ABSTIM':
                value = time;
                break;
        }
        return value;
    }

    $scope.setStartTimeMode = function (mode, sched) {
        sched.START = mode.key;
    }

    $scope.setStopTimeMode = function (mode, sched) {
        sched.STOP = mode.key;
    }

    $scope.setTimeModeSelected = function (mode, index, sched) {
        $scope.SelectedSchedule.START = mode.key;
    }

    $scope.dateChanged = function(isStart) {
      if(isStart) {
        if($scope.systemSettings.stopDate && ($scope.systemSettings.startDate >= $scope.systemSettings.stopDate)) {
          // then reassign the stop date to be a day later.
          $scope.systemSettings.stopDate = moment($scope.systemSettings.startDate).add(1, 'days').toDate();
        }
      } else { // Then the stop date changed.
        if($scope.systemSettings.startDate && ($scope.systemSettings.stopDate <= $scope.systemSettings.startDate)) {
          // then reassign the start date to be a day earlier.
          $scope.systemSettings.startDate = moment($scope.systemSettings.stopDate).subtract(1, 'days').toDate();
        }
      }
      $scope.changeVacation();
    };

    $scope.$on('SchedulesUpdated', function (event, message) {
      //refreshList();
    });

    $scope.$on('Schedules_UPDATES', function (event, message) {
        console.log(">>> BROADCAST SERVING VACATION Schedules_UPDATES");
        $scope.updatingSchedules();
        refreshList();        
    });

    $scope.setToScheduleMode = function (heatMode, index) {

        if (propertyModel.supportConnectedHeaters()) {
            if ($scope.Schedules[index]) {
                $scope.Schedules[index].MODE = heatMode.MODE;
                // get name
                name = getHeatNameModeOnSchedule(heatMode.MODE, $scope.Schedules[index].CIRCUIT);
                $scope.Schedules[index].heatMode = name
            }
        }
    }

    function checkExceptionUltraTemp(bodyID, name) {// checking dependency for ultra all names on heaters selection are default by 'Heat Pump'
        var ultra = false
        angular.forEach(propertyModel.Heaters, function (heater) {         // Transverse to all heaters.
            if ((heater.BODY === bodyID) && (heater.SUBTYP === "ULTRA"))
                ultra = true
        })
        if (ultra)
            name = name.replace("Heat Pump", "UltraTemp")
        return name
    }

    function getBodyPerCircuit(circuit) {
        var BODY = ""
        angular.forEach(propertyModel.Bodies, function (body) {
            if (circuit.SUBTYP === body.SUBTYP)
                BODY = body
        })
        return BODY;
    }

    $scope.AddToHeatMode = function (insert) {
        var present = false;
        angular.forEach($scope.HeatModes, function (item) {
            if (item.MODE === insert.MODE)
                present = true
        });

        if (!present)
            $scope.HeatModes.push(insert);
    }

    function getHeatNameModeOnSchedule(heatMode, scheduleCircuit) {

        var name = enumerations.getHeaterModeName(heatMode)
        if ((scheduleCircuit == 'C0006') || (scheduleCircuit == 'C0001')) {   // POOL OR SPA
            angular.forEach(propertyModel.Circuits, function (circuit) {
                if (circuit.OBJNAM == scheduleCircuit) {
                    var bodyObj = getBodyPerCircuit(circuit)
                    name = checkExceptionUltraTemp(bodyObj.OBJNAM, name)
                }
            })
        }
        return name;
    }

    $scope.getHeaterModeSelected = function (schedule) {
        var name = "Off";
        var scheduled = "";
        if (propertyModel.supportConnectedHeaters()) {
            var mode = 0; // dont change
            if (schedule.MODE) {    // checking if exists
                mode = schedule.MODE
            }
            name = getHeatNameModeOnSchedule(mode, schedule.CIRCUIT)

            return name;
        }
        name = schedule.heatMode;
        return name;
    }

    $scope.toggleVacationMode = function () {
      $scope.systemSettings.vacMode = ($scope.systemSettings.vacMode === 'ON') ? 'OFF' : 'ON';
      $scope.changeVacation();
    };

    $scope.setVacationMode = function (mode) {
        $scope.systemSettings.vacMode = mode;   //ON or OFF
        $scope.setVacationState();
    };

    $scope.applyVacationMode = function () {
    //  $scope.dateChanged(true); // saving start date
    //  $scope.dateChanged(false); // saving end date
        $scope.changeVacation();
    };

    $scope.$watch('systemSettings', function (newVal, oldVal) {
      // we only set params when we get new values from somewhere else so delete your markup vals here
      var oldValComp = {
          VACFLO: oldVal.VACFLO,
          VACTIM: oldVal.VACTIM,
          START: oldVal.START,
          STOP: oldVal.STOP,
      }
      var newValComp = {
          VACFLO: newVal.VACFLO,
          VACTIM: newVal.VACTIM,
          START: newVal.START,
          STOP: newVal.STOP,
      }
      
      // If it is different from outside this instance
      if (!angular.equals(oldValComp, newValComp) && newVal.START !== undefined && newVal.STOP !== undefined) {
        // Then set our variables to have it too in new val
          if(newVal.START === '') {
            newVal.startDate = new Date();
          } else {
            newVal.startDate = new Date(Date.parse(newVal.START.replace(/,/g, '/')));
          }
          if(newVal.STOP === '') {
            newVal.stopDate = new Date();
          } else {
            newVal.stopDate = new Date(Date.parse(newVal.STOP.replace(/,/g, '/')));
          }
          newVal.vacMode = newVal.VACFLO;
          newVal.vacTimer = newVal.VACTIM;
      } 
    }, true);

    $scope.AddSchedule = function () {
      $scope.SelectedSchedule = newSchedule();
      $scope.Schedules.push($scope.SelectedSchedule);
      stopTimePristine = true;
    };

    $scope.changeScheduleDay = function (schedule, range) {
      switch (range) {
        case 'weekends':
          schedule.days = ['A', 'U'];
          break;
        case 'weekdays':
          schedule.days = ['M', 'T', 'W', 'R', 'F'];
          break;
        case 'every day':
          schedule.days = ['M', 'T', 'W', 'R', 'F', 'A', 'U'];
          break;
        case 'clear':
          schedule.days = [];
          break;
      }
    };

    $scope.SetEditMode = function (schedule) {
      if (schedule) {
        var obj = propertyModel.Objects[schedule.CIRCUIT];
        if (!obj) {
          obj = newSchedule();
        }
        $scope.SelectedSchedule = schedule;
        $scope.SelectedSchedule.bodyObject = {};
        $scope.SelectedSchedule.body = false;
        $scope.SelectedSchedule.smartStart = false;
        // check for type circuit fitler
        if (obj.HNAME === 'Pool' || obj.HNAME === 'Spa') {
          $scope.SelectedSchedule.bodyObject = $filter('objectsByParamValue')(propertyModel.Bodies, ['BODY', 'SHARE'], obj.BODY)[0];
          $scope.SelectedSchedule.body = true;
          $scope.HeatModes = propertyModel.Heaters;  //$scope.HeatModes = $filter('objectsByParamValue')(propertyModel.Heaters, ['BODY', 'SHARE'], obj.BODY);          
          if (schedule.HEATER && schedule.HEATER !== '00000') {
              var heater = $filter('objectsByName')(propertyModel.Heaters, schedule.HEATER);
              $scope.SelectedSchedule.heatMode = "HOLD" // OFF
              if (schedule.HEATER != "HOLD") {
                  $scope.SelectedSchedule.heatMode = propertyModel.Objects[schedule.HEATER].SNAME;
                  schedule.coolingCapable = $filter('coolingCapableHeater')(heater);
              }
          }
        }
        if (obj.USAGE && obj.USAGE !== 'USAGE') {
          $scope.SelectedSchedule.smartStart = obj.USAGE.indexOf('E') > -1;
        }
        stopTimePristine = false;
      }
    };

    $scope.ExpandItem = function (event, schedule, index) {
        if (schedule != null) {
            $scope.SelectedSchedule = schedule;
            $scope.SelectedIndex = index;

            $scope.SetEditMode(schedule);
            $scope.Schedules.forEach(function (s) {
                if (s.OBJNAM === schedule.OBJNAM) {
                    s.open = !s.open;
                }
            });

            $scope.checkDefaults(schedule);
        }
        event.stopPropagation();
    };

    $scope.CircuitSelected = function (obj, schedule) {
        if ($scope.SelectedSchedule) {
            delete $scope.SelectedSchedule.HEATER;
            delete $scope.SelectedSchedule.coolingCapable;
            delete $scope.SelectedSchedule.heatingCapable;
            delete $scope.SelectedSchedule.heatMode;
        }
        if (obj) {
            if ($scope.SelectedSchedule == null) {
                $scope.SelectedSchedule = schedule;
            }
            $scope.SelectedSchedule.CIRCUIT = obj.OBJNAM;
            $scope.SelectedSchedule.bodyObject = {
            };
            $scope.SelectedSchedule.body = false;
            $scope.SelectedSchedule.smartStart = false;
            if (obj.HNAME === 'Pool' || obj.HNAME === 'Spa') {
                $scope.SelectedSchedule.bodyObject = propertyModel.Objects[obj.BODY];
                $scope.SelectedSchedule.body = true;
                //$scope.HeatModes = $filter('objectsByParamValue')(propertyModel.Heaters, ['BODY', 'SHARE'], obj.BODY);
                $scope.checkDefaults($scope.SelectedSchedule);

                if ($scope.HeatModes.length > 0) {
                    // don't show heaters if we don't have any
                    $scope.SelectedSchedule.LOTMP = 78;
                    $scope.SelectedSchedule.COOLING = 81;
                } else {
                    $scope.SelectedSchedule.body = false;
                    delete $scope.SelectedSchedule.LOTMP;
                    delete $scope.SelectedSchedule.COOLING;
                }
            } else {
                delete $scope.SelectedSchedule.LOTMP;
                delete $scope.SelectedSchedule.COOLING;
            }
            if (obj.USAGE && obj.USAGE !== 'USAGE') {
                $scope.SelectedSchedule.smartStart = obj.USAGE.indexOf('E') > -1;
            }
            $scope.SelectedSchedule.SNAME = obj.SNAME;
        }
    };


    $scope.getCircuitPerID = function (circuitID) {
        var circuit
        angular.forEach($scope.Model.Circuits, function (item) {
            if (item.OBJNAM == circuitID) {
                circuit = item;
            }
        });
        return circuit;
    };


    $scope.getCircuitPerBody = function (circuitID) {
        var circuit
        angular.forEach($scope.Model.Bodies, function (item) {
            if (item.OBJNAM == circuitID) {
                circuit = item;
            }
        });
        return circuit;
    };

    $scope.checkDefaults = function (schedule) {    // checking defaults values
        if (schedule != null) {
            $scope.HeatModes = [];
            var selCir = schedule.CIRCUIT;
            if (selCir === "") // select by default AUX 1
                selCir = "C0002"

            var circuit = $scope.getCircuitPerID(selCir);
            angular.forEach(propertyModel.Heaters, function (item) {

                var bodyCircuit = $scope.getCircuitPerBody(item.BODY);
                item.MODE = "0";
                if (
                    (circuit.SUBTYP == bodyCircuit.SUBTYP) ||       // POOL = POOL  or SPA = SPA
                    (item.BODY === "B1101 B1202") ||                // shared body  case 1
                    (item.BODY === "B1202 B1101"))                // shared body  case 2
                    $scope.HeatModes.push(item);
            });

        } else {
            $scope.HeatModes = propertyModel.Heaters;
        }
        var insertOff = true;
        var insertDont = true;

        if (propertyModel.supportConnectedHeaters()) {
            $scope.setHeatModesAvailable();
        } else {
            angular.forEach($scope.HeatModes, function (item) {
                if (item.OBJNAM === '00000') {
                    insertOff = false;
                }

                if (item.OBJNAM === 'HOLD') {
                    insertDont = false;
                }
            });
        }

        var _objnam = 'HOLD';
        var _sname = $scope.dontchange;

        if (insertDont === true)
            $scope.HeatModes.unshift({ SNAME: _sname, OBJNAM: _objnam, DISPLAY: _sname, MODE: '0', ORDER: 0 });

        if (insertOff === true)
            $scope.HeatModes.unshift({ SNAME: 'OFF', OBJNAM: '00000', DISPLAY: 'OFF', MODE: '1', ORDER: 1 });


        if (propertyModel.supportConnectedHeaters()) {
            $scope.HeatModes.sort(function (a, b) {
                var keyA = a.ORDER,
                    keyB = b.ORDER;
                if (keyA < keyB) return -1;
                if (keyA > keyB) return 1;
                return 0;
            });
        }
    }

    $scope.setHeatModesAvailable = function () {

        var comboItem;
        var comboPresent = false;
        angular.forEach($scope.HeatModes, function (item) {
            switch (item.OBJNAM) {
                case '00000':
                    insertOff = false;
                    item.ORDER = 1;
                    break;
                case 'HOLD':
                    insertDont = false;
                    item.ORDER = 0;
                    break;
                case 'HXSLR':
                    item.DISPLAY = "Solar Preferred";
                    item.MODE = '4';
                    item.ORDER = 4;
                    break;
                case 'HXHTP':
                    item.DISPLAY = "Heat Pump Preferred";
                    item.MODE = '6';
                    break;

                case 'HXULT':
                    item.DISPLAY = "UltraTemp Preferred";
                    item.MODE = '6';
                    break;
            }

            switch (item.SUBTYP) {
                case 'GENERIC':
                    item.DISPLAY = "Heater";
                    item.MODE = '2';
                    break;

                case 'SOLAR':
                    item.DISPLAY = "Solar Only";
                    item.MODE = '3';
                    item.ORDER = 3;
                    break;

                case 'ULTRA':
                    item.DISPLAY = "UltraTemp Only";
                    item.MODE = '5';
                    item.ORDER = 5;
                    break;

                case 'HTPMP':
                    item.DISPLAY = "Heat Pump Only";
                    item.MODE = '5';
                    item.ORDER = 5;
                    break;
                case 'MASTER':
                    item.DISPLAY = "MasterTemp";
                    item.MODE = '11';
                    item.ORDER = 7;
                    break;
                case 'MAX':
                    item.DISPLAY = "Max-E-Therm";
                    item.MODE = '12';
                    item.ORDER = 8;
                    break;
                case "HCOMBO":
                    item.DISPLAY = "Hybrid-Gas Only Mode"
                    item.MODE = '7';
                    item.ORDER = 9;
                    comboPresent = true;
                    comboItem = angular.copy(item);
                    break;

            }
        });

        if (comboPresent) {
            var item2 = angular.copy(comboItem);
            var item3 = angular.copy(comboItem);
            var item4 = angular.copy(comboItem);

            item2.DISPLAY = "Hybrid-Heat Pump Only Mode"
            item2.MODE = '8';
            item2.ORDER = 10;
            $scope.AddToHeatMode(item2);

            item3.DISPLAY = "Hybrid-Hybrid Mode"
            item3.MODE = '9';
            item3.ORDER = 11;
            $scope.AddToHeatMode(item3);

            item4.DISPLAY = "Hybrid-Dual Mode"
            item4.MODE = '10';
            item4.ORDER = 12;
            $scope.AddToHeatMode(item4);
        }


    }

    $scope.getDaysForSchedule = function (days) {
        var name = days;
        if (days.length == 0) {
            name = "No Days"
        }
        return name
    }

    $scope.CancelEdit = function () {
      $scope.SelectedSchedule = null;
      $scope.successCancel();
      refreshList();
    };

    function getHeatNameModeOnSchedule(heatMode, scheduleCircuit) {

        var name = enumerations.getHeaterModeName(heatMode)
        if ((scheduleCircuit == 'C0006') || (scheduleCircuit == 'C0001')) {   // POOL OR SPA
            angular.forEach(propertyModel.Circuits, function (circuit) {
                if (circuit.OBJNAM == scheduleCircuit) {
                    var bodyObj = getBodyPerCircuit(circuit)
                    name = checkExceptionUltraTemp(bodyObj.OBJNAM, name)
                }
            })
        }
        return name;
    }

    $scope.HeatModeSelected = function (heatMode, index) {
        var body = $scope.SelectedSchedule.CIRCUIT;
        if (!PWC.isEmptyObject(body && heatMode)) {
            $scope.SelectedSchedule.HEATER = heatMode.OBJNAM;
            if (heatMode.SNAME === $scope.dontchange) { // check is hold = Don't Change             
                $scope.SelectedSchedule.heatMode = $scope.dontchange; //"HOLD";
                $scope.SelectedSchedule.MODE = "0";// DONT CHANGE
            }
            else {

                switch (heatMode.OBJNAM) {
                    case "00000":
                        $scope.SelectedSchedule.heatMode = "Off";
                        $scope.SelectedSchedule.MODE = "1"; // OFF
                        break;

                    case "HOLD":
                    case "00001":
                        $scope.SelectedSchedule.heatMode = $scope.dontchange;
                        $scope.SelectedSchedule.MODE = "0";// DONT CHANGE
                        break;

                    default:
                        if (propertyModel.supportConnectedHeaters()) {
                            $scope.SelectedSchedule.heatMode = getHeatNameModeOnSchedule(heatMode.MODE, $scope.SelectedSchedule.CIRCUIT);
                            $scope.SelectedSchedule.MODE = heatMode.MODE;
                        } else {
                            $scope.SelectedSchedule.heatMode = propertyModel.Objects[heatMode.OBJNAM].DISPLAY;
                            $scope.SelectedSchedule.MODE = heatMode.MODE;
                        }
                        break;
                }
                $scope.setToScheduleMode(heatMode, index)
            }

            $scope.SelectedSchedule.coolingCapable = $filter('coolingCapableHeater')([heatMode]);
            $scope.SelectedSchedule.heatingCapable = $filter('heatingCapable')([heatMode]);

            /* KEEPING  OLD VALUES 
          $scope.SelectedSchedule.LOTMP = 78;
          $scope.SelectedSchedule.COOLING = 81;
          */

            if ($scope.SelectedSchedule.coolingCapable) {
                if ($scope.SelectedSchedule.bodyObject) // check if exist
                    $scope.SelectedSchedule.COOLING = $scope.SelectedSchedule.bodyObject.COOLING;
            }
        }
    };

    $scope.TypeChanged = function (value) {
      $scope.SelectedSchedule.days = [];
      $scope.SelectedSchedule.SINGLE = value;
      if (value === 'OFF') {
        $scope.SelectedSchedule.days = ['M', 'T', 'W', 'R', 'F', 'A', 'U']
      } else {
        $scope.SelectedSchedule.days = $scope.getCurrentDayLetter();
      }
    };

    $scope.getCurrentDayLetter = function () {
      var d = new Date().getDay();
      var currentDayLetter = ['M'];
      switch (d) {
        case 0:
          currentDayLetter = ['U'];
          break;
        case 1:
          currentDayLetter = ['M'];
          break;
        case 2:
          currentDayLetter = ['T'];
          break;
        case 3:
          currentDayLetter = ['W'];
          break;
        case 4:
          currentDayLetter = ['R'];
          break;
        case 5:
          currentDayLetter = ['F'];
          break;
        case 6:
          currentDayLetter = ['A'];
          break;
        default:
          currentDayLetter = ['M'];
      }
      return currentDayLetter;
    };

    $scope.ToggleDayOfWeek = function (value) {
      if ($scope.SelectedSchedule.SINGLE === 'ON') {
        $scope.SelectedSchedule.days = [];
        $scope.SelectedSchedule.days.push(value);
      } else {
        var index = $scope.SelectedSchedule.days.indexOf(value);
        if (index < 0) {
          $scope.SelectedSchedule.days.push(value);
        } else {
          $scope.SelectedSchedule.days.splice(index, 1);
        }
      }
    }

    $scope.DeleteSchedules = function () {
      $scope.ConfirmDelete = true;
    };

    $scope.ConfirmDeleteSchedules = function () {
      $scope.Schedules.forEach(function (schedule) {
        if (schedule.deleteFlag) {
          propertyService.SetValue(schedule.OBJNAM, 'STATUS', 'DSTROY');
        }
      });
      $scope.Schedules = [];
      propertyService.GetSchedules();
      $scope.ConfirmDelete = false;
    };

    $scope.checkAllDays = function () {
      $scope.Schedule.Days = enumerations.DaysOfWeek.map(function (item) {
        return item.id;
      });
    };

    $scope.SetTemp = function (value, prop) {
      if (value) {
        var newValue = Number(value) || Number(value.replace(/\D/g, ''));
        if (newValue > $scope.maxValue) {
          newValue = $scope.maxValue;
        }
        if (newValue < $scope.minValue) {
          newValue = $scope.minValue;
        }
        if (prop === 'COOLING') {
          $scope.SelectedSchedule.COOLING = newValue;
        } else {
          $scope.SelectedSchedule.LOTMP = newValue;
        }
      }
    };

    $scope.IncrementTemp = function (obj, prop) {
      if (obj) {
        var newValue = Number(obj) || Number(obj.replace(/\D/g, ''));
        newValue++;
        if (newValue <= $scope.maxTemp) {
          if (prop === 'COOLING') {
            $scope.SelectedSchedule.COOLING = newValue;
          } else {
            $scope.SelectedSchedule.LOTMP = newValue;
          }
        }
      }
    };

    $scope.DecrementTemp = function (obj, prop) {
      if (obj) {
        var newValue = Number(obj) || Number(obj.replace(/\D/g, ''));
        newValue--;
        if (newValue >= $scope.minTemp) {
          if (prop === 'COOLING') {
            $scope.SelectedSchedule.COOLING = newValue;
          } else {
            $scope.SelectedSchedule.LOTMP = newValue;
          }
        }
      }
    };

    $scope.Keypress = function (event) {
      if (event.keyCode < 48 || event.keyCode > 57) {
        event.preventDefault();
      }
    }

    $scope.getTimeModeSelected = function (mode) {
        var result = "Manual"

        switch (mode) {
            case 'ABSTIM':
                result = 'Manual';
                break;

            case 'SRIS':
                result = 'Sunrise';
                break;

            case 'SSET':
                result = 'Sunset';
                break;
        }

        $scope.timeModes.forEach(function (item) {

        });
        return result;
    }

    $scope.CheckAllDays = function () {
      $scope.SelectedSchedule.days = enumerations.DaysOfWeek.map(function (item) {
        return item.id;
      });
    };

    $scope.ValidateNewSchedule = function () {
        return ($scope.SelectedSchedule && $scope.SelectedSchedule.days && $scope.SelectedSchedule.CIRCUIT) ? true : false;
    }

    $scope.SaveSchedule = function () {
      // round to the nearest half hour
      var staTimeMin = $scope.SelectedSchedule.startTime.getMinutes();
      var staTimeHr = $scope.SelectedSchedule.startTime.getHours();
      var stoTimeMin = $scope.SelectedSchedule.stopTime.getMinutes();
      var stoTimeHr = $scope.SelectedSchedule.stopTime.getHours();
      $scope.SelectedSchedule.startTime.setMinutes((parseInt((staTimeMin + 15)/30) * 30) % 60);
      $scope.SelectedSchedule.startTime.setHours(staTimeMin > 45 ? (staTimeHr === 23 ? 0 : staTimeHr+1) : staTimeHr);
      $scope.SelectedSchedule.stopTime.setMinutes((parseInt((stoTimeMin + 15)/30) * 30) % 60);
      $scope.SelectedSchedule.stopTime.setHours(stoTimeMin > 45 ? (stoTimeHr === 23 ? 0 : stoTimeHr+1) : stoTimeHr);

      var params = {
        SNAME: $scope.SelectedSchedule.SNAME,
        CIRCUIT: $scope.SelectedSchedule.CIRCUIT,
        DAY: getDayMsgString($scope.SelectedSchedule.days),
        SINGLE: $scope.SelectedSchedule.SINGLE,
        START: $scope.SelectedSchedule.START,
        TIME: $filter('date')($scope.SelectedSchedule.startTime, 'HH,mm,ss'),
        STOP: $scope.SelectedSchedule.StopMode,
        TIMOUT: $filter('date')($scope.SelectedSchedule.stopTime, 'HH,mm,ss'),
        GROUP: $scope.SelectedSchedule.GROUP,
        STATUS: $scope.SelectedSchedule.STATUS,
        HEATER: $scope.SelectedSchedule.HEATER,
        COOLING: $scope.SelectedSchedule.COOLING + '',
        LOTMP: $scope.SelectedSchedule.LOTMP + '',
        SMTSRT: $scope.SelectedSchedule.SMTSRT,
        VACFLO: 'ON',
        MODE: $scope.SelectedSchedule.MODE,
        DNTSTP: $scope.SelectedSchedule.DNTSTP
      };

      angular.forEach(params, function(value, key) {
        if(value === 'undefined') {
          delete params[key];
        }
      });

      if (!$scope.SelectedSchedule.OBJNAM) {
        $scope.CreatingSchedule = true;
        propertyService.CreateNewObject('SCHED', params).then(function () {
         // $scope.Schedules = [];
         // propertyService.GetSchedules();
          $scope.CreatingSchedule = false;
        });
      } else {
        if ($scope.SelectedSchedule.OBJNAM) {
          if(params.STATUS !== 'OFF') {
            delete params.STATUS;
          }
          delete params.VACFLO;
          propertyService.SetParams($scope.SelectedSchedule.OBJNAM, params).then(function () {
           // $scope.Schedules = [];
           // propertyService.GetSchedules();
          });
        }
      }
      $scope.successSave();
    };

    $scope.IncrementStartTime = function () {
      $scope.SelectedSchedule.startTime = PWC.addMinutes($scope.SelectedSchedule.startTime, 30);
      //syncStopTime();
    };

    $scope.DecrementStartTime = function () {
      $scope.SelectedSchedule.startTime = PWC.addMinutes($scope.SelectedSchedule.startTime, -30);
      //syncStopTime();
    };

    $scope.StartTimeChanged = function (startTime) {
      $scope.SelectedSchedule.startTime = startTime;
      //syncStopTime(startTime);
    };

    $scope.StopTimeChanged = function (stopTime) {
      stopTimePristine = false;
      // $scope.SelectedSchedule.TIMOUT = $filter('translateTime')($filter('date')(stopTime, 'HH,mm'));
      $scope.SelectedSchedule.stopTime = stopTime;
      validateTimes();
    }

    $scope.IncrementStopTime = function () {
      stopTimePristine = false;
      $scope.SelectedSchedule.stopTime = PWC.addMinutes($scope.SelectedSchedule.stopTime, 30);
      validateTimes();
    }
    $scope.DecrementStopTime = function () {
      stopTimePristine = false;
      $scope.SelectedSchedule.stopTime = PWC.addMinutes($scope.SelectedSchedule.stopTime, -30);
      validateTimes();
    };

    function syncStopTime(startTime) {
      startTime = startTime || $scope.SelectedSchedule.startTime;

      if (stopTimePristine || isNaN($scope.SelectedSchedule.stopTime)) {
        $scope.SelectedSchedule.stopTime = addTwohours(startTime);
      }
      validateTimes();
    }

    function validateTimes() {
      var selectedStartTime = new Date($scope.SelectedSchedule.startTime);
      var selectedStopTime = new Date($scope.SelectedSchedule.stopTime);
      selectedStartTime.setDate(1);
      selectedStopTime.setDate(1);

      if (selectedStopTime < selectedStartTime || selectedStopTime.getHours() < selectedStartTime.getHours()) {
        $scope.SelectedSchedule.stopTime = selectedStartTime;
      }
    }

    $scope.DayChanged = function (day) {
      $scope.SelectedSchedule.days = [];
      $scope.SelectedSchedule.days.push(day);
    };


    $scope.isHeatControlEnable = function () {
        if ($scope.SelectedSchedule) {
            if ($scope.SelectedSchedule.heatingCapable) {
                return $scope.SelectedSchedule.heatingCapable;
            }
        }

        return false;
    }

    $scope.isCoolingCapable = function () {
        if ($scope.SelectedSchedule) {
            if ($scope.SelectedSchedule.coolingCapable) {
                return $scope.SelectedSchedule.coolingCapable;
            }
        }

        return false;   // is cooling capable
    }

    $scope.ToggleUseCurrent = function (type) {
      switch (type) {
        case 'HeatMode':
          if ($scope.SelectedSchedule.UseCurrentHeatMode)
            $scope.SelectedSchedule.bodyObject.HEATER = propertyModel.Objects[$scope.SelectedSchedule.CIRCUIT].HEATER;
          break;
        case 'Temp':
          if ($scope.SelectedSchedule.UseCurrentTemp)
            $scope.SelectedSchedule.bodyObject.LOTMP = propertyModel.Objects[$scope.SelectedSchedule.CIRCUIT].LOTMP;
      }
    }

    $scope.ScheduleDeletes = function () {
      var i = 0;
      $scope.Schedules.forEach(function (item) {
        if (item.deleteFlag) {
          i++;
        }
      });
      return i > 0;
    };

    $scope.SetDeleteFlag = function (schedule) {
      if (schedule) {
        schedule.deleteFlag = !schedule.deleteFlag;
      }
    };


    function addTwohours(dateTime) {
      var result = new Date(dateTime);
      if (result.getHours() <= 21) {
        result.setHours(result.getHours() + 2);
      } else if (result.getHours() >= 22) {
        result.setHours(23);
        result.setMinutes(59);
      }
      return result;
    }


    function getTimeFormat() {
      var system = propertyModel.Objects["_C10C"];
      var mode = ((system) ? system.CLK24A : "AMPM");

      return (mode === "AMPM") ? "h:mm a" : "HH:mm";
    }

    function refreshList() {
      $scope.Schedules = [];
      var mode = "0";
      propertyModel.Schedules.forEach(function (schedule) {

        if (schedule.MODE) // CHECK IF MODE EXIST
             mode = schedule.MODE;

        var newSchedule = {
          OBJNAM: schedule.OBJNAM,
          OBJTYP: schedule.OBJTYP,
          SNAME: schedule.SNAME,
          LISTORD: schedule.LISTORD,
          CIRCUIT: schedule.CIRCUIT,
          DAY: schedule.DAY,
          START: schedule.START,
          TIME: $filter('translateTime')(schedule.TIME),
          STOP: schedule.STOP,
          SINGLE: schedule.SINGLE,
          TIMOUT: $filter('translateTime')(schedule.TIMOUT),
          GROUP: schedule.GROUP,
          HEATER: schedule.HEATER,
          COOLING: schedule.COOLING,
          LOTMP: schedule.LOTMP,
          SMTSRT: schedule.SMTSRT,
          DNTSTP: schedule.DNTSTP,
          STATUS: schedule.STATUS,
          MODE: mode,
          startTime: moment(schedule.TIME, 'HHmmSS').toDate(),
          stopTime: moment(schedule.TIMOUT, 'HHmmSS').toDate(),
          days: schedule.DAY.split(''),
          heatMode: '',
          bodyObject: {},
          body: false,
          smartStart: false,
          deleteFlag: false,
          open: false
        };
        if (newSchedule.startTime === 'Invalid Date') {
          newSchedule.startTime = PWC.convertToDate(propertyModel.Objects["_C10C"].MIN, propertyModel.Objects["_C10C"].TIMZON);
          newSchedule.stopTime = addTwohours(PWC.convertToDate(propertyModel.Objects["_C10C"].MIN, propertyModel.Objects["_C10C"].TIMZON));
        }
        if(newSchedule.STATUS === 'OFF') {
          newSchedule.disableStatus = true;
        }

        var obj = propertyModel.Objects[newSchedule.CIRCUIT];
        if (obj) {
          if (obj.SUBTYP === 'POOL' || obj.SUBTYP === 'SPA') {
            if (newSchedule.HEATER && newSchedule.HEATER !== '00000') {
                newSchedule.bodyObject = $filter('objectsByParamValue')(propertyModel.Bodies, ['BODY', 'SHARE'], obj.BODY)[0];
                newSchedule.heatMode = "HOLD" // OFF
                if (newSchedule.HEATER != "HOLD") {
                    newSchedule.heatMode = propertyModel.Objects[newSchedule.HEATER].SNAME;
                    var heater = $filter('objectsByName')(propertyModel.Heaters, newSchedule.HEATER);
                    newSchedule.coolingCapable = $filter('coolingCapableHeater')(heater);
                }

            }
            newSchedule.body = true;
          }
          if (obj.USAGE && obj.USAGE !== 'USAGE') {
            newSchedule.smartStart = obj.USAGE.indexOf('E') > -1;
          }
        }

        if (schedule.VACFLO === 'ON') {
          $scope.Schedules.push(newSchedule);
        }
      });
    }

    function getDayMsgString(days) {
      return (angular.isArray(days) ? days.join('') : days);
    }
    
    $scope.isScheduleSecurityLocked = function () {

        // here security is not enable 
        if (!$scope.securityEnabled)
            return false

        // check if allow schedules
        if ($scope.SHOMNU.indexOf('e') < 0) {
            return true;
        }

        return false;
    }

    $scope.isNotCoolingCapable = function () {
        if ($scope.SelectedSchedule === null)
            return true;    // nor cooling capable

        if ($scope.SelectedSchedule.COOLING === null)
            return true;    // nor cooling capable

        return false;   // is cooling capable
    }

    $scope.isCircuitNotHeatingCapable = function () {

        var capable = $scope.isCircuitHeatingCapable();

        return !capable;   // negative of capable
    }


    $scope.isCircuitHeatingCapable = function () {

        if ($scope.SelectedSchedule === null) //"undefined")
            return false;

        var isCapable = false;
        var obj = propertyModel.Objects[$scope.SelectedSchedule.CIRCUIT];
        if (obj) {
            if (obj.HNAME === 'Pool' || obj.HNAME === 'Spa') {
                isCapable = true;
            }
        }

        return isCapable;
    }


    function newSchedule() {
      var schedule = {
        OBJNAM: null,
        OBJTYP: 'SCHED',
        SNAME: '',
        LISTORD: '',
        CIRCUIT: '',
        DAY: '',
        START: 'ABSTIM',
        TIME: '',
        STOP: 'ABSTIM',
        SINGLE: 'OFF',
        TIMOUT: '',
        GROUP: '',
        HEATER: '',
        COOLING: '',
        LOTMP: '',
        SMTSRT: 'OFF',
        DNTSTP: 'OFF',
        STATUS: 'ON',
        MODE : "0", // DON'T CHANGE
        days: ['M', 'T', 'W', 'R', 'F', 'A', 'U'],
        heatMode: '',
        startTime: moment().hour(8).minute(0).toDate(),
        stopTime: moment().hour(17).minute(0).toDate(),
        bodyObject: {},
        body: false,
        smartStart: false,
        deleteFlag: false,
        open: true
      };
      return schedule;
    }

    function init() {
      refreshList();

      var preference = propertyModel.Objects._5451;
      if (preference) {
        if (preference.MODE.toUpperCase() === "ENGLISH") {
          $scope.UoM = 'F';
          $scope.minTemp = 55;
          $scope.maxTemp = 104;
        }
      }

      if($scope.systemSettings.START === '') {
        $scope.systemSettings.startDate = new Date();
      } else {
        $scope.systemSettings.startDate = new Date(Date.parse($scope.systemSettings.START.replace(/,/g, '/')));
      }
      if($scope.systemSettings.STOP === '') {
        $scope.systemSettings.stopDate = new Date();
      } else {
        $scope.systemSettings.stopDate = new Date(Date.parse($scope.systemSettings.STOP.replace(/,/g, '/')));
      }

      $scope.systemSettings.vacMode = $scope.systemSettings.VACFLO;
      $scope.systemSettings.vacTimer = $scope.systemSettings.VACTIM;

      $scope.ProgrammableCircuits = $filter('programmableCircuits')(propertyModel.Objects);
    }

  }
]);
