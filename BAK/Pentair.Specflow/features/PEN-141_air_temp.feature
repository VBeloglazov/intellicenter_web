Feature: Air Temperature (PEN-141)
As a Pool Owner  
I Want to check the temperature of the ambient air
So that I can know if it is a good day to swim

  Background user is connected to a pool system
  And user is viewing pool details

Scenario: TEMPERATURE CHANGE DISPLAY
When the air temperature value in the simulator changes
Then the displayed air temperature changes