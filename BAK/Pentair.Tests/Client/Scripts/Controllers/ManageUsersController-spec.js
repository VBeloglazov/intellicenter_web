﻿'use strict';
describe('ManageUsersController', function () {
    var PropertyService;
    var $location;
    var $scope;
    var LightShowController;
    var $controller;
    beforeEach(module('PentairWebClient'));

    beforeEach(inject(function (_$controller_, _$rootScope_, _PropertyService_, _$location_) {
        $scope = _$rootScope_.$new();
        PropertyService = _PropertyService_;
        $location = _$location_;
        $controller = _$controller_;
        LightShowController = $controller("ManageUsersController", { $scope: $scope });
        spyOn($location, 'path');
    }));

    //describe('User not in edit session', function () {
    //    beforeEach(function () {
    //        $scope.Property.EditSession = false;
    //    });
    //    describe('GoToDashboard', function () {
    //        it('should take the user the main page', function () {
    //            $scope.GoToDashboard();
    //            expect($location.path).toHaveBeenCalledWith('/property');
    //        });
    //    });
    //    describe('GoToAddUsersPage', function () {
    //        it('should take the user the addUsers page', function () {
    //            $scope.GoToAddUsersPage();
    //            expect($location.path).toHaveBeenCalledWith('/inviteUser');
    //        });
    //    });
    //});

    //describe('User in edit session', function () {
    //    beforeEach(function () {
    //        $scope.Property.EditSession = true;
    //    });
    //    describe('GoToDashboard', function () {
    //        it('should show the ignore edit dialog', function () {
    //            $scope.GoToDashboard();
    //            expect($scope.IgnoreEdit).toBe(true);
    //        });
    //    });
    //    describe('GoToAddUsersPage', function () {
    //        it('should show the ignore edit dialog', function () {
    //            $scope.GoToAddUsersPage();
    //            expect($scope.IgnoreEdit).toBe(true);
    //        });
    //    });
    //    describe('Confirm Ignore ', function () {

    //        it('should take the user the main page', function () {
    //            $scope.GoToDashboard();
    //            $scope.ContinueNavigate();

    //            expect($location.path).toHaveBeenCalledWith('/property');
    //        });
    //        it('should take the user the addUsers page', function () {
    //            $scope.GoToAddUsersPage();
    //            $scope.ContinueNavigate();

    //            expect($location.path).toHaveBeenCalledWith('/inviteUser');
    //        });

    //    });
    //});

});