﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;
using Pentair.Domain.Client.Entity;

namespace Pentair.Domain.Client.Infrastructure
{
    public class ApplicationUserContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationUserContext() : base("ApiContext")
        {
        }

        public static ApplicationUserContext Create()
        {
            return new ApplicationUserContext();
        }
    }
}

