﻿describe('circuitGroupPowerButtons', function () {
    var $scope, $compile;
    var elm;
    var baseTemplate = '<circuit-group-power-buttons id="UT1000_PwrBtn" disabled="isDisabled" target-circuit="MockCircuit"></circuit-group-power-buttons>';

    var propertyService;
    var elmScope;
    beforeEach(module('PentairWebClient'));

    beforeEach(inject(function ($rootScope, _$compile_, _PropertyService_) {
        $scope = $rootScope.$new();
        $compile = _$compile_;
        propertyService = _PropertyService_;
        $scope.MockCircuit = { OBJNAM: 'UT1000' };

        spyOn(propertyService, 'SetValue');

    }));

    describe('Click off button', function () {
        var offBtn;
        it('should invoke setValue method if enabled', function () {
            $scope.isDisabled = false;

            elm = SpecHelper.createDirective($compile, baseTemplate, $scope);
            elmScope = elm.isolateScope();

            offBtn = angular.element(elm[0].querySelector('.off'));
            offBtn.triggerHandler('click');

            expect(propertyService.SetValue).toHaveBeenCalledWith('UT1000', 'STATUS', 'OFF');
        });
        it('should invoke setValue method if enabled', function () {
            $scope.isDisabled = true;

            elm = SpecHelper.createDirective($compile, baseTemplate, $scope);
            elmScope = elm.isolateScope();

            offBtn = angular.element(elm[0].querySelector('.off'));
            offBtn.triggerHandler('click');

            expect(propertyService.SetValue).not.toHaveBeenCalledWith('UT1000', 'STATUS', 'OFF');
        });

    });

    describe('Click on button', function () {
        var onBtn;
        it('should invoke setValue method if enabled', function () {
            $scope.isDisabled = false;

            elm = SpecHelper.createDirective($compile, baseTemplate, $scope);
            elmScope = elm.isolateScope();

            onBtn = angular.element(elm[0].querySelector('.on'));
            onBtn.triggerHandler('click');

            expect(propertyService.SetValue).toHaveBeenCalledWith('UT1000', 'STATUS', 'ON');
        });
        it('should invoke setValue method if enabled', function () {
            $scope.isDisabled = true;

            elm = SpecHelper.createDirective($compile, baseTemplate, $scope);
            elmScope = elm.isolateScope();

            onBtn = angular.element(elm[0].querySelector('.on'));
            onBtn.triggerHandler('click');

            expect(propertyService.SetValue).not.toHaveBeenCalledWith('UT1000', 'STATUS', 'ON');
        });

    });

});