﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pentair.Domain.Simulator.Entity
{
    public class Remote
    {
        // OBJNAM is the UniqueID for the Object
        [Key]
        public int ID { get; set; }
        public string Name { get; set; }
        public UInt32 LastRevision { get; set; }
        public int ListPosition { get; set; }
        public string UserName { get; set; }
        public float Version { get; set; }
        public string SerialNumber { get; set; }
        public DateTime ManufactureDate { get; set; }
        public bool SpaSideEnable { get; set; }
        public int PumpStep { get; set; }

        // Foreign Keys
        public int TypeID { get; set; }
        public int RemoteSubTypeID { get; set; }
        public int CommObjectID { get; set; }
        public int ModuleID { get; set; }
        public int BodyAffiliationID { get; set; }
        public int PumpAffiliationID { get; set; }
    }
}