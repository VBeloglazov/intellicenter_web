var PWC = angular.module('PentairWebClient', ['ngRoute', 'LocalStorageModule', 'ngAnimate'
    , 'ngCookies', 'pascalprecht.translate', 'checklist-model', 'dnTimepicker'
    , 'ui.bootstrap', 'ui.grid', 'ui.grid.paging', 'ui.grid.cellNav', 'ui.grid.pagination'
    , 'angular-svg-round-progress', 'ui.select', 'ui.sortable', 'ui-notification']);

PWC.constant('HOST_NAME', (function () {
    var host = window.location.hostname;
    // host = 'pentdev.pathf.com';
    // host = "pentqa.pathf.com";
    // host = "pentuat.pathf.com";
    return host;
})());

PWC.constant('APP_PATH', (function () {
    var hostedEndpoint = '';
    var hostname = window.location.hostname;
    var endpoint = (function (hostname) {
        return (hostname === 'localhost') ? '' : hostedEndpoint;
    })(hostname);
    return endpoint;
})());

PWC.constant('SERVICE_HOST_NAME', (function () {
    var hostedEndpoint = '/service/';
    var host = window.location.hostname;
    // host = 'pentdev.pathf.com';
    // host = "pentqa.pathf.com";
    // host = "pentuat.pathf.com";
    var serviceBase = ((host === 'localhost') ? 'localhost:44300/' : host + hostedEndpoint); // VB 2016-12-15
    return serviceBase;
})());


PWC.provider('translator', myTranslateProvider);
PWC.config(function (APP_PATH, $routeProvider, translatorProvider, $translateProvider, $httpProvider) {
    //Set up routes object
    PWC.routes =
    {
        "/login": {
            controller: "LoginController",
            templateUrl: APP_PATH + "/app/templates/login.html",
            requireLogin: false,
            css: APP_PATH + "/content/css/pages/login.css"
        },
        "/changePassword": {
            controller: "LoginController",
            templateUrl: APP_PATH + "/app/templates/changePassword.html",
            requireLogin: true
        },
        "/passwordAssistance": {
            controller: "LoginController",
            templateUrl: APP_PATH + "/app/templates/passwordAssistance.html",
            requireLogin: false,
            css: APP_PATH + "/content/css/pages/login.css"
        },
        "/retrievePassword/:user": {
            controller: "LoginController",
            templateUrl: APP_PATH + "/app/templates/changePassword.html",
            requireLogin: false,
            resolve: {
                resetModel: function ($location, $route, AppService) {
                    return AppService.validateEmail($route.current.params.user, $route.current.params.code).then(function (result) {
                        return result.data;
                    }).catch(function () {
                        $location.path("login");
                    });
                }
            },
            css: APP_PATH + "/content/css/pages/login.css"
        },
        //"/manageProperties": {
        //    controller: "ManagePropertiesController",
        //    templateUrl: APP_PATH + "/app/templates/manageProperties.html",
        //    requireLogin: true,
        //    resolve: {
        //        installations: function ($route, AppService) {
        //            return AppService.InstallationsWithDetails(2000);
        //        }
        //    }
        //},
        "/property": {
            controller: "DashboardController",
            templateUrl: APP_PATH + "/app/templates/dashboard.html",
            css: APP_PATH + "/content/css/pages/dashboard.css",
            requireLogin: true,
            resolve: {
                subscribe: function (PropertyService) {
                    //  return PropertyService.SubscribeToPage();
                }
            }
        },
        "/settings": {
            controller: "SettingsController",
            templateUrl: APP_PATH + "/app/templates/settings.html",
            requireLogin: true,
            css: APP_PATH + "/content/css/pages/settings.css",
            resolve: {
                subscribe: function (PropertyService) {
                    //return PropertyService.SubscribeToPage().then(PropertyService.handleMessage);
                },
                schedules: function (PropertyService) {
                    //PropertyService.GetSchedules();
                }
            }
        },
        "/installations": {
            controller: "InstallationsController",
            templateUrl: APP_PATH + "/app/templates/installations.html",
            requireLogin: true,
            css: APP_PATH + "/content/css/pages/installations.css",
            resolve: {
                installations: function ($location, AppService) {
                    return AppService.Installations();
                }
            }
        },
        //"/installationGrid": {
        //    controller: "InstallationsGridController",
        //    templateUrl: APP_PATH + "/app/templates/installationsTable.html",
        //    requireLogin: true,
        //    resolve: {
        //        installations: function ($route, AppService) {
        //            return AppService.Installations();
        //        }
        //    }
        //},
        //"/lightShow": {
        //    controller: "LightShowController",
        //    templateUrl: APP_PATH + "/app/templates/lightShow.html",
        //    requireLogin: true,
        //    resolve: {
        //        subscribe: function (PropertyService) {
        //            PropertyService.SubscribeToPage();
        //        }
        //    }
        //},

        "/manageUsers": {
            controller: "ManageUsersController",
            templateUrl: APP_PATH + "/app/templates/manageUsers.html",
            css: APP_PATH + "/content/css/pages/manageUsers.css",
            requireLogin: true,
            resolve: {
                SecurityTokens: function (PropertyService) {
                    return PropertyService.GetSecurityTokens();
                }
            }
        },

        //"/schedules": {
        //    controller: "ScheduleController",
        //    templateUrl: APP_PATH + "/app/templates/schedules.html",
        //    requireLogin: true,
        //    css: APP_PATH + "/content/css/pages/schedules.css",
        //    resolve: {
        //        subscribe: function (PropertyService) {
        //            return PropertyService.SubscribeToPage().then(PropertyService.handleMessage);
        //        },
        //        schedules: function (PropertyService) {
        //            PropertyService.GetSchedules();
        //        }
        //    }
        //},

        //"/inviteUser": {
        //    controller: "InviteController",
        //    templateUrl: APP_PATH + "/app/templates/inviteUser.html",
        //    requireLogin: true
        //},

        "/invitation/:inviteId": {
            controller: "AcceptInviteController",
            templateUrl: APP_PATH + "/app/templates/acceptInvite.html",
            requireLogin: false,
            resolve: {
                inviteInfo: function ($route, AppService) {
                    return AppService.InviteInfo($route.current.params.inviteId).catch(function (msg) {
                        var err = {error: "Error!"};
                        if (msg.error) {
                            switch (msg.error.Message) {
                                case "Used":
                                    err = {error: "The invitation you have responded to has been used. Please contact the pool owner or administrator if you are having difficulty."};
                                    break;
                                case "Expired":
                                    err = {error: "The invitation you have responded to has expired. Please contact the pool owner or administrator to be re-invited."};
                                    break;
                                default:
                                    err = {error: "Error!"}
                                    break;
                            }
                        }
                        return {data: err}
                    });
                }
            }
        },
        "/confirmUserNameChange/:oldEmail/:inviteId": {
            controller: "ConfirmUserNameChangeController",
            templateUrl: APP_PATH + "/app/templates/confirmUserNameChange.html",
            requireLogin: false,
            resolve: {
                inviteInfo: function ($route, AppService) {
                    return AppService.InviteInfo($route.current.params.inviteId).catch(function (msg) {
                        var err = {error: "Error!"};
                        if (msg.error) {
                            switch (msg.error.Message) {
                                case "Used":
                                    err = {error: "Your username has already been changed. Please contact the pool owner or administrator if you are having difficulty."};
                                    break;
                                default:
                                    err = {error: "Error!"}
                                    break;
                            }
                        }
                        return {data: err}
                    });
                }
            }
        },
        "/configurations": {
            controller: "ConfigurationsContoller",
            templateUrl: APP_PATH + "/app/templates/configurations.html",
            requireLogin: true,
            css: APP_PATH + "/content/css/pages/configurations.css"
        },
        "/systemProtection": {
            controller: "SystemProtectionController",
            templateUrl: APP_PATH + "/app/templates/systemProtection.html",
            requireLogin: true
            //,resolve: {
            //    SecurityTokens: function (PropertyService) {
            //        return PropertyService.GetSecurityTokens();
            //    }
            //}
        },
        "/userNotifications": {
            controller: "UserNotificationController",
            templateUrl: APP_PATH + "app/templates/userNotifications.html",
            requireLogin: true,
            resolve: {
                //TODO: UnNeeded GET?
                UserNotifications: function (PropertyService) {
                    return PropertyService.GetUserNotifications();
                }
            }
        },
        //"/systemInformation": {
        //    controller: "SystemInformationController",
        //    templateUrl: APP_PATH + "/app/templates/systemInformation.html",
        //    requireLogin: true,
        //    css: APP_PATH + "/content/css/pages/systemInformation.css"
        //    , resolve: {
        //        PreFetch: function (PropertyService) {
        //            return PropertyService.SubscribeToPage().then(PropertyService.handleMessage);
        //        },
        //        NotificationRecipients: function (AppService, PropertyModel) {
        //            return AppService.GetData('Api/NotificationUsers/' + PropertyModel.SelectedInstallation)
        //        },
        //        StatusMessageFlags: function (PropertyService, $location) {
        //            return PropertyService.GetQuery('GetStatusMessageFlags').then(function (result) {
        //                return result.answer;
        //            }).catch(function () {
        //             //   $location.path("login");
        //            });
        //        },
        //        SystemInfo: function (AppService, PropertyModel) {
        //            return AppService.GetData('Api/Installations/' + PropertyModel.SelectedInstallation);
        //        }
        //    }
        //},
        "/systemPersonality": {
            controller: "SystemPersonalityController",
            templateUrl: APP_PATH + "/app/templates/systemPersonality.html",
            requireLogin: true,
            resolve: {
                PreFetch: function (PropertyService) {
                    PropertyService.GetSystemInfo();
                    PropertyService.GetUserList();
                }
            }
        },
        "/systemConfigureHardware": {
            controller: "SystemConfigurationController",
            templateUrl: APP_PATH + "/app/templates/systemConfiguration.html",
            requireLogin: true
            //        ,resolve: {
            //            Configuration: function (PropertyService) {
            //        return PropertyService.GetPanels();
            //    }
            //}
        },

        "/systemReportIssue": {
            controller: "SystemReportIssueController",
            templateUrl: APP_PATH + "/app/templates/systemReportIssue.html",
            requireLogin: true          
        },

        "/changeEmail": {
            controller: "ChangeEmailController",
            templateUrl: APP_PATH + "/app/templates/changeEmail.html",
            requireLogin: true
        },
    };

    //add routes to RouteProvider
    for (var path in PWC.routes) {
        $routeProvider.when(path, PWC.routes[path]);
    }

    //Default Page
    $routeProvider.otherwise({redirectTo: "/login"});

    //Http Interceptors
    $httpProvider.interceptors.push('AuthInterceptorService');

    //I18N translation provider
    translatorProvider.init($translateProvider);
});


PWC.run([
    'AuthService', 'SocketService', '$location', '$rootScope', 'LocalStorageService',
    function (AuthService, SocketService, $location, $rootScope, LocalStorageService) {

        // restore session data and fill authentication object
        AuthService.restorePreviousSession();

        cleanCache(LocalStorageService);

        // ping every minute to keep the websocket connection active.
        setInterval(function(){
            SocketService.ping();
        }, 60000);

        // intercept location changes to enforce required login
        $rootScope.$on("$locationChangeStart", function (event, next, current) {
            // TODO The clearSubsriptions function is called here and when tabs
            // are changed in the TabInclude directive.  Maybe one of these is 
            // redundant?
            SocketService.clearSubscriptions();

            for (var i in PWC.routes) {
                if (next.indexOf(i) != -1) {
                    if (PWC.routes[i].requireLogin && !AuthService.authentication.isAuth) {
                        $location.path("/login");
                        event.preventDefault();
                    }
                    // intercept location to enforce security permissions
                    else if(PWC.routes[i].controller === 'ManageUsersController') {
                      if(AuthService.authorization.SHOMNU.indexOf('z') < 0) {
                        $location.path('/login');
                        event.preventDefault();
                      }
                    }
                    else if (PWC.routes[i].controller === 'ConfigurationsContoller') {
                        /* allow user to go to system configuration is already protected by flags on setting menus
                      if(AuthService.authorization.securityEnabled && AuthService.authorization.SHOMNU.indexOf('a') < 0) {
                        $location.path("/login");
                        event.preventDefault();
                      }
                      */
                    }
                }
            }

        });
        // redirect to installations page if authenticated on app start

        if (AuthService.authentication.isAuth && $location.path().indexOf('/invitation/') < 0 && $location.path().indexOf('/confirmUserNameChange/') < 0) {
            $location.path('/installations');
        }
    }
]);

function cleanCache(LocalStorageService) {
    // clean out old cache
    var localStorageKeys = LocalStorageService.keys();
    angular.forEach(localStorageKeys, function(localStorageKey) {
        if(localStorageKey.indexOf('historycache') > -1) {
            var historyDate = parseInt(localStorageKey.split('-')[1]);
            if(moment().unix() - historyDate >= 86400) { // if it is a day old remove it
                LocalStorageService.remove(localStorageKey);
            }
        }
    });
}
