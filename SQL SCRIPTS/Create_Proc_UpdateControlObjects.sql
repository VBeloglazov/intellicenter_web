-- =============================================================
--
-- Author:		Vladimir Beloglazov
-- Create date: 02/12/2018
--
-- Creates:
--
--		Table Type:			TControlObject, TStringList
--
--		Stored Procedure:	PUpdateControlObjects
--
-- 2018-03-12 VB: With Pathfinder's ugly design they save Property Name to both [Installations]
--                and [ControlObjects]. If "PROPNAME" presented, have to update both.
--
-- 2018-04-17 VB: Attempt to update [Installations] can violate UK_Installations_Name_OwnerUserId, which causes exception.
--				  [Installations] updating separated from [ControlObjects] updating (moved to PUpdateInstallationName).
--
-- =============================================================

USE PathfinderIntelliCenter
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (
	SELECT * FROM sys.objects
	WHERE object_id = OBJECT_ID(N'dbo.PUpdateControlObjects')
	AND type in (N'P', N'PC')
)
DROP PROCEDURE dbo.PUpdateControlObjects
GO

IF  EXISTS (
	SELECT * FROM sys.types st JOIN sys.schemas ss
	ON st.schema_id = ss.schema_id
	WHERE st.name = N'TControlObject' AND ss.name = N'dbo')
DROP TYPE dbo.TControlObject
GO

IF  EXISTS (
	SELECT * FROM sys.types st JOIN sys.schemas ss
	ON st.schema_id = ss.schema_id
	WHERE st.name = N'TStringList' AND ss.name = N'dbo')
DROP TYPE dbo.TStringList
GO

-- -------------------------------------------------------------
--
-- Must match class TControlObject.
-- Must match [ControlObjects] design.
--
-- -------------------------------------------------------------
CREATE TYPE dbo.TControlObject AS TABLE
(
	 ObjNam				nvarchar(16)
	,[Key]				nvarchar(16)
	,Value				nvarchar(256)
)
GO

CREATE TYPE dbo.TStringList AS TABLE
(
	Value				nvarchar(256)
)
GO

-- -------------------------------------------------------------
--
-- Updates objects presented in both the input and the [ControlObjects].
-- Inserts objects presented in the input, but not presented in the [ControlObjects].
-- Deletes objects not presented in the input, but presented in the [ControlObjects].
--
-- -------------------------------------------------------------
CREATE PROCEDURE dbo.PUpdateControlObjects
	 @installationId			AS int
	,@deleteNotPresented		AS bit
	,@controlObjectsPresented	AS TControlObject	READONLY
	,@controlObjectsDeleted		AS TStringList		READONLY

AS
BEGIN

	SET NOCOUNT OFF

	BEGIN TRY

		BEGIN TRANSACTION

		-- Update existing ControlObjects
		UPDATE dbo.ControlObjects
		SET  OBJNAM = UPPER(INP.ObjNam) -- Not necessary, but useful for fixing Pathfinder's CS/CI trick consequences
			,Value = INP.Value
		FROM @controlObjectsPresented INP
		INNER JOIN dbo.ControlObjects DB
			ON DB.OBJNAM = INP.ObjNam
			AND DB.[Key] = INP.[Key]
		WHERE DB.INSTALLATION = @installationId AND ParamType = 'Saved'

		-- Create new ControlObjects
		INSERT INTO dbo.ControlObjects (
				INSTALLATION
			,Revision
			,OBJNAM
			,[Key]
			,Value
			,Deleted
			,ParamType
		)
		SELECT
				@installationId	-- INSTALLATION
			,0					-- Revision
			,UPPER(INP.ObjNam)	-- OBJNAM
			,INP.[Key]			-- Key
			,INP.Value			-- Value
			,0					-- Deleted
			,'Saved'			-- ParamType
		FROM @controlObjectsPresented INP
		LEFT JOIN dbo.ControlObjects DB
			ON DB.INSTALLATION = @installationId
			AND DB.OBJNAM = INP.ObjNam
			AND DB.[Key] = INP.[Key]
			AND DB.ParamType = 'Saved'
		WHERE DB.INSTALLATION IS NULL

		-- Delete existing ControlObjects not presented in the initial WriteParamList (timeSince = 0)
		IF @deleteNotPresented = 1
		BEGIN
			DELETE ControlObjects
			FROM dbo.ControlObjects DB
			LEFT JOIN @controlObjectsPresented INP
				ON INP.ObjNam = DB.OBJNAM
				AND INP.[Key] = DB.[Key]
			WHERE DB.INSTALLATION = @installationId AND ParamType = 'Saved' AND INP.ObjNam IS NULL
		END

		-- Delete existing ControlObjects contained in WriteParamList.Deleted
		DELETE FROM ControlObjects
		WHERE OBJNAM IN (SELECT * FROM @controlObjectsDeleted)
		AND INSTALLATION = @installationId
		AND ParamType = 'Saved'

		COMMIT TRANSACTION

	END TRY
	BEGIN CATCH

		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION

		DECLARE @err_message nvarchar(255)
		SET @err_message = ERROR_MESSAGE()
		RAISERROR (@err_message, 11, 1) -- severity 11-19 will cause returning exception to caller

	END CATCH

END
GO
