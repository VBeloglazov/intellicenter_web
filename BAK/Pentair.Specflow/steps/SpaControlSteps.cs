﻿using System;
using TechTalk.SpecFlow;

namespace Pentair.Specflow.steps
{
    [Binding]
    public class SpaControlSteps
    {
        [When(@"a Spa switch is displayed")]
public void WhenASpaSwitchIsDisplayed()
{
    ScenarioContext.Current.Pending();
}

        [When(@"the associated Spa is ON")]
public void WhenTheAssociatedSpaIsON()
{
    ScenarioContext.Current.Pending();
}

        [When(@"the associated Spa is OFF")]
public void WhenTheAssociatedSpaIsOFF()
{
    ScenarioContext.Current.Pending();
}

        [When(@"the user toggles a Spa switch")]
public void WhenTheUserTogglesASpaSwitch()
{
    ScenarioContext.Current.Pending();
}

        [Then(@"the Spa switch is set to ON")]
public void ThenTheSpaSwitchIsSetToON()
{
    ScenarioContext.Current.Pending();
}

        [Then(@"the Spa switch is set to OFF")]
public void ThenTheSpaSwitchIsSetToOFF()
{
    ScenarioContext.Current.Pending();
}

        [Then(@"the state of the on-screen switch is changed")]
public void ThenTheStateOfTheOn_ScreenSwitchIsChanged()
{
    ScenarioContext.Current.Pending();
}

        [Then(@"the appropriate message is sent to the THW")]
public void ThenTheAppropriateMessageIsSentToTheTHW()
{
    ScenarioContext.Current.Pending();
}
    }
}
