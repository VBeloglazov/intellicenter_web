﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity.EntityFramework;
using Pentair.Domain.Client.Entity;
using Pentair.Domain.Client.Helpers.Entity;

namespace Pentair.Domain.Client.Infrastructure
{
    public class ApiContext : DbContext
    {
        public IDbSet<Installation> Installations { get; set; }
        public IDbSet<Association> Associations { get; set; }
        public IDbSet<Connection> Connections { get; set; }
        public IDbSet<Subscription> Subscriptions { get; set; }
        public IDbSet<ControlObject> ControlObjects { get; set; }
        public IDbSet<EventType> EventTypes { get; set; } 
        public IDbSet<Event> Events { get; set; } 
        public IDbSet<UserInvitation> UserInvitations { get; set; }
        public IDbSet<UserNotification> UserNotifications { get; set; }
        public IDbSet<ManufacturingInfo> ManufacturingInfo { get; set; }
        public IDbSet<NotificationRecipient> NotificationRecipients { get; set; }
        public IDbSet<NotificationMessage> NotificationMessages { get; set; }
        public IDbSet<InstallationView> InstallationViews { get; set; }
    
    } // class ApiContext
}

