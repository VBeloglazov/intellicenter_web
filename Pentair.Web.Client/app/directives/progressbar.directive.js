var PWC = PWC || {};

PWC.directive('progressBar', [function() {
  var directiveDefinitionObject = {
    restrict: 'E',
    replace: false,
    scope: {
      data: '=chartData'
    },
    link: function(scope, element, attrs) {
      scope.$watch("data",function(newValue,oldValue) {
        console.log('we are here'+oldValue.progressValue+ ' ' +newValue.progressValue);
        var scale = (1/scope.data.maxValue)*100;
        var dataRight = [scope.data.progressValue];
        var uniqueId = 'a'+Math.uuid(15); // id's can't start with numbers
        // remove existing content
        element.html('');
        element.append('<div id="'+uniqueId+'"></div>');
        d3.select('#'+uniqueId).attr('class','progressbar');
        var anchorRight = d3.select("#"+uniqueId);
        //Bind data for bars
        var divRight = anchorRight.selectAll("#"+uniqueId+" div")
          .data(dataRight);
        //------------------------------------------
        divRight.enter().append("div")
          .attr("class", "shadow-hidden");

        var triangleBar = divRight.enter().append("div")
          .attr("class", "shadow-hidden");

        d3.select("body").selectAll(".shadow-hidden")
          .append("div")
          .attr("class","bar-hidden");

        d3.select("body").selectAll(".bar-hidden")
          .append("div")
          .attr("class","path-hidden path2");

        d3.select('#'+uniqueId+' .bar-hidden:nth-child(1)').attr('style','left:-26px');

        d3.select("body").selectAll(".path-hidden")
          .append("div")
          .attr("class","pattern-hidden patt");
        //-------------------------------------------

        divRight.enter().append("div")
          .attr("class", "shadow");

        //Create the bar
        d3.select("#"+uniqueId).selectAll(".shadow")
          .append("div")
          .attr("class","bar");

        //Create the path
        d3.select("#"+uniqueId).selectAll(".bar")
          .append("div")
          .attr("class","path path2");

        //Add the pattern for the bar
        d3.select("#"+uniqueId).selectAll(".path")
          .append("div")
          .attr("class","pattern patt");

          // change duration from 1000 to 0
        //value
        //add the percentage to the progress bar and transition the number
        d3.select("#"+uniqueId).select("div.pattern-hidden:nth-child(1)")
          .append("div")
          .text(0)
          .attr("class", "percentage")
          .transition()
          .duration(0)
          .style("left", function(d, i) {
            return d * (scale) + "%";
          })
          .tween(".percentage", function(d) { //increasing value
            var i = d3.interpolate(this.textContent, d);
            var prec = (d + "").split(".");
            var round = (prec.length > 1) ? Math.pow(10, prec[1].length) : 1;
            return function(t) {
              this.textContent = Math.round(i(t) * round) / round + "";
            };
          });
        //triangle
        triangleBar.select("#"+uniqueId+" div.pattern-hidden:nth-child(1)").append('svg').attr('id','trisvg').attr('width','15px')
          .attr('height','25px').attr('style','display:inline-block;position:absolute;left:0%')
          .append('g')
          .attr('id',"triangle")
          .append('polyline')
          .attr('points', '0 0, 7.5 11.25, 15 0')
          .attr("transform", "translate(0,8)")
          .style('fill', '#ff714b');

          // change duration from 1000 to 0
        d3.select("#"+uniqueId).selectAll("#trisvg")
          .transition()
          .duration(0)
          .style("left", function(d, i) {
            return d * (scale) + "%";
          });

          // change duration from 1000 to 0
        //transition the width of the path
        d3.select("#"+uniqueId).selectAll(".path")
          .transition()
          .duration(0)
          .style("width", function(d, i) {
            return d * (scale) + "%";
          });
      });
    }
  };
  return directiveDefinitionObject;
}]);