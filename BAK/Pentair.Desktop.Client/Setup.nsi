;--------------------------------------------------------------------------
; PentairSetup.nsi
;
; This script installs the Pentair application and also remember the directories, 
; has uninstall support and (optionally) installs start menu shortcuts.
;
; It will install PentairSetup.nsi into a directory that the user selects.
;--------------------------------------------------------------------------

; Include the required nsh script files
;-----------------------------------
!include "CheckMSIVersion.nsh"
!include "FileAssociation.nsh"
!include "LogicLib.nsh"
!include "MUI2.nsh"
!include "DotNetChecker.nsh"
!include nsDialogs.nsh
!include "FileFunc.nsh"
!include "Registry.nsh"

; The name of the installer
;-----------------------------------
Name "Pentair Desktop"

; Clear the nullsoft branding text
;-----------------------------------
BrandingText " "

XPStyle on

; The controls for the custom dialog page
;-----------------------------------
Var FileTypeAssociationDialog
Var YesRadioButton
Var NoRadioButton
Var YesRadioButton_State
Var NoRadioButton_State

Var CurrentRegValue
Var WindowsVersion

;Define Installer Properties
;-----------------------------------
VIProductVersion "2.7.5.5"
VIAddVersionKey /LANG=1033-English "ProductName" "Pentair Pool Controller"
VIAddVersionKey /LANG=1033-English "CompanyName" "Pentair"
VIAddVersionKey /LANG=1033-English "LegalTrademarks" ""
VIAddVersionKey /LANG=1033-English "LegalCopyright" "© Pentair Corporation"
VIAddVersionKey /LANG=1033-English "FileDescription" "Pentair Controller"
VIAddVersionKey /LANG=1033-English "FileVersion" "1.0"

; The file to write
;-----------------------------------
OutFile "DesktopSetup.exe"

; The default installation directory
;-----------------------------------
InstallDir "$PROGRAMFILES\Pentair"

; Registry key to check for directory (so if you install again, it will 
; overwrite the old one automatically)
;-----------------------------------
InstallDirRegKey HKLM "Software\Pentair" "Install_Dir"

; Request application privileges for Windows Vista
RequestExecutionLevel admin

;The icons for the installer and uninstaller
;-----------------------------------
!define MUI_ICON Pentair.ico
!define MUI_UNICON Pentair.ico

!define MUI_ABORTWARNING
!define PROC 0 

;The class and title for the application
;-----------------------------------
!define WNDCLASS "Pentair.Desktop.Client"                  
!define WNDTITLE "Pentair"

;Define a function that will kick off the application after the installation
;-----------------------------------
!define MUI_FINISHPAGE_RUN
!define MUI_FINISHPAGE_RUN_CHECKED
!define MUI_FINISHPAGE_RUN_TEXT "Start Pentair"
!define MUI_FINISHPAGE_RUN_FUNCTION "LaunchApplication"

;Installer Pages
;--------------------------------
  !insertmacro MUI_PAGE_WELCOME
  ; License page text 
  ;-------------------------------- 
  !insertmacro MUI_PAGE_INSTFILES
  !insertmacro MUI_PAGE_FINISH
  
;Uninstaller Pages
;--------------------------------
  !insertmacro MUI_UNPAGE_WELCOME
  !insertmacro MUI_UNPAGE_CONFIRM
  !insertmacro MUI_UNPAGE_INSTFILES
  !insertmacro MUI_UNPAGE_FINISH
 
; Language 
;--------------------------------
 !insertmacro MUI_LANGUAGE "English"
 
; The install section
;-----------------------------------
Section "PentairSetup (required)"
   
  SectionIn RO

  AddSize 350000
  
  ; check windows MSI installer 3.0
  !insertmacro CheckMSIVersion
  
  AddSize 350000

  ; Set output path to the installation directory.
  SetOutPath $INSTDIR

  Call CheckBonjourInstall
  StrCmp $0 "continue" +2
  Abort
      
  ; check IE Version
  Call GetIEVersion
  Pop $R0
  IntCmp $R0 9 +3 0 +3
  MessageBox MB_ICONSTOP "Requires Internet Explorer Version 10 or greater."
  Abort

  ; check .NET Version
  Call CheckAndDownloadDotNet451

  installed:

  ; Include the files for the install package
  File "Setup.nsi"
  File "Pentair.exe"
  File "pentair.exe.config"
  File "Newtonsoft.Json.dll"
  SetOutPath $INSTDIR\site
  File /r "..\..\..\Pentair.web.client\*.*"
  
  ; Write the installation path into the registry
  WriteRegStr HKLM "SOFTWARE\Pentair" "Install_Dir" "$INSTDIR"
  
  ; Write the uninstall keys for Windows
 ${GetSize} "$INSTDIR" "/S=0K" $0 $1 $2
 IntFmt $0 "0x%08X" $0
 WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Pentair" "EstimatedSize" "$0"

  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Pentair" "DisplayName" "Pentair"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Pentair" "Publisher" "Pentair"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Pentair" "DisplayVersion" "1.0"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Pentair" "UninstallString" '"$INSTDIR\uninstall.exe"'
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Pentair" "NoModify" 1
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Pentair" "NoRepair" 1
  
 SectionEnd

; Start Menu shortcuts section
;-----------------------------------
Section "Start Menu Shortcuts"

  CreateDirectory "$SMPROGRAMS\Pentair"
  CreateShortCut "$SMPROGRAMS\Pentair\Pentair Controller.lnk" "$INSTDIR\Pentair.exe" "" "$INSTDIR\Pentair.exe" 0
  CreateShortCut "$SMPROGRAMS\Pentair\Pentair Uninstall.lnk" "$INSTDIR\Pentairuninstall.exe" "" "$INSTDIR\uninstall.exe" 0
  CreateShortCut "$DESKTOP\Pentair Controller.lnk" "$INSTDIR\Pentair.exe" ""

SectionEnd


; Uninstall Section
;-----------------------------------
Section "Uninstall"
  SetShellVarContext all

  ; Remove registry keys
  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Pentair"
  DeleteRegKey HKLM "SOFTWARE\Pentair"
  
  ReadRegStr $CurrentRegValue HKCR .pdf ""
  
  ; Remove all the files 
  Delete "$INSTDIR\Setup.nsi"
  Delete "$INSTDIR\Pentair.exe"
  Delete "$INSTDIR\uninstall.exe"
  Delete "$INSTDIR\Pentair.exe.config"
  Delete "$INSTDIR\Newtonsoft.Json.dll"

  ; Remove shortcuts, if any
  Delete "$SMPROGRAMS\Pentair\*.*"
  Delete "$DESKTOP\Pentair.lnk"

  ; Remove directories used
  RMDir /r "$PROGRAMFILES\Pentair"
  RMDir /r "$SMPROGRAMS\Pentair"
  RMDir /r "$DESKTOP\Pentair"
  RMDir /r "INSTDIR\site"
SectionEnd

; On install init
;-----------------------------------
Function .onInit
  
  ; Prevent multiple instances of installer
  System::Call 'kernel32::CreateMutexA(i 0, i 0, t "myMutex") i .r1 ?e'
  Pop $R0
 
  StrCmp $R0 0 +3
  MessageBox MB_OK|MB_ICONEXCLAMATION "The installer is already running."
  Abort

  ; shut down app before running the install
  FindWindow $0 "${WNDCLASS}" "${WNDTITLE}"
  
   StrCmp $0 0 continueInstall
     goto oninitAbort  
  
 oninitAbort:
 
    MessageBox MB_ICONSTOP|MB_OK "Installation cannot proceed when the application is already running. Please close it and try again."
    Abort

 continueInstall:
 
  ; Kill any running instances of Pentair running in the background
  Strcpy $0 "PentairSimulator.exe"
  KillProc::KillProcesses
 
  System::Call "kernel32::GetCurrentProcess() i .s"
  System::Call "kernel32::IsWow64Process(i s, *i .r0)"
  
  IntCmp $0 0 is32
  
  Strcpy $WindowsVersion "64"
  goto exitfunction
    
  is32:
    Strcpy $WindowsVersion "32"
    
  exitfunction:

FunctionEnd

; On Uninstall init
;-----------------------------------
Function un.onInit
  ; Use the 32 bit registry entries
  SetRegView 32

  ; Prevent multiple instances of the installer
  System::Call 'kernel32::CreateMutexA(i 0, i 0, t "myMutex") i .r1 ?e'
  Pop $R0
 
  StrCmp $R0 0 +3
  MessageBox MB_OK|MB_ICONEXCLAMATION "The UnInstaller is already running."
  Abort
  
  ; shut down app before uninstall
  FindWindow $0 "${WNDCLASS}" "${WNDTITLE}"
  
  StrCmp $0 0 continueUnInstall
  goto unoninitAbort

unoninitAbort:
    MessageBox MB_ICONSTOP|MB_OK "The application is running. Please close it and try uninstalling again."
    Abort
 
 continueUnInstall:
   ; Kill any running instances of Pentair running in the background
  Strcpy $0 "PentairSimulator.exe"
  KillProc::KillProcesses
 
  System::Call "kernel32::GetCurrentProcess() i .s"
  System::Call "kernel32::IsWow64Process(i s, *i .r0)"
FunctionEnd

; Function to create custom file type association page
;-----------------------------------
Function nsDialogsPage

        !insertmacro MUI_HEADER_TEXT "Default application" "Do you want Pentair to be the default application for PDF Documents?" 

    nsDialogs::Create 1018
    Pop $FileTypeAssociationDialog
    
    ${If} $FileTypeAssociationDialog == error
      Abort
    ${EndIf}
        
        ${NSD_CreateRadioButton} 0 10u 100% 10u "&Yes"
        Pop $YesRadioButton
        
        ${NSD_Check} $YesRadioButton
            
    ${NSD_CreateRadioButton} 0 30u 100% 10u "&No"
        Pop $NoRadioButton 
        
        ${NSD_Uncheck} $NoRadioButton
        
        nsDialogs::Show
    
FunctionEnd

Function nsDialogsPageLeave

    ${NSD_GetState} $YesRadioButton $YesRadioButton_State
    ${NSD_GetState} $NoRadioButton $NoRadioButton_State

FunctionEnd

;Function to create custom directory page, not to be displayed during an upgrade
;----------------------------------
Function dirPre

    ReadRegStr $0 HKLM "Software\Pentair" "Install_Dir"

    ${If} $0 != ""
        Abort
    ${EndIf}

FunctionEnd  

Function LaunchApplication
  ExecShell "" "$INSTDIR\Pentair.exe"
FunctionEnd

 Function CheckAndDownloadDotNet451
    # Let's see if the user has the .NET Framework 4.5 installed on their system or not
    # Remember: you need Vista SP2 or 7 SP1.  It is built in to Windows 8, and not needed
    # In case you're wondering, running this code on Windows 8 will correctly return is_equal
    # or is_greater (maybe Microsoft releases .NET 4.5 SP1 for example)
 
    # Set up our Variables
    Var /GLOBAL dotNET451IsThere
    Var /GLOBAL dotNET_CMD_LINE
    Var /GLOBAL EXIT_CODE
 
        # We are reading a version release DWORD that Microsoft says is the documented
        # way to determine if .NET Framework 4.51 is installed
    DetailPrint "Checking for Version 4.51 Dot Net Assemblies"

    ReadRegDWORD $dotNET451IsThere HKLM "SOFTWARE\Microsoft\NET Framework Setup\NDP\v4\Full" "Release"
    IntCmp $dotNET451IsThere 378675 is_equal is_less is_greater
 
    is_equal:
        Goto done_compare_not_needed
    is_greater:
        # Useful if, for example, Microsoft releases .NET 4.5 SP1
        # We want to be able to simply skip install since it's not
        # needed on this system
        Goto done_compare_not_needed
    is_less:
        Goto done_compare_needed
 
    done_compare_needed:
        DetailPrint "Installing Microsoft Dot Net"

        #.NET Framework 4.51 install is *NEEDED*
 
        # Microsoft Download Center EXE:
        # Web Bootstrapper: http://go.microsoft.com/fwlink/?LinkId=225704
        # Full Download: http://go.microsoft.com/fwlink/?LinkId=225702
 
        # Setup looks for components\dotNET45Full.exe relative to the install EXE location
        # This allows the installer to be placed on a USB stick (for computers without internet connections)
        # If the .NET Framework 4.5 installer is *NOT* found, Setup will connect to Microsoft's website
        # and download it for you
 
        # Reboot Required with these Exit Codes:
        # 1641 or 3010
 
        # Command Line Switches:
        # /showrmui /passive /norestart
 
        # Silent Command Line Switches:
        # /q /norestart
 
 
        # Let's see if the user is doing a Silent install or not
        IfSilent is_quiet is_not_quiet
 
        is_quiet:
            StrCpy $dotNET_CMD_LINE "/q /norestart"
            Goto LookForLocalFile
        is_not_quiet:
            StrCpy $dotNET_CMD_LINE "/showrmui /passive /norestart"
            Goto LookForLocalFile
 
        LookForLocalFile:
            # Let's see if the user stored the Full Installer
            IfFileExists "$EXEPATH\components\dotNET451Full.exe" do_local_install do_network_install
 
            do_local_install:
                # .NET Framework found on the local disk.  Use this copy
 
                ExecWait '"$EXEPATH\components\dotNET451Full.exe" $dotNET_CMD_LINE' $EXIT_CODE
                Goto is_reboot_requested
 
            # Now, let's Download the .NET
            do_network_install:
 
                Var /GLOBAL dotNetDidDownload
                NSISdl::download "http://go.microsoft.com/fwlink/p/?LinkId=310158" "$TEMP\dotNET451Web.exe" $dotNetDidDownload
 
                StrCmp $dotNetDidDownload success fail
                success:
                    ExecWait '"$TEMP\dotNET451Web.exe" $dotNET_CMD_LINE' $EXIT_CODE
                    Goto is_reboot_requested
 
                fail:
                    MessageBox MB_OK|MB_ICONEXCLAMATION "Unable to download .NET Framework.  Pentair will be installed, but will not function without the Framework!"
                    Goto done_dotNET_function
 
                # $EXIT_CODE contains the return codes.  1641 and 3010 means a Reboot has been requested
                is_reboot_requested:
                    ${If} $EXIT_CODE = 1641
                    ${OrIf} $EXIT_CODE = 3010
                        SetRebootFlag true
                    ${EndIf}
 
    done_compare_not_needed:
        # Done dotNET Install
        Goto done_dotNET_function
 
    #exit the function
    done_dotNET_function:
 
    FunctionEnd

;--------------------------------
; CheckBonjour
;
; Written by Jim Hewitt (jhewitt@bhst.com)
;
; For this function Apple Bonjour must be installed on the computer.
Function CheckBonjourInstall
    ClearErrors
	EnumRegKey $0 HKLM "SOFTWARE\Apple Inc." 0
    IfErrors InstallBonjour Continue

	InstallBonjour:
	MessageBox MB_OKCANCEL "This product requires the Bonjour Discovery Service from Apple be installed, proceed?" IDOK BonjourInstall IDCANCEL CancelInstall

	BonjourInstall:
	StrCpy $0 "abort"
	Goto ExitCheckBonjour

	CancelInstall:
	StrCpy $0 "abort"
	Goto ExitCheckBonjour
	 
	Continue:
	StrCpy $0 "continue"

    ExitCheckBonjour:
 
FunctionEnd

Function StrTok
  Exch $R1
  Exch 1
  Exch $R0
  Push $R2
  Push $R3
  Push $R4
  Push $R5
 
  ;R0 fullstring
  ;R1 tokens
  ;R2 len of fullstring
  ;R3 len of tokens
  ;R4 char from string
  ;R5 testchar
 
  StrLen $R2 $R0
  IntOp $R2 $R2 + 1
 
  loop1:
    IntOp $R2 $R2 - 1
    IntCmp $R2 0 exit
 
    StrCpy $R4 $R0 1 -$R2
 
    StrLen $R3 $R1
    IntOp $R3 $R3 + 1
 
    loop2:
      IntOp $R3 $R3 - 1
      IntCmp $R3 0 loop1
 
      StrCpy $R5 $R1 1 -$R3
 
      StrCmp $R4 $R5 Found
    Goto loop2
  Goto loop1
 
  exit:
  ;Not found!!!
  StrCpy $R1 ""
  StrCpy $R0 ""
  Goto Cleanup
 
  Found:
  StrLen $R3 $R0
  IntOp $R3 $R3 - $R2
  StrCpy $R1 $R0 $R3
 
  IntOp $R2 $R2 - 1
  IntOp $R3 $R3 + 1
  StrCpy $R0 $R0 $R2 $R3
 
  Cleanup:
  Pop $R5
  Pop $R4
  Pop $R3
  Pop $R2
  Exch $R0
  Exch 1
  Exch $R1
 
FunctionEnd

 ; GetIEVersion
 ;
 ; Based on Yazno's function, http://yazno.tripod.com/powerpimpit/
 ; Returns on top of stack
 ; 1-6 (Installed IE Version)
 ; or
 ; '' (IE is not installed)
 ;
 ; Usage:
 ;   Call GetIEVersion
 ;   Pop $R0
 ;   ; at this point $R0 is "5" or whatnot

 Function GetIEVersion
 Push $R0
   ClearErrors
   ReadRegStr $R0 HKLM "Software\Microsoft\Internet Explorer" "Version"
   IfErrors lbl_123 lbl_456

   lbl_456: ; ie 4+
     Strcpy $R0 $R0 1
   Goto lbl_done

   lbl_123: ; older ie version
     ClearErrors
     ReadRegStr $R0 HKLM "Software\Microsoft\Internet Explorer" "IVer"
     IfErrors lbl_error

       StrCpy $R0 $R0 3
       StrCmp $R0 '100' lbl_ie1
       StrCmp $R0 '101' lbl_ie2
       StrCmp $R0 '102' lbl_ie2

       StrCpy $R0 '3' ; default to ie3 if not 100, 101, or 102.
       Goto lbl_done
         lbl_ie1:
           StrCpy $R0 '1'
         Goto lbl_done
         lbl_ie2:
           StrCpy $R0 '2'
         Goto lbl_done
     lbl_error:
       StrCpy $R0 ''
   lbl_done:
   Exch $R0
 FunctionEnd