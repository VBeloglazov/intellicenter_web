﻿/* TODO: 2b removed
'use strict';
describe('ChemistryController', function () {
    var propertyModel;
    var $scope;
    var $filter;
    var propertyService;
    var ChemistryController;
    var $controller;
    var messageProcessor;
    beforeEach(module('PentairWebClient'));

    beforeEach(inject(function (_$controller_, _$rootScope_, _$filter_, _PropertyService_, _PropertyModel_, _MessageProcessor_) {
        $scope = _$rootScope_.$new();
        propertyModel = _PropertyModel_;
        $filter = _$filter_;
        $controller = _$controller_;
        propertyService = _PropertyService_;

        messageProcessor = _MessageProcessor_;

        ChemistryController = $controller("ChemistryController", { $scope: $scope });
    }));

    describe('Default Vars', function () {
        it('should have the following default variables', function () {
            expect($scope.Property).toBeDefined();
            expect($scope.AllBodies).toEqual([]);
        });
    });
    describe('getBodies', function () {
        beforeEach(function () {
            messageProcessor.ProcessNotification(JSON.parse(SpecHelper.JSONdata.Bodies));
            messageProcessor.ProcessNotification(JSON.parse(SpecHelper.JSONdata.Chemistry));
            messageProcessor.ProcessNotification(JSON.parse(SpecHelper.JSONdata.PageSubscriptions.Chemistry));

            ChemistryController = $controller("ChemistryController", { $scope: $scope });
        });

        it('should have the following default variables', function () {
            expect($scope.AllBodies.length).toEqual(3);
            expect($scope.AllBodies[0].parent.OBJNAM).toEqual('B1002');
            expect($scope.AllBodies[0].child.OBJNAM).toEqual('B1003');
            expect($scope.AllBodies[0].child.CHEMISTRY_debug.ICHEM.OBJNAM).toEqual('I2573');
            expect($scope.AllBodies[0].child.CHEMISTRY_debug.ICHLOR.OBJNAM).toEqual('I2572');
        });
    });
});
*/