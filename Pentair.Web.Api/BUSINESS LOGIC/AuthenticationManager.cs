using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Security;
using System.Web.WebPages;
using System.Net;

using Microsoft.AspNet.Identity.EntityFramework;

using Newtonsoft.Json;

using SERVICE_SHARED;

namespace Pentair.Web.Api
{
    public static class AuthenticationManager
    {
        private const string EVENT_SOURCE = "AuthenticationManager";

        #region Public Methods

        /// <summary>
        /// Returns Device ID or 0.
        /// </summary>
        public static int AuthenticateDevice(
            string message)
        {
            int deviceId = 0;

            try
            {
                AuthenticationMessage input = JsonConvert.DeserializeObject<AuthenticationMessage>(message);

                int propertyId;
                if (String.IsNullOrEmpty(input.PropertyID) || !int.TryParse(input.PropertyID, out propertyId))
                {
                    throw new Exception("Invalid Property ID = '" + input.PropertyID + "'");
                }

                // TODO: 2018-03-22 VB: What if the property is already connected?
                //                      Shall we close the "old" connection and allow the "new" one?

                using (DbContextPentair db = DbContextPentair.New())
                {
                    Installation installationDB = db.GetInstallation(propertyId);

                    if (installationDB == null)
                    {
                        throw new Exception("Installation not found for Property Id = " + propertyId);
                    }

                    if (input.SharedSecret != installationDB.Secret)
                    {
                        throw new Exception("Invalid Shared Secret = '" + input.SharedSecret + "'");
                    }

                } // using (DbContextPentair db)

                if (WebApiApplication.LogVerbose)
                {
                    LogManager.LogInformationAsync(
                        source: EVENT_SOURCE,
                        eventBriefDescription: "Authenticated",
                        installationId: propertyId,
                        userId: Guid.Empty,
                        connectionId: Guid.Empty,
                        eventFullDescription: String.Empty
                    );
                }

                deviceId = propertyId;
            }
            catch (Exception e)
            {
                LogManager.LogWarningAsync(
                    source: EVENT_SOURCE,
                    eventBriefDescription: "Authentication failed",
                    installationId: 0,
                    userId: Guid.Empty,
                    connectionId: Guid.Empty,
                    eventFullDescription:
                        String.Format("Authentication request: {0}\n\n{1}",
                            message,
                            ExceptionManager.CustomizeException(e).ErrorMessage(
                                errorHeader: null,
                                addStackTrace: true
                            )
                        ),
                    saveEventLog: true
                );
            }

            return deviceId;

        } // AuthenticateDevice()

        public static RegisteredPropertiesModelResponse RegisteredProperties(
            string message)
        {
            RegisteredPropertiesModelResponse response;

            try
            {
                AccountController accountController = new AccountController(
                    new ApplicationUserManager()
                );

                RegisteredPropertiesMessage input =
                    JsonConvert.DeserializeObject<RegisteredPropertiesMessage>(message);

                string userIdTxt = accountController.ValidateUser(input.UserID, input.Password);

                if (!string.IsNullOrEmpty(userIdTxt))
                {
                    Guid userId = new Guid(userIdTxt);

                    List<InstallationModel> objectList = new List<InstallationModel>();

                    using (DbContextPentair db = DbContextPentair.New())
                    {
                        // TODO: It converts [Installations] record to UserInstallation,
                        //       Then it converts UserInstallation to InstallationModel.
                        //       Make SQL return list of InstallationModel objects.

                        IEnumerable<UserInstallation> properties = db.UserInstallationListWithDetails(userId);

                        foreach (UserInstallation property in properties)
                        {
                            objectList.Add(
                                new InstallationModel(
                                    installationID: property.InstallationId.ToString(),
                                    poolName: property.PoolName,
                                    address: property.Address,
                                    city: property.City,
                                    state: property.State,
                                    zip: property.Zip,
                                    onLine: property.OnLine ? "YES" : "NO"
                                )
                            );
                        }

                    } // using (DbContextPentair db)

                    response = new RegisteredPropertiesModelResponse(
                        command: CommandString.GetRegisteredProperties,
                        messageID: input.MessageID,
                        response: ResponseString.OK,
                        objectList: objectList
                    );
                }
                else
                {
                    LogManager.LogWarningAsync(
                        source: EVENT_SOURCE,
                        eventBriefDescription: "User not authorized",
                        installationId: 0,
                        userId: Guid.Empty,
                        connectionId: Guid.Empty,
                        eventFullDescription:
                            String.Format("Invalid user credentials: \"{0}\"/\"{1}\"",
                                input.UserID,
                                input.Password
                            ),
                        saveEventLog: true
                    );

                    response = new RegisteredPropertiesModelResponse(
                        command: CommandString.GetRegisteredProperties,
                        messageID: input.MessageID,
                        response: ResponseString.Unauthorized,
                        objectList: new List<InstallationModel>()
                    );
                }
            }
            catch (Exception ex)
            {
                LogManager.LogErrorAsync(
                    source: EVENT_SOURCE,
                    eventBriefDescription: "Registered Properties error",
                    installationId: 0,
                    userId: Guid.Empty,
                    connectionId: Guid.Empty,
                    eventFullDescription:
                        ExceptionManager.CustomizeException(ex).ErrorMessage(
                            errorHeader: null,
                            addStackTrace: true
                        )
                );

                response = new RegisteredPropertiesModelResponse(
                    command: CommandString.GetRegisteredProperties,
                    messageID: String.Empty,
                    response: ResponseString.BadRequest,
                    objectList: null
                );
            }

            return response;
        
        } // RegisteredProperties()

        public static RegistrationResponse RegisterDevice(
            string serializedMessage)
        {
            try
            {
                RegistrationMessage registrationMessage =
                    JsonConvert.DeserializeObject<RegistrationMessage>(serializedMessage);

                string userIdTxt = GetUserId(registrationMessage);

                // We have a valid user, implement the registration logic
                if (!string.IsNullOrEmpty(userIdTxt))
                {
                    Guid userId = Guid.Parse(userIdTxt);

                    bool reRegister =
                        registrationMessage.HasMoved.ToUpper() == "YES" ||
                        registrationMessage.HasMoved.ToUpper() == "TRUE";

                    if (reRegister)
                    {
                        return ReRegisterProperty(registrationMessage, userId);
                    }
                    else
                    {
                        return CreateNewProperty(registrationMessage, userId);
                    }
                }
                else
                {
                    LogManager.LogWarningAsync(
                        source: EVENT_SOURCE,
                        eventBriefDescription: "User not authorized",
                        installationId: 0,
                        userId: Guid.Empty,
                        connectionId: Guid.Empty,
                        eventFullDescription:
                            String.Format("Invalid user credentials: \"{0}\"/\"{1}\"",
                                registrationMessage.UserName,
                                registrationMessage.Password
                            ),
                            saveEventLog: true
                    );

                    return new RegistrationResponse(
                        command: CommandString.AcknowledgeRegistration,
                        messageID: String.Empty,
                        propertyID: String.Empty,
                        sharedSecret: String.Empty,
                        response: ResponseString.Unauthorized
                    );
                }
            }
            catch (Exception ex)
            {
                LogManager.LogErrorAsync(
                    source: EVENT_SOURCE,
                    eventBriefDescription: "Register device error",
                    installationId: 0,
                    userId: Guid.Empty,
                    connectionId: Guid.Empty,
                    eventFullDescription:
                        ExceptionManager.CustomizeException(ex).ErrorMessage(
                            errorHeader: null,
                            addStackTrace: true
                        )
                );

                return new RegistrationResponse(
                    command: CommandString.AcknowledgeRegistration,
                    messageID: String.Empty,
                    propertyID: String.Empty,
                    sharedSecret: String.Empty,
                    response: ResponseString.InternalServerError
                );
            }
        
        } // RegisterDevice()

        #endregion Public Methods

        #region Private Methods

        private static string GetUserId(
            RegistrationMessage input)
        {
            var accountController = new AccountController(new ApplicationUserManager());

            string usrId = accountController.ValidateUser(input.UserName, input.Password);

            // If the user does not exist and the create flag is set to true,
            // create a new user.
            if (String.IsNullOrEmpty(usrId) &&
                (input.CreateNew.ToUpper() == "YES" || input.CreateNew.ToUpper() == "TRUE"))
            {
                usrId = accountController.CreateUser(input.UserName, input.Password);
            }

            return usrId;
        
        } // GetUserId()

        /// <exception cref="Exception" />
        private static RegistrationResponse CreateNewProperty(
            RegistrationMessage input,
            Guid userId)
        {
            CreateNewPropertyReturn ret;

            if (!string.IsNullOrWhiteSpace(input.Name))
            {
                using (DbContextPentair db = DbContextPentair.New())
                {
                    ret = db.CreateNewProperty(input.Name, userId, StaticData.Admin);

                } // using (DbContextPentair db)
            }
            else
            {
                // input.Name is null or empty
                ret = new CreateNewPropertyReturn() { HttpStatusCode = (int)HttpStatusCode.BadRequest };
            }

            return 
                new RegistrationResponse(
                    command: CommandString.AcknowledgeRegistration,
                    messageID: input.MessageID,
                    propertyID: ret.InstallationId.ToString(),
                    sharedSecret: (!String.IsNullOrWhiteSpace(ret.NewSecret)) ? ret.NewSecret : String.Empty,
                    response: ret.HttpStatusCode.ToString()
                );

        } // CreateNewProperty()

        private static RegistrationResponse ReRegisterProperty(
            RegistrationMessage input,
            Guid userId)
        {
            ReRegisterPropertyReturn ret;

            int propertyId = 0;
            if (int.TryParse(input.PropertyID, out propertyId))
            {
                using (DbContextPentair db = DbContextPentair.New())
                {
                    ret = db.ReRegisterProperty(propertyId, userId);

                } // using (DbContextPentair db)
            }
            else
            {
                // Cannot parse input.PropertyID
                ret = new ReRegisterPropertyReturn() { HttpStatusCode = (int)HttpStatusCode.BadRequest };
            }

            if (ret.HttpStatusCode != (int)HttpStatusCode.OK && ret.HttpStatusCode != (int)HttpStatusCode.Created)
            {
                LogManager.LogWarningAsync(
                   source: EVENT_SOURCE,
                   eventBriefDescription: "Re-register device error",
                   installationId: propertyId,
                   userId: userId,
                   connectionId: Guid.Empty,
                   eventFullDescription: String.Format("Returning {0} back to client..."),
                   saveEventLog: true
               );
            }

            return
                new RegistrationResponse(
                    command: CommandString.AcknowledgeRegistration,
                    messageID: input.MessageID,
                    propertyID: propertyId.ToString(),
                    sharedSecret: (!String.IsNullOrWhiteSpace(ret.NewSecret)) ? ret.NewSecret : String.Empty,
                    response: ret.HttpStatusCode.ToString()
                );
        
        } // ReRegisterProperty()

        #endregion Private Methods

    } // class AuthenticationManager
}