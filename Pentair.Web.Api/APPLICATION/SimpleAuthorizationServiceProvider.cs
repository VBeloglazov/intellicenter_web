﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;

using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security.OAuth;

using SERVICE_SHARED;

namespace Pentair.Web.Api
{
    public class SimpleAuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        private const string EVENT_SOURCE = "SimpleAuthorizationServerProvider";

        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
            return Task.FromResult(true);
        }

        public override async Task GrantResourceOwnerCredentials(
            OAuthGrantResourceOwnerCredentialsContext context)
        {
            try
            {
                context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });
                var userId = string.Empty;
                using (var repo = new AuthRepository())
                {
                    ApplicationUser user = await repo.FindUser(context.UserName, context.Password);

                    if (user == null)
                    {
                        throw new Exception("User not found or invalid password");
                    }

                    userId = user.Id;
                }

                var identity = new ClaimsIdentity(context.Options.AuthenticationType);
                identity.AddClaim(new Claim("sub", context.UserName));
                identity.AddClaim(new Claim(ClaimTypes.Name, userId));
                identity.AddClaim(new Claim("role", "user"));

                context.Validated(identity);
            }
            catch (Exception e)
            {
                context.SetError("invalid_grant", "User authorization failed.");

                LogManager.LogWarningAsync(
                    source: EVENT_SOURCE,
                    eventBriefDescription: "User authorization failed",
                    installationId: 0,
                    userId: Guid.Empty,
                    connectionId: Guid.Empty,
                    eventFullDescription:
#if DEBUG
                        String.Format("UserName: \"{0}\"\nPassword: \"{1}...\"\n\n{2}",
                            context.UserName,
                            context.Password[0],
                            ExceptionManager.CustomizeException(e).ErrorMessage(null, true)
                        ),
#else
                        String.Format("UserName: \"{0}\"\n\n{1}",
                            context.UserName,
                            ExceptionManager.CustomizeException(e).ErrorMessage(null, true)
                        ),
#endif
                    saveEventLog: true
                );
            }

        } // GrantResourceOwnerCredentials()

    } // class SimpleAuthorizationServerProvider

    public class QueryStringOAuthBearerProvider : OAuthBearerAuthenticationProvider
    {
        readonly string _name;

        public QueryStringOAuthBearerProvider(string name)
        {
            _name = name;
        }

        public override Task RequestToken(OAuthRequestTokenContext context)
        {
            var value = context.Request.Query.Get(_name) ?? context.Request.Headers.Get("Authorization");
            if (!string.IsNullOrEmpty(value))
            {
                context.Token = value;
            }

            return Task.FromResult<object>(null);
        }
    }

}