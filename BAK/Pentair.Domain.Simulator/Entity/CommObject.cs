﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pentair.Domain.Simulator.Entity
{
    public class CommObject
    {
        [Key]
        public int ID { get; set; }
        public string Name { get; set; }
        public int Port { get; set; }
        public string MacAddress { get; set; }
        public int MaxSpeed { get; set; }
        public int SessionInfo { get; set; }
        public int Status { get; set; }

        // Foreign Keys
        public int CommTypeID { get; set; }     // TODO What is this?
        public int AssignedID { get; set; }
        public int ParentID { get; set; }
    }
}