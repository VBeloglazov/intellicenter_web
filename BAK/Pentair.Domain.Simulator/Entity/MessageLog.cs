﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pentair.Domain.Simulator.Entity
{
    public class MessageLog
    {
        [Required, Key]
        public int ID { get; set; }

        [Required]
        public string Action { get; set; }

        public string Message { get; set; }

        [Required]
        public DateTime Timestamp { get; set; }

        [Required]
        public Guid HardwareID { get; set; }
    }
}
