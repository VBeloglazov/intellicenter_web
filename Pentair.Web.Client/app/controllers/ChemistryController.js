var PWC = PWC || {};
PWC.controller('ChemistryController', [
    '$scope', '$filter', '$interval', 'PropertyService', 'PropertyModel',
    function ($scope, $filter, $interval, propertyService, propertyModel) {
        'use strict';
        $scope.Property = propertyService;
        $scope.Model = propertyModel;
        $scope.$parent.Layout = "layout-base";
        $scope.AllBodies = [];
        getBodies();

        $scope.BODYPOOL = "B1101";
        $scope.BODYSPA = "B1202";
        $scope.BODYPOOLSPA = "B1101 B1202";


        $scope.displayTankValue = function(tankValue) { // for some reason intellichlor returns 1-7 instead of 0-6
            if(!tankValue) {
                return 0;
            }
            if(!isNaN(tankValue)) {
                if(parseInt(tankValue) === 0) {
                    return tankValue;
                } else {
                    return (parseInt(tankValue) - 1) + '';
                }
            }
            return tankValue;
        };

        $scope.displayTankDescription = function (tankValue) { // for some reason intellichlor returns 1-7 instead of 0-6
            var status = "Empty"

            if (!tankValue) {
                return status
            }
            if (!isNaN(tankValue)) {
                var level = parseInt(tankValue)-1
                if ( level === 0) {
                    status = "Empty"
                } 
                else if (level >= 6) {
                    status =  "Full"
                }
                else { // between 1 and 5 
                    status = "." 
                }                
            }

            return status;
        };

        $scope.tankDescriptionColor = function (tankValue) { // for some reason intellichlor returns 1-7 instead of 0-6
            var color = "#ffffff;"   // white

            if (!tankValue) {
                return color
            }
            if (!isNaN(tankValue)) {
                var level = parseInt(tankValue) - 1
                if (level === 0) {
                    color = "#ff7340;" // "Empty" : red
                }
                else if (level >= 6) {
                    color = "#009900;" // Full : green
                }
                else { // between 1 and 5 
                    color = "#ffffff;" // white
                }
            }

            return color;
        };

        // check if ICHEM OR ICHLOR STILL CONNECTED ?
        $scope.isIntelliChemAvailable = function (parent) {
            var status = true
            
            angular.forEach($scope.Model.lostcommunication, function (alert) {
                if (alert.PARENT === parent.OBJNAM) {                    
                    status = false  // communication has been lost                    
                }
            });            
            return status;
        }


        $scope.isPresentIChlorBody = function (body) {
            if (body.CHEMISTRY.ICHLOR != undefined)
                return true;
            else
                return false;
        };

        $scope.isNotIChlorBody = function (body) {
            if (body.CHEMISTRY.ICHLOR != undefined)
                return false;
            else
                return true;
        };

        $scope.isNotIChemBody = function (body) {
            if (body.CHEMISTRY.ICHEM != undefined)
                return false;
            else
                return true;
        };

        $scope.isIChemControlingChlor = function (body) {
            var status = false
            if (body === undefined)
                return status

            if ((body.CHEMISTRY.ICHEM != undefined) && (body.CHEMISTRY.ICHLOR != undefined)) {

                // check of ichem is controlling intellichlor
                if (body.CHEMISTRY.ICHEM.CHLOR && body.CHEMISTRY.ICHEM.CHLOR === "ON")
                    status = true

            }

            return status

        }

        $scope.showOrpTankLevel = function (body) {
            var display  = false;

            if ($scope.isIntelliChemAvailable(body.CHEMISTRY.ICHEM))  // if not available 
                display = true;
            else
                display = false;

            if ($scope.isIChemControlingChlor(body))   // hide if chem is controlling chlor
                display = false;

            return display;
        }

        // Check the following
        $scope.canDisplayIChlor = function (body) {

            var status = false
            if (body === undefined)
                return status

            // for single body : INTELICHLOR PRESENT AND NO INTELLICHEM
            if ( (body.CHEMISTRY.ICHEM === undefined)  &&  
                 (body.CHEMISTRY.ICHLOR != undefined)
                )
                status = true

            // for dual body : INTELICHLOR AND INTELLICHEM PRESENT BUT ON DIFFERENT BODIES
            if ((body.CHEMISTRY.ICHEM != undefined) && (body.CHEMISTRY.ICHLOR != undefined)) {

                // check of ichem is controlling intellichlor
                if ( body.CHEMISTRY.ICHEM.CHLOR && body.CHEMISTRY.ICHEM.CHLOR === "OFF") 
                    status = true

            }
            
            return status

        }

        $scope.isChemistryPresent = function (body) {
            
            if (!body.CHEMISTRY)   // check if chemistry
                return false;
            
            if ( (body.CHEMISTRY.ICHEM) ||
                 (body.CHEMISTRY.ICHLOR) )
                return true;
            else
                return false;

        };

        function getBodies() {
            var result = [];
            angular.forEach(propertyModel.SharedBodies, function (value, key) {
                var relationShip = {
                    parent: {},
                    child: {},
                    SNAME: '',
                    LISTORD: "0"
                };
                if (propertyModel.Objects[value]) { // check if is valid
                    if (propertyModel.Objects[key].LISTORD < propertyModel.Objects[value].LISTORD) {
                        relationShip = {
                            parent: propertyModel.Objects[key],
                            child: propertyModel.Objects[value]
                        };
                    } else {
                        relationShip = {
                            parent: propertyModel.Objects[value],
                            child: propertyModel.Objects[key]
                        };
                    }
                }
                relationShip.SNAME = relationShip.parent.SNAME + '/' + relationShip.child.SNAME;
                relationShip.LISTORD = relationShip.parent.LISTORD;
                relationShip.CHEMISTRY = getChemistries(relationShip.parent.OBJNAM);
                result.push(relationShip);
            });


            var SingleBodies = propertyModel.SingleBodies.map(function (item) {
                var obj = propertyModel.Objects[item];
                obj.CHEMISTRY = getChemistries(obj.OBJNAM);
                return obj;
            });

            $scope.AllBodies = result.concat(SingleBodies);

        }

        $scope.phSegs = ['#EE1C25', '#F26724', '#F8C611', '#F5ED1C', '#B5D333', '#84C341', '#4DB749', '#33A94B', '#22B46B', '#0AB8B6', '#4690CD', '#3853A4', '#5A51A2', '#63459D', '#462C83'];
        $scope.orpSegs = ['#EE1C25', '#F26724', '#FFFF01', '#BCFF00', '#00FF25', '#00FFD5', '#0141FA', '#02124E'];
        $scope.phRange = {
            min: 7, 
            max: 7.6, 
        };
        $scope.orpRange = {
            min: 400, 
            max: 800 
        };

        $scope.ranges = {
            PH: { min: 4, max: 10 },
            PH_ZOOM: { min: 6.5, max: 8.3 },    // !!! do not change(6.5 TO 8.3) this values are zooming values for PH
            ORP: { min: 0, max: 999 },
            ORP_ZOOM: { min: 400, max: 999 },    // !!! do not change(300 TO 999) this values are zooming values for ORP
            SALT: { min: 0, max: 999 },
            ALK: { min: 25, max: 800},
            CALC: { min: 25, max: 800},
            CYACID: { min: 0, max: 201},
            PRIM: { min: 0, max: 100 },
            SEC: { min: 0, max: 100 },
            TIMOUT: { min: 0, max: 96 },

        };

        $scope.setSuperChlor = function (objNam, param, value) {
            var adjVal = (Number(value)) * (60 * 60);
            propertyService.SetValue(objNam, param, adjVal);
        };

        //avoid this by not displaying this controller until the property is ready.
        $scope.$watch('Model.Bodies', function (newVal, oldVal) {
            if (newVal.length !== oldVal.length) {
                getBodies();
            }
        }, true);



        function getChemistries(bodyName) {
            var chemistry = {
                ICHEM: null,
                ICHLOR: null
            };

            if (propertyModel.PanelType){ // "SINGLE",    // NONE, SINGLE, SHARE, DUAL
                console.log("========== paneltype " , propertyModel.PanelType);
               
            }
            var allChemistries = $filter('filterOutByObjectType')(propertyModel.Objects, 'CHEM');   
            var chemistries = $filter('objectsByParamValue')(allChemistries, ['BODY', 'SHARE'], bodyName);
            chemistry.ICHLOR = $filter('objectsBySubType')(chemistries, 'ICHLOR')[0];
            chemistry.ICHEM = $filter('objectsBySubType')(chemistries, 'ICHEM')[0];
            return chemistry;
        }
    }
]);
