﻿using Newtonsoft.Json;

namespace Pentair.Web.Api
{
    public class HistoryItem
    {
        [JsonProperty("P", Required = Required.Always)]
        public string Name { get; private set; }

        [JsonProperty("V", Required = Required.Always)]
        public string Value { get; private set; }

        [JsonProperty("T", Required = Required.Always)]
        public string Timestamp { get; private set; }

        public HistoryItem(
            string P,
            string V,
            string T)
        {
            this.Name = P;
            this.Value = V;
            this.Timestamp = T;
        }

    } // class HistoryItem
}
