﻿using System;
using System.Collections.Generic;
using System.Linq;
using Machine.Specifications;
using Microsoft.AspNet.Identity.EntityFramework;
using Pentair.Domain.Client.BusinessLogic;
using Pentair.Domain.Client.BusinessLogic.Classes;
using Pentair.Domain.Client.Entity;
using Pentair.Domain.Client.Helpers;
using Pentair.Domain.Client.Infrastructure;
using Pentair.Web.Api;

namespace Pentair.Tests.Domain
{
    [Subject("BusinessLogic")]
    public class WhenIAskForInstallations
    {
        #region Database Records

        private static int property1 = 1;
        private static int property2 = 2;
        private static string user1 = "668c6b40-97e9-4064-8e2c-e3bcf732095b";
        private static string user2 = "768c6b40-97e9-4064-8e2c-e3bcf732095b";
        private static string user3 = "788c6b40-97e9-4064-8e2c-e3bcf732095b";

        public class TestContent
        {
            private ApplicationUserManager identity;

            public IApiContext Data { get; private set; }
            public IQueryable<ApplicationUser> Users { get { return identity.Users; } }

            public TestContent()
            {
                var users = new FakeContext()
                {
                    ApplicationUsers =
                    {
                        new ApplicationUser()
                        {
                            Id = user1
                        }
                    }
                };

                identity = new ApplicationUserManager(
                    new UserStore<ApplicationUser>(
                        new ApplicationUserContext()));

                Data = new FakeContext()
                {
                    Associations =
                    {
                        new Association()
                        {
                            UserId = new Guid(user1),
                            InstallationId = property1,
                            AccessToken = StaticData.Admin
                        },
                        new Association()
                        {
                            UserId = new Guid(user2),
                            InstallationId = property1,
                            AccessToken = StaticData.Guest
                        },
                        new Association()
                        {
                            UserId = new Guid(user1),
                            InstallationId = property2,
                            AccessToken = StaticData.Admin
                        },
                        new Association()
                        {
                            UserId = new Guid(user2),
                            InstallationId = property2,
                            AccessToken = StaticData.Admin
                        }
                    },
                    Installations =
                    {
                        new Installation()
                        {
                            Name = "Property1",
                            InstallationId = property1,
                            Connected = true
                        },
                        new Installation()
                        {
                            Name = "property2",
                            InstallationId = property2,
                            Connected = false
                        }
                    },
                    ControlObjects =
                    {
                        new ControlObject()
                        {
                            INSTALLATION = property1,
                            OBJNAM = StaticData.Guest,
                            //OBJTYP = "PERMIT",
                            //SNAME = "Guest",
                            //SHOMNU = "slb"
                        },
                        new ControlObject()
                        {
                            INSTALLATION = property1,
                            OBJNAM = StaticData.Admin,
                            //OBJTYP = "PERMIT",
                            //SNAME = "Admin",
                            //SHOMNU = "slb"
                        },
                        new ControlObject()
                        {
                            INSTALLATION = property2,
                            OBJNAM = StaticData.Guest,
                            //OBJTYP = "PERMIT",
                            //SNAME = "Guest",
                            //SHOMNU = "slb"
                        },
                        new ControlObject()
                        {
                            INSTALLATION = property2,
                            OBJNAM = StaticData.Admin,
                            //OBJTYP = "PERMIT",
                            //SNAME = "Admin",
                            //SHOMNU = "slb"
                        }

                    },
                    UserInvitations =
                    {
                        new UserInvitation()
                        {
                            InvitationId = Guid.Empty,
                            EmailAddress = "jhewitt@bhst.com",
                            InstallationId = property1,
                            AccessToken = StaticData.Admin,
                            Status = "UNKNOWN",
                            EmailId = ""
                        },
                        new UserInvitation()
                        {
                            InvitationId = Guid.Empty,
                            EmailAddress = "dhoward@pathf.com",
                            InstallationId = property2,
                            AccessToken = StaticData.Admin,
                            Status = "UNKNOWN",
                            EmailId = ""
                        }
                    }
                };
            }
        }

        #endregion

        #region Tests

        public class WhenBogusUserIsDeletFromProperty
        {
            private static UserManagementResult result;
            private static UserAssociations subject;

            private Establish context = () => subject = new UserAssociations(new TestContent().Data);

            private Because of = () => result = subject.RemoveUserFromInstallation(new Guid(user3), property2);

            private It UsderInstallationList_Should_Return_UserNotFound = () => result.ShouldEqual(UserManagementResult.UserNotAssociatedWithProperty);
        }

        public class WhenTheUserInstallationsConstructorIsPassedParameters
        {
            private static UserInstallations subject;

            private Establish context = () => subject = new UserInstallations(property1, "Name", "Token", true);

            private It UserInstallations_Should_Have_Defalut_Properties_Set = () => subject.AccessToken.ShouldEqual("Token");
        }

        public class WhenTheAssociatedUsersConstructorIsPassedParameters
        {
            private static AssociatedUser subject;

            private Establish context = () => subject = new AssociatedUser(user1, "Name", "Email", "Token");

            private It AssociatedUsers_Should_Have_Defalut_Properties_Set = () => subject.EmailAddress.ShouldEqual("Email");
        }

        public class WhenAnInvitationIsDeleted
        {
            private static UserAssociations subject;

            private static UserManagementResult result;

            private Establish context = () => subject = new UserAssociations(new TestContent().Data);

            private Because Of = () => result = subject.RemoveUserInvite("jhewitt@bhst.com", property1);

            private It SuccessfullDeleteOfInvite = () => result.ShouldEqual(UserManagementResult.Success);
        }

        public class WhenAnInvitationIsNotFound
        {
            private static UserAssociations subject;

            private static UserManagementResult result;

            private Establish context = () => subject = new UserAssociations(new TestContent().Data);

            private Because Of = () => result = subject.RemoveUserInvite("JohnDoe@bhst.com", property1);

            private It SuccessfullDeleteOfInvite = () => result.ShouldEqual(UserManagementResult.InviteNotFound);
        }

        public class WhenTheUserAssociationListIsNeeded
        {
            private static UserAssociations subject;

            private static TestContent data;

            private static List<AssociatedUser> result;

            private Establish context = () =>
            {
                data = new TestContent();
                subject = new UserAssociations(data.Data);
            };

            private Because Of = () => result = subject.UserAssociationList(property1, null, 20, 0);
        
            private It ShouldContainTheUserAssociationList = () => result.Count.ShouldEqual(3);
        }

        public class WhenUserChangesAccountAccessOnAnInvite
        {
            private static UserAssociations subject;
            private static TestContent data;
            private static UserManagementResult result;
            private static UserInvitation user;

            private Establish context = () =>
            {
                data = new TestContent();
                subject = new UserAssociations(data.Data);
            };

            private Because of = () =>
            {
                result = subject.ChangeUserAccess(
                    "jhewitt@bhst.com",
                    StaticData.Guest,
                    Guid.Empty,
                    property1
                    );
                user = (from u in data.Data.UserInvitations
                    where u.EmailAddress == "jhewitt@bhst.com"
                    select u).FirstOrDefault();
            };

            private It UserShouldNowBeGuest = () =>
            {
                result.ShouldEqual(UserManagementResult.Success);
                user.ShouldNotBeNull();
                user.AccessToken.ShouldEqual(StaticData.Guest);
            };
        }

        public class WhenUserChangesAccountAccessOnAnExistingUser
        {
            private static UserAssociations subject;
            private static TestContent data;
            private static UserManagementResult result;
            private static Association user;

            private Establish context = () =>
            {
                data = new TestContent();
                subject = new UserAssociations(data.Data);
            };

            private Because of = () =>
            {
                result = subject.ChangeUserAccess(
                    "",
                    StaticData.Admin,
                    new Guid(user2),
                    property1
                    );
                user = (from u in data.Data.Associations
                        where u.InstallationId == property1 &&
                              u.UserId == new Guid(user2)
                        select u).FirstOrDefault();
            };

            private It UserShouldNowBeGuest = () =>
            {
                result.ShouldEqual(UserManagementResult.Success);
                user.ShouldNotBeNull();
                user.AccessToken.ShouldEqual(StaticData.Admin);
            };
        }

        public class WhenUserChangesAccountAccessOnAnNonExistantUser
        {
            private static UserAssociations subject;
            private static TestContent data;
            private static UserManagementResult result;

            private Establish context = () =>
            {
                data = new TestContent();
                subject = new UserAssociations(data.Data);
            };

            private Because of = () =>
            {
                result = subject.ChangeUserAccess(
                    "",
                    StaticData.Admin,
                    new Guid(user3),
                    property1
                    );
            };

            private It UserShouldNowBeGuest = () => result.ShouldEqual(UserManagementResult.UserNotFound);
        }
        #endregion
    }
}
