﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pentair.Domain.Simulator.Entity
{
    public class Service
    {
        [Key]
        public int ID { get; set; }
        public string Name { get; set; }
        public UInt32 LastRevision { get; set; }
        public bool Status { get; set; }

        // Foreign Keys
        public int TypeID { get; set; }
        public int CircuitOnID { get; set; }    // TODO Are these two separately controlled circuits?
        public int CircuitOffID { get; set; }   // TODO or one circuit defined by status (on/off)?
    }
}