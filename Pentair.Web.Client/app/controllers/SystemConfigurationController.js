﻿'use strict';
var PWC = PWC || {};
PWC.controller('SystemConfigurationController', [
    '$scope', '$location', '$filter', 'PropertyService', 'PropertyModel', 'Enumerations',
    function($scope, $location, $filter, propertyService, propertyModel, enumerations) {
        $scope.property = propertyService;
        $scope.model = propertyModel;
        $scope.enumerations = enumerations;
        $scope.selectedPanel = propertyModel.Panels[0].OBJNAM;
        $scope.relay = propertyModel.Relays[0];
        $scope.circuit = $scope.relay && $scope.relay.CIRCUIT ? propertyModel.Objects[$scope.relay.CIRCUIT] : null;
        $scope.circuitFunctions = [];
        $scope.circuitNames = {};
        $scope.circuitSName = '';
        $scope.circuitSubtype = '';
        $scope.editMode = false;
        $scope.createFlag = false;
        $scope.freezeMode = 'OFF';
        $scope.freezeModeValue = false;
        $scope.$parent.Layout = "layout-base";

        (function() {
            for (var i = 0; i < 22; i++) {
                var num = "CF_" + (i + 1);

                var val = $filter("translate")(num);
                var lbl = $filter("translate")(val);
                var newObject = {
                    LABEL: lbl,
                    VALUE: val
                }
                $scope.circuitFunctions[i] = newObject;
            }
        }());

        (function () {
            for (var i = 0; i < 83; i++) {
                var val = "CN_" + (i);
                var newObject = $filter("translate")(val);
                $scope.circuitNames[i] = newObject;
            }
        }());

        var setCircuitSubtype = function (type) {
            if (type) {
                angular.forEach($scope.circuitFunctions, function (circuitType) {
                    if (type == circuitType.VALUE) {
                        $scope.circuitSubtype = circuitType.VALUE;
                    }
                });
            }
        }

        $scope.relayCircuitSubtype = function (relay) {
            if (relay) {
                if (relay.CIRCUIT) {
                    if (propertyModel.Objects[relay.CIRCUIT]) {
                        return propertyModel.Objects[relay.CIRCUIT].SUBTYP;
                    }
                }
            }
            return null;
        }

        $scope.relayCircuitName = function (relay) {
            if (relay && propertyModel.Objects[propertyModel.Objects[relay].CIRCUIT]) {
                if (propertyModel.Objects[propertyModel.Objects[relay].CIRCUIT].SNAME) {
                    if (propertyModel.Objects[propertyModel.Objects[relay].CIRCUIT].SNAME.length > 0) {
                        return " - " + propertyModel.Objects[propertyModel.Objects[relay].CIRCUIT].SNAME;
                    }
                    return "";
                }
                return "";
            }
            return "";
        }

        $scope.GoToConfigPage = function() {
            propertyService.ReleaseTimeUpdates();
            $location.path("/configure");
        }

        $scope.itemSelected = function(item) {
            if ($scope.editMode) {
                $scope.cancelEditMode();
            }

            $scope.circuit = {};
            $scope.circuit.SNAME = '';
            $scope.circuit.RLY = $scope.relay.OBJNAM;
            $scope.circuit.FREEZE = 'OFF';
            $scope.circuitSubtype = '';

            if (item && propertyModel.Objects[item]) {
                $scope.relay = propertyModel.Objects[item];
                if ($scope.relay.CIRCUIT) {
                    if (propertyModel.Objects[$scope.relay.CIRCUIT]) {
                        $scope.circuit = propertyModel.Objects[$scope.relay.CIRCUIT];
                        setCircuitSubtype($scope.circuit.SUBTYP);
                        $scope.circuitSName = $scope.circuit.SNAME;
                    }
                }
            }
            $scope.freezeModeValue = $scope.circuit.FREEZE == 'ON' ? true : false;
            $scope.freezeMode = $scope.circuit.FREEZE;
        }

        $scope.circuitFunctionSelected = function(value) {
            if (value) {
                $scope.circuitSubype = value;
            }
        }

        $scope.panelSelected = function(panel) {
            if (panel) {
                $scope.selectedPanel = panel;
            }
        }

        $scope.setEditMode = function() {
            $scope.editMode = true;
            if ($scope.circuit && $scope.circuit.OBJNAM) {
                $scope.circuit = propertyModel.Objects[$scope.relay.CIRCUIT];
                setCircuitSubtype($scope.circuit.SUBTYP);
                $scope.circuitSName = $scope.circuit.SNAME;
                $scope.freezeMode = $scope.circuit.FREEZE;
                $scope.createFlag = false;
            } else {
                $scope.createFlag = true;
                $scope.circuit = {};
                $scope.circuit.HNAME = 'CIRCUIT';
                $scope.circuit.SNAME = '';
                $scope.circuit.STATIC = 'ON';
                $scope.circuit.RLY = $scope.relay.OBJNAM;
                $scope.circuit.FREEZE = 'OFF';
                $scope.circuit.SUBTYP = 'GENERIC';
            }
            $scope.freezeModeValue = $scope.circuit.FREEZE === 'ON' ? true : false;
        }

        $scope.saveChanges = function() {
            if ($scope.createFlag) {
                $scope.createFlag = false;
                propertyService.CreateNewObject('CIRCUIT');
            } else {
                $scope.editMode = false;
                saveCircuit($scope.circuit.OBJNAM);
            }
        }

        $scope.cancelEditMode = function() {
            if ($scope.relay.CIRCUIT) {
                propertyModel.Objects[$scope.relay.CIRCUIT].SNAME = $scope.circuitSName;
                propertyModel.Objects[$scope.relay.CIRCUIT].SUBTYP = $scope.circuitSubtype;
                propertyModel.Objects[$scope.relay.CIRCUIT].FREEZE = $scope.freezeMode;
                $scope.circuit = propertyModel.Objects[$scope.relay.CIRCUIT];
                $scope.freezeModeValue = $scope.circuit.FREEZE === 'ON' ? true : false;
            } else {
                $scope.circuit = null;
            }
            $scope.editMode = false;
        }

        $scope.$on('NotifyObjectCreation', function (event, message) {
            $scope.editMode = false;
            saveCircuit(message.objnam);
        });

        function saveCircuit(objNam) {
            if (!objNam) {
                $scope.Error = true;
                return;
            }
            $scope.circuit.OBJNAM = objNam;

            var rlyparams = {
                CIRCUIT: objNam
            }
            propertyService.SetParams($scope.relay.OBJNAM, rlyparams);
            var params = {
                SNAME: $scope.circuit.SNAME,
                HNAME: $scope.circuit.HNAME,
                STATIC: $scope.circuit.STATIC,
                FREEZE: $scope.freezeModeValue ? "ON" : "OFF",
                SUBTYP: $scope.circuit.SUBTYP,
                RLY: $scope.circuit.RLY
            }
            propertyService.SetParams(objNam, params);
        }
    }
]);