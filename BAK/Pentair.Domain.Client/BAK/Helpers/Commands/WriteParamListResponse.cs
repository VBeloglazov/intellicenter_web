﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Pentair.Domain.Client.Helpers.Commands
{
    public class WriteParamListResponse : ExtendedParamListResponse
    {
        public WriteParamListResponse(
            string messageID,
            string response,
            string timeNow,
            string timeSince,
            List<ExtendedParamObject> objectList
            )
            : base(CommandString.WriteParamList, messageID, response, timeNow, timeSince, objectList)
        {
            this.IsValid = true;

        } // WriteParamListResponse()

        /// <summary>
        /// Always returns an object, valid or not. If invalid, check the DeserializationError property value.
        /// </summary>
        public static new WriteParamListResponse Deserialize(
            string serializedMessage)
        {
            WriteParamListResponse command = null;

            try
            {
                command = JsonConvert.DeserializeObject<WriteParamListResponse>(serializedMessage);
                command.IsValid = true;
            }
            catch (Exception e)
            {
                // Deserialization as WriteParamListResponse failed

                StringBuilder sbError = new StringBuilder();

                sbError.Append("Cannot deserialize as WriteParamListResponse. " + e.Message);

                // Try to get at least the base message data
                ExtendedParamListResponse baseCommand = ExtendedParamListResponse.Deserialize(serializedMessage);

                if (!baseCommand.IsValid)
                {
                    sbError.Append("\n" + baseCommand.DeserializationError);
                }

                command = new WriteParamListResponse(
                    messageID: baseCommand.MessageID,
                    response: ResponseString.BadRequest,
                    timeNow:String.Empty,
                    timeSince: String.Empty,
                    objectList: null)
                {
                    IsValid = false,
                    DeserializationError = sbError.ToString()
                };
            }

            return command;

        } // Deserialize()

    } // class WriteParamListResponse
}