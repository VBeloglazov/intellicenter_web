/**
 * Created by Adam Carnagey on 5/9/16.
 */
'use strict';
describe('ConfigureCircuitsController', function () {
  var $scope, $location, $interpolate, PropertyService, propertyModel, Enumerations, $filter,
    ConfigureCircuitsController, $route, $controller, messageProcessor, appService, $httpBackend, secretEmptyKey,
    deferred, $q;

  beforeEach(function() {
    module('PentairWebClient');
    // Provide will help us create fake implementations for our dependencies
    module(function($provide) {
      // Fake PropertyService Implementation returning a promise
      $provide.value('PropertyService', {
        SetParams: function(OBJNAM, PARAMS) {
          return {
            then: function(callback) {return callback([{ some: 'thing' }]);}
          };
        },
        GetHardwareDefinition: function() {
          return {
            then: function(callback) {return callback([{ someOther: 'thingy'}]);}
          };
        },
        getCircuitsFull: function() {
          return {
            then: function(callback) {return callback([{ someOthery: 'things'}]);}
          };
        }
      });
      return null;
    });
  });

  beforeEach(function() {

    // When Angular Injects the PropertyService dependency,
    // it will use the implementation we provided above
    inject(function (_$controller_, _$httpBackend_, _$rootScope_, _PropertyService_, _PropertyModel_,
                     _$location_, _$filter_, _$q_, _SocketService_, _$route_, _MessageProcessor_, _AppService_, _$interpolate_,
                     _Enumerations_) {
      //SocketService = _SocketService_;
      $scope = _$rootScope_.$new();
      PropertyService = _PropertyService_;
      propertyModel = _PropertyModel_;
      $location = _$location_;
      $filter = _$filter_;
      $controller = _$controller_;
      $route = _$route_;
      messageProcessor = _MessageProcessor_;
      $httpBackend = _$httpBackend_;
      appService = _AppService_;
      $interpolate = _$interpolate_;
      Enumerations = _Enumerations_;
      secretEmptyKey = '[$empty$]';
      $scope.$parent.Layout = 'layout-base';
      $scope.property = propertyModel;
      $scope.accordionStateByObjectNames = {};
      $q = _$q_;
      deferred = $q.defer();

      $scope.property.hardwareDefinition = [{
        objnam: 'M0000',
        params: {
          OBJTYP: 'PANEL',
          SUBTYP: 'OCP',
          HNAME: 'OCP 0',
          SNAME: 'OCP 0',
          LISTORD: '0',
          '': 'VAR',
          OBJLIST: [{
            objnam: 'm0010',
            params: {
              OBJTYP: 'MODULE',
              SUBTYP: 'I10PS',
              SNAME: 'I10PS',
              LISTORD: '1',
              PARENT: 'M0000',
              PORT: '1',
              '': 'VAR',
              BADGE: 'BADGE',
              OBJLIST: [
                {
                  objnam: 'C88C9',
                  params: {
                    OBJTYP: 'CIRCUIT',
                    SUBTYP: 'LIGHT',
                    HNAME: 'AUX1',
                    SNAME: 'Light 2',
                    LISTORD: '7',
                    PARENT: 'm0010',
                    BODY: '00000',
                    FREEZE: 'OFF',
                    VER: 'VER',
                    TIMZON: 'TIMZON',
                    TIME: '720',
                    RLY: 'r0012'
                  }
                }, {
                  objnam: 'C0829',
                  params: {
                    OBJTYP: 'CIRCUIT',
                    SUBTYP: 'LIGHT',
                    HNAME: 'AUX9',
                    SNAME: 'AUX9',
                    LISTORD: '9',
                    PARENT: 'm0010',
                    BODY: '00000',
                    FREEZE: 'OFF',
                    VER: 'VER',
                    TIMZON: 'TIMZON',
                    TIME: '720',
                    RLY: 'r0012'
                  }
                }, {
                  objnam: 'CDB49',
                  params: {
                    OBJTYP: 'CIRCUIT',
                    SUBTYP: 'LIGHT',
                    HNAME: 'AUX8',
                    SNAME: 'AUX8',
                    LISTORD: '8',
                    PARENT: 'm0010',
                    BODY: '00000',
                    FREEZE: 'OFF',
                    VER: 'VER',
                    TIMZON: 'TIMZON',
                    TIME: '720',
                    RLY: 'r0012'
                  }
                }, {
                  objnam: 'CDB49',
                  params: {
                    OBJTYP: 'CIRCUIT',
                    SUBTYP: 'LIGHT',
                    HNAME: 'AUX8',
                    SNAME: 'AUX8',
                    LISTORD: '8',
                    PARENT: 'm0010',
                    BODY: '00000',
                    FREEZE: 'OFF',
                    VER: 'VER',
                    TIMZON: 'TIMZON',
                    TIME: '720',
                    RLY: 'r0012'
                  }
                }
              ]
            }
          }]
        }
      }];

      $scope.property.Circuits = [
        {
          OBJNAM: 'C88C9',
          OBJTYP: 'CIRCUIT',
          SNAME: 'Light 2',
          SHOMNU: 'fcsrelhzmto',
          STATUS: 'OFF',
          SUBTYP: 'LIGHT',
          USAGE: '',
          ACT: 'ACT',
          FREEZE: 'OFF',
          Delay: {VISIBLE: false, MESSAGE: ''},
          $$hashKey: 'object:858',
          MODE: 'MODE',
          LISTORD: '10',
          LIMIT: '100',
          USE: '',
          MANUAL: 'OFF',
          FEATR: 'OFF',
          DNTSTP: 'OFF',
          CHILD: 'CHILD'
        },
        {
          OBJNAM: 'CF2D2',
          OBJTYP: 'CIRCUIT',
          SNAME: 'Light 1',
          SHOMNU: 'fcsrelhzmto',
          STATUS: 'OFF',
          SUBTYP: 'LIGHT',
          USAGE: '',
          ACT: 'ACT',
          FREEZE: 'OFF',
          Delay: {VISIBLE: false, MESSAGE: ''},
          $$hashKey: 'object:859',
          MODE: 'MODE',
          LISTORD: '9',
          LIMIT: '100',
          USE: '',
          MANUAL: 'ON',
          FEATR: 'OFF',
          DNTSTP: 'OFF',
          CHILD: 'CHILD'
        }];

      ConfigureCircuitsController = $controller('ConfigureCircuitsController', {$scope: $scope});

      $scope.circuitsGroups = {
        OBJNAM: 'GRP',
        name: 'CIRCUITS GROUPS',
        list: [
          {
            OBJNAM: 'GRPITM1',
            hName: 'GRP1',
            sName: 'MAKE IT RAIN',
            isSelected: false,
            circuits: [
              {
                name: 'SPA PUMP',
                isOn: true
              }
            ]
          },
          {
            OBJNAM: 'GRPITM2',
            hName: 'GRP2',
            sName: 'CLICK ME',
            isSelected: false,
            circuits: [
              {
                name: 'SPA JETS HIGH',
                isOn: false
              },
              {
                name: 'OTHER THING',
                isOn: true
              },
              {
                name: 'SPILLWAY',
                isOn: false
              }
            ]
          }
        ]
      };

      $scope.panels = [{
        objectName: 'M0000',
        hName: 'OCP 0',
        sName: 'OCP 0',
        circuits: [
          {
            objectName: 'CB683',
            hName: 'AUX1',
            sName: 'a circuit',
            subtype: 'GENERIC',
            freezeProtection: false,
            eggTimerHours: 12,
            eggTimerMinutes: 0,
            showAsFeature: false,
            disableEggTimer: false
          },
          {

            disableEggTimer: false,
            eggTimerHours: 12,
            eggTimerMinutes: 0,
            freezeProtection: false,
            hName: "AUX9",
            objectName: "C0829",
            sName: "AUX9",
            showAsFeature: false,
            subtype: "GENERIC"
          },
          {
            disableEggTimer: false,
            eggTimerHours: 12,
            eggTimerMinutes: 0,
            freezeProtection: false,
            hName: "AUX8",
            objectName: "CDB49",
            sName: "AUX8",
            showAsFeature: false,
            subtype: "GENERIC"
          }
        ]
      }];

      $scope.panelsBeforeEdit = angular.copy($scope.panels);
      $scope.circuitsGroupsBeforeEdit = angular.copy($scope.circuitsGroups);

      //spyOn($location, 'path').and.returnValue('/systemInformation');
      //$httpBackend.expectGET('/app/templates/configure-circuits.partial.html').respond(200);
    });
  });

  // describe('Initial Values', function () {
  //   it('should expect the following initial values ', function () {
      // TODO it is null right now until it works
      // expect($scope.circuitsGroups).toEqual({
      //   OBJNAM: 'GRP',
      //   name: 'CIRCUITS GROUPS',
      //   list: [
      //     {
      //       OBJNAM: 'GRPITM1',
      //       hName: 'GRP1',
      //       sName: 'MAKE IT RAIN',
      //       isSelected: false,
      //       circuits: [
      //         {
      //           name: 'SPA PUMP',
      //           isOn: true
      //         }
      //       ]
      //     },
      //     {
      //       OBJNAM: 'GRPITM2',
      //       hName: 'GRP2',
      //       sName: 'CLICK ME',
      //       isSelected: false,
      //       circuits: [
      //         {
      //           name: 'SPA JETS HIGH',
      //           isOn: false
      //         },
      //         {
      //           name: 'OTHER THING',
      //           isOn: true
      //         },
      //         {
      //           name: 'SPILLWAY',
      //           isOn: false
      //         }
      //       ]
      //     }
      //   ]
      // });

  //     expect($scope.panels).toEqual([{
  //       objectName: 'M0000',
  //       hName: 'OCP 0',
  //       sName: 'OCP 0',
  //       circuits: [
  //         {
  //           objectName: 'CB683',
  //           hName: 'AUX1',
  //           sName: 'a circuit',
  //           subtype: 'GENERIC',
  //           freezeProtection: false,
  //           eggTimerHours: 12,
  //           eggTimerMinutes: 0,
  //           showAsFeature: false,
  //           disableEggTimer: false
  //         },
  //         {
  //
  //           disableEggTimer: false,
  //           eggTimerHours: 12,
  //           eggTimerMinutes: 0,
  //           freezeProtection: false,
  //           hName: "AUX9",
  //           objectName: "C0829",
  //           sName: "AUX9",
  //           showAsFeature: false,
  //           subtype: "GENERIC"
  //         },
  //         {
  //           disableEggTimer: false,
  //           eggTimerHours: 12,
  //           eggTimerMinutes: 0,
  //           freezeProtection: false,
  //           hName: "AUX8",
  //           objectName: "CDB49",
  //           sName: "AUX8",
  //           showAsFeature: false,
  //           subtype: "GENERIC"
  //         }
  //       ]
  //     }]);
  //
  //     expect($scope.property.Circuits).toEqual([
  //       {
  //         OBJNAM: 'C88C9',
  //         OBJTYP: 'CIRCUIT',
  //         SNAME: 'Light 2',
  //         SHOMNU: 'fcsrelhzmto',
  //         STATUS: 'OFF',
  //         SUBTYP: 'LIGHT',
  //         USAGE: '',
  //         ACT: 'ACT',
  //         FREEZE: 'OFF',
  //         Delay: {VISIBLE: false, MESSAGE: ''},
  //         $$hashKey: 'object:858',
  //         MODE: 'MODE',
  //         LISTORD: '10',
  //         LIMIT: '100',
  //         USE: '',
  //         MANUAL: 'OFF',
  //         FEATR: 'OFF',
  //         DNTSTP: 'OFF',
  //         CHILD: 'CHILD'
  //       },
  //       {
  //         OBJNAM: 'CF2D2',
  //         OBJTYP: 'CIRCUIT',
  //         SNAME: 'Light 1',
  //         SHOMNU: 'fcsrelhzmto',
  //         STATUS: 'OFF',
  //         SUBTYP: 'LIGHT',
  //         USAGE: '',
  //         ACT: 'ACT',
  //         FREEZE: 'OFF',
  //         Delay: {VISIBLE: false, MESSAGE: ''},
  //         $$hashKey: 'object:859',
  //         MODE: 'MODE',
  //         LISTORD: '9',
  //         LIMIT: '100',
  //         USE: '',
  //         MANUAL: 'ON',
  //         FEATR: 'OFF',
  //         DNTSTP: 'OFF',
  //         CHILD: 'CHILD'
  //       }]);
  //
  //   });
  // });

  // describe('Concatenating and trimming a circuits display name.', function () {
  //   it('should combine both names together but not truncate if it falls within the length limit', function () {
  //     var circuit = $scope.panels[0].circuits[0];
  //     expect(circuit.hName).toBe('AUX1');
  //     expect(circuit.sName).toBe('a circuit');
  //     expect($filter('cut')($scope.combineName(circuit), false, 17, ' ...')).toBe('AUX1 - a circuit');
  //   });
  //   it('should combine both names and truncate if it exceeds the length limit', function () {
  //     var circuit = $scope.panels[0].circuits[0];
  //     circuit.sName = 'This is a really really long name for a circuit';
  //     expect($filter('cut')($scope.combineName(circuit), false, 17, ' ...'))
  //       .toBe('AUX1 - This is a  ...');
  //   });
  // });
  //
  // describe('Cancelling out of a aux edit', function () {
  //   beforeEach(function () {
  //     $scope.SetEditMode($scope.panels[0].circuits[0]);
  //     $scope.panels[0].circuits[0].sName = 'new name';
  //     $scope.panels[0].circuits[0].eggTimerHours = 3;
  //     $scope.panels[0].circuits[0].eggTimerMinutes = 5;
  //   });
  //   it('should reset the circuit to the initial values', function () {
  //     $scope.CancelEdit(0, 0);
  //     expect($scope.panels[0].circuits[0].editMode).toBeFalsy();
  //     expect($scope.panels[0].circuits[0].sName).toBe('a circuit');
  //     expect($scope.panels[0].circuits[0].eggTimerHours).toBe(12);
  //     expect($scope.panels[0].circuits[0].eggTimerMinutes).toBe(0);
  //   });
  // });
  //
  // describe('Cancelling out of a group edit', function () {
  //   beforeEach(function () {
  //     $scope.SetEditMode($scope.circuitsGroups.list[0]);
  //     $scope.circuitsGroups.list[0].sName = 'new name';
  //   });
  //   it('should reset the circuit to the initial values', function () {
  //     $scope.CancelEdit(0);
  //     expect($scope.circuitsGroups.list[0].editMode).toBeFalsy();
  //     expect($scope.circuitsGroups.list[0].sName).toBe('MAKE IT RAIN');
  //   });
  // });
  //
  // describe('Saving a single AUX circuit from a panel', function () {
  //   beforeEach(function () {
  //     $scope.SetEditMode($scope.panels[0].circuits[0]);
  //     $scope.panels[0].circuits[0].sName = 'new name';
  //     $scope.panels[0].circuits[0].eggTimerHours = 3;
  //     $scope.panels[0].circuits[0].eggTimerMinutes = 5;
  //
  //     // Jasmine spy over the SetParams service.
  //     // Since we provided a fake response already we can just call through.
  //     spyOn(PropertyService, 'SetParams').and.callThrough();
  //     spyOn(PropertyService, 'GetHardwareDefinition').and.callThrough();
  //   });
  //   it('should post to aux update to Server if form is valid', function () {
  //     $scope.SaveAUXCircuit($scope.panels[0].circuits[0]);
  //
  //     expect(PropertyService.SetParams.calls.all()[0].args[0]).toBe('CB683');
  //     expect(PropertyService.SetParams.calls.all()[0].args[1].SNAME).toBe('new name');
  //     expect(PropertyService.SetParams.calls.all()[0].args[1].TIME).toBe('185');
  //     expect(PropertyService.SetParams).toHaveBeenCalled();
  //     expect(PropertyService.GetHardwareDefinition).toHaveBeenCalled();
  //
  //     expect($scope.panels[0].circuits[0].editMode).toBeFalsy();
  //   });
  //   it('should Not post to aux update to server if form is not valid', function () {
  //     $scope.panels[0].circuits[0].editMode = true;
  //     $scope.panels[0].circuits[0].sName = '';
  //
  //     $scope.SaveAUXCircuit($scope.panels[0].circuits[0]);
  //
  //     expect(PropertyService.GetHardwareDefinition).not.toHaveBeenCalled();
  //     expect(PropertyService.SetParams).not.toHaveBeenCalled();
  //
  //     expect($scope.panels[0].circuits[0].editMode).toBeTruthy();
  //   });
  // });
  //
  // describe('Saving a single group circuit from the group listing', function () {
  //   beforeEach(function() {
  //     $scope.SetEditMode($scope.circuitsGroups.list[0]);
  //     $scope.circuitsGroups.list[0].sName = 'new name';
  //   });
  //   it('should post to group update to server if form is valid', function() {
  //     $scope.SaveGroupCircuit($scope.circuitsGroups.list[0]);
  //     expect($scope.circuitsGroups.list[0].editMode).toBeFalsy();
  //   });
  // });
  //
  // describe('Adding a circuit to a circuit group from the circuit group list', function() {
  //   it('should add a new group circuit', function() {
  //     expect($scope.circuitsGroups.list[0].circuits.length).toBe(1);
  //     $scope.addCircuitToCircuitsGroup($scope.circuitsGroups.list[0]);
  //     expect($scope.circuitsGroups.list[0].circuits.length).toBe(2);
  //   });
  // });
  //
  // describe('Deleting a circuit from a circuit group from the circuit group list', function() {
  //   it('should delete a grouped circuit', function() {
  //     expect($scope.circuitsGroups.list[0].circuits.length).toBe(1);
  //     $scope.deleteCircuitFromCircuitsGroup($scope.circuitsGroups.list[0], 0);
  //     expect($scope.circuitsGroups.list[0].circuits.length).toBe(0);
  //   });
  // });
  //
  // describe('Adding a circuit group to the circuit group list', function() {
  //   it('should add a new group', function() {
  //     expect($scope.circuitsGroups.list.length).toBe(2);
  //     $scope.addCircuitGroup();
  //     expect($scope.circuitsGroups.list.length).toBe(3);
  //   });
  // });
  //
  // describe('Removing circuit groups from the circuit group list that are selected', function() {
  //   beforeEach(function() {
  //     $scope.circuitsGroups.list[0].isSelected = true;
  //   });
  //   it('should remove groups that are selected', function() {
  //     expect($scope.circuitsGroups.list.length).toBe(2);
  //     $scope.removeCircuitGroups();
  //     expect($scope.circuitsGroups.list.length).toBe(1);
  //   });
  // });
  //
  // describe('comparing the names from the typeahead comparator', function() {
  //   it('should say they are equal', function() {
  //     expect($scope.nameComparator('this', 'thIs')).toBeTruthy();
  //   });
  // });
  //
  // describe('getting the dictionary long hand value based on the actual THW value', function() {
  //   it('should be able to find the long hand value', function() {
  //     expect($scope.getDictionaryValue('BOOST')).toBe('Heater Boost');
  //   });
  //   it('should return the undefined if undefined', function() {
  //     expect($scope.getDictionaryValue(undefined)).toBeUndefined();
  //   });
  // });

});
