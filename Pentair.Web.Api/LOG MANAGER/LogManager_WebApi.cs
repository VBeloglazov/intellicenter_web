﻿/*
--------------------------------------------------------------------------------

    LogManager_WebApi.cs

    Copyright © Pentair Water Pool and Spa, Inc. 2015

--------------------------------------------------------------------------------
*/

using System;

namespace SERVICE_SHARED
{
    // TODO: Instead of "async void" methods it can call "return Task" ones.
    //       If caller doesn't want to wait, it can ignore the return.
    public static class LogManager
    {
        public static ILogger_WebApi Logger { private get; set; }

        public static void LogError(
            string source,
            string eventBriefDescription,
            int installationId,
            Guid userId,
            Guid connectionId,
            string eventFullDescription)
        {
            Logger.LogError(source, eventBriefDescription, installationId, userId, connectionId, eventFullDescription);

        } // LogError()

        public static void LogWarning(
            string source,
            string eventBriefDescription,
            int installationId,
            Guid userId,
            Guid connectionId,
            string eventFullDescription,
            bool saveEventLog = false)
        {
            Logger.LogWarning(source, eventBriefDescription, installationId, userId, connectionId, eventFullDescription, saveEventLog);

        } // LogWarning()

        public static void LogInformation(
            string source,
            string eventBriefDescription,
            int installationId,
            Guid userId,
            Guid connectionId,
            string eventFullDescription,
            bool saveEventLog = false)
        {
            Logger.LogInformation(source, eventBriefDescription, installationId, userId, connectionId, eventFullDescription, saveEventLog);

        } // LogInformation()

        public static void LogErrorAsync(
            string source,
            string eventBriefDescription,
            int installationId,
            Guid userId,
            Guid connectionId,
            string eventFullDescription)
        {
            if (Logger != null)
            {
                Logger.LogErrorAsync(source, eventBriefDescription, installationId, userId, connectionId, eventFullDescription);
            }
            else
            {
                // TODO: Log to the EventViewer Application log. (New static class for that?)

                // HACK
                // TODO: It is unhandled! To reproduce, run as non-administrator.
                throw new Exception("Logger is null");
            }

        } // LogErrorAsync()

        public static void LogWarningAsync(
            string source,
            string eventBriefDescription,
            int installationId,
            Guid userId,
            Guid connectionId,
            string eventFullDescription,
            bool saveEventLog = false)
        {
            Logger.LogWarningAsync(source, eventBriefDescription, installationId, userId, connectionId, eventFullDescription, saveEventLog);

        } // LogWarningAsync()

        public static void LogInformationAsync(
            string source,
            string eventBriefDescription,
            int installationId,
            Guid userId,
            Guid connectionId,
            string eventFullDescription,
            bool saveEventLog = false)
        {
            Logger.LogInformationAsync(source, eventBriefDescription, installationId, userId, connectionId, eventFullDescription, saveEventLog);

        } // LogInformationAsync()

    } // class LogManager
}