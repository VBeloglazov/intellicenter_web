﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Helpers;

namespace Pentair.Web.Api
{
    public static class DateTimeHelper
    {
        public static DateTime ParseJavaScriptDateTimeToUtc(string dateTime)
        {
            return Convert.ToDateTime(dateTime).ToUniversalTime();
        }
    }
}