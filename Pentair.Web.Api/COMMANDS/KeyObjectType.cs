﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Pentair.Web.Api
{
    public class KeyObjectType
    {
        [JsonProperty("objtyp")]
        public string ObjTyp { get; private set; }

        [JsonProperty("keys")]
        public List<string> Keys { get; private set; }

        public KeyObjectType(
            string objtyp,
            List<string> keys)
        {
            this.ObjTyp = objtyp;
            this.Keys = keys;
        }

    } // class KeyObjectType
}
