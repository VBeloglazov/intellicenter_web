﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Pentair.Desktop.Simulator.BusinessLogic;
using Pentair.Domain.Client.Entity;
using Pentair.Domain.Client.Helpers.Commands;
using SuperSocket.SocketBase;
using SuperWebSocket;

namespace Pentair.Desktop.Simulator.Services
{ 
    public class LocalServer : WebSocketServer<LocalClient>
    {
        #region Private Properties

        private MessageProcessor _messageProcessor;
        #endregion

        #region Public Properties
        public ConnectionStatus Status { get; set; }
        public List<String> messages = new List<string>();
        #endregion

        #region Events and Delegates
        public delegate void LogMessage(string message, bool request);
        public event OnOpen OnOpenEvent;
        public delegate void OnOpen(Object sender, ClientSocketEventArgs e);
        public event OnClose OnCloseEvent;
        public delegate void OnClose(Object sender, ClientSocketEventArgs e);
//        public event OnError OnErrorEvent;
        public delegate void OnError(Object sender, ClientSocketEventArgs e);
//        public event OnMessage OnMessageEvent;
        public delegate void OnMessage(Object sender, ClientSocketEventArgs e);
        //public event AuthenticateError AuthenticateErrorEvent;
        public delegate void AuthenticateError(Object sender, ClientSocketEventArgs e);
        public event LogMessage LogMessageLocalEvent;
        #endregion


        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public LocalServer(MessageProcessor messageProcessor)
        {
            _messageProcessor = messageProcessor;
            Setup("Any", BusinessLogic.Helpers.LocalHostPort);
            Status = ConnectionStatus.Enabled;
            NewMessageReceived += OnNewMessage;
        }
        #endregion

        #region Public Methods

        public void CloseSessions()
        {
            foreach (var session in WebSocketConnections.Clients)
            {
                session.CloseWithHandshake(1, "Server Close Event");
            }
        }

        public void ChangeHost(Property host)
        {
            _messageProcessor.Configuration = host.LocalPropertyId;
            foreach (var session in WebSocketConnections.Clients)
            {
                session.CloseWithHandshake(1, "Pool Change Event");
            }
        }

        public void SendMessage(string msg)
        {
            foreach (var session in WebSocketConnections.Clients)
            {
                session.Send(msg);
            }
            if (null != LogMessageLocalEvent)
            {
                LogMessageLocalEvent(msg, false);
            }
        }

        public void SendNotificationParameterList(NotifyList msg)
        {
            var notifyList = JsonConvert.SerializeObject(msg);
            foreach (var session in WebSocketConnections.Clients)
            {
                session.Send(notifyList);
            }
            if (null != LogMessageLocalEvent)
            {
                LogMessageLocalEvent(notifyList, false);
            }
        }
        #endregion

        #region Private Methods
        protected override void OnNewSessionConnected(LocalClient session)
        {
            WebSocketConnections.Clients.Add(session);
            session.MsgProcessor = _messageProcessor;
            _messageProcessor.OnConfigurationChangeEvent += session.ConfigurationUpdate;
            if (null != OnOpenEvent)
            {
                OnOpenEvent(this, new ClientSocketEventArgs(ConnectionStatus.Connected, "Local Host Open for Business"));
            }
            session.LogMessageEvent += OnLogClientMessage;
        }

        protected override void OnSessionClosed(LocalClient session, CloseReason reason)
        {
            session.LogMessageEvent -= OnLogClientMessage;
            session.MsgProcessor.OnConfigurationChangeEvent -= session.ConfigurationUpdate;
            if (WebSocketConnections.Clients.Contains(session))
            {
                WebSocketConnections.Clients.Remove(session);
                if (null != OnCloseEvent)
                {
                    OnCloseEvent(this, new ClientSocketEventArgs(ConnectionStatus.Enabled, "Client closed connection"));
                }
                if (null != LogMessageLocalEvent)
                {
                    LogMessageLocalEvent(string.Format("Local client closed ({0})", session.SessionID), true);
                }
            }
            base.OnSessionClosed(session, reason);
        }

        /// <summary>
        /// A configuration change or notification event has occured, notify the client
        /// </summary>
        /// <param name="sender">MessageProcessor</param>
        /// <param name="e">WriteParam message</param>
        //public void NotifyClients(object sender, string e)
        //{
        //    foreach (var session in WebSocketConnections.Clients)
        //    {
        //        session.HandleReceived(e);
        //    }
        //}

        private void OnNewMessage(LocalClient session, string message)
        {
            session.ProcessMessage(message);
        }

        private void OnLogClientMessage(string message, bool request)
        {
            if (null != LogMessageLocalEvent)
            {
                LogMessageLocalEvent(message, request);
            }
        }

        #endregion
    }

    #region Helper Classes
    /// <summary>
    /// Installation connection cache.
    /// </summary>
    public static class WebSocketConnections
    {
        public static List<LocalClient> Clients = new List<LocalClient>();

        public static bool IsClientValid(string id)
        {
            return GetClientById(id) != null;
        }

        public static LocalClient GetClientById(string id)
        {
            return Clients.First(c => c.SessionID == id);
        }
    }
    #endregion
}
