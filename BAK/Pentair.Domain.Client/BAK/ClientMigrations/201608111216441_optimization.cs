namespace Pentair.Domain.Client.ClientMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class optimization : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Associations",
                c => new
                    {
                        UserId = c.Guid(nullable: false),
                        InstallationId = c.Int(nullable: false),
                        AccessToken = c.String(),
                        CreateDateTime = c.DateTime(nullable: false, precision: 7, storeType: "datetime2", defaultValueSql: "GETUTCDATE()"),
                        UpdateDateTime = c.DateTime(nullable: false, precision: 7, storeType: "datetime2", defaultValueSql: "GETUTCDATE()"),
                    })
                .PrimaryKey(t => new { t.UserId, t.InstallationId })
                .ForeignKey("dbo.Installations", t => t.InstallationId, cascadeDelete: true)
                .Index(t => t.InstallationId);
            
            CreateTable(
                "dbo.Installations",
                c => new
                    {
                        InstallationId = c.Int(nullable: false, identity: true),
                        ConnectionId = c.Guid(nullable: false),
                        Secret = c.String(maxLength: 255),
                        Name = c.String(maxLength: 255),
                        ProtocolVersion = c.String(maxLength: 128),
                        Connected = c.Boolean(nullable: false),
                        HostMachine = c.String(nullable: false),
                        LastChange = c.Long(nullable: false),
                        LastNotify = c.Long(nullable: false),
                        LastStatus = c.Long(nullable: false),
                        LastHistory = c.Long(nullable: false),
                        CreateDateTime = c.DateTime(nullable: false, precision: 7, storeType: "datetime2", defaultValueSql: "GETUTCDATE()"),
                        UpdateDateTime = c.DateTime(nullable: false, precision: 7, storeType: "datetime2", defaultValueSql: "GETUTCDATE()"),
                    })
                .PrimaryKey(t => t.InstallationId);
            
            CreateTable(
                "dbo.Connections",
                c => new
                    {
                        ConnectionId = c.Guid(nullable: false, identity: true),
                        HostMachine = c.String(nullable: false),
                        InstallationId = c.Int(nullable: false),
                        UserId = c.Guid(nullable: false),
                        CreateDateTime = c.DateTime(nullable: false, precision: 7, storeType: "datetime2", defaultValueSql: "GETUTCDATE()"),
                    })
                .PrimaryKey(t => t.ConnectionId)
                .ForeignKey("dbo.Installations", t => t.InstallationId, cascadeDelete: true)
                .Index(t => t.InstallationId);
            
            CreateTable(
                "dbo.NotificationRecipients",
                c => new
                    {
                        RecipientId = c.Int(nullable: false, identity: true),
                        InstallationId = c.Int(nullable: false),
                        Name = c.String(maxLength: 255),
                        EmailAddress1 = c.String(maxLength: 255),
                        EmailAddress2 = c.String(maxLength: 255),
                        Phone1 = c.String(maxLength: 20),
                        Phone2 = c.String(maxLength: 20),
                        CreateDateTime = c.DateTime(nullable: false, precision: 7, storeType: "datetime2", defaultValueSql: "GETUTCDATE()"),
                        UpdateDateTime = c.DateTime(nullable: false, precision: 7, storeType: "datetime2", defaultValueSql: "GETUTCDATE()"),
                    })
                .PrimaryKey(t => t.RecipientId)
                .ForeignKey("dbo.Installations", t => t.InstallationId, cascadeDelete: true)
                .Index(t => t.InstallationId);
            
            CreateTable(
                "dbo.UserInvitations",
                c => new
                    {
                        InvitationId = c.Guid(nullable: false),
                        EmailAddress = c.String(),
                        InstallationId = c.Int(),
                        AccessToken = c.String(),
                        Status = c.String(),
                        EmailId = c.String(),
                        CreateDateTime = c.DateTime(nullable: false, precision: 7, storeType: "datetime2", defaultValueSql: "GETUTCDATE()"),
                        UpdateDateTime = c.DateTime(nullable: false, precision: 7, storeType: "datetime2", defaultValueSql: "GETUTCDATE()"),
                    })
                .PrimaryKey(t => t.InvitationId)
                .ForeignKey("dbo.Installations", t => t.InstallationId)
                .Index(t => t.InstallationId);
            
            CreateTable(
                "dbo.ControlObjects",
                c => new
                    {
                        INSTALLATION = c.Int(nullable: false),
                        Revision = c.Long(nullable: false),
                        OBJNAM = c.String(nullable: false, maxLength: 16),
                        Key = c.String(nullable: false, maxLength: 16),
                        Value = c.String(maxLength: 256),
                        Deleted = c.Boolean(nullable: false),
                        ParamType = c.String(),
                        CreateDateTime = c.DateTime(nullable: false, precision: 7, storeType: "datetime2", defaultValueSql: "GETUTCDATE()"),
                        UpdateDateTime = c.DateTime(nullable: false, precision: 7, storeType: "datetime2", defaultValueSql: "GETUTCDATE()"),
                    })
                .PrimaryKey(t => new { t.INSTALLATION, t.Revision, t.OBJNAM, t.Key });
            
            CreateTable(
                "dbo.Events",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EventId = c.Int(nullable: false),
                        InstallationId = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                        CreateDateTime = c.DateTime(nullable: false, precision: 7, storeType: "datetime2", defaultValueSql: "GETUTCDATE()"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.EventTypes",
                c => new
                    {
                        EventId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.EventId);
            
            CreateTable(
                "dbo.ManufacturingInfo",
                c => new
                    {
                        Device_Mac_Address = c.Int(nullable: false, identity: true),
                        DeviceType = c.String(maxLength: 256),
                        CM_ManufactureDate = c.DateTime(precision: 7, storeType: "datetime2"),
                        QAD_SerialNumber = c.String(maxLength: 256),
                        EOL_ManufactureDate = c.DateTime(precision: 7, storeType: "datetime2"),
                        RSSI_Value = c.Int(),
                    })
                .PrimaryKey(t => t.Device_Mac_Address);
            
            CreateTable(
                "dbo.MessageLogs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Action = c.String(nullable: false),
                        Message = c.String(),
                        Timestamp = c.DateTime(nullable: false, precision: 7, storeType: "datetime2", defaultValueSql: "GETUTCDATE()"),
                        HardwareID = c.Int(nullable: false),
                        UserID = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.NotificationMessages",
                c => new
                    {
                        MessageId = c.String(nullable: false, maxLength: 10),
                        OBJTYP = c.String(nullable: false, maxLength: 10),
                        Notes = c.String(),
                        CreateDateTime = c.DateTime(nullable: false, precision: 7, storeType: "datetime2", defaultValueSql: "GETUTCDATE()"),
                        UpdateDateTime = c.DateTime(nullable: false, precision: 7, storeType: "datetime2", defaultValueSql: "GETUTCDATE()"),
                    })
                .PrimaryKey(t => new { t.MessageId, t.OBJTYP });
            
            CreateTable(
                "dbo.Subscriptions",
                c => new
                    {
                        ClientId = c.Guid(nullable: false),
                        PropertyId = c.Int(nullable: false),
                        ObjNam = c.String(nullable: false, maxLength: 128),
                        ObjParm = c.String(nullable: false, maxLength: 128),
                        CreateDateTime = c.DateTime(nullable: false, precision: 7, storeType: "datetime2", defaultValueSql: "GETUTCDATE()"),
                    })
                .PrimaryKey(t => new { t.ClientId, t.PropertyId, t.ObjNam, t.ObjParm });
            
            CreateTable(
                "dbo.UserNotifications",
                c => new
                    {
                        UserId = c.Guid(nullable: false),
                        InstallationId = c.Int(nullable: false),
                        EventId = c.Int(nullable: false),
                        Duration = c.Time(nullable: false, precision: 7),
                        NotificationType = c.Int(nullable: false),
                        NotificationSent = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        CreateDateTime = c.DateTime(nullable: false, precision: 7, storeType: "datetime2", defaultValueSql: "GETUTCDATE()"),
                        UpdateDateTime = c.DateTime(nullable: false, precision: 7, storeType: "datetime2", defaultValueSql: "GETUTCDATE()"),
                    })
                .PrimaryKey(t => new { t.UserId, t.InstallationId, t.EventId })
                .ForeignKey("dbo.EventTypes", t => t.EventId, cascadeDelete: true)
                .ForeignKey("dbo.Installations", t => t.InstallationId, cascadeDelete: true)
                .Index(t => t.InstallationId)
                .Index(t => t.EventId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserNotifications", "InstallationId", "dbo.Installations");
            DropForeignKey("dbo.UserNotifications", "EventId", "dbo.EventTypes");
            DropForeignKey("dbo.UserInvitations", "InstallationId", "dbo.Installations");
            DropForeignKey("dbo.NotificationRecipients", "InstallationId", "dbo.Installations");
            DropForeignKey("dbo.Connections", "InstallationId", "dbo.Installations");
            DropForeignKey("dbo.Associations", "InstallationId", "dbo.Installations");
            DropIndex("dbo.UserNotifications", new[] { "EventId" });
            DropIndex("dbo.UserNotifications", new[] { "InstallationId" });
            DropIndex("dbo.UserInvitations", new[] { "InstallationId" });
            DropIndex("dbo.NotificationRecipients", new[] { "InstallationId" });
            DropIndex("dbo.Connections", new[] { "InstallationId" });
            DropIndex("dbo.Associations", new[] { "InstallationId" });
            DropTable("dbo.UserNotifications");
            DropTable("dbo.Subscriptions");
            DropTable("dbo.NotificationMessages");
            DropTable("dbo.MessageLogs");
            DropTable("dbo.ManufacturingInfo");
            DropTable("dbo.EventTypes");
            DropTable("dbo.Events");
            DropTable("dbo.ControlObjects");
            DropTable("dbo.UserInvitations");
            DropTable("dbo.NotificationRecipients");
            DropTable("dbo.Connections");
            DropTable("dbo.Installations");
            DropTable("dbo.Associations");
        }
    }
}
