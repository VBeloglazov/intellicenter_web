﻿using System;
using System.Configuration;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using Newtonsoft.Json;
using Pentair.Desktop.Simulator.BusinessLogic;
using Pentair.Domain.Client.Entity;
using Pentair.Domain.Client.Helpers;
using Pentair.Domain.Client.Helpers.Commands;
using Pentair.Domain.Client.Infrastructure;
using WebSocket4Net;
using WebSocket = WebSocket4Net.WebSocket;
using WebSocketState = WebSocket4Net.WebSocketState;

namespace Pentair.Desktop.Simulator.Services
{
    public class ClientSocket : IDisposable
    {
        #region Private Properties

        private const int PingTime = 1000*60*10;

        private WebSocket _clientSocket;
        private Property _property;
        private readonly Guid _client = Guid.Empty;
        private readonly object _lockObject = new object();
        private readonly string[] _messageQueue = new string[32];
        private int _nextIn;
        private int _nextOut;
        private Timer _keepAliveTimer;
        private readonly MessageProcessor _messageProcessor;
        #endregion

        #region Public Properties
        public ConnectionStatus Status { get; private set; }
        public Property Connection { get { return _property; } }

        public int Configuration
        {
            get { return _messageProcessor.Configuration; }
            set { _messageProcessor.Configuration = value; }
        }
        #endregion

        #region Events and Delegates
        public event OnOpen OnOpenEvent;
        public delegate void OnOpen(Object sender, ClientSocketEventArgs e);
        public event OnClose OnCloseEvent;
        public delegate void OnClose(Object sender, ClientSocketEventArgs e);
        public event OnError OnErrorEvent;
        public delegate void OnError(Object sender, ClientSocketEventArgs e);
        public event OnMessage OnMessageEvent;
        public delegate void OnMessage(Object sender, ClientSocketEventArgs e);
        public event LogMessage LogMessageEvent;
        public delegate void LogMessage(string message, bool direction);
        public event RegisterDevice RegisterDeviceEvent;
        public delegate void RegisterDevice(RegistrationResponse message);
        public event AuthenticateError AuthenticateErrorEvent;
        public delegate void AuthenticateError(Object sender, ClientSocketEventArgs e);
        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public ClientSocket(MessageProcessor messageProcessor)
        {
            _nextIn = _nextOut = 0;
            _messageProcessor = messageProcessor;
        }

        ~ClientSocket()
        {
            Dispose();
        }

        public void Dispose()
        {
            if (null != _keepAliveTimer)
            {
                _keepAliveTimer.Dispose();
            }
            _clientSocket = null;
        }

        #endregion

        #region Public Methods
        /// <summary>
        /// Queue the message type and assign its ID.
        /// </summary>
        /// <param name="message">Message type being sent</param>
        /// <returns>Unique message id</returns>
        public void QueueMessage(string message)
        {
            _messageQueue[_nextIn] = message;
            _nextIn = (_nextIn + 1) % 32;
        }

        /// <summary>
        /// OpenConnection
        /// </summary>
        /// <param name="property">data object containing the property Id and secret</param>
        public void OpenConnection(Property property)
        {
            if (null == _clientSocket)
            {
                _property = property;
                _clientSocket = new WebSocket(property.RemoteHost) { AllowUnstrustedCertificate = true };
                _clientSocket.Opened += onOpen;
                _clientSocket.Error += onError;
                _clientSocket.Closed += onClose;
                _clientSocket.MessageReceived += onReceiveMessage;
            }


            if (WebSocketState.Closed != _clientSocket.State)
            {
                if (WebSocketState.Connecting == _clientSocket.State ||
                    WebSocketState.Open == _clientSocket.State)
                {
                    return;
                }

                if (WebSocketState.Closing == _clientSocket.State)
                {
                    while (WebSocketState.Closing == _clientSocket.State)
                    {
                        Thread.Sleep(10);
                    }

                }
            }
            Status = ConnectionStatus.Enabled;
            _clientSocket.Open();
        }

        public void OpenConnection(string message, string host)
        {
            if (null == _clientSocket)
            {
                _clientSocket = new WebSocket(host) { AllowUnstrustedCertificate = true };
                _clientSocket.Opened += onOpen;
                _clientSocket.Error += onError;
                _clientSocket.Closed += onClose;
                _clientSocket.MessageReceived += onReceiveMessage;
            }

            // queue the message.
            SendMessage(message);

            if (WebSocketState.Open != _clientSocket.State)
            {
                Status = ConnectionStatus.Enabled;
                _clientSocket.Open();
            }
        }

        /// <summary>
        /// Close the web socket connection
        /// </summary>
        public void CloseConnection()
        {
            if (null == _clientSocket)
            {
                return;
            }
            lock (_lockObject)
            {
                while (_clientSocket.State != WebSocketState.Open &&
                       _clientSocket.State != WebSocketState.Closed)
                {
                    Task.Delay(20);
                }
                if (_clientSocket.State == WebSocketState.Open)
                {
                    _clientSocket.Close();
                    while (_clientSocket.State != WebSocketState.Closed)
                    {
                        Task.Delay(20);
                    }
                }
                _clientSocket = null;
            }
        }

        /// <summary>
        /// Send message to the pentair server
        /// </summary>
        /// <param name="message">Json message to send</param>
        public void SendMessage(string message)
        {
            var logMsg = message;
            try
            {
                if (_clientSocket.State != WebSocketState.Open)
                {
                    QueueMessage(message);
                }
                else
                {
                    lock (_lockObject)
                    {
                        try
                        {
                            _clientSocket.Send(message);
                        }
                        catch (Exception ex)
                        {
                            if (null != LogMessageEvent)
                            {
                                LogMessageEvent(
                                    string.Format("Error: {0}, Original Message: {1}", ex.Message, message), false);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logMsg = string.Format("Error: {0}, Original Message: {1}", ex.Message, message);
            }
            finally
            {
                if (null != LogMessageEvent)
                {
                    LogMessageEvent(logMsg, false);
                }
            }
        }
        public void ChangeHost(Property host)
        {
            Configuration = host.LocalPropertyId;
            if (null == _clientSocket)
            {
                OpenConnection(host);
            }
            else
            {
                _property = host;
                CloseConnection();
            }
        }

        public void SendWriteParameterList(WriteParamListResponse msg)
        {
            SendMessage(JsonConvert.SerializeObject(msg));
        }

        public void SendNotificationParameterList(NotifyList msg)
        {
            SendMessage(JsonConvert.SerializeObject(msg));
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// A configuration change has been made, notify the client
        /// </summary>
        /// <param name="sender">MessageProcessor</param>
        /// <param name="e">WriteParam message</param>
        public void ConfigurationUpdate(object sender, string e)
        {
            SendMessage(e);
        }

        private void PingServer(object sender)
        {
            if (null == _clientSocket)
            {
                _keepAliveTimer.Dispose();
                CloseConnection();
            }
            lock (_lockObject)
            {
                _clientSocket.Send("ping");
            }
        }

        private void onOpen(Object sender, EventArgs e)
        {
            if (null != _property)
            {
                _messageProcessor.Property = _property.PropertyId;
                if (!String.IsNullOrEmpty(_property.Secret))
                {
                    const string command = CommandString.Authenticate;
                    var authRequest = String.Format(
                        "{{\"command\": \"{0}\",\"PropertyId\": \"{1}\",\"sharedSecret\": \"{2}\",\"apiVersion\": \"{3}\"}}",
                        command,
                        _property.PropertyId,
                        _property.Secret,
                        Messages.PROTOCOL_VERSION);
                    SendMessage(authRequest);
                    _messageProcessor.ClearSubscriptions(_client);
                    Status = ConnectionStatus.Authenticating;
                }
                if (_nextIn != _nextOut)
                {
                    SendMessage(_messageQueue[_nextOut]);
                    _nextOut = (_nextOut + 1)%32;
                }
            }
            else
            {
                if (_nextIn != _nextOut)
                {
                    SendMessage(_messageQueue[_nextOut]);
                    _nextOut = (_nextOut + 1) % 32;
                }
            }

            if (null != OnOpenEvent && null != _property && null != _property.RemoteHost)
            {
                OnOpenEvent(this, new ClientSocketEventArgs(Status, String.Format("Client web socket opened ({0}).", _property.RemoteHost)));
            }

            _keepAliveTimer = new Timer(PingServer, null, PingTime, PingTime);
        }

        private void onClose(Object sender, EventArgs e)
        {
            if (null != _keepAliveTimer)
            {
                _keepAliveTimer.Dispose();
            }
            var authenticating = ConnectionStatus.Authenticating == Status;
            lock (_lockObject)
            {
                if (null != _clientSocket)
                {
                    if (null != _property) {
                        if (-1 < _property.PropertyId)
                        {
                            _messageProcessor.ClearSubscriptions(_client);
                        }
                    }
                    _clientSocket.Opened -= onOpen;
                    _clientSocket.Error -= onError;
                    _clientSocket.Closed -= onClose;
                    _clientSocket.MessageReceived -= onReceiveMessage;
                    _clientSocket = null;
                }
            }
            Status = ConnectionStatus.Disabled;
            if (authenticating)
            {
                if (null != AuthenticateErrorEvent)
                {
                    AuthenticateErrorEvent(this, new ClientSocketEventArgs(Status, "Authenticate error"));
                }
            }
            else if (null != OnCloseEvent)
            {
                OnCloseEvent(this, new ClientSocketEventArgs(Status, "Client web socket closed."));
            }
        }

        private void onError(Object sender, EventArgs e)
        {
            string message = String.Format("Client web socket error: {0}", ((SuperSocket.ClientEngine.ErrorEventArgs)e).Exception.Message);
            if (null != OnErrorEvent)
            {
                OnErrorEvent(this, new ClientSocketEventArgs(Status, message));
            }
        }

        private void onReceiveMessage(Object sender, MessageReceivedEventArgs e)
        {
            if ("pong" == e.Message)
            {
                return;
            }
            if (null != LogMessageEvent)
            {
                LogMessageEvent(e.Message, true);
            }
            MessageParser(e.Message);
        }
        #endregion

        #region Private Methods

        private void MessageParser(string message)
        {
            // Parse the message type
            var messageType = WebSocketMessage.ParseMessageType(message, EndpointType.Server);

            switch (messageType.CommandType)
            {
                case CommandType.SuccessMessage:
                    ProcessSuccess(message);
                    break;
                case CommandType.ErrorMessage:
                    ProcessError(message);
                    break;
                case CommandType.RegistrationAcknowledgement:
                    if (null != RegisterDeviceEvent)
                    {
                        try
                        {
                            var msg = JsonConvert.DeserializeObject<RegistrationResponse>(message);
                            if (msg.Response == ResponseString.OK ||
                                msg.Response == ResponseString.Created)
                            {
                                if (null == msg.PropertyID ||
                                    null == msg.SharedSecret)
                                {
                                    var error =
                                        String.Format(
                                            "Error parsing Registration Acknowledgement, PropertyId: {0} Secret: {1}",
                                            msg.PropertyID, msg.SharedSecret);
                                    LogMessageEvent(error, true);
                                    _messageProcessor.CloseSocket();
                                }
                                else
                                {
                                    RegisterDeviceEvent(msg);
                                }
                            }
                            else
                            {
                                var error = String.Format("Error registering property, Response code: {0}", msg.Response);
                                LogMessageEvent(error, true);
                                _messageProcessor.CloseSocket();
                            }
                        }
                        catch (Exception ex)
                        {
                            if (null != LogMessageEvent)
                            {
                                var error = String.Format("Error parsing Registration Acknowledgement, Error: {0}",
                                    ex.Message);
                                LogMessageEvent(error, true);
                                _messageProcessor.CloseSocket();
                            }
                        }
                    }
                    break;

                default:
                    var rsp = _messageProcessor.ProcessMessage(message, _client);
                    if (null != rsp)
                    {
                        SendMessage(rsp);
                    }
                    break;
            }
        }

        private void ProcessSuccess(string message)
        {
            if (Status == ConnectionStatus.Authenticating)
            {
                Status = ConnectionStatus.Connected;
                if (null != OnMessageEvent)
                {
                    OnMessageEvent(this, new ClientSocketEventArgs(Status, message));
                }
            }
        }

        private void ProcessError(string message)
        {
            if (Status == ConnectionStatus.Authenticating)
            {
                Status = ConnectionStatus.Connected;
                if (null != AuthenticateErrorEvent)
                {
                    AuthenticateErrorEvent(this, new ClientSocketEventArgs(Status, message));
                }
            }
        }

        #endregion
    }
}
