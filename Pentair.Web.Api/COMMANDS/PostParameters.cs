namespace Pentair.Web.Api
{
    public class PostParameters
    {
        public string Action { get; private set; }
        public string Message { get; private set; }
        public string Timestamp { get; private set; }
        public string HardwareId { get; private set; }
    }
}