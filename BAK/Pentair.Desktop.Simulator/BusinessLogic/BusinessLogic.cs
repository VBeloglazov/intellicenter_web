﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using System.Web.Http.Description;
using Newtonsoft.Json;
using Pentair.Desktop.Simulator.Views;
using Pentair.Domain.Client.BusinessLogic;
using Pentair.Domain.Client.DataLayer;
using Pentair.Domain.Client.Entity;
using Pentair.Domain.Client.Helpers;
using Pentair.Domain.Client.Helpers.Commands;
using Pentair.Domain.Client.Helpers.Entity;
using Pentair.Domain.Client.Infrastructure;
using Pentair.Web.Api.WebSockets;

namespace Pentair.Desktop.Simulator.BusinessLogic
{
    public enum ObjectStatus
    {
        On,
        Off,
        DlyOn,
        DlyOff,
        Destroy,
        Unchanged,
    }

    /// <summary>
    /// The business logic class provides simulation of communications responces
    /// </summary>
    public class BusinessLogic : IBusinessLogic
    {
        #region Private Properties

        private readonly ISimulatorContext _data;
        private readonly int _installation;
        private readonly List<ControlObject> _changes;
        private List<ObjectView> _queryResult;
        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="data">Data object for the simulator</param>
        /// <param name="installation">Unique installation name</param>
        public BusinessLogic(ISimulatorContext data, int installation)
        {
            _data = data;
            _installation = installation;
            _queryResult = new List<ObjectView>();
            _changes = new List<ControlObject>();
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// GetParamList retrieves and returns the requested parameters to the caller
        /// </summary>
        /// <param name="request">List of parameters requested</param>
        /// <returns></returns>
        public List<ParamObject> GetParamList(GetParamList request)
        {
            var result = new List<ParamObject>();

            var co = new ObjectManager(_data, _installation);

            foreach (var item in request.ObjectList)
            {
                string[] requests = item.Keys[0].Split(new char[] { '?' });
                if (null == requests[0])
                {
                    throw new Exception("Parse error, null request.");
                }
                var parameterList = requests[0].Split(new char[] { ':' });
                if (requests.GetUpperBound(0) > 1)
                {
                    throw new Exception("Parse error, compound conditional.");
                }

                // If this is request contains a deprecated query string, error out.
                if (requests.GetUpperBound(0) > 0)
                {
                    throw new Exception("deprecated call");
                }
                if (!String.IsNullOrEmpty(request.Condition))
                {
                    var parser = new ProcessQueryString(request.Condition, _data, _installation);
                    List<string> tmpResults = parser.Query();
                    _queryResult = (from c in _data.ObjectViews
                                    where c.INSTALLATION == _installation &&
                                    tmpResults.Contains(c.OBJNAM)
                                    select c).ToList();
                }
                else
                {
                    if (item.ObjNam == "ALL")
                    {
                        _queryResult = (from c in _data.ObjectViews
                                        where c.INSTALLATION == _installation
                                        select c).ToList();
                    }
                    else
                    {
                        _queryResult = (from c in _data.ObjectViews
                                        where c.INSTALLATION == _installation
                                              && c.OBJNAM == item.ObjNam
                                        select c).ToList();
                    }
                }
                result.AddRange(co.BuildResponse(_queryResult, parameterList));

            } // foreach (var item in request.ObjectList)

            return result;

        } // GetParamList()

        // Delete lightgroup
        public bool DeleteLightGrouping(object args)
        {
            var result = true;
            var json = JsonConvert.DeserializeObject<Dictionary<string, object>>(args.ToString());
            var objnam = json["objnam"] as string;
            if (string.IsNullOrEmpty(objnam))
            {
                return false;
            }
            var revision = Globals.CurrentTime();
            var groupCircuit = _data.ObjectViews.FirstOrDefault(x => x.OBJNAM == objnam && x.INSTALLATION == _installation);
            if (null != groupCircuit)
            {
                var children = _data.ObjectViews.Where(x => x.PARENT == groupCircuit.OBJNAM && x.INSTALLATION == _installation).ToList();
                foreach (var child in children)
                {
                    var o = new ControlObject
                    {
                        OBJNAM = child.OBJNAM,
                        INSTALLATION = _installation,
                        Deleted = true,
                        Revision = revision,
                        Key = StaticData.Status,
                        Value = "DSTROY"
                    };
                    DeleteObject(o);
                    _changes.Add(o);
                }
                var p = new ControlObject
                {
                    OBJNAM = groupCircuit.OBJNAM,
                    INSTALLATION = _installation,
                    Deleted = true,
                    Revision = revision,
                    Key = StaticData.Status,
                    Value = "DSTROY"
                };
                DeleteObject(p);
                _changes.Add(p);
            }
            return result;
        }

        public List<ParamObject> SaveLightGrouping(object args)
        {
            var result = new List<ParamObject>();

            var json = JsonConvert.DeserializeObject<Dictionary<string, object>>(args.ToString());
            if (null == json)
            {
                return result;
            }

            var circuits = json["LIGHTS"] as IEnumerable<object>;
            if (null == circuits)
            {
                return result;
            }

            var lights = new List<string>();
            foreach (var item in circuits)
            {
                lights.Add(item.ToString());
            }
            var objnam = json["objnam"] as string;
            var name = json["SNAME"] as string;
            var showMenu = json["SHOMNU"] as string;
            var freeze = json["FREEZE"] as string;

            if (lights.Count < 1 ||
                null == objnam ||
                null == name ||
                null == showMenu ||
                null == freeze)
            {
                return result;
            }

            var circuitGroup = _data.ObjectViews.FirstOrDefault(x => x.OBJNAM == objnam && x.INSTALLATION == _installation);
            if (null != circuitGroup)
            {
                var revision = Globals.CurrentTime();
                if (circuitGroup.NAME != name)
                {
                    var o =
                        _data.ControlObjects.FirstOrDefault(x => x.INSTALLATION == _installation &&
                                                            x.OBJNAM == circuitGroup.OBJNAM &&
                                                            x.Key == StaticData.SystemName);
                    if (null != o)
                    {
                        o.Value = name;
                        o.Revision = revision;
                        _data.SaveChanges();
                    }
                }

                var children = _data.ObjectViews.Where(x => x.PARENT == circuitGroup.OBJNAM && x.INSTALLATION == _installation).ToList();
                // First delete the group circuits no longer needed.
                foreach (var child in children)
                {
                    if (!lights.Contains(child.CIRCUIT))
                    {
                        var o = new ControlObject
                        {
                            OBJNAM = child.OBJNAM,
                            INSTALLATION = _installation,
                            Deleted = true,
                            Revision = revision,
                            Key = StaticData.Status,
                            Value = "DSTROY"
                        };
                        DeleteObject(o);
                        _changes.Add(o);
                    }
                }
                // Now add the new group circuits.
                foreach (var light in lights)
                {
                    var newFlag = true;
                    foreach (var child in children)
                    {
                        if (light == child.CIRCUIT)
                        {
                            newFlag = false;
                        }
                    }
                    if (newFlag)
                    {
                        createGroupObject(circuitGroup.OBJNAM, light, revision);
                    }
                }
            }
            return result;
        }

        public List<ParamObject> CreateLightGrouping(object args)
        {
            var result = new List<ParamObject>();

            var json = JsonConvert.DeserializeObject<Dictionary<string, object>>(args.ToString());
            if (null == json)
            {
                return result;
            }

            var circuits = json["LIGHTS"] as IEnumerable<object>;
            if (null == circuits)
            {
                return result;
            }

            var lights = new List<string>();
            foreach (var item in circuits)
            {
                lights.Add(item.ToString());
            }
            var name = json["SNAME"] as string;
            var showMenu = json["SHOMNU"] as string;
            var freeze = json["FREEZE"] as string;

            if (lights.Count < 1 ||
                null == name ||
                null == showMenu ||
                null == freeze)
            {
                return result;
            }

            var revision = Globals.CurrentTime();
            var objName = CreateObject(StaticData.CircuitType);
            var lightCapabilities = LightGroupCapabilities(lights);
            string child = "";
            foreach (var item in lights)
            {
                child = child + item + ",";
            }
            child = child.Substring(0, child.Length - 1);

            // Create the group circuit
            CreateControlObject(objName, StaticData.SubType, StaticData.LightShowType, revision);
            CreateControlObject(objName, StaticData.SystemName, name, revision);
            CreateControlObject(objName, StaticData.ListOrder, "1", revision);
            CreateControlObject(objName, "SHOMNU", showMenu, revision);
            CreateControlObject(objName, "USAGE", lightCapabilities, revision);
            CreateControlObject(objName, "CHILD", child, revision);
            CreateControlObject(objName, StaticData.Status, "OFF", revision);

            result.Add(
                new ParamObject(
                    objNam: objName,
                    parameters: new Dictionary<string, object>()
                    {
                        { StaticData.ObjectType, StaticData.CircuitType },
                        { StaticData.SystemName, name },
                        { StaticData.SubType, StaticData.CircuitGroupType },
                        { StaticData.ListOrder, "1" },
                        { "SHOMNU", showMenu },
                        { "USAGE", lightCapabilities },
                        { "COUNT", lights.Count },
                        { "OBJLIST", getGroupObjects(objName, lights, revision) }
                    }
                )
            );

            return result;

        } // CreateLightGrouping()

        public bool SetBodyState(object args)
        {
            var settings = JsonConvert.DeserializeObject<Dictionary<string, object>>(args.ToString());

            if (null == settings)
            {
                return false;
            }

            if (settings.Keys.Count != 1)
            {
                return false;
            }
            var obj = settings.Keys.FirstOrDefault();
            if (null == obj)
            {
                return false;
            }

            return toggleBodyState(obj, ((string)settings[obj]).ToString());
        }

        public void MasterOff()
        {
            // Turn off the bodies
            foreach (var body in _data.ObjectViews
                .Where(x => x.OBJTYP == StaticData.BodyType && x.INSTALLATION == _installation)
                .ToList())
            {
                var b = _data.ControlObjects.FirstOrDefault(x => x.INSTALLATION == _installation &&
                                                                 x.OBJNAM == body.OBJNAM &&
                                                                 x.Key == StaticData.Status);
                if (null != b && b.Value.ToUpper() == "ON")
                {
                    toggleBodyState(body.OBJNAM, "OFF");
                }
            }

            var om = new ObjectManager(_data, _installation);

            foreach (var circuit in _data.ObjectViews
                    .Where(x => x.OBJTYP == StaticData.CircuitType && x.INSTALLATION == _installation).ToList())
            {
                var revision = Globals.CurrentTime();

                if (Statics.SimpleCircuits.Contains(circuit.SUBTYP))
                {
                    var c = (from o in _data.ControlObjects
                             where o.INSTALLATION == _installation &&
                                   o.OBJNAM == circuit.OBJNAM &&
                                   o.Key == StaticData.Status
                             select o).FirstOrDefault();

                    if (null != c)
                    {
                        c.Value = "OFF";
                        c.Revision = revision;
                        om.UpdateParameter(c);
                        _changes.Add(c);
                    }
                }
            }

        } // MasterOff()

        public bool SetStatusMessageMasks(object args)
        {

            var settings = args as Dictionary<string, string>;

            if (null == settings)
            {
                return false;
            }

            var obj = (from o in _data.ObjectViews
                       where o.INSTALLATION == _installation
                       select o).ToList();

            foreach (var o in obj)
            {
                switch (o.OBJTYP)
                {
                    case StaticData.PumpType:
                        UpdateStatusMask(o, settings["PUMP"]);
                        break;
                    case StaticData.ChemistryType:
                        if (o.SUBTYP == "ICHEM")
                        {
                            UpdateStatusMask(o, settings["ICHEM"]);
                        }
                        else
                        {
                            UpdateStatusMask(o, settings["ICHLOR"]);
                        }
                        break;
                    case StaticData.CircuitType:
                        if (o.SUBTYP == "FRZ")
                        {
                            UpdateStatusMask(o, settings["CIRCUIT"]);
                        }
                        break;
                    case StaticData.HeaterType:
                        UpdateStatusMask(o, settings["HEATER"]);
                        break;
                }
            }

            return true;
        }

        public bool ClearStatusMessages()
        {
            var revision = Globals.CurrentTime();
            foreach (var message in _data.ObjectViews
                .Where(x => x.OBJTYP == StaticData.StatusMessageType &&
                            x.INSTALLATION == _installation &&
                            !x.Deleted))
            {
                var o = _data.ControlObjects.FirstOrDefault(x => x.OBJNAM == message.OBJNAM &&
                                                                 x.INSTALLATION == _installation &&
                                                                 x.Key == StaticData.ShowMenu);
                if (null != o && o.Value == "OFF")
                {
                    o.Value = "ON";
                    o.Revision = revision;
                    _changes.Add(o);
                }
            }
            _data.SaveChanges();

            return true;
        }

        public List<ParamObject> GetCompatibleLightCircuits(object parms)
        {
            var result = new List<ParamObject>();
            var objNam = parms.ToString();
            var lightCircuit = _data.ObjectViews.FirstOrDefault(x => x.OBJNAM == objNam && x.INSTALLATION == _installation);
            if (null == lightCircuit)
            {
                return result;
            }
            var filter = new List<String>();
            switch (lightCircuit.SUBTYP.ToUpper())
            {
                case "MAGIC1":
                    filter = StaticData.LightGroupZ;
                    break;
                case "MAGIC2":
                    filter = StaticData.LightGroupY;
                    break;
                case "INTELLI":
                    filter = StaticData.LightGroupY;
                    break;
                case "SAML":
                    filter = StaticData.LightGroupY;
                    break;
                case "COLORW":
                    filter.Add(lightCircuit.SUBTYP.ToUpper());
                    break;
                case "PHOTON":
                    filter.Add(lightCircuit.SUBTYP.ToUpper());
                    break;
                case "DIMMER":
                    filter.Add(lightCircuit.SUBTYP.ToUpper());
                    break;
                case "LIGHT":
                    filter.Add(lightCircuit.SUBTYP.ToUpper());
                    break;
                case "GLOW":
                    filter = StaticData.LightGroupY;
                    break;
                case "GLOWT":
                    filter.Add(lightCircuit.SUBTYP.ToUpper());
                    break;
                default:
                    filter.Add(lightCircuit.SUBTYP.ToUpper());
                    break;
            }
            var compatibleCircuits = _data.ObjectViews.Where(x => filter.Contains(x.SUBTYP) && x.INSTALLATION == _installation).ToList();

            result.AddRange(
                compatibleCircuits.Select(circuit =>
                    new ParamObject(
                        objNam: circuit.OBJNAM,
                        parameters: new Dictionary<string, object>()
                        {
                            { StaticData.ObjectType, circuit.OBJTYP },
                            { StaticData.SubType, circuit.SUBTYP },
                            { StaticData.SystemName, circuit.SNAME }
                        }
                    )
                )
            );

            return result;

        } // GetCompatibleLightCircuits()

        public List<ParamObject> GetConfiguration()
        {
            var result = new List<ParamObject>();
            try
            {
                // Build the singletons
                var security = _data.ObjectViews.FirstOrDefault(x => x.OBJNAM == StaticData.Admin && x.INSTALLATION == _installation);
                if (null != security)
                {
                    var m = new ParamObject(
                        objNam: security.OBJNAM,
                        parameters: new Dictionary<string, object>()
                        {
                            {"ENABLE", security.ENABLE}
                        }
                    );

                    result.Add(m);
                }

                var local = _data.ObjectViews.FirstOrDefault(x => x.OBJNAM == StaticData.Location && x.INSTALLATION == _installation);
                if (null != local)
                {
                    var m = new ParamObject(
                        objNam: local.OBJNAM,
                        parameters: new Dictionary<string, object>()
                        {
                            {"CLK24A", local.CLK24A}
                        }
                    );

                    result.Add(m);
                }
                var preferences = _data.ObjectViews.FirstOrDefault(x => x.OBJNAM == StaticData.Preferences && x.INSTALLATION == _installation);
                if (null != preferences)
                {
                    var m = new ParamObject(
                        objNam: preferences.OBJNAM,
                        parameters: new Dictionary<string, object>()
                        {
                            { StaticData.Mode, preferences.MODE }
                        }
                    );

                    result.Add(m);
                }
                // Build the body objects
                foreach (var body in _data.ObjectViews
                    .Where(x => x.OBJTYP == StaticData.BodyType && x.INSTALLATION == _installation)
                    .ToList())
                {
                    var b = new ParamObject(
                        objNam: body.OBJNAM,
                        parameters: new Dictionary<string, Object>()
                        {
                            {StaticData.ObjectType, body.OBJTYP},
                            {StaticData.SubType, body.SUBTYP},
                            {StaticData.SystemName, body.SNAME},
                            {StaticData.LowTemp, body.LOTMP},
                            {StaticData.HiTemp, body.HITMP},
                            {StaticData.Share, body.SHARE},
                            {StaticData.Prime, body.PRIM},
                            {StaticData.ListOrder, body.LISTORD},
                            {"SEC", body.SEC},
                            {"ACT1", body.ACT1},
                            {"ACT2", body.ACT2},
                            {"ACT3", body.ACT3},
                            {"ACT4", body.ACT4}
                        }
                    );

                    var children = new List<ParamObject>();
                    foreach (var child in (from h in _data.ObjectViews
                                           where h.INSTALLATION == _installation &&
                                                 (h.OBJTYP == StaticData.HeaterType ||
                                                  h.OBJTYP == StaticData.HeatComboType ||
                                                  h.OBJTYP == StaticData.ChemistryType) &&
                                                 (h.BODY == body.OBJNAM || h.SHARE == body.OBJNAM)
                                           select h).ToList())
                    {
                        children.Add(
                            new ParamObject(
                                objNam: child.OBJNAM,
                                parameters: new Dictionary<string, object>()
                                {
                                    { StaticData.ObjectType, child.OBJTYP },
                                    { StaticData.SubType, child.SUBTYP },
                                    { StaticData.SystemName, child.SNAME },
                                    { StaticData.ListOrder, child.LISTORD }
                                }
                            )
                        );

                    } // foreach (var child)

                    // Are there heaters for this body?
                    if (null != _data.ObjectViews.FirstOrDefault(x => x.INSTALLATION == _installation &&
                                                        (x.OBJTYP == StaticData.HeaterType ||
                                                         x.OBJTYP == StaticData.HeatComboType) &&
                                                        (x.BODY == body.OBJNAM || x.SHARE == body.OBJNAM)))
                    {
                        children.Add(
                            new ParamObject(
                                objNam: "00000",
                                parameters: new Dictionary<string, object>()
                                {
                                    { StaticData.ObjectType, StaticData.HeaterType },
                                    { StaticData.SubType, "GENERIC" },
                                    { StaticData.SystemName, "Heat Mode Off" },
                                    { StaticData.ListOrder, "0" }
                                }
                            )
                        );
                    }
                    else
                    {
                        children.Add(
                            new ParamObject(
                                objNam: "00000",
                                parameters: new Dictionary<string, object>()
                                {
                                    { StaticData.ObjectType, StaticData.HeaterType },
                                    { StaticData.SubType, "GENERIC" },
                                    { StaticData.SystemName, "No Heater" },
                                    { StaticData.ListOrder, "0" }
                                }
                            )
                        );
                    }

                    b.Parameters.Add("OBJLIST", children);

                    result.Add(b);
                }

                // Add all light circuits and light group circuits.
                foreach (var light in (from l in _data.ObjectViews
                                       where l.INSTALLATION == _installation &&
                                             l.OBJTYP == StaticData.CircuitType &&
                                             !l.Deleted &&
                                             Statics.SimpleCircuits.Contains(l.SUBTYP)
                                       select l).ToList())
                {
                    if (!light.SHOMNU.Contains("l") && !light.SHOMNU.Contains("f"))
                    {
                        continue;
                    }
                    var affects = (from a in _data.ObjectViews
                                   where a.CIRCUIT == light.OBJNAM &&
                                         !a.Deleted &&
                                         (a.OBJTYP == StaticData.CircuitGroupType ||
                                          a.OBJTYP == StaticData.LightShowType)
                                   select a).ToList();

                    var circuit = new ParamObject(
                        objNam: light.OBJNAM,
                        parameters: new Dictionary<string, object>()
                        {
                            { StaticData.ObjectType, light.OBJTYP },
                            { StaticData.SubType, light.SUBTYP },
                            { StaticData.SystemName, light.SNAME },
                            { StaticData.ListOrder, light.LISTORD },
                            { StaticData.ShowMenu, light.SHOMNU },
                            { "USE", light.ACT },
                            { "USAGE", light.USAGE }
                        }
                    );

                    var children = affects.Select(affect =>
                        new ParamObject(
                            objNam: affect.OBJNAM,
                            parameters: new Dictionary<string, object>()
                            {
                                { StaticData.ObjectType, light.OBJTYP }, 
                                { StaticData.CircuitType, light.CIRCUIT },
                                { StaticData.Action, light.ACT }, 
                                { StaticData.ListOrder, light.LISTORD },
                                { StaticData.Delay, light }
                            }
                        )
                    )
                    .ToList();

                    if (children.Count > 0)
                    {
                        circuit.Parameters.Add("OBJLIST", children);
                    }

                    result.Add(circuit);
                
                } // foreach (var light)
            }
            catch (Exception ex)
            {
                // VB: What the hack is that?! Exception handling a la Pathfinder? ;)
                var m = ex.Message;
            }

            return result;
        
        } // GetConfiguration()

        public Dictionary<string, string> GetStatusMessageMasks()
        {
            var obj = (from o in _data.ObjectViews
                       where o.INSTALLATION == _installation
                       select o).ToList();

            var circuitMsk = "";
            var pumpMsk = "";
            var ichlorMsk = "";
            var ichemMsk = "";
            var heaterMsk = "";

            foreach (var c in obj)
            {
                switch (c.OBJTYP)
                {
                    case StaticData.PumpType:
                        pumpMsk = c.MASKSM ?? pumpMsk;
                        break;
                    case StaticData.ChemistryType:
                        if (c.SUBTYP == "ICHEM")
                        {
                            ichemMsk = c.MASKSM ?? ichemMsk;
                        }
                        else
                        {
                            ichlorMsk = c.MASKSM ?? ichlorMsk;
                        }
                        break;
                    case StaticData.CircuitType:
                        if (c.SUBTYP == "FRZ")
                        {
                            circuitMsk = c.MASKSM ?? circuitMsk;
                        }
                        break;
                    case StaticData.HeaterType:
                        heaterMsk = c.MASKSM ?? heaterMsk;
                        break;
                }
            }

            return new Dictionary<string, string>()
            {
                {StaticData.CircuitType, circuitMsk},
                {StaticData.PumpType, pumpMsk},
                {"ICHLOR", ichlorMsk},
                {"ICHEM", ichemMsk},
                {StaticData.HeaterType, heaterMsk}
            };
        }

        public WriteStatusMessage GetWarningsAndAlerts(
            string messageID,
            string timeSince)
        {
            WriteStatusMessage response = null;

            var changes = false;
            
            var result = new ExtendedParamObject(
                deleted: new List<string>(),
                created: new List<string>(),
                changes: new List<ParamObject>()
            );

            var alerts = _data.ObjectViews.Where(
                x => x.INSTALLATION == _installation &&
                     x.OBJTYP == StaticData.StatusMessageType).ToList();

            foreach (var o in alerts)
            {

                if (o.Deleted)
                {
                    result.Deleted.Add(o.OBJNAM);
                }
                else
                {
                    var objLastUpdate = _data.ControlObjects.FirstOrDefault(x => x.INSTALLATION == _installation &&
                             x.OBJNAM == o.OBJNAM &&
                             x.Key == StaticData.ShowMenu);

                    if (objLastUpdate != null && objLastUpdate.Revision > UInt32.Parse(timeSince))
                    {
                        var obj = (from c in _data.ControlObjects
                                   where c.INSTALLATION == _installation &&
                                         c.OBJNAM == o.OBJNAM &&
                                         StaticData.StatusMessageParms.Contains(c.Key)
                                   select c).ToDictionary(t => t.Key, t => t.Value as object);

                        result.Changes.Add(
                            new ParamObject(objNam: o.OBJNAM, parameters: obj)
                        );
                    }
                }

            } // foreach (var o in alerts)

            changes = result.Changes.Count > 0 || result.Deleted.Count > 0;
            if (changes)
            {
                var property = _data.Properties.FirstOrDefault(x => x.LocalPropertyId == _installation);
                UInt32 lastTime = 0;
                UInt32 currentTime = Globals.CurrentTime();

                response = new WriteStatusMessage(
                     messageID,
                     ResponseString.OK,
                     currentTime.ToString(),
                     lastTime.ToString(),
                     new List<ExtendedParamObject>() { result } // return an empty list if there is nothing to report
                 );
            }
            else
            {
                response = new WriteStatusMessage(
                     messageID,
                     ResponseString.InternalServerError,
                     String.Empty,
                     String.Empty,
                     null
                 );
            }

            return response;

        } // GetWarningsAndAlerts()

        /// <summary>
        /// Create a new object
        /// </summary>
        /// <param name="item">KeyObject containing the object type and constructor parameters</param>
        /// <returns>New object name (empty string for failures).</returns>
        public string CreateObject(string item)
        {
            var result = "";
            try
            {
                var uids = _data.ControlObjects
                    .Where(x => x.Key == "OBJTYP" &&
                                x.Value == item &&
                                x.INSTALLATION == _installation)
                    .OrderBy(x => x.OBJNAM)
                    .Select(x => x.OBJNAM).ToList();
                var nextItem = NextObjectItem(uids);
                if (0 < nextItem)
                {
                    switch (item)
                    {
                        case "SCHED":
                            result = string.Format("{0}{1}", "S", nextItem.ToString("0000"));
                            break;

                        case StaticData.CircuitType:
                            result = string.Format("{0}{1}", "C", nextItem.ToString("0000"));
                            break;
                        case StaticData.CircuitGroupType:
                            result = string.Format("{0}{1}", "c", nextItem.ToString("0000"));
                            break;
                        case StaticData.StatusMessageType:
                            result = string.Format("{0}{1}", "t", nextItem.ToString("0000"));
                            break;
                    }
                }

                if (!string.IsNullOrEmpty(result))
                {
                    CreateControlObject(result, StaticData.ObjectType, item, Globals.CurrentTime());
                }
            }
            catch
            {
                result = "";
            }
            return result;
        }

        /// <summary>
        /// Update the system clock
        /// </summary>
        /// <param name="value"></param>
        public void UpdateSystemClock(DateTime value)
        {
            var om = new ObjectManager(_data, _installation);

            var revision = Globals.CurrentTime();
            var day = om.ParamValue(StaticData.Location, StaticData.DateParam);
            var time = om.ParamValue(StaticData.Location, StaticData.ClockParam);

            DateTime val;
            if (null == day || null == time)
            {
                val = DateTime.Now;
            }
            else
            {
                val = DateTime.MinValue == value ? DateTime.Parse(day.Replace(',', '/') + " " + time.Replace(',', ':')).AddMinutes(1) : value;
            }

            var d = new ControlObject()
            {
                OBJNAM = StaticData.Location,
                INSTALLATION = _installation,
                Key = StaticData.DateParam,
                Value = val.ToShortDateString().Replace('/', ','),
                Revision = revision
            };
            om.UpdateParameter(d);
            _changes.Add(d);
            var t = new ControlObject()
            {
                OBJNAM = StaticData.Location,
                INSTALLATION = _installation,
                Key = StaticData.ClockParam,
                Value = val.ToString("HH:mm:ss").Replace(':', ','),
                Revision = revision
            };
            om.UpdateParameter(t);
            _changes.Add(t);
            var m = new ControlObject()
            {
                OBJNAM = StaticData.Location,
                INSTALLATION = _installation,
                Key = StaticData.MinuteParam,
                Value = val.ToString("HH:mm").Replace(':', ','),
                Revision = revision
            };
            om.UpdateParameter(m);
            _changes.Add(m);

            var bodies = _data.ObjectViews
                .Where(x => x.OBJTYP == StaticData.BodyType && x.INSTALLATION == _installation)
                .ToList();
            foreach (var body in bodies)
            {
                var filter = om.ParamValue(body.OBJNAM, StaticData.Filter);
                if (null != filter)
                {
                    var ctl = om.ParamValue(filter, StaticData.Timeout);
                    if (!String.IsNullOrEmpty(ctl))
                    {
                        int delay;
                        if (int.TryParse(ctl, out delay))
                        {
                            delay -= 60;
                            if (delay <= 0)
                            {
                                ClearDelay(om, filter, body.OBJNAM, revision);
                            }
                            else
                            {
                                var timeout = new ControlObject()
                                {
                                    OBJNAM = filter,
                                    INSTALLATION = _installation,
                                    Key = StaticData.Timeout,
                                    Value = delay.ToString(),
                                    Revision = revision
                                };
                                om.UpdateParameter(timeout);
                                _changes.Add(timeout);
                            }
                        }
                        else
                        {
                            ClearDelay(om, filter, body.OBJNAM, revision);
                        }
                    }
                    else
                    {
                        ClearDelay(om, filter, body.OBJNAM, revision);
                    }
                }
            }

        } // UpdateSystemClock()

        /// <summary>
        /// SetParameterList sets the parameters given by the requestor.
        /// </summary>
        /// <param name="request">Parameters to change</param>
        public void SetParamList(SetParamListResponse request)
        {
            try
            {
                var om = new ObjectManager(_data, _installation);
                {
                    foreach (var item in request.ObjectList)
                    {
                        var revision = Globals.CurrentTime();

                        // Handle the All Lights command
                        // This logic presumes that only Status can be changed when receiving an AllLights command
                        if (item.ObjNam == StaticData.AllLights)
                        {
                            var objGrp = om.GetObjectsByType(StaticData.CircuitType);
                            foreach (var child in objGrp)
                            {
                                var s = om.ParamValue(child.ObjNam, StaticData.SubType);
                                if (s == StaticData.LightSubtype ||
                                    s == StaticData.Magic1Subtype ||
                                    s == StaticData.Magic2Subtype ||
                                    s == StaticData.IntellibrightSubtype ||
                                    s == StaticData.SwimLaneType ||
                                    s == StaticData.ColorWheelType ||
                                    s == StaticData.GlowType ||
                                    s == StaticData.GlowWhiteType ||
                                    s == StaticData.DimmerSubtype ||
                                    child.ObjNam == StaticData.AllLights)
                                {
                                    foreach (var param in item.Parameters)
                                    {
                                        if (!forbiddenUpdate(param))
                                        {

                                            var o = new ControlObject()
                                            {
                                                INSTALLATION = _installation,
                                                OBJNAM = child.ObjNam,
                                                Key = param.Key,
                                                Value = param.Value.ToString(),
                                                Revision = revision
                                            };
                                            om.UpdateParameter(o);
                                            _changes.Add(o);
                                        }
                                    }
                                }
                            }
                        }

                        // Handle generic circuit groups
                        // This logic presumes that only Status can be changed on a circuit group
                        else if (om.ParamValue(item.ObjNam, StaticData.SubType) == "CIRCGRP" ||
                                 om.ParamValue(item.ObjNam, StaticData.SubType) == "LITSHO")
                        {
                            var objGrp = om.GetChildren(item.ObjNam);

                            foreach (var child in objGrp)
                            {
                                foreach (var param in item.Parameters)
                                {
                                    if (!forbiddenUpdate(param))
                                    {
                                        var o = new ControlObject()
                                        {
                                            INSTALLATION = _installation,
                                            OBJNAM = om.ParamValue(child.ObjNam, StaticData.CircuitType),
                                            Key = param.Key,
                                            Value = param.Value.ToString(),
                                            Revision = revision
                                        };
                                        om.UpdateParameter(o);
                                        _changes.Add(o);
                                    }
                                }
                            }
                            foreach (var param in item.Parameters)
                            {
                                if (!forbiddenUpdate(param))
                                {
                                    var o = new ControlObject()
                                        {
                                            INSTALLATION = _installation,
                                            OBJNAM = item.ObjNam,
                                            Key = param.Key,
                                            Value = param.Value.ToString(),
                                            Revision = revision
                                        };
                                    om.UpdateParameter(o);
                                    _changes.Add(o);
                                }
                            }
                        }

                        else
                        {
                            foreach (var param in item.Parameters)
                            {
                                if (!forbiddenUpdate(param))
                                {
                                    updateSuperChlorTimeOut(om, item, revision, param);
                                    var parmValue = validateTemperature(om, item, param);

                                    var o = new ControlObject()
                                    {
                                        INSTALLATION = _installation,
                                        OBJNAM = item.ObjNam,
                                        Key = param.Key,
                                        Value = parmValue.ToString(),
                                        Revision = revision
                                    };
                                    if (param.Key == StaticData.Status && param.Value.ToString() == "DSTROY")
                                    {
                                        o.Deleted = true;
                                        DeleteObject(o);
                                    }
                                    om.UpdateParameter(o);
                                    _changes.Add(o);
                                }
                            }
                        }
                    }
                }
                _data.SaveChanges();
            }
            catch (Exception ex)
            {
                // VB: What the hack is that?! Exception handling a la Pathfinder? ;)
                throw new Exception(ex.Message);
            }
        
        } // SetParamListResponse()

        private object validateTemperature(ObjectManager om, ParamObject item, KeyValuePair<string, object> param)
        {
            var units = _data.ObjectViews.Where(x => x.INSTALLATION == _installation && x.OBJNAM == StaticData.Preferences).FirstOrDefault();
            var max = (units != null && units.MODE == "METRIC") ? 40 : 104;
            var min = (units != null && units.MODE == "METRIC") ? 5 : 55;

            if (param.Key == StaticData.HiTemp && param.Value != null)
            {
                var lowTmp = om.ParamValue(item.ObjNam, StaticData.LowTemp);
                if (null == lowTmp)
                {
                    return param.Value;
                }
                int lowTempValue;
                if (int.TryParse(lowTmp.ToString(), out lowTempValue))
                {
                    int highTempValue;
                    if (int.TryParse(param.Value.ToString(), out highTempValue))
                    {
                        if (highTempValue > max)
                        {
                            return max.ToString();
                        }
                        if (highTempValue < min)
                        {
                            return min.ToString();
                        }
                        if (lowTempValue - 4 < highTempValue)
                        {
                            if (lowTempValue - 4 >= min)
                            {
                                return (lowTempValue - 4).ToString();
                            }
                            else
                            {
                                return min.ToString();
                            }
                        }
                    }
                }
            }

            if (param.Key == StaticData.LowTemp && param.Value != null)
            {
                var highTmp = om.ParamValue(item.ObjNam, StaticData.HiTemp);
                if (null == highTmp)
                {
                    return param.Value;
                }
                int highTempValue;
                if (int.TryParse(highTmp.ToString(), out highTempValue))
                {
                    int lowTempValue;
                    if (int.TryParse(param.Value.ToString(), out lowTempValue))
                    {
                        if (lowTempValue > max)
                        {
                            return max.ToString();
                        }
                        if (lowTempValue < min)
                        {
                            return min.ToString();
                        }
                        if (lowTempValue - 4 < highTempValue)
                        {
                            if (highTempValue + 4 < max)
                            {
                                return (highTempValue + 4).ToString();
                            }
                            else
                            {
                                return max.ToString();
                            }
                        }
                    }
                }
            }
            return param.Value;
        }

        private void updateSuperChlorTimeOut(ObjectManager om, ParamObject item, uint revision, KeyValuePair<string, object> param)
        {
            if (om.ParamValue(item.ObjNam, StaticData.SubType) == "ICHLOR")
            {
                if (param.Key == "SUPER")
                {
                    if ("ON" == param.Value.ToString())
                    {

                    }
                    else if ("OFF" == param.Value.ToString())
                    {
                        var ChemTimeOut = new ControlObject()
                        {
                            INSTALLATION = _installation,
                            OBJNAM = item.ObjNam,
                            Key = "TIMOUT",
                            Value = StaticData.DefaultSuperChlorTimeout,
                            Revision = revision
                        };
                        om.UpdateParameter(ChemTimeOut);
                        _changes.Add(ChemTimeOut);
                    }
                }
            }
        }

        private bool forbiddenUpdate(KeyValuePair<string, object> param)
        {
            return isLightAction(ref param);
        }

        private static bool isLightAction(ref KeyValuePair<string, object> param)
        {
            string[] ignoreValues = { "ROTATE", "STOP", "SYNC" };
            if (param.Key == "ACT")
            {
                return ignoreValues.Contains(param.Value.ToString().ToUpper());
            }

            return false;
        }

        public WriteParamListResponse GetConfigChanges()
        {
            WriteParamListResponse result = null;

            var om = new ObjectManager(_data, _installation);

            var property = _data.Properties.FirstOrDefault(x => x.LocalPropertyId == _installation);

            if (property != null)
            {
                var lastChange = property.LastChange;
                property.LastChange = Globals.CurrentTime();
                _data.SaveChanges();
                var c = (from i in _changes
                         join b in _data.ControlObjects on i.OBJNAM equals b.OBJNAM
                         where b.Key == StaticData.ObjectType &&
                                 b.INSTALLATION == _installation
                         join p in _data.ObjectParameterTypes on b.Value equals p.OBJTYP
                         where p.PARAM == i.Key &&
                                 p.Saved == true
                         select i).ToList();

                result = new WriteParamListResponse(
                    messageID: String.Empty,
                    response: ResponseString.OK,
                    timeNow: Convert.ToString(property.LastChange),
                    timeSince: Convert.ToString(lastChange),
                    objectList: new List<ExtendedParamObject>()
                    {
                        new ExtendedParamObject(
                            changes: om.BuildMessage(c).ToList()
                        )
                    }
                );
            }
            else
            {
                result = new WriteParamListResponse(
                    messageID: String.Empty,
                    response: String.Empty,
                    timeNow: String.Empty,
                    timeSince: String.Empty,
                    objectList: new List<ExtendedParamObject>()
                );
            }

            return result;

        } // GetConfigChanges()

        /// <summary>
        /// Retrieve the list of all configuration changes.
        /// </summary>
        /// <returns></returns>
        public WriteParamListResponse GetConfigChanges(string lastUpdate)
        {
            WriteParamListResponse result = null;

            var om = new ObjectManager(_data, _installation);

            var property = _data.Properties.FirstOrDefault(x => x.LocalPropertyId == _installation);
            if (property != null)
            {
                property.LastChange = Globals.CurrentTime();
                _data.SaveChanges();
                var o = om.GetChangesSince(Convert.ToUInt32(lastUpdate));
                var c = (from i in o
                         join b in _data.ControlObjects on i.OBJNAM equals b.OBJNAM
                         where b.Key == StaticData.ObjectType &&
                               b.INSTALLATION == _installation
                         join p in _data.ObjectParameterTypes on b.Value equals p.OBJTYP
                         where p.PARAM == i.Key &&
                               p.Saved == true
                         select i).ToList();

                var changes = new ExtendedParamObject(changes: om.BuildMessage(c).ToList());

                result = new WriteParamListResponse(
                    messageID: String.Empty,
                    response: ResponseString.OK,
                    timeNow: property.LastChange.ToString(),
                    timeSince: lastUpdate,
                    objectList: new List<ExtendedParamObject>() { changes }
                );
            }
            else
            {
                result = new WriteParamListResponse(
                    messageID: String.Empty,
                    response: String.Empty,
                    timeNow: String.Empty,
                    timeSince: String.Empty,
                    objectList: new List<ExtendedParamObject>()
                );
            }

            return result;

        } // GetConfigChanges()

        public WriteStatusMessage GetWarningAndAlertChanges(string timeSince)
        {
            var property = _data.Properties.FirstOrDefault(x => x.LocalPropertyId == _installation);

            return GetWarningsAndAlerts(String.Empty, timeSince);
        }
        
        public WriteHistory GetHistory(long timeSince)
        {
            WriteHistory message = null;

            var history = new Dictionary<string, List<HistoryItem>>();
            var time = Globals.CurrentTime();
            var property = _data.Properties.FirstOrDefault(x => x.LocalPropertyId == _installation);

            if (null != property)
            {
                var lastChange = property.LastHistory;
                var timeNow = Globals.CurrentTime();
                property.LastHistory = timeNow;
                _data.SaveChanges();

                for (int i = 0; i < 10; i++)
                {
                    var name = "C100" + i.ToString();
                    var item = new List<HistoryItem>();
                    for (int j = 3; j >= 0; j--)
                    {
                        var changeTime = time - (j*10);
                        item.Add(new HistoryItem("STATUS", j%2 == 1 ? "ON" : "OFF", changeTime.ToString()));
                    }
                    history.Add(name, item);
                }
                var array = new List<Dictionary<string, List<HistoryItem>>>();
                array.Add(history);

                message = new WriteHistory(
                    messageID: String.Empty,
                    response: ResponseString.OK,
                    timeNow: timeNow.ToString(),
                    timeSince: lastChange.ToString(),
                    objectList: array
                );
            }
            else
            {
                message = new WriteHistory(
                    messageID: String.Empty,
                    response: ResponseString.NotFound,
                    timeNow: String.Empty,
                    timeSince: String.Empty,
                    objectList: new List<Dictionary<string, List<HistoryItem>>>()
                );
            }

            return message;
        }

        /// <summary>
        /// Retrieve the list of notification changes.
        /// </summary>
        /// <returns></returns>
        public NotifyList GetNotifications()
        {
            NotifyList result = null;

            var om = new ObjectManager(_data, _installation);

            var property = _data.Properties.FirstOrDefault(x => x.LocalPropertyId == _installation);
            if (null != property)
            {
                var lastChange = property.LastNotify;
                property.LastNotify = Globals.CurrentTime();
                _data.SaveChanges();
                var n = (from b in _changes
                         join s in _data.Subscriptions on new { X1 = b.OBJNAM, X2 = b.Key } equals
                             new { X1 = s.ObjNam, X2 = s.ObjParm }
                         select new ControlObject()
                         {
                             OBJNAM = b.OBJNAM,
                             INSTALLATION = b.INSTALLATION,
                             Key = b.Key,
                             Value = b.Value
                         }).ToList();
                var c = (from i in n
                         join b in _data.ControlObjects on i.OBJNAM equals b.OBJNAM
                         where b.Key == StaticData.ObjectType &&
                               b.INSTALLATION == _installation
                         join p in _data.ObjectParameterTypes on b.Value equals p.OBJTYP
                         where p.PARAM == i.Key
                         select i).ToList();

                result = new NotifyList(
                    messageID: String.Empty,
                    response: ResponseString.OK,
                    timeNow: Convert.ToString(property.LastChange),
                    timeSince: Convert.ToString(lastChange),
                    objectList: om.BuildMessage(c).ToList()
                );
            }
            else
            {
                result = new NotifyList(
                    messageID: String.Empty,
                    response: ResponseString.BadRequest,
                    timeNow: String.Empty,
                    timeSince: String.Empty,
                    objectList: null
                );
            }

            return result;

        } // GetNotifications()

        //<summary>
        //Create subscriptions
        //</summary>
        //<param name="request">List of itmems being subscribed to</param>
        //<param name="client">Client subscribimg</param>
        //<param name="installation">Installation being subscribed to</param>
        public NotifyList RequestParamList(RequestParamList request, Guid client, int installation)
        {
            NotifyList result = null;

            var om = new ObjectManager(_data, _installation);

            var property = _data.Properties.FirstOrDefault(x => x.LocalPropertyId == _installation);
            if (null != property)
            {
                var lastChange = property.LastNotify;
                property.LastNotify = Globals.CurrentTime();
                _data.SaveChanges();
                // Iterate the objects
                var obj = new List<ControlObject>();
                foreach (var item in request.ObjectList)
                {
                    var c = (from o in _data.ControlObjects
                             where o.INSTALLATION == _installation &&
                                   o.OBJNAM == item.ObjNam
                             select o).ToList();
                    obj.AddRange(c.Where(o => item.Keys.Contains(o.Key)));

                    var r = item.Keys.Where(parm => !_data.Subscriptions.Any(
                        s => s.ClientId == client &&
                             s.ObjNam == item.ObjNam &&
                             s.ObjParm == parm));

                    // Iterate the parameters
                    foreach (var parm in r)
                    {
                        // Create a subscription
                        _data.Subscriptions.Add(new Subscription()
                        {
                            ClientId = client,
                            PropertyId = installation,
                            ObjNam = item.ObjNam,
                            ObjParm = parm
                        });
                    }
                }

                result = new NotifyList(
                    messageID: String.Empty,
                    response: ResponseString.OK,
                    timeNow: Convert.ToString(property.LastChange),
                    timeSince: Convert.ToString(lastChange),
                    objectList: om.BuildMessage(obj).ToList()
                );
            }
            else
            {
                result = new NotifyList(
                    messageID: String.Empty,
                    response: ResponseString.BadRequest,
                    timeNow: String.Empty,
                    timeSince: String.Empty,
                    objectList: null
                );
            }

            _data.SaveChanges();

            return result;

        } // RequestParamList()

        /// <summary>
        /// Clear all subscriptions for this client
        /// </summary>
        /// <param name="client">Client Id</param>
        /// <param name="installation">Installation Id</param>
        public void ClearParamList(Guid client, int installation)
        {
            var entries = (from c in _data.Subscriptions
                           where c.ClientId == client
                           select c).ToList();
            foreach (var item in entries)
            {
                _data.Subscriptions.Remove(item);
            }
            _data.SaveChanges();
        }

        public void DeleteObject(ControlObject o)
        {
            var om = new ObjectManager(_data, o.INSTALLATION);
            om.MarkDeletedObject(o.OBJNAM);
        }

        #endregion

        #region Private Methods

        private string LightGroupCapabilities(List<string> circuits)
        {
            var group = _data.ObjectViews.Where(x => circuits.Contains(x.OBJNAM) && x.INSTALLATION == _installation).ToList();

            var capabilities = StaticData.AllLightCapabilities;

            foreach (var circuit in group)
            {
                switch (circuit.SUBTYP.ToUpper())
                {
                    case "MAGIC1":
                        if (capabilities.Length > StaticData.Magic1Capabilities.Length)
                        {
                            capabilities = StaticData.Magic1Capabilities;
                        }
                        break;
                    case "MAGIC2":
                        if (capabilities.Length > StaticData.Magic2Capabilities.Length)
                        {
                            capabilities = StaticData.Magic2Capabilities;
                        }
                        break;
                    case "INTELLI":
                        if (capabilities.Length > StaticData.Intelli.Length)
                        {
                            capabilities = StaticData.Intelli;
                        }
                        break;
                    case "SAML":
                        if (capabilities.Length > StaticData.SamSalCapabilities.Length)
                        {
                            capabilities = StaticData.SamSalCapabilities;
                        }
                        break;
                    case "COLORW":
                        if (capabilities.Length > StaticData.ColorWheelCapabilities.Length)
                        {
                            capabilities = StaticData.ColorWheelCapabilities;
                        }
                        break;
                    case "PHOTON":
                        if (capabilities.Length > StaticData.PhotonCapabilities.Length)
                        {
                            capabilities = StaticData.PhotonCapabilities;
                        }
                        break;
                    case "DIMMER":
                        if (capabilities.Length > StaticData.DimmerCapabilities.Length)
                        {
                            capabilities = StaticData.DimmerCapabilities;
                        }
                        break;
                    case "LIGHT":
                        if (capabilities.Length > StaticData.LightCapabilities.Length)
                        {
                            capabilities = StaticData.LightCapabilities;
                        }
                        break;
                    case "GLOW":
                        if (capabilities.Length > StaticData.GlobecCapabilities.Length)
                        {
                            capabilities = StaticData.GlobecCapabilities;
                        }
                        break;
                    case "GLOWT":
                        if (capabilities.Length > StaticData.GlobewCapabilities.Length)
                        {
                            capabilities = StaticData.GlobewCapabilities;
                        }
                        break;
                    default:
                        if (capabilities.Length > StaticData.LightCapabilities.Length)
                        {
                            capabilities = StaticData.LightCapabilities;
                        }
                        break;
                }
            }

            return capabilities;
        }

        private void ClearDelay(ObjectManager om, string filter, string body, long revision)
        {
            // Complete the operation
            var source = om.ParamValue(filter, StaticData.Source);
            var state = om.ParamValue(filter, StaticData.Status);
            var bstate = om.ParamValue(body, StaticData.Status);
            if (state == "DLYON" || state == "DLYOFF" || bstate == "DLYON" || bstate == "DLYOFF")
            {
                setBodyState(
                    om,
                    body,
                    state == "DLYON" ? "ON" : "OFF",
                    "",
                    "",
                    revision
                    );
            }
            var dly = om.ParamValue(filter, StaticData.Timeout);
            if (null != dly && string.IsNullOrEmpty(dly))
            {
                return;
            }
            var timeout = new ControlObject()
            {
                OBJNAM = filter,
                INSTALLATION = _installation,
                Key = StaticData.Timeout,
                Value = "",
                Revision = revision
            };
            om.UpdateParameter(timeout);
            _changes.Add(timeout);
            var statMsg = _data.ControlObjects.Where(
                x => x.Key == StaticData.Parent &&
                     x.Value == source &&
                     x.INSTALLATION == _installation &&
                     !x.Deleted).ToList();
            foreach (var s in statMsg)
            {
                var m = _data.ControlObjects.FirstOrDefault(
                    x => x.OBJNAM == s.OBJNAM &&
                         x.INSTALLATION == _installation &&
                         x.Key == StaticData.ObjectType);
                m.Deleted = true;
                m.Revision = revision;
            }
            _data.SaveChanges();
        }

        private bool toggleBodyState(string oName, string newState)
        {
            bool result = false;
            var body = (from o in _data.ObjectViews
                        where o.INSTALLATION == _installation &&
                              o.OBJNAM == oName
                        select o).FirstOrDefault();
            if (null != body)
            {
                var revision = Globals.CurrentTime();

                var om = new ObjectManager(_data, _installation);

                var state = om.ParamValue(body.OBJNAM, StaticData.Status);
                    if ((newState == "ON" && (state == "ON" || state == "DLYON")) ||
                        (newState == "OFF" && (state == "OFF" || state == "DLYOFF")))
                    {
                        return true;
                    }
                    var share = om.ParamValue(body.OBJNAM, StaticData.Share);
                    var heater = om.ParamValue(body.OBJNAM, StaticData.HeaterType);
                    var valve = "V1000"; //om.ParamValue(body.OBJNAM, StaticData.ValveType);
                    // Set a heater delay if we are turning the body off.
                    if (null != heater && newState == "OFF")
                    {
                        var timeOut = om.ParamValue(heater, StaticData.Delay);
                        setBodyState(
                            om,
                            body.OBJNAM,
                            "DLYOFF",
                            timeOut,
                            heater,
                            revision);
                        var a = CreateObject(StaticData.StatusMessageType);
                        var alert =
                            _data.ControlObjects.FirstOrDefault(x => x.OBJNAM == a && x.INSTALLATION == _installation);
                        SetAlertOrStatus(alert, heater, "", "3", "Cool Down: " + timeOut);
                    }
                    else if (null != share &&
                             om.ParamValue(share, StaticData.Status) == "ON" &&
                             newState == "ON")
                    {
                        var timeOut = om.ParamValue(valve, StaticData.Delay);
                        setBodyState(
                            om,
                            body.OBJNAM,
                            "DLYON",
                            timeOut,
                            valve,
                            revision);
                        setBodyState(
                            om,
                            share,
                            "OFF",
                            timeOut,
                            valve,
                            revision);
                        var a = CreateObject(StaticData.StatusMessageType);
                        var alert =
                            _data.ControlObjects.FirstOrDefault(x => x.OBJNAM == a && x.INSTALLATION == _installation);
                        SetAlertOrStatus(alert, valve, "", "3", "Valve Delay: " + timeOut);
                    }
                    else
                    {
                        setBodyState(
                            om,
                            body.OBJNAM,
                            newState,
                            null,
                            valve,
                            revision);
                    }

                    result = true;
            }

            return result;
        
        } // toggleBodyState()

        private void setBodyState(ObjectManager om, string body, string state, string delay, string source,
            long revision)
        {
            var filter = om.ParamValue(body, StaticData.Filter);
            var filterDelay = new ControlObject
            {
                OBJNAM = filter,
                INSTALLATION = _installation,
                Key = StaticData.Timeout,
                Value = delay,
                Revision = revision
            };
            om.UpdateParameter(filterDelay);
            _changes.Add(filterDelay);
            var delaySource = new ControlObject
            {
                OBJNAM = filter,
                INSTALLATION = _installation,
                Key = StaticData.Source,
                Value = source,
                Revision = revision
            };
            om.UpdateParameter(delaySource);
            _changes.Add(delaySource);
            var filterStatus = new ControlObject
            {
                OBJNAM = filter,
                INSTALLATION = _installation,
                Key = StaticData.Status,
                Value = state,
                Revision = revision
            };
            om.UpdateParameter(filterStatus);
            _changes.Add(filterStatus);
            var bodyStatus = new ControlObject
            {
                OBJNAM = body,
                INSTALLATION = _installation,
                Key = StaticData.Status,
                Value = state,
                Revision = revision
            };
            om.UpdateParameter(bodyStatus);
            _changes.Add(bodyStatus);

            updateLastTMP(om, body, state, revision);
        }

        private void updateLastTMP(ObjectManager om, string body, string state, long revision)
        {
            if (state == "OFF")
            {
                var bodyTemp = new ControlObject
                {
                    OBJNAM = body,
                    INSTALLATION = _installation,
                    Key = "LSTTMP",
                    Value = om.ParamValue(body, StaticData.Temperature),
                    Revision = revision
                };
                om.UpdateParameter(bodyTemp);
                _changes.Add(bodyTemp);
            }
        }

        private void UpdateStatusMask(ObjectView o, string mask)
        {
            var obj = (from c in _data.ControlObjects
                       where c.INSTALLATION == _installation &&
                             c.OBJNAM == o.OBJNAM &&
                             c.Key == "MASKSM"
                       select c).FirstOrDefault();
            if (null != obj)
            {
                obj.Value = mask;
            }
            else
            {
                _data.ControlObjects.Add(new ControlObject()
                {
                    OBJNAM = o.OBJNAM,
                    Deleted = false,
                    INSTALLATION = _installation,
                    Key = "MASKSM",
                    Value = mask,
                    Revision = Globals.CurrentTime()
                });
            }
            _data.SaveChanges();
        }

        private int NextObjectItem(List<string> names)
        {
            int nextValue = 1000;
            foreach (var n in names)
            {
                int value;
                if (int.TryParse(n.Substring(1, n.Length - 1), out value))
                {
                    if (value >= nextValue)
                    {
                        nextValue = value + 1;
                    }
                }
            }
            return nextValue;
        }

        void SetAlertOrStatus(ControlObject obj, string parent, string sindex, string mode, string sname)
        {
            _data.ControlObjects.Add(new ControlObject
            {
                OBJNAM = obj.OBJNAM,
                Revision = obj.Revision,
                INSTALLATION = _installation,
                Key = StaticData.Parent,
                Value = parent
            });
            _data.ControlObjects.Add(new ControlObject
            {
                OBJNAM = obj.OBJNAM,
                Revision = obj.Revision,
                INSTALLATION = _installation,
                Key = StaticData.SIndex,
                Value = sindex
            });
            _data.ControlObjects.Add(new ControlObject
            {
                OBJNAM = obj.OBJNAM,
                Revision = obj.Revision,
                INSTALLATION = _installation,
                Key = StaticData.SystemName,
                Value = sname
            });
            _data.ControlObjects.Add(new ControlObject
            {
                OBJNAM = obj.OBJNAM,
                Revision = obj.Revision,
                INSTALLATION = _installation,
                Key = StaticData.Mode,
                Value = mode
            });
            _data.ControlObjects.Add(new ControlObject
            {
                OBJNAM = obj.OBJNAM,
                Revision = obj.Revision,
                INSTALLATION = _installation,
                Key = StaticData.ShowMenu,
                Value = "OFF"
            });
            _data.SaveChanges();
        }

        List<ParamObject> getGroupObjects(string parent, IEnumerable<string> circuits, long revision)
        {
            var obj = new List<ParamObject>();

            foreach (var circuit in circuits)
            {
                obj.Add(createGroupObject(parent, circuit, revision));
            }
            return obj;
        }

        ParamObject createGroupObject(string parent, string circuit, long revision)
        {
            var objNam = CreateObject(StaticData.CircuitGroupType);
            
            CreateControlObject(objNam, StaticData.CircuitType, circuit, revision);
            CreateControlObject(objNam, "ACT", "", revision);
            CreateControlObject(objNam, "DLY", "0", revision);
            CreateControlObject(objNam, "LISTORD", "0", revision);
            CreateControlObject(objNam, "COLOR", "", revision);
            CreateControlObject(objNam, "START", "1", revision);
            CreateControlObject(objNam, "PARENT", parent, revision);

            return new ParamObject(
                objNam: objNam,
                parameters: new Dictionary<string, object>()
                {
                    { StaticData.ObjectType, StaticData.CircuitGroupType },
                    { StaticData.CircuitType, circuit },
                    { "ACT", "" },
                    { "DLY", "0" },
                    { "LISTORD", "0" },
                    { "COLOR", "" },
                    { "START", "1" }
                }
            );

        } // createGroupObject()

        private void CreateControlObject(string objName, string Key, string Value, long revision)
        {
            var o = new ControlObject()
            {
                INSTALLATION = _installation,
                OBJNAM = objName,
                Revision = revision,
                Key = Key,
                Value = Value
            };

            _data.ControlObjects.Add(o);
            _changes.Add(o);
            _data.SaveChanges();
        }

        //private void CancelDelay(ControlObject circuit)
        //{
        //    using (var om = new ObjectManager(_data, _installation))
        //    {
        //         Get the delaying circuit
        //        var delayCircuit = (from d in _data.ControlObjects
        //                            where d.OBJNAM == circuit.SOURCE
        //                            select d).FirstOrDefault();

        //         turn off the delay circuit
        //        if (null != delayCircuit)
        //        {
        //            AddChange(delayCircuit, "STATUS");
        //            delayCircuit.STATUS = "OFF";
        //        }
        //         Cancel the delay
        //        AddChange(circuit, "SOURCE");
        //        circuit.SOURCE = "";
        //        AddChange(circuit, "TIMOUT");
        //        circuit.TIMOUT = "";
        //        AddChange(circuit, "STATUS");
        //        circuit.STATUS = circuit.STATUS == "DLYON" ? "ON" : "OFF";
        //    }
        //}

        #endregion
    }
}
