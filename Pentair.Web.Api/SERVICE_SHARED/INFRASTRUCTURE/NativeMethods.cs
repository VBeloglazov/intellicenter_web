/*
--------------------------------------------------------------------------------

    NativeMethods.cs

    Copyright � Pentair Water Quality Systems, Inc. 2015

--------------------------------------------------------------------------------
*/

using System.Runtime.InteropServices;

namespace SERVICE_SHARED
{
    internal static class NativeMethods
    {
        [DllImport("Kernel32.dll")]
        public static extern bool QueryPerformanceFrequency(out long frequency);

        [DllImport("Kernel32.dll")]
        public static extern void QueryPerformanceCounter(out long ticks);

    } // class NativeMethods
}

