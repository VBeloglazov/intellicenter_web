﻿using System.Collections.Generic;

namespace Pentair.Domain.Client.Helpers
{
    public static class StaticData
    {
        public const string  PentairPhoneContact = "800-UNKNOWN";

        // Well known OBJNAM definitions
        public const string SuperAdmin = "UFFFF";
        public const string Admin = "UFFFE";
        public const string Guest = "U0000";
        public const string AllLights = "C0A11";
        public const string Location = "_C10C";
        public const string Clock = "_C105";
        public const string Preferences = "_5451";
        public const string SensorSingleton = "_A135";
        public const string FeatureMenuSingleton ="_CFEA";

        // OBJTYP definitions
        public const string CircuitType = "CIRCUIT";
        public const string CircuitGroupType = "CIRCGRP";
        public const string LightShowType = "LITSHO";
        public const string BodyType = "BODY";
        public const string HeaterType = "HEATER";
        public const string HeatComboType = "HCOMBO";
        public const string PumpType = "PUMP";
        public const string PumpAffectsType = "PMPCIRC";
        public const string ValveType = "VALVE";
        public const string ValveAffectsType = "VLVCIRC";
        public const string SensorType = "SENSE";
        public const string PanelType = "PANEL";
        public const string ModuleType = "MODULE";
        public const string RelayType = "RLY";
        public const string ChemistryType = "CHEM";
        public const string ScheduleType = "SCHED";
        public const string SystemType = "SYSTEM";
        public const string NetworkType = "NETWRK";
        public const string SystemTimeType = "SYSTIM";
        public const string PermitType = "PERMIT";
        public const string RemoteType = "REMOTE";
        public const string RemoteButtonType = "REMCIRC";
        public const string SupportType = "SUPORT";
        public const string EmailAlertType = "EMAIL";
        public const string StatusMessageMasterType = "PRESS";
        public const string StatusMessageType = "STAMSG";
        public const string SensorSingletonType = "STATUS";
        public const string MenuType = "FEATR";
        public const string ServiceType = "SERVICE";
        public const string BoardType = "PWR";
        public const string UserType = "USE";

        // SUBTYP definitions
        public const string GenericSubtype = "GENERIC";
        public const string LightSubtype = "LIGHT";
        public const string Magic1Subtype = "MAGIC1";
        public const string Magic2Subtype = "MAGIC2";
        public const string IntellibrightSubtype = "INTELLI";
        public const string SwimLaneType = "SAML";
        public const string GlowType = "GLOW";
        public const string GlowWhiteType = "GLOWT";
        public const string ColorWheelType = "COLORW";
        public const string SolarSubtype = "SOLAR";
        public const string DimmerSubtype = "DIMMER";

        // Parameter name definitions
        public const string ObjectName = "OBJNAM";
        public const string ObjectType = "OBJTYP";
        public const string ObjectVersion = "OBJREV";
        public const string Static = "STATIC";
        public const string SubType = "SUBTYP";
        public const string SystemName = "SNAME";
        public const string HardName = "HNAME";
        public const string Status = "STATUS";
        public const string Parent = "PARENT";
        public const string Mode = "MODE";
        public const string Count = "COUNT";
        public const string Party = "PARTY";
        public const string SIndex = "SINDEX";
        public const string Alarm = "ALARM";
        public const string HiTemp = "HITMP";
        public const string LowTemp = "LOTMP";
        public const string Temperature = "TEMP";
        public const string Filter = "FILTER";
        public const string Prime = "PRIM";
        public const string Use = "USE";
        public const string Timeout = "TIMOUT";
        public const string RunOn = "RUNON";
        public const string Source = "SOURCE";
        public const string HeatSource = "HTSRC";
        public const string Share = "SHARE";
        public const string Ready = "READY";
        public const string Delay = "DLY";
        public const string Action = "ACT";
        public const string Color = "COLOR";
        public const string Start = "START";
        public const string ListOrder = "LISTORD";
        public const string ShowMenu = "SHOMNU";
        public const string Port = "PORT";
        public const string ClockParam = "TIME";
        public const string DateParam = "DAY";
        public const string MinuteParam = "MIN";
        public const string MenuParam = "MENU";
        public const string StatusMessageMask = "MASKSM";

        // Well known Parameter Values
        public const string Off = "OFF";
        public const string On = "ON";
        public const string True = "TRUE";
        public const string False = "FALSE";

        // Embedded commands
        public const string Destroy = "DSTROY";

        public static int InviteValidAge = 72;

        public static string DefaultSuperChlorTimeout = "86400";

        public static List<string> StatusMessageParms = new List<string>()
        {
            ObjectType,
            Parent,
            Mode,
            SystemName,
            SIndex,
            ShowMenu
        }; 
        public static List<string> StaticObjects = new List<string>()
        {
            SuperAdmin,
            Admin,
            Guest,
            AllLights
        };

        public static List<string> LightGroupX = new List<string>()
        {
            "GLOW",
            "INTELLI",
            "MAGIC2"
        };

        public static List<string> LightGroupY = new List<string>()
        {
            "GLOW",
            "INTELLI",
            "MAGIC2",
            "SAML"
        };
        
        public static List<string> LightGroupZ = new List<string>()
        {
            "MAGIC1",
            "MAGIC2"
        };

        public static string AllLightCapabilities = "rpcsQdyMbaSowgITeiGnqBARLhmtEuDO";
        public static string DimmerCapabilities = "D";
        public static string LightCapabilities = "O";
        public static string GlobecCapabilities = "rpcsQdyMbaSowgITeiGnqBARLhE";
        public static string GlobewCapabilities = "O";
        public static string Intelli = "rpcsQdyMbaSowgITeiGnqBARE";
        public static string Magic1Capabilities = "hmtu";
        public static string Magic2Capabilities = "rpcsQdyMbaSowgITeiGnqBARLE";
        public static string SamSalCapabilities = "rpceiGnqBARLE";
        public static string PhotonCapabilities = "rpcsiGnqBARLO";
        public static string ColorWheelCapabilities = "rpcsiGnqBARL";
    }
}
