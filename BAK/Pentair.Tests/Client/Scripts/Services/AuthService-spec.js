function isFirefox() {
    return navigator.userAgent.match(/firefox/i);
}

describe('AuthService', function () {
    var AuthService;
    var $httpBackend;
    var $q;
    var LocalStorageService;
    var loginData;
    var response;
    var mockStorage;
    var Enumerations;
    var location;
    var postData;
    var HTTP_SERVICE_URL;
    beforeEach(module('PentairWebClient'));

    beforeEach(inject(function (_AuthService_, _$q_, _$httpBackend_, _LocalStorageService_, _Enumerations_,
                                _$location_, _SERVICE_HOST_NAME_) {
        AuthService = _AuthService_;
        $httpBackend = _$httpBackend_;
        $q = _$q_;
        LocalStorageService = _LocalStorageService_;
        mockStorage = {};
        Enumerations = _Enumerations_;
        location = _$location_;
        HTTP_SERVICE_URL = 'https://' + _SERVICE_HOST_NAME_;
        spyOn(LocalStorageService, 'get').and.callFake(function (key) {
            return mockStorage[key];
        });
        spyOn(LocalStorageService, 'set').and.callFake(function (key, value) {
            mockStorage[key] = value;
        });
        spyOn(LocalStorageService, 'clearAll');
        spyOn(window.sessionStorage, 'clear');
    }));

    describe('login success', function () {

        beforeEach(function () {
            loginData = {
                userName: "user",
                password: "password",
                rememberMe: true
            };

            response = { access_token: "A57F5BFC-2022-4227-8562-FD22B3AD64BE" };
            postData = "grant_type=password&username=" + loginData.userName + "&password=" + loginData.password;

            $httpBackend.expectPOST(HTTP_SERVICE_URL + 'login', postData).respond(response);
            spyOn(location, 'path');

        });

        describe('Login', function () {

            it('should save to session storage', function () {
                if (!isFirefox()) {
                  AuthService.logOut();

                  AuthService.login(loginData);
                  $httpBackend.flush();
                  expect(window.sessionStorage.token).toBe(response.access_token);
                  expect(window.sessionStorage.userName).toBe("user");
                }
            });

            it('should save to local storage', function () {
                if (!isFirefox()) {
                    AuthService.logOut();

                    AuthService.login(loginData);
                    $httpBackend.flush();
                    expect(location.path).toHaveBeenCalledWith("/login");
                    expect(LocalStorageService.get("token")).toBe(response.access_token);
                    expect(LocalStorageService.get("userName")).toBe("user");
                    expect(LocalStorageService.get("saveSession")).toBe(true);
                }
            });

            it('should not save to local storage', function () {
                if (!isFirefox()) {
                    AuthService.logOut();

                    loginData.rememberMe = false;
                    AuthService.login(loginData);
                    $httpBackend.flush();

                    expect(LocalStorageService.get("token")).toBeUndefined();
                    expect(LocalStorageService.get("userName")).toBeUndefined();
                    expect(LocalStorageService.get("saveSession")).toBeUndefined();
                }
            });
            it('should be a valid authentication object', function () {
                if (!isFirefox()) {
                    AuthService.logOut();

                    AuthService.login(loginData);
                    $httpBackend.flush();
                    expect(AuthService.authentication.isAuth).toBe(true);
                    expect(AuthService.authentication.userName).toBe("user");
                }
            });

            it('should clear the authentication alerts on login', function () {
                if (!isFirefox()) {
                    AuthService.authentication.accountCreated = true;
                    AuthService.authentication.passwordChanged = true;
                    AuthService.authentication.loginError = true;
                    AuthService.login(loginData);

                    expect(AuthService.authentication.accountCreated).toBe(false);
                    expect(AuthService.authentication.passwordChanged).toBe(false);
                    expect(AuthService.authentication.loginError).toBe(false);
                }
            });


        });

        describe('LogOut', function () {
            it('should clear credentials', function () {
                if (!isFirefox()) {
                    AuthService.login(loginData);
                    $httpBackend.flush();

                    AuthService.logOut();

                    expect(LocalStorageService.clearAll).toHaveBeenCalled();
                    expect(window.sessionStorage.clear).toHaveBeenCalled();
                    expect(AuthService.authentication.isAuth).toBe(false);
                    expect(AuthService.authentication.userName).toBe("");
                }
            });

        });

        // https://github.com/pivotal/jasmine/issues/299

        describe('restorePreviousSession', function () {
            it('should copy local storage to session storage', function () {
                if (!isFirefox()) {
                    AuthService.logOut();

                    LocalStorageService.set("token", response.access_token);
                    LocalStorageService.set("userName", "User");
                    AuthService.restorePreviousSession();

                    expect(window.sessionStorage.token).toBe(response.access_token);
                    expect(window.sessionStorage.userName).toBe("User");
                    expect(window.sessionStorage.selectedInstallation).toBeUndefined();
                }
            });
        });
    });

    describe('login error', function () {
        beforeEach(function () {
            loginData = {
                userName: "user",
                password: "password",
                rememberMe: true
            };
            $httpBackend.expectPOST(HTTP_SERVICE_URL + 'login', postData).respond(500);
            spyOn(AuthService, 'logOut');
            spyOn(location, 'path');
        });

        it('should logout and raise flag on error', function () {
            if (!isFirefox()) {
                expect(AuthService.authentication.loginError).toBe(false);
                AuthService.login(loginData);
                $httpBackend.flush();
                expect(AuthService.authentication.loginError).toBe(true);
                expect(location.path).toHaveBeenCalled();
            }
        });
    });

});
