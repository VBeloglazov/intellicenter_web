﻿using System.Collections.Generic;

namespace Pentair.Web.Api
{
    public enum EndpointType
    {
        Unknown = 0, // To catch errors
        THW,
        Client,
        Server
    };

    public enum SortDirection
    {
        Ascending,
        Decending
    }

    // TODO: Replace with System.Net.HttpStatusCode
    public class ResponseString
    {
        public const string
            OK = "200",
            Created = "201",
            NotModified = "304",
            BadRequest = "400",
            Unauthorized = "401",
            Forbidden = "403",
            NotFound = "404",
            Conflict = "409",
            InternalServerError = "500"
        ;
    }

    /// <summary>
    /// Commands received from a client or issued to the THW
    /// </summary>
    public class CommandString
    {
        public const string
            GetRegisteredProperties = "GETREGISTEREDPROPERTIES",
            GetParamList = "GETPARAMLIST",
            SetParamList = "SETPARAMLIST",
            WriteParamList = "WRITEPARAMLIST",
            RequestParamList = "REQUESTPARAMLIST",
            ReleaseParamList = "RELEASEPARAMLIST",
            ClearParam = "CLEARPARAM",
            ClearHistoryMessage = "CLEARHISTORYMESSAGE",
            GetQuery = "GETQUERY",
            SendQuery = "SENDQUERY",
            SetCommand = "SETCOMMAND",
            AcknowledgeRegistration = "ACKNOWLEDGEREGISTRATIONREQUEST",
            GetParamListResponse = "SENDPARAMLIST",
            CreateObject = "CREATEOBJECT",
            ObjectCreated = "OBJECTCREATED",
            NotifyList = "NOTIFYLIST",
            Registration = "REQUESTREGISTRATION",
            Authenticate = "REQUESTAUTHENTICATION",
            QueryPermissionLevel = "QUERYPERMISSIONLEVEL",
            InviteUser = "INVITEUSER",
            ResendInvite = "RESENDINVITE",
            DeleteUser = "DELETEUSER",
            UserList = "USERLIST",
            //UserNotificationList = "USERNOTIFICATIONLIST",
            //UserNotificationEdit = "EDITNOTIFICATION",
            EditUser = "EDITUSER",
            SystemInfoList = "SYSTEMINFO",
            SystemInfoEdit = "SAVESYSTEMINFO",
            ApiResponse = "APIRESPONSE",
            SuccessMessage = "OK",
            ErrorMessage = "ERROR",
            NotFoundMessage = "NOTFOUND",
            Ping = "PING",
            Invalid = "INVALIDREQUEST",

            ReadHistoryMessage = "READHISTORYMESSAGE",
            WriteHistoryMessage = "WRITEHISTORYMESSAGE",
            ReadLastHistoryTime = "READLASTHISTORYTIME",
            WriteLastHistoryTime = "WRITELASTHISTORYTIME",

            ReadStatusMessage = "READSTATUSMESSAGE",
            WriteStatusMessage = "WRITESTATUSMESSAGE",

            // 1.0.0.14
            AlertOn = "ALERTON",
            AlertOff = "ALERTOFF",

            ServerRemoveThwClients = "SERVERREMOVETHWCLIENTS",

            WriteHistoryMessageResponse = "WRITEHISTORYMESSAGERESPONSE",

            ReadPostalCodeInfoMessage = "READPOSTALCODEINFOMESSAGE",
            WritePostalCodeInfoMessage = "WRITEPOSTALCODEINFOMESSAGE",

            ReadCityNameInfoMessage = "READCITYNAMEINFOMESSAGE"
            ;

    } // class CommandString

    public enum CommandType
    {
        GetRegisteredProperties,
        GetParamListResponse,
        WriteParamList,
        NotifyList,
        CreateObject,
        ObjectCreated,
        Registration,
        Authenticate,
        QueryPermissionLevel,
        GetParamList,
        SetParamList,
        RequestParamList,
        ReleaseParamList,
        ClearParam,
        ClearHistoryMessage, 
        GetQuery,
        SendQuery,
        SetCommand,
        RegistrationAcknowledgement,
        InviteUser,
        ResendInvite,
        DeleteUser,
        UserList,
        UserNotificationList,
        UserNotificationEdit,
        EditUser,
        SystemInfoList,
        SystemInfoEdit,
        ApiResponse,
        SuccessMessage,
        ErrorMessage,
        Invalid,
        Ping,

        ReadHistoryMessage,
        WriteHistoryMessage,
        ErrorHistoryMessage,
        ReadLastHistoryTime,
        WriteLastHistoryTime,

        ReadStatusMessage,
        WriteStatusMessage,

        // 1.0.0.14
        AlertOn,
        AlertOff,

        ServerRemoveThwClients,

        ReadPostalCodeInfoMessage,
        WritePostalCodeInfoMessage,

        ReadCityNameInfoMessage

    } // enum CommandType

    public class CommandList
    {
        public static Dictionary<string, CommandType> Commands = new Dictionary<string, CommandType>()
        {
            {CommandString.GetRegisteredProperties, CommandType.GetRegisteredProperties},
            {CommandString.GetParamListResponse, CommandType.GetParamListResponse},
            {CommandString.WriteParamList, CommandType.WriteParamList},
            {CommandString.NotifyList, CommandType.NotifyList},
            {CommandString.CreateObject, CommandType.CreateObject},
            {CommandString.ObjectCreated, CommandType.ObjectCreated},
            {CommandString.Registration, CommandType.Registration},
            {CommandString.Authenticate, CommandType.Authenticate},
            {CommandString.GetParamList, CommandType.GetParamList},
            {CommandString.SetParamList, CommandType.SetParamList},
            {CommandString.RequestParamList, CommandType.RequestParamList},
            {CommandString.ReleaseParamList, CommandType.ReleaseParamList},
            {CommandString.ClearParam, CommandType.ClearParam},
            {CommandString.ClearHistoryMessage, CommandType.ClearHistoryMessage}, 
            {CommandString.GetQuery, CommandType.GetQuery},
            {CommandString.SendQuery, CommandType.SendQuery},
            {CommandString.SetCommand, CommandType.SetCommand},
            {CommandString.AcknowledgeRegistration, CommandType.RegistrationAcknowledgement},
            {CommandString.QueryPermissionLevel, CommandType.QueryPermissionLevel},
            {CommandString.InviteUser, CommandType.InviteUser},
            {CommandString.ResendInvite, CommandType.ResendInvite},
            {CommandString.DeleteUser, CommandType.DeleteUser},
            {CommandString.UserList, CommandType.UserList},
            //{CommandString.UserNotificationList, CommandType.UserNotificationList},
            //{CommandString.UserNotificationEdit, CommandType.UserNotificationEdit},
            {CommandString.EditUser, CommandType.EditUser},
            {CommandString.SystemInfoList, CommandType.SystemInfoList},
            {CommandString.SystemInfoEdit, CommandType.SystemInfoEdit},
            {CommandString.SuccessMessage, CommandType.SuccessMessage},
            {CommandString.ErrorMessage, CommandType.ErrorMessage},
            {CommandString.Ping, CommandType.Ping},

            {CommandString.ReadHistoryMessage, CommandType.ReadHistoryMessage},
            {CommandString.WriteHistoryMessage, CommandType.WriteHistoryMessage},
            {CommandString.ReadLastHistoryTime, CommandType.ReadLastHistoryTime},
            {CommandString.WriteLastHistoryTime, CommandType.WriteLastHistoryTime},

            {CommandString.ReadStatusMessage, CommandType.ReadStatusMessage},
            {CommandString.WriteStatusMessage, CommandType.WriteStatusMessage},

            // 1.0.0.14
            {CommandString.AlertOn, CommandType.AlertOn},
            {CommandString.AlertOff, CommandType.AlertOff},

            {CommandString.ServerRemoveThwClients, CommandType.ServerRemoveThwClients},

            {CommandString.ReadPostalCodeInfoMessage, CommandType.ReadPostalCodeInfoMessage},
            {CommandString.WritePostalCodeInfoMessage, CommandType.WritePostalCodeInfoMessage},

            {CommandString.ReadCityNameInfoMessage, CommandType.ReadCityNameInfoMessage}
        };
    
    } // class CommandList

    public class WebSocketActions
    {
        public const string 
            Open = "Open",
            Close = "Close",
            Send = "Send THW",
            Receive = "Receive THW",
            Error = "Error",
            SendClient = "Send Client",
            ReceiveClient = "Receive Client",
            ReceiveClientInvalid = "Receive Client Invalid"
        ;
    }

    public enum UserManagementResult
    {
        UserNotFound = 0,               // 0
        LastAdminError,                 // 1
        PropertyNotFound,               // 2
        UserNotAssociatedWithProperty,  // 3
        InviteNotFound,                 // 4
        Success,                        // 5
        UnknownError                    // 6
    };
}