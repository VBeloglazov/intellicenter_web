﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Configuration;
using System.Web.Helpers;
using System.Web.Http.Results;
using System.Configuration;

namespace Pentair.Web.Api
{
    /* TODO: 2b deleted
    public class EmailApi_Mandrill
    {
        private class JsonEmail
        {
            /// <summary>
            /// JSon used to send a raw-send type email.
            /// </summary>
            public string key { get; set; }
            public string raw_message { get; set; }
            public string from_email { get; set; }
            public string from_name { get; set; }
            public List<string> to { get; set; }
            public bool async { get; set; }
            public string ip_pool { get; set; }
            public string send_at { get; set; }
            public string return_path_domain { get; set; }
        }

        // 1.0.0.14
        private class JsonEmail_HTML_Recipient
        {
            public string email { get; set; }
            public string name { get; set; }
            public string type { get { return "to"; } }

        } // class JsonEmail_HTML_Recipient

        // 1.0.0.14
        private class JsonEmail_HTML_Message
        {
            public string html { get; set; }
            public string subject { get; set; }
            public string from_email { get; set; }
            public string from_name { get; set; }
            public List<JsonEmail_HTML_Recipient> to { get; set; }

        } // JsonEmail_HTML_Message

        // 1.0.0.14
        private class JsonEmail_HTML
        {
            public string key { get; set; }
            public bool async { get; set; }
            public string ip_pool { get; set; }
            public string return_path_domain { get; set; }
            public string send_at { get; set; }
            public JsonEmail_HTML_Message message { get; set; }

        } // class JsonEmail_HTML

        private readonly string _mailServer;
        private readonly string _key;
        private readonly string _from;

        public EmailApi_Mandrill()
        {
            Configuration configSettings = WebConfigurationManager.OpenWebConfiguration("/service");
            _mailServer = configSettings.AppSettings.Settings["mailserver"].Value;
            _key = configSettings.AppSettings.Settings["emailkey"].Value;
            _from = configSettings.AppSettings.Settings["fromaddress"].Value;
        
        } // EmailApi_Mandrill()

        // 1.0.0.14
        // TODO: Make it async
        public List<EmailResponse> SendEmail_HTML(
            Email email)
        {
            JsonEmail_HTML emailObject = new JsonEmail_HTML()
            {
                key = _key,
                async = false,
                ip_pool = "Main Pool",
                return_path_domain = "",
                send_at = "",
                message = new JsonEmail_HTML_Message()
                {
                    html = email.Message,
                    subject = email.Subject,
                    from_email = String.IsNullOrEmpty(email.From) ? _from : email.From,
                    from_name = "Pentair IntelliCenter",
                    to = new List<JsonEmail_HTML_Recipient>() { new JsonEmail_HTML_Recipient() { email = email.To } }
                }
            };

            string emailJson = Json.Encode(emailObject);

            string result;
            using (WebClient wc = new WebClient())
            {
                result = wc.UploadString(_mailServer + "/messages/send.json", emailJson);
            }

            return Json.Decode<List<EmailResponse>>(result);

        } // SendEmail_HTML()

        /// <summary>
        /// Send an email to the person specified by the email object passed in.
        /// </summary>
        /// <param name="email">email object</param>
        /// <returns>string representing the result of the process.</returns>
        public List<EmailResponse> SendEmail(
            Email email)
        {
            JsonEmail emailObject = new JsonEmail()
            {
                from_email = String.IsNullOrEmpty(email.From) ? _from : email.From,
                raw_message = String.Format("Subject: {0}\n\n{1}", email.Subject, email.Message),
                key = _key,
                async = false,
                from_name = "",
                ip_pool = "Main Pool",
                return_path_domain = "",
                send_at = "",
                to = new List<string>() { email.To }
            };

            string result;
            using (WebClient wc = new WebClient())
            {
                result = wc.UploadString(_mailServer + "/messages/send-raw.json", Json.Encode(emailObject));
            }

            return Json.Decode<List<EmailResponse>>(result);
        
        } // SendEmail()

        /// <summary>
        /// Ping the email server to validate connectivity and a valid key
        /// </summary>
        /// <returns>boolean</returns>
        public bool PingServer()
        {
            var ping = string.Format("{{\"key\" : \"{0}\"}}", _key);
            var response = "";
            using (var wc = new WebClient())
            {
                response = wc.UploadString(_mailServer + "/users/ping.json", ping);
            }
            return (response.Contains("PONG!"));
        }

    } // class EmailApi_Mandrill
    */
}