﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pentair.Web.Api
{
    public class ConfirmUserNameChangeViewModel
    {
        [Required]
        [EmailAddress]
        public string OldUserName { get; set; }
        
        [Required]
        [EmailAddress]
        public string UserName { get; set; }

        [Required]
        public string InviteId { get; set; }
    }
}