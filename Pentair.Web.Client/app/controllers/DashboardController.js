/// <reference path="DashboardController.js" />
'use strict';
var PWC = PWC || {};
PWC.controller('DashboardController', [
    '$scope', '$rootScope', '$location', 'PropertyService', 'AppService', 'PropertyModel', 'Enumerations',
    function ($scope, $rootScope, $location, propertyService, appService, propertyModel, enumerations) {
        $scope.Busy = false;
        $scope.preferences = "ENGLISH";
        $scope.AdminSecurity = false;
        $scope.DashboardTabs = propertyModel.dashboardTabs;
        $scope.$parent.Layout = "layout-base";
        $scope.forecast = [];
        $scope.weatherView = 'CURRENT';
        $scope.serviceError = true;
        $scope.installation = propertyModel.getInstallationObject(propertyModel.SelectedInstallation);
        $scope.securityEnabled = propertyModel.SecurityEnabled;
        $scope.SHOMNU = propertyModel.ACL.SHOMNU;
        $scope.ACL = propertyModel.ACL;
        $scope.Property = propertyModel;

        if (propertyModel.Objects._5451) {
            if (propertyModel.Objects._5451.MODE) {
                $scope.preferences = propertyModel.Objects._5451.MODE;
            }
        }
        createWeatherItems();

        init();

        PWC.setActiveTab($scope.DashboardTabs, 'home');

        $scope.gotoTab = function (tab) {
            var allow = true
            if ($scope.securityEnabled) {   // check security
                switch(tab){
                    case 'lights':
                        if ($scope.SHOMNU.indexOf('l') < 0) 
                            allow = false    // do no process
                        break;

                    case 'features':
                        if ($scope.SHOMNU.indexOf('f') < 0) 
                            allow = false    // do no process
                        break;

                    case 'pumps':
                        if (($scope.SHOMNU.indexOf('p') >= 0) &&     // Pool :  p
                            ($scope.SHOMNU.indexOf('m') >= 0))       // spa  :  m
                            allow = true;
                        else
                            allow = false;
                        break;

                    case 'schedules':
                        if ($scope.SHOMNU.indexOf('e') > 0) {
                            $location.path("/settings");
                            return;
                        }

                        allow = false;                        
                        break;
                        

                    case 'chemistry':
                        if ($scope.SHOMNU.indexOf('c') < 0)
                            allow = false;
                        break;

                    default: // do nothing
                        break;
                }                
            }

            if (allow)
                PWC.setActiveTab($scope.DashboardTabs, tab);
        }

        $scope.isFreezeProtectionOn = function () {
            if (propertyModel.freezeStatus === "1")
                return true;
            else
                return false;
      };

      $scope.masterOff = function () {
          propertyService.SetCommand('MasterOff');
          console.log("<<<<<<<<<<<<<<<<<<<<<<<<<<< ********************* turning off all lights")
          propertyService.SetAllLightsOff();                          
      };

      $scope.toggleWeather = function () {
        $scope.weatherView = $scope.weatherView == 'CURRENT' ? 'FORECAST' : 'CURRENT';
        init();
      }

      $scope.updateWeather = function () {
          // update weather every 20 minutes
          init();
      }

      $scope.WeatherForcast = function (type) {
        if (type) {
          var icon = enumerations.WeatherCodes[type];
          if (icon) {
            return icon.icon;
          }
        }
      }
      //  create weather items
      function createWeatherItems() {
          console.log("------------------- Create weather Items -------------------")
          for (var i = 0; i < 7; i++) {
              var wo = {
                  time:         "",
                  clouds:       "",
                  tempC:         0,
                  tempHiC:       0,
                  tempLoC:       0, 
                  tempF:         0,
                  tempHiF:       0,
                  tempLoF:       0,
                  indexC:        0,
                  indexF:        0,
                  humidity:      0,
                  precipMM:      0,
                  precipIN:     0.0,
                  precipProb:    0,
                  pressureMB:    0,
                  pressureIN:    0,
                  dewPointC:     0,
                  dewPointF:     0,
                  sunrise:      "",
                  sunset:       "",
                  weatherText:  "",
                  weatherCode:  "",
                  cloudCode:    "",
                  cloudCover:   0,
                  icon:         "",
                  windDir:      "",
                  windSpeedMPH:  0,
                  windSpeedKPH:  0,
                  uvIndex: 0
              };
              $scope.forecast[i] = wo;

          }
      }

      // init
      function init() {
          if ($scope.installation && $scope.installation.Zip) {
              // current appService.GetData
              
              appService.GetData("api/WeatherCurrentConditions?zip=" + $scope.installation.Zip + "&service=" + $scope.weatherView).then(function successCallback(response) {
                  if (response && response.data) {
                      if (response.data["success"] == true) {
                          var data = response.data["response"];
                          $scope.current;
                          if (data["ob"]) {
                              var p = data["ob"];
                              if (p) {
                                  var imageName = p.icon.indexOf('.');
                                  var imageIcon = 'iconw-na';
                                  if (imageName > 0) {
                                      imageIcon = 'iconw-' + p.icon.slice(0, imageName);
                                  }
                                 
                                  var wo = {
                                      time: p.timestamp,                        //validTime,
                                      clouds: p.cloudsCoded,
                                      tempC: p.tempC,                           //avgTempC,
                                      tempHiC: $scope.forecast[0].tempHiC,      //p.tempMax6hrC,   //maxTempC,
                                      tempLoC: $scope.forecast[0].tempLoC,      //p.tempMin6hrC,   //minTempC,
                                      tempF: p.tempF,                           //avgTempF,
                                      tempHiF: $scope.forecast[0].tempHiF,      //p.tempMax6hrF,   //maxTempF,
                                      tempLoF: $scope.forecast[0].tempLoF,      //p.tempMin6hrF,   //minTempF,
                                      indexC: p.feelslikeC,
                                      indexF: p.feelslikeF,
                                      humidity: p.humidity,
                                      precipMM: p.precipMM,
                                      precipIN: p.precipIN,
                                      precipProb: $scope.forecast[0].precipProb,                     //pop,
                                      pressureMB: p.pressureMB,
                                      pressureIN: p.pressureIN,
                                      dewPointC: p.dewpointC,
                                      dewPointF: p.dewpointF,
                                      sunrise: p.sunriseISO,
                                      sunset: p.sunsetISO,
                                      weatherText: p.weatherPrimary,
                                      weatherCode: p.weatherPrimaryCoded,
                                      cloudCode: p.cloudsCoded,
                                      cloudCover: p.sky,
                                      icon: imageIcon,
                                      windDir: p.windDir,
                                      windSpeedMPH: p.windSpeedMPH,
                                      windSpeedKPH: p.windSpeedKPH,
                                      uvIndex: $scope.forecast[0].uvIndex,      //p.uvi
                                  };
                                  $scope.current = wo;
                                  $scope.forecast[0] = wo;
                              }
                              $scope.serviceError = false;
                          }
                      }
                  }
              });
              
              // curent appService.GetData
              
              //-------------
              // forecast appService.GetData
              appService.GetData("api/WeatherForecast?zip=" + $scope.installation.Zip + "&service=" + $scope.weatherView).then(function successCallback(response) {
                  if (response && response.data) {
                      if (response.data["success"] == true) {
                          var data = response.data["response"];
                          //$scope.forecast = [];                                            
                          if (data[0]) {
                          for (var i = 0; i < 6; i++) {
                              var periods = data[0]["periods"];
                              if (periods) {
                                  var p = periods[i];
                                  if (p) {
                                      var imageName = p.icon.indexOf('.');
                                      var imageIcon = 'iconw-na';
                                      if (imageName > 0) {
                                          imageIcon = 'iconw-'+p.icon.slice(0, imageName);
                                      }
                                      var wo = {
                                          time: p.validTime,
                                          clouds: p.cloudsCoded,
                                          tempC: p.avgTempC,
                                          tempHiC: p.maxTempC,
                                          tempLoC: p.minTempC,
                                          tempF: p.avgTempF,
                                          tempHiF: p.maxTempF,
                                          tempLoF: p.minTempF,
                                          indexC: p.feelslikeC,
                                          indexF: p.feelslikeF,
                                          humidity: p.humidity,
                                          precipMM: p.precipMM,
                                          precipIN: p.precipIN,
                                          precipProb: p.pop,
                                          pressureMB: p.pressureMB,
                                          pressureIN: p.pressureIN,
                                          dewPointC: p.dewpointC,
                                          dewPointF: p.dewpointF,
                                          sunrise: p.sunriseISO,
                                          sunset: p.sunsetISO,
                                          weatherText: p.weatherPrimary,
                                          weatherCode: p.weatherPrimaryCoded,
                                          cloudCode: p.cloudsCoded,
                                          cloudCover: p.sky,
                                          icon: imageIcon,
                                          windDir: p.windDir,
                                          windSpeedMPH: p.windSpeedMPH,
                                          windSpeedKPH: p.windSpeedKPH,
                                          uvIndex: p.uvi
                                      };
                                      if (i === 0) {    // update data from current forcast (today).
                                          $scope.forecast[0].tempHiC = wo.tempHiC;
                                          $scope.forecast[0].tempLoC = wo.tempLoC;

                                          $scope.forecast[0].tempHiF = wo.tempHiF;
                                          $scope.forecast[0].tempLoF = wo.tempLoF;

                                          $scope.forecast[0].precipProb = wo.precipProb;

                                          $scope.forecast[0].uvIndex = wo.uvIndex;
                                      }                                      
                                      // $scope.forecast.push(wo);
                                      $scope.forecast[i + 1] = wo;
                                  }
                              }
                          }
                          }
                          $scope.serviceError = false;
                      } else {
                          $scope.serviceError = true;
                      }
                  } else {
                      $scope.serviceError = true;
                  }
              });
              // forecast appService.GetData

              // retrieve webapiversion
              appService.GetData('Api/Account/VersionNumber').then(function successCallback(response) {
                  $scope.Property.webApiVersion = response.data;
               });
          } else {
              $scope.serviceError = true;
          }
      }

/*********** SECURITY MODE July-18-2018 ********
VacationMode		,  v        done 
Support  		    ,  o        NA    
GeneralSettings		,  g        done  , under testing
AlertsNotification	,  q        done
UserPortal		    ,  i        ONLY  ADMIN : THW
Groups			    ,  k        done 
Remotes			    ,  w        Removed     *** this option has been removed July-2018 : replace 'w' to 'a' option as per steve recomendation
AdvancedSettings	,  a        done   , under testing
Chemistry		    ,  c        done
Status			    ,  t        done
DeleteStatus		,  r        Removed    *** this option has been removed July-2018 : replace 'r' to 't' option as per steve recomendation
Schedules		    ,  e        done
Features		    ,  f        done
Lights			    ,  l        done
Pool			    ,  p        done
PoolHeatSource		,  h        **
PoolTemp		    ,  P        **
Spa			        ,  m        done
SpaHeatSource		,  n        **
SpaTemp			    ,  S        **
History			    ,  u        done
MultiBodyDrawer		,  b        NA
ServiceMode		    ,  x        done 
SystemConfig		,  C        done 
********************************/
      $scope.getTabName = function (tab) {
          var name = tab.heading
          //if ($scope.isSecurityLocked(tab)) {
          //    name = name + " *"              
          //}
          
        return name
      }

      $scope.getTab = function (tabs,index) {
          var name = tab.heading
          //if ($scope.isSecurityLocked(tab)) {
          //    name = name + " *"
          //}

          return name
      }

      $scope.isSecurityEnabled = function () {

          // checking
          if ($scope.ACL) {    // ckecking if ACL exist
              if ($scope.ACL.OBJNAM) {    // ckecking if objnam exist
                  if ($scope.ACL.OBJNAM === "U0000") {   // checking GUEST IS SIGNED
                      return true //security is enabled by GUEST user OR HAS NO RIGTHS ...
                  }
              }
          }

          if ($scope.Property && ($scope.Property.SecurityEnabled === true))      //if ($scope.securityEnabled)          
              return true
          else
              return false
      }

      $scope.isSecurityLocked = function (tab) {
        
          // check first if security is enabled

        if ($scope.isSecurityEnabled() === false) {
              return false;
        }

        if (tab.heading === 'HOME') {       // allow always home page view only..
                return false;
        }

        if (tab.heading === 'POOLS') {
            if ($scope.isSecurityEnabled()) {
                 if (($scope.SHOMNU.indexOf('p') >= 0) ||     // Pool :  p
                      ($scope.SHOMNU.indexOf('m') >= 0)       // spa  :  m
                     )
                     return false;
                 else
                     return true;
             }
        }

        if (tab.heading === 'FEATURES' && ($scope.isSecurityEnabled() && $scope.SHOMNU.indexOf('f') < 0)) {
            return true;
        }

        if (tab.heading === 'LIGHTS' && ($scope.isSecurityEnabled() && $scope.SHOMNU.indexOf('l') < 0)) {
            return true;
        }

        if (tab.heading === 'LIGHT GROUPS') {
            if ($scope.isSecurityEnabled() &&
              ($scope.SHOMNU.indexOf('l') >= 0) &&
              ($scope.SHOMNU.indexOf('k') >= 0) ) 
            return false        
        else 
            return true
        }

        if (tab.heading === 'CHEMISTRY' && ($scope.isSecurityEnabled() && $scope.SHOMNU.indexOf('c') < 0)) {
            return true;
        }

        if (tab.heading === 'PUMPS'){
            if ($scope.isSecurityEnabled()) {
                if (($scope.SHOMNU.indexOf('p') >= 0) &&     // Pool :  p
                     ($scope.SHOMNU.indexOf('m') >= 0)       // spa  :  m
                    )
                    return false;
                else
                    return true;
            }
        }
        
        if (tab.heading === 'HISTORY' && ($scope.isSecurityEnabled() && $scope.SHOMNU.indexOf('u') < 0)) {
          return true;
        }
        
        if (tab.heading === 'REMOTES' && ($scope.isSecurityEnabled() && $scope.SHOMNU.indexOf('a') < 0)) {
            return true;
        }

        if (tab.heading === 'SCHEDULES' && ($scope.isSecurityEnabled() && $scope.SHOMNU.indexOf('e') < 0)) {
            return true;
        }
        
        if (tab.heading === 'SYSTEM CONFIGURATION') {             
              return false; // allow system configuration
        }
        
        if (tab.heading === 'GENERAL SETTINGS') {
            if ($scope.isSecurityEnabled())
                if (($scope.SHOMNU.indexOf('a') >= 0) ||    // AdvancedSettings
                     ($scope.SHOMNU.indexOf('q') >= 0) ||    // AlertsNotification
                     ($scope.SHOMNU.indexOf('g') >= 0)       // general settings
                    )
                    return false;
                else
                    return true;
        }
        
        return false

      };
    }
]);