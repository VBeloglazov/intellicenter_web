﻿using System;
using System.Collections.Generic;

using Newtonsoft.Json;

namespace Pentair.Domain.Client.Helpers.Commands
{
    public class NewParamObject
    {
        [JsonProperty("objtyp")]
        public string ObjTyp { get; private set; }

        [JsonProperty("params")]
        public Dictionary<string, object> Parameters { get; private set; }

        public NewParamObject(
            string objTyp)
        {
            this.ObjTyp = objTyp;
        }

    } // class NewParamObject
}