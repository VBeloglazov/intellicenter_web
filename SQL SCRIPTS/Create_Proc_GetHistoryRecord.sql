USE [PathfinderIntellicenter]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (
	SELECT * FROM sys.objects
	WHERE object_id = OBJECT_ID(N'dbo.pr_GetHistoryRecord')
	AND type in (N'P', N'PC')
)
DROP PROCEDURE dbo.pr_GetHistoryRecord
GO


-- =============================================
-- Author			:	Wayne Hui
-- Create date		:	02/13/2019
-- Parameters		:
--	@InstallationId	:	Device ID
--  @StartTime		:	Revision start
--  @EndTime		:	Revision end
-- Description		:	Get the history between the revision range
--
-- Modified By			Date		Description
-- =============================================
CREATE PROCEDURE [dbo].[pr_GetHistoryRecord]
	@InstallationId		INT
	, @StartTime		BIGINT
	, @EndTime			BIGINT
AS
BEGIN

	SET NOCOUNT OFF

----TEST
--DECLARE
--	@InstallationId		INT
--	, @StartTime		BIGINT
--	, @EndTime			BIGINT

--SET @InstallationId	= 3948
--SET @StartTime		= 1550077200
--SET @EndTime		= 1550077680

----END TEST

	---- Update the value for this row

	SELECT 
		Installation
		,Revision
		,ObjNam
		,[Key]
		,Value
		,CreateDateTime
		,UpdateDateTime
	FROM 
		dbo.ControlObjectsHistory
	WHERE
		INSTALLATION = @InstallationId 
        AND Revision BETWEEN @StartTime AND @EndTime 
	ORDER BY
		ObjNam
		, Revision
		
END

GO


--GRANT EXECUTE ON [dbo].[pr_GetHistoryRecord] TO PUBLIC
--GO
