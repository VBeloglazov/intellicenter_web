﻿using System.Collections.Generic;
using System.Linq;

namespace Pentair.Domain.Simulator.Repository
{
    public interface IRepository<T>
    {
        IQueryable<T> Items { get; }
        T AddItem(T newItem);
        List<T> ItemList();
        T FindById(int id);
        T RemoveById(int id);
        T Remove(T item);
        int SaveChanges();
    }
}
