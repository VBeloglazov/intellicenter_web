﻿using Ninject;
using Ninject.Syntax;
using System;
using System.Web;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Web.Http.Dependencies;
using Pentair.Domain.Client.Infrastructure;
using Pentair.Domain.Client.Entity;
using Pentair.Domain.Client.Repository;

namespace Pentair.Web.Api.Infrastructure
{
    /* TODO: 2b removed 2017-12-13
    public class NinjectDependencyResolver : NinjectDependencyScope, IDependencyResolver
    {
        private IKernel _kernel;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="kernel"></param>
        public NinjectDependencyResolver(IKernel kernel)
            : base(kernel)
        {
            this._kernel = kernel;
            AddBindings();
        }

        public IDependencyScope BeginScope()
        {
            return new NinjectDependencyScope(_kernel.BeginBlock());
        }

        private void AddBindings()
        {
            // Create only 1 instance of each context per request
            _kernel.Bind<ApiContext>()
                .To<ApiContext>().InScope(ctx => HttpContext.Current);
        }
    }
    */
}