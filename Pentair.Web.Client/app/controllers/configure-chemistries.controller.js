'use strict';
var PWC = PWC || {};
PWC.controller('ConfigureChemistryController', [
  '$rootScope', '$scope', '$location', '$interpolate', 'PropertyService', 'PropertyModel', 'Enumerations', '$q', 'Notification',
  function ($rootScope, $scope, $location, $interpolate, propertyService, propertyModel, Enumerations, $q, Notification) {
    $scope.property = propertyModel;
    $scope.accordionStateByObjectNames = {};

    $scope.chems = [];
    
    $scope.chemSubtypes = [];
    $scope.currentchemSubtypes = [];
    $scope.chemSubtypes = Enumerations.getChemSubtypeObjects();
    $scope.bodies = $scope.property.Bodies;
    $scope.firstUpdate = true;

    $rootScope.editMode.chemistrycontrol = {};

    updateChemistry();

    $scope.errorSave = function(errorMessage) {
      Notification.error({message: 'Failed!' + ' ' + errorMessage, delay: 1000});
    };

    $scope.successSave = function() {
      Notification.success({message: 'Saved!', delay: 1000});
    };

    $scope.successCancel = function() {
      Notification.success({message: 'Cancelled!', delay: 1000});
    };

    $scope.cancelEdit = function(chem, index) {
      // $rootScope.editMode.chemistrycontrol[chem.OBJNAM] = false;
      $scope.chems = angular.copy($scope.chemsBeforeEdit);
      $rootScope.forms.chemistrycontrol[index].$setPristine();
      $scope.successCancel();
    };

    $scope.add = function () {
     if ($scope.canCreateChemsItems() === false)
            return; // do nothing
      var chem = {
        OBJNAM: Math.uuid(4),
        createMode: true,
        addressMap: [],
        PRIM: 60,
        SEC: 10,
        enableChlorination: false,
        chlorTimeLeftHours: 12
      };
      if(propertyModel.isSingleOrSharedBody()) {
        chem.BODY = propertyModel.getSingleOrSharedPoolID();
      }
      chem.addressMap = (Array.apply(null, {length: 16})).map(function(item, i) {
        return i+1;
      });
      $scope.chems.push(chem);
      $scope.accordionStateByObjectNames[chem.objnam] = {};
      $scope.accordionStateByObjectNames[chem.objnam].open = true;
      $rootScope.editMode.chemistrycontrol[chem.OBJNAM] = true;
    };

    $scope.removeItems = function() {
      // populate the list of object names we are going to delete
      $scope.objectNamesToDelete = $scope.chems.map(function(obj) {
        if (obj.isSelected) {
          return obj.OBJNAM;
        }
      });
      propertyService.DeleteObject($scope.objectNamesToDelete).then(function() {
        updateChemistry();
      });
    };

    $scope.processCurrentChemList = function () {
        $scope.currentchemSubtypes.length = 0;
        var index  = 0;
        angular.forEach($scope.chemSubtypes, function (chem) { // adding items to list
            
            if (chem.actualValue === 'ICHLOR')  {     // MAXIMUM OF INTELLICHLOR IS 1
                if ($scope.reachMaxIntelliChlor() === false){
                    $scope.currentchemSubtypes[index] = chem; // adding intellichlor 
                    index = index + 1;
                }
            }
            else {
                if ($scope.reachMaxIntelliChem() === false) {   // MAXIMUM OF INTELLICHEM base on how many bodies
                    $scope.currentchemSubtypes[index] = chem;
                    index = index + 1;
                }
            }
        });
        
    };

    $scope.hideAddControllerChem = function () {
        if ($scope.canCreateChemsItems() === false)
            return true;
        else
            return false;
    };

    $scope.disableChemEntry = function (type) {        
        if (type === 'ICHLOR') {    // 
            if ($scope.reachMaxIntelliChlor())
                return true;
        }
        return false;
    };
      // check if is possible to add intellichlor devices
    $scope.reachMaxIntelliChlor = function () {
        var enable = false;
        angular.forEach($scope.chems, function (chem) {
            if (chem.SUBTYP === 'ICHLOR') {     // MAXIMUM OF INTELLICHLOR IS 1
                enable = true;
            }
        });

        return enable;
    };

      // check if is possible to add intellichem devices
    $scope.reachMaxIntelliChem = function () {
        var count = 0;
        var maxchems = 1;

        if ((propertyModel.PANID === "SINGLE") ||
            (propertyModel.PANID === "SHARE"))
            maxchems = 1;

        if (propertyModel.PANID === "DUAL")
            maxchems = 2;

        angular.forEach($scope.chems, function (chem) {
            if (chem.SUBTYP === 'ICHEM') {     // MAXIMUM OF INTELLICHLOR IS 1
                count = count + 1;
            }
        });

        if (count >= maxchems)
            return true;
        else
            return false;

        
    };

    $scope.getChemsCount = function () {
        var count = $scope.chems.length;
        return count;
    };
      // check can add more FeatureCircuits
    $scope.getMaxControllers = function () {
        var maxchems = 2;

        if ((propertyModel.PANID === "SINGLE") ||
             (propertyModel.PANID === "SHARE"))
            maxchems = 2;

        if (propertyModel.PANID === "DUAL")
            maxchems = 3;

        return maxchems;

    };

    // check if user can create more devices
    $scope.canCreateChemsItems = function () {  
        var count = $scope.chems.length;
        var maxchems = 2;
        
        if ((propertyModel.PANID === "SINGLE") ||
             (propertyModel.PANID === "SHARE"))
            maxchems = 2;

        if (propertyModel.PANID === "DUAL")
            maxchems = 3;
                        
        if (count < maxchems)
            return true;
        else
            return false;
    };


    $scope.isDualOrShare = function () {
        if ((propertyModel.PANID === "DUAL") ||
            (propertyModel.PANID === "SHARE") )
            {
            return true;
        }
        else {
            return false;
        }
    };

    $scope.isDualConfiguration = function () {                
        if (propertyModel.PANID === "DUAL") {
            return true;
        }
        else {
            return false;
        }
    };


    $scope.save = function(chem, index) {
      var params = {};
      var objnam = chem.objnam;
      params.SNAME = chem.SNAME;
      params.SUBTYP = chem.SUBTYP;
      params.BODY = chem.BODY;
      params.COMUART = chem.COMUART;
      if(chem.SUBTYP === 'ICHLOR') {
        params.TIMOUT = (chem.chlorTimeLeftHours * 3600) + '';
        params.SUPER = chem.enableChlorination ? 'ON' : 'OFF';
        params.PRIM = chem.PRIM + '';
        params.SEC = chem.SEC + '';
      }
      $rootScope.editMode.chemistrycontrol[chem.objnam] = false;
      if (chem.createMode) {
        propertyService.CreateNewObject('CHEM', params).then(function() {
          updateChemistry();
          $rootScope.forms.chemistrycontrol[index].$setPristine();
          $scope.successSave();
        });
      } else {
        propertyService.SetParams(objnam, params).then(function() {
          updateChemistry();
          $rootScope.forms.chemistrycontrol[index].$setPristine();
          $scope.successSave();
        });
      }
    };

    $scope.setEditMode = function(chemObject) {
      chemObject.editMode = true;
    };

    $scope.getDictionaryValue = function(actualValue) {
      if (!actualValue)
        return actualValue;
      var match = $scope.chemSubtypes.filter(function(chemSubtype) {
        if (chemSubtype.actualValue === actualValue) {
          return chemSubtype;
        }
      });

      if (match && match.length > 0 && match[0].dictionaryValue) {
        return match[0].dictionaryValue;
      }
      return actualValue;
    };

    $scope.$on('NotifyList', function (objname, matchObject, objUpdated) {

        //console.log("---- processing notify Chemistry ..." + matchObject);
        
        angular.forEach($scope.chems, function (chem) {            
            if (chem.hName === objUpdated.hName ) { // check match
                if (chem.SNAME) {
                   // chem.SNAME = objUpdated.params.SNAME;                    
                    //console.log(" set chem updated name " + objUpdated.params.SNAME);
                }
            }            
        });        
    })

    function updateChemistry() {
      propertyService.getChemistries().then(function(chemistries) {
        $scope.chems = chemistries.objectList.map(function(item) {
          // flatten params
          var chem = item.params;
          chem.objnam = item.objnam;
          chem.addressMap = (Array.apply(null, {length: 16})).map(function(item, i) {
            return i+1;
          });
          // super chlorinator time remaining is in seconds but the frontend has it stepping in hours
          // going to round up to nearest hour as behaviour here
          chem.chlorTimeLeftHours = Math.ceil(chem.TIMOUT / 3600);
          //chem.chlorTimeLeftHours = Math.floor(chem.TIMOUT / 3600);
          //chem.chlorTimeLeftMinutes = Math.floor((chem.TIMOUT % 3600) / 60);
          //chem.chlorTimeLeftSeconds = chem.TIMOUT % 60;
          chem.enableChlorination = (chem.SUPER === 'ON') ? true : false;
          return chem;
        }).sort(function(a, b) {
            var keyA = a.LISTORD,
              keyB = b.LISTORD;
            if (keyA < keyB) return -1;
            if (keyA > keyB) return 1;
            return 0;
          });
        // Then snapshot it to revert from an edit
        $scope.chemsBeforeEdit = angular.copy($scope.chems);
        if($scope.firstUpdate) { // The first time we update chem controllers expand the first item.
          if($scope.chems.length > 0) {
            $scope.accordionStateByObjectNames[$scope.chems[0].objnam] = {};
            $scope.accordionStateByObjectNames[$scope.chems[0].objnam].open = true;
          }
          $scope.firstUpdate = false;
        }

          // set current allow chemistry devices
        $scope.processCurrentChemList();

      });
    }
  }
]);
