﻿using System;
using System.Linq;
using System.Web.Helpers;
using System.Web.UI.WebControls.WebParts;
using System.Net.WebSockets;
using System.Threading.Tasks;

using Microsoft.Web.WebSockets;
using Microsoft.AspNet.Identity.EntityFramework;

using Newtonsoft.Json;

using SERVICE_SHARED;

namespace Pentair.Web.Api
{
    /// <summary>
    /// Websocket Client class
    /// This class is responsible for managing an open web socket.
    /// Both Client and THW web socket sessions are managed within this class.
    /// The data context is also managed at this level, creating the required
    /// data context and passing it to all used objects requiring the context are
    /// performed at this level.  Each Web Socket message that requires a data context
    /// will create it for the lifetime of the message itself.
    /// 
    /// This gives the data context a scope with the same lifetime as the unit of work
    /// involved in processing a message.  This does not include processing any responses
    /// to the message but does include processing any actions the message generates.  This
    /// may include for example, generating additional notification messages to multiple
    /// clients that have subscribed to events the message is reporting.
    /// </summary>
    public abstract partial class WebSocketClient : WebSocketHandler, IDisposable
    {
        protected virtual string EventSourceName { get { return "WebSocketClient"; } }

        #region Inactivity Timeout
        
        protected const double CONNECTION_INACTIVITY_TIMEOUT_MSEC_BEFORE_ADDCONNECTION =  35000.0; // 35 sec
        protected const double CONNECTION_INACTIVITY_TIMEOUT_MSEC_AFTER_ADDCONNECTION  = 120000.0; // 2 min
        
        protected object lockTimerInactivity = new object();
        protected System.Timers.Timer timerInactivity = new System.Timers.Timer()
        {
            AutoReset = false, // Signal once
            Interval = CONNECTION_INACTIVITY_TIMEOUT_MSEC_BEFORE_ADDCONNECTION
        };

        #endregion Inactivity Timeout

        #region Abstracts

        public abstract EndpointType TypeEndpoint { get; }

        public abstract void AddConnection();

        public abstract Task RemoveConnectionAsync();

        /// <exception cref="Exception" />
        protected abstract Task ProcessRequestAsync(
            string message
        );

        #endregion Abstracts

        #region Public Properties

        /// <summary>
        /// Can be null.
        /// </summary>
        public Connection Connection { get; protected set; }

        public int InstallationId { get; protected set; }
        public Guid UserId { get; private set; }
        public Guid ConnectionId { get { return (this.Connection != null) ? this.Connection.ConnectionId : Guid.Empty; } }

        #endregion Public Properties

        #region Constructor

        /// <exception cref="Exception" />
        public WebSocketClient(
            int installationId,
            Guid userId)
        {
            this.timerInactivity.Elapsed +=
                (object sender, System.Timers.ElapsedEventArgs e) =>
                {
                    try
                    {
                        Disconnect(
                            isHardStop: true,
                            caller: "Connections Inactivity Timeout Handler",
                            reason: "Inactivity timeout expired: " + (int)this.timerInactivity.Interval / 1000 + " sec"
                        );
                    }
                    catch (Exception ex)
                    {
                        LogManager.LogErrorAsync(
                            source: this.EventSourceName,
                            eventBriefDescription: String.Format("Disconnecting failed {0}", this.TypeEndpoint),
                            installationId: this.InstallationId,
                            userId: this.UserId,
                            connectionId: this.ConnectionId,
                            eventFullDescription:
                                String.Format("Handling inactivity timeout failed\nWebSocket State: {0}\n\n{1}",
                                    base.WebSocketContext.WebSocket.State,
                                    ExceptionManager.CustomizeException(ex).ErrorMessage(null, true)
                                )
                        );
                    }
                };

            this.Connection = null;
            this.InstallationId = installationId;
            this.UserId = userId;

        } // WebSocketClient()

        #endregion Constructor

        #region IDisposable implementation

        private volatile bool disposed = false;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);

        } // Dispose()

        protected virtual void Dispose(
            bool disposing)
        {
            if (disposed)
                return;

            disposed = true;

            if (disposing)
            {
                // Free managed objects

                lock (this.lockTimerInactivity)
                {
                    if (this.timerInactivity != null)
                    {
                        this.timerInactivity.Dispose();
                        this.timerInactivity = null;
                    }
                }

                this.Connection = null;
                this.InstallationId = -1;
                this.UserId = Guid.Empty;
            }

            // Free unmanaged objects

        } // Dispose(bool disposing)

        #endregion IDisposable implementation

        #region WebSocketHandler overrides

        /// <summary>
        /// Open event occurs when the client opens the socket.
        /// </summary>
        public override void OnOpen()
        {
            if (WebApiApplication.LogVerbose)
            {
                LogManager.LogInformationAsync(
                    source: this.EventSourceName,
                    eventBriefDescription: String.Format("CONN {0}", this.TypeEndpoint),
                    installationId: this.InstallationId,
                    userId: this.UserId,
                    connectionId: this.ConnectionId,
                    eventFullDescription:
                        String.Format("WebSocket State: {0}",
                            base.WebSocketContext.WebSocket.State
                        )
                );
            }

            if (this.TypeEndpoint == EndpointType.THW)
            {
                WebApiApplication.PerformanceCountersMgr.IncrementCounter(
                    WebApiApplication.PerformanceCountersMgr.CounterTotalConnectedThw
                );
            }
            else if (this.TypeEndpoint == EndpointType.Client)
            {
                WebApiApplication.PerformanceCountersMgr.IncrementCounter(
                    WebApiApplication.PerformanceCountersMgr.CounterTotalConnectedClient
                );
            }

            WebApiApplication.PerformanceCountersMgr.IncrementCounter(
                WebApiApplication.PerformanceCountersMgr.CounterConnectionsPerSec
            );

            lock (this.lockTimerInactivity)
            {
                this.timerInactivity.Start();
            }

        } // OnOpen()

        /// <summary>
        /// Receive message event.
        /// </summary>
        /// <param name="message"></param>
        public override void OnMessage(
            string message)
        {
            lock (this.lockTimerInactivity)
            {
                if (this.timerInactivity.Enabled)
                {
                    this.timerInactivity.Stop();
                    this.timerInactivity.Start();
                }

            } // lock (this.lockTimerInactivity)

            WebApiApplication.PerformanceCountersMgr.IncrementCounter(
                WebApiApplication.PerformanceCountersMgr.CounterRequestsPerSec
            );

            try
            {
                long ticksStart, ticksEnd;

                NativeMethods.QueryPerformanceCounter(out ticksStart);

                if (WebApiApplication.LogVerbose)
                {
                    LogManager.LogInformationAsync(
                        source: this.EventSourceName,
                        eventBriefDescription: String.Format("RCVD {0}", this.TypeEndpoint),
                        installationId: this.InstallationId,
                        userId: this.UserId,
                        connectionId: this.ConnectionId,
                        eventFullDescription: String.Format("{0}", message)
                    );
                }

                ProcessRequestAsync(message).Wait();

                NativeMethods.QueryPerformanceCounter(out ticksEnd);

                double elapsedSec = (double)(ticksEnd - ticksStart) / WebApiApplication.counterFrequency;

                WebApiApplication.PerformanceCountersMgr.SetCounterValue(
                    WebApiApplication.PerformanceCountersMgr.CounterLatencyRequest,
                    (long)(elapsedSec * 1000.0 + 0.5)
                );
            }
            catch (Exception e)
            {
                LogManager.LogErrorAsync(
                    source: this.EventSourceName,
                    eventBriefDescription: String.Format("Request processing failed {0}", this.TypeEndpoint),
                    installationId: this.InstallationId,
                    userId: this.UserId,
                    connectionId: this.ConnectionId,
                    eventFullDescription:
                        String.Format("WebSocket State: {0}\n\n{1}\n\n{2}",
                            base.WebSocketContext.WebSocket.State,
                            message,
                            ExceptionManager.CustomizeException(e).ErrorMessage(null, true)
                        )
                );

                try
                {
                    string messageId;
                    try
                    {
                        MessageType msgType = WebSocketMessage.ParseMessageType(message, this.TypeEndpoint);
                        messageId = msgType.MessageID;
                    }
                    catch
                    {
                        messageId = String.Empty;
                    }

                    ErrorResponse errorResponse = new ErrorResponse(
                        messageID: messageId,
                        response: ResponseString.BadRequest,
                        description: "Request processing failed"
                    );

                    SendMessage(
                        JsonConvert.SerializeObject(errorResponse),
                        "Send " + this.TypeEndpoint
                    );
                }
                catch (Exception e1)
                {
                    LogManager.LogErrorAsync(
                        source: this.EventSourceName,
                        eventBriefDescription: String.Format("Cannot send error to {0}", this.TypeEndpoint),
                        installationId: this.InstallationId,
                        userId: this.UserId,
                        connectionId: this.ConnectionId,
                        eventFullDescription:
                            String.Format("WebSocket State: {0}\n\n{1}",
                                base.WebSocketContext.WebSocket.State,
                                ExceptionManager.CustomizeException(e1).ErrorMessage(null, true)
                            )
                    );
                }
            }

        } // OnMessage()

        /// <summary>
        /// Close Event occurs when the web socket is closed by the client or the server.
        /// </summary>
        public override void OnClose()
        {
            OnDisconnected(bSuccess: true);

        } // OnClose()

        /// <summary>
        /// Error Event occurs when an error condition is detected.
        /// </summary>
        public override void OnError()
        {
            // Aborted by peer is not an error. Will be handled within OnClose().
            if (base.WebSocketContext.WebSocket.State != WebSocketState.Aborted)
            {
                LogManager.LogErrorAsync(
                    source: this.EventSourceName,
                    eventBriefDescription: String.Format("WebSocket Error {0}", this.TypeEndpoint),
                    installationId: this.InstallationId,
                    userId: this.UserId,
                    connectionId: this.ConnectionId,
                    eventFullDescription:
                        String.Format("WebSocket State: {0}\n\n{1}",
                            base.WebSocketContext.WebSocket.State,
                            ExceptionManager.CustomizeException(base.Error).ErrorMessage(null, true)
                        )
                );

                Disconnect(
                    isHardStop: true,
                    caller: "WebSocketClient.OnError()",
                    reason: "WebSocket error event"
                );
            }

        } // OnError()

        #endregion WebSocketHandler overrides

        #region Implementation

        /// <summary>
        /// Public method for sending a message to the client,
        /// a log entry will be created
        /// </summary>
        /// <param name="message">message to be sent</param>
        public virtual void SendMessage(
            string message,
            string action)
        {
            try
            {
                Send(message);

                if (WebApiApplication.LogVerbose)
                {
                    LogManager.LogInformationAsync(
                        source: this.EventSourceName,
                        eventBriefDescription: String.Format("SENT {0}", this.TypeEndpoint),
                        installationId: this.InstallationId,
                        userId: this.UserId,
                        connectionId: this.ConnectionId,
                        eventFullDescription: message
                    );
                }
            }
            catch (Exception e)
            {
                LogManager.LogErrorAsync(
                    source: this.EventSourceName,
                    eventBriefDescription: String.Format("Send Error {0}", this.TypeEndpoint),
                    installationId: this.InstallationId,
                    userId: this.UserId,
                    connectionId: this.ConnectionId,
                    eventFullDescription:
                        String.Format("WebSocket State: {0}\n\n{1}",
                            base.WebSocketContext.WebSocket.State,
                            ExceptionManager.CustomizeException(e).ErrorMessage(null, true)
                        )
                );
            }

        } // SendMessage()

        /// <exception cref="Exception" />
        protected virtual Task BroadcastEsbAsync(
            string destHostMachine,
            EndpointType destType,
            int installationId,
            Guid clientConnectionId,
            string serializedMessage)
        {
            if (EsbClientConnection.IsEsbConnected)
            {
                lock (EsbClientConnection.lockClientESB)
                {
                    return EsbClientConnection.ClientESB.BroadcastAsync(
                        destHostMachine,
                        destType,
                        installationId,
                        clientConnectionId,
                        serializedMessage
                    );
                }
            }
            else
            {
                string error;

                switch (destType)
                {
                    case EndpointType.THW:
                        error = String.Format("THW {0} connected to {1} and ESB is not available",
                            installationId,
                            destHostMachine
                        );
                        break;

                    case EndpointType.Client:
                        error = String.Format("Client {0} connected to {1} and ESB is not available",
                            clientConnectionId,
                            destHostMachine
                        );
                        break;

                    case EndpointType.Server:
                        
                        // Impossible to broadcast to other servers, still must handle it itself
                        EsbClientConnection.ProcessMessage(
                            isBroadcast: false,
                            destType: EndpointType.Server,
                            installationId: installationId,
                            clientConnectionId: clientConnectionId,
                            serializedMessage: serializedMessage
                        );

                        error = null;
                        break;

                    default:
                        error = "Unknown destination type: " + destType;
                        break;

                } // switch(destinationType)

                if (String.IsNullOrWhiteSpace(error))
                {
                    return Task.FromResult(0);
                }
                else
                {
                    throw new Exception(error);
                }
            }

        } // BroadcastEsbAsync()

        public virtual void Disconnect(
            bool isHardStop,
            string caller,
            string reason)
        {
            LogManager.LogInformationAsync(
                source: this.EventSourceName,
                eventBriefDescription:
                    String.Format("{0} {1} connection...",
                        (isHardStop) ? "Terminating" : "Closing",
                        this.TypeEndpoint
                    ),
                installationId: this.InstallationId,
                userId: this.UserId,
                connectionId: this.ConnectionId,
                eventFullDescription:
                    String.Format("WebSocket State: {0}\nCaller: {1}\nReason: {2}",
                    base.WebSocketContext.WebSocket.State,
                    caller,
                    reason
                )
            );

            try
            {
                // If socket is already disposed, it will throw.

                if (!isHardStop)
                {
                    Close();
                }
                else
                {
                    this.WebSocketContext.WebSocket.Abort();
                }
            }
            catch (ObjectDisposedException e)
            {
                // Not necessarily an error, we were trying to close already disposed socket
                // (no OnClose event in this case)

                LogManager.LogWarningAsync(
                    source: this.EventSourceName,
                    eventBriefDescription: "WebSocket closing error",
                    installationId: this.InstallationId,
                    userId: this.UserId,
                    connectionId: this.ConnectionId,
                    eventFullDescription:
                        ExceptionManager.CustomizeException(e).ErrorMessage(
                            errorHeader: null,
                            addStackTrace: true
                        )
                );

                // Still must be called to cleanup and update diagnostics
                OnDisconnected(bSuccess: false);
            }

        } // Disconnect()

        // 2018-01-11
        /// <summary>
        /// Cleans Connections, stops inactivity timer, updates performance counters.
        /// </summary>
        private void OnDisconnected(
            bool bSuccess)
        {
            lock (this.lockTimerInactivity)
            {
                this.timerInactivity.Stop();
            }

            if (WebApiApplication.LogVerbose)
            {
                LogManager.LogInformationAsync(
                    source: this.EventSourceName,
                    eventBriefDescription: String.Format("GONE {0}", this.TypeEndpoint),
                    installationId: this.InstallationId,
                    userId: this.UserId,
                    connectionId: this.ConnectionId,
                    eventFullDescription:
                        String.Format("WebSocket closing: {0}\nWebSocket State: {1}",
                            bSuccess ? "SUCCEEDED" : "FAILED",
                            base.WebSocketContext.WebSocket.State
                        )
                );
            }

            if (this.TypeEndpoint == EndpointType.THW)
            {
                WebApiApplication.PerformanceCountersMgr.DecrementCounter(
                    WebApiApplication.PerformanceCountersMgr.CounterTotalConnectedThw
                );
            }
            else if (this.TypeEndpoint == EndpointType.Client)
            {
                WebApiApplication.PerformanceCountersMgr.DecrementCounter(
                    WebApiApplication.PerformanceCountersMgr.CounterTotalConnectedClient
                );
            }

            RemoveConnectionAsync().Wait();

            this.Connection = null;
            this.InstallationId = -1;
            this.UserId = Guid.Empty;

        } // OnDisconnected()

        #endregion Implementation

    } // class WebSocketClient()
}
