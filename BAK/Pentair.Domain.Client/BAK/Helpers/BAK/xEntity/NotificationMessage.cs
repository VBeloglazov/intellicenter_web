﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pentair.Domain.Client.Helpers.Entity
{
    public class NotificationMessage
    {
        [Key]
        [Column(Order = 1)]
        [MaxLength(10)]
        public string MessageId { get; set; }
        [Key]
        [Column(Order = 2)]
        [MaxLength(10)]
        public string OBJTYP { get; set; }

        public string Notes { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(TypeName = "DateTime2")]
        public DateTime CreateDateTime { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [Column(TypeName = "DateTime2")]
        public DateTime UpdateDateTime { get; set; }
    }
}
