﻿using System.Collections.Generic;

namespace Pentair.Domain.Client.Helpers.Commands
{
    public class GetParamList : KeyCommand
    {
        public GetParamList(
            string messageID,
            string condition,
            List<KeyObject> objectList
            )
            : base(CommandString.GetParamList, messageID, condition, objectList)
        {
        }

    } // class GetParamList
}