﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Pentair.Domain.Client.Helpers.Commands
{
    public class UserListResponse : ParamListResponse
    {
        public UserListResponse(
            string messageID,
            string response,
            string totalCount,
            List<ParamObject> objectList
            )
            : base(CommandString.UserList, messageID, response, totalCount, objectList)
        {
        } // UserListResponse()

        /// <summary>
        /// Always returns an object, valid or not. If invalid, check the DeserializationError property value.
        /// </summary>
        public static new UserListResponse Deserialize(
            string serializedMessage)
        {
            UserListResponse command = null;

            try
            {
                command = JsonConvert.DeserializeObject<UserListResponse>(serializedMessage);
                command.IsValid = true;
            }
            catch (Exception e)
            {
                // Deserialization as UserListResponse failed

                StringBuilder sbError = new StringBuilder();

                sbError.Append("Cannot deserialize as UserListResponse. " + e.Message);

                // Try to get at least the base message data
                ParamListResponse baseCommand = ParamListResponse.Deserialize(serializedMessage);

                if (!baseCommand.IsValid)
                {
                    sbError.Append("\n" + baseCommand.DeserializationError);
                }

                command = new UserListResponse(
                    messageID: baseCommand.MessageID,
                    response: ResponseString.BadRequest,
                    totalCount: String.Empty,
                    objectList: null)
                {
                    IsValid = false,
                    DeserializationError = sbError.ToString()
                };
            }

            return command;

        } // Deserialize()

    } // class UserListResponse
}
