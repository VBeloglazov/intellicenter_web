namespace Pentair.Domain.Client.SimulatorMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class optimization : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ControlObjects",
                c => new
                    {
                        INSTALLATION = c.Int(nullable: false),
                        Revision = c.Long(nullable: false),
                        OBJNAM = c.String(nullable: false, maxLength: 16),
                        Key = c.String(nullable: false, maxLength: 16),
                        Value = c.String(maxLength: 256),
                        Deleted = c.Boolean(nullable: false),
                        ParamType = c.String(),
                        CreateDateTime = c.DateTime(nullable: false, precision: 7, storeType: "datetime2", defaultValueSql: "GETUTCDATE()"),
                        UpdateDateTime = c.DateTime(nullable: false, precision: 7, storeType: "datetime2", defaultValueSql: "GETUTCDATE()"),
                    })
                .PrimaryKey(t => new { t.INSTALLATION, t.Revision, t.OBJNAM, t.Key });
            
            CreateTable(
                "dbo.MessageLogs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Action = c.String(nullable: false),
                        Message = c.String(),
                        Timestamp = c.DateTime(nullable: false, precision: 7, storeType: "datetime2", defaultValueSql: "GETUTCDATE()"),
                        HardwareID = c.Int(nullable: false),
                        UserID = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ObjectParameterTypes",
                c => new
                    {
                        OBJTYP = c.String(nullable: false, maxLength: 16),
                        PARAM = c.String(nullable: false, maxLength: 16),
                        DEFAULT = c.String(),
                        Saved = c.Boolean(nullable: false),
                        History = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => new { t.OBJTYP, t.PARAM });
            
            CreateTable(
                "dbo.Properties",
                c => new
                    {
                        LocalPropertyId = c.Int(nullable: false, identity: true),
                        PropertyId = c.Int(nullable: false),
                        RemoteHost = c.String(maxLength: 128),
                        Configuration = c.Int(nullable: false),
                        ProtocolVersion = c.String(maxLength: 16),
                        Secret = c.String(maxLength: 128),
                        Name = c.String(maxLength: 128),
                        LastChange = c.Long(nullable: false),
                        LastNotify = c.Long(nullable: false),
                        LastAlert = c.Long(nullable: false),
                        LastHistory = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.LocalPropertyId);
            
            CreateTable(
                "dbo.Subscriptions",
                c => new
                    {
                        ClientId = c.Guid(nullable: false),
                        PropertyId = c.Int(nullable: false),
                        ObjNam = c.String(nullable: false, maxLength: 128),
                        ObjParm = c.String(nullable: false, maxLength: 128),
                        CreateDateTime = c.DateTime(nullable: false, precision: 7, storeType: "datetime2", defaultValueSql: "GETUTCDATE()"),
                    })
                .PrimaryKey(t => new { t.ClientId, t.PropertyId, t.ObjNam, t.ObjParm });
            
            CreateTable(
                "dbo.UserInvitations",
                c => new
                    {
                        InvitationId = c.Guid(nullable: false),
                        EmailAddress = c.String(),
                        InstallationId = c.Int(),
                        AccessToken = c.String(),
                        Status = c.String(),
                        EmailId = c.String(),
                        CreateDateTime = c.DateTime(nullable: false, precision: 7, storeType: "datetime2", defaultValueSql: "GETUTCDATE()"),
                        UpdateDateTime = c.DateTime(nullable: false, precision: 7, storeType: "datetime2", defaultValueSql: "GETUTCDATE()"),
                    })
                .PrimaryKey(t => t.InvitationId)
                .ForeignKey("dbo.Installations", t => t.InstallationId)
                .Index(t => t.InstallationId);
            
            CreateTable(
                "dbo.Installations",
                c => new
                    {
                        InstallationId = c.Int(nullable: false, identity: true),
                        ConnectionId = c.Guid(nullable: false),
                        Secret = c.String(maxLength: 255),
                        Name = c.String(maxLength: 255),
                        ProtocolVersion = c.String(maxLength: 128),
                        Connected = c.Boolean(nullable: false),
                        HostMachine = c.String(nullable: false),
                        LastChange = c.Long(nullable: false),
                        LastNotify = c.Long(nullable: false),
                        LastStatus = c.Long(nullable: false),
                        LastHistory = c.Long(nullable: false),
                        CreateDateTime = c.DateTime(nullable: false, precision: 7, storeType: "datetime2", defaultValueSql: "GETUTCDATE()"),
                        UpdateDateTime = c.DateTime(nullable: false, precision: 7, storeType: "datetime2", defaultValueSql: "GETUTCDATE()"),
                    })
                .PrimaryKey(t => t.InstallationId);
            
            CreateTable(
                "dbo.Associations",
                c => new
                    {
                        UserId = c.Guid(nullable: false),
                        InstallationId = c.Int(nullable: false),
                        AccessToken = c.String(),
                        CreateDateTime = c.DateTime(nullable: false, precision: 7, storeType: "datetime2", defaultValueSql: "GETUTCDATE()"),
                        UpdateDateTime = c.DateTime(nullable: false, precision: 7, storeType: "datetime2", defaultValueSql: "GETUTCDATE()"),
                    })
                .PrimaryKey(t => new { t.UserId, t.InstallationId })
                .ForeignKey("dbo.Installations", t => t.InstallationId, cascadeDelete: true)
                .Index(t => t.InstallationId);
            
            CreateTable(
                "dbo.Connections",
                c => new
                    {
                        ConnectionId = c.Guid(nullable: false, identity: true),
                        HostMachine = c.String(nullable: false),
                        InstallationId = c.Int(nullable: false),
                        UserId = c.Guid(nullable: false),
                        CreateDateTime = c.DateTime(nullable: false, precision: 7, storeType: "datetime2", defaultValueSql: "GETUTCDATE()"),
                    })
                .PrimaryKey(t => t.ConnectionId)
                .ForeignKey("dbo.Installations", t => t.InstallationId, cascadeDelete: true)
                .Index(t => t.InstallationId);
            
            CreateTable(
                "dbo.NotificationRecipients",
                c => new
                    {
                        RecipientId = c.Int(nullable: false, identity: true),
                        InstallationId = c.Int(nullable: false),
                        Name = c.String(maxLength: 255),
                        EmailAddress1 = c.String(maxLength: 255),
                        EmailAddress2 = c.String(maxLength: 255),
                        Phone1 = c.String(maxLength: 20),
                        Phone2 = c.String(maxLength: 20),
                        CreateDateTime = c.DateTime(nullable: false, precision: 7, storeType: "datetime2", defaultValueSql: "GETUTCDATE()"),
                        UpdateDateTime = c.DateTime(nullable: false, precision: 7, storeType: "datetime2", defaultValueSql: "GETUTCDATE()"),
                    })
                .PrimaryKey(t => t.RecipientId)
                .ForeignKey("dbo.Installations", t => t.InstallationId, cascadeDelete: true)
                .Index(t => t.InstallationId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserInvitations", "InstallationId", "dbo.Installations");
            DropForeignKey("dbo.NotificationRecipients", "InstallationId", "dbo.Installations");
            DropForeignKey("dbo.Connections", "InstallationId", "dbo.Installations");
            DropForeignKey("dbo.Associations", "InstallationId", "dbo.Installations");
            DropIndex("dbo.NotificationRecipients", new[] { "InstallationId" });
            DropIndex("dbo.Connections", new[] { "InstallationId" });
            DropIndex("dbo.Associations", new[] { "InstallationId" });
            DropIndex("dbo.UserInvitations", new[] { "InstallationId" });
            DropTable("dbo.NotificationRecipients");
            DropTable("dbo.Connections");
            DropTable("dbo.Associations");
            DropTable("dbo.Installations");
            DropTable("dbo.UserInvitations");
            DropTable("dbo.Subscriptions");
            DropTable("dbo.Properties");
            DropTable("dbo.ObjectParameterTypes");
            DropTable("dbo.MessageLogs");
            DropTable("dbo.ControlObjects");
        }
    }
}
