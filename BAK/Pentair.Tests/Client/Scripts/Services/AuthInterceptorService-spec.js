﻿describe('AuthInterceptorService', function () {
    var $location;
    var $q;
    var config;
    var rejection;
    var AuthInterceptorService;
    var access_token;
    var Authservice;
    var HOSTNAME;

    beforeEach(module('PentairWebClient'));

    beforeEach(inject(function (_AuthInterceptorService_, _$location_, _$q_,_AuthService_, _HOST_NAME_) {
        $location = _$location_;
        $q = _$q_;
        AuthInterceptorService = _AuthInterceptorService_;
        Authservice = _AuthService_;
        HOSTNAME = _HOST_NAME_;
    }));

    describe('Request', function () {
        beforeEach(function () {
            config = { headers: {}, url:'https://' + HOSTNAME + ':2240' };

            access_token = "A57F5BFC-2022-4227-8562-FD22B3AD64BE";
            window.sessionStorage.token = access_token;
        });

        it('should add authorization Heeader with bearer Token', function () {
            config = AuthInterceptorService.request(config);
            expect(config.headers.Authorization).toBeDefined();
            expect(config.headers.Authorization).toBe("Bearer "+access_token);
        });


        it('should NOT add authorization when no token is present', function () {
            window.sessionStorage.clear();
            config = AuthInterceptorService.request(config);
            expect(config.headers.Authorization).toBeUndefined();
        });

    });

    describe('responseError', function () {
        beforeEach(function () {
            rejection = { status: 401 }
            spyOn(Authservice, 'logOut');
        });

        it('should redirect to login page when status is 401', function () {
            AuthInterceptorService.responseError(rejection);
            expect(Authservice.logOut).toHaveBeenCalledWith();
        });

        it('should NOT redirect to login page', function () {
            rejection.status = 200;
            AuthInterceptorService.responseError(rejection);
            expect(Authservice.logOut).not.toHaveBeenCalledWith();
        });

    });
});