﻿using System.Collections.Generic;

using Newtonsoft.Json;

namespace Pentair.Domain.Client.Helpers.Commands
{
    public class HistoryReadLastTimeCommand : CommandBase
    {
        public HistoryReadLastTimeCommand(
            string messageID
            )
            : base(CommandString.ReadLastHistoryTime, messageID)
        {
        } // HistoryReadLastTimeCommand()

    } // class HistoryReadLastTimeCommand
}
