﻿-- =============================================================
--
-- Author:		Vladimir Beloglazov
-- Create date: 02/08/2018
--
-- Creates:
--
--		Table Type:			None
--
--		Stored Procedure:	PGetThwClientConnections
--
-- VB: Duplicates SP PGetThwClientConnections stored in the TFS and used by RM Services.
--
-- =============================================================

USE PathfinderIntelliCenter
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (
	SELECT * FROM sys.objects
	WHERE object_id = OBJECT_ID(N'dbo.PGetThwClientConnections')
	AND type in (N'P', N'PC')
)
DROP PROCEDURE dbo.PGetThwClientConnections
GO

-- -------------------------------------------------------------
--
-- Returns [Connections] records.
--
-- -------------------------------------------------------------
CREATE PROCEDURE dbo.PGetThwClientConnections
	 @installationId AS int
AS
BEGIN

	SET NOCOUNT OFF

	-- Result set
    SELECT * FROM dbo.Connections
    WHERE InstallationId = @installationId
    AND UserId <> CAST(0x0 AS uniqueidentifier) -- Exclude THW itself

END
GO
