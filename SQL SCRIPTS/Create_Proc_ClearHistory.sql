-- =============================================================
--
-- Author:		Vladimir Beloglazov
-- Create date: 04/17/2018
--
-- Creates:
--
--		Table Type:			None
--
--		Stored Procedure:	PClearHistory
--
-- =============================================================

USE PathfinderIntelliCenter
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (
	SELECT * FROM sys.objects
	WHERE object_id = OBJECT_ID(N'dbo.PClearHistory')
	AND type in (N'P', N'PC')
)
DROP PROCEDURE dbo.PClearHistory
GO

-- -------------------------------------------------------------
--
-- Updates [ControlObjects].
--
-- -------------------------------------------------------------
CREATE PROCEDURE dbo.PClearHistory
	@installationId	AS int,
	@timeStart		AS bigint,
	@timeEnd		AS bigint

AS
BEGIN

	SET NOCOUNT OFF

	DELETE ControlObjects
	WHERE INSTALLATION = @installationId
    AND ParamType = 'History'
	AND Revision BETWEEN @timeStart AND @timeEnd
 
END
GO
