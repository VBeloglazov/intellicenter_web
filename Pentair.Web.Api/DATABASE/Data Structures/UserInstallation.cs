﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pentair.Web.Api
{
    // TODO: VB It partially duplicates InstallationWithDetails -- better solution?

    /// <summary>
    /// UserInstallation is a DTO class used to provide pool information
    /// The access token is only valid when the DTO is used in a user context.
    /// </summary>
    /// 
    public class UserInstallation
    {
        #region Public Properties

        public int InstallationId { get; set; }
        public string PoolName { get; set; }
        public string AccessToken { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string Zip { get; set; }
        public string Phone { get; set; }
        public string OwnerName { get; set; }
        public string Email { get; set; }
        public string TimeZone { get; set; }
        public int TotalAdmins { get; set; }
        public bool OnLine { get; set; }
        public DateTime LastStatusChange { get; set; }

        //this is only here until we setup relationships correctly
        public Guid OwnerId { get; set; }

        #endregion Public Properties

        #region Construction

        public UserInstallation()
        {
            InstallationId = -1;
            AccessToken = "";
            TotalAdmins = 0;
            OnLine = false;
        
        } // UserInstallation()

        public UserInstallation(
            int Id,
            string name,
            string token,
            bool online)
        {
            InstallationId = Id;
            PoolName = name;
            AccessToken = token;
            OnLine = online;
            TotalAdmins = 0;

        } // UserInstallation(int Id, string name, string token, bool online)

        #endregion Construction

        #region Public Interface

        public bool Contains(
            string term)
        {
            string[] values = { PoolName, OwnerName, City, State, Zip, Phone, Address };

            return
                String.IsNullOrEmpty(term) ||
                values.Any(x => !String.IsNullOrEmpty(x) && x.ToLower().Contains(term.ToLower()))
            ;

        } // Contains()

        #endregion Public Interface
    }
}
