﻿var PWC = PWC || {};
PWC.directive('verticalProgressBar', [function () {
    'use strict';
    return {
        restrict: 'A',
        scope: {
            markerClass: '@',
            class: '@',
            type: '@',
            model: '=verticalProgressBar'
        },
        template: '<div class="vertical">' +
                   '<span class="{{markerClass}}" ng-style="markerPosition"></span>' +
                    '<progressbar class="{{class}}" type="{{type}}" value="model"></progressbar>' +
                  '</div>',
        controller: function ($scope, $element) {
            $scope.markerPosition = {};

            $scope.$watch('model', function () {
                getPosition();
            });

            var unWatcher = $scope.$watch(function () {
                return $element[0].children[0].offsetWidth !== 0;
            }, function (notZero) {
                if (notZero) {
                    getPosition();
                    unWatcher();
                }
            });

            function getPosition() {
                var markerHeight = $element[0].children[0].children[0].offsetHeight;
                var range = {
                    min: 0,
                    max: 100
                };
                var height = $element[0].children[0].offsetWidth;
                var value = $scope.model || 0;
                if (value < range.min) {
                    value = range.min;
                } else if (value > range.max) {
                    value = range.max;
                }
                $scope.markerPosition = {
                    left: ((value / range.max) * height) - markerHeight / 2 + "px"
                };
            };
        }
    };
}]);