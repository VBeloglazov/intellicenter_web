﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pentair.Domain.Simulator.Entity
{
    public class Schedule
    {
        [Key]
        public int ID { get; set; }
        public string Name { get; set; }
        public UInt32 LastRevision { get; set; }
        public int ListPosition { get; set; }
        public string UserName { get; set; }
        public string Days { get; set; }
        public string StartTime { get; set; }
        public string StopTime { get; set; }
        public bool Status { get; set; }
        public int ScheduleGroup { get; set; }

        // Foreign Keys
        public int TypeID { get; set; }
        public int CurrentCircuitID { get; set; }
        public int StartModeID { get; set; }
        public int StopModeID { get; set; }
        public int AffectingHeaterID { get; set; }
        public int? SystemConfigID { get; set; }
    }
}