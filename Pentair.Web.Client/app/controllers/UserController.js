﻿'use strict';
var PWC = PWC || {};
PWC.controller('UserController', [
    '$scope', 'PropertyService', 'PropertyModel',
    function ($scope, propertyService, propertyModel) {

        $scope.Property = propertyService;
        $scope.Edit = false;
        $scope.Delete = false;
        $scope.Notify = false;
        $scope.SecurityObject = "";

        $scope.EditUser = function (level) {
            if (propertyModel.EditSession === false) {
                for (var i = 0; i < propertyModel.Security.length; i++) {
                    if (propertyModel.Security[i].name === level) {
                        $scope.SecurityObject = propertyModel.Security[i].value;
                    }
                }
                $scope.Edit = true;
                propertyModel.EditSession = true;
            }
        }


        $scope.EditSave = function (email) {
            propertyService.EditUser(email, $scope.SecurityObject);
            $scope.Edit = false;
            propertyModel.EditSession = false;
        }


        $scope.CancelEdit = function () {
            propertyModel.EditSession = false;
            $scope.Edit = false;
        }


        $scope.RemoveUser = function (email, status, notify) {
            propertyService.DeleteUser(email, status, notify);
            $scope.Delete = false;
            $scope.Notify = false;
            propertyModel.EditSession = false;
        }
    }

]);