﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Pentair.Domain.Client.Helpers.Entity;

namespace Pentair.Domain.Client.Entity
{
    public class Installation
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int InstallationId { get; set; }

        public Guid ConnectionId { get; set; }

        [MaxLength(255)]
        public string Secret { get; set; }

        [MaxLength(255)]
        public string Name { get; set; }

        [MaxLength(128)]
        public string ProtocolVersion { get; set; }

        [Required]
        public Boolean Connected { get; set; }
 
        [Required]
        public string HostMachine { get; set; }
        public long LastChange { get; set; }
        public long LastNotify { get; set; }
        public long LastStatus { get; set; }
        public long LastHistory { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(TypeName = "DateTime2")]
        public DateTime CreateDateTime { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [Column(TypeName = "DateTime2")]
        public DateTime UpdateDateTime { get; set; }

        [Column(TypeName = "DateTime2")]
        public DateTime LastPinged { get; set; }

        public virtual ICollection<Association> Associations { get; set; }
        public virtual ICollection<Connection> Connections { get; set; } 
        public virtual ICollection<UserInvitation> UserInvitations { get; set; }
        public virtual ICollection<NotificationRecipient> NotificationRecipients { get; set; }
    }
}
