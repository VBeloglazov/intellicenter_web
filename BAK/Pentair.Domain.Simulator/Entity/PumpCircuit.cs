﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pentair.Domain.Simulator.Entity
{
    public class PumpCircuit
    {
        [Key]
        public int ID { get; set; }
        public string Name { get; set; }
        public UInt32 LastRevision { get; set; }
        public int Flow { get; set; }
        public int Speed { get; set; }
        public int ListPosition { get; set; }

        // Foreign Keys
        public int TypeID { get; set; }
        public int ParentPumpID { get; set; }
        public int CurrentCircuitID { get; set; }
        public int? SystemConfigID { get; set; }
    }
}