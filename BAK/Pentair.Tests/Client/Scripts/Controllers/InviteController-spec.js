﻿'use strict';
describe('Manage Users Controller', function () {
    var PropertyService;
    var $location;
    var $scope;
    var LightShowController;
    var $controller;
    var Enumerations;
    var $route;
    var PropertyModel;
    beforeEach(module('PentairWebClient'));

    beforeEach(inject(function (_$controller_, _$rootScope_, _PropertyService_, _$location_, _Enumerations_, _$route_, _PropertyModel_) {
        $scope = _$rootScope_.$new();
        PropertyService = _PropertyService_;
        PropertyModel = _PropertyModel_;
        PropertyModel.Security = [
        { value: "UFFFE", name: "ADMIN" },
        { value: "U0000", name: "GUEST" }
        ];
        $location = _$location_;
        $controller = _$controller_;
        Enumerations = _Enumerations_;
        $route = _$route_;

        _$route_.current = {
            locals: { 'installations': SpecHelper.mocks.Installations }
        };
        LightShowController = $controller("InviteController", { $scope: $scope, $route: _$route_ });
        spyOn($location, 'path');
    }));

    describe('Default Vars', function () {
        it('should have the following default variables', function () {
            expect($scope.InviteSubmitted).toBe(false);
            expect($scope.InviteGroup).toBe('UFFFE');
            expect($scope.InviteEmail).toBe("");
        });
    });
    describe('GoToManageUsersPage', function () {
        beforeEach(function () {
            spyOn($scope, 'ResetInvite');
        });
        it('should take the user the main page', function () {
            $scope.GoToManageUsersPage();
            expect($location.path).toHaveBeenCalledWith('/manageUsers');
        });
    });
    describe('ResetInvite', function () {
        beforeEach(function () {
            $scope.AddUserForm = {
                $setPristine: jasmine.createSpy()
            };
        });
        it('should take reset the vars to their default state', function () {
            $scope.InviteGroup = "ADMIN";
            $scope.InviteEmail = "Test@gmail.com";
            PropertyModel.InviteStatus = { Status: 'OK', Message: 'Test Message' };
            $scope.InviteSubmitted = true;

            $scope.ResetInvite();
            expect($scope.InviteSubmitted).toBe(false);
            expect($scope.InviteGroup).toBe('UFFFE');
            expect($scope.InviteEmail).toBe("");
            expect($scope.AddUserForm.$setPristine).toHaveBeenCalled();
        });
    });



    describe('invite User', function () {
        beforeEach(function () {
            $scope.InviteEmail = 'Valid@email.com';
            $scope.InviteGroup = 'ADMIN';
            $scope.AddUserForm = { $setPristine: jasmine.createSpy() };
            spyOn(PropertyService, 'InviteUser');
        });
        it('should call property service with email and mode', function () {
            var email = $scope.InviteEmail;
            var group = $scope.InviteGroup;
            $scope.InviteUser();
            expect(PropertyService.InviteUser).toHaveBeenCalledWith(email, group);
        });

        it('should call reset the Invite variables', function () {
            var email = $scope.InviteEmail;
            var group = $scope.InviteGroup;
            $scope.ResetInvite();
            expect($scope.InviteEmail).toBe("");
            expect(PropertyModel.InviteStatus).toEqual({});
            expect($scope.InviteGroup).toBe("UFFFE");
            expect($scope.AddUserForm.$setPristine).toHaveBeenCalled();

        });
    });

});