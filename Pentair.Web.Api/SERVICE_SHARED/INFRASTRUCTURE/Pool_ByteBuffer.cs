﻿/*
--------------------------------------------------------------------------------

    Pool_ByteBuffer.cs

    Copyright © Pentair Water Pool and Spa, Inc. 2014

--------------------------------------------------------------------------------
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SERVICE_SHARED
{
    public class Pool_ByteBuffer_Item : IDisposable
    {
        public byte[] Buffer { get; set; }
        public int Offset { get; set; }
        public int BufferSize { get; set; }

        public void Dispose()
        {
            // N/A

        } // Dispose()

        /// <summary>
        /// Sets all the pool bytes to zero.
        /// </summary>
        public void Reset()
        {
            Array.Clear(this.Buffer, this.Offset, this.BufferSize);

        } // Reset()

    } // Pool_ByteBuffer_Item

    public class Pool_ByteBuffer_Manager : IDisposable
    {
        private static string WHO_THIS = typeof(Pool_ByteBuffer_Manager).ToString();
        private static string WHO_SETBUFFER = WHO_THIS + ".SetBuffer";
        private static string WHO_FREEBUFFER = WHO_THIS + ".FreeBuffer";

        private int bufferSize;

        private object lockFreeIndexBytePool = new object();

        private byte[] bufferBytePool;
        private Stack<int> freeIndexBytePool;
        private int currentByteIndex = 0;

        #region Construction

        public Pool_ByteBuffer_Manager(
            int bufferSize,
            int capacity)
        {
            this.bufferSize = bufferSize;

            this.bufferBytePool = new byte[capacity * bufferSize];
            this.freeIndexBytePool = new Stack<int>(capacity);

        } // Pool_ByteBuffer_Manager

        #endregion Construction

        #region IDisposable implementation

        private volatile bool disposed = false;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);

        } // Dispose()

        protected virtual void Dispose(
            bool disposing)
        {
            if (this.disposed)
            {
                return;
            }
            else
            {
                this.disposed = true;
            }

            if (disposing)
            {
                // Free managed objects

                this.bufferBytePool = null;
                this.freeIndexBytePool = null;
            }

            // Free unmanaged objects
            // N/A

        } // Dispose(bool disposing)

        #endregion IDisposable implementation

        #region Public Interface

        /// <summary>
        /// Assigns a buffer from the pool to the specified Pool_ByteBuffer_Item object.
        /// </summary>
        /// <exception cref="Exception" />
        public bool SetBuffer(
            Pool_ByteBuffer_Item item)
        {
            bool bSuccess = true;

            LockWithTimeout.DoWithLock(
                lockObj: this.lockFreeIndexBytePool,
                timeoutMSec: 100,
                action: () =>
                {
                    if (this.freeIndexBytePool.Count > 0)
                    {
                        item.Buffer = this.bufferBytePool;
                        item.Offset = this.freeIndexBytePool.Pop();
                        item.BufferSize = this.bufferSize;
                    }
                    else
                    {
                        if ((this.bufferBytePool.Length - this.bufferSize) < this.currentByteIndex)
                        {
                            bSuccess = false;
                        }

                        item.Buffer = this.bufferBytePool;
                        item.Offset = this.currentByteIndex;
                        item.BufferSize = this.bufferSize;

                        this.currentByteIndex += this.bufferSize;
                    }
                }
            );

            return bSuccess;

        } // SetBuffer()

        public void FreeBuffer(
            Pool_ByteBuffer_Item item)
        {
            LockWithTimeout.DoWithLock(
                lockObj: this.lockFreeIndexBytePool,
                timeoutMSec: 100,
                action: () =>
                {
                    this.freeIndexBytePool.Push(item.Offset);

                    item.Buffer = null;
                    item.Offset = 0;
                    item.BufferSize = 0;
                }
            );

        } // FreeBuffer()

        #endregion Public Interface

    } // class Pool_ByteBuffer_Manager

    public class Pool_ByteBuffer : ObjectPool<Pool_ByteBuffer_Item>
    {
        private const string EVENT_SOURCE = "Pool_ByteBuffer";
        protected override string EventSourceName { get { return EVENT_SOURCE; } }

        private Pool_ByteBuffer_Manager bufferMgr = null;

        public int BufferSize { get; private set; }

        public override Pool_ByteBuffer_Item CreateNewObject()
        {
            // Should not normally get here. Pool_ByteBuffer_Item must be a fixed size pool,
            // nobody should try to create extra items.
            throw new Exception("(Pool_ByteBuffer) New object creation prohibited.");

        } // CreateNewObject()

        public Pool_ByteBuffer(
            string eventSourceNamePrefix,
            int bufferSize,
            int capacity,
            string caller
            )
            : base(eventSourceNamePrefix, capacity, isFixedSize: true)
        {
            this.BufferSize = bufferSize;

            this.bufferMgr = new Pool_ByteBuffer_Manager(bufferSize, capacity);

            for (int i = 0; i < capacity; i++)
            {
                Pool_ByteBuffer_Item item = new Pool_ByteBuffer_Item();
                this.bufferMgr.SetBuffer(item);

                Push(item, caller);

            } // for (int i)

        } // Pool_ByteBuffer()

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

        } // Dispose(bool disposing)

        public override void Push(
            Pool_ByteBuffer_Item item,
            string caller)
        {
            this.bufferMgr.FreeBuffer(item);
            base.Push(item, caller);

        } // Push()

        public override Pool_ByteBuffer_Item Pop(
            string caller)
        {
            Pool_ByteBuffer_Item item = base.Pop(caller);
            bool succeeded = this.bufferMgr.SetBuffer(item);
            return succeeded ? item : null;

        } // Pop()

    } // class Pool_ByteBuffer
}
