﻿using System;
using System.Linq;
using System.Timers;
using System.Windows.Forms;
using Pentair.Domain.Client.Infrastructure;
using Timer = System.Timers.Timer;


namespace Pentair.Domain.Client.BusinessLogic
{
    /// <summary>
    /// The Notification manager detects events in the
    /// system that users can elect to receive notifications for.
    /// </summary>
    public class NotificationManager
    {
        private ApiContext _data; 
        /// <summary>
        /// Constructor, set up an interval timer to check system events
        /// and generate notifications with a one minute resolution.
        /// </summary>
        public NotificationManager(ApiContext data)
        {
            _data = data;
            var eventTimer = new Timer();
            eventTimer.Elapsed += new ElapsedEventHandler(checkNotifications);
            eventTimer.Interval = 36000;
            eventTimer.Enabled = true;
        }

        // The checkNotifications event will determine which events to check for
        // and spin up worker threads to perform those tasks
        private void checkNotifications(object sender, ElapsedEventArgs e)
        {
            // Check for disconnected properties every time
            var disconnects = (from i in _data.Installations
                join n in _data.UserNotifications
                    on i.InstallationId equals n.InstallationId
                where i.Connected == false
                select new
                {
                    Installation = i.InstallationId,
                    User = n.UserId,
                    Duration = n.Duration,
                    LastUpdate = i.UpdateDateTime,
                    LastNotification = n.NotificationSent
                }).ToList();

            foreach (var evt in disconnects)
            {
                if (evt.LastUpdate < evt.LastNotification &&
                    evt.Duration < new TimeSpan(DateTime.Now.Ticks - evt.LastUpdate.Ticks))
                {
                    var notification = (from item in _data.UserNotifications
                        where item.InstallationId == evt.Installation && item.UserId == evt.User
                        select item).FirstOrDefault();
                    sendDisconnectNotification(evt.User, evt.Installation);
                }
            }
        }

        private void sendDisconnectNotification(Guid UserId, int Installation)
        {
            
        }
    }
}
