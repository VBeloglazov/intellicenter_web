﻿using System.Windows;
using System.Windows.Controls;

namespace Pentair.Desktop.Simulator.Views
{
    /// <summary>
    /// Interaction logic for UserView.xaml
    /// </summary>
    public partial class UserView : UserControl
    {
        #region Events

        public static readonly RoutedEvent InviteUserEvent =
            EventManager.RegisterRoutedEvent("InviteUser",
                RoutingStrategy.Bubble,
                typeof(RoutedEventHandler),
                typeof(RegistrationView));
        public event RoutedEventHandler RegisterProperty
        {
            add { AddHandler(InviteUserEvent, value); }
            remove { RemoveHandler(InviteUserEvent, value); }
        }

        #endregion

        #region Constructor
        /// <summary>
        /// Consttructor
        /// </summary>
        public UserView()
        {
            InitializeComponent();
        }
        #endregion

        #region Event Handlers
        public void UserButton_Click(object sender, RoutedEventArgs e)
        {
            RaiseEvent(new RoutedEventArgs(UserView.InviteUserEvent, this));
        }
        #endregion
    }
}
