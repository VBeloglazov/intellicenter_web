﻿describe('MessageProcessor', function () {

    var SocketService;
    var Enumerations;
    var PropertyModel;
    var $q;
    var deferred;
    var $filter;
    var messageProcessor;
    var $location;

    beforeEach(module('PentairWebClient'));

    beforeEach(inject(function (_$q_, _$filter_, _SocketService_, _Enumerations_, _PropertyModel_, _MessageProcessor_) {
        SocketService = _SocketService_;
        Enumerations = _Enumerations_;
        PropertyModel = _PropertyModel_;
        $filter = _$filter_;
        messageProcessor = _MessageProcessor_;
        $q = _$q_;
        deferred = $q.defer();
        PropertyModel.Clear();
        spyOn(messageProcessor, 'ProcessBody').and.callThrough();
        spyOn(messageProcessor, 'ProcessCircuit').and.callThrough();
        spyOn(messageProcessor, 'ProcessHeater').and.callThrough();
        spyOn(messageProcessor, 'ProcessSchedule').and.callThrough();
        spyOn(messageProcessor, 'ProcessPanel').and.callThrough();
        spyOn(messageProcessor, 'ProcessSensor').and.callThrough();
        spyOn(messageProcessor, 'ProcessModule').and.callThrough();
        spyOn(messageProcessor, 'ProcessRelay').and.callThrough();
        spyOn(messageProcessor, 'ProcessSecurityToken').and.callThrough();
        spyOn(messageProcessor, 'ProcessSystemConfig').and.callThrough();
        spyOn(messageProcessor, 'ProcessAirSensor').and.callThrough();
    }));

    describe('ProcessBodies', function () {
        it('should process bodies message and populate the model objects', function () {
            var message = JSON.parse(SpecHelper.JSONdata.Bodies);
            messageProcessor.ProcessNotification(message);

            expect(messageProcessor.ProcessBody.calls.count()).toEqual(4);
            expect(PropertyModel.Bodies.length).toBe(4);
            expect(Object.keys(PropertyModel.Objects).length).toBe(4);

        });
    });

    describe('ProcessSchedules', function () {
        it('should process bodies message and populate the model objects', function () {
            var message = JSON.parse(SpecHelper.JSONdata.Schedules);
            messageProcessor.ProcessNotification(message);
            expect(messageProcessor.ProcessSchedule.calls.count()).toEqual(1);

            expect(Object.keys(PropertyModel.Schedules).length).toBe(1);
        });
    });

    describe('ProcessCircuits', function () {
        it('should process Circuits message and populate the model objects', function () {
            var message = JSON.parse(SpecHelper.JSONdata.Circuits);
            messageProcessor.ProcessNotification(message);
            expect(messageProcessor.ProcessCircuit.calls.count()).toEqual(6);

            expect(PropertyModel.Circuits.length).toBe(6);
            expect(Object.keys(PropertyModel.Objects).length).toBe(6);
            expect(PropertyModel.Circuits[0].OBJNAM).toBe('C1355');

        });
    });


    describe('ProcessHeaters', function () {
        it('should process Heaters message and populate the model objects', function () {
            var message = JSON.parse(SpecHelper.JSONdata.Heaters);

            PropertyModel.Objects = {
                "B1000": {
                    OBJNAM: "B1000",
                    SNAME: "Pool",
                    FILTER: "C1355",
                    TEMP: "75^F",
                    LOTMP: "72^F",
                    HITMP: "78^F",
                    OBJTYP: "BODY",
                    SUBTYP: "POOL",
                    HEATER: "H1000",
                    HEATERS: { HEATER: jasmine.any(Object), GENERIC: jasmine.any(Object), SECONDARY: jasmine.any(Object) }
                },
                "B1001": {
                    OBJNAM: "B1001",
                    SNAME: "Spa",
                    FILTER: "C1356",
                    TEMP: "108^F",
                    LOTMP: "100^F",
                    HITMP: "109^F",
                    OBJTYP: "BODY",
                    SUBTYP: "SPA",
                    HEATER: "H1001",
                    HEATERS: { HEATER: jasmine.any(Object), GENERIC: jasmine.any(Object), SECONDARY: jasmine.any(Object) }
                }
            };

            messageProcessor.ProcessNotification(message);
            expect(messageProcessor.ProcessHeater.calls.count()).toEqual(2);

            expect(PropertyModel.Heaters.length).toBe(2);
            expect(Object.keys(PropertyModel.Objects).length).toBe(4);

            expect(PropertyModel.Heaters[0].OBJNAM).toBe('H1000');

        });
    });


    describe('ProcessAir Sensors', function () {
        it('should process Air sensors message and populate the model objects', function () {
            var message = JSON.parse(SpecHelper.JSONdata.AirSensors);

            messageProcessor.ProcessNotification(message);
            expect(messageProcessor.ProcessAirSensor.calls.count()).toEqual(1);

            expect(PropertyModel.AirSensors.length).toBe(1);
            expect(Object.keys(PropertyModel.Objects).length).toBe(1);

            expect(PropertyModel.AirSensors[0].OBJNAM).toBe('A1000');

        });
    });


    describe('ProcessNotificaion', function () {
        it('should turn Circuit OFF after processesing notification', function () {
            //process circuits
            var message = JSON.parse(SpecHelper.JSONdata.Circuits);
            messageProcessor.ProcessNotification(message);

            //handle notification
            expect(PropertyModel.Objects['C5437'].STATUS).toBe("ON");
            message = JSON.parse(SpecHelper.JSONdata.Notification);
            messageProcessor.ProcessNotification(message);
            expect(PropertyModel.Objects['C5437'].STATUS).toBe("OFF");

        });
    });


    describe('ProcessUserList', function () {
        it('should have list of users after being called', function () {
            //process user list
            var message = JSON.parse(SpecHelper.JSONdata.UserList);
            messageProcessor.ProcessUserListMessage(message);

            //handle response
            expect(Object.keys(PropertyModel.Users).length).toBe(1);
        });
    });
});
