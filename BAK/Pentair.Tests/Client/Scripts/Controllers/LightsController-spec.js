﻿'use strict';
describe('DashboardController', function () {
    var PropertyService;
    var $location;
    var $scope;
    var LightShowController;
    var $controller;
    beforeEach(module('PentairWebClient'));

    beforeEach(inject(function (_$controller_, _$rootScope_, _PropertyService_, _$location_) {
        $scope = _$rootScope_.$new();
        PropertyService = _PropertyService_;
        $location = _$location_;
        $controller = _$controller_;

        LightShowController = $controller("LightsController", { $scope: $scope });
        spyOn($location, 'path');

    }));

    describe('ToggleIntelliHelp', function () {
        it('should set  the true if false is provided', function () {
            $scope.IntelliHelp = true;
            $scope.ToggleIntelliHelp();
            expect($scope.IntelliHelp).toBe(false);
        });
        it('should set  the true if false is provided', function () {
            $scope.IntelliHelp = false;
            $scope.ToggleIntelliHelp();
            expect($scope.IntelliHelp).toBe(true);
        });
    });


    describe('ToggleMagicHelp ', function () {
        it('should set  the true if false is provided', function () {
            $scope.MagicHelp = true;
            $scope.ToggleMagicHelp();
            expect($scope.MagicHelp).toBe(false);
        });
        it('should set  the true if false is provided', function () {
            $scope.MagicHelp = false;
            $scope.ToggleMagicHelp();
            expect($scope.MagicHelp).toBe(true);
        });
    });

    describe('GoToDashboard', function () {
        it('should take the user the main page', function () {
            $scope.GoToDashboard();
            expect($location.path).toHaveBeenCalledWith('/property');
        });
    });
});