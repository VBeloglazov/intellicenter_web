-- =============================================================
--
-- Author:		Vladimir Beloglazov
-- Create date: 03/08/2018
--
-- Creates:
--
--		Table Type:			None
--
--		Stored Procedure:	PAddUpdateAssociation
--
-- =============================================================

USE PathfinderIntelliCenter
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (
	SELECT * FROM sys.objects
	WHERE object_id = OBJECT_ID(N'dbo.PAddUpdateAssociation')
	AND type in (N'P', N'PC')
)
DROP PROCEDURE dbo.PAddUpdateAssociation
GO

-- -------------------------------------------------------------
--
-- Updates [Associations] and [UserInvitations] records.
--
-- -------------------------------------------------------------
CREATE PROCEDURE dbo.PAddUpdateAssociation
	 @userId			AS uniqueidentifier
	,@installationId	AS int
	,@accessToken		AS nvarchar(max)
	,@invitationId		AS uniqueidentifier
AS
BEGIN

	SET NOCOUNT OFF

	BEGIN TRANSACTION

	BEGIN TRY
	
		-- Update existing [Associations] record
        UPDATE dbo.Associations
		SET AccessToken = @accessToken
        WHERE UserId = @userId
        AND InstallationId = @installationId

		-- Create new [Associations] record
        INSERT INTO Associations (UserId, InstallationId, AccessToken)
        SELECT @userId, @installationId, @accessToken
        WHERE NOT EXISTS (
			SELECT 1 FROM Associations
	        WHERE UserId = @userId
	        AND InstallationId = @installationId
        )

        -- Delete accepted & successfully processed [UserInvitations] record
        IF @invitationId <> CAST(0x0 AS uniqueidentifier)
        BEGIN
			DELETE dbo.UserInvitations
            WHERE InvitationId = @invitationId
		END

	END TRY
	BEGIN CATCH

		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION

		DECLARE @err_message nvarchar(255)
		SET @err_message = ERROR_MESSAGE()
		RAISERROR (@err_message, 11, 1) -- severity 11-19 will cause returning exception to caller

	END CATCH

	IF @@TRANCOUNT > 0
		COMMIT TRANSACTION

END
GO
