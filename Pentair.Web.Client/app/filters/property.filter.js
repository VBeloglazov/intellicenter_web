var PWC = PWC || {};


PWC.filter('objectsByName', [function() {
    return function(circuits, objnam) {
        if (!circuits || !objnam) {
            return circuits;
        }

        var filteredCircuits = [];
        angular.forEach(circuits, function(circuit) {
            if (objnam && circuit.OBJNAM === objnam) {
                filteredCircuits.push(circuit);
            }
        });
        return filteredCircuits;
    };
}]);

PWC.filter('moduleByParent', [function() {
    return function(modules, parentName) {
        if (!modules || !parentName) {
            return modules;
        }

        var filteredModules = [];
        angular.forEach(modules, function(module) {
            if (angular.isDefined(parentName) && module.PARENT === parentName) {
                filteredModules.push(module);
            }
        });
        return filteredModules;
    };
}]);

PWC.filter('relayByPanel', [function() {
    return function(relays, panelName) {
        if (!relays || !panelName) {
            return relays;
        }
        var filteredRelays = [];
        angular.forEach(relays, function(relay) {
            if (angular.isDefined(panelName) && relay.PANEL === panelName) {
                filteredRelays.push(relay);
            }
        });
        return filteredRelays;
    };
}]);

PWC.filter('objectsByCharSet', [function() {
    return function(circuits, charSet, parity) {
        parity = parity || '';

        if (!circuits || !charSet) {
            return circuits;
        }
        var filteredCircuits = [];
        angular.forEach(circuits, function(circuit) {
            if (circuit.SUBTYP !== 'BODY' && circuit.SHOMNU) {
                var containsAllCharSet = true;
                var i = charSet.length;
                while(i--) {
                    var currentCharSetChar = charSet.charAt(i);
                    containsAllCharSet = (circuit.SHOMNU.toLowerCase().indexOf(currentCharSetChar) > -1);
                    if(!containsAllCharSet) {
                        break;
                    }
                }
                if(containsAllCharSet) {
                    if (!parity) {
                        filteredCircuits.push(circuit);
                    } else {
                        switch (parity.toLowerCase()) {
                            case 'even':
                                if (circuit.LISTORD % 2 === 0) {
                                    filteredCircuits.push(circuit);
                                }
                                break;
                            case 'odd':
                                if (circuit.LISTORD % 2 !== 0 || circuit.LISTORD % 2 == NaN) {
                                    filteredCircuits.push(circuit);
                                }
                                break;
                        }
                    }
                }
            }
        });
        return filteredCircuits.sort(function(a, b) {
            return a.LISTORD - b.LISTORD;
        });
    };

}]);

PWC.filter('objectsByCharSet_Parity', [function () {
    return function (circuits, charSet, parity) {
        parity = parity || '';

        if (!circuits || !charSet) {
            return circuits;
        }
        var sortedCircuits = [];
        var filteredCircuits = [];
        angular.forEach(circuits, function (circuit) {
            if (circuit.SUBTYP !== 'BODY' && circuit.SHOMNU) {
                var containsAllCharSet = true;
                var i = charSet.length;
                while (i--) {
                    var currentCharSetChar = charSet.charAt(i);
                    containsAllCharSet = (circuit.SHOMNU.toLowerCase().indexOf(currentCharSetChar) > -1);
                    if (!containsAllCharSet) {
                        break;
                    }
                }
                if (containsAllCharSet) {                    
                    sortedCircuits.push(circuit);
                }
            }
        });

        // filter based on the parity
        var counter = 0
        angular.forEach(sortedCircuits, function (circuit) {
            if (parity == 'even' && (counter % 2) == 0)
                filteredCircuits.push(circuit)
            if (parity == 'odd' && (counter % 2) != 0)
                filteredCircuits.push(circuit)
            counter++
        })
        
        return filteredCircuits.sort(function (a, b) {
            return a.LISTORD - b.LISTORD;
        });
    };

}]);

PWC.filter('epochTimeToReadableDate', [function() {
    return function(epochTime, systemTime) {
        if(!systemTime || systemTime === '--') {
            return '--';
        }
        var duration = moment.duration(moment(systemTime, 'HH,mm,ss').diff(moment.unix(parseInt(epochTime))));
        var hours = duration.asHours();
        if(hours < 0.0) {
            return '0 MIN AGO';
        }
        if(hours < 1.0) {
            // display like 34 min ago
            return Math.floor(duration.asMinutes()) + ' MIN AGO';
        }
        if(hours < 2.0 && hours > 1.0) {
            return Math.floor(hours) + ' HOUR AGO';
        }
        if(hours < 24.0) {
            // display like 4 hour(s) ago
            return Math.floor(hours) + ' HOURS AGO';
        }
        if(hours > 24.0) {
            // display like
            // 05/02/16
            // 2:47pm
            return moment.unix(parseInt(epochTime)).format('MM/DD/YY h:mma');
        }
    }
}]);


PWC.filter('objectsBySection', [function() {
    return function(circuits, sections, parity) {
        parity = parity || '';
        if (!angular.isArray(sections)) {
            sections = [sections];
        }
        if (!circuits || !sections[0]) {
            return circuits;
        }
        var filteredCircuits = [];
        angular.forEach(circuits, function(circuit) {
            if (circuit.SUBTYP !== 'BODY' && circuit.SHOMNU) {
                angular.forEach(sections, function(section) {
                    if (circuit.SHOMNU.toLowerCase().indexOf(section) > -1) {
                        if (!parity) {
                            filteredCircuits.push(circuit);
                        } else {
                            switch (parity.toLowerCase()) {
                                case 'even':
                                    if (circuit.LISTORD % 2 === 0) {
                                        filteredCircuits.push(circuit);
                                    }
                                    break;
                                case 'odd':
                                    if (circuit.LISTORD % 2 !== 0 || circuit.LISTORD % 2 == NaN) {
                                        filteredCircuits.push(circuit);
                                    }
                                    break;
                            }
                        }
                    }
                });
            }
        });
        return filteredCircuits.sort(function(a, b) {
            return a.LISTORD - b.LISTORD;
        });
    };

}]);


PWC.filter('objectsBySection_Parity', [function () {
    return function (circuits, sections, parity) {
        parity = parity || '';
        if (!angular.isArray(sections)) {
            sections = [sections];
        }
        if (!circuits || !sections[0]) {
            return circuits;
        }
        var sectionCircuits = [];
        var sortedCircuits = [];
        var filteredCircuits = [];
        angular.forEach(circuits, function (circuit) {
            if (circuit.SUBTYP !== 'BODY' && circuit.SHOMNU) {
                angular.forEach(sections, function (section) {
                    if (circuit.SHOMNU.toLowerCase().indexOf(section) > -1) {                        
                        sectionCircuits.push(circuit);
                    }
                });
            }
        });
        // Order per name now
        sortedCircuits = sectionCircuits.sort(function (a, b) {
            return a.SNAME - b.SNAME;
        });
        // filter based on the parity
        var counter = 0
        angular.forEach(sortedCircuits, function (circuit) {
            if (parity == 'even' && (counter % 2) == 0)
                filteredCircuits.push(circuit)
            if (parity == 'odd' && (counter % 2) != 0)
                filteredCircuits.push(circuit)
            counter++
        })

        return filteredCircuits
    };

}]);

PWC.filter('objectsBySubType', [function() {
    return function(objects, subTypes) {
        var filteredObjects = [];
        if (!angular.isArray(subTypes)) {
            subTypes = [subTypes];
        }
        if (!objects || !subTypes) {
            return objects;
        }

        angular.forEach(objects, function(obj) {
            if (subTypes.indexOf(obj.SUBTYP) > -1) {
                filteredObjects.push(obj);
            }
        });
        return filteredObjects;
    };
}]);



PWC.filter('objectsByParamValue', [function() {
    return function(objects, params, value) {
        var filteredObjects = [];
        if (!angular.isArray(params)) {
            params = [params];
        }
        if (!objects || !params || !value) {
            return objects;
        }

        angular.forEach(objects, function(obj) {
            angular.forEach(params, function(param) {
                if (obj[param] == value) {
                    filteredObjects.push(obj);
                }
            });
        });
        return filteredObjects;
    };
}]);

PWC.filter('lightGroupCircuits', [function() {
    return function(circuits) {
        if (!PWC.isEmptyObject(circuits)) {
            var filteredCircuits = [];
            angular.forEach(circuits, function(circuit) {
                if (circuit.SUBTYP === 'LITSHO') {
                    filteredCircuits.push(circuit);
                }
            });
            return filteredCircuits;
        } else {
            return circuits;
        };
    };
}]);


// circuit groups
PWC.filter('GroupCircuits', [function () {
    return function (circuits) {
        if (!PWC.isEmptyObject(circuits)) {
            var filteredCircuits = [];
            angular.forEach(circuits, function (circuit) {
                if (circuit.SUBTYP === 'CIRCGRP') {
                    filteredCircuits.push(circuit);
                }
            });
            return filteredCircuits;
        } else {
            return circuits;
        };
    };
}]);


// circuit groups
PWC.filter('GroupCircuits_Parity', [function () {
    return function (circuits, parity) {

        parity = parity || '';

        var sortedCircuits = [];
        var filteredCircuits = [];
        if (!PWC.isEmptyObject(circuits)) {
            
            angular.forEach(circuits, function (circuit) {
                if (circuit.SUBTYP === 'CIRCGRP') {
                    sortedCircuits.push(circuit);
                }
            });            
        }


        // filter based on the parity
        var counter = 0
        angular.forEach(sortedCircuits, function (circuit) {
            if (parity == 'even' && (counter % 2) == 0)
                filteredCircuits.push(circuit)
            if (parity == 'odd' && (counter % 2) != 0)
                filteredCircuits.push(circuit)
            counter++
        })

        return filteredCircuits.sort(function (a, b) {
            return a.SNAME - b.SNAME;
        });
    };
}]);


// all pump circuits : include (circuits,group circuits, light circuits).
PWC.filter('AllPumpCircuits', [function () {
    return function (circuits, groups) {

        //console.log("------ALL CIRCUITS ---- ");
        if (!PWC.isEmptyObject(circuits)) {
            var filteredCircuits = [];

            angular.forEach(circuits, function (circuit) {
                if (circuit.OBJNAM) {
                    if (circuit.OBJNAM.substring(0, 1) == "X") { // REMOVING SPECIAL CIRCUITS, SUCH AS: X0039 = 'Pump Speed+'... ETC                        
                        
                        switch (circuit.OBJNAM) { //  Adding special circuits "X___" as follow
                            case 'X0034':   // Heat Pump
                            case 'X0035':   // UltraTemp
                            case 'X0036':   // Hybrid
                            //case 'X0043':   // Cleaner    : REMOVED FROM LIST
                            case 'X0044':   // Pool Heater
                            case 'X0045':   // Spa Heater
                            case 'X0046':   // Freeze
                            case 'X0047':   // Pool/Spa
                            //case 'X0050':   // Cleaners   : REMOVED FROM LIST
                            case 'X0051':   // Heater
                            case 'X0052':   // Solar                                
                                //console.log("INCLUDED SPECIAL CIRCUITS:  OBJNAM(" + circuit.OBJNAM + ")  HNAME(" + circuit.HNAME + ")  SNAME(" + circuit.SNAME + ") ");
                                filteredCircuits.push(circuit); // add to list
                                break;
                            default:
                                // remove "Excluded"  these special circuits
                               // console.log("EXCLUDED  SPECIAL CIRCUITS:  OBJNAM(" + circuit.OBJNAM + ")  HNAME(" + circuit.HNAME + ")  SNAME(" + circuit.SNAME + ") ");
                                break;
                        }

                    }
                    else {   // add all other than "X_
                        filteredCircuits.push(circuit); // add to list
                    }   
                }
            });

            // adding group circuits : 1 group circuits, 2 light group circuits
            angular.forEach(groups, function (circuit) {
                if ((circuit.SUBTYP === 'CIRCGRP') || (circuit.SUBTYP === 'LITSHO')){
                    filteredCircuits.push(circuit); // add to list
                }
            });
            
            return filteredCircuits;         
            
        } else {
            return circuits;
        };
    };
}]);

PWC.filter('lightCircuits', [function() {
    return function(circuits) {
        var filteredCircuits = [];
        angular.forEach(circuits, function(circuit) {
            if (circuit.SUBTYP) {
                if (circuit.SUBTYP === 'LIGHT' ||
                    circuit.SUBTYP === 'MAGIC1' ||
                    circuit.SUBTYP === 'MAGIC2' ||
                    circuit.SUBTYP === 'CLRCASC' ||
                    circuit.SUBTYP === 'INTELLI' ||
                    circuit.SUBTYP === 'SAML' ||
                    circuit.SUBTYP === 'COLORW' ||
                    circuit.SUBTYP === 'PHOTON' ||
                    circuit.SUBTYP === 'DIMMER' ||
                    circuit.SUBTYP === 'GLOW' ||
                    circuit.SUBTYP === 'GLOWT') {
                    filteredCircuits.push(circuit);
                }
            }
        });
        return filteredCircuits;
    };
}]);


PWC.filter('programmableCircuits', [function() {
    return function(circuits) {
        if (!PWC.isEmptyObject(circuits)) {
            var filteredCircuits = [];
            angular.forEach(circuits, function(circuit) {
                if (circuit.OBJTYP === 'CIRCUIT' && circuit.SHOMNU && circuit.SHOMNU.toLowerCase().indexOf('e') > -1)
                    filteredCircuits.push(circuit);
            });
            return filteredCircuits;
        } else {
            return circuits;
        }
    };
}]);

PWC.filter('remoteCircuits', [function() {
    return function(circuits) {
        if (!PWC.isEmptyObject(circuits)) {
            var filteredCircuits = [];
            angular.forEach(circuits, function(circuit) {
                if (circuit.OBJTYP === 'CIRCUIT' && circuit.SHOMNU && circuit.SHOMNU.toLowerCase().indexOf('r') > -1)
                    filteredCircuits.push(circuit);
            });
            return filteredCircuits;
        } else {
            return circuits;
        }
    };
}]);

PWC.filter('coolingCapableHeater', [function() {
    return function(heaters) {
        if (!PWC.isEmptyObject(heaters)) {
            var coolingCapable = false;
            angular.forEach(heaters, function(heater) {
                // only if cooling is enabled
                if (heater.SHOMNU) {
                    if (heater.SHOMNU.indexOf('e') >= 0) {
                        coolingCapable = true;
                    }
                }
            });
            return coolingCapable;
        }
    }
}]);

PWC.filter('coolingAvailable', [function () {
    return function (heaters) {
        if (!PWC.isEmptyObject(heaters)) {
            var coolingAvailable = false;
            angular.forEach(heaters, function (heater) {
                // only if cooling is enabled
                if (heater.SHOMNU) {
                    if (heater.SHOMNU.indexOf('e') >= 0) {
                        coolingAvailable = true;
                    }
                }
            });
            return coolingAvailable;
        }
    }
}]);

PWC.filter('heatingCapable', [function () {
    return function (heaters) {
        if (!PWC.isEmptyObject(heaters)) {
            var heatCapable = true; // all heaters should be able to heat!!
            /*
            angular.forEach(heaters, function (heater) {
                // only if cooling is enabled
                if (heater.SHOMNU) {
                    if (heater.SHOMNU.indexOf('h') >= 0) {
                        heatCapable = true;
                    }
                }else{
                    heatCapable = true; // by default any heater must be able to heat!!
                }
            });
            */
            return heatCapable;
        }
    }
}]);
PWC.filter('translateTemperature', ["PropertyModel", function(PropertyModel) {
    return function(temp, UoM) {
        var result = {
            NUMBER: NaN,
            TEMPERATURE: NaN,
            UNIT: 'C',
            ALL: ''
        };

        var preference = PropertyModel.Objects._5451;

        UoM = UoM || ((preference && preference.MODE) ? preference.MODE : "METRIC");

        if (temp && UoM) {
            var numeric = Number(temp) || Number(temp.replace(/\D/g, ''));

            if (UoM.toUpperCase() === "ENGLISH") {
                result.NUMBER = Math.round((numeric * 9) / 5 + 32);
                result.TEMPERATURE = result.NUMBER + '°';
                result.UNIT = 'F';
            } else {
                result.NUMBER = Math.round(numeric);
                result.TEMPERATURE = result.NUMBER + '°';
            }
        } else {
            result.NUMBER = '--';
            result.TEMPERATURE = result.NUMBER + '°';
            if (UoM.toUpperCase() === "ENGLISH") {
                result.UNIT = 'F';
            }
        }

        if (result.TEMPERATURE && result.UNIT) {
            result.ALL = result.TEMPERATURE + result.UNIT;
        }

        return result;
    };
}]);


PWC.filter('connectionStatus', [function() {
    return function(code) {
        switch (code) {
            case 0:
                return "Connecting";
            case 1:
                return "Connected";
            case 2:
                return "Closing";
            case 3:
                return "Closed";
            default:
                return "";
        }
    };
}]);

PWC.filter('scheduleDays', [function() {
    return function(days) {
        if (days) {
            days = days.replace('1', '');
            if (angular.equals(days.split('').sort(), ["A", "F", "M", "R", "T", "U", "W"]))
                return "Every Day";
            var ordered = "";
            if (days.indexOf('M') > -1) { ordered = ordered + 'Mon '; }
            if (days.indexOf('T') > -1) { ordered = ordered + 'Tue '; }
            if (days.indexOf('W') > -1) { ordered = ordered + 'Wed '; }
            if (days.indexOf('R') > -1) { ordered = ordered + 'Thu '; }
            if (days.indexOf('F') > -1) { ordered = ordered + 'Fri '; }
            if (days.indexOf('A') > -1) { ordered = ordered + 'Sat '; }
            if (days.indexOf('U') > -1) { ordered = ordered + 'Sun '; }
            ordered = ordered.split(' ').join(", ").toString();
            return ordered.substr(0, ordered.length - 2);
        }
        return "No Days";
    };
}]);

PWC.filter('scheduleType', [function() {
    return function(days) {
        var value = '';
        if (days) {
            switch (days) {
                case 'ON':
                    value = 'One Time Only';
                    break;
                case 'OFF':
                    value = 'Weekly';
                    break;
            }
        }
        return value;
    };
}]);


PWC.filter('translateWeatherTime', ['PropertyModel', function(PropertyModel) {
    return function(time, zone) {
        if (time && zone) {
            var localTime = new Date(time);
            var utc = localTime.getTime() + localTime.getTimezoneOffset() * 60000;
            var newTime = new Date(utc + (3600000 * (parseInt(zone)+1)));
            var system = PropertyModel.Objects._C10C;
            var mode = ((system) ? system.CLK24A : "AMPM");

            if (mode === "AMPM") {
                var hours24 = newTime.getHours();
                var hours = ((hours24 + 11) % 12) + 1;
                var amPm = hours24 > 11 ? 'pm' : 'am';
                var minutes = newTime.getMinutes().toString();
                for (var i = minutes.length; i < 2; i++) {
                    minutes = "0" + minutes;
                }
                return hours + ':' + minutes + amPm;
            } else {
                var minutes = newTime.getMinutes().toString();
                for (var i = minutes.length; i < 2; i++) {
                    minutes = "0" + minutes;
                }
                return newTime.getHours() + ':' + minutes
            }
        }
        return '--';
    }
}]);


PWC.filter('translateWeatherDate', ['PropertyModel', function(PropertyModel) {
    return function(date) {
        if (date) {
            var newDate = new Date(date);
            return newDate.getMonth() + 1 + '/' + newDate.getDate() + '/' + newDate.getFullYear();
        }
        return '--';
    }
}]);


PWC.filter('translateTime', ['PropertyModel', function(PropertyModel) {
    return function(time) {
        if (time) {
            var timeElements = time.split(',');
            time = timeElements[0] + ':' + timeElements[1];
            var system = PropertyModel.Objects._C10C;
            var mode = ((system) ? system.CLK24A : "AMPM");

            if (mode === "AMPM") {
                time = time.replace(':', '');
                var hours24 = parseInt(time.substring(0, 2));
                var hours = ((hours24 + 11) % 12) + 1;
                var amPm = hours24 > 11 ? 'pm' : 'am';
                var minutes = time.substring(2);

                return hours + ':' + minutes + amPm;
            } else {
                return time;
            }
        }
        return '--';
    };
}]);

PWC.filter('convertTime', ['PropertyModel', function(PropertyModel) {
    return function(time) {
        var system = PropertyModel.Objects._C10C;
        var mode = ((system) ? system.CLK24A : "AMPM");
        if (mode === "AMPM") {
            var offset = 0;
            var index = time.indexOf("pm");
            if (index > 0) {
                offset = 12;
            } else {
                index = time.indexOf("am");
            }
            time = time.slice(0, index);
            var components = time.split(":");
            hours = Number(components[0]);
            if (hours < 12) {
                hours = hours + offset;
            }
            return hours + ',' + components[1] + ',00';
        }
        var components = time.split(":");
        return components[0] + ',' + components[1] + ',00';
    }
}]);

PWC.filter('translateDate', [function() {
    return function(date) {
        if (date) {
            var values = date.split(",")
            return values[0] + '/' + values[1] + '/' + values[2]
        }
        return '--';
    }
}]);

PWC.filter('locationCityState', [function() {
    return function(locale) {
        var result = { City: '', State: '' };
        if (locale) {
            var parms = locale.split(',');
            result.City = parms[0].trim() || '';
            result.State = parms[1].trim() || '';
        }
        return result;
    };
}]);

PWC.filter('userAdmin', [function() {
    return function(users) {
        if (!PWC.isEmptyObject(users)) {
            var filteredUsers = [];
            angular.forEach(users, function(user) {
                if (user.level === 'ADMIN') {
                    filteredUsers.push(user);
                }
            });
            return filteredUsers;
        } else {
            return {};
        }
    };
}]);


PWC.filter('selectedUser', [function() {
    return function(users, userId) {
        if (!PWC.isEmptyObject(users)) {
            var selectedUser = null;
            for (var i = 0; i < users.length; i++) {
                var item = users[i];
                if (item.id.toString() === userId.toString()) {
                    selectedUser = item;
                }
            }
            return selectedUser;
        } else {
            return null;
        }
    };
}]);


PWC.filter('dropdownSelections', [
    'Enumerations',
    function(enumerations) {
        return function(list) {
            if (list) {
                return enumerations.CLOCK_LISTS[list];
            } else {
                console.warn('TODO');
            }
        };
    }
]);


PWC.filter('heatModes', ['Enumerations', function(enumerations) {
    return function(body) {
        if (!PWC.isEmptyObject(body)) {
            if (!PWC.isEmptyObject(body.HEATERS.SECONDARY.HEATER)) {
                return enumerations.HEAT_MODES[body.HEATERS.SECONDARY.HEATER.SUBTYP];
            } else {
                return enumerations.HEAT_MODES.GENERIC;
            }
        }
        return [];
    };
}]);


PWC.filter('currentHeatMode', ['$filter', function($filter) {
    return function(body, valueField, heatModes) {
        heatModes = heatModes || $filter('heatModes')(body);
        var status = "";
        if (!PWC.isEmptyObject(body) && heatModes) {

            switch (body.HEATER) {
                case body.HEATERS.GENERIC.HEATER.OBJNAM:
                    status = "GENERIC";
                    break;
                case body.HEATERS.SECONDARY.HEATER.OBJNAM:
                    status = "ONLY";
                    break;
                case body.HEATERS.VIRTUAL.OBJNAM:
                    if ('1' === body.HEATERS.SECONDARY.HCOMBO.LISTORD.toString() && '2' === body.HEATERS.GENERIC.HCOMBO.LISTORD.toString())
                        status = "PREF";
                    break;
                case "00000":
                case "000FF":
                    status = 'NONE';
                    break;
            }

            for (var i = 0; i < heatModes.length; i++) {
                if (status === heatModes[i][valueField || 'value'])
                    return heatModes[i];
            }
        }
        return null;
    };
}]);


PWC.filter('modeToHeater', [function() {
    return function(body, heatMode) {
        var heater = "";
        if (!PWC.isEmptyObject(body) && heatMode) {
            switch (heatMode) {
                case "NONE":
                    heater = '000FF';
                    break;
                case "GENERIC":
                    heater = body.HEATERS.GENERIC.HEATER.OBJNAM;
                    break;
                case "PREF":
                    heater = body.HEATERS.VIRTUAL.OBJNAM;
                    break;
                case "ONLY":
                    heater = body.HEATERS.SECONDARY.HEATER.OBJNAM;
                    break;
            }
        }
        return heater;

    };
}]);




PWC.filter('intToWord', [function() {
    return function(numericValue) {
        var units = new Array("Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen");
        var tens = new Array("Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety");
        var theword = "";
        var started;
        if (numericValue > 999) return "Lots";
        if (numericValue.toString() === '0') return units[0];
        for (var i = 9; i >= 1; i--) {
            if (numericValue >= i * 100) {
                theword += units[i];
                started = 1;
                theword += " hundred";
                if (numericValue !== i * 100) theword += " and ";
                numericValue -= i * 100;
                i = 0;
            }
        }

        for (var x = 9; x >= 2; x--) {
            if (numericValue >= x * 10) {
                theword += (started ? tens[x - 2].toLowerCase() : tens[x - 2]);
                started = 1;
                if (numericValue !== x * 10) theword += "-";
                numericValue -= x * 10;
                x = 0;
            }
        }

        for (var y = 1; y < 20; y++) {
            if (numericValue === y) {
                theword += (started ? units[y].toLowerCase() : units[y]);
            }
        }
        return theword;
    };
}]);

PWC.filter('insertEmail', [function() {
    return function(Phrase, options) {
        return Phrase.replace("{-Email-}", options.email);
    };
}]);

PWC.filter('lightActionOptions', ['Enumerations', function(enumerations) {
    return function(light) {
        var usages = light.USAGE ? light.USAGE.split('') : [];
        var options = [];
        angular.forEach(usages, function(usageFlag) {
            var option = enumerations.lightCapabilities[usageFlag];
            if (option && !option.color) {
                options.push(option);
            }
        });
        return options;
    };
}]);

PWC.filter('lightColorOptions', ['Enumerations', function(enumerations) {
    return function(light) {
        var usages = light.USAGE ? light.USAGE.split('') : [];
        var options = [];
        angular.forEach(usages, function(usageFlag) {
            var option = enumerations.lightCapabilities[usageFlag];
            if (option && option.color) {
                options.push(option);
            }
        });
        return options;
    };
}]);


PWC.filter('getChildObjects', ['PropertyModel', function(PropertyModel) {
    return function(childString) {
        var children = [];
        if (childString) {
            var childNames = childString.split(',');
            angular.forEach(childNames, function(objnam) {
                var child = PropertyModel.Objects[objnam];
                if (child) {
                    children.push(child);
                }
            });
        }
        return children;
    };
}]);

PWC.filter('filterOutByObjectType', [function() {
    return function(objs, OBJTYP) {
        var objectsByType = [];
        for (var key in objs) {
            if(objs[key].OBJTYP === OBJTYP) {
                objectsByType.push(objs[key]);
            }
        }
        return objectsByType;
    }
}]);


PWC.filter('alertsByPage', ['$route', 'PropertyModel', function($route, PropertyModel) {
    return function(alerts) {
        var filteredAlerts = [];
        if (alerts && $route.current && $route.current.$$route) {
            angular.forEach(alerts, function(alert) {
                if (alert && alert.PARENT && alert.SHOMNU && alert.SHOMNU === 'OFF') {
                    var route = $route.current.$$route.originalPath || '';

                    switch (route) {
                        case '/property':
                            addPropertyTabAlert(alert);
                            break
                        default:
                            //filteredAlerts.push(alert);
                            break;
                    }
                }
            });
        }

        function addPropertyTabAlert(alert) {
            var tab = getActiveTab(PropertyModel.dashboardTabs);
            var parentObj = PropertyModel.Objects[alert.PARENT];

            switch (tab && tab.toLowerCase()) {
                case 'chemistry':
                    if (parentObj && parentObj.OBJTYP === 'CHEM') {
                        filteredAlerts.push(alert);
                    }
                    break;
                case 'home':
                case 'pools':
                    filteredAlerts.push(alert);
                    break;
                default:
                    //filteredAlerts.push(alert);
                    break;

            }
        }

        function getActiveTab(tabs) {
            var activeTab = '';
            angular.forEach(tabs, function(tab, tabName) {
                if (tab.active) {
                    activeTab = tabName;
                }
            });
            return activeTab;
        }

        return filteredAlerts;
    }
}]);

PWC.filter('capitalize', function() {
    return function(input) {
        return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
    }
});

PWC.filter('applyFilter', function($filter) {
    return function(value, filterSpec) {
        var filters = filterSpec.split('|');
        angular.forEach(filters, function(f) {
            var args = f.trim().split(':');
            var filter = $filter(args.shift());
            args.unshift(value);
            value = filter.apply(null, args)
        });
        return value
    };
});

PWC.filter('unitOfMeasurment', function($filter) {
    return function(mode) {
        var UoM = 'C';
        if (mode && mode.toUpperCase() === "ENGLISH") {
            UoM = 'F';
        }
        return UoM;
    };
});

PWC.filter('unitOfMeasurmentDegrees', function ($filter) {
    return function (mode) {
        var UoM = '℃';
        if (mode && mode.toUpperCase() === "ENGLISH") {
            UoM = '℉';
        }
        return UoM;
    };
});
PWC.filter('currentHeater', function(PropertyModel) {
    return function(body) {
        var heater = {};
        var heaterObjNam;
        if (body) {
            if (body.HTSRC && body.HTSRC != "000FF") {
                heaterObjNam = body.HTSRC;
            } else {
                heaterObjNam = body.HEATER;
            }
            if (heaterObjNam) {
                heater = PropertyModel.Objects[heaterObjNam]
            }
        }
        return heater;
    };
});

PWC.filter('currentHeatMode', function(Enumerations, PropertyModel) {
    return function(body) {
        var heatMode = "";
        var heaterObjNam;
        if (body) {
/*
            if (body.HTSRC && body.HTSRC != "000FF") {
                heaterObjNam = body.HTSRC;
                
            } else {
                heaterObjNam = body.HEATER;
            }
*/
            heaterObjNam = body.HEATER;
            //console.log("=========================================== Heater Obj Name: " + heaterObjNam);
            if (heaterObjNam) {
                var heater = PropertyModel.Objects[heaterObjNam] || {};
                var heaterType = heater.SUBTYP === 'HCOMBO' ? heater.HTMODE : heater.SUBTYP;
                
                heatMode = Enumerations.heatModes[heaterType];
                //console.log("===************************************** heater type: " + heater.SUBTYP + "  heat mode: " + heatMode);
            }
            // here are the prefered subtypes
            if (heatMode === undefined) {
                switch (heaterObjNam) {
                    case 'HXSLR': heatMode = 'Solar Preferred'; 
                        break;
                    case 'HXHTP': heatMode = 'Heat Pump Prefer';
                        break;
                    case 'HXULT': heatMode = 'UltraTemp Preferred';
                        break;
                    default:
                        heatMode = Enumerations.heatModes[heaterObjNam];
                        break;
                }
                
                //console.log("===++++++++++++++++++++++IS Preferred :   heater type: " + heater.SUBTYP + "  heat mode: " + heatMode);
            }
        }
        return heatMode;
    };
});

PWC.filter('currentHeaterStatus', function(Enumerations, PropertyModel) {
    return function(body) {
        var status = "";
        var heaterObjNam;
        if (body) {
            heaterObjNam = body.HEATER;
            //console.log("====== currentHeaterStatus :" + heaterObjNam);
            if (heaterObjNam) {
                var heater = PropertyModel.Objects[heaterObjNam] || {};
                var heaterType = heater.SUBTYP === 'HCOMBO' ? heater.HTMODE : heater.SUBTYP;
                status = Enumerations.heaterStatus[heater.STATUS];
                // console.log("===== STATUS heater type : " + heater.SUBTYP + "  heat status : " + status);

            }
        }        
        return status;
    };
});

PWC.filter('weatherForecast', function() {
    return function(array, start, end) {
        return (array || []).slice(start, end);
    };
});

PWC.filter('yesNo', function() {
    return function(input) {
        return input ? 'yes' : 'no';
    };
});

PWC.filter('displayAccess', function() {
   return function (value) {
       if(value === undefined || value === null || value === '')
           return 'NO ACCESS';
       if(value.length >= 22)
           return 'ALL ACCESS';
       return value.length + ' of ' + 22;
   };
});

PWC.filter('cut', function () {
    return function (value, wordwise, max, tail) {
        if (!value) return '';

        max = parseInt(max, 10);
        if (!max) return value;
        if (value.length <= max) return value;

        value = value.substr(0, max);
        if (wordwise) {
            var lastspace = value.lastIndexOf(' ');
            if (lastspace != -1) {
                //Also remove . and , so its gives a cleaner result.
                if (value.charAt(lastspace-1) == '.' || value.charAt(lastspace-1) == ',') {
                    lastspace = lastspace - 1;
                }
                value = value.substr(0, lastspace);
            }
        }
        return value + (tail || ' …');
    };
});

PWC.filter('debug', function() {
    return function(input) { return JSON.stringify(input); }
});

PWC.filter('validAlerts', function() {
    return function(alerts) {

        var filteredAlerts= [];
        if(alerts) {
            for( var i = 0; i < alerts.length; i++) {
                if (alerts[i].MODE <= 3) { // only alert, warning, inform, and status
                    filteredAlerts.push(alerts[i]);
                }
            }

            // remove dup objects
            var mySet = new Set();
            filteredAlerts = filteredAlerts.filter(function(filteredAlert) {
                var key =  filteredAlert.OBJNAM, isNew = !mySet.has(key);
                if(isNew) mySet.add(key);
                return isNew;
            });

            // remove objects that have shomnu off, those one have been dismissed.
            filteredAlerts = filteredAlerts.filter(function(filteredAlert) {
                return filteredAlert.SHOMNU === 'ON';
            });

            // sort by date asc
            filteredAlerts.sort(function(a, b) {
                var keyA = parseInt(a.TIME),
                    keyB = parseInt(b.TIME);
                if (keyA < keyB) return -1;
                if (keyA > keyB) return 1;
                return 0;
            });

            for( var i = 0; i < filteredAlerts.length; i++) {
                filteredAlerts[i].offSet = i * 10;
                filteredAlerts[i].zIndex = i + 10;
            }
        }

        return filteredAlerts;
    }
});


// sort heaters 
PWC.filter('sortHeaters', function () {
    return function (heaters) {

        var filteredHeaters = [];
        if (heaters) {
            for (var i = 0; i < heaters.length; i++) {
                if (!heaters[i].ORDER) // IF DOES NOT EXIST ASSING 0
                    heaters[i].ORDER = 0

                filteredHeaters.push(heaters[i]);
                
            }
            
            // sort by date asc
            filteredHeaters.sort(function (a, b) {
                var keyA = parseInt(a.ORDER),
                    keyB = parseInt(b.ORDER);
                if (keyA < keyB) return -1;
                if (keyA > keyB) return 1;
                return 0;
            });
                
        }

        return filteredHeaters;
    }
});



// remove only from heaters
PWC.filter('removeHeatersOnly', function () {
    return function (heaters) {

        var filteredHeaters = [];
        if (heaters) {
            for (var i = 0; i < heaters.length; i++) {
                //if (!heaters[i].ORDER) // IF DOES NOT EXIST ASSING 0
                //    heaters[i].ORDER = 0

                filteredHeaters.push(heaters[i]);

            }
        }

        return filteredHeaters;
    }
});

PWC.filter('filterAlertByMode', [function() {
    return function(alerts, mode) {
        // remove dup objects
        var mySet = new Set();
        alerts = alerts.filter(function(filteredAlert) {
            var key =  filteredAlert.OBJNAM, isNew = !mySet.has(key);
            if(isNew) mySet.add(key);
            return isNew;
        });

        // sort the alerts by time desc
        alerts.sort(function(a, b) {
            var keyA = parseInt(a.TIME),
              keyB = parseInt(b.TIME);
            if (keyA > keyB) return -1;
            if (keyA < keyB) return 1;
            return 0;
        });

        if(!mode) {
            return alerts;
        }
        switch(mode) {
            case 'ALL':
                return alerts.filter(function (filteredAlert) {
                    return filteredAlert;
                });
                break;
            case 'Alerts':
                return alerts.filter(function (filteredAlert) {
                    return filteredAlert.MODE === '0';
                });
                break;
            case 'Warnings':
                return alerts.filter(function (filteredAlert) {
                    return filteredAlert.MODE === '1';
                });
                break;
            case 'Information':
                return alerts.filter(function (filteredAlert) {
                    return filteredAlert.MODE === '2';
                });
                break;
            case 'Status':
                return alerts.filter(function (filteredAlert) {
                    return filteredAlert.MODE === '3';
                });
                break;
            case 'Local':
                return alerts.filter(function (filteredAlert) {
                    return filteredAlert.MODE === '4';
                });
                break;
        }
        return alerts;
    }
}]);

PWC.filter('filterPumpAffectLowSpeed', [function() {
    return function (pumpAffects) {
        if (pumpAffects === undefined)
            return null;


        var filterResult = pumpAffects.filter(function(pumpAffect) {
          return pumpAffect.BOOST === 'OFF';
        });
        if(filterResult && filterResult.length > 0) {
            return filterResult;
        } else {
            return null;
        }
    }
}]);

PWC.filter('filterPumpAffectHighSpeed', [function() {
    return function (pumpAffects) {

        if (pumpAffects === undefined)
            return null;

        var filterResult = pumpAffects.filter(function(pumpAffect) {
            return pumpAffect.BOOST === 'ON';
        });
        if(filterResult && filterResult.length > 0) {
            return filterResult;
        } else {
            return filterResult;
        }
    }
}]);

PWC.filter('excludeCircuitsFromExistingPumpAffects', [function() {
    return function(objectList, excludeList) {
        angular.forEach(excludeList, function(excludeItem) {
            objectList = objectList.filter(function(objectItem) {
                return objectItem.OBJNAM !== excludeItem.CIRCUIT;
            });
        });
        // also remove all on all off
        objectList = objectList.filter(function(circuit) {
            return (circuit.SNAME !== 'All Lights On' && circuit.SNAME !== 'All Lights Off');
        });
        return objectList;
    }
}]);

PWC.filter('sortByPumpAffectPriority', [function() {
    return function(pump) { // currently this is only used by VS pumps
        if (!pump || !pump.pumpAffects){
            return [];
        }
        if(pump.PRIOR === 'SPEED') {
            pump.pumpAffects.sort(function(a, b) {
                var keyA = parseInt(a.SPEED),
                  keyB = parseInt(b.SPEED);
                if (keyA > keyB) return -1;
                if (keyA < keyB) return 1;
                return 0;
            });
            return pump.pumpAffects;
        } else {
            pump.pumpAffects.sort(function(a, b) {
                var keyA = parseInt(a.LISTORD),
                  keyB = parseInt(b.LISTORD);
                if (keyA < keyB) return -1;
                if (keyA > keyB) return 1;
                return 0;
            });
            return pump.pumpAffects;
        }
    }
}]);


PWC.filter('circuitsExcludeAllOnOff', [function() {
    return function(circuits) {
        return circuits.filter(function(circuit) {
            //return (circuit.HNAME !== 'All Lights On' && circuit.HNAME !== 'All Lights Off');
            if (circuit.HNAME !== 'All Lights On')
                return circuit;
            if (circuit.HNAME !== 'All Lights Off')
                return circuit;

        });
    }
}]);


PWC.filter('pumpAffectedExcludeInternals', [function () {
    return function (pump) {
        return pump.filter(function (circuit) {            
            if (circuit.SNAME !== 'PRIMING')
                return circuit;            
        });
    }
}]);

PWC.filter('excludeObjectName', [function() {
    return function(objectList, objnam) {
        if(objectList === undefined)
            return objectList;
        objectList = objectList.filter(function(objectItem) {
            return (objectItem.OBJNAM !== objnam);
        });
        return objectList;
    }
}]);


PWC.filter('circuitsBodyOnly', [function () {
    return function (circuits) {
        return circuits.filter(function (circuit) {
            //return (circuit.HNAME !== 'All Lights On' && circuit.HNAME !== 'All Lights Off');
            if (circuit.HNAME === 'Pool')
                return circuit;
            if (circuit.HNAME === 'Spa')
                return circuit;
            if (circuit.HNAME === 'Pool/Spa')
                return circuit;

        });
    }
}]);

// is failing this function !!!
PWC.filter('AllBodiesChemistry', [function () {
    return function (AllBodies) {
        var filteredObjects = [];
        angular.forEach(AllBodies, function (body) {
            if (body.CHEMISTRY) {
                if ((body.CHEMISTRY.ICHEM) || (body.CHEMISTRY.ICHLOR)) {
                    filteredObjects.push(body);
                }
            }
        });
        return filteredObjects;
    };
}]);
