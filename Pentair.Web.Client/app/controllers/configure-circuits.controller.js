'use strict';
var PWC = PWC || {};
PWC.controller('ConfigureCircuitsController', [
    '$rootScope', '$scope', '$location', '$interpolate', 'PropertyService', 'PropertyModel', 'Enumerations',
    '$q', 'Notification',
    function ($rootScope, $scope, $location, $interpolate, propertyService, propertyModel, Enumerations,
             $q, Notification) {
        var secretEmptyKey = '[$empty$]';
        $scope.$parent.Layout = 'layout-base';
        $scope.property = propertyModel;
        $scope.accordionStateByObjectNames = {};
        $rootScope.editMode.circuits = {};

        $scope.theProperty = propertyService;
        $scope.SHOMNU = propertyModel.ACL.SHOMNU;
        $scope.ACL = propertyModel.ACL;
        $scope.securityEnabled = propertyModel.SecurityEnabled;

        $scope.maxFeatureCircuits = 32;
        $scope.maxGroupCircuits = 16;

        updatePanels();
        updateFeatureCircuits();
        updateCircuitGroups();

        $scope.circuitsGroupsBeforeEdit = angular.copy($scope.circuitsGroups);
        $scope.auxCircuitNames = $scope.property.circuitNames;
        $scope.featureCircuitNames = $scope.property.circuitNames.filter(function (circuitName) {
            if (circuitName.indexOf('AUX') < 0) {
                return circuitName;
            }
        });
        $scope.circuitSubtypes = propertyModel.circuitTypes;
        $scope.featureCircuitSubtypes = Enumerations.getFeatureCircuitSubtypeObjects();

        $scope.featureCircuitMenu = function (selected) {
            var result = angular.copy($scope.featureCircuitSubtypes);
            return result;
        }

        $scope.errorSave = function (errorMessage) {
            Notification.error({ message: 'Failed!' + ' ' + errorMessage, delay: 1000 });
        };

        $scope.successSave = function () {
            Notification.success({ message: 'Saved!', delay: 1000 });
        };

        $scope.successUpdated = function () {
            Notification.success({ message: 'Updated!', delay: 1000 });
        };

        $scope.successCancel = function () {
            Notification.success({ message: 'Cancelled!', delay: 1000 });
        };

        $scope.isSplillwaySelected = function () {

            var spill = false;
            // apply only if is shared body
            if ($scope.property.PANID === "SHARE") {
                $scope.featureCircuits.forEach(function (circuit) {
                    if (circuit.subtype === "SPILL")
                        spill = true;// there is at least 1 spillway feature circuit
                });
            }

            return spill;
        };


        $scope.isSplillwayChoosen = function (value) {
            if (value === "SPILL")
                return true;
            else
                return false;

        };


        $scope.isPoolorSpa = function (circuit) {
            if ((circuit.subtype === "POOL") || (circuit.subtype === "SPA"))
                return true;


            return false;

        };


        $scope.setFeatureCircuitSubtype = function (featurecircuit, value) {

            var spill = false;
            // apply only if is shared body
            if ($scope.property.PANID === "SHARE") {
                $scope.featureCircuits.forEach(function (circuit) {
                    if (circuit.subtype === "SPILL")
                        spill = true;// there is at least 1 spillway feature circuit
                });
            }

            if (spill) {
                if (value !== "SPILL") {
                    featurecircuit.subtype = value;     // set it only if spillway is not selected
                }
            }
            else {
                // set anytype if spill is not selected
                featurecircuit.subtype = value;
            }

        };

        // return how many FeatureCircuits are free 
        $scope.getGroupCircuitsFree = function () {
            var free = $scope.circuitsGroups.length;
            return free;
        };
        // check can add more FeatureCircuits
        $scope.canAddGroupCircuits = function () {
            var free = $scope.maxGroupCircuits - $scope.circuitsGroups.length;
            if (free > 0)
                return true;
            else
                return false;
        };

        $scope.areSelectedGroupCircuitsToRemove = function () {
            var enabled = false;
            $scope.circuitsGroups.forEach(function (Circuit) {
                if (Circuit.isSelected) {
                    enabled = true;     // at least 1 is enable (selected).
                }
            });

            return enabled;
        };

        /*********** SECURITY MODE July-18-2018 ********
        VacationMode		,  v        done 
        Support  		    ,  o        NA    
        GeneralSettings		,  g        done  , under testing
        AlertsNotification	,  q        done
        UserPortal		    ,  i        ONLY  ADMIN : THW
        Groups			    ,  k        done 
        Remotes			    ,  w        Removed     *** this option has been removed July-2018 : replace 'w' to 'a' option as per steve recomendation
        AdvancedSettings	,  a        done   , under testing
        Chemistry		    ,  c        done
        Status			    ,  t        done
        DeleteStatus		,  r        Removed    *** this option has been removed July-2018 : replace 'r' to 't' option as per steve recomendation
        Schedules		    ,  e        done
        Features		    ,  f        done
        Lights			    ,  l        done
        Pool			    ,  p        done
        PoolHeatSource		,  h        **
        PoolTemp		    ,  P        **
        Spa			        ,  m        done
        SpaHeatSource		,  n        **
        SpaTemp			    ,  S        **
        History			    ,  u        done
        MultiBodyDrawer		,  b        NA
        ServiceMode		    ,  x        done 
        SystemConfig		,  C        done 
        ********************************/

        $scope.isSecurityEnabled = function () {

            // checking
            if ($scope.ACL) {    // ckecking if ACL exist
                if ($scope.ACL.OBJNAM) {    // ckecking if objnam exist
                    if ($scope.ACL.OBJNAM === "U0000") {   // checking GUEST IS SIGNED
                        return true //security is enabled by GUEST user OR HAS NO RIGTHS ...
                    }
                }
            }

            if ($scope.securityEnabled)
                return true
            else
                return false
        }

        $scope.isAllowBySecurity = function (section) {

            var status = true


            // check security is not enable
            if ($scope.isSecurityEnabled() === false) {
                return true // allow security is not enabled.
            }

            // if secur

            switch (section) {
                case 'SECTION_SECURITY':
                    //status = false          // hide this tab is security is enabled
                    if ($scope.SHOMNU.indexOf('C') >= 0)
                        status = true
                    else
                        status = false
                    break

                case 'SECTION_VACATION_MODE':
                    if ($scope.SHOMNU.indexOf('v') >= 0)
                        status = true
                    else
                        status = false
                    break


                case 'SECTION_NOTIFICATIONS':
                    if ($scope.SHOMNU.indexOf('q') >= 0)
                        status = true
                    else
                        status = false
                    break

                case 'SECTION_ADVANCED_SETTINGS':
                    if ($scope.SHOMNU.indexOf('a') >= 0)
                        status = true
                    else
                        status = false
                    break

                case 'SECTION_GENERAL_SETTINGS':
                    if ($scope.SHOMNU.indexOf('g') >= 0)
                        status = true
                    else
                        status = false
                    break

                case 'SECTION_GROUPS':
                    if ($scope.SHOMNU.indexOf('k') >= 0)
                        status = true
                    else
                        status = false
                    break

                default:

                    break

            }

            return status
        }

        $scope.combineName = function (obj) {
            return $interpolate('{{hName}} - {{sName}}')(obj);
        };


        $scope.getCircuitAuxID = function (obj) {
            return $interpolate('{{hName}}')(obj);
        };

        $scope.getCircuitName = function (obj) {
            return $interpolate('{{sName}}')(obj);
        };
        $scope.combineNameTab = function (obj) {
            return $interpolate('Aux-Circuits ({{sName}})')(obj);
        };

        $scope.SetEditMode = function (obj) {
            if (obj.objectName) {
                $rootScope.editMode.circuits[obj.objectName] = true;
            }
            if (obj.OBJNAM) {
                $rootScope.editMode.circuits[obj.OBJNAM] = true;
            }
        };

        $scope.CancelEdit = function (index, parentIndex) {
            // FIXME: need to figure out a better more efficient way of reverting changes
            if (parentIndex !== undefined) {
                // OCP
                $scope.panels = angular.copy($scope.panelsBeforeEdit);
                $rootScope.forms.circuits.aux[parentIndex][index].$setPristine();
                $scope.successCancel();
            } else {
                // group
                // $rootScope.editMode.circuits[$scope.circuitsGroups[index].OBJNAM] = false;
                $scope.circuitsGroups = angular.copy($scope.circuitsGroupsBeforeEdit);
                $rootScope.forms.circuits.group[index].$setPristine();
                //$scope.circuitsGroups[index].editMode = false;
                $scope.successCancel();
            }
        };

        $scope.cancelFeatureEdit = function (featureCircuit, index) {
            $rootScope.editMode.circuits[featureCircuit.objectName] = false;
            $scope.featureSetPristine(index);// $rootScope.forms.circuits.feature[index].$setPristine();
            $scope.featureCircuits = angular.copy($scope.featuredCircuitsBeforeEdit);
            $scope.successCancel();
        };

        $scope.SaveAUXCircuit = function (circuit, index, parentIndex) {
            if (circuit.sName === '') {
                $scope.errorSave('Form invalid, no name.');
                return;
            }
            var params = {};
            params.SNAME = circuit.sName;
            params.SUBTYP = circuit.subtype;
            params.FREEZE = (circuit.freezeProtection) ? 'ON' : 'OFF';
            var minutes = circuit.eggTimerMinutes;
            minutes += (circuit.eggTimerHours * 60);
            params.TIME = minutes + '';
            params.DNTSTP = (circuit.disableEggTimer) ? 'ON' : 'OFF';
            params.FEATR = (circuit.showAsFeature) ? 'ON' : 'OFF';
            propertyService.SetParams(circuit.objectName, params).then(function () {
                propertyService.subscribeToObject({ OBJNAM: circuit.objectName, OBJTYP: 'CIRCUIT', SUBTYP: circuit.subtype }).then(function () {
                    propertyService.GetHardwareDefinitionPrecise().then(function () {
                        $scope.panels = [];
                        updatePanels();
                        updateFeatureCircuits();
                        $rootScope.forms.circuits.aux[parentIndex][index].$setPristine();
                        $scope.successSave();
                    });
                });
            });
            $rootScope.editMode.circuits[circuit.objectName] = false;
        };


        $scope.SaveAUXCircuitOne = function (circuit, index, parentIndex) {
            if (circuit.sName === '') {
                $scope.errorSave('Form invalid, no name.');
                return;
            }
            var params = {};
            params.SNAME = circuit.sName;
            params.SUBTYP = circuit.subtype;
            params.FREEZE = (circuit.freezeProtection) ? 'ON' : 'OFF';

            // if disableEggTimer is on time set to 24 hours and 0 minutes
            if (circuit.disableEggTimer) {
                circuit.eggTimerHours = 24;
                circuit.eggTimerMinutes = 0;
            } else {
                // check if eggtimer is larger than 24 hours set disableEddTimer to "true"
                if (circuit.eggTimerHours >= 24) {
                    circuit.eggTimerHours = 24;
                    circuit.eggTimerMinutes = 0;
                    circuit.disableEggTimer = true;
                }
            }
            // minimum eggtimer is 1
            if ((circuit.eggTimerHours === 0) && (circuit.eggTimerMinutes === 0)) {
                circuit.eggTimerMinutes = 1
            }

            var minutes = circuit.eggTimerMinutes;
            minutes += (circuit.eggTimerHours * 60);
            params.TIME = minutes + '';
            params.DNTSTP = (circuit.disableEggTimer) ? 'ON' : 'OFF';
            params.FEATR = (circuit.showAsFeature) ? 'ON' : 'OFF';
            var objName = circuit.objectName;
            propertyService.SetParams(circuit.objectName, params).then(function () {
                propertyService.subscribeToObject({ OBJNAM: circuit.objectName, OBJTYP: 'CIRCUIT', SUBTYP: circuit.subtype }).then(function () {
                    propertyService.GetHardwareDefinitionPrecise().then(function () {
                        //$scope.panels = [];
                        updatePanels();
                        //updateFeatureCircuits();                        
                        // $rootScope.forms.circuits.aux[parentIndex][index].$setPristine();
                        $scope.successSave();
                    });
                });
            });


            $rootScope.editMode.circuits[circuit.objectName] = false;

            // retrieve params from target board
            $scope.theProperty.GetParamsCircuit(objName);

            $scope.printAllCircuit();

        };

        $scope.printAllCircuit = function () {
            console.log("-------------------- all AUX circuits -----------------------------------");
            angular.forEach($scope.panels, function (panel) {
                angular.forEach(panel.circuits, function (circuit) {
                    console.log("circuit name: " + circuit.sName + "    hw name:" + circuit.hName);
                });
            });
        }

        $scope.printAllFeatureCircuit = function () {
            console.log("-------------------- all FEATURE circuits -----------------------------------");
            angular.forEach($scope.panels, function (panel) {
                angular.forEach($scope.featureCircuits, function (circuit) {
                    console.log("Feature circuit name: " + circuit.sName + "    hw name:" + circuit.hName);
                });
            });
        }

        $scope.printAllGroupCircuit = function () {
            console.log("-------------------- all GROUP circuits -----------------------------------");
            angular.forEach($scope.circuitsGroups, function (circuit) {
                console.log("group circuit name: " + circuit.sName + "    hw name:" + circuit.hName);
            });

        }

        $scope.saveFeatureCircuit = function (circuit, index) {
            if (circuit.sName === '') {
                $scope.errorSave('Form invalid, no name.');
                return;
            }
            var params = {};

            // if disableEggTimer is on time set to 24 hours and 0 minutes
            if (circuit.disableEggTimer) {
                circuit.eggTimerHours = 24;
                circuit.eggTimerMinutes = 0;
            } else {
                // check if eggtimer is larger than 24 hours set disableEddTimer to "true"
                if (circuit.eggTimerHours >= 24) {
                    circuit.eggTimerHours = 24;
                    circuit.eggTimerMinutes = 0;
                    circuit.disableEggTimer = true;
                }
            }

            params.SNAME = circuit.sName;
            params.SUBTYP = circuit.subtype;
            params.FREEZE = (circuit.freezeProtection) ? 'ON' : 'OFF';
            var minutes = circuit.eggTimerMinutes;
            minutes += (circuit.eggTimerHours * 60);
            params.TIME = minutes + '';
            params.DNTSTP = (circuit.disableEggTimer) ? 'ON' : 'OFF';
            params.FEATR = (circuit.showAsFeature) ? 'ON' : 'OFF';
            var objName = circuit.objectName;

            if (circuit.createMode) {
                propertyService.CreateNewObject('CIRCUIT', params).then(function (result) {
                    propertyService.subscribeToObject({ OBJNAM: result.objnam, OBJTYP: 'CIRCUIT', SUBTYP: circuit.subtype }).then(function () {
                        propertyService.GetHardwareDefinitionPrecise().then(function () {
                            $scope.panels = [];
                            updateFeatureCircuits();
                            updatePanels();
                            $scope.featureSetPristine(index);
                            $scope.successSave();
                        });
                        $rootScope.editMode.circuits[result.objnam] = false;

                    });
                });
            } else {
                propertyService.SetParams(circuit.objectName, params).then(function (result) {
                    propertyService.subscribeToObject({ OBJNAM: circuit.objectName, OBJTYP: 'CIRCUIT', SUBTYP: circuit.subtype }).then(function (result) {
                        propertyService.GetHardwareDefinitionPrecise().then(function () {
                            $scope.panels = [];
                            updateFeatureCircuits();
                            updatePanels();
                            $scope.featureSetPristine(index);
                            $scope.successSave();
                        });
                        $rootScope.editMode.circuits[circuit.objectName] = false;
                    });
                });
            }

            $rootScope.editMode.circuits[circuit.objectName] = false;
            // retrieve params from target board
            $scope.theProperty.GetParamsCircuit(objName);
            // print all feature circuits
            $scope.printAllFeatureCircuit();
        };

        $scope.featureSetPristine = function (index) {
            if ($rootScope.forms.circuits && $rootScope.forms.circuits.feature && $rootScope.forms.circuits.feature[index])
                $rootScope.forms.circuits.feature[index].$setPristine();
        }

        $scope.isValidCircuitGroup = function (obj) {
            // needs a sname and subtype
            if (!obj.sName) {
                return false;
            }
            // for all circuits, there needs to be a circuit assigned.
            if (obj.circuits && obj.circuits.length > 0) {
                for (var i = 0; i < obj.circuits.length; i++) {
                    if (!obj.circuits[i].OBJNAM) {
                        return false;
                    }
                }
            } else {
                return false;
            }

            return true;
        };

        $scope.supportGroupCircuitState = function () {
            // add logic to support group circuits based on the version
            var version = getOCPVersion();

            if (version >= 1.041)    // support group circuit version
                return true;
            else
                return false;
        }


        $scope.setCircuitGroupItemState = function (circuit, state) {
            // add logic to support group circuits based on the version
            if (circuit.ACT)
                circuit.ACT = state;
            console.log(circuit);
        }

        $scope.getCircuitGroupStyle = function (circuit, state) { // return color of background
            var style = "background-color:#CCCCCC;;color:626262;";

            if (circuit.ACT == state)
                style = "background-color:#9FCC14;color:#626262;";

            return style;
        }

        $scope.isCircuitStateConflicted = function (circuitsGroup, circuit) { // return color of background
            var conflict = false; // check if theres is a conflict 
            angular.forEach($scope.circuitsGroups, function (group) {
                if (group.circuits) {
                    if (group.OBJNAM != circuitsGroup.OBJNAM) { // check only different items
                        angular.forEach(group.circuits, function (item) {
                            if (item.OBJNAM === circuit.OBJNAM) { // is the same item
                                if (item.ACT != circuit.ACT) { // is the same item                                    
                                    conflict = true;
                                }
                            }
                        });

                    }
                }
            });

            return conflict;
        }

        $scope.isCircuitOverlap = function (circuitsGroup, circuit) { // return color of background
            var style = "height:62px;";
            var conflic = $scope.isCircuitStateConflicted(circuitsGroup, circuit);
            if (conflic == true)
                style = "height:110px;";

            return style;
        }

        $scope.SaveGroupCircuit = function (obj, index) {
            var args = {};
            args.OBJNAM = obj.OBJNAM;
            args.SNAME = obj.sName;
            var minutes = obj.eggTimerMinutes;
            minutes += (obj.eggTimerHours * 60);
            args.TIME = minutes + ''
            args.OBJLIST = obj.circuits.map(function (circuit) {

                //var minutes = circuit.eggTimerMinutes;
                //minutes += (circuit.eggTimerHours * 60);

                var objItem = {};
                objItem.objnam = circuit.OBJNAM;
                objItem.TIME = args.TIME;
                objItem.DNTSTP = (circuit.disableEggTimer) ? 'ON' : 'OFF';
                objItem.params = {};
                objItem.params.ACT = circuit.ACT; //objItem.params.ACT = circuit.isOn; // 'ON' : 'OFF';                                                
                // removing listord
                //objItem.params.LISTORD = (circuit.listOrder) ? circuit.listOrder + '' : '1';

                return objItem;
            });
            if (obj.createMode) {
                delete args.OBJNAM;
                args.OBJTYP = 'CIRCGRP';
                propertyService.CreateGroup(args).then(function (result) {
                    // set params for act
                    var objectList = [];
                    angular.forEach(result.objectList[0].params.OBJLIST, function (objectAffect) {
                        var objectItem = {
                            objnam: objectAffect.objnam,
                            params: {
                                ACT: objectAffect.params.ACT,
                                CIRCUIT: objectAffect.params.CIRCUIT
                            }
                        };
                        objectList.push(objectItem);
                    });
                    propertyService.SetParamList(objectList).then(function () {
                        propertyService.GetHardwareDefinitionPrecise().then(function () {
                            updateCircuitGroups();
                            //$rootScope.editMode.circuits[obj.OBJNAM] = false;
                            obj.createMode = false;
                            $rootScope.forms.circuits.group[index].$setPristine();
                            $scope.successSave();
                        });
                    });
                });
                obj.createMode = false;
            } else {
                //delete args.OBJLIST;
                propertyService.EditGroup(args).then(function (result) {
                    // set params for act
                    var objectList = [];
                    angular.forEach(result.objectList[0].params.OBJLIST, function (objectAffect) {
                        var objectItem = {
                            objnam: objectAffect.objnam,
                            params: {
                                ACT: objectAffect.params.ACT,
                                CIRCUIT: objectAffect.params.CIRCUIT
                            }
                        };
                        objectList.push(objectItem);
                    });
                    propertyService.SetParamList(objectList).then(function () {
                        propertyService.GetHardwareDefinitionPrecise().then(function (result2) {
                            updateCircuitGroups();
                            // $rootScope.editMode.circuits[obj.OBJNAM] = false;
                            $rootScope.forms.circuits.group[index].$setPristine();
                            $scope.successSave();
                        });
                    });
                });
            }


            // retrieve params from target board
            $scope.theProperty.GetParamsCircuit(obj.OBJNAM);
        };

        $scope.addCircuitToCircuitsGroup = function (circuitGroup) {
            var circuit = {
                OBJNAM: "",
                SNAME: "",
                listOrder: 1,
                ACT: 'ON',
            };
            circuitGroup.circuits.push(circuit);
        };

        $scope.deleteCircuitFromCircuitsGroup = function (circuitsGroup, index) {
            circuitsGroup.circuits.splice(index, 1);
        };

        // Set default circuit group
        $scope.getDefaultNameCircuitGroup = function () {
            var name = 'Circuit Group'
            var count = $scope.circuitsGroups.length
            if (count >= 0) {
                count = count + 1
                name = "Circuit Group " + count
            }
            return name
        }

        $scope.addCircuitGroup = function () {
            var uuid = Math.uuid(4);
            var circuitList = propertyModel.Circuits.filter(function (circuit) {
                // ignore special circuits like all on and all off
                if (circuit.OBJNAM !== '_A111' && circuit.OBJNAM !== '_A110') {
                    return circuit;
                }
            });
            var circuitsGroup = {
                OBJNAM: uuid,
                hName: uuid,
                sName: $scope.getDefaultNameCircuitGroup(),
                isSelected: false,
                circuits: [],
                editMode: true,
                createMode: true,
                eggTimerHours: 12,
                eggTimerMinutes: 0,
                selectableCircuits: circuitList
            };

            $scope.circuitsGroups.push(circuitsGroup);
            // var args = {
            //     OBJTYP: 'CIRCGRP',
            //     SNAME: uuid
            // };
            $scope.accordionStateByObjectNames[circuitsGroup.OBJNAM] = { open: true };
            // $rootScope.editMode.circuits[uuid] = true;
        };

        $scope.removeCircuitGroups = function () {

            var item = $scope.removeCircuitGroupsRemoving();
            // update current circuits
            propertyService.GetHardwareDefinitionPrecise().then(function () {
                updateCircuitGroups();
            });

            $scope.printAllGroupCircuit();
        };

        // receiving broadcast 
        $scope.$on('NewObjectFound', function (event, message) {
            // updating group circuits
            //console.log("-----------------CIRCUITS : Processing Broadcast ")
            updateCircuitGroups();
        });

        $scope.removeCircuitGroupsRemoving_old = function () {
            //console.log("removing group circuits...");
            // populate the list of object names we are going to delete
            $scope.objectNamesToDelete = $scope.circuitsGroups.map(function (obj) {
                if (obj.isSelected) {
                    if ($rootScope.editMode.circuits[obj.OBJNAM]) {
                        delete $rootScope.editMode.circuits[obj.OBJNAM];
                    }
                    return obj.OBJNAM;
                }
            });
            propertyService.DeleteObject($scope.objectNamesToDelete).then(function (response) {
                // if we successfully deleted that list filter out the ones that we deleted and snapshot it.
                $scope.circuitsGroups = $scope.circuitsGroups.filter(function (obj) {
                    if (!obj.isSelected) {
                        return obj;
                    }
                });
                $scope.circuitsGroupsBeforeEdit = angular.copy($scope.circuitsGroups);
            });
        };

        $scope.removeCircuitGroupsRemoving = function () {
            console.log("removing group circuits...")
            var defer = $q.defer();
            var promises = [];
            $scope.circuitsGroups.forEach(function (circuit) {
                if (circuit.isSelected) {
                    var objNam = circuit.OBJNAM;
                    promises.push(propertyService.SetValue(objNam, 'STATUS', 'DSTROY'));
                }
            });

            $q.all(promises).then(function () {
                // TODO: need to get the broadcast figured out
                $scope.circuitsGroups = $scope.circuitsGroups.filter(function (obj) {
                    return obj.isSelected === false;
                });
                defer.resolve();
            });

            return defer.promise;
        };

        $scope.getDefaultNameFeatureCircuit = function () {
            var name = 'Feature'
            var count = $scope.featureCircuits.length
            if (count >= 0) {
                count = count + 1
                name = "Feature " + count
            }
            return name
        }

        $scope.addFeatureCircuit = function () {
            var uuid = Math.uuid(4);
            var featureCircuit = {
                objectName: uuid,
                hName: uuid,
                sName: $scope.getDefaultNameFeatureCircuit(), //'FEA',
                isSelected: false,
                subtype: 'GENERIC',
                showAsFeature: true,
                freezeProtection: false,
                eggTimerHours: 12,
                eggTimerMinutes: 0,
                disableEggTimer: false,
                editMode: true,
                createMode: true
            };

            $scope.featureCircuits.push(featureCircuit);
            $scope.accordionStateByObjectNames['feat-' + uuid] = { open: true };
            // $rootScope.editMode.circuits[featureCircuit.objectName] = true;
        };


        // return how many FeatureCircuits are free 
        $scope.getFeatureCircuitsFree = function () {
            var free = $scope.featureCircuits.length;
            return free;
        };
        // check can add more FeatureCircuits
        $scope.canAddFeatureCircuits = function () {
            var free = $scope.maxFeatureCircuits - $scope.featureCircuits.length;
            if (free > 0)
                return true;
            else
                return false;
        };

        $scope.areSelectedFeatureCircuitsToRemove = function () {
            var enabled = false;
            $scope.featureCircuits.forEach(function (featureCircuit) {
                if (featureCircuit.isSelected) {
                    enabled = true;     // at least 1 is enable (selected).
                }
            });

            return enabled;
        }

        $scope.removeFeatureCircuits = function () {
            var defer = $q.defer();
            var promises = [];
            $scope.featureCircuits.forEach(function (featureCircuit) {
                if (featureCircuit.isSelected) {
                    promises.push(propertyService.SetValue(featureCircuit.objectName, 'STATUS', 'DSTROY'));
                    if ($rootScope.editMode.circuits[featureCircuit.objectName]) {
                        delete $rootScope.editMode.circuits[featureCircuit.objectName];
                    }
                }
            });

            $q.all(promises).then(function () {
                // TODO: need to get the broadcast figured out
                $scope.featureCircuits = $scope.featureCircuits.filter(function (obj) {
                    return obj.isSelected === false;
                });
                defer.resolve();
            });

            return defer.promise;
        };

        $scope.selectCircuitName = function ($item, $model, $label) {
            //console.log($item);
        };

        $scope.nameComparator = function (name, viewValue) {
            return viewValue === secretEmptyKey || ('' + name).toLowerCase().indexOf(('' + viewValue).toLowerCase()) > -1;
        };

        $scope.getDictionaryValue = function (actualValue) {
            if (!actualValue)
                return actualValue;
            var match = $scope.circuitSubtypes.filter(function (circuitSubtype) {
                if (circuitSubtype.systemValue === actualValue) {
                    return circuitSubtype;
                }
            });

            if (match && match.length > 0 && match[0].readableValue) {
                return match[0].readableValue;
            }
            return actualValue;
        };

        $scope.setCircuitDontStop = function (circuit) {
            if (circuit.disableEggTimer === true) {
                circuit.eggTimerHours = "24";
                circuit.eggTimerMinutes = "0";
            }
            else {
                // if set false and 
                if (circuit.eggTimerHours >= "24") {
                    circuit.eggTimerHours = "12";
                    circuit.eggTimerMinutes = "0";
                }
            }
        };

        $scope.$on('NotifyList', function (objname, item, obj) {

            //console.log("---- processing notify Circuit controller..." + item);          

            // process aux circuits
            angular.forEach($scope.panels, function (panel) {
                angular.forEach(panel.circuits, function (circuit) {
                    if (obj.objnam === circuit.objectName) { // check match
                        updateCircuit(circuit, obj);
                    }
                });
            });

            // process  feature circuits
            angular.forEach($scope.featureCircuits, function (circuit) {
                if (obj.objnam === circuit.objectName) { // check match
                    updateCircuit(circuit, obj);
                }
            });

            // process feature circuits
            angular.forEach($scope.panels, function (panel) {
                angular.forEach(panel.group, function (circuit) {
                    if (obj.objnam === circuit.objectName) { // check match
                        updateCircuit(circuit, obj);
                    }
                });
            });

        });

        // BROADCAST : DELETE , CREATE CIRCUITS
        $scope.$on('UPDATE_CIRCUITS', function (objname, item, obj) {
            switch (obj) {
                case "FEATURES": updateFeatureCircuits();
                    break;

                case "GROUPS":
                    //updateCircuitGroups();
                    $scope.UpdateHardwareDefinition();    
                    break;
            }
        });

        //
        $scope.UpdateHardwareDefinition = function () {
            console.log(" Updating hardware definition... ");
            propertyService.GetHardwareDefinitionPrecise().then(function (result) {
                updateCircuitGroups();
                $scope.successUpdated();
            });
        }
        $scope.$on('WriteParamList', function (objname, item, obj) {

            console.log("---- processing WriteParamList Circuit controller...", item);
            // process group circuits
            angular.forEach($scope.circuitsGroups, function (circuit) {
                if (item) {
                    if (item.objnam) {
                        if (item.objnam === circuit.OBJNAM) { // check match
                            updateCircuitGroup(circuit, item.params);
                            //circuitsGroup.sName = item.params.SNAME;
                        }
                    }
                }
            });
        });

        function updateCircuitGroup(circuit, params) {
            // update circuit name
            if (circuit.sName && params.SNAME) {
                circuit.sName = params.SNAME;
            }

            // update time
            if (params.TIME) {
                circuit.eggTimerHours = Math.floor(params.TIME / 60);
                circuit.eggTimerMinutes = params.TIME % 60;
            }
        }

        // update circuitaux, feature circuit, groups circuits
        function updateCircuit(circuit, obj) {
            if (circuit.TIME && obj.params.TIME) {  // check if params exist
                circuit.TIME = obj.params.TIME;
            }

            if (circuit.sName && obj.params.SNAME) {
                circuit.sName = obj.params.SNAME;
            }

            if (circuit.subtype && obj.params.SUBTYP) {
                circuit.subtype = obj.params.SUBTYP;
            }

            if (obj.params.FEATR) {
                if (obj.params.FEATR === "ON")
                    circuit.showAsFeature = true;
                else
                    circuit.showAsFeature = false;
            }

            if (obj.params.FREEZE) {
                if (obj.params.FREEZE === "ON")
                    circuit.freezeProtection = true;
                else
                    circuit.freezeProtection = false;
            }

            if (obj.params.DNTSTP) {
                if (obj.params.DNTSTP === "ON")
                    circuit.disableEggTimer = true;
                else
                    circuit.disableEggTimer = false;
            }

            if (obj.params.TIME) {

                circuit.eggTimerHours = Math.floor(obj.params.TIME / 60);
                circuit.eggTimerMinutes = obj.params.TIME % 60;
            }
        }

        function appendPanel(panel, item) {
            var circuit = {};
            circuit.objectName = item.OBJNAM;                   // done
            circuit.hName = item.HNAME;
            circuit.sName = item.SNAME;
            circuit.listOrder = parseInt(item.LISTORD);
            circuit.subtype = item.SUBTYP;
            //Sometimes the FREEZE attribute does not show up

            circuit.freezeProtection = (item.FREEZE && item.FREEZE === 'ON');
            circuit.eggTimerHours = Math.floor(item.TIME / 60);
            circuit.eggTimerMinutes = item.TIME % 60;
            // need to get these from a different object
            circuit.showAsFeature = false;
            circuit.disableEggTimer = false;
            circuit.showAsFeature = (item.FEATR === 'ON') ? true : false;
            circuit.disableEggTimer = (item.DNTSTP === 'ON') ? true : false;

            panel.circuits.push(circuit);
        }

        function getOCPVersion() {   // return OCP version #
            var icversionfull = "";
            var version = 1.040;    // default

            if (!propertyModel.Objects._5451.VER) // check if 5451 object already exist.
                return version;

            icversionfull = propertyModel.Objects._5451.VER.split(',');  // THW Version: IC: 1.040 , ICWEB:2019-06-18 0.90.02
            if (icversionfull[0]) {   // THW Version: IC: 1.040 
                var icversion = icversionfull[0].split(' ');      // find the last space ' '  on   "THW Version: IC: 1.040"           
                if (icversion[1])   // 1.040 
                    version = Number(icversion[1]);
            }
            return version;
        }

        function updatePanels() {   // to support temporary broken panel list 

            var version = getOCPVersion();

            if ((version >= 1.025) && (version <= 1.039)) {
                updatePanelsVersion39();    //console.log("Loading updatePanelsVersion39");                
            } else {
                updatePanels_original();    //console.log("Loading updatePanels_original");
            }
        }

        function updatePanelsVersion39() {   // to support temporary broken panel list 
            updatePanels_original();

            var p2 = {
                hName: "PNL02",
                objectName: "PNL02",
                sName: "Panel 2",
                circuits: [],
            };
            var p3 = {
                hName: "PNL03",
                objectName: "PNL03",
                sName: "Panel 3",
                circuits: [],
            };
            var p4 = {
                hName: "PNL04",
                objectName: "PNL04",
                sName: "Panel 4",
                circuits: [],
            };
            var xcp = [false, false, false];
            angular.forEach($scope.property.Circuits, function (item) {
                switch (item.OBJNAM) {
                    case "C0014":
                    case "C0015":
                    case "C0017":
                    case "C0018":
                    case "C0018":
                    case "C0020":
                    case "C0021":
                    case "C0022":
                    case "C0023":
                    case "C0024":
                        appendPanel(p2, item);
                        xcp[0] = true;
                        break;

                    case "C0024":
                    case "C0026":
                    case "C0027":
                    case "C0028":
                    case "C0029":
                    case "C0030":
                    case "C0031":
                    case "C0032":
                    case "C0033":
                    case "C0034":
                        appendPanel(p3, item);
                        xcp[1] = true;
                        break;

                    case "C0035":
                    case "C0036":
                    case "C0037":
                    case "C0038":
                    case "C0039":
                    case "C0040":
                    case "C0041":
                    case "C0042":
                    case "C0043":
                    case "C0044":
                        appendPanel(p4, item);
                        xcp[2] = true;
                        break;

                }

            });
            var count = 1;
            // panel 2
            if (xcp[0] === true)
                $scope.panels[count++] = p2;

            // panel 3
            if (xcp[1] === true)
                $scope.panels[count++] = p3;

            // panel 4
            if (xcp[2] === true)
                $scope.panels[count++] = p4;

        }


        function updatePanels_original() {
            $scope.panels = $scope.property.hardwareDefinition.map(function (hPanel) {
                var panel = {};
                panel.objectName = hPanel.objnam;
                panel.hName = hPanel.params.HNAME;
                panel.sName = hPanel.params.SNAME;
                panel.circuits = hPanel.params.OBJLIST.map(function (hModule) {
                    var circuits = [];
                    if (hModule.params.CIRCUITS) {
                        circuits = circuits.concat(hModule.params.CIRCUITS
                            .filter(function (filterCircuit) {
                                // needed to only grab aux circuits here with the most recent API changes
                                if ((filterCircuit.params.HNAME && filterCircuit.params.HNAME.indexOf('AUX') >= 0) ||
                                    (filterCircuit.params.HNAME && filterCircuit.params.HNAME.indexOf('Spa') >= 0) ||
                                    (filterCircuit.params.HNAME && filterCircuit.params.HNAME.indexOf('Pool') >= 0)
                                    ) {
                                    return filterCircuit;
                                }
                            })
                            .map(function (hCircuit) {
                                var circuit = {};
                                circuit.objectName = hCircuit.objnam;
                                circuit.hName = hCircuit.params.HNAME;
                                circuit.sName = hCircuit.params.SNAME;
                                circuit.listOrder = parseInt(hCircuit.params.LISTORD);
                                circuit.subtype = hCircuit.params.SUBTYP;
                                //Sometimes the FREEZE attribute does not show up

                                circuit.freezeProtection = (hCircuit.params.FREEZE && hCircuit.params.FREEZE === 'ON');
                                circuit.eggTimerHours = Math.floor(hCircuit.params.TIME / 60);
                                circuit.eggTimerMinutes = hCircuit.params.TIME % 60;
                                // need to get these from a different object
                                circuit.showAsFeature = false;
                                circuit.disableEggTimer = false;
                                circuit.showAsFeature = (hCircuit.params.FEATR === 'ON') ? true : false;
                                circuit.disableEggTimer = (hCircuit.params.DNTSTP === 'ON') ? true : false;
                                return circuit;
                            })
                        );
                    }
                    return circuits;
                });
                // flatten the module array
                panel.circuits = [].concat.apply([], panel.circuits);
                panel.circuits.sort(function (a, b) {
                    /*
                    var keyA = a.hName,
                        keyB = b.hName;
                    if (keyA < keyB) return -1;
                    if (keyA > keyB) return 1;
                    */

                    //sort items per : listOrder
                    var keyA = a.listOrder,
                    keyB = b.listOrder;
                    if (keyA < keyB) return -1;
                    if (keyA > keyB) return 1;

                    return 0;
                });
                // panel.circuits = panel.circuits.map(function (circuit) {
                //   delete circuit.listOrder;
                //   return circuit;
                // });
                return panel;
            });

            if ($scope.panels.length > 0) {
                $scope.accordionStateByObjectNames[$scope.panels[0].objectName] = {};
                $scope.accordionStateByObjectNames[$scope.panels[0].objectName].open = true;
            }

            $scope.circuitNames = $scope.property.Circuits.map(function (circuit) {
                return circuit['SNAME'];
            });

            $scope.panelsBeforeEdit = angular.copy($scope.panels);
        }


        function updateFeatureCircuits() {
            // Call the API
            // propertyService.getCircuitsFull().then(function(result){//
            // Then filter it
            $scope.featureCircuits = $scope.property.Circuits.filter(function (circuit) {
                //if (circuit.FEATR && circuit.FEATR === 'ON' && circuit.RLY === '00000') { // adding relay unset check to exclude AUX circuits from the list
                if (circuit.FEATR && circuit.RLY === '00000') { // adding relay unset check to exclude AUX circuits from the list
                    return circuit;
                }
            });
            // Then map it
            $scope.featureCircuits = $scope.featureCircuits.map(function (circuit) {
                var mappedCircuit = {};
                mappedCircuit.objectName = circuit.OBJNAM;
                mappedCircuit.hName = circuit.HNAME;
                mappedCircuit.sName = circuit.SNAME;
                mappedCircuit.listOrder = parseInt(circuit.LISTORD);
                mappedCircuit.subtype = circuit.SUBTYP;
                mappedCircuit.freezeProtection = circuit.FREEZE.toUpperCase() !== 'OFF';
                mappedCircuit.eggTimerHours = Math.floor(circuit.TIME / 60);
                mappedCircuit.eggTimerMinutes = circuit.TIME % 60;
                mappedCircuit.showAsFeature = (circuit.FEATR === 'ON') ? true : false;
                mappedCircuit.disableEggTimer = (circuit.DNTSTP === 'ON') ? true : false;
                mappedCircuit.isSelected = false;
                mappedCircuit.relay = circuit.RLY;
                return mappedCircuit;
            });
            // Then sort it
            $scope.featureCircuits.sort(function (a, b) {
                var keyA = a.listOrder,
                    keyB = b.listOrder;
                if (keyA < keyB) return -1;
                if (keyA > keyB) return 1;
                return 0;
            });
            // Then snapshot it to revert from an edit
            $scope.featuredCircuitsBeforeEdit = angular.copy($scope.featureCircuits);
            // });
        }


        function checkIDuplicated(array, check) {
            var exists = false;
            angular.forEach(array, function (item) {
                if (item.hName === check.hName)
                    exists = true;
            });
            return exists;
        }

        function removeDuplicationHNAME(items) {
            var newItems = [];
            angular.forEach(items, function (item) {
                var duplicated = checkIDuplicated(newItems, item);
                if (duplicated === false)
                    newItems.push(item);
            });

            return newItems;
        }

        function updateCircuitGroups() {
            console.log(">>>> [start] updating group circuits...");
            $scope.circuitsGroups = $scope.property.hardwareDefinition.map(function (hPanel) {
                return hPanel.params.OBJLIST
                    .filter(function (objectItem) {
                        // needed to only grab aux circuits here with the most recent API changes
                        if (objectItem.params.SUBTYP && objectItem.params.SUBTYP === 'CIRCGRP') {
                            return objectItem;
                        }
                    })
                    .map(function (hCircuitGroup) {
                        var circuitGroup = {};
                        circuitGroup.OBJNAM = hCircuitGroup.objnam;
                        circuitGroup.disableEggTimer = (hCircuitGroup.params.DNTSTP === 'ON') ? true : false;
                        circuitGroup.eggTimerHours = Math.floor(hCircuitGroup.params.TIME / 60);
                        circuitGroup.eggTimerMinutes = hCircuitGroup.params.TIME % 60;
                        circuitGroup.hName = hCircuitGroup.params.HNAME;
                        circuitGroup.sName = hCircuitGroup.params.SNAME;
                        circuitGroup.isSelected = false;
                        circuitGroup.listOrder = parseInt(hCircuitGroup.params.LISTORD);
                        // some information for the circuit group lies within the config version of the object like SELECT.

                        var configCircuitGroup = $scope.property.CircuitGroups.filter(function (configCircuitGroup) {
                            if (configCircuitGroup.OBJNAM === circuitGroup.OBJNAM) {
                                return configCircuitGroup;
                            }
                        })[0];
                        circuitGroup.selectableCircuits = [];
                        if (configCircuitGroup) {
                            // FIXME: this gets set to undefined, when a subscription update happens, need to fix this.
                            var circuitList = (!configCircuitGroup.SELECT) ?
                            propertyModel.Circuits :
                            configCircuitGroup.SELECT.split(' ').map(function (circuitOBJNAM) {
                                var circuitResult = $scope.property.Circuits.filter(function (tempCircuit) {
                                    if (tempCircuit.OBJNAM === circuitOBJNAM) {
                                        return tempCircuit;
                                    }
                                })[0];
                                if (circuitResult) {
                                    return circuitResult;
                                } else {
                                    return { OBJNAM: circuitOBJNAM, SNAME: null };
                                }
                            }).filter(function (circuit) {
                                // ignore special circuits like all on and all off
                                if (circuit.OBJNAM !== '_A111' && circuit.OBJNAM !== '_A110') {
                                    return circuit;
                                }
                            });
                            circuitGroup.selectableCircuits = circuitList;
                            // don't show circuits that don't have an SNAME
                            circuitGroup.selectableCircuits = circuitGroup.selectableCircuits.filter(function (selectableCircuit) {
                                return selectableCircuit.SNAME !== null;
                            });
                        }
                        circuitGroup.circuits = [];
                        if (hCircuitGroup.params.OBJLIST) {
                            circuitGroup.circuits = hCircuitGroup.params.OBJLIST.map(function (hCircuit) {
                                var circuit = {};
                                circuit.affect = hCircuit.objnam;
                                circuit.OBJNAM = hCircuit.params.CIRCUIT;

                                circuit.eggTimerHours = Math.floor(hCircuit.TIME / 60);
                                circuit.eggTimerMinutes = hCircuit.TIME % 60;
                                circuit.disableEggTimer = (hCircuit.DNTSTP === 'ON') ? true : false;

                                var configCircuit = $scope.property.Circuits.filter(function (tempCircuit) {
                                    return tempCircuit.OBJNAM === circuit.OBJNAM;
                                })[0];
                                circuit.SNAME = (configCircuit && configCircuit.SNAME) ? configCircuit.SNAME : circuit.OBJNAM;
                                //circuit.isOn = hCircuit.params.ACT;
                                circuit.ACT = hCircuit.params.ACT;
                                circuit.listOrder = parseInt(hCircuit.params.LISTORD);
                                return circuit;
                            });
                        }
                        circuitGroup.circuits.sort(function (a, b) {
                            var keyA = a.listOrder,
                                keyB = b.listOrder;
                            if (keyA < keyB) return -1;
                            if (keyA > keyB) return 1;
                            return 0;
                        });
                        return circuitGroup;
                    });
            });
            // flatten the module array
            $scope.circuitsGroups = [].concat.apply([], $scope.circuitsGroups);

            // removing duplicated items
            $scope.circuitsGroups = removeDuplicationHNAME($scope.circuitsGroups);

            $scope.circuitsGroups.sort(function (a, b) {
                var keyA = a.listOrder,
                    keyB = b.listOrder;
                if (keyA < keyB) return -1;
                if (keyA > keyB) return 1;
                return 0;
            });
            $scope.circuitsGroups.OBJNAM = 'circuitsGroups';
            $scope.circuitsGroupsBeforeEdit = angular.copy($scope.circuitsGroups);

            console.log("<<< [stop] updating group circuits...");

            //$scope.printAllGroupCircuit();
        }



        $scope.changeCircuitsGroupCircuit = function (outterIndex, index, selectedCircuit) {
            //var isOn = $scope.circuitsGroups[outterIndex].circuits[index].isOn;
            $scope.circuitsGroups[outterIndex].circuits[index] = angular.copy(selectedCircuit);
            //$scope.circuitsGroups[outterIndex].circuits[index].isOn = isOn;

            // CHANGING THE CIRCUIT GROUP AFFECTED 
            $scope.circuitsGroups[outterIndex].circuits[index].OBJNAM = selectedCircuit.OBJNAM;
            $scope.circuitsGroups[outterIndex].circuits[index].SNAME = selectedCircuit.SNAME;
            $scope.circuitsGroups[outterIndex].circuits[index].listOrder = 1;
            $scope.circuitsGroups[outterIndex].circuits[index].ACT = 'ON';            

        }

    }
]);
