﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using Pentair.Desktop.Simulator.Models;

namespace Pentair.Desktop.Simulator.Views
{
    /// <summary>
    /// Interaction logic for SystemConfigView.xaml
    /// </summary>
    public partial class SystemConfigView : UserControl
    {
        private string _configurationName;

        public string HostName { get; set; }

        public string ConfigurationName
        {
            get { return _configurationName; }
            set 
            { 
                _configurationName = value;
                SelectedConfiguration.Text = value;
            }
        }
        #region Events

        public static readonly RoutedEvent ChangeHostEvent =
            EventManager.RegisterRoutedEvent("NotifyProperty",
                RoutingStrategy.Bubble,
                typeof(RoutedEventHandler),
                typeof(SystemConfigView));

        public event RoutedEventHandler ChangeHost
        {
            add { AddHandler(ChangeHostEvent, value); }
            remove { RemoveHandler(ChangeHostEvent, value); }
        }

        public static readonly RoutedEvent ChangeConfigurationEvent =
            EventManager.RegisterRoutedEvent("ConfigurationProperty",
                RoutingStrategy.Bubble,
                typeof(RoutedEventHandler),
                typeof(SystemConfigView));

        public event RoutedEventHandler ChangeConfig
        {
            add { AddHandler(ChangeConfigurationEvent, value);}
            remove { RemoveHandler(ChangeConfigurationEvent, value); }
        }

        public static readonly RoutedEvent NotifyEvent =
            EventManager.RegisterRoutedEvent("Notify",
                RoutingStrategy.Bubble,
                typeof (RoutedEventHandler),
                typeof (SystemConfigView));

        public event RoutedEventHandler Notify
        {
            add { AddHandler(NotifyEvent, value); }
            remove { RemoveHandler(NotifyEvent, value); }
        }

        #endregion
        public SystemConfigView()
        {
            InitializeComponent();
        }

        #region Event Handlers
        public void AddConfiguration_Click(object sender, RoutedEventArgs e)
        {
            RaiseEvent(new RoutedEventArgs(SystemConfigView.NotifyEvent, this));
        }

        public void HostSelect_Changed(object sender, RoutedEventArgs e)
        {
            HostName = SelectedHost.SelectedItem.ToString();
            RaiseEvent(new RoutedEventArgs(SystemConfigView.ChangeHostEvent, this));
        }

        public void ConfigurationSelect_Changed(object sender, RoutedEventArgs e)
        {
            _configurationName = ((KeyValuePair<string, string>)SelectedConfiguration.SelectedItem).Value;
            RaiseEvent(new RoutedEventArgs(SystemConfigView.ChangeConfigurationEvent, this));
        }
        #endregion

        private void HostSelect_Changed(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
