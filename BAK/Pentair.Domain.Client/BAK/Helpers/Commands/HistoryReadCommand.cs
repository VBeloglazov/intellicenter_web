﻿using System.Collections.Generic;

using Newtonsoft.Json;

namespace Pentair.Domain.Client.Helpers.Commands
{
    public class HistoryReadCommand : CommandWithStartAndEndTime
    {
        public HistoryReadCommand(
            string messageID,
            string timeStart,
            string timeEnd)
            : base(CommandString.ReadHistoryMessage, messageID, timeStart, timeEnd)
        {
        }

    } // class HistoryReadCommand
}
