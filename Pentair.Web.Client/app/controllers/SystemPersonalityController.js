﻿'use strict';
var PWC = PWC || {};
PWC.controller('SystemPersonalityController', [
    '$scope', '$location', 'PropertyService', 'PropertyModel',
    function ($scope, $location, propertyService, propertyModel) {
        $scope.Property = propertyService;
        $scope.Model = propertyModel;
        $scope.EditMode = false;
        $scope.$parent.Layout = "layout-base";

        $scope.GoToConfigPage = function () {
            $location.path("/configure");
        }

        $scope.RefreshSystemPersonality = function () {
            //propertyService.GetPanels();
        }

        $scope.ModuleCapabilities = function(module) {
            var objects = [];
            switch (module) {
                case "I5P":
                    objects = ['I5P1', 'I5P2', 'I5P3', 'I5P4', 'I5P5', 'I5P6', 'I5P7', 'I5P8'];
                    break;
                case "I5X":
                    objects = ['I5X1', 'I5X2'];
                    break;
                case "I5PS":
                    objects = ['I5PS1', 'I5PS2', 'I5PS3'];
                    break;
                case "I8PS":
                    objects = ['I8PS1', 'I8PS2', 'I8PS3', 'I8PS4'];
                    break;
                case "I8P":
                    objects = ['I8P1'];
                    break;
                case "I10D":
                    objects = ['I10D1', 'I10D2', 'I10D3', 'I10D4', 'I10D5', 'I10D6', 'I10D7'];
                    break;
                case "I10PS":
                    objects = ['I10PS1', 'I10PS2', 'I10PS3', 'I10PS4'];
                    break;
                case "VALVEXP":
                    objects = ['VALVE_EXP'];
                    break;
                case "ANGLEXP":
                    objects = ['ANALOG_EXP'];
                    break;
            }
            return objects;
        }
    }
]);