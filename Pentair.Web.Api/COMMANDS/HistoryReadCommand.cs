﻿using System.Collections.Generic;

using Newtonsoft.Json;

namespace Pentair.Web.Api
{
    public class HistoryReadCommand : CommandWithStartAndEndTime
    {
        public HistoryReadCommand(
            string messageID,
            string timeStart,
            string timeEnd)
            : base(CommandString.ReadHistoryMessage, messageID, timeStart, timeEnd)
        {
        }

    } // class HistoryReadCommand
}
