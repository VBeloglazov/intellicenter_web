﻿'use strict';
//TODO: Non THW traffic should be over HTTP since promises are no longer enabled.
var PWC = PWC || {};
PWC.controller('ManageUsersController', [
    '$scope', '$location', '$filter', 'PropertyService', 'AuthService', 'PropertyModel', 'Notification',
    function ($scope, $location, $filter, propertyService, authService, propertyModel, Notification) {
        propertyService.DeleteUserError = false;
        propertyService.EditSession = false;
        $scope.SortDirection = "asc";
        $scope.SortField = "email";
        $scope.Filter = null;
        $scope.UserList = [];
        $scope.Model = propertyModel;
        $scope.Property = propertyService;
        $scope.Authentication = authService;
        $scope.Security = propertyModel.Security;
        $scope.Property = propertyService;
        $scope.EditRow = null;
        $scope.SecurityObject = null;
        $scope.EditFilter = false;
        $scope.EditUser = null;
        $scope.filters = [];
        $scope.filterSelect = false;
        $scope.ConfirmDelete = false;
        $scope.SendNotification = false;
        $scope.AdminCount = propertyModel.ListCount;

        (function () {
            var all = { label: 'ALL', value: $filter("translate")("FILTER_ALL") }
            $scope.filters.push(all);
            $scope.Filter = all;
            var accepted = { label: 'ACCEPTED', value: $filter("translate")("FILTER_ACCEPTED") }
            $scope.filters.push(accepted);
            var bounced = { label: 'BOUNCED', value: $filter("translate")("FILTER_BOUNCED") }
            $scope.filters.push(bounced);
            var expired = { label: 'EXPIRED', value: $filter("translate")("FILTER_EXPIRED") }
            $scope.filters.push(expired);
            var noResponse = { label: 'UNKNOWN', value: $filter("translate")("FILTER_NO_RESPONSE") }
            $scope.filters.push(noResponse);
        }());

        fetchUsers();

          $scope.errorSave = function(errorMessage) {
            Notification.error({message: 'Failed!' + ' ' + errorMessage, delay: 1000});
          };

          $scope.successSave = function() {
            Notification.success({message: 'Saved!', delay: 1000});
          };

          $scope.successCancel = function() {
            Notification.success({message: 'Cancelled!', delay: 1000});
          };

        $scope.UserStatus = function (status) {
            var result = $filter("translate")("FILTER_NO_RESPONSE");
            switch (status) {
                case 'BOUNCED':
                    result = $filter("translate")("FILTER_BOUNCED");
                    break;
                case 'ACCEPTED':
                    result = $filter("translate")("FILTER_ACCEPTED");
                    break;
                case 'EXPIRED':
                    result = $filter("translate")("FILTER_EXPIRED");
                    break;
                default:
                    break;
            }
            return result;
        }

        $scope.UserDeletes = function () {
            /*
            if ($scope.EditUser) {
                return false;
            }
            */
            var i = 0;
            $scope.UserList.forEach(function (item) {
                if (item.deleteFlag) {
                    i++;
                }
            });
            return i > 0;
        };

        $scope.$on('UserUpdateError', function (event, message) {
        });

        $scope.$on('UserUpdated', function (event, message) {
            fetchUsers();
        });

        $scope.$on('UsersRefreshed', function (event, message) {
            refreshList();
        });

        $scope.$on('SecurityUpdated', function (event, message) {
          $scope.Security = propertyModel.Security;
        })

        $scope.$on('UsersNotificationsUpdated', function (event, message) {
        });

        $scope.Sort = function (column) {
            if ($scope.SortField === column) {
                $scope.SortDirection = $scope.SortDirection === 'asc' ? 'desc' : 'asc';
            } else {
                $scope.SortField = column;
            }
            fetchUsers();
        };

        $scope.Resend = function (user) {
            propertyService.ResendInvite(user);
        };

        $scope.AddUser = function () {
            var newUser = {
                email: '',
                id: 'new',
                level: '',
                status: $filter('translate')('FILTER_NO_RESPONSE'),
                open: true
            };
            $scope.UserList.push(newUser);
            $scope.EditUser = newUser;
            $scope.ExpandItem(newUser);
        };

      $scope.ExpandItem = function (user) {
        if (user) {
          $scope.SetEditUser(user);
        }
        // event.stopPropagation();
      };

        $scope.SetEditUser = function (user) {
            if (user) {
                $scope.EditUser = user;
                for (var i = 0; i < $scope.Security.length; i++) {
                    if ($scope.Security[i].value === user.securityToken) {
                        $scope.SecurityObject = $scope.Security[i];
                    }
                }
            }
        };

        $scope.CancelEdit = function () {
            $scope.SecurityObject = null;
            refreshList();
            $scope.successCancel();
        };

        $scope.SetDeleteFlag = function (user) {
            if (user) {
                user.deleteFlag = !user.deleteFlag;
            }
        };

        $scope.SelectSecurity = function (selection) {
            if (selection) {
                $scope.EditUser.level = selection.name;
                $scope.SecurityObject = selection;
            }
        };

        $scope.SaveEdit = function (user) {
            if (user && $scope.SecurityObject) {
                propertyService.EditUser(user.email, $scope.SecurityObject.value);
                $scope.EditUser = null;
                $scope.SecurityObject = null;
                fetchUsers();
                $scope.successSave();
            }
        };

        $scope.ValidateInvite = function (user) {
            return (user.email && $scope.SecurityObject)
        };

        $scope.CreateInvite = function (user) {
            if (user && $scope.SecurityObject) {
                propertyService.InviteUser(user.email, $scope.SecurityObject.value);
                $scope.EditUser = null;
                $scope.SecurityObject = null;
                fetchUsers();
                $scope.successSave();
            }
        };

        $scope.DeleteUsers = function () {
            $scope.ConfirmDelete = true;
        };

        $scope.ContinueDelete = function(choice) {
            $scope.ConfirmDelete = false;
            if (choice) {
                $scope.SendNotification = true;
            }
        };

        $scope.DoTheDelete = function(continueFlag, notify) {
            if (continueFlag) {
                $scope.UserList.forEach(function(user) {
                    if (user.deleteFlag) {
                        propertyService.DeleteUser(user.email, user.status, notify);
                    }
                });
            }
            $scope.SendNotification = false;
            fetchUsers();
        };

        $scope.UserIsExpendable = function (user) {
            if (user) {
                if (user.email === $scope.Authentication.authentication.userName) {
                    return false;
                } else {
                    return user.Level !== 'ADMIN' || $scope.AdminCount > 1;
                }
            }
            return false;
        };

        $scope.Search = function () {
            $scope.FilterUsers();
        };

        $scope.ClearSearchTerm = function () {
            $scope.SearchTerm = "";
            $scope.FilterUsers();
        };

        $scope.SelectFilter = function (filter) {
            $scope.Filter = filter;
            $scope.filterSelect = false;
            fetchUsers();
        };

        function fetchUsers() {
            return propertyService.GetUserList($scope.Filter.label, $scope.SearchTerm, $scope.SortField, $scope.SortDirection);
        };


        function refreshList() {
            $scope.UserList = [];
            propertyModel.Users.forEach(function (user) {
                var newUser = {
                    id: user.id,
                    email: user.email,
                    status: $scope.UserStatus(user.status),
                    level: user.level,
                    securityToken: user.securityToken,
                    deleteFlag: false,
                    open: false
                };
                $scope.UserList.push(newUser);
            });
        };
    }
]);