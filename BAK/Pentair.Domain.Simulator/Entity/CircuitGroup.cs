﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pentair.Domain.Simulator.Entity
{
    public class CircuitGroup
    {
        [Key]
        public int ID { get; set; }
        public string Name { get; set; }
        public UInt32 LastRevision { get; set; }
        public int Delay { get; set; }
        public int ListPosition { get; set; }

        // Foreign Keys
        public int TypeID { get; set; }
        public int ParentCircuitGroupID { get; set; }
        public int CurrentCircuitID { get; set; }
        public int CircuitAffectID { get; set; }    // Light Mode
        public int ColorID { get; set; }            // Light Mode
        public int SwimStartID { get; set; }        // Light Mode
        public int? SystemConfigID { get; set; }
    }
}