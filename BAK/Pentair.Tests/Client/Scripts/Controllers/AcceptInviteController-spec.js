﻿describe('AcceptInviteController', function () {
    var Controller;
    var PropertyService;
    var $scope;
    var $location;
    var $controller;
    var SocketService;
    var routeParams;
    var Enumerations;
    var AppService;
    var $httpBackend;
    var AuthService;
    var fakeRoute;
    var HTTP_SERVICE_URL;

    beforeEach(angular.mock.module('PentairWebClient'));

    beforeEach(function() {
        inject(function (_$controller_, _PropertyService_, _$rootScope_, _$location_, _$httpBackend_,
                         _SocketService_, _Enumerations_, _AppService_, _AuthService_, _SERVICE_HOST_NAME_) {
            PropertyService = _PropertyService_;
            $scope = _$rootScope_.$new();
            $location = _$location_;
            $controller = _$controller_;
            $httpBackend = _$httpBackend_;
            SocketService = _SocketService_;
            Enumerations = _Enumerations_;
            AppService = _AppService_;
            AuthService = _AuthService_;
            HTTP_SERVICE_URL = 'https://' + _SERVICE_HOST_NAME_;
            routeParams = { inviteId: "1-1-1-1" };

            fakeRoute = {
                current: {
                    locals: {
                        inviteInfo: {
                            data: {
                                PropertyName: 'Pentair Pool',
                                Email: 'UnitTest@Pentair.com'
                            }
                        }
                    }
                }
            };

            AuthService.authentication.isAuth = false;
            Controller = $controller("AcceptInviteController", { $scope: $scope, $routeParams: routeParams, $route: fakeRoute });
        })
    });

    describe('Default Vars', function () {
        it('should have the following default variables', function () {

            expect($scope.Property).toBe(PropertyService);
            expect($scope.PermissionGroups).toBe(Enumerations.PermissionGroups);
            expect($scope.InviteError).toBe(false);
            expect($scope.InvalidInvite).toBe(false);

            expect($scope.AcceptInviteData).toEqual({
                email: "UnitTest@Pentair.com",
                password: "",
                confirmPassword: "",
                propertyName: "Pentair Pool",
                inviteId: '1-1-1-1'
            });

        });

        it('should set InvalidInvitId to true if an  invite was not returned', function () {
            fakeRoute.current.locals.inviteInfo = { data: { error: "No Invite Found" } };
            Controller = $controller("AcceptInviteController", { $scope: $scope, $routeParams: routeParams, $route: fakeRoute });

            expect($scope.InvalidInvite).toBe(true);
        });
    });

    describe('AcceptInvite', function () {
        beforeEach(function () {
            spyOn(AppService, 'PostData').and.callThrough();
            spyOn($location, 'path');
            $scope.AcceptInviteData = {
                email: "UnitTest@Pentair.com",
                password: "Password",
                confirmPassword: "Password",
                propertyName: "Pentair Pool",
                inviteId: '1-1-1-1'
            }
        });
        it('should send data to AccpetInvite Api endpoint and redirect to login', function () {
            $httpBackend.expectPOST(HTTP_SERVICE_URL + 'Api/Account/AcceptInvite').respond(200, { "Email": "UnitTest@Pentair.com", "UserName": "UnitTest@Pentair.com", "InstallationId": "2-2-2-2" });
            var postdata = {
                data: {
                    Email: "UnitTest@Pentair.com",
                    Password: "Password",
                    ConfirmPassword: "Password",
                    InviteId: '1-1-1-1'
                }
            }
            $scope.AcceptInvite();
            $httpBackend.flush();
            expect(AppService.PostData).toHaveBeenCalledWith('Api/Account/AcceptInvite', postdata.data);
            expect(AuthService.authentication.lastUserName).toBe("UnitTest@Pentair.com");
            expect(AuthService.authentication.accountCreated).toBe(true);
            expect($location.path).toHaveBeenCalledWith('/login');
            expect($scope.InviteError).toBe(false);

        });

        it('should not redirect user to login on error', function () {
            $httpBackend.expectPOST(HTTP_SERVICE_URL + 'Api/Account/AcceptInvite').respond(403);
            var postdata = {
                Email: "UnitTest@Pentair.com",
                Password: "Password",
                ConfirmPassword: "Password",
                InviteId: '1-1-1-1'
            }
            $scope.AcceptInvite();
            $httpBackend.flush();
            expect(AppService.PostData).toHaveBeenCalledWith('Api/Account/AcceptInvite', postdata);
            expect(AuthService.authentication.accountCreated).toBe(false);
            expect(AuthService.authentication.lastUserName).toBe("");
            //  expect($location.path).not.toHaveBeenCalled();
            expect($scope.InviteError).toBe(true);
        });
    });


    describe('toggle Eula', function () {
        it('should hide the Eula if it is shown', function () {
            $scope.ShowEula = true;
            $scope.ToggleEula();
            expect($scope.ShowEula).toBe(false);
        });

        it('should show the Eula if it is hidden', function () {
            $scope.ShowEula = false;
            $scope.ToggleEula();
            expect($scope.ShowEula).toBe(true);
        });
    });


    describe('validate a passwords minimum requirements', function () {
        it('should require six characters with no capital', function () {
            $scope.AcceptInviteData.password = 'password';
            expect($scope.validatePassword()).toBe(false);
        });
        it('should require six characters with one capital', function () {
            $scope.AcceptInviteData.password = 'Password';
            expect($scope.validatePassword()).toBe(true);
        });
        it('should require six characters with one capital and numbers', function () {
            $scope.AcceptInviteData.password = 'Password98';
            expect($scope.validatePassword()).toBe(true);
        });
        it('should require six characters with no password', function () {
            $scope.AcceptInviteData.password = '';
            expect($scope.validatePassword()).toBe(false);
        });
    });

    describe('verify confirmation password', function () {
        beforeEach(function () {
            $scope.AcceptInviteData = {
                password: "",
                confirmPassword: ""
            }
            $scope.AcceptInvitationForm = {
                password: { $invalid: true },
                confirmPassword: { $invalid: true }
            }
        });
        it('should be invalid new password', function () {
            expect($scope.verifyConfirmationPassword()).toBe(false);
        });
        it('should be invalid confirm password', function () {
            $scope.AcceptInviteData.password = 'Password';
            expect($scope.verifyConfirmationPassword()).toBe(false);
        });
        it('should be invalid new password', function () {
            $scope.AcceptInviteData.confirmPassword = 'Password';
            expect($scope.verifyConfirmationPassword()).toBe(false);
        });
        it('should be valid confirm password', function () {
            spyOn($scope, 'validatePassword').and.returnValue(true);
            $scope.AcceptInvitationForm.confirmPassword.$invalid = false;
            $scope.AcceptInviteData.confirmPassword = 'Password';
            $scope.AcceptInviteData.password = 'Password';
            expect($scope.verifyConfirmationPassword()).toBe(true);
        });
        it('should be invalid confirm password', function () {
            spyOn($scope, 'validatePassword').and.returnValue(true);
            $scope.AcceptInvitationForm.confirmPassword.$invalid = false;
            $scope.AcceptInviteData.confirmPassword = 'Password';
            $scope.AcceptInviteData.password = 'Passwird';
            expect($scope.verifyConfirmationPassword()).toBe(false);
        });
    });

    describe('Validate form', function () {
        it('should be valid when password and confirmation is valid', function () {
            spyOn($scope, 'validatePassword').and.returnValue(true);
            spyOn($scope, 'verifyConfirmationPassword').and.returnValue(true);

            expect($scope.validateForm()).toBe(true);
        });

        it('should not be valid when password is invalid and confirmation is valid', function () {
            spyOn($scope, 'validatePassword').and.returnValue(false);
            spyOn($scope, 'verifyConfirmationPassword').and.returnValue(true);

            expect($scope.validateForm()).toBe(false);
        });

        it('should not be valid when password is valid and confirmation is invalid', function () {
            spyOn($scope, 'validatePassword').and.returnValue(true);
            spyOn($scope, 'verifyConfirmationPassword').and.returnValue(false);

            expect($scope.validateForm()).toBe(false);
        });
    });

    describe('Existing Account', function () {
        describe('toggle existing Account', function () {
            it('should hide the Eula if it is shown', function () {
                $scope.ShowExistingAcct = true;
                $scope.ToggleExistingAcct();
                expect($scope.ShowExistingAcct).toBe(false);
            });

            it('should show the Eula if it is hidden', function () {
                $scope.ShowExistingAcct = false;
                $scope.ToggleExistingAcct();
                expect($scope.ShowExistingAcct).toBe(true);
            });
        });

        describe('AttachToExistingAcct', function () {
            beforeEach(function () {
                spyOn(AppService, 'PostData').and.callThrough();
                spyOn($location, 'path');
                spyOn(PropertyService, 'Connect');
                spyOn(PropertyService, 'FetchInstallations');
                $scope.LoginData = {
                    email: "UnitTest@Pentair.com",
                    password: "Password",
                    inviteId: '1-1-1-1'
                }
            });
            it('should send data to AccpetInvite Api endpoint and automatically login and connect with valid login data', function () {
                var loginData = "grant_type=password&username=" + $scope.LoginData.email + "&password=" + $scope.LoginData.password;
                var loginResponse = { access_token: "A57F5BFC-2022-4227-8562-FD22B3AD64BE" };
                $httpBackend.expectPOST(HTTP_SERVICE_URL + 'login', loginData).respond(loginResponse);

                $httpBackend.expectPOST(HTTP_SERVICE_URL + 'Api/Account/AcceptInvite').respond(200, { "Email": "UnitTest@Pentair.com", "InstallationId": "2-2-2-2" });
                var postdata = {
                    Email: "UnitTest@Pentair.com",
                    Password: "Password",
                    ConfirmPassword: "Password",
                    InviteId: '1-1-1-1'
                }
                $scope.AttachToExistingAcct();
                $httpBackend.flush();
                expect(PropertyService.Connect).toHaveBeenCalled();
                expect(PropertyService.SelectedInstallation).toBe('2-2-2-2');
                expect(PropertyService.FetchInstallations).toHaveBeenCalled();
                expect(AppService.PostData).toHaveBeenCalledWith('Api/Account/AcceptInvite', postdata);
                expect($scope.InviteError).toBe(false);

            });

            it('should raise flag on login error', function () {
                var loginData = "grant_type=password&username=" + $scope.LoginData.email + "&password=" + $scope.LoginData.password;
                $httpBackend.expectPOST(HTTP_SERVICE_URL + 'login', loginData).respond(500);
                $scope.AttachToExistingAcct();
                $httpBackend.flush();
                expect($scope.ExistingInviteLoginError).toBe(true);

            });

            it('should raise flag on in invite error', function () {
                var loginResponse = { access_token: "A57F5BFC-2022-4227-8562-FD22B3AD64BE" };
                var loginData = "grant_type=password&username=" + $scope.LoginData.email + "&password=" + $scope.LoginData.password;

                $httpBackend.expectPOST(HTTP_SERVICE_URL + 'login', loginData).respond(loginResponse);
                $httpBackend.expectPOST(HTTP_SERVICE_URL + 'Api/Account/AcceptInvite').respond(403);
                var postdata = {
                    Email: "UnitTest@Pentair.com",
                    Password: "Password",
                    ConfirmPassword: "Password",
                    InviteId: '1-1-1-1'
                }
                $scope.AttachToExistingAcct();
                $httpBackend.flush();
                expect(AppService.PostData).toHaveBeenCalledWith('Api/Account/AcceptInvite', postdata);
                expect($scope.ExistingInviteError).toBe(true);
            });
        });

        describe('Auto Associate with Existing Account while logged in as invited user', function () {
            beforeEach(function () {
                AuthService.authentication.isAuth = true;
                AuthService.authentication.userName = "UnitTest@Pentair.com";
                $httpBackend.expectPOST(HTTP_SERVICE_URL + 'Api/Account/AcceptInvite').respond(200, { "Email": "UnitTest@Pentair.com", "InstallationId": "2-2-2-2" } );

                spyOn(PropertyService, 'Connect');
                spyOn(PropertyService, 'FetchInstallations');
                spyOn($location, 'path');
                Controller = $controller("AcceptInviteController", { $scope: $scope, $routeParams: routeParams, $route: fakeRoute });

            });
            it('should send data to AccpetInvite Api endpoint and automatically login and connect with valid login data', function () {

                $httpBackend.flush();

                expect(PropertyService.Connect).toHaveBeenCalled();
                expect(PropertyService.SelectedInstallation).toBe('2-2-2-2');
                expect(PropertyService.FetchInstallations).toHaveBeenCalled();
                expect($scope.InviteError).toBe(false);
            });
        });

        describe('AutoConnectToExistingAcct with invalid token', function () {
            beforeEach(function () {
                AuthService.authentication.isAuth = true;
                AuthService.authentication.userName = "UnitTest@Pentair.com";
                $httpBackend.expectPOST(HTTP_SERVICE_URL + 'Api/Account/AcceptInvite').respond(500);
                Controller = $controller("AcceptInviteController", { $scope: $scope, $routeParams: routeParams, $route: fakeRoute });

                spyOn(AuthService, 'logOut');

            });

            it('should raise flag on in invite error', function () {

                $httpBackend.flush();
                expect(AuthService.logOut).toHaveBeenCalled();
            });
        });

        describe('Auto Associate with Existing Account while logged in as a diffrent user', function () {
            beforeEach(function () {
                AuthService.authentication.isAuth = true;
                AuthService.authentication.userName = "DIff@Pentair.com";
                spyOn(AuthService, 'resetAuthData');
                spyOn(PropertyService, 'Connect');
                spyOn($location, 'path');
                Controller = $controller("AcceptInviteController", { $scope: $scope, $routeParams: routeParams, $route: fakeRoute });

            });
            it('should log the user out and remain on the Accept invite page', function () {
                expect(AuthService.resetAuthData).toHaveBeenCalledWith(true);
                expect(PropertyService.Connect).not.toHaveBeenCalled();
            });
        });
    });

    describe('refuse Eula', function () {

        it('should hide EULA and raid flag', function () {
            $scope.RefusedEula();
            expect($scope.ShowEula).toBe(false);
            expect($scope.EulaRefused).toBe(true);
        });
    });
});