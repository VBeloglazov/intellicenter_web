﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Pentair.Web.Api
{
    public class HistoryWriteLastTimeResponse : ResponseCommand
    {
        [JsonProperty("lastHistoryTime", Required = Required.Always)]
        public string LastHistoryTime { get; private set; }

        public HistoryWriteLastTimeResponse(
            string messageID,
            string response,
            long lastHistoryTime
            )
            : base(CommandString.WriteLastHistoryTime, messageID, response)
        {
            this.LastHistoryTime = lastHistoryTime.ToString();

        } // HistoryWriteLastTimeResponse()

        /// <summary>
        /// Always returns an object, valid or not. If invalid, check the DeserializationError property value.
        /// </summary>
        public static new HistoryWriteLastTimeResponse Deserialize(
            string serializedMessage)
        {
            HistoryWriteLastTimeResponse command = null;

            try
            {
                command = JsonConvert.DeserializeObject<HistoryWriteLastTimeResponse>(serializedMessage);
                command.IsValid = true;
            }
            catch (Exception e)
            {
                // Deserialization as HistoryWriteLastTimeResponse failed

                StringBuilder sbError = new StringBuilder();

                sbError.Append("Cannot deserialize as HistoryWriteLastTimeResponse. " + e.Message);

                // Try to get at least the base message data
                ResponseCommand baseCommand = ResponseCommand.Deserialize(serializedMessage);

                if (!baseCommand.IsValid)
                {
                    sbError.Append("\n" + baseCommand.DeserializationError);
                }

                command = new HistoryWriteLastTimeResponse(
                    messageID: baseCommand.MessageID,
                    response: ResponseString.BadRequest,
                    lastHistoryTime: 0L)
                {
                    IsValid = false,
                    DeserializationError = sbError.ToString()
                };
            }

            return command;

        } // Deserialize()

    } // class HistoryWriteLastTimeResponse
}
