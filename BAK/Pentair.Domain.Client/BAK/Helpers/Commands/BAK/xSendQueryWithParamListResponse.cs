﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Pentair.Domain.Client.Helpers.Commands
{
    // TODO: 2b removed, not in use.
    /*
    public class SendQueryWithParamListResponse : ResponseCommand
    {
        [JsonProperty("answer", Required = Required.Always)]
        public List<ParamObject> Answer { get; private set; }

        public SendQueryWithParamListResponse(
            string messageID,
            string response,
            List<ParamObject> answer
            )
            : base(CommandString.SendQuery, messageID, response)
        {
            this.Answer = answer;

        } // SendQueryWithParamListResponse()

        /// <summary>
        /// Always returns an object, valid or not. If invalid, check the DeserializationError property value.
        /// </summary>
        public static new SendQueryWithParamListResponse Deserialize(
            string serializedMessage)
        {
            SendQueryWithParamListResponse command = null;

            try
            {
                command = JsonConvert.DeserializeObject<SendQueryWithParamListResponse>(serializedMessage);
                command.IsValid = true;
            }
            catch (Exception e)
            {
                // Deserialization as SendQueryWithParamListResponse failed

                StringBuilder sbError = new StringBuilder();

                sbError.Append("Cannot deserialize as SendQueryWithParamListResponse. " + e.Message);

                // Try to get at least the base message data
                ResponseCommand baseCommand = ResponseCommand.Deserialize(serializedMessage);

                if (!baseCommand.IsValid)
                {
                    sbError.Append("\n" + baseCommand.DeserializationError);
                }

                command = new SendQueryWithParamListResponse(
                    messageID: baseCommand.MessageID,
                    response: ResponseString.BadRequest,
                    answer: null)
                {
                    IsValid = false,
                    DeserializationError = sbError.ToString()
                };
            }

            return command;

        } // Deserialize()

    } // class SendQueryWithParamListResponse
    */
}
