﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Pentair.Web.Api
{
    public class CommandWithTimeSpan : CommandBase
    {
        [JsonProperty("timeSince")]
        public string TimeSince { get; private set; }

        public CommandWithTimeSpan(
            string command,
            string messageID,
            string timeSince
            )
            : base(command, messageID)
        {
            this.TimeSince = timeSince;
        }

    } // class CommandWithTimeSpan
}
