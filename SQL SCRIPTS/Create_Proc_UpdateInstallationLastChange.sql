USE [PathfinderIntellicenter]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (
	SELECT * FROM sys.objects
	WHERE object_id = OBJECT_ID(N'dbo.pr_UpdateInstallationLastChange')
	AND type in (N'P', N'PC')
)
DROP PROCEDURE dbo.pr_UpdateInstallationLastChange
GO


-- =============================================
-- Author			:	Wayne Hui
-- Create date		:	02/13/2019
-- Parameters		:
--	@InstallationId	:	Device ID
--  @TimeNow		:	last change time
-- Description		:	Update the last change time
--
-- Modified By			Date		Description
-- =============================================
CREATE PROCEDURE [dbo].[pr_UpdateInstallationLastChange]
	@InstallationId	INT
	, @TimeNow		BIGINT

AS
BEGIN

	SET NOCOUNT OFF

	UPDATE dbo.Installations 
	SET LastChange = @TimeNow
    WHERE 
		InstallationId = @InstallationId


END
GO


--GRANT EXECUTE ON [dbo].[pr_UpdateInstallationLastChange] TO PUBLIC
--GO

