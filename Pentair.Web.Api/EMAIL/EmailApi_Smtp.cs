﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Mail;

using SERVICE_SHARED;

namespace Pentair.Web.Api
{
    static class EmailApi_Smtp
    {
        private const string EVENT_SOURCE = "EmailApi_Smtp";

        private static string addressFrom;
        private static string smtpHost;
        private static int smtpPort;
        private static string smtpUserName;
        private static string smtpPassword;
        private static bool smtpEnableSsl;

        private static SmtpClient smtpClient;

        static EmailApi_Smtp()
        {
            EmailApi_Smtp.addressFrom = Properties.Settings.Default.EmailAddressFrom;
            EmailApi_Smtp.smtpHost = Properties.Settings.Default.SmtpHost;
            EmailApi_Smtp.smtpPort = Properties.Settings.Default.SmtpPort;
            EmailApi_Smtp.smtpUserName = Properties.Settings.Default.EmailAddressFrom;
            EmailApi_Smtp.smtpPassword = Properties.Settings.Default.SmtpPassword;
            EmailApi_Smtp.smtpEnableSsl = Properties.Settings.Default.SmtpEnableSsl;

            EmailApi_Smtp.smtpClient = new SmtpClient()
            {
                Host = EmailApi_Smtp.smtpHost,
                Port = EmailApi_Smtp.smtpPort,
                Credentials = new NetworkCredential(EmailApi_Smtp.smtpUserName, EmailApi_Smtp.smtpPassword),
                EnableSsl = EmailApi_Smtp.smtpEnableSsl
            };

        } // static EmailApi_Smtp()

        /// <exception cref="Exception" />
        public static void SendEmail(
            string subject,
            string body,
            string addressTo,
            bool isBodyHtml)
        {
            MailMessage mailMessage = new MailMessage()
            {
                From = new MailAddress(EmailApi_Smtp.addressFrom),
                Subject = subject,
                Body = body,
                IsBodyHtml = isBodyHtml
            };

            mailMessage.To.Add(addressTo);

            EmailApi_Smtp.smtpClient.Send(mailMessage);

        } // SendEmail()

    } // class EmailApi_Smtp
}
