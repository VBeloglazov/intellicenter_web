﻿using System.Collections.Generic;
using System.Linq;
using System.Web;

using Newtonsoft.Json;

namespace Pentair.Web.Api
{
    public class KeyCommand : CommandBase
    {
        [JsonProperty("condition")]
        public string Condition { get; private set; }

        [JsonProperty("objectList")]
        public List<KeyObject> ObjectList { get; private set; }

        public KeyCommand(
            string command,
            string messageID,
            string condition,
            List<KeyObject> objectList
            )
            : base(command, messageID)
        {
            this.Condition = condition;
            this.ObjectList = objectList;
        }

    } // class KeyCommand
}