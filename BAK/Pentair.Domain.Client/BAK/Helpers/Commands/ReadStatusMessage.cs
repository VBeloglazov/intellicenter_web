﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pentair.Domain.Client.Helpers.Commands
{
    public class ReadStatusMessage : CommandWithTimeSpan
    {
        public ReadStatusMessage(
            string messageID,
            string lastTime
            )
            : base(CommandString.ReadStatusMessage, messageID, timeSince: lastTime)
        {
        } // ReadStatusMessage()

    } // class ReadStatusMessage
}
