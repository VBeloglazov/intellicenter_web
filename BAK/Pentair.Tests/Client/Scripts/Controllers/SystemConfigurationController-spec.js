﻿describe('SystemConfigurationController', function() {
    var Controller;
    var $scope;
    var $controller;
    var $httpBackend;
    var propertyService;
    var $filter;
    var $location;
    var propertyModel;
    var messageProcessor;
    var deferred;
    beforeEach(module('PentairWebClient'));

    beforeEach(inject(function(_$controller_, _$rootScope_, _$httpBackend_, _$filter_, _$location_, _$q_, _PropertyService_, _PropertyModel_, _MessageProcessor_) {
        $scope = _$rootScope_.$new();
        $controller = _$controller_;
        $httpBackend = _$httpBackend_;
        propertyService = _PropertyService_;
        propertyModel = _PropertyModel_;
        propertyModel.Objects["M1000"] = { OBJNAM: "M1000", OBJTYP: "PANEL", SUBTYP: "Main" }
        propertyModel.Objects["m1000"] = { OBJNAM: "m1000", OBJTYP: "MODULE", SUBTYP: "i5P", PARENT: "M1000" }
        propertyModel.Objects["r1000"] = { OBJNAM: "r1000", OBJTYP: "RLY", PARENT: "m1000" }
        propertyModel.Objects["r1001"] = { OBJNAM: "r1001", OBJTYP: "RLY", PARENT: "m1000" }
        propertyModel.Panels[0] = { OBJNAM: "M1000" }
        propertyModel.Relays[0] = { OBJNAM: "r1000", PARENT: "m1000" }
        $location = _$location_;
        $filter = _$filter_;
        messageProcessor = _MessageProcessor_;
        $q = _$q_;
        deferred = $q.defer();

        messageProcessor.ProcessNotification(JSON.parse(SpecHelper.JSONdata.Bodies));
        messageProcessor.ProcessNotification(JSON.parse(SpecHelper.JSONdata.Circuits));

        Controller = $controller("SystemConfigurationController", { $scope: $scope });
    }));


    describe('Initial Variables', function() {
        it('should have the folloing initial state', function() {
            expect($scope.property).toBe(propertyService);

        });

        it('should set the selected panel and relay', function() {
            expect($scope.selectedPanel).toEqual('M1000');
            expect($scope.relay).toEqual(propertyModel.Relays[0]);
        });
    });
    describe('Item Select', function () {
        beforeEach(function() {
            $scope.editMode = true;
        });
        it('should set up the local variables when a new relay or module is selected', function () {
            $scope.itemSelected("r1000");
            expect($scope.relay.OBJNAM).toBe('r1000');
            expect($scope.circuit.RLY).toBe('r1000');
            expect($scope.editMode).toBe(false);
        });
    });
    describe('Item Select with circuit', function () {
        beforeEach(function () {
            $scope.editMode = true;
            $scope.circuitFunctions[0] = { VALUE: "GENERIC", LABEL: "Generic" };
            propertyModel.Objects["r1002"] = { OBJNAM: "r1002", OBJTYP: "RLY", PARENT: "m1000", CIRCUIT: "C1000" }
            propertyModel.Objects["C1000"] = { OBJNAM: "C1000", HNAME: "CIRCUIT", SNAME: "Light", OBJTYP: "CIRCUIT", SUBTYP: "GENERIC", RLY: "r1002" }
        });
        it('should set up the local variables when a new relay or module is selected', function () {
            $scope.itemSelected("r1002");
            expect($scope.relay.OBJNAM).toBe('r1002');
            expect($scope.circuit.RLY).toBe('r1002');
            expect($scope.circuit.OBJNAM).toBe("C1000");
            expect($scope.relayCircuitSubtype(propertyModel.Objects['r1002'])).toBe('GENERIC');
            expect($scope.editMode).toBe(false);
        });
    });
    describe('Set edit mode', function () {
        beforeEach(function () {
            $scope.editMode = false;
            $scope.itemSelected("r1000");
        });
        it('should set up the local variables when a new relay or module is selected', function () {
            $scope.setEditMode();
            expect($scope.editMode).toBe(true);
            expect($scope.circuit.RLY).toBe('r1000');
            expect($scope.createFlag).toBe(true);
        });
    });
});