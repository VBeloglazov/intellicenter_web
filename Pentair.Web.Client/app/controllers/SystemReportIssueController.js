﻿'use strict';
var PWC = PWC || {};
PWC.controller('SystemReportIssueController', [
    '$scope', '$location', '$filter', 'PropertyService', 'PropertyModel', 'Enumerations',
    function ($scope, $location, $filter, propertyService, propertyModel, enumerations) {
        $scope.property = propertyService;
        $scope.model = propertyModel;
        $scope.enumerations = enumerations;        
        $scope.editMode = false;
        $scope.$parent.Layout = "layout-base";
        $scope.cancelNavigateModal = false;
        var savedData = {};

        $scope.AddressModel = propertyModel.Objects['_5451'];

        $scope.installationNames = {
            PropertyId: propertyModel.SelectedInstallation.toString(),
            OwnerName: '',
            OwnerPhone: '',
            OwnerEmail: '',
            PropertyName: '',
            Description: "",
            PanelType: "",
            TargetVersion: "",
            PentairRep: "",
            includelogs: false,
            includeimages: false,

        };

        init(); // on startup call init


        function init() {
            propertyService.GetUserSettings();
        }

        
        $scope.AttachImage = function () {

        }

        $scope.checkCamara = function () {
            $scope.Property.camaraEnabled = $scope.installationNames.includeimages;
        }
        $scope.displayImages = function () {
            if ($scope.installationNames.includeimages === true)
                return true;
            else
                return false;
        }

        $scope.getBoardType = function () {
            var panelType = "SINGLE";

            if (propertyModel.PANID) {
                panelType = propertyModel.PANID;
            }
            switch (panelType) {
                case 'SHARE':
                    panelType = "SHARED BODY";
                    break;

                default:
                    panelType = panelType + " BODY";
                    break;
            }

            return panelType;
        };

        $scope.getTargetVersion = function () {
            var version = "";
            if (propertyModel.Objects._5451) {
                //console.log("--- target version: " + propertyModel.Objects._5451.VER);
                version = propertyModel.Objects._5451.VER;
            }
            return version;
        };

        $scope.$on('SystemConfigUpdated', function (event, message) {
            savedData.AddressModel = angular.extend({}, propertyModel.Objects['_5451']);

            // address model                
            $scope.installationNames.OwnerName = savedData.AddressModel.NAME;
            $scope.installationNames.OwnerPhone = savedData.AddressModel.PHONE;
            $scope.installationNames.OwnerEmail = savedData.AddressModel.EMAIL;
            $scope.installationNames.PropertyName = savedData.AddressModel.PROPNAME;
            $scope.installationNames.targetVersion = $scope.getTargetVersion();
            $scope.installationNames.PanelType = $scope.getBoardType();
            
            // location
            $scope.AddressModel.ADDRESS = savedData.AddressModel.ADDRESS;
            $scope.AddressModel.CITY = savedData.AddressModel.CITY;
            $scope.AddressModel.STATE = savedData.AddressModel.STATE;
            $scope.AddressModel.ZIP = savedData.AddressModel.ZIP;
            $scope.AddressModel.COUNTRY = savedData.AddressModel.COUNTRY;
            $scope.AddressModel.TIMZON = savedData.AddressModel.TIMZON;
            
        });


        $scope.gotohomePage = function () {
            $location.path("/property");
        }


        $scope.cancelNavigate = function () {
            $scope.cancelNavigateModal = false;            
        };

        $scope.proceedNavigate = function () {            
            $scope.cancelNavigateModal = false;
            gotohomePage();            
        };

        $scope.isNewRecipientReady = function () {

            if ((!$scope) ||
                (!$scope.NewRecipient) ||
                (!$scope.NewRecipient.Name) ||
                (!$scope.NewRecipient.EmailAddress1))

                return false;

            if (($scope.NewRecipient.Name.length > 0) &&
                 ($scope.NewRecipient.EmailAddress1.length > 0)

                )
                return true;
            else
                return false;
        };

        function resetNewRecipient() {
            $scope.NewRecipient = {
                PropertyId: propertyModel.SelectedInstallation.toString(),
                Name: '',
                Phone1: '',
                EmailAddress1: '',
                Phone2: '',
                EmailAddress2: '',
                UserId: '0'
            };
            if ($scope.forms.newRep) {
                $rootScope.forms.general.newRep.$setPristine();
                $rootScope.forms.general.newRep.$setUntouched();
            }
        }


        /**/

        /*
        function retrieveImageFromClipboardAsBlob(pasteEvent, callback) {
            if (pasteEvent.clipboardData == false) {
                if (typeof (callback) == "function") {
                    callback(undefined);
                }
            };

            var items = pasteEvent.clipboardData.items;

            if (items == undefined) {
                if (typeof (callback) == "function") {
                    callback(undefined);
                }
            };

            for (var i = 0; i < items.length; i++) {
                // Skip content if not image
                if (items[i].type.indexOf("image") == -1) continue;
                // Retrieve image on clipboard as blob
                var blob = items[i].getAsFile();

                if (typeof (callback) == "function") {
                    callback(blob);
                }
            }
        }

        
        window.addEventListener("paste", function (e) {

            // Handle the event
            retrieveImageFromClipboardAsBlob(e, function (imageBlob) {
                // If there's an image, display it in the canvas
                if (imageBlob) {
                    var canvas = document.getElementById("mycanvas");
                    var ctx = canvas.getContext('2d');

                    // Create an image to render the blob on the canvas
                    var img = new Image();

                    // Once the image loads, render the img on the canvas
                    img.onload = function () {
                        // Update dimensions of the canvas with the dimensions of the image
                        canvas.width = this.width;
                        canvas.height = this.height;

                        // Draw the image
                        ctx.drawImage(img, 0, 0);
                    };

                    // Crossbrowser support for URL
                    var URLObj = window.URL || window.webkitURL;

                    // Creates a DOMString containing a URL representing the object given in the parameter
                    // namely the original Blob
                    img.src = URLObj.createObjectURL(imageBlob);
                }
            });
        }, false);
        */





        function retrieveImageFromClipboardAsBase64(pasteEvent, callback, imageFormat) {
            if (pasteEvent.clipboardData == false) {
                if (typeof (callback) == "function") {
                    callback(undefined);
                }
            };

            var items = pasteEvent.clipboardData.items;

            if (items == undefined) {
                if (typeof (callback) == "function") {
                    callback(undefined);
                }
            };

            for (var i = 0; i < items.length; i++) {
                // Skip content if not image
                if (items[i].type.indexOf("image") == -1) continue;
                // Retrieve image on clipboard as blob
                var blob = items[i].getAsFile();

                // Create an abstract canvas and get context
                var mycanvas = document.createElement("canvas");
                var ctx = mycanvas.getContext('2d');

                // Create an image
                var img = new Image();

                // Once the image loads, render the img on the canvas
                img.onload = function () {
                    // Update dimensions of the canvas with the dimensions of the image
                    mycanvas.width = this.width;
                    mycanvas.height = this.height;

                    // Draw the image
                    ctx.drawImage(img, 0, 0);

                    // Execute callback with the base64 URI of the image
                    if (typeof (callback) == "function") {
                        callback(mycanvas.toDataURL(
                            (imageFormat || "image/png")
                        ));
                    }
                };

                // Crossbrowser support for URL
                var URLObj = window.URL || window.webkitURL;

                // Creates a DOMString containing a URL representing the object given in the parameter
                // namely the original Blob
                img.src = URLObj.createObjectURL(blob);
            }
        }

        window.addEventListener("paste", function (e) {

            // Handle the event
            retrieveImageFromClipboardAsBase64(e, function (imageDataBase64) {
                // If there's an image, open it in the browser as a new window :)
                if (imageDataBase64) {
                    // data:image/png;base64,iVBORw0KGgoAAAAN......
                    window.open(imageDataBase64);
                }
            });
        }, false);        

    }
]);