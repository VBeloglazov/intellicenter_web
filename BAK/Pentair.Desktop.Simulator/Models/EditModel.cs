﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using Pentair.Domain.Client.Entity;
using Pentair.Domain.Client.Helpers;
using Pentair.Domain.Client.Helpers.Entity;
using Pentair.Domain.Client.Infrastructure;

namespace Pentair.Desktop.Simulator.Models
{
    public class EditModel : INotifyPropertyChanged
    {
        #region Events

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Public Properties

        public ObjectView Object { get; set; }
        public bool Changes { get; set; }
        public bool CreateFlag { get; private set; }
        public string MessageType { get; set; }
        public bool DeleteFlag { get; set; }
        public bool DeleteEnable { get; set; }

    // live properties
        public string parentType { get; set; }
        public ObservableCollection<string> sindexSelector { get; set; }
        public ObservableCollection<string> parents { get; set; } 

        // _object properties
        public string objnam { get; set; }
        public string objtyp { get; set; }
        public string absmax { get; set; }
        public string act { get; set; }
        public string body { get; set; }
        public string chem { get; set; }
        public string circuit { get; set; }
        public string clk24a { get; set; }
        public string color { get; set; }
        public string dimmer { get; set; }
        public string featr { get; set; }
        public string filter { get; set; }
        public string freeze { get; set; }
        public string group { get; set; }
        public string heater { get; set; }
        public string listord { get; set; }
        public string mode { get; set; }
        public string module { get; set; }
        public string parent { get; set; }
        public string pump { get; set; }
        public string sense { get; set; }
        public string share { get; set; }
        public string shomnu { get; set; }
        public string sindex { get; set; }
        public string sname { get; set; }
        public string source { get; set; }
        public string subtyp { get; set; }
        public string usage { get; set; }
        #endregion

        #region Private Properties

        public Dictionary<int, string> modeValues = new Dictionary<int, string>()
        {
            {0, "ALERT"},
            {1, "WARNING"},
            {2, "INFORM"},
            {3, "STATUS"},
            {4, "LOCAL"}
        };

        #endregion

        #region Private Methods

        #region Constructor

        public EditModel(string objName, string objType, int property)
        {
            DeleteFlag = false;
            CreateFlag = false;
            DeleteEnable = false;
            if (string.IsNullOrEmpty(objName))
            {
                NewObject(objType, property);
            }
            else
            {
                using (var db = new SimulatorContext())
                {
                    DeleteFlag = false;
                    Changes = false;
                    var obj = db.ObjectViews.FirstOrDefault(x => x.INSTALLATION == property && x.OBJNAM == objName);
                    if (null == obj)
                    {
                        NewObject(objType, property);
                    }
                    else
                    {
                        Object = obj;
                        var deleted = db.ControlObjects.FirstOrDefault(
                            x => x.INSTALLATION == property &&
                                 x.OBJNAM == obj.OBJNAM &&
                                 x.Key == StaticData.Parent);
                        DeleteFlag = null != deleted && deleted.Deleted;
                        DeleteEnable = !DeleteFlag;
                    }
                }
            }
        }

        public void OnPropertyChanged(string propertyName)
        {
            if (null != PropertyChanged)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

        private void NewObject(string type, int property)
        {
            CreateFlag = true;
            Object = new ObjectView()
            {
                INSTALLATION = property,
                OBJNAM = "<new>",
                OBJTYP = type
            };
        }
        #endregion
    }
}
