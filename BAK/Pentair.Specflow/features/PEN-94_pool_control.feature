Feature: Pool Control (PEN-94)
As a Pool Owner
I want turn on or off my pool
So That I can demonstrate a larger ability to control the system more generally

  Background: 
  Given user is connected to a pool system
  And user is viewing pool details

Scenario: DISPLAY ON POOL STATUS
  When a Pool switch is displayed
  And the associated Pool is ON
  Then the Pool switch is set to ON
 
Scenario: DISPLAY OFF POOL STATUS
  When a Pool switch is displayed
  And the associated Pool is OFF
  Then the Pool switch is set to OFF

Scenario: TOGGLE POOL SWITCH
  When the user toggles a Pool switch
  Then the state of the on-screen switch is changed
  And the appropriate message is sent to the THW

Scenario: NO SPA
  Given the user is connected to a pool system without a spa 
  And user is viewing pool details
  Then interface elements related to a spa will not be displayed

Scenario: NO POOL
  Given the user is connected to a pool system without a pool
  And user is viewing pool details
  Then interface elements related to a pool will not be displayed 