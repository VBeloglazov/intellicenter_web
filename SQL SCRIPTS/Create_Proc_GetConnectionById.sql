-- =============================================================
--
-- Author:		Vladimir Beloglazov
-- Create date: 04/23/2018
--
-- Creates:
--
--		Table Type:			None
--
--		Stored Procedure:	PGetConnectionById
--
-- =============================================================

USE PathfinderIntelliCenter
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (
	SELECT * FROM sys.objects
	WHERE object_id = OBJECT_ID(N'dbo.PGetConnectionById')
	AND type in (N'P', N'PC')
)
DROP PROCEDURE dbo.PGetConnectionById
GO

-- -------------------------------------------------------------
--
-- Returns [Connections] record field values.
--
-- -------------------------------------------------------------
CREATE PROCEDURE dbo.PGetConnectionById
	  @connectionId	AS uniqueidentifier
AS
BEGIN

	SET NOCOUNT OFF

	-- Result set
	SELECT *
	FROM dbo.Connections
	WHERE ConnectionId = @connectionId

END
GO
