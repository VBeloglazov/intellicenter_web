using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using System.Data.Entity.Infrastructure;
using System.Configuration;
using System.Web.Configuration;
using System.Diagnostics;
using System.Net;

using Ninject;

using SERVICE_SHARED;

namespace Pentair.Web.Api
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        private const string EVENT_SOURCE = "WebApi";

        private static bool debugHTTP = true;

        private static bool logVerbose = true;
        public static bool LogVerbose { get { return WebApiApplication.logVerbose; } }

        public static readonly string HOSTNAME = Environment.MachineName.ToUpper(); // TODO: Usage?

        private static LogManagerMMF_WebApi logManager = null;

        public static long counterFrequency;
        public static WebApiPerformanceCountersManager PerformanceCountersMgr { get; private set; }

        protected void Application_Start()
        {
            //
            // Logger MMF
            //

            WebApiApplication.logManager = new LogManagerMMF_WebApi();

            WebApiApplication.logManager.Initialize(
                logDirectory: Properties.Settings.Default.LogDirectory,
                logFileSizeMax: Properties.Settings.Default.LogFileSizeMax,
                logMinBytesToFlush: Properties.Settings.Default.LogMinBytesToFlush,
                eventLogName: Properties.Settings.Default.EventLogName,
                eventSourceName: EVENT_SOURCE
            );

            LogManager.Logger = WebApiApplication.logManager;

            LogManager.LogInformationAsync(
                source: EVENT_SOURCE,
                eventBriefDescription: "Starting...",
                installationId: 0,
                userId: Guid.Empty,
                connectionId: Guid.Empty,
                eventFullDescription: String.Empty,
                saveEventLog: true
            );

            //
            // Performance Counters
            //

            NativeMethods.QueryPerformanceFrequency(out WebApiApplication.counterFrequency);
            WebApiApplication.PerformanceCountersMgr = new WebApiPerformanceCountersManager();

            //
            // Microsoft's and Pathfinder's stuff
            //

            AreaRegistration.RegisterAllAreas();

            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);

// 1.0.0.13            DatabaseCreationManager.CreateUpdateDatabase();
            // Log the database identity
            using (DbContextPentair db = DbContextPentair.New())
            {
                LogManager.LogInformationAsync(
                    source: EVENT_SOURCE,
                    eventBriefDescription: String.Format("Database: \"{0}\" at {1}",
                        db.Database.Connection.Database,
                        db.Database.Connection.DataSource
                    ),
                    installationId: 0,
                    userId: Guid.Empty,
                    connectionId: Guid.Empty,
                    eventFullDescription: String.Empty,
                    saveEventLog: true
                );

            } // using (DbContextPentair db)

            // Finally, spin up the notification background task
            new NotificationManager(); // TODO: What does it do, if anything?

            Configuration configSettings = WebConfigurationManager.OpenWebConfiguration("/service");

            if (!System.Boolean.TryParse(
                configSettings.AppSettings.Settings["debugHTTP"].Value, out WebApiApplication.debugHTTP))
            {
                LogManager.LogErrorAsync(
                    source: EVENT_SOURCE,
                    eventBriefDescription: "Cannot parse config setting",
                    installationId: 0,
                    userId: Guid.Empty,
                    connectionId: Guid.Empty,
                    eventFullDescription:
                        String.Format("Name: \"debugHTTP\"\nType: boolean\nConfig value: \"{0}\"",
                            configSettings.AppSettings.Settings["debugHTTP"].Value
                        )
                );

                return;
            }

            if (!System.Boolean.TryParse(
                configSettings.AppSettings.Settings["logVerbose"].Value, out WebApiApplication.logVerbose))
            {
                LogManager.LogErrorAsync(
                    source: EVENT_SOURCE,
                    eventBriefDescription: "Cannot parse config setting",
                    installationId: 0,
                    userId: Guid.Empty,
                    connectionId: Guid.Empty,
                    eventFullDescription:
                        String.Format("Name: \"logVerbose\"\nType: boolean\nConfig value: \"{0}\"",
                            configSettings.AppSettings.Settings["logVerbose"].Value
                        )
                );

                return;
            }

            CleanupAfterPossibleCrash();

            LogManager.LogInformationAsync(
                source: EVENT_SOURCE,
                eventBriefDescription: "STARTED",
                installationId: 0,
                userId: Guid.Empty,
                connectionId: Guid.Empty,
                eventFullDescription: String.Empty,
                saveEventLog: true
            );

        } // Application_Start()

        protected void Application_End()
        {
            WebApiApplication.logManager.Stop(
                source: EVENT_SOURCE,
                eventBriefDescription: "STOPPED",
                installationId: 0,
                userId: Guid.Empty,
                connectionId: Guid.Empty,
                eventFullDescription: String.Empty,
                typeEntry: EventLogEntryType.Information
            );

        } // Application_End()

        void Application_Error(
            object sender,
            EventArgs e)
        {
            Exception exception = Server.GetLastError();

            if (!(exception is StackExchange.Redis.RedisConnectionException ||
                (exception.InnerException != null && exception.InnerException is StackExchange.Redis.RedisConnectionException)))
            {
                LogManager.LogErrorAsync(
                    source: EVENT_SOURCE,
                    eventBriefDescription: "UNHANDLED EXCEPTION",
                    installationId: 0,
                    userId: Guid.Empty,
                    connectionId: Guid.Empty,
                    eventFullDescription:
                        ExceptionManager.CustomizeException(exception).ErrorMessage(
                            errorHeader: null,
                            addStackTrace: true
                        )
                );
            }

            Server.ClearError();

        } // Application_Error()

        protected void Application_BeginRequest()
        {
            if (WebApiApplication.debugHTTP)
            {
                LogManager.LogInformationAsync(
                    source: EVENT_SOURCE,
                    eventBriefDescription: "HTTP Request",
                    installationId: 0,
                    userId: Guid.Empty,
                    connectionId: Guid.Empty,
                    eventFullDescription:
                        String.Format("{0} {1}\nREMOTE_HOST: {2}\nHTTP_USER_AGENT: {3}",
                            Context.Request.HttpMethod,
                            Context.Request.RawUrl,
                            Context.Request.Params.Get("REMOTE_HOST"),
                            Context.Request.Params.Get("HTTP_USER_AGENT")
                        )
                );
            }

        } // Application_BeginRequest()

        protected void Application_EndRequest()
        {
            HttpContextWrapper httpContextWrapper = new HttpContextWrapper(Context);

            // if responce type is JSON then return 401 and avoid redirect
            if (Context.Response.StatusCode == 302 &&
                httpContextWrapper.Response.ContentType.ToLower().Contains("json"))
            {
                Context.Response.Clear();
                Context.Response.StatusCode = 401;
            }

            if (Context.Response.StatusCode == (int)HttpStatusCode.BadRequest           ||  // 400
                Context.Response.StatusCode == (int)HttpStatusCode.Unauthorized         ||  // 401
                Context.Response.StatusCode == (int)HttpStatusCode.Forbidden            ||  // 403
                Context.Response.StatusCode == (int)HttpStatusCode.NotFound             ||  // 404
                Context.Response.StatusCode == (int)HttpStatusCode.Conflict             ||  // 409
                Context.Response.StatusCode == (int)HttpStatusCode.InternalServerError  )   // 500
            {
                LogManager.LogErrorAsync(
                    source: EVENT_SOURCE,
                    eventBriefDescription: "HTTP Response",
                    installationId: 0,
                    userId: Guid.Empty,
                    connectionId: Guid.Empty,
                    eventFullDescription:
                        String.Format("Request: {0} {1}\nResponse status: {2}",
                            Context.Request.HttpMethod,
                            Context.Request.RawUrl,
                            Context.Response.Status
                        )
                );
            }
            else
            {
                if (WebApiApplication.debugHTTP)
                {
                    LogManager.LogInformationAsync(
                        source: EVENT_SOURCE,
                        eventBriefDescription: "HTTP Response",
                        installationId: 0,
                        userId: Guid.Empty,
                        connectionId: Guid.Empty,
                        eventFullDescription:
                            String.Format("Request: {0} {1}\n\nResponse status: {2}",
                                Context.Request.HttpMethod,
                                Context.Request.RawUrl,
                                Context.Response.Status
                            )
                    );
                }
            }

        } // Application_EndRequest()

        /// <summary>
        /// If the server didn't have a chance to cleaup doing last shutdown
        /// (crash, power off, network was not available, etc.), it shall do it here.
        /// </summary>
        private void CleanupAfterPossibleCrash()
        {
            try
            {
                using (DbContextPentair db = DbContextPentair.New())
                {
                    db.CleanupAfterPossibleCrash();

                } // using (DbContextPentair db)
            }
            catch(System.Data.Entity.Validation.DbEntityValidationException e)
            {
                foreach (var entityValidationErrors in e.EntityValidationErrors)
                {
                    foreach (var validationError in entityValidationErrors.ValidationErrors)
                    {
                        LogManager.LogErrorAsync(
                            source: EVENT_SOURCE,
                            eventBriefDescription: "CleanupAfterPossibleCrash validation error",
                            installationId: 0,
                            userId: Guid.Empty,
                            connectionId: Guid.Empty,
                            eventFullDescription:
                                String.Format("Parameter: {0}\nError: {1}",
                                    validationError.PropertyName,
                                    validationError.ErrorMessage
                                )
                        );
                    }
                }
            }
            catch (Exception e)
            {
                LogManager.LogErrorAsync(
                    source: EVENT_SOURCE,
                    eventBriefDescription: "CleanupAfterPossibleCrash failed",
                    installationId: 0,
                    userId: Guid.Empty,
                    connectionId: Guid.Empty,
                    eventFullDescription:
                        ExceptionManager.CustomizeException(e).ErrorMessage(
                            errorHeader: null,
                            addStackTrace: true
                        )
                );
            }

        } // CleanupAfterPossibleCrash()

    } // class WebApiApplication
}
