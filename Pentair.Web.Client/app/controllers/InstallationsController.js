﻿'use strict';
var PWC = PWC || {};
PWC.controller('InstallationsController', [
    '$scope', '$location', '$route', '$filter', '$q', 'PropertyService', 'LocalStorageService', 'SocketService', 'PropertyModel', 'AppService',
    function($scope, $location, $route, $filter, $q, propertyService, localStorageService, socketService, propertyModel, appService) {

        $scope.Property = propertyService;
        $scope.Model = propertyModel;
        $scope.$parent.Layout = "layout-base";
        $scope.Filter = "All";
        $scope.SearchTerm = "";
        $scope.Installations = [];
        $scope.ConfirmDelete = false;

        $scope.$on('PropertyListUpdates', function(event, message) {
            buildInstallations();
        });

        $scope.ExpandItem = function(item) {
            item.open = !item.open;
        };

        $scope.TryConnectToProperty = function(property) {
            if (property && property.OnLine) {
                $scope.ConnectToProperty(property.InstallationId)
            }
        }

        $scope.ConnectToProperty = function(propertyId, isOnline) {
            if (!isOnline) return;

            if (propertyModel.Ready) {
                if (propertyModel.SelectedInstallation.InstallationId != propertyId) {
                    propertyService.ChangeConnection(propertyId);
                } else {
                    $location.path("/property");
                }
            } else {
                propertyModel.Clear();
                propertyService.Connect(propertyId);
            }
        }

        $scope.GroupDeletes = function() {
            var deletes = false;
            angular.forEach($scope.Installations, function(installation) {
                if (installation.deleteFlag) {
                    deletes = true;
                }
            })
            return deletes;
        }

        $scope.ValidateDelete = function() {
            $scope.ConfirmDelete = true;
        };

        $scope.RemoveInstallations = function() {
            $scope.ConfirmDelete = false;
            var pools = [];
            angular.forEach($scope.Installations, function(installation) {
                if (installation.deleteFlag) {
                    pools.push(installation.InstallationId);
                };
            });
            if (pools.length > 0) {
                var properties = {
                    PropertyIds: pools
                };
                appService.DeleteData('Api/Installations', properties).then(function() {
                    refreshData()
                });
            };
        };

        $scope.filterInstallations = function(filter) {
            if (filter) {
                $scope.Filter = filter;
                refreshData();
            }
        }

        $scope.searchInstallations = function() {
            refreshData();
        }

        $scope.titlebarAddress = function(installation) {
            if (installation && installation.Address && installation.City && installation.State && installation.Zip) {
                return installation.Address + ', ' + installation.City + ', ' + installation.State + ' ' + installation.Zip;
            }
            return '--';
        }

        $scope.TimezoneIcon = function(timezone) {
            if (timezone) {
                return "tz" + (Number(timezone) + 13)
            }
            return ""
        };

        $scope.getCountFiltered = function () {

            if (!$scope.Installations)
                return ""

            var count = 0

            if (($scope.Filter === "All") || ($scope.Filter === "ALL")) {
                count = $scope.Installations.length
            }
            if ($scope.Filter === "Connected") {
                angular.forEach($scope.Installations,function( installation ) {
                    if (installation.OnLine)
                        count = count+1
                });                
            }
            if ($scope.Filter === "Off-line") {
                angular.forEach($scope.Installations, function (installation) {
                    if (!installation.OnLine )
                        count = count + 1
                });
            }
            var result = ' TOTAL SYSTEMS : '+ String(count) + " ,   "

            return result
        }


        //Private Methods

        function init() {
            refreshData();
            if (!propertyModel.Ready) {
                loadProperty();
            }
        }

        function loadProperty() {
            propertyModel.Clear(true);

            if ($route.current && $route.current.locals && $route.current.locals.installations)
                propertyModel.Installations = $route.current.locals.installations.data;

            reloadFromCache();
            if (PWC.isEmptyObject(socketService.socket))
                autoConnectToSingleProperty();
        }

        function refreshData() {
            propertyService.FetchInstallationsWithDetails($scope.SearchTerm, $scope.Filter);
        };

        function reloadFromCache() {
            if (sessionStorage.selectedInstallation) {
                propertyModel.SelectedInstallation = JSON.parse(sessionStorage.selectedInstallation);
            };
        };

        function autoConnectToSingleProperty() {
            // reconnect only if 1 installation is online only
            
            if (1 === propertyModel.Installations.length) {
                console.log("------ only 1 property available -------");
                if (propertyModel.Installations[0].OnLine === true) {
                    console.log("------ property Is Online, will try to reconnect");
                    propertyModel.SelectedInstallation = propertyModel.Installations[0].InstallationId;
                    propertyService.Connect();
                } else {
                    console.log("------ property Is Offline.. ");
                }
            };
        };

        function buildInstallations() {
            $scope.Installations = [];
            angular.forEach(propertyModel.Installations, function(installation) {
                var property = installation;
                property["deleteFlag"] = false;
                property["open"] = false;
                $scope.Installations.push(property);
            });
        };

        //END


        //Init
        init();
        //END

    }
]);
