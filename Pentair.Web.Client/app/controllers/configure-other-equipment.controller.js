'use strict';
var PWC = PWC || {};
PWC.controller('ConfigureOtherEquipmentController', [
  '$rootScope', '$scope', '$location', '$interpolate', 'PropertyService', 'PropertyModel', 'Enumerations', '$q',
  '$filter', 'MessageProcessor', 'Notification',
  function ($rootScope, $scope, $location, $interpolate, propertyService, propertyModel, Enumerations, $q, $filter,
            messageProcessor, Notification) {
    //FIXME: figure out why this isn't picking up the other heaters.
    $scope.propertyBodies = propertyModel.Bodies;
    $scope.propertyBodiesAvailable = [];
    $scope.accordionStateByObjectNames = {};
    $scope.model = propertyModel;
    $scope.heaterTypes = Enumerations.heatersParams;
    $scope.heaterPrefer = Enumerations.heatersPrefer;
    $scope.circuits = propertyModel.Circuits;
    $scope.holddeletedCircuitFromCover = [];

    $scope.BODYPOOL       = "B1101";
    $scope.BODYSPA        = "B1202";
    $scope.BODYPOOLSPA    = "B1101 B1202";
     
      // HEAT MODES FOR HYBRID
    /*
    $scope.HEATMODE_NONE        = '0';
    $scope.HEATMODE_HEAT_PUMP   = '1';
    $scope.HEATMODE_GAS         = '2';
    $scope.HEATMODE_HYBRID      = '3';
    $scope.HEATMODE_DUAL = '4';
    */
      // new heat modes for hybrid
    $scope.HEATMODE_DONT_CHANGE                         = '0';
    $scope.HEATMODE_OFF                                 = '1';
    $scope.HEATMODE_GAS                                 = '2';
    $scope.HEATMODE_SOLAR_ONLY                          = '3';
    $scope.HEATMODE_SOLAR_PREF                          = '4';
    $scope.HEATMODE_ULTRA_TEMP_OR_HEAT_PUMP             = '5';
    $scope.HEATMODE_ULTRA_TEMP_OR_HEAT_PUMP_PREFERRED   = '6';
    $scope.HEATMODE_HYBRID_GAS_HEAT_ONLY                = '7';
    $scope.HEATMODE_HYBRID_HEAT_PUMP_ONLY               = '8';
    $scope.HEATMODE_HYBRID_HYBRID                       = '9';
    $scope.HEATMODE_HYBRID_DUAL                         = '10';
    $scope.HEATMODE_MASTERTEMP                          = '11';
    $scope.HEATMODE_MAX_E_THERM                         = '12';
    $scope.HEATMODE_UNDEFINED                           = '255';

    
    $scope.sortableOptionsForHeaterGroups = {};
    $scope.acts = [{systemValue:'CONCUR',readableValue:'Concurrent'}, {systemValue:'PRIOR',readableValue:'Priority'}];

    $rootScope.editMode.otherequipment = {};
    $rootScope.editMode.valves = {};
    $scope.circuitPositions = ['0', '1'];
    $scope.heaters = [];    
    $scope.maxheaters = 16; // maximum heaters
    updateValves();
    updateHeaters();
   // updateHeaterGroups();
    updateCovers();

    $scope.$watch('model.Valves', function (newVal, oldVal) {
      if(!angular.equals(newVal, oldVal)) {
        updateValves();
      }
    }, true);

    //Generating drop downs
    // remove prefer subtypes
    $scope.heaterSubtypes = Enumerations.getHeaterSubtypeObjectsNoPref();   //$scope.heaterSubtypes = Enumerations.getHeaterSubtypeObjects();        

    $scope.isHeatersOnBody = function (subtype, body) {
        var status = false
        angular.forEach($scope.heaters, function (heater) {
            // check if body and subtype are present
            if (heater.body === body && heater.subtype === subtype)
                status = true
        });
        return status
    };


    $scope.isGasHeaterOnBody = function (body) {
        var status =false        
        angular.forEach($scope.heaters, function (heater) {
            if ($scope.isHeatersOnBody('GENERIC', body)  //|| $scope.isHeatersOnBody('GENERIC', $scope.BODYPOOLSPA)
                )
                status = true
        });
        return status
    }

    $scope.isMaxHeaterOnBody = function (body) { // MAX E THERM
        var status = false
        angular.forEach($scope.heaters, function (heater) {
            if ($scope.isHeatersOnBody('MAX', body)  //|| $scope.isHeatersOnBody('GENERIC', $scope.BODYPOOLSPA)
                )
                status = true
        });
        return status
    }

    $scope.isMasterHeaterOnBody = function (body) { // MASTERTEMP
        var status = false
        angular.forEach($scope.heaters, function (heater) {
            if ($scope.isHeatersOnBody('MASTER', body)  //|| $scope.isHeatersOnBody('GENERIC', $scope.BODYPOOLSPA)
                )
                status = true
        });
        return status
    }

    $scope.isSolarHeaterOnBody = function (body) {
        var status = false
        angular.forEach($scope.heaters, function (heater) {
            if ($scope.isHeatersOnBody('SOLAR', body)  // || $scope.isHeatersOnBody('SOLAR', $scope.BODYPOOLSPA)
                )
                status = true
        });
        return status
    }

    $scope.isHeatPumpHeaterOnBody = function (body) {
        var status = false
        angular.forEach($scope.heaters, function (heater) {
            if ($scope.isHeatersOnBody('HTPMP', body)  // || $scope.isHeatersOnBody('HTPMP', $scope.BODYPOOLSPA)
                )
                status = true
        });
        return status
    }

    $scope.isUltraHeaterOnBody = function (body) {
        var status = false
        angular.forEach($scope.heaters, function (heater) {
            if ($scope.isHeatersOnBody('ULTRA', body)  // || $scope.isHeatersOnBody('ULTRA', $scope.BODYPOOLSPA)
                )
                status = true
        });
        return status
    }
    $scope.isHybridHeaterOnBody = function (body) {
        var status = false
        angular.forEach($scope.heaters, function (heater) {
            if ($scope.isHeatersOnBody('HCOMBO', body)  // || $scope.isHeatersOnBody('HCOMBO', $scope.BODYPOOLSPA)
                )
                status = true
        });
        return status
    };

    $scope.isAnyHeatPumpModelOnBody = function (body) {
        if ($scope.isUltraHeaterOnBody(body) === true)
            return true
        if ($scope.isHybridHeaterOnBody(body) === true)
            return true
        if ($scope.isHeatPumpHeaterOnBody(body) === true)
            return true
        return false;
    };       
    
    $scope.getDefaultBody = function (heater) {
        var body = $scope.BODYPOOLSPA
        
        if (!heater)
            return body;

        var b1 = $scope.isBodyEnable(heater, $scope.BODYPOOL)
        var b2 = $scope.isBodyEnable(heater, $scope.BODYSPA)
        var b1a2 = $scope.isBodyEnable(heater, $scope.BODYPOOLSPA)

        switch($scope.model.PANID){
            case "SINGLE":
                body = $scope.BODYPOOL
                break

            case "SHARE":
                if (!b1 && !b2) 
                    body = $scope.BODYPOOLSPA

                if (b1 && b2)
                    body = $scope.BODYPOOLSPA

                if (!b1 && b2)
                    body = $scope.BODYSPA
                
                if (b1 && !b2)
                    body = $scope.BODYPOOL                
                break

            case "DUAL":
                if (b1 && b2)
                    body = $scope.BODYPOOL

                if (!b1 && b2)
                    body = $scope.BODYSPA

                if (b1 && !b2)
                    body = $scope.BODYPOOL
                break;

            default:
                body = $scope.BODYPOOL
                break;
        }
        
        return body

    }

    $scope.setAvailableBodies = function (heater) {

       // $scope.propertyBodies = propertyModel.Bodies;
        if (!heater)
            return;        

        var tempbodies = propertyModel.Bodies;
        var bodies = [];

        var b1 = $scope.isBodyEnable(heater, $scope.BODYPOOL)
        var b2 = $scope.isBodyEnable(heater, $scope.BODYSPA)
        var b1a2 = $scope.isBodyEnable(heater, $scope.BODYPOOLSPA)
        
        angular.forEach(tempbodies, function (obj) {            
            var body = obj.OBJNAM         
            switch ($scope.model.PANID) {
                case "SINGLE":
                    if ( (body === $scope.BODYPOOL) )
                        bodies.push(obj)
                    break

                case "SHARE":                    
                    // pool selected and spa no-selected
                    if (b1 && (body === $scope.BODYPOOL))
                        bodies.push(obj)
                    
                    // pool no-selected and spa selected
                    if (b2 && (body === $scope.BODYSPA))
                        bodies.push(obj)                    
                    
                    if (b1a2){
                        if(body === $scope.BODYPOOLSPA)
                            bodies.push(obj)

                        if (!b1 && (body === $scope.BODYPOOL))
                            bodies.push(obj)

                        if (!b2 && (body === $scope.BODYSPA))
                            bodies.push(obj)

                    }

                    break

                case "DUAL":                    
                    if (b1 && (body === $scope.BODYPOOL))
                        bodies.push(obj)                    
                    
                    if (b2 && (body === $scope.BODYSPA))
                        bodies.push(obj)

                    break;

                default:
                    // do nothing
                    break;
              }
        });

        // add current selection
        angular.forEach(tempbodies, function (obj) {                
            if (heater.body === obj.OBJNAM)
                bodies.push(obj)
        });

        // set available bodies now
        $scope.propertyBodiesAvailable = bodies;
    }

    $scope.isBodyInAvailableList = function (source) {
        var status = false
        angular.forEach($scope.propertyBodiesAvailable, function (obj) {
            if (obj.OBJNAM === source.OBJNAM)
                status =true
        });
        return status
    }

    $scope.isBodyEnable = function (heater,body) {
        var pool_occupied=false
        var spa_occupied=false
        var isAvailable = true
        var heater_type = heater.subtype
        switch(heater_type){
            case "GENERIC":
            case "MAX":
            case "MASTER":
                if($scope.isAvailableHeaterBodyOn_Gas_Master_Max($scope.BODYPOOL))
                    pool_occupied=true
                if ($scope.isAvailableHeaterBodyOn_Gas_Master_Max($scope.BODYSPA))
                    spa_occupied=true;
                break;
            case "SOLAR":
            case "HTPUMP":
            case "ULTRA":
                if ($scope.isAvailableHeaterBodyOn_Solar_HeatPump_Ultra($scope.BODYPOOL))
                    pool_occupied=true;
                if ($scope.isAvailableHeaterBodyOn_Solar_HeatPump_Ultra($scope.BODYSPA))
                    spa_occupied=true;                  
                break;
            case "HCOMBO":
                if($scope.isHybridHeaterOnBody($scope.BODYPOOL))
                    pool_occupied=true;
                if ($scope.isHybridHeaterOnBody($scope.BODYSPA))
                    spa_occupied=true;                
                break;
        }


        isAvailable=true;
        switch(body){
            case $scope.BODYPOOL:
                if (pool_occupied)
                    isAvailable=false;
                break;

            case $scope.BODYSPA:
                if(spa_occupied)
                    isAvailable=false;
                break;
            case $scope.BODYPOOLSPA:
                if ( (pool_occupied || !spa_occupied) ||  (!pool_occupied || spa_occupied) )
                    isAvailable = true;
                if (pool_occupied && spa_occupied) // if both are selected is not allow then
                    isAvailable = false;
                break;

            default:
                isAvailable=true;
                break;
        }

        return isAvailable;
    }


      // check if heater for body is available for: Gas, MasterTemp,Max-E
    $scope.isAvailableHeaterBodyOn_Gas_Master_Max = function (body) {
        if (($scope.isGasHeaterOnBody(body)) ||
            ($scope.isMasterHeaterOnBody(body)) ||
            ($scope.isMaxHeaterOnBody(body)))
            return true;
        else
            return false;
    }

    // check if heater for body is available for:solar,heat pump,ultra
    $scope.isAvailableHeaterBodyOn_Solar_HeatPump_Ultra = function (body) {        
        if (($scope.isSolarHeaterOnBody(body)) ||
            ($scope.isHeatPumpHeaterOnBody(body)) ||
            ($scope.isUltraHeaterOnBody(body)))
            return true;
        else
            return false;
    }

    // check heater type is already configured
    $scope.isheaterAvailable = function (heatertype) {        
        var available = true
        switch (heatertype) {
            case "GENERIC": // gas heater
            case "MAX":     // MAX-E THERM
            case "MASTER":  // MASTERTEMP
                switch ($scope.model.PANID) {
                    case "SINGLE":
                        if ($scope.isAvailableHeaterBodyOn_Gas_Master_Max($scope.BODYPOOL))
                            available = false
                        break;
                    case "SHARE":
                        if ($scope.isAvailableHeaterBodyOn_Gas_Master_Max($scope.BODYPOOLSPA))
                            available = false

                        break;
                    case "DUAL":
                        if ($scope.isAvailableHeaterBodyOn_Gas_Master_Max($scope.BODYPOOL) && $scope.isAvailableHeaterBodyOn_Gas_Master_Max($scope.BODYSPA))
                            available = false                            
                        break;
                }
                break;
            case "SOLAR":   // SOLAR HEATER
            case "HTPMP":   // HEAT PUMP HEATER         
            case "ULTRA":   // ULTRA 
                switch ($scope.model.PANID) {
                    case "SINGLE":
                        if ($scope.isAvailableHeaterBodyOn_Solar_HeatPump_Ultra($scope.BODYPOOL))
                            available = false
                        break;
                    case "SHARE":
                        if ($scope.isAvailableHeaterBodyOn_Solar_HeatPump_Ultra($scope.BODYPOOLSPA))
                            available = false
                        break;
                    case "DUAL":
                        if (($scope.isAvailableHeaterBodyOn_Solar_HeatPump_Ultra($scope.BODYPOOL)) && ($scope.isAvailableHeaterBodyOn_Solar_HeatPump_Ultra($scope.BODYSPA)))
                            available = false
                        break;
                }
                break;
            case "HCOMBO":  // HYBRID
                switch ($scope.model.PANID) {
                    case "SINGLE":
                        if ($scope.isHybridHeaterOnBody($scope.BODYPOOL))
                            available = false
                        break;
                    case "SHARE":
                        if ($scope.isHybridHeaterOnBody($scope.BODYPOOLSPA))
                            available = false
                        break;
                    case "DUAL":
                        if ($scope.isHybridHeaterOnBody($scope.BODYPOOL) && $scope.isHybridHeaterOnBody($scope.BODYSPA))
                            available = false
                        break;

                }
                break;
            default:
                available = true
                break;
        }

        return available;
    };

    
      // check available heaters
    $scope.getAvailableHeaters = function () {
        var tempSubtypes = Enumerations.getHeaterSubtypeObjectsNoPref();
        var subtypes = [];         

        angular.forEach(tempSubtypes, function (heater) {
            if ($scope.isheaterAvailable(heater.actualValue) === true) {
                if (heater.dictionaryValue.indexOf("Only") >= 0) {
                    var temp = heater.dictionaryValue.replace(' Only', '');
                    heater.dictionaryValue = temp;
                }
                subtypes.push(heater)
            }
        });

        return subtypes;
    };    
    

    $scope.setAvailableHeaters = function () {
        // set now
        $scope.heaterSubtypes = $scope.getAvailableHeaters();
    }
    

    $scope.valveSubtypes = Enumerations.getValveSubtypeObjects();
    for(var i = 0; i < $scope.valveSubtypes.length; i++) {
      if($scope.valveSubtypes[i].actualValue == 'INTELLI') {
        $scope.valveSubtypes[i].dictionaryValue = 'INTELLI';
      }
    }

    $scope.addresses = Array.apply(null, { length: 16 }).map(function (element, index) { return index + 1; });

    $scope.effiMode = Array.apply(null, { length: 4 }).map(function (element, index) { return index + 1; });

    $scope.errorSave = function(errorMessage) {
      Notification.error({message: 'Failed!' + ' ' + errorMessage, delay: 1000});
    };

    $scope.successSave = function() {
      Notification.success({message: 'Saved!', delay: 1000});
    };

    $scope.successCancel = function() {
      Notification.success({message: 'Cancelled!', delay: 1000});
    };

    $scope.getReadableFromSystem = function(systemValue) {
      var match = $scope.acts.filter(function(act) {
        return act.systemValue === systemValue;
      });
      if(match.length > 0) {
        return match[0].readableValue;
      }
      return systemValue;
    };

    $scope.combineName = function(obj) {
      return $interpolate('{{sName}} - VALVES')(obj);
    };
  
    $scope.getHeaterNamePerType = function(heater){
        var sName = "Heater"
        switch(heater.subtype) {
            case 'GENERIC': 
                sName = "Gas Heater";
                break;
            case 'HTPMP':                
                sName = "Heat Pump";
                break;
            case 'SOLAR':
                sName = "Solar Heater";
                break;
            case 'ULTRA':                
                sName = "UltraTemp";
                break;
            case 'HCOMBO':
                sName = "Hybrid";
                break;
            case 'HXSLR':
                sName = 'Solar Preferred';
                break;
            case  'HXHTP': 
                sName = 'Heat Pump Preferred';
                break;
            case 'HXULT': 
                sName = 'UltraTemp Preferred';
                break;
            case 'MASTER':
                sName = 'MasterTemp';
                break;
            case 'MAX':
                sName = 'Max-E-Therm';
                break;

            default:
                sName = "Heater";
                break;
        }
        return sName;
    }

    $scope.isPortHeaterAvailable = function (list_address, address) {

        var strAddr = address.toString() 
        if (list_address === strAddr) // same address allow to select
            return true;

        var available = true;
                
        angular.forEach($scope.heaters, function (heater) {
            switch (heater.subtype) {
                case "HCOMBO":
                case "MASTER":
                case "MAX":
                case "ULTRA":
                    if (heater.address == strAddr)
                        available = false;  // do notuse this address
                    break;
            }
        });            
        

        return available;
    };

    $scope.getHeaterAvailablePort = function (heaterSelected) {    // #2 if there are 2 or more of the same type, empty if is the only one heater type
        var address_Selected = 1;
        // now check how many are on the same type
        for (i = 1; i < 16; i++) {
            var used = false;
            angular.forEach($scope.heaters, function (heater) {
                switch (heater.subtype) {
                    case "HCOMBO":
                    case "MASTER":
                    case "MAX":
                    case "ULTRA":
                        if (heater.address == i)
                            used = true;
                       break;                            
                }
            });
            if (used == false)
                return i;   // port is available
        
        }        

        return address_Selected;
    };

    $scope.getHeaterNameWithCount = function (heaterSelected,start) {    // #2 if there are 2 or more of the same type, empty if is the only one heater type

        var subtype = heaterSelected.subtype;
        var name = $scope.getHeaterNamePerType(heaterSelected);
        var count = start
        // now check how many are on the same type
        angular.forEach($scope.heaters, function (heater) {
            if (heater.subtype === subtype)
                count++;
        });

        if (count > 1) {            
            name += " #" + count.toString();
        }

        return name;        
    };
    

    $scope.onTypeChange = function(heater, heaterSubtype) {
      if(heater.subtype === heaterSubtype.actualValue) {
        return;
      }
      heater.subtype = heaterSubtype.actualValue;
      heater.body = $scope.getDefaultBody(heater)
      switch(heater.subtype) {
        case 'GENERIC': 
          // delete optional fields if they are there
          delete heater.startTempDelta;
          delete heater.stopTempDelta;
          delete heater.nocturnalCooling;
          delete heater.coolingEnabled;
          delete heater.address;          
          heater.sName = $scope.getHeaterNameWithCount(heater,0);
          break;
        case 'HTPMP':
          delete heater.stopTempDelta;
          delete heater.nocturnalCooling;
          delete heater.coolingEnabled;
          delete heater.address;
          heater.startTempDelta = 6;
          heater.sName = $scope.getHeaterNameWithCount(heater,0);
          break;
        case 'SOLAR':
          delete heater.coolingEnabled;
          delete heater.address;
          heater.startTempDelta = 6;
          heater.stopTempDelta = 3;
          heater.nocturnalCooling = false;
          heater.sName = $scope.getHeaterNameWithCount(heater,0);
          break;
        case 'ULTRA':
          delete heater.stopTempDelta;
          delete heater.nocturnalCooling;
          heater.startTempDelta = 6;
          heater.coolingEnabled = false;
          heater.address = $scope.getHeaterAvailablePort(heater) 
          heater.sName = $scope.getHeaterNameWithCount(heater,0);
          break;
       case 'HCOMBO':
          delete heater.stopTempDelta;
          delete heater.nocturnalCooling;
          heater.startTempDelta = 5;    // boost temp
          heater.coolingEnabled = false;
          heater.address = $scope.getHeaterAvailablePort(heater)
          heater.sName = "Hybrid";
          heater.efficiencyMode = $scope.HEATMODE_HYBRID; //3;    // hybrid mode
          heater.boostTemp = 5;         // boost temp 
          heater.economyTime = 1;          // hybrid mode
          heater.sName = $scope.getHeaterNameWithCount(heater,0);
          break;
     case 'MASTER':   // MASTERTEMP
         delete heater.stopTempDelta;
         delete heater.nocturnalCooling;
         heater.cooldowndelay = 5;
         heater.address = $scope.getHeaterAvailablePort(heater)
         heater.sName = "Max-E-Therm";
         heater.sName = $scope.getHeaterNameWithCount(heater,0);
         break;
      case 'MAX':   // MAX-E-THERM
         delete heater.stopTempDelta;
         delete heater.nocturnalCooling;          
         heater.cooldowndelay = 5;
         heater.address = $scope.getHeaterAvailablePort(heater)
         heater.sName = "Max-E-Therm";                  
         heater.sName = $scope.getHeaterNameWithCount(heater,0);
         break;

      }
    };


    // Business logic
    $scope.isSolarHeaterType = function(heater) {
      return heater.subtype === 'SOLAR';
    };

    $scope.showRowThree = function(heater) {
        return !!(heater.subtype === 'HTPMP' || heater.subtype === 'ULTRA' || heater.subtype === 'HCOMBO' ||
                  heater.subtype === 'MAX' || heater.subtype === 'MASTER' );
    };

    $scope.isHeaterCoolDownDelay = function (heater) {
        var status = false;
        if (
            (heater.subtype === 'MAX') ||
            (heater.subtype === 'MASTER'))
            status = true;

        return status;
    };

    $scope.isHeaterConnectedGas = function (heater) {
        var status = false;
        if (
            (heater.subtype === 'MAX') ||
            (heater.subtype === 'MASTER'))
            status = true;

        return status;
    };

    $scope.isHybrid = function (heater) {
        var status = heater.subtype === 'HCOMBO';
        return status;
    };

    $scope.supportDiffentialTemp = function (heater) {
        var status = heater.subtype === 'ULTRA';
        return status;
    };

    $scope.isHybridMode = function(heater){
        if ($scope.isHybrid(heater) === false)  // not a hybrid heater
            return false;

        if (heater.efficiencyMode) {
            if (heater.efficiencyMode === $scope.HEATMODE_HYBRID) {
                return true;
            }
        }
        
      return false;
      }

    $scope.showAllRowThree = function(heater) {
        return (heater.subtype === 'ULTRA' || heater.subtype === 'HCOMBO' ) ;
    };

    // when first visiting the page, open the first heater accordion if there is one.
    if($scope.heaters.length > 0) {
      $scope.accordionStateByObjectNames[$scope.heaters[0].objectName] = { open: true };
    }

    $scope.getBodyNameByObjectName = function(objectName) {
      var filterResult = $scope.propertyBodies.filter(function(propertyBody) {
        if(propertyBody.OBJNAM === objectName) {
          return propertyBody;
        }
      });

      if (filterResult.length > 0) {
          var temp = $scope.filterWordOnly(filterResult[0].SNAME);
          return temp;
      }
        
      if (objectName === "B1101 B1202") {   // FILTER "B1101 B1202" // this is dual body
          objectName = "Pool";
      }


      return objectName;
    };


    $scope.getEfficiencyMode = function (mode) {
        var result = 'Hybrid';
        switch (mode) {
            case $scope.HEATMODE_NONE: // 0:
            case 0:
                result = "";
                break;
            case $scope.HEATMODE_HEAT_PUMP:     // 1:
            case 1:
                result = "Heat Pump Only";
                break;
            case $scope.HEATMODE_GAS:           //2:
            case 2:
                result = "Gas Heat Only";
                break;
            case $scope.HEATMODE_HYBRID:        //3:
            case 3:
                result = "Hybrid Mode";
                break;
            case $scope.HEATMODE_DUAL:          //4
            case 4:
                result = "Dual Mode";
                break;

            default:
                return "Hybrid Mode";
        }
        return result;
    };
       
    $scope.isHeaterPrefer = function (heater) {      
        var enabled = false;
        //console.log("----------------------- heaters:: --------------------- ", heater);
        if (heater.objectName) {                
            angular.forEach($scope.heaterPrefer.types, function (item) {
                if (heater.objectName === item.key) {
                    enabled = true
                }
            });
       }
       return enabled;
    };

    $scope.getHeaterNamefromSubtype = function (type) {
        var name = type;
        //console.log("type is: ", name);       

        angular.forEach($scope.heaterTypes.types, function (item) {
            if (type === item.key)
                name = $scope.filterWordOnly(item.value);            
        });
        return name;
    };

    $scope.isGasHeaterAvailable = function () {
        var status = true
        ds
        return status;
    };


    $scope.isSingleOrSharedConfiguration = function () {
      var isEnable = false;
      if (($scope.model.PANID === "SINGLE") ||
           ($scope.model.PANID === "SHARE"))
          isEnable = true;

      return isEnable
    };

    $scope.isDualOrShare = function () {
        if (($scope.model.PANID === "DUAL") ||
            ($scope.model.PANID === "SHARE")) {
            return true;
        }
        else {
            return false;
        }
    };

    $scope.isDualConfiguration = function () {
        if ($scope.model.PANID === "DUAL") {
            return true;
        }
        else {
            return false;
        }
    };


    $scope.getCoverIsEnabled = function (enabled) {
        if (enabled === "ON") {
            return "ENABLED";
        } else {
            return "DISABLED";
        }        
    };

    $scope.getCurrentMin = function () {
        var result = 40
        return result.toString();
    };

   // return current range
    $scope.getCurrentRange = function (defaultMin, defaultMax, low, high ) {
        //var range = "min:15,max:MAX"
               
        var MIN = low
        var MAX = high
        
        // check minimum value does not goes below default
        if (MIN < defaultMin)
            MIN = defaultMin

        // check maximum value does not goes above default
        if ( MAX > defaultMax )
            MAX = defaultMax

        //var range = "min:" + MIN + ",max:" + MAX        

        var range = {
            min: MIN,
            max: MAX
        };
        var result = '"' + range + '"'
        return result;

    };


    $scope.getCurrentStep = function (pump) {
        var step = "1"
        if (pump & pump.SETTMP)
            step = pump.SETTMP

        step = '"' + step + '"'

        return step
    }
            
      
    $scope.deleteHeaterFromGroup = function(heaterGroup, index) {
      heaterGroup.params.OBJLIST.splice(index, 1);
    };

    $scope.getDictionaryValueHeater = function (actualValue) {
        var name = "";
        // filter for MAX and MASTER tokens only!
        switch (actualValue) {
            case "MASTER":
            case "MAX":
                name = "Connected Gas";
                break;
            case "GENERIC":
                name = "Gas";
                break;
            
            default:
                name = $scope.getDictionaryValue(actualValue);
                break;
                
        }
        return name;        
    }

    // used for heater droplist 
    $scope.getHeaterNameType = function (type) {
        var name = "";        
        switch (type) {
            case "Master Cleaner":  // using 
                name = "MasterTemp";
                break;
            case "MAX":
                name = "Max-E-Therm";
                break;
            case "heater":
                name = "Gas";
                break;

            default:
                name = type;
                break;
        }
        return name;
    }

      // used for heater droplist 
    $scope.getHeaterName = function (type) {
 
        var name = $scope.getDictionaryValue(type);
        switch (type) {            
            case "MASTER":  
                name = "MasterTemp";
                break;
            case "MAX":
                name = "Max-E-Therm";
                break;
            case "heater":
                name = "Gas";
                break;
            case "GENERIC":
                name = "Gas";
                break;
            case "SOLAR":
                name = "Solar";
                break;
            case "HTPMP":
                name = "Heat Pump";
                break;
            case "ULTRA":
                name = "UltraTemp";
                break;
            case "HCOMBO":
                name = "Hybrid";
                break;
            default:
                name = type;
                break;
        }
        return name;
    }

    $scope.getDictionaryValue = function (actualValue) {
      if (!actualValue)
          return actualValue;          

      var match = $scope.heaterSubtypes.filter(function (heaterSubtype) {
          if (heaterSubtype.actualValue === actualValue) {
              if (heaterSubtype.dictionaryValue) { // check if exist
                  var name = heaterSubtype.dictionaryValue;
                  heaterSubtype.dictionaryValue = $scope.filterWordOnly(name)
              }
            return heaterSubtype;
        }
      });
        
      if (match && match.length > 0 && match[0].dictionaryValue) {
          var temp = $scope.filterWordOnly(match[0].dictionaryValue);
          return temp;
      }
      return actualValue;
    };


     // remove 'Only' word from value
    $scope.filterWordOnly = function (Value) {
        if (!Value)
            return Value;

        if (Value.indexOf('Only') >=0 ) {
            // removing only word
            Value = Value.replace('Only', '');
            
        }

        if (Value.indexOf('ONLY') >= 0) {
            // removing only word
            Value = Value.replace('ONLY', '');

        }

        return Value;
    };


    $scope.getValveDictionaryValue = function(actualValue) {
      if (!actualValue)
        return actualValue;
      var match = $scope.valveSubtypes.filter(function (valveSubtype) {
        if (valveSubtype.actualValue === actualValue) {
          return valveSubtype;
        }
      });
        
      if (match && match.length > 0 && match[0].dictionaryValue) {
        // removing only word
          var temp = $scope.filterWordOnly(match[0].dictionaryValue);
        return temp;  
      }
      return actualValue;
    };

    function updateValves() {
        propertyService.GetHardwareDefinitionPrecise().then(function () {
        $scope.valvePanels = $scope.model.hardwareDefinition.map(function(hPanel) {
          var valvePanel = {};
          valvePanel.objectName = hPanel.objnam;
          valvePanel.hName = hPanel.params.HNAME;
          valvePanel.sName = hPanel.params.SNAME;
          valvePanel.circuits = hPanel.params.OBJLIST.map(function(hModule) {
            var circuits = [];
            if (hModule.params.CIRCUITS) {
              circuits = circuits.concat(hModule.params.CIRCUITS
                .filter(function(filterCircuit) {
                  // Need to filter for valves, the only reliable way on the call currently is to check whether the
                  // first letter on the object name is a capital v.
                  if (filterCircuit.objnam.indexOf('V') === 0) {
                    return filterCircuit;
                  }
                })
                .map(function(hCircuit) {
                  var circuit = hCircuit.params;
                  circuit.OBJNAM = hCircuit.objnam;
                  circuit.DLY = (circuit.DLY === 'ON');
                  return circuit;
                })
              );
            }
            return circuits;
          });
          // flatten the module array
          valvePanel.circuits = [].concat.apply([], valvePanel.circuits);
          valvePanel.circuits.sort(function(a, b) {
            var keyA = a.listOrder,
              keyB = b.listOrder;
            if (keyA < keyB) return -1;
            if (keyA > keyB) return 1;
            return 0;
          });
          return valvePanel;
        });

        if($scope.valvePanels.length > 0) {
          $scope.accordionStateByObjectNames[$scope.valvePanels[0].objectName] = {};
          $scope.accordionStateByObjectNames[$scope.valvePanels[0].objectName].open = false; //true;
          // if($scope.valvePanels[0].circuits.length > 0) {
          //   $scope.accordionStateByObjectNames[$scope.valvePanels[0].circuits[0].OBJNAM] = { open: true };
          // }
        }

        $scope.valvePanelsBeforeEdit = angular.copy($scope.valvePanels);
      });
    }

    //---- check mif valve is INTAKE OR RETURN
    $scope.isNotIntakeorReturnValve = function (valve, index) {
        var value = valve.circuits[index].SNAME;
        
        if (value === 'Intake' || value === 'Return') {
            //console.log("==== IS isIntakeorReturnValve : ", valve.circuits[index].SNAME, " index ", index);
            return false;
        }
        else { 
            //console.log("++++   IS NOT isIntakeorReturnValve : ", valve.circuits[index].SNAME, " index ", index);
            return true;
        }
    }
    
    $scope.getSelectValveDescription = function (circuit, valve, index) {        
        var value = valve.circuits[index].SNAME;
        if (value === 'Intake' || value === 'Return') {            
            return "Rotates on Pool/Spa";
        }
        else {            
            return value ; //"Select A Type";
        }       
    }
    
    $scope.saveValve = function(valve, parentIndex, index) {
      var params = {};      
      params.SUBTYP = valve.SUBTYP;
      params.SNAME = valve.SNAME;
      params.DLY = (valve.DLY === true) ? 'ON' : 'OFF';               

      if (valve.CIRCUIT) { // if exist the assingned                         
            params.CIRCUIT = valve.OBJLIST[0].params.CIRCUIT;
      }

      $rootScope.editMode.otherequipment[valve.OBJNAM] = false;
      propertyService.SetParams(valve.OBJNAM, params).then(function() {
        if(valve.ASSIGN !== 'RETURN' && valve.ASSIGN !== 'INTAKE') {         
            propertyService.SetParams(valve.OBJNAM, params).then(function () {    // FIX BUG WEB CLIENT 9

            // retrieve params from target board
            var params1 = "SUBTYP : SNAME : CIRCUIT";
            propertyService.GetParamListByObject(params1, valve.OBJNAM);
               
            //updateValves(); // REMOVING RETRIEVE ALL  HARDWARE DEFINITION
            $rootScope.forms.otherequipment.valve[parentIndex][index].$setPristine();
            $scope.successSave();
          });
        } else {
          updateValves();
          $rootScope.forms.otherequipment.valve[parentIndex][index].$setPristine();
          $scope.successSave();
        }
      });
    };

          
    $scope.getSelectCircuitName = function (circuit, index) {
        var name = "Select Circuit"
        if (circuit.OBJLIST) {
            if (circuit.OBJLIST[0].params.CIRCUIT != null)
                name = $scope.model.Objects[circuit.OBJLIST[0].params.CIRCUIT].SNAME;
        } else {
            if ($scope.model.Objects[circuit.CIRCUIT])  // check if object exists
                name = $scope.model.Objects[circuit.CIRCUIT].SNAME;
        }
        //console.log(" selected circuit: " + name);
        return name; 
    }

    $scope.getSelectCircuitCoverName = function (circuit, index) {
        var name = "Select Circuit"        
        if (circuit.objnam != "00000") {
            var name = circuit.objnam;
            if ($scope.model.Objects[name])  // check if object exists    if (name != null)
                name = $scope.model.Objects[name].SNAME;
        }
 /*       else {
            if ($scope.model.Objects[name])  // check if object exists
                name = $scope.model.Objects[circuit.CIRCUIT].SNAME;
        }
        */
        //console.log(" selected circuit: " + name);
        return name;
    }
    
    // processing deleted groups
    $scope.$on('UpdateParamList', function (params) {
        updateHeaterGroups();
        /*
        angular.forEach($scope.heaterGroups, function(group)){
            if (group.OBJNAM === item.OBJNAM){
                delete $scope.heaterGroups[item.OBJNAM];                
            }
        });
        */
    });

    $scope.$on('NotifyList', function (objname, item, obj) {
        // process valves 
        angular.forEach($scope.valvePanels, function (valvePanel) {
            angular.forEach(valvePanel.circuits, function (circuit) {
                
                if (circuit.OBJNAM === obj.objnam) { // check match
                    if (obj.params.SNAME != null )
                        circuit.SNAME = obj.params.SNAME;

                    if (obj.params.CIRCUIT != null) {
                        circuit.CIRCUIT = obj.params.CIRCUIT;                        
                        //console.log("NotifyList : set valve name :" + obj.params.CIRCUIT);
                    }
                }
            });
        });
    });

    $scope.$on('CircuitUpdate', function (objname, item, obj) { 
        // process valves 
        angular.forEach($scope.valvePanels, function (valvePanel) {
            angular.forEach(valvePanel.circuits, function (circuit) {
                if (item.objectList[0]) { // CHECK IF EXIST
                    if (item.objectList[0].objnam) { // CHECK IF EXIST
                        if (circuit.OBJNAM === item.objectList[0].objnam) { // check match
                            if (item.objectList[0].params.SNAME != null)
                                circuit.SNAME = item.objectList[0].params.SNAME;
                            if (item.objectList[0].params.CIRCUIT != null) {
                                circuit.CIRCUIT = item.objectList[0].params.CIRCUIT;
                                circuit.OBJLIST[0].params.CIRCUIT = item.objectList[0].params.CIRCUIT;
                                //console.log(" CircuitUpdate : set valve name :" + item.objectList[0].params.CIRCUIT);
                            }
                        }
                    }
                }
            });
        });
    });

    $scope.isReturnOrIntake = function (name) {
        if ((name === 'Intake') || (name === 'Return')) {
            return true;
        }
        else {
            return false;
        }
    };

    $scope.coverDeleteAllAffectedObjects = function (itemObjName) {

        angular.forEach($scope.holddeletedCircuitFromCover, function (circuit) {    // remove each object affected first
          //  console.log("removing cover object affect " + circuit.objnam + " id : " + circuit.objID)

            var args = {
                objnam: circuit.objID,
                params: { CIRCUIT: '00000' }
            };
            
            propertyService.DeleteCoverCircuit(args)
        });
    };

    $scope.saveCover = function(cover, index) {
      
        var params = {
           // SNAME: cover.params.SNAME,    // we do no supoport change name 
            NORMAL: cover.params.NORMAL,
            STATUS: cover.params.STATUS,
        };
        
        var sc = "";    // selected circuits

        // first delete all cover circuits
        
        $scope.coverDeleteAllAffectedObjects(cover.objnam);
        // create new remain object circuits
        //console.log(" ---------------- SAVING COVER ------------------- ")
        var newCircuits = [];
        // selecting new circuits only
        angular.forEach(cover.circuits, function (circuit) {
            if (!circuit.objparent)    // if objnam does not exist then added to circuit are new
                newCircuits.push(circuit);
        });
            
        // Removing just the newones
        if (newCircuits.length > 0)
            propertyService.createCircuitObjectAffects(cover.objnam, newCircuits);       //propertyService.createCircuitObjectAffects(cover.objnam, cover.circuits);
        
        //propertyService.editObjectAffect(cover.objnam, cover.circuits); 
        propertyService.SetParams(cover.objnam, params).then(function() {
            updateCovers();
            $rootScope.forms.otherequipment.cover[index].$setPristine();
            $scope.successSave();
        })
   };

    $scope.cancelEdit = function(object, parentIndex, index) {
      if(parentIndex === -1) {
        $scope.heaters = angular.copy($scope.heatersBeforeEdit);
        $rootScope.forms.otherequipment.heater[index].$setPristine();
      } else {
        $scope.valvePanels = angular.copy($scope.valvePanelsBeforeEdit);
        $rootScope.forms.otherequipment.valve[parentIndex][index].$setPristine();
      }
      object.editMode = false;
      $scope.successCancel();
    };

    $scope.cancelCoverEdit = function(cover, index) {
      $scope.covers = angular.copy($scope.coversBeforeEdit);
      $rootScope.forms.otherequipment.cover[index].$setPristine();
      $scope.successCancel();
    };

    $scope.cancelHeaterGroupEdit = function(object, index) {
      $rootScope.forms.otherequipment.heatergroup[index].$setPristine();
      $scope.heaterGroups = angular.copy($scope.heaterGroupsBeforeEdit);
      $scope.successCancel();
    };

    $scope.saveHeaterGroup = function(heaterGroup, index) {
      if(!heaterGroup.params.OBJLIST) {
        heaterGroup.params.OBJLIST = [];
      }
      var args = {
        ACT: heaterGroup.params.ACT,
        SNAME: heaterGroup.params.SNAME,
        OBJLIST: []
      };
      angular.forEach(heaterGroup.params.OBJLIST, function(heater) {
        var tempHeater = {
          objnam: heater.params.USE,
          params: {
            LISTORD: heater.params.LISTORD + ''
          }
        };
        args.OBJLIST.push(tempHeater);
      });
      if(heaterGroup.createMode) {
        args.OBJTYP = 'HCOMBO';
        propertyService.CreateGroup(args).then(function() {
          propertyService.GetHardwareDefinition().then(function() {
            // $rootScope.editMode.otherequipment[heaterGroup.objnam] = false;
            heaterGroup.createMode = false;
            updateHeaterGroups();
            $rootScope.forms.otherequipment.heatergroup[index].$setPristine();
            $scope.successSave();
          });
        });
      } else {
        args.OBJNAM = heaterGroup.objnam;
        propertyService.EditGroup(args).then(function() {
          propertyService.GetHardwareDefinition().then(function() {
            // $rootScope.editMode.otherequipment[heaterGroup.objnam] = false;
            updateHeaterGroups();
            $rootScope.forms.otherequipment.heatergroup[index].$setPristine();
            $scope.successSave();
          });
        });
      }
    };


    $scope.$on('UpdatedHeaters', function (objname, item, obj) {

        console.log("---- processing updated heaters ..", item);

        updateHeaters();
    });



    $scope.saveHeater = function(heater, index) {
      if (heater.sName === '')
        return;
      var params = {};
      params.SNAME = heater.sName;
      params.SUBTYP = heater.subtype;
      params.BODY = heater.body;
      switch (heater.subtype) {
        case 'GENERIC':
          params.DLY = heater.cooldowndelay + '';
          break;
        case 'HTPMP':
          params.START = heater.startTempDelta + '';
          break;
        case 'SOLAR':
          params.START = heater.startTempDelta + '';
          params.STOP = heater.stopTempDelta + '';
          params.COOL = (heater.nocturnalCooling) ? 'ON' : 'OFF';
          break;
        case 'ULTRA':
          params.START = heater.startTempDelta + '';
          params.COOL = (heater.coolingEnabled) ? 'ON' : 'OFF';
          params.COMUART = heater.address + '';
          break;
        case 'HCOMBO': 
            params.START = heater.startTempDelta + '';
            params.COOL =  'OFF';
            params.COMUART = heater.address + '';
            params.HTMODE = heater.efficiencyMode + '';     // hybrid mode
            params.BOOST = heater.boostTemp + '' ;         // hybrid mode
            params.TIME = heater.economyTime + '';          // hybrid mode
            break;

        case 'MASTER':
            params.START = heater.startTempDelta + '';           
            params.COMUART = heater.address + '';                        
            params.DLY = heater.cooldowndelay + '';
            break;

        case 'MAX':
            params.START = heater.startTempDelta + '';            
            params.COMUART = heater.address + '';                        
            params.DLY = heater.cooldowndelay + '';
            break;
      }

      var heaterParams = Enumerations.LiveParams.filter(function(liveParam) {
        if(liveParam.objtyp && liveParam.objtyp === 'heater') {
            return liveParam;
        }
      })[0].liveParams;
      // $rootScope.editMode.otherequipment[heater.objectName] = false;
      if(heater.createMode) {
        propertyService.CreateNewObject('HEATER', params).then(function(result) {
          propertyService.RequestParamList([{
            objnam:result.objnam,
            keys:["OBJTYP","SNAME"].concat(heaterParams)}]).then(function(result) {
            messageProcessor.ProcessHeater(result.objectList[0]);
            updateHeaters();
            $rootScope.forms.otherequipment.heater[index].$setPristine();
            $scope.successSave();
          });
        });
      } else {
        // ALOW TO CHANGE BODY
        // delete params.BODY;      
        delete params.SUBTYP;
        propertyService.SetParams(heater.objectName, params).then(function() {
          propertyService.RequestParamList([{
            objnam:heater.objectName,
            keys:["OBJTYP","SNAME"].concat(heaterParams)}]).then(function(result) {
            messageProcessor.ProcessHeater(result.objectList[0]);
            updateHeaters();
            $rootScope.forms.otherequipment.heater[index].$setPristine();
            $scope.successSave();
          });
        });
      }
    };

    $scope.removeHeaters = function() {
      // populate the list of object names we are going to delete
      $scope.objectNamesToDelete = $scope.heaters.map(function(obj) {
        if (obj.isSelected) {
          return obj.objectName;
        }
      });
      propertyService.DeleteObject($scope.objectNamesToDelete).then(function() {
        angular.forEach($scope.objectNamesToDelete, function(objectNameToDelete) {
          delete $scope.model.Objects[objectNameToDelete];
        });
        updateHeaters();
      });
    };

    $scope.addHeaterToGroup = function(heaterGroup) {
      var max = 0;
      angular.forEach(heaterGroup.params.OBJLIST, function(heater) {
        var tempListOrd = parseInt(heater.params.LISTORD);
        max = (tempListOrd > max) ? tempListOrd : max;
      });
      var listOrder = (max + 1) + '';
      var heater = {
        objnam: Math.uuid(4),
        params: {
          LISTORD: listOrder
        }
      };
      if(!heaterGroup.params.OBJLIST) {
          heaterGroup.params.OBJLIST = [];
      }
      heaterGroup.params.OBJLIST.push(heater);
    };

    $scope.setHeaterToGroup = function(heater, objnam) {
      heater.params.USE = objnam;
    };

    $scope.getHeatersCount = function () {

        var count = $scope.heaters.length;
        var prefercount = 0;
        angular.forEach($scope.heaters, function (heater) {
            if ($scope.isHeaterPrefer(heater) === true)
                prefercount = prefercount + 1;
        });
        
          
        count = count - prefercount;
        return count;
    };
            // check can add more FeatureCircuits
    $scope.canAddHeaters = function () {
        var free = $scope.maxheaters - $scope.heaters.length;

        var availableType = $scope.getAvailableHeaterType()

        if ( (free > 0 ) && (availableType != "") )
            return true;
        else
            return false;
    };

    $scope.areSelectedHeatersToRemove = function () {
        var enabled = false;

        $scope.heaters.map(function (obj) {
            if (obj.isSelected) {
                enabled = true;     // at least 1 is enable (selected).
            }
        });

        return enabled;
    }

    $scope.enabledAddHeaters= function () {
        if (getAvailableHeaterType() == "")
            return false
        else
            return true;
    }

    $scope.getAvailableHeaterType = function () {
        var available = ""
        if ($scope.isheaterAvailable("GENERIC"))
            return "GENERIC"

        if ($scope.isheaterAvailable("SOLAR"))
            return "SOLAR"

        if ($scope.isheaterAvailable("HTPMP"))
            return "HTPMP"

        if ($scope.isheaterAvailable("ULTRA"))
            return "ULTRA"

        if ($scope.isheaterAvailable("HCOMBO"))
            return "HCOMBO"

        if ($scope.isheaterAvailable("MASTER"))
            return "MASTER"

        if ($scope.isheaterAvailable("MAX"))
            return "MAX"

        return "" // NONE IS AVAILABLE
    }

    $scope.addHeater = function () {
        $scope.setAvailableHeaters();
        var availableType = $scope.getAvailableHeaterType()

        var newHeater = {
                objectName: Math.uuid(4),
                subtype: availableType,  //'GENERIC',
            isSelected: false,
            editMode: true,
            createMode: true,
            startTempDelta: 6,
            stopTempDelta: 3,
            nocturnalCooling: false,
            coolingEnabled: false,
            address: 1,
            efficiencyMode: 3,  // hybrid mode
            boostTemp: 5,       // hybrid mode
            economyTime: 1,        // hybrid mode
            cooldowndelay: 5,      // cool down delay
    };

    switch(availableType) {
        case 'GENERIC':
        // delete optional fields if they are there
            delete newHeater.startTempDelta;
            delete newHeater.stopTempDelta;
            delete newHeater.nocturnalCooling;
            delete newHeater.coolingEnabled;
            delete newHeater.address;
            delete newHeater.efficiencyMode;        // hybrid mode
            delete newHeater.boostTemp;             // hybrid mode
            delete newHeater.economyTime;           // hybrid mode

            newHeater.cooldowndelay = 5;
            newHeater.sName = $scope.getHeaterNameWithCount(newHeater, 1);
            break;
        case 'HTPMP':
            delete newHeater.stopTempDelta;
            delete newHeater.nocturnalCooling;
            delete newHeater.coolingEnabled;
            delete newHeater.address;
            newHeater.startTempDelta = 6;
            newHeater.sName = $scope.getHeaterNameWithCount(newHeater, 1);

            break;
        case 'SOLAR':
            delete newHeater.coolingEnabled;
            delete newHeater.address;
            newHeater.startTempDelta = 6;
            newHeater.stopTempDelta = 3;
            newHeater.nocturnalCooling = false;
            newHeater.sName = $scope.getHeaterNameWithCount(newHeater, 1);
            break;
        case 'ULTRA':
            delete newHeater.stopTempDelta;
            delete newHeater.nocturnalCooling;
            newHeater.startTempDelta = 6;
            newHeater.coolingEnabled = false;
            newHeater.address = 1;
            newHeater.sName = $scope.getHeaterNameWithCount(newHeater, 1);
            break;
        case 'HCOMBO':
            delete newHeater.stopTempDelta;
            delete newHeater.nocturnalCooling;
            newHeater.startTempDelta = 6;
            newHeater.coolingEnabled = false;
            newHeater.address = 1;
            newHeater.sName = $scope.getHeaterNameWithCount(newHeater, 1);
            break;
        case 'MASTER':
            delete newHeater.stopTempDelta;
            delete newHeater.nocturnalCooling;
            newHeater.cooldowndelay = 5;
            newHeater.address = 1;
            newHeater.sName = $scope.getHeaterNameWithCount(newHeater, 1);
            break;
        case 'MAX':
            delete newHeater.stopTempDelta;
            delete newHeater.nocturnalCooling;
            newHeater.cooldowndelay = 5;
            newHeater.address = 1;
            newHeater.sName = $scope.getHeaterNameWithCount(newHeater, 1);
            break;

    }

      newHeater.body = $scope.getDefaultBody(newHeater)
      $rootScope.editMode.otherequipment[newHeater.objectName]= true;
      $scope.heaters.push(newHeater);
        // have the accordion open for it.
        $scope.accordionStateByObjectNames[newHeater.objectName]= {
        open: true
    };


    };

    $scope.isDualConfiguration = function () {
        if (PropertyModel.PANID === "DUAL") {
            return true;
    }
    else {
            return false;
    }
    };
    $scope.setEditMode = function (circuit) {
      $rootScope.editMode.otherequipment[circuit.OBJNAM]= true;
    };
    $scope.removeHeaterGroups = function () {
        // populate the list of object names we are going to delete
        $scope.objectNamesToDelete = $scope.heaterGroups.filter(function (obj) {
          if (obj.isSelected) {
            return obj;
        }
      }).map(function (obj) {
        return obj.objnam;
    });
    if($scope.objectNamesToDelete.length > 0) {
      propertyService.DeleteObject($scope.objectNamesToDelete).then(function () {
          propertyService.GetHardwareDefinition().then(function () {
            angular.forEach($scope.objectNamesToDelete, function(nameToDelete) {
              propertyModel.Heaters = propertyModel.Pumps.filter(function (heater) {
                if(heater.objnam !== nameToDelete) {
                  return heater;
              }
            });
          });
            updateHeaterGroups();
      });
    });
    }
    };

    $scope.addHeaterGroup = function () {
        var heaterGroup = {
                objnam: Math.uuid(4),
            createMode: true,
              params: {ACT: 'CONCUR'
        },
            OBJLIST: []
    };
    $scope.sortableOptionsForHeaterGroups[heaterGroup.objnam]= {
              update: function (e, ui) {
                  // callback before changed model, don't need to do anything here.
    },
              stop: function (e, ui) {
            var currentElement = $(e.toElement);
            var attempts = 0;
                  // we try to grab parentsUntil class row to get the id
            while(!$(currentElement).hasClass("row")) {
              currentElement = $(currentElement).parent();
              if(attempts > 2) { //> 5) {       // retries only 2 times
                break;
            }
          }
                  // if we found the row proceed
            if($(currentElement).hasClass("row")) {
                //get the heater group's id
              var rowId = $(currentElement).attr('id');
              var heaterGroupId = parseInt(rowId.split('-')[1]);
              $scope.heaterGroups[heaterGroupId].params.OBJLIST = $scope.heaterGroups[heaterGroupId].params.OBJLIST.map(function (heater, idx) {
                heater.params.LISTORD = 1 +idx;
                return heater;
            });
          }
    }
    };
    $rootScope.editMode.otherequipment[heaterGroup.objnam]= true;
    $scope.heaterGroups.push(heaterGroup);
    $scope.accordionStateByObjectNames[heaterGroup.objnam]= {
    };
    $scope.accordionStateByObjectNames[heaterGroup.objnam].open = true;
    };

    $scope.isValidHeaterGroup = function(heaterGroup) {
        // needs a sname and subtype
        if(!heaterGroup.params.SNAME) {
          return false;
      }
      if(!heaterGroup.params.ACT) {
        return false;
    }
        // for all heaters, there needs to be a circuit assigned.
        if(heaterGroup.params.OBJLIST && heaterGroup.params.OBJLIST.length > 0) {
          for (var i=0; i < heaterGroup.params.OBJLIST.length; i++) {
          if(!heaterGroup.params.OBJLIST[i].params.USE) {
            return false;
        }
        }
        } else {
          return false;
      }
      return true;
    };

    $scope.addCircuitToCover = function (cover) {
      cover.circuits.push({
              objnam: '00000'
    });
    };

    $scope.changeCoverCircuit = function (coverIndex, circuitIndex, option) {

      angular.copy(propertyModel.Objects[option.OBJNAM], $scope.covers[coverIndex].circuits[circuitIndex]);
        // TODO need use another var for selecting valid circuits depending on what circuits are aleady assigned to the cover.
    };

    $scope.changeCoverSelectedCircuit = function (circuit, circuitIndex, option) {

        //console.log("Selected: " + option.OBJNAM);
        circuit.objnam = option.OBJNAM;
        //angular.copy(propertyModel.Objects[option.OBJNAM], $scope.covers[coverIndex].circuits[circuitIndex]);
        // TODO need use another var for selecting valid circuits depending on what circuits are aleady assigned to the cover.
    };

    $scope.deleteCircuitFromCover = function (cover, circuitIndex) {
        // first saved so can be sended to THW        

        var newObject = {
                objnam: cover.circuits[circuitIndex].objnam,
                objparent: cover.circuits[circuitIndex].objparent,
                objID: cover.circuits[circuitIndex].objID
    };

        $scope.holddeletedCircuitFromCover.push(newObject);

        cover.circuits.splice(circuitIndex, 1);
    };

        function updateHeaters() {
            $scope.heaters = $filter('filterOutByObjectType') ($scope.model.Objects, 'HEATER').filter(function (propertyHeater) {

         if (propertyHeater.SUBTYP !== 'NONE') {    //'HCOMBO'
            return propertyHeater;
            }
        })
            .map(function(propertyHeater) {
          var heater = {
            };
          heater.objectName = propertyHeater.OBJNAM;
          heater.sName = propertyHeater.SNAME;
          heater.subtype = propertyHeater.SUBTYP;
          heater.body = propertyHeater.BODY;
          if (heater.subtype === 'SOLAR' || heater.subtype === 'HTPMP' || heater.subtype === 'ULTRA' || heater.subtype === 'HCOMBO') {
              heater.startTempDelta = propertyHeater.START;

            }
          if(heater.subtype === 'SOLAR') {
            heater.stopTempDelta = propertyHeater.STOP;
            heater.nocturnalCooling = (propertyHeater.COOL === 'ON');
            }
          if((heater.subtype === 'ULTRA')) {
            heater.coolingEnabled = (propertyHeater.COOL === 'ON');
            heater.address = propertyHeater.COMUART;
            }

          if (heater.subtype === 'HCOMBO') {
              heater.coolingEnabled = 'OFF';
              heater.address = propertyHeater.COMUART;
              heater.economyTime = propertyHeater.TIME; // ECONOMY TIME
              heater.boostTemp = propertyHeater.BOOST;  // BOOST TIME
              heater.efficiencyMode = propertyHeater.HTMODE;  // EFFICIENCY MODE
            }

          if (heater.subtype === 'MASTER') { // Connected heater              
              heater.address = propertyHeater.COMUART;
              heater.cooldowndelay = propertyHeater.DLY; // Cool Down Delay
            }

          if (heater.subtype === 'MAX') { // Connected heater              
              heater.address = propertyHeater.COMUART;
              heater.cooldowndelay = propertyHeater.DLY; // Cool Down Delay
            }

          if (heater.subtype === 'GENERIC') { // gas heater              
              heater.cooldowndelay = propertyHeater.DLY; // Cool Down Delay
            }
              return heater;
        });
            // Then snapshot it to revert from an edit
            $scope.heatersBeforeEdit = angular.copy($scope.heaters);
    }

    function updateHeaterGroups() {
      $scope.heaterGroups = $scope.model.hardwareDefinition.map(function (hPanel) {
        return hPanel.params.OBJLIST
          .filter(function (objectItem) {
              // needed to only grab aux circuits here with the most recent API changes
            if (objectItem.params.SUBTYP && objectItem.params.SUBTYP === 'HCOMBO') {
              return objectItem;
          }
      });
    })[0];
        //NOTE: need to get STATIC since it is not in that call
        var objectList = $scope.heaterGroups.map(function(heaterGroup) {
          return {
                  objnam: heaterGroup.objnam,
              keys: ['STATIC']
        };
    });
    propertyService.RequestParamList(objectList).then(function (result) {
        // map it back to the original object
        $scope.heaterGroups.map(function(heaterGroup) {
          angular.forEach(result.objectList, function(objectItem) {
            if(heaterGroup.objnam === objectItem.objnam) {
              heaterGroup.params.STATIC = objectItem.params.STATIC;
          }
        });
    });
    });
    angular.forEach($scope.heaterGroups, function(heaterGroup) {
        // this one is for sorting
        $scope.sortableOptionsForHeaterGroups[heaterGroup.objnam]= {
              update: function (e, ui) {
                  // callback before changed model, don't need to do anything here.
        },
              stop: function (e, ui) {
            var currentElement = $(e.toElement);
            var attempts = 0;
                  // we try to grab parentsUntil class row to get the id
            while(!$(currentElement).hasClass("row")) {
              currentElement = $(currentElement).parent();
              if (attempts > 2) { //> 5) {       // retries only 2 times
                break;
            }
          }
                  // if we found the row proceed
            if($(currentElement).hasClass("row")) {
                //get the heater group's id
              var rowId = $(currentElement).attr('id');
              var heaterGroupId = parseInt(rowId.split('-')[1]);
              $scope.heaterGroups[heaterGroupId].params.OBJLIST = $scope.heaterGroups[heaterGroupId].params.OBJLIST.map(function (heater, idx) {
                heater.params.LISTORD = 1 +idx;
                return heater;
            });
          }
        }
    };


        if (heaterGroup.params.OBJLIST) {   // check if exist
              // ordering the listord
            heaterGroup.params.OBJLIST.sort(function (a, b) {
                var keyA = parseInt(a.params.LISTORD),
                  keyB = parseInt(b.params.LISTORD);
                if (keyA < keyB) return -1;
                if (keyA > keyB) return 1;
                return 0;
          });
    }
    });

    $scope.heaterGroupsBeforeEdit = angular.copy($scope.heaterGroups);
    }

    function updateCovers() {
      $scope.covers =[];
      propertyService.getCovers().then(function () {
        $scope.covers = propertyModel.covers;

        angular.forEach($scope.covers, function (cover) {
            cover.circuits =[];
      });
      $scope.coversBeforeEdit = angular.copy($scope.covers);
    });
    }

  }
]);
