﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pentair.Domain.Simulator.Entity
{
    public class Property
    {
        [Key]
        [MaxLength(128)]
        public string RemoteHost { get; set; }
        [MaxLength(64)]
        public string PropertyId { get; set; }
        [MaxLength(16)]
        public string ProtocolVersion { get; set; }
        [MaxLength(128)]
        public string Secret { get; set; }
        [MaxLength(128)]
        public string Name { get; set; }
    }
}
