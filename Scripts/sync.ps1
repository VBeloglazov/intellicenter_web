Select-AzureSubscription "Pentair - Visual Studio Professional with MSDN"

Import-Module "C:\Program Files (x86)\Microsoft SDKs\Windows Azure\PowerShell\Azure\Azure.psd1"
 
$publishingServer = (gc env:computername).toLower()
 
$serviceName = "PentairDev-03"
 
Get-AzureVM -ServiceName $serviceName | foreach { 
    if ($_.Name.toLower() -ne $publishingServer) {
       $target = $_.Name;
       $source = $publishingServer;
 
       $exe = "C:\Program Files\IIS\Microsoft Web Deploy V3\msdeploy.exe";
       [Array]$params = "-verb:sync", "-source:contentPath=C:\Inetpub\wwwroot,computerName=$source", "-dest:contentPath=C:\Inetpub\wwwroot,computerName=$target";
 
       & $exe $params;
    }   
}
