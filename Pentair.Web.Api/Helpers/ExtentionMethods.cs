﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;

namespace Pentair.Web.Api
{
    public static class ExtentionMethods
    {
        /// <summary>
        /// GetUserId from Owin if avalible.
        /// </summary>
        /// <param name="identity"></param>
        /// <returns></returns>
        public static Guid GetOwinUserId(this IIdentity identity)
        {
            return new Guid(identity.GetUserId() ?? identity.Name);
        }
    }

    public class MvcApplication : HttpApplication {
    protected void Application_EndRequest() {
        var context = new HttpContextWrapper(Context);
        // If we're an ajax request, and doing a 302, then we actually need to do a 401
        if (Context.Response.StatusCode == 302 && context.Request.IsAjaxRequest()) {
            Context.Response.Clear();
            Context.Response.StatusCode = 401;
        }
    }
}
}