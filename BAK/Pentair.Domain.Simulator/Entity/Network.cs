﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pentair.Domain.Simulator.Entity
{
    public class Network
    {
        [Key]
        public int ID { get; set; }
        public string Name { get; set; }
        public UInt32 LastRevision { get; set; }
        public string IpAddress { get; set; }
        public string SubnetMask { get; set; }
        public string DefaultGateway { get; set; }
        public string DnsServer { get; set; }
        public string SystemPassword { get; set; }
        public int Timeout { get; set; }
        public string WapNetwork { get; set; }
        public string WapPassword { get; set; }
        public int PanAddress { get; set; }

        // Foreign Keys
        public int TypeID { get; set; }
        public int IpAddressModeID { get; set; }
    }
}