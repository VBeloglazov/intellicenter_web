-- =============================================================
--
-- Author:		Vladimir Beloglazov
-- Create date: 01/29/2018
--
-- Creates:
--
--		Table Type:			None
--
--		Stored Procedure:	PClearParam
--
-- =============================================================

USE PathfinderIntelliCenter
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (
	SELECT * FROM sys.objects
	WHERE object_id = OBJECT_ID(N'dbo.PClearParam')
	AND type in (N'P', N'PC')
)
DROP PROCEDURE dbo.PClearParam
GO

-- -------------------------------------------------------------
--
-- Returns [TSubscription] containing subscriptions to be sent to THW for cancellation.
--
-- -------------------------------------------------------------
CREATE PROCEDURE dbo.PClearParam
	 @clientId		uniqueidentifier -- Which is actually ConnectionId
	,@propertyId	int

AS
BEGIN

	SET NOCOUNT OFF

	DECLARE @deleted TSubscription

	DELETE Subscriptions
	OUTPUT	 DELETED.ClientId   AS ClientId
			,DELETED.PropertyId AS PropertyId
			,DELETED.ObjNam		AS ObjNam
			,DELETED.ObjParm	AS ObjParm
	INTO @deleted
	FROM Subscriptions S
	WHERE ClientId = @clientId

	-- Result set
	SELECT * FROM @deleted D
	LEFT JOIN Subscriptions S
	ON (S.PropertyId = D.PropertyID) AND (S.ObjNam = D.ObjNam) AND (S.ObjParm = D.ObjParm)
	WHERE S.ClientId IS NULL
 
END
GO
