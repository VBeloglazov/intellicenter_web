﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Pentair.Web.Api
{
    // TODO: 2b deleted? Is sending notifications still actual task?

    /// <summary>
    /// TODO: VB this description doesn't match what this class actually does.
    ///       Update description or rename the class?
    /// 
    /// The Notification manager detects events in the
    /// system that users can elect to receive notifications for.
    /// </summary>
    public class NotificationManager
    {
        /// <summary>
        /// Constructor, set up an interval timer to check system events
        /// and check stale connections with a 5 minute resolution.
        /// </summary>
        public NotificationManager()
        {
        } // NotificationManager()

        // The checkNotifications event will determine which events to check for
        // and spin up worker threads to perform those tasks
        //private void checkNotifications(object sender, ElapsedEventArgs e)
        //{
        //    // Check for disconnected properties every time
        //    var disconnects = (from i in _data.Installations
        //        join n in _data.UserNotifications
        //            on i.InstallationId equals n.InstallationId
        //        where i.Connected == false && n.Duration.Hours > 0
        //        select new
        //        {
        //            Installation = i.InstallationId,
        //            Name = i.Name,
        //            User = n.UserId,
        //            Duration = n.Duration,
        //            LastUpdate = i.UpdateDateTime,
        //            LastNotification = n.NotificationSent
        //        }).ToList();

        //    foreach (var evt in disconnects)
        //    {
        //        if (evt.LastNotification < evt.LastUpdate &&
        //            evt.Duration < new TimeSpan(DateTime.UtcNow.Ticks - evt.LastUpdate.Ticks))
        //        {
        //            var notification = (from item in _data.UserNotifications
        //                where item.InstallationId == evt.Installation && item.UserId == evt.User
        //                select item).FirstOrDefault();
        //            if (null != notification)
        //            {
        //                notification.NotificationSent = evt.LastUpdate.Add(evt.Duration);
        //                _data.SaveChanges();
        //                sendDisconnectNotification(evt.User, evt.Name, evt.Duration.Hours);
        //            }
        //        }
        //    }

        //    // Limit notifications to 50 per minute.
        //    var throttle = 50;
        //    while (NotificationQueue.Notifications.Count > 0 && throttle-- > 0)
        //    {
        //        sendNotification(NotificationQueue.NextNotification());
        //    }

        //}

        // VB 2017-02-07 Not in use
        /*
        private void sendDisconnectNotification(Guid UserId, string name, int duration)
        {
            using (var identity = new ApplicationUserManager()
            {
                var emailAddress = identity.GetEmail(UserId.ToString());
                var email = new Email()
                {
                    To = emailAddress,
                    Subject = "Pentair Notification",
                    Message =
                        string.Format("{0}\n\nIt has come to our attention that your pool has been disconnected from the Internet for \n{1} hour(s).\n\n" +
                        "This is not an emergency, but it is something you should know.  We will not be able to connect to\n" +
                        "your pool until it is reconnected to your local wifi.\n\nRespectfully\n\nPentair Pools",
                        emailAddress, duration)
                };
                var server = new EmailApi();
                server.SendEmail(email);
            }
        }
        */

        //private void sendNotification(UserNotificationObject notification)
        //{
        //    var note = (from n in _data.NotificationMessages
        //        where n.MessageId == notification.StatusType
        //        select n.Notes).FirstOrDefault();

        //    var email = new Email()
        //    {
        //        Subject = "Notification",
        //        Message =
        //            string.Format("{0}\n\n{1}", notification.Message, note)
        //    };

        //    var server = new EmailApi();
        //    foreach (var recipient in notification.Recipients)
        //    {
        //        if (!recipient.EmailAddress1.IsNullOrWhiteSpace())
        //        {
        //            email.To = recipient.EmailAddress1;
        //            server.SendEmail(email);
        //        }
        //        if (!recipient.EmailAddress2.IsNullOrWhiteSpace())
        //        {
        //            email.To = recipient.EmailAddress2;
        //            server.SendEmail(email);
        //        }
        //    }
        //}

    } // class NotificationManager

    // VB 2017-02-07 Not in use
    /*
    // TODO Put this queue in SQL
    public static class NotificationQueue
    {
        public static List<UserNotificationObject> Notifications = new List<UserNotificationObject>();

        public static UserNotificationObject NextNotification()
        {
            var next = Notifications[Notifications.Count -1];
            Notifications.Remove(next);
            return next;
        }
    }

    public class UserNotificationObject
    {
        public readonly string StatusType;
        public readonly string Message;
        public readonly List<NotificationRecipient> Recipients;

        public UserNotificationObject(string t, string m, List<NotificationRecipient> r)
        {
            StatusType = t;
            Message = m;
            Recipients = r;
        }
    }
    */
}
