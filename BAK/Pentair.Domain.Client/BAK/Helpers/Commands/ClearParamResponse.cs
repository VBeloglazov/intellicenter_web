﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Pentair.Domain.Client.Helpers.Commands
{
    public class ClearParamResponse : ResponseCommand
    {
        public ClearParamResponse(
            string messageID,
            string response
            )
            : base(CommandString.ClearParam, messageID, response)
        {
        } // ClearParamResponse()

        /// <summary>
        /// Always returns an object, valid or not. If invalid, check the DeserializationError property value.
        /// </summary>
        public static new ClearParamResponse Deserialize(
            string serializedMessage)
        {
            ClearParamResponse command = null;

            try
            {
                command = JsonConvert.DeserializeObject<ClearParamResponse>(serializedMessage);
                command.IsValid = true;
            }
            catch (Exception e)
            {
                // Deserialization as ClearParamResponse failed

                StringBuilder sbError = new StringBuilder();

                sbError.Append("Cannot deserialize as ClearParamResponse. " + e.Message);

                // Try to get at least the base message data
                ResponseCommand baseCommand = ResponseCommand.Deserialize(serializedMessage);

                if (!baseCommand.IsValid)
                {
                    sbError.Append("\n" + baseCommand.DeserializationError);
                }

                command = new ClearParamResponse(
                    messageID: baseCommand.MessageID,
                    response: ResponseString.BadRequest)
                {
                    IsValid = false,
                    DeserializationError = sbError.ToString()
                };
            }

            return command;

        } // Deserialize()

    } // class ClearParamResponse
}
