'use strict';
var PWC = PWC || {};

PWC.controller('IndexController', ['$rootScope', '$scope', '$location', '$window', 'AuthService', 'SocketService',
    'PropertyService', 'AppService', 'PropertyModel', '$q', '$filter',
    function ($rootScope, $scope, $location, $window, authService, socketService, propertyService, appService, propertyModel,
              $q, $filter) {
        $rootScope.editMode = {};
        $rootScope.forms = {};
        $rootScope.configurationIssue = false;
        $scope.Property = propertyModel;
        $scope.selectedInstallationName = null;
        $scope.Authentication = authService.authentication;
        $scope.DisplayAccountMenu = false;
        $scope.IsLogin = true;
        $scope.bodyStyle = {};
        $scope.showNotificationList = false;
        $scope.Layout = "layout-login";
        $scope.spaRemotesQuestion = "Would you like to disable all Spa-Side Remotes?";
        $scope.spaRemotesClick = "Yes, disable all";
        $scope.alertFilter = 'ALL';
        $scope.model = propertyModel;
        $scope.remotes = propertyModel.remotes;
        $scope.sysPref = propertyModel.Objects._5451;
        $scope.allremotes = [];
        $scope.securityEnabled = propertyModel.SecurityEnabled;
        $scope.SHOMNU = propertyModel.ACL.SHOMNU;
        $scope.ACL = propertyModel.ACL;
        $scope.displayFwInProgress = false;
        $scope.appService = appService;
        $scope.webApiVersion = "WEB-API Version";



        var bodyEl = angular.element($window.document);
        $scope.targetVersion = "NA";
        $scope.newFWVersion = "NO";    //= "READY" when new firware is ready
        $scope.firmwareUpdateInProgress = false;

        $scope.Menus = propertyModel.systemMenu;
        $scope.remotesAreEnabled = true;                //***

        $scope.passwordInputType = 'password';               

        $scope.changeUsernameModal = false;
        $scope.setUsernameModal = true;
        $scope.confirmUsernameModal = false;
        $scope.changePasswordModal = false;



        $scope.getDeviceWidth = function () {   // return the device width
            var size = 1280;    // default width size
            if (window) {
                size = window.innerWidth;
            }
            //console.log("device width is : ", size);
            return size
        }

        $scope.getDeviceHeight = function () {   // return the device heigth
            var size = 720;    // default height size
            if (window) {
                size = window.innerHeight;
            }
            //console.log("device height is : ", size);
            return size
        }

        $scope.showChangeUsernameModal = function () {
            $scope.DisplayAccountMenu = false;
            $scope.changeUsernameModal = true;
            $scope.setUsernameModal = true;
            $scope.confirmUsernameModal = false;
        }
        $scope.showPasswordChangeModal = function () {
            $scope.DisplayAccountMenu = false;
            $scope.changePasswordModal = true;
        }
        $scope.closeChangeUsernameModal = function () {
            $scope.changeUsernameModal = false;
            $scope.setUsernameModal = true;
            $scope.confirmUsernameModal = false;


        }
        $scope.closePasswordChangeModal = function () {
            $scope.changePasswordModal = false;
        }

        $scope.togglePasswordReveal = function () {
            $scope.passwordInputType = ($scope.passwordInputType === 'password') ? 'text' : 'password';
        }


        $scope.checkSizePassword = function (item) {
            if (item.length >= 6)
                return true;
            else
                return false;
        }

        $scope.subMenuAvailable = function (menu) {
            if (menu.submenu)
                return "caret"
            else
                return "";
        }

        $scope.checkUppercasePassword = function (item) {
            var status = false;
            if (item.length <= 0)
                return status;

            for (var i = 0; i < item.length; i++) {
                if (item[i] >= 'A' && item[i] <= 'Z')
                    status = true;
            }
            return status;
        }

        $scope.checkMatchPassword = function (item1, item2) {
            if ((item1 === item2) && (item1.length >= 6))
                return true;
            else
                return false;
        }

        processRemoteActionsMessages();


        // init class
        (function () {
            console.log("*******************************----------    Init Index Controller   ---------*********************")
            //loadingRemotes();
            //$scope.retrievegetWebAPIVersion();

        }());

        $rootScope.$on("$routeChangeSuccess", function () {
            setSelectedInstallationName();
        });

        $scope.handleClickOutsideNotificationsDDL = function (event) {
            if ($scope.showNotificationList) {
                $scope.showNotificationList = false;
                $scope.$apply();
            }
        };

        // :true   is the user is a demo username
        $scope.isDemoUser = function (username) {
            // the demove username is fix !
            if (username === 'icdemop@gmail.com')
                return true
            else
                return false;
        };

        // init all values to default after reconnect 
        $scope.InitSetDefaults = function () {
            $scope.displayFwInProgress = false;
            $scope.Property.firmwareUpdateStatus = "";
            $scope.newFWVersion = "NO";
            $scope.firmwareUpdateInProgress = false;

        }


        $scope.isInServiceMode = function () {
            var status = false
            if ($scope.Property.serviceStatus) {
                switch ($scope.Property.serviceStatus) {
                    case 'MANUAL':      // is in service mode please logout and reconnect later
                    case 'TIMOUT':     // is in service mode please logout and reconnect later
                        status = true;
                        break;

                    case 'AUTO':        // normal operation
                    default:
                        status = false;
                        break;
                }
            }

            return status;
        }

        $scope.getEyeIconPassword = function () {

            var icon = ($scope.passwordInputType === 'password') ? 'icon-theeye-off' : 'icon-theeye';

            return icon;
        }

        $scope.messageService = function () {
            var msg = ""
            if ($scope.Property.serviceStatus) {
                switch ($scope.Property.serviceStatus) {
                    case 'MANUAL':      // is in service mode please logout and reconnect later
                        msg = "IS IN SERVICE MODE";
                        break;

                    case 'TIMOUT':     // is in service mode please logout and reconnect later
                        msg = "IS IN SERVICE MODE";
                        break;

                    case 'AUTO':        // normal operation
                    default:
                        msg = "IS IN AUTO MODE";
                        break;
                }
            }

            return msg;
        }


        $scope.isFirmwareUpdateInProgress = function () {
            return $scope.firmwareUpdateInProgress;
        }

        //---------------- NEW FIRMWARE AVAIALBLE

        $scope.isNewUpdateAvailable = function () {
            var status = false
            if ($scope.Property.firmwareUpdateStatus) {
                switch ($scope.Property.firmwareUpdateStatus) {
                    case 'YES':     // is in service mode please logout and reconnect later
                        status = true;
                        break;

                    case 'NO':        // normal operation                    
                        status = false;
                        $scope.firmwareUpdateInProgress = false;
                        break;
                }
            }


            return status;
        }


        $scope.LogOutService = function () {
            $scope.InitSetDefaults();
            $scope.Property.serviceStatus = "";
            $scope.LogOut();
        }

        // logout firmware update

        $scope.LogOutFirmwareUpdate = function () {
            $scope.Property.firmwareUpdateStatus = "";  // removing the dialog
            $scope.LogOut();
        }

        $scope.handleClickOutsideAccountMenu = function (event) {
            $scope.DisplayAccountMenu = true;
            $scope.ToggleAccountMenu();
            $scope.$apply();
        };

        $scope.splitAlarmMessage = function (message, index) {
            var msg = message
            var splited = message.split(":")
            // get message splited
            if (splited[index])
                msg = splited[index];

            return msg;

        };

        $scope.$on('UpdateService', function (message) {
            if (message.objnam === "_5451") {
                if (message.params) {
                    if (message.params.SERVICE) {
                        console.log("-------------------------------------- SERVICE VALUE :", message.params.SERVICE);
                        $scope.sysPref.SERVICE = message.params.SERVICE;
                    }
                }
            }
        });

        $scope.$on('UpdateFirmwareIn', function (event, message) {
            if (message.objnam === "_5451") {
                if (message.params) {
                    if (message.params.IN) {
                        //console.log("-------------------------------------- FIRMWARE UPDATE REMOTE COMMAND:", message.params.IN);
                        $scope.installFirmwareRemote(message.params.IN);   // EXECUTE MSG
                    }
                }
            }
        });


        $scope.$on('RemotesUpdatedBroadcast', function (message) {

            $scope.remotesAreEnabled = false
            angular.forEach($scope.Property.RemoteList, function (remote) {
                if (remote.params && remote.params.ENABLE === 'ON') {
                    $scope.remotesAreEnabled = true;
                }
            });

            if ($scope.remotesAreEnabled === true)  // if at least 1 is on
                $scope.Property.systemMenu.spaSideRemotes.heading = "DISABLE SPA-SIDE REMOTES";
            else
                $scope.Property.systemMenu.spaSideRemotes.heading = "ENABLE SPA-SIDE REMOTES";

            console.log("=====================  Remotes updated by broadcast: ", $scope.Property.systemMenu.spaSideRemotes.heading)

            processRemoteActionsMessages();
        });


        $scope.$watch('Property.Remotes', function () {
            // get the all on or off state for spa side remotes
            // if any of them are on then set it to true
            var remotesOn = $scope.Property.Remotes.filter(function (remote) {
                return remote.ENABLE === 'ON';
            });
            // toggle the on / off states here and close the modal if its open
            $scope.remotesAreEnabled = (remotesOn.length > 0);

            //$scope.Property.systemMenu.spaSideRemotes.disabled = $scope.remotesAreEnabled;

            if ($scope.remotesAreEnabled === true)
                $scope.Property.systemMenu.spaSideRemotes.heading = "DISABLE SPA-SIDE REMOTES";
            else
                $scope.Property.systemMenu.spaSideRemotes.heading = "ENABLE SPA-SIDE REMOTES";


            processRemoteActionsMessages();
        }, true);


        $scope.LogOut = function () {
            if ($location.path() === '/configurations') {
                var activeEdit = false;
                angular.forEach($rootScope.editMode[$rootScope.selectedTab], function (sectionEdit) {
                    if (sectionEdit) {
                        activeEdit = true;
                    }
                });
                if (activeEdit) {
                    // open the modal
                    $scope.bodyStyle.overflow = 'hidden';
                    $scope.cancelNavigateModal = true;
                    $scope.navigateTo = 'login';
                    return;
                } else {
                    // continue
                }
            }
            $scope.logoutDirectly();
        };

        $scope.logoutDirectly = function () {
            socketService.closeSocket();
            authService.logOut();
            propertyService.Reset(true);
            $scope.ToggleAccountMenu();
            $scope.InitSetDefaults();
        };

        $scope.proceedNavigate = function () {
            if ($scope.navigateTo === 'login') {
                $scope.cancelNavigateModal = false;
                $scope.bodyStyle.overflow = 'none';
                $scope.logoutDirectly();
            } else {
                $scope.cancelNavigateModal = false;
                $scope.bodyStyle.overflow = 'none';
                $location.path($scope.navigateTo);
            }
        };

        $scope.cancelNavigate = function () {
            $scope.cancelNavigateModal = false;
            $scope.bodyStyle.overflow = 'none';
        };

        $scope.Navigate = function (page) {
            if (page.location) {
                if ($location.path() === '/configurations') {
                    var activeEdit = false;
                    angular.forEach($rootScope.editMode[$rootScope.selectedTab], function (sectionEdit) {
                        if (sectionEdit) {
                            activeEdit = true;
                        }
                    });
                    if (activeEdit) {
                        // open the modal
                        $scope.bodyStyle.overflow = 'hidden';
                        $scope.cancelNavigateModal = true;
                        $scope.navigateTo = page.location;
                        return;
                    } else {
                        // continue
                    }
                }
                $location.path(page.location);
            } else {
                if (page.action === 'disableSpaSideRemotes') {
                    $scope.bodyStyle.overflow = 'hidden';
                    $scope.spaSideRemotesModal = true;
                }


                if (page.action === 'displayAboutVersion') {
                    $scope.displayAboutVersionModal = true;
                }

                if (page.action === 'displayReportIssue') {
                    $scope.displayReportIssueModal = true;
                }


                if (page.action === 'openUsersGuide') {
                    //var localwin = window.open('http://pentairpool.com/IntelliCenterManual', '_blank');
                    console.log("***********                  open Users Guide")
                    var localwin = window.open('https://www.pentair.com/content/dam/extranet/product-related/product-manuals/pool-and-spa-equipment/intellicenter/IntelliCenter_Quick_Ref_Guide.pdf', '_blank');
                    if (localwin) {
                        localwin.focus();
                    } else {
                        alert("IntelliCenter : User's Guide is not available");
                    }
                }
                // new items
                if (page.action === 'openQuickReferenceGuide') {
                    console.log("***********                  open Quick Reference Guide")
                    var localwin = window.open('https://www.pentair.com/content/dam/extranet/product-related/product-manuals/pool-and-spa-equipment/intellicenter/IntelliCenter_Quick_Ref_Guide.pdf', '_blank');
                    if (localwin) {
                        localwin.focus();
                    } else {
                        alert("IntelliCenter : Quick Reference Guide is not available");
                    }
                }

                if (page.action === 'openInstallationGuide') {
                    console.log("***********                  open Installation Guide")
                    var localwin = window.open('https://www.pentair.com/content/dam/extranet/product-related/product-manuals/pool-and-spa-equipment/intellicenter/IntelliCenter_Install_Guide.pdf', '_blank');
                    if (localwin) {
                        localwin.focus();
                    } else {
                        alert("IntelliCenter : Installation Guide is not available");
                    }
                }

                if (page.action === 'openUsersGuide') {
                    console.log("***********                  open Users Guide")
                    var localwin = window.open('https://www.pentair.com/content/dam/extranet/product-related/product-manuals/pool-and-spa-equipment/intellicenter/IntelliCenter_Users_Guide.pdf', '_blank');
                    if (localwin) {
                        localwin.focus();
                    } else {
                        alert("IntelliCenter : User's Guide is not available");
                    }
                }

                if (page.action === 'openWirelessRemote') {
                    console.log("***********                  open Wireless Remote")
                    var localwin = window.open('https://www.pentair.com/content/dam/extranet/product-related/product-manuals/pool-and-spa-equipment/intellicenter/intellicenter-wireless-remote-guide-english.pdf', '_blank');
                    if (localwin) {
                        localwin.focus();
                    } else {
                        alert("IntelliCenter : Wireless Remote is not available");
                    }
                }

                if (page.action === 'openIndoorControlPanelInstallationGuide') {
                    console.log("***********                  open Indoor Control Panel Installation Guide")
                    var localwin = window.open('https://www.pentair.com/content/dam/extranet/product-related/product-manuals/pool-and-spa-equipment/intellicenter/intellicenter_indoor_control_panel.pdf', '_blank');
                    if (localwin) {
                        localwin.focus();
                    } else {
                        alert("IntelliCenter : Indoor Control Panel Installation Guide is not available");
                    }
                }

                if (page.action === 'openPersonalityandExpansionCardInstallationGuide') {
                    var localwin = window.open('https://www.pentair.com/content/dam/extranet/aquatics/residential-pool/owners-manuals/automation/intellicenter-expansion-card-installation-guide.pdf', '_blank');
                    if (localwin) {
                        localwin.focus();
                    } else {
                        alert("IntelliCenter : Personality and Expansion Card Installation Guide is not available");
                    }
                }

                if (page.action === 'openRemoteAccessSetupInstructions') {
                    var localwin = window.open('https://www.pentair.com/content/dam/extranet/product-related/product-manuals/pool-and-spa-equipment/intellicenter/IntelliCenter%20Remote%20Access%20Setup.pdf', '_blank');
                    if (localwin) {
                        localwin.focus();
                    } else {
                        alert("IntelliCenter : Personality and Expansion Card Installation Guide is not available");
                    }
                }


                if (page.action === 'openTemperatureSensorInstallGuide') {
                    var localwin = window.open('https://www.pentair.com/content/dam/extranet/aquatics/residential-pool/owners-manuals/automation/intellicenter/temperature-sensor-install-guide-intellicenter-easytouch-intellitouch-suntouch-and-compool-english.pdf', '_blank');
                    if (localwin) {
                        localwin.focus();
                    } else {
                        alert("IntelliCenter : Personality and Expansion Card Installation Guide is not available");
                    }
                }

            }
        };

        $scope.anyRemotesAreEnabled = function (enabled) {
            if ($scope.remotesAreEnabled === enabled)
                return true;
            else
                return false;
        };

        $scope.toggleSpaSideRemotes = function () {
            console.log("toggleSpaSideRemotes side remotes");
            // iterate through each of our remote objects
            angular.forEach($scope.Property.Remotes, function (remote) {
                var params = { ENABLE: 'ON' };
                if ($scope.remotesAreEnabled === true) {
                    if (remote.ENABLE === 'ON') {
                        params.ENABLE = 'OFF';
                    }
                }
                propertyService.SetParams(remote.OBJNAM, params).then(function () {
                    $scope.bodyStyle.overflow = 'none';
                });
            });
            $scope.spaSideRemotesModal = false;
        };

        function loadingRemotes() {
            console.log("---init loading remotes --- ");
            propertyService.getRemotes().then(function (remotes) {
                $scope.allremotes = remotes.answer.map(function (item) { return item; });
                angular.forEach($scope.allremotes, function (remote) {
                    console.log("---display remote : " + remote);
                });
            });
        }

        function enableAllRemotes(value) {
            propertyService.getRemotes().then(function (remotes) {
                $scope.allremotes = remotes.answer.map(function (item) { return item; });
                angular.forEach($scope.allremotes, function (remote) {
                    var params = { ENABLE: value };
                    console.log("---set remote : " + remote.objnam + " ENABLE: " + value);
                    propertyService.SetParams(remote.objnam, params);
                });
            });
        }

        $scope.toggleTheSideRemotes = function () {
            $scope.spaSideRemotesModal = false;
            console.log("-----  toggle side remotes  ------");
            if ($scope.remotesAreEnabled === true) {
                $scope.disableSpaSideRemotes();
            }
            else {
                $scope.enableSpaSideRemotes();
            }

            processRemoteActionsMessages();
        };



        $scope.enableSpaSideRemotes = function () {
            // iterate through each of our remote objects
            console.log("--- Sending commands to enableSpaSideRemotes ");
            enableAllRemotes('ON');
            $scope.Property.systemMenu.spaSideRemotes.heading = "DISABLE SPA-SIDE REMOTES";
            $scope.enablespaSideRemotesModal = false;
            $scope.remotesAreEnabled = true;        // enable now                
        };

        $scope.disableSpaSideRemotes = function () {
            console.log("--- Sending commands to disableSpaSideRemotes ");
            enableAllRemotes('OFF');
            $scope.Property.systemMenu.spaSideRemotes.heading = "ENABLE SPA-SIDE REMOTES";
            $scope.spaSideRemotesModal = false;
            $scope.remotesAreEnabled = false;        // Disable now                
        };

        $scope.displayAbout = function () {

            //console.log("--- displayAbout ");            
            $scope.displayAboutVersionModal = false;
        };

        $scope.closeReportIssue = function () {
            $scope.displayReportIssueModal = false;
        };

        // firmware in progress
        $scope.setFwInProgress = function (status) {
            $scope.displayFwInProgress = status;
        };

        $scope.closeFwInProgress = function () {
            $scope.displayFwInProgress = false;
        };


        $scope.isFwInProgress = function () {
            if ($scope.displayFwInProgress === true)
                return true;
            else
                return false;
        }

        $scope.isPresentIChlorBody = function (body) {
            if (body.CHEMISTRY.ICHLOR != undefined)
                return true;
            else
                return false;
        };

        $scope.getBoardType = function () {
            var panelType = "SINGLE";

            if (propertyModel.PANID) {
                panelType = propertyModel.PANID;
            }
            switch (panelType) {
                case 'SHARE':
                    panelType = "SHARED BODY";
                    break;

                case 'NONE':
                    panelType = "SINGLE BODY";
                    break;

                default:
                    panelType = panelType + " BODY";
                    break;
            }

            return panelType;
        };


        $scope.getWebClientVersion = function () {

            var version = "1.048 b8";           // software version
            var day = " Jun 9, 2020 ";     // day released           
            version = version + day;
            return version;
        };

        $scope.getWebAPIVersion = function () {
            var version = $scope.Property.webApiVersion;            
            return version;
        };

        $scope.getTargetVersion = function () {
            if (propertyModel.Objects._5451) {
                //console.log("--- target version: " + propertyModel.Objects._5451.VER);
                $scope.targetVersion = propertyModel.Objects._5451.VER;
            }
            return $scope.targetVersion;
        };

        $scope.isNewFWVersionReady = function () {
            if (propertyModel.Objects._5451) {
                if (propertyModel.Objects._5451.UPDATE) {    // check if update exist
                    //console.log("--- status new firmware version: " + propertyModel.Objects._5451.UPDATE);
                    if (propertyModel.Objects._5451.UPDATE === "READY") {
                        $scope.newFWVersion = propertyModel.Objects._5451.UPDATE
                        return true;
                    }
                }
            }
            return false;
        };


        $scope.installFirmwareRemote = function (token) {
            // console.log("--- Rx remote command to install firmware : " + token);
            switch (token) {
                case "YES":
                    // console.log("--- installing firmware, remote click : " + token);
                    propertyModel.Objects._5451.UPDATE = "YES";
                    $scope.firmwareUpdateInProgress = true;
                    setTimeout(function () { $scope.LogOutFirmwareUpdate(); }, 10000);
                    break;
                case "NO":
                    // console.log("--- installing firmware, remote click : " + token);
                    propertyModel.Objects._5451.UPDATE = "NO";
                    $scope.firmwareUpdateInProgress = false;
                    $scope.Property.firmwareUpdateStatus = "NO";
                    break;
                case "IN":
                    //  console.log("--- installing firmware, remote init status : " + token);
                    break;
            }
        };

        // firmware distribution is in progress
        $scope.firmwareIsInProgress = function () {
            // check if exist
            if ($scope.Property.firmwareUpdateStatus) {
                if ($scope.Property.firmwareUpdateStatus == "PROGRES")
                    return true;
                else
                    return false;
            } else {
                return false;
            }

        }

        $scope.installFirmware = function (token) {
            if (propertyModel.Objects._5451)
                if (propertyModel.Objects._5451.UPDATE) {    // check if update exist
                    // console.log("--- Selected firmware Update Operation : " + token);
                    switch (token) {
                        case "YES":
                            propertyModel.Objects._5451.UPDATE = "YES";
                            $scope.firmwareUpdateInProgress = true;
                            $scope.txUpdateFirmwareSelection('_5451', 'YES');
                            // send command
                            setTimeout(function () { $scope.LogOutFirmwareUpdate(); }, 10000);
                            break;
                        case "NO":
                            propertyModel.Objects._5451.UPDATE = "NO";
                            $scope.firmwareUpdateInProgress = false;
                            $scope.Property.firmwareUpdateStatus = "NO";
                            // send command
                            $scope.txUpdateFirmwareSelection('_5451', 'NO');
                            break;
                    }
                }
        };


        $scope.txUpdateFirmwareSelection = function (objNam, value) {
            propertyService.SetParams(objNam, { IN: value });
        };

        $scope.getNewFwVersion = function () { //
            var version = "-";
            /*
            if (propertyModel.Objects._5451) {
                if (propertyModel.Objects._5451.NEWFWVER) {
                    console.log("--- new firmware version: " + propertyModel.Objects._5451.NEWFWVER);
                    version = propertyModel.Objects._5451.NEWFWVER;
                }
            }
            */
            if ($scope.Property.firmwareUpdateVersion) {
                version = $scope.Property.firmwareUpdateVersion;
            }
            return version;
        };



        $scope.ToggleAccountMenu = function () {
            $scope.DisplayAccountMenu = !$scope.DisplayAccountMenu;

            //if (!$scope.DisplayAccountMenu) {
            //    bodyEl.on('click', onBodyClick);
            //} else {
            //    bodyEl.off('click', onBodyClick);
            //}
        };

        $scope.toggleNotificationList = function () {
            $scope.showNotificationList = ($scope.showNotificationList) ? false : true;
        };

        $scope.takeScreenShot = function () {
            var canvas = document.getElementById("canvas");
            var image = canvas.toDataURL('image.jpg', 1.0);

            console.log("----------------------- THE IMAGE IS ---------------------------");
            console.log(image);
            /*
            var aLink = document.createElement('a');
            var env = document.createEvent("HTMLEvents");
            env.initEvent()
            */
        }

        $scope.takeScreenShot_back = function () {
            console.log("[BEGIN TAKING SCREENSHOT]");
            /*
            var e = jQuery.Event("keydown");
            e.which = 44;
            e.keys = 13;
            */
            var status = jQuery(this).trigger({
                type: 'keyup',
                which: 44,
                altKey: true,
            });


            jQuery(this).trigger({
                type: 'keypress',
                which: 44,
                altKey: true,
            });


            var kbEvent = document.createEvent("KeyboardEvent");
            var initMethod = typeof kbEvent.initKeyboardEvent !== 'undefined' ? "initKeyboardEvent" : "initKeyEvent";

            kbEvent[initMethod](
                               "keydown", // event type : keydown, keyup, keypress
                                true, // bubbles
                                true, // cancelable
                                window, // viewArg: should be window
                                false, // ctrlKeyArg
                                true, // altKeyArg
                                false, // shiftKeyArg
                                false, // metaKeyArg
                                44, // keyCodeArg : unsigned long the virtual key code , else 0
                                0 // charCodeArgs : unsigned long the Unicode character associated with the depressed key, else 0
            );
            document.dispatchEvent(kbEvent);

            console.log("[END TAKING SCREENSHOT] : ", status);

        };
        function onBodyClick(evt) {
            if (evt.target === angular.element(document.getElementsByClassName("accountMenuPanel"))) return;
            return true && $scope.ToggleAccountMenu();
        }

        function processRemoteActionsMessages() {
            if ($scope.remotesAreEnabled === true) {
                $scope.spaRemotesQuestion = "Would you like to disable all Spa-Side Remotes?";
                $scope.spaRemotesClick = "Yes, disable all";
            }
            else {
                $scope.spaRemotesQuestion = "Would you like to enable all Spa-Side Remotes?";
                $scope.spaRemotesClick = "Yes, enable all";
            }
        }


        $scope.GoTo = function (path) {
            if (propertyModel.Ready) {
                $scope.ToggleAccountMenu();
                if ($location.path() === '/configurations') {
                    var activeEdit = false;
                    angular.forEach($rootScope.editMode[$rootScope.selectedTab], function (sectionEdit) {
                        if (sectionEdit) {
                            activeEdit = true;
                        }
                    });
                    if (activeEdit) {
                        // open the modal
                        $scope.bodyStyle.overflow = 'hidden';
                        $scope.cancelNavigateModal = true;
                        $scope.navigateTo = path;
                        return;
                    } else {
                        // continue
                    }
                }
                $scope.GoToDirectly(path);
            }
        };

        $scope.GoToDirectly = function (path) {
            $location.path(path);
        };

        $scope.GoToChangeInstallationsGrid = function () {
            $location.path("/installationGrid");
        };


        /*********** SECURITY MODE July-18-2018 ********
        VacationMode		,  v        done 
        Support  		    ,  o        NA    
        GeneralSettings		,  g        done  , under testing
        AlertsNotification	,  q        done
        UserPortal		    ,  i        ONLY  ADMIN : THW
        Groups			    ,  k        done 
        Remotes			    ,  w        Removed     *** this option has been removed July-2018 : replace 'w' to 'a' option as per steve recomendation
        AdvancedSettings	,  a        done   , under testing
        Chemistry		    ,  c        done
        Status			    ,  t        done
        DeleteStatus		,  r        Removed    *** this option has been removed July-2018 : replace 'r' to 't' option as per steve recomendation
        Schedules		    ,  e        done
        Features		    ,  f        done
        Lights			    ,  l        done
        Pool			    ,  p        done
        PoolHeatSource		,  h        **
        PoolTemp		    ,  P        **
        Spa			        ,  m        done
        SpaHeatSource		,  n        **
        SpaTemp			    ,  S        **
        History			    ,  u        done
        MultiBodyDrawer		,  b        NA
        ServiceMode		    ,  x        done 
        SystemConfig		,  C        done 
        ********************************/


        $scope.isSecurityEnabled = function () {
            // checking
            if ($scope.ACL) {    // ckecking if ACL exist
                if ($scope.ACL.OBJNAM) {    // ckecking if objnam exist
                    if ($scope.ACL.OBJNAM === "U0000") {   // checking GUEST IS SIGNED
                        return true //security is enabled by GUEST user OR HAS NO RIGTHS ...
                    }
                }
            }

            // check if security is enabled on             
            if ($scope.Property && ($scope.Property.SecurityEnabled === true))
                return true
            else
                return false
        }
        $scope.showBellIcon = function (section) {
            if ($scope.selectedInstallationName === null)
                return false;

            var status = $scope.isAllowBySecurity('SECTION_ALERTS_NOTIFICATIONS')

            return status;
        }

        $scope.showCameraIcon = function () {
            var status = false;
            if ($scope.Property.camaraEnabled) {
                status = $scope.Property.camaraEnabled;
            }
            return status;
        }


        $scope.isAllowBySecurity = function (section) {

            var status = true

            // check security is not enable            
            if ($scope.isSecurityEnabled() === false) {
                return status
            }


            if (!propertyModel.ACL) {
                switch (section) {
                    case 'SECTION_STATUS':          // Status	:  t
                        if ($scope.SHOMNU.indexOf('t') >= 0)
                            status = true
                        else
                            status = false
                        break

                    case 'SECTION_DELETE_STATUS':   // DeleteStatus	: r has been replaced to 't'
                        if ($scope.SHOMNU.SHOMNU.indexOf('t') >= 0)
                            status = true
                        else
                            status = false
                        break

                    case 'SECTION_ALERTS_NOTIFICATIONS':   // AlertsNotification	,  q
                        if ($scope.SHOMNU.indexOf('q') >= 0)
                            status = true
                        else
                            status = false
                        break

                    default:
                        break

                }
            }

            return status
        }

        $scope.closeAlert = function (objNam) {
            propertyService.SetParams(objNam, { SHOMNU: 'OFF' });
        };

        $scope.isAlertClosed = function (objNam) {

            if (objNam.SHOMNU) {
                if (objNam.SHOMNU === 'OFF') {
                    return true;
                }
            }

            return false;
        };


        $scope.isAlertOn = function (objNam) {

            if (objNam.SHOMNU) {
                if (objNam.SHOMNU === 'ON') {
                    return true;
                }
            }

            return false;
        };

        $scope.closeAllAlerts = function () {
            // check if is allow by security

            if (!$scope.isAllowBySecurity('SECTION_DELETE_STATUS'))
                return  //

            var statuses = $filter('filterOutByObjectType')($scope.Property.Objects, 'STATUS');
            var objList = statuses.map(function (status) {
                return {
                    objnam: status.OBJNAM,
                    params: {
                        SHOMNU: 'OFF'
                    }
                }
            });
            propertyService.SetParamList(objList).then(function () { });
            propertyService.DismissAllAlerts(); // this doesn't work

            // retrieve all alarms now
            angular.forEach(objList, function (obj) {
                // request each object alarms
                propertyService.GetAlarmsParams(obj.objnam);
            });
        };

        /**
         * @return {string}
         */
        $scope.TimezoneIcon = function () {
            if ($rootScope.timeZone) {
                return "tz" + (Number($rootScope.timeZone) + 13);
            }
            return "";
        };

        $scope.filterAlertsList = function (type) {
            $scope.alertFilter = type;
        };

        $scope.isSecurityLocked = function (menuItem) {
            if (menuItem.heading === 'MANAGE USERS' && ($scope.model.SecurityEnabled && $scope.model.ACL.SHOMNU.indexOf('z') < 0)) {
                //console.log("Security is Locked ")
                return true;
            }

            //console.log("no SecurityLocked ")
            return false;

        };

        $scope.isOptionDisable = function (menuItem) {
            if ((menuItem.heading === 'MANAGE USERS') ||
                (menuItem.heading === 'ENABLE SPA-SIDE REMOTES') ||
                (menuItem.heading === 'DISABLE SPA-SIDE REMOTES')
            ) {
                if ($scope.model.ACL.SHOMNU) {
                    // SHOMNU Exist check if flag 'z' is present
                    if ($scope.model.ACL.SHOMNU.indexOf('z') < 0) {
                        //console.log("Security is Locked ")
                        return true;
                    }
                }
            }

            return false;
        };


        // process global broadcast notification
        $scope.$on('UpdateMinutes', function (event, message) {
            if (propertyModel.Objects) {    // check if objects in propertyModel exist!
                if (propertyModel.Objects._C10C) {// check if _C10C exist
                    $rootScope.timeZone = propertyModel.Objects._C10C.TIMZON;
                    $scope.currentDate = moment(propertyModel.Objects._C10C.DAY, 'MM,DD,YY').format('dddd, MMMM D, YYYY ');
                    if (propertyModel.Objects._C10C.CLK24A){
                        if (propertyModel.Objects._C10C.CLK24A === "HR24")
                            $scope.currentTime = moment(propertyModel.Objects._C10C.MIN, 'HH,mm,ss').format('HH:mm');   // 24 hours format
                        else
                            $scope.currentTime = moment(propertyModel.Objects._C10C.MIN, 'HH,mm,ss').format('h:mma');   // 12 hours format
                    }else{
                        $scope.currentTime = moment(propertyModel.Objects._C10C.MIN, 'HH,mm,ss').format('h:mma');       // 12 hours format
                    }
                }
            }
        });

        //
        //$scope.bodyOffset = function() {
        //    if(Property.Alerts)
        //    ((Property.Alerts | validAlerts).length > 0)? (Property.Alerts | validAlerts).length*10+50
        //}

        function setSelectedInstallationName() {
            $scope.selectedInstallationName = null;
            // even though it is still in local storage, don't show the installation they are on on installation page
            if ($scope.Property.Installations.length > 0 && '/installations' !== $location.path()) {
                var selectedInstallation = $scope.Property.Installations.filter(function (installation) {
                    if (installation.InstallationId === $scope.Property.SelectedInstallation) {
                        return installation;
                    }
                })[0];
                if (selectedInstallation !== undefined) { // meaning we matched an installation
                    $scope.selectedInstallationName = selectedInstallation.PoolName;
                    //$scope.currentTime = moment().utcOffset(parseInt(propertyModel.Objects._5451.TIMZON)).format('ddd, MMMM D h:mma');
                    propertyService.RequestParamList([{ objnam: '_C10C', keys: ['DAY', 'MIN', 'TIMZON'] }]).then(function (result) {
                        if (propertyModel.Objects._C10C === undefined) {
                            var newObject = {
                                DAY: result.objectList[0].params.DAY,
                                MIN: result.objectList[0].params.MIN,
                                TIMZON: result.objectList[0].params.TIMZON
                            }
                            propertyModel.Objects._C10C = newObject;
                        }
                        if (propertyModel.Objects._C10C) {
                            propertyModel.Objects._C10C.DAY = result.objectList[0].params.DAY;
                            propertyModel.Objects._C10C.MIN = result.objectList[0].params.MIN;
                            $rootScope.currentDate = moment(propertyModel.Objects._C10C.DAY, 'MM,DD,YY');
                            $rootScope.timeZone = result.objectList[0].params.TIMZON;
                            $scope.currentDate = moment(propertyModel.Objects._C10C.DAY, 'MM,DD,YY').format('dddd, MMMM D, YYYY ');
                            $scope.currentTime = moment(propertyModel.Objects._C10C.MIN, 'HH,mm,ss').format('h:mma'); // timezone is already accounted for.
                        }
                    });
                }
            }
        }

    }]);
