-- =============================================================
--
-- Author:		Vladimir Beloglazov
-- Create date: 01/29/2018
--
-- Creates:
--
--		Table Type:			TSubscription
--
--		Stored Procedure:	PReleaseParamList
--
-- =============================================================

USE PathfinderIntelliCenter
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (
	SELECT * FROM sys.objects
	WHERE object_id = OBJECT_ID(N'dbo.PReleaseParamList')
	AND type in (N'P', N'PC')
)
DROP PROCEDURE dbo.PReleaseParamList
GO

IF  EXISTS (
	SELECT * FROM sys.types st JOIN sys.schemas ss
	ON st.schema_id = ss.schema_id
	WHERE st.name = N'TSubscription' AND ss.name = N'dbo')
DROP TYPE dbo.TSubscription
GO

-- -------------------------------------------------------------
--
-- Must match class TSubscription.
-- Must match [Subscriptions] design.
--
-- -------------------------------------------------------------
CREATE TYPE dbo.TSubscription AS TABLE
(
	 ClientId				uniqueidentifier
	,PropertyId				int
	,ObjNam					nvarchar(128)
	,ObjParm				nvarchar(128)
)
GO

-- -------------------------------------------------------------
--
-- Returns [TSubscription] containing actually deleted subscriptions.
--
-- -------------------------------------------------------------
CREATE PROCEDURE dbo.PReleaseParamList
	@subscriptions AS TSubscription READONLY

AS
BEGIN

	SET NOCOUNT OFF

	DELETE Subscriptions
	OUTPUT	 DELETED.ClientId   AS ClientId
			,DELETED.PropertyId AS PropertyId
			,DELETED.ObjNam		AS ObjNam
			,DELETED.ObjParm	AS ObjParm
	FROM Subscriptions S
	INNER JOIN @subscriptions INP ON INP.ClientId = S.ClientId
	AND INP.PropertyId = S.PropertyId
	AND INP.ObjNam = S.ObjNam
	AND INP.ObjParm = S.ObjParm
	WHERE (S.ObjNam + S.ObjParm) IN
    (
        SELECT (S.ObjNam + S.ObjParm) FROM Subscriptions S
		INNER JOIN @subscriptions INP ON INP.ClientId = S.ClientId
		AND INP.PropertyId = S.PropertyId
		AND INP.ObjNam = S.ObjNam
		AND INP.ObjParm = S.ObjParm
        GROUP BY S.ObjNam, S.ObjParm
        HAVING COUNT(*) = 1
    )
 
END
GO
