Feature: Spa Control (PEN-229)
As a Pool Owner
I want to turn on or off my spa
So That I can enjoy my spa without having to turn on or off the whole system 

Background the user has completed logging in

Scenario: DISPLAY ON SPA STATUS
  When a Spa switch is displayed
  And the associated Spa is ON
  Then the Spa switch is set to ON
 
Scenario: DISPLAY OFF SPA STATUS
  When a Spa switch is displayed
  And the associated Spa is OFF
  Then the Spa switch is set to OFF

Scenario: TOGGLE SPA SWITCH
  When the user toggles a Spa switch
  Then the state of the on-screen switch is changed
  And the appropriate message is sent to the THW