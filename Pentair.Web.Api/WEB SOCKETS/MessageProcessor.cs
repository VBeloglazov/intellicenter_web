﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Results;
using System.Net;
using System.Data.Entity.Migrations;

using Microsoft.AspNet.Identity.EntityFramework;

using Newtonsoft.Json;

using SERVICE_SHARED;

namespace Pentair.Web.Api
{
    public static class MessageProcessor
    {
        private static string EVENT_SOURCE = "MessageProcessor";

        #region Public Methods

        /// <summary>
        /// Modify the users access level on the pool
        /// </summary>
        public static string EditUser(
            string message,
            int propertyId)
        {
            KeyValueCommand parms = JsonConvert.DeserializeObject<KeyValueCommand>(message);

            string email = parms.Parameters["EMAIL"];
            string level = parms.Parameters["PERMISSIONGROUP"];

            ApplicationUser user = UserFromUserEmail(email);

            Guid userId = (user == null || user.Id == null) ? Guid.Empty : new Guid(user.Id);

            UserManagementResult result;

            using (DbContextPentair db = DbContextPentair.New())
            {
                result = db.ChangeUserAccess(email, level, userId, propertyId);

            } // using (DbContextPentair db)

            StatusResponse response = null;

            if (result == UserManagementResult.Success)
            {
                response = new StatusResponse(
                    command: CommandString.ApiResponse,
                    messageID: parms.MessageID,
                    response: ResponseString.OK,
                    status: CommandString.SuccessMessage,
                    messageDetail: "Changed"
                );
            }
            else
            {
                response = new StatusResponse(
                    command: CommandString.ApiResponse,
                    messageID: parms.MessageID,
                    response: ResponseString.NotModified,
                    status: CommandString.ErrorMessage,
                    messageDetail: "Not changed"
                );
            }

            return JsonConvert.SerializeObject(response);

        } // EditUser()

        /// <summary>
        /// Delete the user specified in the message from the indicated pool
        /// </summary>
        /// <param name="message">User to be deleted</param>
        /// <param name="propertyId">pool</param>
        /// <returns></returns>
        public static string DeleteUser(
            string message,
            int propertyId,
            Guid userId,
            Guid connectionId)
        {
            KeyValueCommand userInfo = JsonConvert.DeserializeObject<KeyValueCommand>(message);

            string userEmail = userInfo.Parameters["EMAIL"];

            bool bNotifyUser;
            if (!System.Boolean.TryParse(userInfo.Parameters["NOTIFY"], out bNotifyUser))
            {
                StatusResponse responseError = new StatusResponse(
                    command: CommandString.ApiResponse,
                    messageID: userInfo.MessageID,
                    response: ResponseString.BadRequest,
                    status: CommandString.ErrorMessage,
                    messageDetail: userEmail
                );

                return JsonConvert.SerializeObject(responseError);
            }

            ApplicationUser user = UserFromUserEmail(userEmail);

            if (user != null)
            {
                using (DbContextPentair db = DbContextPentair.New())
                {
                    db.RemoveUserAssociation(
                        (user.Id != null) ? new Guid(user.Id) : Guid.Empty, // Accepted/Not Accepted
                        propertyId,
                        userEmail
                    );

                    /* TODO: VB Add return code to db.RemoveUserAssociation() and handle LastAdminError situation here.
                    if (result != UserManagementResult.Success)
                    {
                        string statusResponse = (result == UserManagementResult.LastAdminError)
                            ? CommandString.ErrorMessage
                            : CommandString.NotFoundMessage
                        ;

                        StatusResponse responseError = new StatusResponse(
                            command: CommandString.ApiResponse,
                            messageID: userInfo.MessageID,
                            response: ResponseString.Forbidden,
                            status: statusResponse,
                            messageDetail: userEmail
                        );

                        return JsonConvert.SerializeObject(responseError);
                    }
                    */

                } // using (DbContextPentair db)
            }

            Installation installation = null;
            using (DbContextPentair db = DbContextPentair.New())
            {
                installation = db.GetInstallation(propertyId);
            }

            if (bNotifyUser)
            {
                // 1.0.0.17
                try
                {
                    EmailApi_Smtp.SendEmail(
                        subject: "Account removed",
                        addressTo: userEmail,
                        isBodyHtml: false,
                        body:
                            String.Format("You have been removed from the list of users with access to {0} pool.",
                                (installation != null) ? installation.Name : "Unknown"
                            )
                    );
                }
                catch(Exception e)
                {
                    LogManager.LogErrorAsync(
                       source: EVENT_SOURCE,
                       eventBriefDescription: "Emailing user notification failed",
                       installationId: propertyId,
                       userId: userId,
                       connectionId: connectionId,
                       eventFullDescription:
                        String.Format("Recipient: {0}\n\nReject reason: \"{1}\"\n\nMessage:\n{2}",
                            userEmail,
                            e.Message,
                            message
                        )
                   );
                }
            }

            StatusResponse response = new StatusResponse(
                command: CommandString.ApiResponse,
                messageID: userInfo.MessageID,
                response: ResponseString.OK,
                status: CommandString.SuccessMessage,
                messageDetail: userEmail
            );

            return JsonConvert.SerializeObject(response);

        } // DeleteUser()

        /// <summary>
        /// Create a list of users that have been invited or 
        /// currently have remote access to my pool
        /// </summary>
        /// <param name="message">Request message</param>
        /// <param name="propertyId">Property in question</param>
        /// <returns></returns>
        public static string ListUsers(string message, int propertyId)
        {
            KeyValueCommand parms = JsonConvert.DeserializeObject<KeyValueCommand>(message);

            int page = 1;
            int pageSize = 0;
            var filter = "all";
            var sortField = "email";
            var searchTerm = "";
            var sort = "asc";
            if (null != parms.Parameters)
            {
                foreach (var item in parms.Parameters)
                {
                    switch (item.Key)
                    {
                        case "PAGE":
                            int.TryParse(item.Value, out page);
                            break;
                        case "PAGESIZE":
                            int.TryParse(item.Value, out pageSize);
                            break;
                        case "FILTER":
                            filter = item.Value;
                            break;
                        case "SORTFIELD":
                            sortField = item.Value;
                            break;
                        case "SORTDIRECTION":
                            sort = item.Value;
                            break;
                        case "SEARCHTERM":
                            searchTerm = item.Value;
                            break;
                    }
                }
            }

            SortDirection sortDirection = sort.ToLower().Contains("a")
                ? SortDirection.Ascending
                : SortDirection.Decending;

            List<AssociatedUser> userList;

            using (ApplicationUserManager identity = new ApplicationUserManager())
            {
                using (DbContextPentair db = DbContextPentair.New())
                {
                    int start = (pageSize > 0) ? page * pageSize : 0;
                    int lines = (pageSize > 0) ? pageSize : identity.Users.Count();

                    if (lines > 100)
                        lines = 100;

                    userList = 
                        db.GetUserAssociationList(
                            propertyId, sortField, sortDirection, searchTerm, filter, start, lines
                        )
                        .ToList();

                } // using (DbContextPentair db)

            } // using (ApplicationUserManager identity)

            int totalCount = userList.Where(u => u.AccessToken == StaticData.Admin).Count();

            List<ParamObject> users = new List<ParamObject>();

            foreach (AssociatedUser associatedUser in userList)
            {
                users.Add(
                    new ParamObject(
                        objNam: associatedUser.EmailAddress,
                        parameters: new Dictionary<string, string>()
                        {
                            {"id", associatedUser.UserId.ToString()},
                            {"name", associatedUser.UserName},
                            {"email", associatedUser.EmailAddress},
                            {"level", associatedUser.SecurityName},
                            {"security", associatedUser.AccessToken},
                            {"status", associatedUser.Status}
                        }
                    )
                );

            } // foreach(AssociatedUser associatedUser)

            UserListResponse response = new UserListResponse(
                messageID: parms.MessageID,
                response: ResponseString.OK,
                totalCount: totalCount.ToString(),
                objectList: users
            );

            return JsonConvert.SerializeObject(response);
        
        } // ListUsers()

        /// <summary>
        /// Resend an existing invite.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="propertyId"></param>
        /// <returns></returns>
        public static string ResendInvite(
            string message,
            int propertyId,
            Guid userId,
            Guid connectionId)
        {
            KeyValueCommand parms = JsonConvert.DeserializeObject<KeyValueCommand>(message);

            string statusResponse;
            string messageDetailResponse;

            if (parms.Parameters.ContainsKey("ID"))
            {
                using (DbContextPentair db = DbContextPentair.New())
                {
                    TUserInvitation invitationDB = db.UpdateUserInvitation(new Guid(parms.Parameters["ID"]));

                    if (invitationDB != null)
                    {
                        statusResponse = EmailInvite(
                            invitationDB.EmailAddress,
                            invitationDB.InvitationId,
                            propertyId,
                            userId,
                            connectionId
                        );
                        
                        messageDetailResponse = "Sent";
                    }
                    else
                    {
                        statusResponse = ResponseString.NotFound;
                        messageDetailResponse = "Invitation not found.";
                    }

                } // using (DbContextPentair db)
            }
            else
            {
                statusResponse = CommandString.ErrorMessage;
                messageDetailResponse = "Invitation ID is missing";
            }

            StatusResponse response = new StatusResponse(
                command: CommandString.SuccessMessage,
                messageID: parms.MessageID,
                response: ResponseString.OK,
                status: statusResponse,
                messageDetail: messageDetailResponse
            );

            return JsonConvert.SerializeObject(response);
        
        } // ResendInvite()

        /// <summary>
        /// Invite a new user to manage my pool.
        /// </summary>
        public static string InviteNewUser(
            string message,
            int propertyId,
            Guid userId,
            Guid connectionId)
        {
            KeyValueCommand parms = JsonConvert.DeserializeObject<KeyValueCommand>(message);

            string statusResponse;
            string messageDetailResponse;

            if (parms.Parameters.ContainsKey("EMAIL") && parms.Parameters.ContainsKey("PERMISSIONGROUP"))
            {
                string securityToken = parms.Parameters["PERMISSIONGROUP"];
                string emailAddress = parms.Parameters["EMAIL"];

                /* 2018-09-27 1.0.0.19 VB E-mail is always required now.
                ApplicationUser user = new ApplicationUser();

                //check for existing user 
                using (var userDb = new ApplicationUserContext())
                {
                    user = userDb.Users.FirstOrDefault(x => x.Email == emailAddress);
                }

                if (user != null)
                {
                    using (DbContextPentair db = DbContextPentair.New())
                    {
                        db.AddUpdateAssociation(new Guid(user.Id), propertyId, securityToken);
                    }

                    // 2018-04-06 VB We agreed not to send e-mail notification in this case,
                    //               user just will see a new pool in his list.
                    //               If later it will be required, it should be added here
                    //               and Client should handle it like it does with a new user
                    //               (asking user for confirmation).

                    statusResponse = CommandString.SuccessMessage;
                    messageDetailResponse = "This existing User now has access to your pool: " + emailAddress;
                }
                else
                {
                    UserInvitation invitationNew = new UserInvitation()
                    {
                        InvitationId = Guid.NewGuid(),
                        AccessToken = securityToken,
                        EmailAddress = emailAddress,
                        InstallationId = propertyId,
                        EmailId = ""
                    };

                    using (DbContextPentair db = DbContextPentair.New())
                    {
                        db.AddUserInvitation(invitationNew);
                    }

                    statusResponse = EmailInvite(
                        invitationNew.EmailAddress,
                        invitationNew.InvitationId,
                        propertyId,
                        userId,
                        connectionId
                    );

                    messageDetailResponse = "Sent";
                }
                */
                // 2018-09-27 1.0.0.19 VB
                UserInvitation invitationNew = new UserInvitation()
                {
                    InvitationId = Guid.NewGuid(),
                    AccessToken = securityToken,
                    EmailAddress = emailAddress,
                    InstallationId = propertyId,
                    EmailId = ""
                };

                using (DbContextPentair db = DbContextPentair.New())
                {
                    db.AddUserInvitation(invitationNew);
                }

                statusResponse = EmailInvite(
                    invitationNew.EmailAddress,
                    invitationNew.InvitationId,
                    propertyId,
                    userId,
                    connectionId
                );

                messageDetailResponse = "Sent";
            }
            else
            {
                statusResponse = CommandString.ErrorMessage;
                messageDetailResponse = "Invalid Email";
            }

            return
                JsonConvert.SerializeObject(
                    new StatusResponse(
                        command: CommandString.ApiResponse,
                        messageID: parms.MessageID,
                        response: ResponseString.OK,
                        status: statusResponse,
                        messageDetail: messageDetailResponse
                    )
                );
        
        } // InviteNewUser()

        /* 2b removed
        /// <summary>
        /// Return a list of user notifications
        /// </summary>
        /// <param name="message">original request</param>
        /// <param name="propertyId">property Id</param>
        /// <param name="userId">Users Id</param>
        /// <returns></returns>
        public static string UserNotificationList(
            string message,
            int propertyId,
            Guid userId)
        {
            var notifications = new List<ParamObject>();
            var parms = JsonConvert.DeserializeObject<CommandBase>(message);

            // Add the user supported notifications
            using (DbContextPentair db = DbContextPentair.New())
            {
                // TODO: Replace with raw SQL

                UserNotification notifyValueDB = db.GetUserNotification(
                    eventId: (int)EventTypeCodes.InstallationDisconnectEvent,
                    userId: userId,
                    installationId: propertyId
                );

                if (notifyValueDB != null)
                {
                    notifications.Add(
                        new ParamObject(
                            objNam: "disconnect",
                            parameters: new Dictionary<string, string>()
                        {
                            {"id", ((int) EventTypeCodes.InstallationDisconnectEvent).ToString()},
                            {"duration", (notifyValueDB != null) ? notifyValueDB.Duration.Hours.ToString() : "0"},
                            {"type", "email"}
                        }
                        )
                    );
                }

            } // using (DbContextPentair db)

            ParamCommandResponse response = new ParamCommandResponse(
                command: CommandString.UserNotificationList,
                messageID: parms.MessageID,
                response: ResponseString.OK,
                objectList: notifications
            );

            return JsonConvert.SerializeObject(response);

        } // UserNotificationList()

        /// <summary>
        /// Save the notification to the users set of notifications
        /// </summary>
        /// <param name="message">original request</param>
        /// <param name="propertyId">property Id</param>
        /// <param name="userId">Users Id</param>
        /// <returns></returns>
        public static string UserNotificationEdit(
            string message,
            int propertyId,
            Guid userId)
        {
            StatusResponse response = null;

            ParamCommandResponse parms = ParamCommandResponse.Deserialize(message);

            var notificationNew = new UserNotification()
            {
                InstallationId = propertyId,
                UserId = userId,
            };

            foreach (var e in parms.ObjectList)
            {
                var valid = false;
                switch (e.ObjNam)
                {
                    case "disconnect":
                        notificationNew.EventId = (int)EventTypeCodes.InstallationDisconnectEvent;
                        break;
                }
                foreach (var p in e.Parameters)
                {
                    switch (p.Key.ToLower())
                    {
                        case "id":
                            int eventId;
                            valid = int.TryParse(p.Value.ToString(), out eventId);
                            notificationNew.EventId = eventId;
                            break;
                        case "duration":
                            int duration;
                            valid = int.TryParse(p.Value.ToString(), out duration);
                            if (valid)
                            {
                                notificationNew.Duration = new TimeSpan(duration, 0, 0);
                            }
                            break;
                        case "type":
                            switch (p.Value.ToString().ToLower())
                            {
                                case "email":
                                    notificationNew.NotificationType = (int)NotificationType.Email;
                                    break;
                                case "sms":
                                    notificationNew.NotificationType = (int)NotificationType.SMS;
                                    break;
                            }
                            break;
                    }
                }

                if (valid)
                {
                    using (DbContextPentair db = DbContextPentair.New())
                    {
                        // TODO: 2b removed after testing
                        //UserNotification notificationDB = db.GetUserNotification(
                        //    eventId: notificationNew.EventId,
                        //    userId: notificationNew.UserId,
                        //    installationId: notificationNew.InstallationId
                        //);
                        //
                        //if (notificationDB != null)
                        //{
                        //    notificationDB.CopyFrom(notificationNew); // TODO: Test!!! // Deep copy
                        //}
                        //else
                        //{
                        //    db.UserNotifications.Add(notificationNew);
                        //}
                        //
                        //db.SaveChanges();

                        db.AddUpdateUserNotification(notificationNew);

                    } // using (DbContextPentair db)

                    response = new StatusResponse(
                        command: CommandString.ApiResponse,
                        messageID: parms.MessageID,
                        response: ResponseString.OK,
                        status: CommandString.SuccessMessage,
                        messageDetail: String.Empty
                    );
                }
                else
                {
                    response = new StatusResponse(
                        command: CommandString.ApiResponse,
                        messageID: parms.MessageID,
                        response: ResponseString.BadRequest,
                        status: CommandString.ErrorMessage,
                        messageDetail: "Invalid Notification"
                    );
                }
            
            } // foreach (var e in parms.ObjectList)

            return JsonConvert.SerializeObject(response);
        
        } // UserNotificationEdit()
        */

        /// <summary>
        /// Returns system info for the property.
        /// </summary>
        /// <param name="message">original request</param>
        /// <param name="propertyId">property Id</param>
        /// <param name="userId">Users Id</param>
        /// <returns></returns>
        public static string SystemInfoList(
            string message,
            int propertyId)
        {
            List<ParamObject> systemInfo = new List<ParamObject>();

            CommandBase requestBase = JsonConvert.DeserializeObject<CommandBase>(message);

            using (DbContextPentair db = DbContextPentair.New())
            {
                Installation installationDB = db.GetInstallation(propertyId);

                if (installationDB != null)
                {
                    systemInfo.Add(
                        new ParamObject(
                            objNam: propertyId.ToString(),
                            parameters:
                                new Dictionary<string, string>()
                                {
                                    {"INSTALLATION", propertyId.ToString()},
                                    //{"OWNER", sysInfo.OwnerName?? ""},
                                    //{"ADDRESS", sysInfo.Address ?? ""}
                                }
                        )
                    );
                }
            }

            ParamCommandResponse response = new ParamCommandResponse(
                command: CommandString.SystemInfoList,
                messageID: requestBase.MessageID,
                response: ResponseString.OK,
                objectList: systemInfo
            );

            return JsonConvert.SerializeObject(response);
        
        } // SystemInfoList()

        #endregion

        #region Private Methods

        private static string EmailInvite(
            string emailAddress,
            Guid invitationId,
            int propertyId,
            Guid userId,
            Guid connectionId)
        {
            string response = ResponseString.BadRequest;

            string inviteUrl = "";
            string host = "";
            HttpRequest request = HttpContext.Current.Request;

            //build inviteUrl & Email
            if (HttpContext.Current.Request.Url.Host.ToLower() == "localhost")
            {
                host = "http://localhost:44300";
            }
            else
            {
                inviteUrl = request.IsSecureConnection ? "https://" : "http://";
                host = request["HTTP_HOST"];
            }

            inviteUrl += host + "/#/invitation/";

            string message =
                String.Format("You have been invited to monitor and manage the pool system, visit {0}{1} to create your account and gain access." +
                            "\n\n If you already have a Pentair account but it does not use the e-mail address {2}, a link on the page will allow " +
                            "you to log in and connect to the pool using your account",
                    inviteUrl,
                    invitationId,
                    emailAddress
                );

            // 1.0.0.17
            try
            {
                EmailApi_Smtp.SendEmail(
                    subject: "You have been invited to control a Pentair IntelliCenter System",
                    addressTo: emailAddress,
                    isBodyHtml: false,
                    body: message
                );

                response = ResponseString.OK;
            }
            catch (Exception e)
            {
                LogManager.LogErrorAsync(
                   source: EVENT_SOURCE,
                   eventBriefDescription: "Emailing user invitation failed",
                   installationId: propertyId,
                   userId: userId,
                   connectionId: connectionId,
                   eventFullDescription:
                    String.Format("Recipient: {0}\n\nReject reason: \"{1}\"\n\nMessage:\n{2}",
                        emailAddress,
                        e.Message,
                        message
                    )
               );
            }

            return response;

        } // EmailInvite()

        private static ApplicationUser UserFromUserEmail(
            string email)
        {
            ApplicationUser user;

            using (ApplicationUserManager identity = new ApplicationUserManager())
            {
                user = new ApplicationUser
                {
                    Id = (from i in identity.Users
                          where i.Email == email
                          select i.Id
                         )
                         .FirstOrDefault()
                };

            } // using (ApplicationUserManager identity)
            
            return user;
        
        } // UserFromUserEmail()

        #endregion Private Methods
    }
}