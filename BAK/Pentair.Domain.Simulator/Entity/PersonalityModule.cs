﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pentair.Domain.Simulator.Entity
{
    public class PersonalityModule
    {
        [Key]
        public int ID { get; set; }
        public string Name { get; set; }
        public UInt32 LastRevision { get; set; }
        public int ListPosition { get; set; }
        public int Channel { get; set; }
        public float Version { get; set; }
        public string SerialNumber { get; set; }
        public DateTime ManufactureDate { get; set; }
        public string Configuration { get; set; }
        public DateTime FirstDetect { get; set; }
        public DateTime LastDetect { get; set; }

        // Foreign Keys
        public int TypeID { get; set; }
        public int ModuleSubTypeID { get; set; }
        public int PanelID { get; set; }
    }
}