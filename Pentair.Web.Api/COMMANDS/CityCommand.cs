﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Pentair.Web.Api
{
    public class CityCommand : CommandBase
    {
        [JsonProperty("city")]
        public string City { get; private set; }

        [JsonProperty("state")]
        public string State { get; private set; }

        public CityCommand(
            string command,
            string messageID,
            string city,
            string state
            )
            : base(CommandString.ReadCityNameInfoMessage, messageID)
        {
            this.City = city;
            this.State = state;
        }

    } // class CityCommand
}
