# Pentair THW API Specification v2
*Note: the API specification uses "propertyID" to mean "entityID" for backwards-compatibility purposes.*

*All key names and string values shall be quoted.*

---

## Object Definitions

### Read Object

key | type | value
--- | ---- | -----
objname | string | UID of the object (Circuit, Pump, etc.)
keys | array | array  of param names (string)

### Set Object

key | type | value
--- | ---- | -----
objname | string | UID of the object (Circuit, Pump, etc.)
params | object | object list where key is parameter name and value is what parameter should be set to

### Entity object
key | type | value
--- | ---- | -----
propertyID | string | ID of the entity 
name | string | friendly name of the entity


---

## Connection

### Open WebSocket Request

Sent by the THW to open the WebSocket (details TBD).

key | type | value
--- | ---- | -----
propertyID | string | Unique ID of the property where the THW is located 
sharedSecret | string | a string representation of the secret shared between the THW and the API server
apiVersion | integer | the revision sequence of the API, counting from 1

```json
{
    "propertyID" : "0xB1050000", 
    "sharedSecret" : "0x0123456789ABCDEF",
	"apiVersion" : 2
}
```

---

## Server to THW

### GetParamList

The GetParamList command specifies an array of one or more Read Objects (as defined above). This is a one-time read of the parameter.

key | type | value
--- | ---- | -----
command	| string | "GetParamList"
messageID | string | a locally unique identifier for the message
objectList | array | array of Read objects to to be read with optional query string

#### Examples

Read a single param from a Circuit:

```json
{
    "command": "GetParamList",
	"messageID" : "0xDEADBEEF",
    "objectList": [
        {
            "objname": "ALL",
            "keys": [
                "STATUS ? OBJTYP=BODY&(SUBTYP=SPA|SUBTYP=BODY)"
            ]
        }
    ]
}
```

Read multiple params from a Circuit:

```json
{
    "command": "GetParamList",
	"messageID" : "0xDEADBEEF",
    "objectList": [
        {
            "objname": "C155",
            "query":,
            "keys": [
                "STATUS",
                "TIMOUT"
            ]
        }
    ]
}
```

Read params from multiple objects:

```json
{
    "command": "GetParamList",
	"messageID" : "0xDEADBEEF",
    "objectList": [
        {
            "objname": "ALL",
            "keys": [
                "OBJTYP",
                "STATUS",
                "TIMOUT",
                "OBJTYP"="CIRCUIT"
            ]
        },
        {
            "objname": "ALL",
            "keys": [
                "MODE",
                "CIRCUIT",
                "TEMP",
                "OBJTYP"="BODY"
            ]
        }
    ]
}
```


### SetParamList

The SetParamList command specifies an array of one or more Set Objects (as defined above).

key | type | value
--- | ---- | -----
command	| string | "SetParamList"
messageID | string | a locally unique identifier for the message
objectList | array | array of Set objects to be set

#### Examples

Set a single Circuit STATUS to ON:

```json
{
    "command": "SetParamList",
	"messageID" : "0xDEADBEEF",
    "objectList": [
		{
			"objname": "C155",
			"params": {
				"STATUS": "ON"
			}
		}
	]
}
```

Set multiple parameters on multiple Circuits:

```json
{
    "command": "SetParamList",
	"messageID" : "0xDEADBEEF",
	"objectList": [
		{
			"objname": "C155",
			"params": {
				"STATUS": "ON",
                "TIMOUT": "3600"
			}
		},
		{
			"objname": "C165",
			"params": {
				"ACT": "ROYAL"
			}
		}
	]
}
```

### RequestParamList

The RequestParamList command specifies an array of Read Objects (as defined above). This command subscribes the sender to change notifications.

key | type | value
--- | ---- | -----
command	| string | "RequestParamList"
messageID | string | a locally unique identifier for the message
objectList | array | array of Read objects to be subscribed to

#### Examples

Request notifications from multiple objects and params:

```json
{
    "command": "RequestParamList",
	"messageID" : "0xDEADBEEF",
	"objectList": [
		{
			"objnam": "C155",
			"keys": [
				"STATUS",
				"MODE"
			]
		},
		{
			"objnam": "C145",
			"keys": [
				"STATUS",
				"MODE",
				"TIMOUT"
			]
		}
	]
}
```

### ReadParam

The ReadParam command specifies the timeNow received in the last WriteParamList or NotifyList message that was received
when a dropped or missing message condition is detected.  It's purpose is to have all dropped or missing messages re-transmitted.

key | type | value
--- | ---- | -----
command	| string | "ReadParam"
messageID | string | a locally unique identifier for the message
timeSince | string | the value sent by the last known good message for timeNow

#### Examples

Request all NotifyList and WriteParamList messages since the given point in time:

```json
{
    "command": "ReadParam",
	"timeSince": "123456789"
}
```

### ReadStatusMessages

The ReadStatusMessages command specifies the timeNow received in the last WriteStatusMessages message that was received
when a dropped or missing message condition is detected.  It's purpose is to have all dropped or missing status messages re-transmitted.

key | type | value
--- | ---- | -----
command	| string | "ReadStatusMessage"
messageID | string | a locally unique identifier for the message
timeSince | string | the value sent by the last known good message for timeNow

#### Examples

Request all NotifyList and WriteParamList messages since the given point in time:

```json
{
    "command": "ReadStatusMessage",
	"timeSince": "123456789"
}
```

### ReadHistorySince

The ReadHistorySince command specifies the timeNow received in the last ReadHistorySince message that was received
when a dropped or missing message condition is detected.  It's purpose is to have all dropped or missing history messages re-transmitted.

key | type | value
--- | ---- | -----
command	| string | "ReadHistorySince"
messageID | string | a locally unique identifier for the message
timeSince | string | the value sent by the last known good message for timeNow

#### Examples

Request all NotifyList and WriteParamList messages since the given point in time:

```json
{
    "command": "ReadHistorySince",
	"timeSince": "123456789"
}
```

### ReleaseParamList

The ReleaseParamList command specifies an array of Read Objects. This command un-subscribes the sender from change notifications.

key | type | value
--- | ---- | -----
command	| string | "ReleaseParamList"
sharedSecret | string | a string representation of the secret shared between the THW and the API server
messageID | string | a locally unique identifier for the message
objectList | array | array of Read objects to be unsubscribed from

#### Examples

Un-subscribe from notifications on multiple objects and params:

```json
{
    "command": "ReleaseParamList",
	"messageID" : "0xDEADBEEF",
	"objectList": [
		{
			"objnam": "C155",
			"keys": [
				"STATUS",
				"MODE"
			]
		},
		{
			"objnam": "C145",
			"keys": [
				"STATUS",
				"MODE",
				"TIMOUT"
			]
		}
	]
}
```


### ClearParam

Releases all notifications the sender had previously requested.

key | type | value
--- | ---- | -----
command	| string | "ClearParam"
messageID | string | a locally unique identifier for the message

#### Examples

Release all notifications:

```json
{
    "command": "ClearParam",
	"messageID" : "0xDEADBEEF",
}
```

### CreateObjectFromParam
This command requests that the THU create a new object of the specified type.  Depending on the object type one or more parameters may be required.
Required parameters:
    SCHEDULE: NONE
    PUMP: PARENT
    
key | type | value
--- | ---- | -----
command	| string | "ClearParam"
messageID | string | a locally unique identifier for the message
object | object | object to be created

#### Examples

Set a single Circuit STATUS to ON:

```json
{
    "command": "CreateObjectFromParam",
	"messageID" : "0xDEADBEEF",
    {
		"objtyp": "PUMP",
		"params": {
			"PARENT": "B1000"
		}
	}
}
```

### AcknowledgeRegistrationRequest

This command informs the THW of a successful registration and provides the information necessary for the THW to identify itself to the API server and the secret shared bwtween THW and API se
This command will also be invoked upon a shared secret regeneration.

key | type | value
--- | ---- | -----
command | string | "AcknowledgeRegistrationRequest"
messageID | string | a locally unique identifier for the message
requestID | string | the messageID of the request that is being acknowledged
propertyID | string | the ID of the Entity that this THW is associated with, may be same as the existing one
sharedSecret | string | a string representation of the secret shared between the THW and the API server

####Examples

```json
{
  "command": "AcknowledgeRegistrationRequest",
  "messageID": "0xB00D13",
  "requestID": "0xDEADBEEF",                        //message ID of the request
  "propertyID": "0x31337",                            //may be the same as the existing one
  "sharedSecret" : "0x0123456789ABCDEF"
}
```

## THW to Server

### Request Registration (Entity) ID

key | type | value
--- | ---- | -----
command | string | "requestRegistration"
messageID | string | a locally unique identifier for the message
userID | string | the email of the account that will be associated with the Entity
password | string | the password of this acount, either for authentication or for creation of new account
name | string | a friendly name for the Entity
hasMoved | string | "yes" or "no" depending on whether the THW detects that it has been moved
createNew | string | "yes" or "no" depending on whether a new user account should be created
propertyID | string | the most recently-assigned Entity ID (if the THW has one) otherwise blank

```json
{
   "command": "requestRegistration",      
   "messageID": "0xDEADBEEF",
   "username": "PoolManBob@BobsPoolService.com",
   "password": "1234secretpassword",
   "name": "My Vacation Home",                       
   "hasMoved": "yes",
   "createNew": "yes",
   "newRegistration": "yes",
   "propertyID": "71077345"                   
}
```

### Request New Shared Secret

Request a new shared secret.

key | type | value
--- | ---- | -----
command | string | "requestSharedSecret"
messageID | string | a locally unique identifier for the message

```json
{
   "command": "requestSharedSecret",      
   "messageID": "0xDEADBEEF",
}
```

### SendParamList

Invoked by THW in response to GetParamList. Specifies a list of Set Objects (as defined above).

key | type | value
--- | ---- | -----
command	| string | "SendParamList"
messageID | string | a locally unique identifier for the message
objectList | array | array of Set objects with the current parameter readings

#### Examples

Response from a previously requested GetParamList command:

```json
{
    "command": "SendParamList",
	"messageID" : "0xDEADBEEF",
	"objectList": [
		{
			"objname": "C155",
			"params": {
				"STATUS": "ON",
                "TIMOUT": "3600"
			}
		},
		{
			"objname": "C165",
			"params": {
				"ACT": "ROYAL"
			}
		}
	]
}
```

### NotifyList

Invoked by THW when an update is sent due to an earlier RequestParamList command. Specifies a list of Set Objects (as defined above).

key | type | value
--- | ---- | -----
command	| string | "NotifyList"
messageID | string | a locally unique identifier for the message
timeNow   | string | THW current timestamp
timeSince | string | THW timestamp of last NotifyList message
objectList | array | array of Set objects with the current parameter readings

#### Examples

Response from a previously requested RequestParamList command:

```json
{
    "command": "NotifyList",
    "timeNow"   : "123456789",
    "timeSince" : "123456788",
	"objectList": [
		{
			"objname": "C155",
			"params": {
				"STATUS": "ON",
                "TIMOUT": "3600"
			}
		},
		{
			"objname": "C165",
			"params": {
				"ACT": "ROYAL"
			}
		}
	]
}
```

### WriteParamList

Invoked by THW when a change is made to a configuration parameter.  Specified the object, the parameter and the new value.

key | type | value
--- | ---- | -----
command	| string | "WriteParam"
messageID | string | a locally unique identifier for the message
timeNow   | string | THW current timestamp
timeSince | string | THW timestamp of last NotifyList message
objectList | array | array of Set objects with the current parameter readings

#### Examples

Release all notifications:

Configuration changes to multiple parameters on multiple Circuits:

```json
{
    "command": "WriteParam",
    "timeNow"   : "123456789",
    "timeSince" : "123456788",
	"objectList": [
		{
			"deleted": ["G1000", "G1001" ...]
			"created": ["G2100", "G2103" ...]
            "changes": [
			    "objname": "S1000",
			    "params": {
				    "CIRCUIT": "C5433",
                    "DAY": "MUTFA",
                    "SINGLE": "OFF',
                    "START": "ABSTIM",
                    "TIME": "12:00",
                    "STOP": "ABSTIM",
                    "TIMOUT": "14:00",
                    "GROUP": "1"
			    }
            ]
		}
	]
} 
```

### WriteStatusMessages

Invoked by THW when a change is made to any status parameter.  Specified the object, the parameter and the new value.

key | type | value
--- | ---- | -----
command	| string | "WriteStatusMessages"
messageID | string | a locally unique identifier for the message
timeNow   | string | THW current timestamp
timeSince | string | THW timestamp of last NotifyList message
objectList | array | array of Set objects with the current parameter readings

#### Examples

Release all notifications:

Changes to multiple statuses on multiple circuits:

```json
{
    "command": "WriteStatusMessages",
    "timeNow"   : "123456789",
    "timeSince" : "123456788",
	"objectList": [
		{
			"deleted": ["G1000", "G1001" ...]
			"created": ["G2100", "G2103" ...]
            "changes": [
				"objname": "G3221",
                "params":
                {
                    "MODE":   "LTRED",
                    "SNAME":  "Heater is cooling down",
                    "PARENT": "B1000"
                }
                ...
			]
		}
	]
}
```

### WriteHistoryList

Invoked by THW when new status information has been accumulated for historical reporting.  The value of the history parameter is an octect-stream
of binary data.  The actual schema for the binary data is defined below.

key | type | value
--- | ---- | -----
command	| string | "WriteHistoryList"
messageID | string | a locally unique identifier for the message
timeNow   | string | THW current timestamp
timeSince | string | THW timestamp of last NotifyList message
history   | string | A base 64 encoded string of binary data.
                   | for supplying historical reporting data.

#### Examples

Release all notifications:

Historical reporting data:

```json
{
    "command": "WriteHistoryMessages",
    "timeNow"   : "123456789",
    "timeSince" : "123456788",
	"value": "TWFuIGlzIGRpc3Rpbmd1aZSBzaG9ydCB2ZWhlbWVuY2Ugb2YgYW55IGNhcm5hbCBwbGVhc3VyZS4="
}
```

### Schema

TBD

### NotifyObjectCreation (Deprecated)

The NotifyObjectCreation command is the responce to the CreateObjectFromParam and provides the new objects UID.

key | type | value
--- | ---- | -----
command	| string | "RequestParamList"
messageID | string | a locally unique identifier for the message
objectList | array | array of Read objects to be subscribed to

#### Examples

Response to request for object creation:

```json
{
    "command": "NotifyObjectCreation",
	"messageID" : "0xDEADBEEF",
    'objectList" : [
    {
		"objnam": "P1002"
	}
}
```

## Acknowledgement messages
These messages are sent in the case of an error, or as an acknowledgement for a command with a "void" return type

### OK

An acknowledgement that a command was received and processed properly

key | type | value
--- | ---- | -----
command	| string | "OK"
messageID | string | a locally unique identifier for the message
messageRef | string | the ID of the message that triggered the acknowledgement


```json
{
    "command": "OK",
    "messageID" : "0x000CAAAA",
    "messageRef" : "0xDEADBEEF"
}
```

### Error

A message sent when a command cannot be processed

key | type | value
--- | ---- | -----
command	| string | "Error"
messageID | string | a locally unique identifier for the message
messageRef | string | the ID of the message that triggered the error
message | string | an optional clarifying message.  May be an descriptive message, or JSON containing the failed command

```json
{
    "command": "Error",
    "messageRef" : "0xDEADBEEF",
    "message" : "The parameter 'FUBAR' is not recognized for object 'C155'"
}
```
