'use strict';
var PWC = PWC || {};
PWC.controller('ConfirmUserNameChangeController', [
    '$scope', '$location', '$routeParams', '$route', 'PropertyService', 'Enumerations', 'AppService', 'AuthService',
    function ($scope, $location, $routeParams, $route, propertyService, enumerations, appService, authService) {
        var inviteInfo = $route.current.locals.inviteInfo.data;
        var autoAcceptData = {
            UserName: inviteInfo.Email,
            OldUserName: $routeParams.oldEmail,
            InviteId: $routeParams.inviteId
        };
        $scope.InvalidInvite = false;
        $scope.EulaRefused = false;
        $scope.ShowEula = false;
        $scope.ShowExistingAcct = false;
        $scope.$parent.Layout = "layout-base";
        
        if (inviteInfo.error) {
            $scope.InvalidInvite = true;
            $scope.InvaidInviteMessage = inviteInfo.error;
        }

        $scope.Property = propertyService;
        $scope.InviteError = false;
        $scope.ExistingInviteError = false;
        $scope.ExistingInviteLoginError = false;

        $scope.AcceptInviteData = autoAcceptData;

        $scope.LoginData = {
            email: "",
            password: "",
            inviteId: $routeParams.inviteId
        };


        //AutoConfirm
        appService.PostData("Api/Account/ConfirmUserNameChange", autoAcceptData).then(function () {
            authService.authentication.userNameChanged = true;
            console.log("---------------------------logout  : userNameChanged ----------------------------------------")
            authService.logOut();
        }).catch(function (error) {
            $scope.InvalidInvite = true;
            $scope.InvaidInviteMessage = error;
        });

    }
]);