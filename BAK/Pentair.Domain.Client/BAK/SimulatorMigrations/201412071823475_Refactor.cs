namespace Pentair.Domain.Client.SimulatorMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Refactor : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ControlObjects",
                c => new
                    {
                        INSTALLATION = c.Int(nullable: false),
                        OBJNAM = c.String(nullable: false, maxLength: 16),
                        OBJTYP = c.String(maxLength: 16),
                        STATIC = c.String(maxLength: 16),
                        SUBTYP = c.String(maxLength: 16),
                        RECENT = c.String(maxLength: 16),
                        FREQ = c.Int(),
                        CUSTOM = c.Int(),
                        LISTORD = c.Int(),
                        HNAME = c.String(maxLength: 128),
                        SNAME = c.String(maxLength: 128),
                        READY = c.String(maxLength: 16),
                        PRIM = c.String(maxLength: 16),
                        SEC = c.String(maxLength: 16),
                        COMUART = c.String(maxLength: 16),
                        BODY = c.String(maxLength: 16),
                        SHARE = c.String(maxLength: 16),
                        PARENT = c.String(maxLength: 16),
                        COOL = c.String(maxLength: 16),
                        COLOR = c.String(maxLength: 16),
                        START = c.String(maxLength: 16),
                        STOP = c.String(maxLength: 16),
                        DLY = c.String(maxLength: 16),
                        PERMIT = c.String(maxLength: 16),
                        FILTER = c.String(maxLength: 16),
                        PUMP = c.String(maxLength: 16),
                        MASTER = c.String(maxLength: 16),
                        HTSRC = c.String(maxLength: 16),
                        HEATER = c.String(maxLength: 16),
                        VOL = c.Int(nullable: false),
                        SHOMNU = c.String(maxLength: 16),
                        ALPHA = c.Int(),
                        TIMZON = c.Int(),
                        FEATR = c.String(maxLength: 16),
                        USAGE = c.String(maxLength: 16),
                        CYCTIM = c.Int(),
                        DAY = c.String(maxLength: 16),
                        SINGLE = c.String(maxLength: 5),
                        GROUP = c.Int(),
                        HITMP = c.Single(),
                        LOTMP = c.Single(),
                        SETTMP = c.Single(),
                        PRIMFLO = c.String(maxLength: 16),
                        HTMODE = c.String(maxLength: 16),
                        SERVICE = c.String(),
                        PASSWRD = c.String(maxLength: 255),
                        ENABLE = c.String(maxLength: 16),
                        SENSE = c.Int(),
                        PRIMTIM = c.Int(),
                        PRESS = c.Int(),
                        SYSTIM = c.Int(),
                        BAKFLO = c.Int(),
                        BAKTIM = c.Int(),
                        RNSTIM = c.Int(),
                        VACFLO = c.Int(),
                        VACTIM = c.Int(),
                        CIRCUIT = c.String(maxLength: 16),
                        RPM = c.Int(),
                        FLOW = c.Int(),
                        PWR = c.Int(),
                        PRIOR = c.String(maxLength: 16),
                        SPEED = c.Int(),
                        BOOST = c.String(maxLength: 16),
                        IPADY = c.String(maxLength: 128),
                        SUBNET = c.String(maxLength: 128),
                        DFGAT = c.String(maxLength: 128),
                        DNSSERV = c.String(maxLength: 128),
                        WAPNET = c.String(maxLength: 128),
                        WAPPW = c.String(maxLength: 128),
                        PANID = c.Int(),
                        LEGACY = c.String(maxLength: 255),
                        STATUS = c.String(maxLength: 255),
                        TEMP = c.String(maxLength: 16),
                        EMAIL = c.String(maxLength: 255),
                        MIN = c.String(),
                        MAX = c.Int(),
                        CHEM = c.String(maxLength: 16),
                        SYSTEM = c.String(maxLength: 16),
                        PHONE = c.String(maxLength: 16),
                        URL = c.String(maxLength: 255),
                        LOGO = c.Binary(),
                        BADGE = c.Int(),
                        TIMOUT = c.String(maxLength: 255),
                        SOURCE = c.String(maxLength: 16),
                        ZIP = c.String(maxLength: 16),
                        LOCX = c.Single(),
                        LOCY = c.Single(),
                        DLSTIM = c.String(maxLength: 16),
                        USE = c.String(maxLength: 255),
                        CLK24A = c.String(maxLength: 16),
                        OFFSET = c.Int(),
                        ACCEL = c.String(maxLength: 16),
                        ABSTIM = c.DateTime(),
                        SRIS = c.String(),
                        SSET = c.String(),
                        MENU = c.String(maxLength: 16),
                        RESET = c.String(maxLength: 16),
                        PARTY = c.String(maxLength: 255),
                        ON = c.String(maxLength: 16),
                        OFF = c.String(maxLength: 16),
                        RLY = c.String(maxLength: 16),
                        FREEZE = c.String(maxLength: 16),
                        VER = c.Single(),
                        SERNUM = c.String(maxLength: 255),
                        MNFDAT = c.DateTime(),
                        NORMAL = c.Int(),
                        DIMMER = c.Int(),
                        MANUAL = c.String(maxLength: 16),
                        TIME = c.String(maxLength: 30),
                        BATT = c.Int(),
                        RADIO = c.Int(),
                        AIR = c.Int(),
                        PORT = c.Int(),
                        CNFG = c.String(maxLength: 255),
                        PANEL = c.String(maxLength: 16),
                        RUNON = c.String(maxLength: 16),
                        ASSIGN = c.String(maxLength: 16),
                        DFLT = c.Int(),
                        ACT = c.String(maxLength: 255),
                        POSIT = c.Int(),
                        VALVE = c.String(maxLength: 16),
                        MODULE = c.String(maxLength: 16),
                        COUNT = c.Int(),
                        UNITS = c.String(maxLength: 16),
                        SUPER = c.Int(),
                        SALT = c.String(maxLength: 16),
                        DEVICE = c.String(maxLength: 16),
                        SALTLO = c.String(maxLength: 16),
                        VERYLO = c.String(maxLength: 16),
                        CURRENT = c.String(maxLength: 16),
                        VOLT = c.String(maxLength: 16),
                        PHSET = c.Int(),
                        ORPSET = c.Int(),
                        PHTNK = c.Int(),
                        ORPTNK = c.Int(),
                        CALC = c.Int(),
                        CYACID = c.Int(),
                        ALK = c.Int(),
                        SINDEX = c.Int(),
                        PHFED = c.Int(),
                        ORPFED = c.Int(),
                        PHVAL = c.Int(),
                        ORPVAL = c.Int(),
                        PHTIM = c.Int(),
                        ORPTIM = c.Int(),
                        PHVOL = c.Int(),
                        ORPVOL = c.Int(),
                        NOFLO = c.String(),
                        PHHI = c.String(maxLength: 16),
                        PHLO = c.String(maxLength: 16),
                        ORPHI = c.String(maxLength: 16),
                        ORPLO = c.String(maxLength: 16),
                        PHCHK = c.String(maxLength: 16),
                        ORPCHK = c.String(maxLength: 16),
                        PROBE = c.String(maxLength: 16),
                        LOCKOUT = c.String(maxLength: 16),
                        PHLIM = c.String(maxLength: 16),
                        ORPLIM = c.String(maxLength: 16),
                        INVALID = c.String(maxLength: 255),
                        COMLNK = c.String(maxLength: 16),
                        CALIB = c.String(maxLength: 16),
                        ALARM = c.String(maxLength: 16),
                        PHTYP = c.String(maxLength: 16),
                        ORPTYP = c.String(maxLength: 16),
                        PHMOD = c.String(maxLength: 16),
                        ORPMOD = c.String(maxLength: 16),
                        FLOWDLY = c.String(maxLength: 16),
                        PHPRIOR = c.String(maxLength: 16),
                        MODE = c.String(maxLength: 16),
                        CHLOR = c.String(maxLength: 16),
                        CreateDateTime = c.DateTime(nullable: false, precision: 7, storeType: "datetime2", defaultValueSql: "GETUTCDATE()"),
                        UpdateDateTime = c.DateTime(nullable: false, precision: 7, storeType: "datetime2", defaultValueSql: "GETUTCDATE()"),
                    })
                .PrimaryKey(t => new { t.INSTALLATION, t.OBJNAM });
            
            CreateTable(
                "dbo.MessageLogs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Action = c.String(nullable: false),
                        Message = c.String(),
                        Timestamp = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        HardwareID = c.Int(nullable: false),
                        UserID = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ParameterTypes",
                c => new
                    {
                        OBJNAM = c.String(nullable: false, maxLength: 6),
                        OBJTYP = c.String(nullable: false, maxLength: 16),
                        PARAM = c.String(nullable: false, maxLength: 16),
                        Saved = c.Boolean(nullable: false),
                        History = c.Boolean(nullable: false),
                        Schedule = c.Int(nullable: false),
                        CreateDateTime = c.DateTime(nullable: false, precision: 7, storeType: "datetime2", defaultValueSql: "GETUTCDATE()"),
                        UpdateDateTime = c.DateTime(nullable: false, precision: 7, storeType: "datetime2", defaultValueSql: "GETUTCDATE()"),
                    })
                .PrimaryKey(t => new { t.OBJNAM, t.OBJTYP, t.PARAM });
            
            CreateTable(
                "dbo.PoolConfigurations",
                c => new
                    {
                        PropertyId = c.Int(nullable: false),
                        ConfigurationName = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.PropertyId, t.ConfigurationName });
            
            CreateTable(
                "dbo.Properties",
                c => new
                    {
                        RemoteHost = c.String(nullable: false, maxLength: 128),
                        PropertyId = c.Int(nullable: false),
                        ProtocolVersion = c.String(maxLength: 16),
                        Secret = c.String(maxLength: 128),
                        Name = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.RemoteHost);
            
            CreateTable(
                "dbo.Subscriptions",
                c => new
                    {
                        ClientId = c.Guid(nullable: false),
                        PropertyId = c.Int(nullable: false),
                        ObjNam = c.String(nullable: false, maxLength: 128),
                        ObjParm = c.String(nullable: false, maxLength: 128),
                        CreateDateTime = c.DateTime(nullable: false, precision: 7, storeType: "datetime2", defaultValueSql: "GETUTCDATE()"),
                    })
                .PrimaryKey(t => new { t.ClientId, t.PropertyId, t.ObjNam, t.ObjParm });
            
            CreateTable(
                "dbo.UserInvitations",
                c => new
                    {
                        InvitationId = c.Guid(nullable: false),
                        EmailAddress = c.String(),
                        InstallationId = c.Int(nullable: false),
                        AccessToken = c.String(),
                        Status = c.String(),
                        EmailId = c.String(),
                        CreateDateTime = c.DateTime(nullable: false, precision: 7, storeType: "datetime2", defaultValueSql: "GETUTCDATE()"),
                        UpdateDateTime = c.DateTime(nullable: false, precision: 7, storeType: "datetime2", defaultValueSql: "GETUTCDATE()"),
                    })
                .PrimaryKey(t => t.InvitationId)
                .ForeignKey("dbo.Installations", t => t.InstallationId, cascadeDelete: true)
                .Index(t => t.InstallationId);
            
            CreateTable(
                "dbo.Installations",
                c => new
                    {
                        InstallationId = c.Int(nullable: false, identity: true),
                        ConnectionId = c.Guid(nullable: false),
                        Secret = c.String(maxLength: 255),
                        Name = c.String(maxLength: 255),
                        ProtocolVersion = c.String(maxLength: 128),
                        Connected = c.Boolean(nullable: false),
                        HostMachine = c.String(nullable: false),
                        CreateDateTime = c.DateTime(nullable: false, precision: 7, storeType: "datetime2", defaultValueSql: "GETUTCDATE()"),
                        UpdateDateTime = c.DateTime(nullable: false, precision: 7, storeType: "datetime2", defaultValueSql: "GETUTCDATE()"),
                    })
                .PrimaryKey(t => t.InstallationId);
            
            CreateTable(
                "dbo.Associations",
                c => new
                    {
                        UserId = c.Guid(nullable: false),
                        InstallationId = c.Int(nullable: false),
                        AccessToken = c.String(),
                        CreateDateTime = c.DateTime(nullable: false, precision: 7, storeType: "datetime2", defaultValueSql: "GETUTCDATE()"),
                        UpdateDateTime = c.DateTime(nullable: false, precision: 7, storeType: "datetime2", defaultValueSql: "GETUTCDATE()"),
                    })
                .PrimaryKey(t => new { t.UserId, t.InstallationId })
                .ForeignKey("dbo.Installations", t => t.InstallationId, cascadeDelete: true)
                .Index(t => t.InstallationId);
            
            CreateTable(
                "dbo.Connections",
                c => new
                    {
                        ConnectionId = c.Guid(nullable: false, identity: true),
                        HostMachine = c.String(nullable: false),
                        InstallationId = c.Int(nullable: false),
                        UserId = c.Guid(nullable: false),
                        CreateDateTime = c.DateTime(nullable: false, precision: 7, storeType: "datetime2", defaultValueSql: "GETUTCDATE()"),
                    })
                .PrimaryKey(t => t.ConnectionId)
                .ForeignKey("dbo.Installations", t => t.InstallationId, cascadeDelete: true)
                .Index(t => t.InstallationId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserInvitations", "InstallationId", "dbo.Installations");
            DropForeignKey("dbo.Connections", "InstallationId", "dbo.Installations");
            DropForeignKey("dbo.Associations", "InstallationId", "dbo.Installations");
            DropIndex("dbo.Connections", new[] { "InstallationId" });
            DropIndex("dbo.Associations", new[] { "InstallationId" });
            DropIndex("dbo.UserInvitations", new[] { "InstallationId" });
            DropTable("dbo.Connections");
            DropTable("dbo.Associations");
            DropTable("dbo.Installations");
            DropTable("dbo.UserInvitations");
            DropTable("dbo.Subscriptions");
            DropTable("dbo.Properties");
            DropTable("dbo.PoolConfigurations");
            DropTable("dbo.ParameterTypes");
            DropTable("dbo.MessageLogs");
            DropTable("dbo.ControlObjects");
        }
    }
}
