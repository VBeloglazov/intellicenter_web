﻿(function () {
    PWC.isEmptyObject = function (obj) {
        for (var name in obj) {
            return false;
        }
        return true;
    };


    PWC.isValidObject = function (obj, props) {
        if (!obj)
            return false;

        var result = true;
        if (props) {
            for (var i = 0; i < props.length; i++) {
                if (!obj.hasOwnProperty(props[i]))
                    result = false;
            }
        }
        return result;
    }

    PWC.addMinutes = function (dateTime, minutes) {
        var result = new Date(dateTime);
        result.setMinutes(result.getMinutes() + minutes);
        return result;
    }

    PWC.addTwohours = function (dateTime) {
        var result = new Date(dateTime);
        result.setHours(result.getHours() + 2);
        return result;
    }


    PWC.convertToDate = function (HHmmSS, timeZone) {
        HHmmSS = HHmmSS.replace(/,/g, ':');
        var tz = timeZone > 0 ?
            timeZone > 9 ? timeZone.toString() : '0' + timeZone.toString() :
            timeZone < -9 ? '-' + Math.abs(timeZone).toString() : '-0' + Math.abs(timeZone).toString();
        tz = tz + '00';
        var newDate = new Date(Date.parse('2014-10-08T' + HHmmSS + tz));
        if (isNaN(newDate.getTime())) {
            newDate = new Date(Date.parse('2014/10/08T' + HHmmSS + tz));
        }
        if(HHmmSS === '') {
            newDate = new Date(Date.parse('2014-10-08T' + '00:00:00' + tz));
        }

        if (newDate == 'Invalid Date') {
            newDate = new Date(Date.parse('2014-10-08T' + '00:00:00' + tz));
        }
        return newDate;
    };

    PWC.convertToTime = function (HHmm) {
        HHmm = HHmm.replace(',', ';');
        return HHmm;
    }

    PWC.setActiveTab = function (tabs, page) {
        angular.forEach(tabs, function (tab) {
            tab.active = tab.page.toUpperCase() === page.toUpperCase();
        });
    }

}());