﻿describe('onClickOutside', function () {
    var $scope, $compile;
    var elm;
    var baseTemplate = '<div on-click-outside="outSideClicked()"></div>';

    var propertyService;
    var elmScope;
    beforeEach(module('PentairWebClient'));

    beforeEach(inject(function ($rootScope, _$compile_, _$document_, _PropertyService_) {
        $scope = $rootScope.$new();
        $compile = _$compile_;
        $document = _$document_;
        propertyService = _PropertyService_;

        $scope.outSideClicked = jasmine.createSpy('outSideClicked');
    }));

    describe('Click insde', function () {
        it('should not invoke outSideClicked method', function () {

            elm = SpecHelper.createDirective($compile, baseTemplate, $scope);
            elmScope = elm.isolateScope();
            $document.find('body').append(elm)
            elm.triggerHandler('click');

            expect($scope.outSideClicked).not.toHaveBeenCalled();
        });
    });
    describe('Click outside', function () {
        it('should invoke outSideClicked method', function () {

            elm = SpecHelper.createDirective($compile, baseTemplate, $scope);
            elmScope = elm.isolateScope();
            $document.find('body').append(elm)
            $document.triggerHandler('click');

            expect($scope.outSideClicked).toHaveBeenCalled();
        });
    });

    describe('after destroy', function () {
        it('should not invoke outSideClicked method', function () {

            elm = SpecHelper.createDirective($compile, baseTemplate, $scope);
            elmScope = elm.isolateScope();
            $document.find('body').append(elm)

            $document.triggerHandler('click');
            elmScope.$destroy();

            $document.triggerHandler('click');
            expect($scope.outSideClicked.calls.count()).toBe(1);
        });
    });


});