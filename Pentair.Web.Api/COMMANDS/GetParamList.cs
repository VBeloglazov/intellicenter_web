﻿using System.Collections.Generic;

namespace Pentair.Web.Api
{
    public class GetParamList : KeyCommand
    {
        public GetParamList(
            string messageID,
            string condition,
            List<KeyObject> objectList
            )
            : base(CommandString.GetParamList, messageID, condition, objectList)
        {
        }

    } // class GetParamList
}