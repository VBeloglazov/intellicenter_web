-- =============================================================
--
-- Author:		Vladimir Beloglazov
-- Create date: 03/12/2018
--
-- Creates:
--
--		Table Type:			None
--
--		Stored Procedure:	PUpdateInstallationName
--
--
-- 2018-04-17 VB: Restored (content moved from PUpdateControlObjects).
--
-- =============================================================

USE PathfinderIntelliCenter
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (
	SELECT * FROM sys.objects
	WHERE object_id = OBJECT_ID(N'dbo.PUpdateInstallationName')
	AND type in (N'P', N'PC')
)
DROP PROCEDURE dbo.PUpdateInstallationName
GO

-- -------------------------------------------------------------
--
-- Updates [Installations].Name.
--
-- -------------------------------------------------------------
CREATE PROCEDURE dbo.PUpdateInstallationName
	 @installationId	AS int
	,@name				AS nvarchar(256)

AS
BEGIN

	SET NOCOUNT OFF

	BEGIN TRY

		-- Can throw if UK_Installations_Name_OwnerUserId violation
		UPDATE dbo.Installations
		SET  Name = @name
		WHERE InstallationId = @installationId

	END TRY
	BEGIN CATCH

		DECLARE @err_message_1 nvarchar(255)
		SET @err_message_1 = ERROR_MESSAGE()
		RAISERROR (@err_message_1, 11, 1) -- severity 11-19 will cause returning exception to caller

	END CATCH

END
GO
