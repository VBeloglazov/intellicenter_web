﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Pentair.Domain.Client.Helpers.Commands
{
    public class StatusResponse : ResponseCommand
    {
        [JsonProperty("status", Required = Required.Always)]
        public string Status { get; private set; }

        [JsonProperty("messageDetail", Required = Required.Always)]
        public string MessageDetail { get; private set; }

        public StatusResponse(
            string command,
            string messageID,
            string response,
            string status,
            string messageDetail
            )
            : base(command, messageID, response)
        {
            this.Status = status;
            this.MessageDetail = messageDetail;

        } // StatusResponse()

        /// <summary>
        /// Always returns an object, valid or not. If invalid, check the DeserializationError property value.
        /// </summary>
        public static new StatusResponse Deserialize(
            string serializedMessage)
        {
            StatusResponse command = null;

            try
            {
                command = JsonConvert.DeserializeObject<StatusResponse>(serializedMessage);
                command.IsValid = true;
            }
            catch (Exception e)
            {
                // Deserialization as StatusResponse failed

                StringBuilder sbError = new StringBuilder();

                sbError.Append("Cannot deserialize as StatusResponse. " + e.Message);

                // Try to get at least the base message data
                ResponseCommand baseCommand = ResponseCommand.Deserialize(serializedMessage);

                if (!baseCommand.IsValid)
                {
                    sbError.Append("\n" + baseCommand.DeserializationError);
                }

                command = new StatusResponse(
                    command: baseCommand.Command,
                    messageID: baseCommand.MessageID,
                    response: ResponseString.BadRequest,
                    status: String.Empty,
                    messageDetail: String.Empty)
                {
                    IsValid = false,
                    DeserializationError = sbError.ToString()
                };
            }

            return command;

        } // Deserialize()

    } // class StatusResponse
}
