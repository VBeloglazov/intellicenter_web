using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;

using Microsoft.AspNet.Identity.EntityFramework;

using Newtonsoft.Json;

using SERVICE_SHARED;

namespace Pentair.Web.Api
{
    public class WebSocketClient_THW : WebSocketClient
    {
        protected override string EventSourceName { get { return "WebSocketClient_THW"; } }

        #region Construction

        /// <summary>
        /// Any Property will send an authenticate message with its InstallationID
        /// and shared secret before it is allowed access to any services other than
        /// registration.
        /// </summary>
        public WebSocketClient_THW()
            : base(
                installationId: -1,
                userId: Guid.Empty
            )
        {
        } // WebSocketClient_THW()

        #endregion Construction

        #region WebSocketClient overrides

        public override EndpointType TypeEndpoint
        {
            get { return EndpointType.THW; }

        } // TypeEndpoint

        public override void AddConnection()
        {
            try
            {
                // Remove all Clients related to the Installation Id: both THW and CLient types.

                WebSocketClient.RemoveThwClients(
                    installationId: this.InstallationId,
                    removeThw: true, // To prevent duplicated THW connections
                    caller: "AddConnection",
                    reason: "THW reconnected"
                );

                using (DbContextPentair db = DbContextPentair.New())
                {
                    this.Connection = db.AddConnectionThw(
                        installationId: this.InstallationId,
                        eventId: (int)EventTypeCodes.InstallationConnectEvent,
                        userId: 0 // TODO: int? Has anybody tested this?
                    );

                } // using (DbContextPentair db)

                WebSocketClient.AddClient(this);
            }
            catch (Exception e)
            {
                LogManager.LogErrorAsync(
                    source: this.EventSourceName,
                    eventBriefDescription: "Adding THW connection failed",
                    installationId: this.InstallationId,
                    userId: this.UserId,
                    connectionId: this.ConnectionId,
                    eventFullDescription:
                        ExceptionManager.CustomizeException(e).ErrorMessage(
                            errorHeader: null,
                            addStackTrace: true
                        )
                );

                Disconnect(
                    isHardStop: false,
                    caller: "WebSocketClient_THW.AddConnection()",
                    reason: "Error"
                );
            }

        } // AddConnection()

        public override async Task RemoveConnectionAsync()
        {
            try
            {
                if (this.InstallationId > 0)
                {
                    // Remove the connection from the list and the database
                    RemoveClient(this);

                    // Notify servers to make them disconnect all the THW clients
                    await BroadcastEsbAsync(
                        destHostMachine: null, // This is broadcast, it shall be processed by all the servers including this one.
                        destType: EndpointType.Server,
                        installationId: this.InstallationId,
                        clientConnectionId: Guid.Empty,
                        serializedMessage: new ServerCommandRemoveThwClients(this.InstallationId).Serialize()
                    )
                    .ConfigureAwait(false);

                } // if (this.InstallationId > 0)
            }
            catch (Exception e)
            {
                LogManager.LogErrorAsync(
                    source: this.EventSourceName,
                    eventBriefDescription: "Removing THW connection failed",
                    installationId: this.InstallationId,
                    userId: this.UserId,
                    connectionId: this.ConnectionId,
                    eventFullDescription:
                        ExceptionManager.CustomizeException(e).ErrorMessage(
                            errorHeader: null,
                            addStackTrace: true
                        )
                );
            }

        } // RemoveConnectionAsync()

        /// <exception cref="Exception" />
        protected override async Task ProcessRequestAsync(
            string message)
        {
            // Only THW connections are allowed with null ID, check for authentication
            // or registration message.
            if (this.ConnectionId.Equals(Guid.Empty))
            {
                ProcessHardwareAuthentication(message);
            }

            // THW message with valid connection Id
            else if (!WebSocketMessage.IsAnAcknowledgementMessage(message))
            {
                await ProcessHardwareMessageAsync(message).ConfigureAwait(false);
            }

        } // ProcessRequestAsync()

        protected override void RemoveClient(
            WebSocketClient clientToRemove)
        {
            // Remove this THW Client
            base.RemoveClient(clientToRemove);

            // Disconnect all Clients related to the Installation Id.
            WebSocketClient.RemoveThwClients(
                installationId: this.InstallationId,
                removeThw: false, // Client only
                caller: "WebSocketClient_THW.RemoveClient",
                reason: "THW disconnected"
            );

        } // RemoveClient()

        #endregion WebSocketClient overrides

        #region Implementation

        /// <exception cref="Exception" />
        private void ProcessHardwareAuthentication(
            string message)
        {
            MessageType msgType;
            try
            {
                if (message == "ping")
                {
                    msgType = new MessageType(EndpointType.THW, CommandType.Ping, null);
                }
                else
                {
                    msgType = WebSocketMessage.ParseMessageType(message, EndpointType.THW);
                }
            }
            catch (Exception e)
            {
                msgType = new MessageType(EndpointType.THW, CommandType.Invalid, null);

                LogManager.LogErrorAsync(
                    source: this.EventSourceName,
                    eventBriefDescription: "ProcessHardwareAuthentication failed",
                    installationId: this.InstallationId,
                    userId: this.UserId,
                    connectionId: this.ConnectionId,
                    eventFullDescription:
                        ExceptionManager.CustomizeException(e).ErrorMessage(
                            errorHeader: null,
                            addStackTrace: true
                        )
                );

                Disconnect(
                    isHardStop: false,
                    caller: "WebSocketClient_THW.ProcessHardwareAuthentication()",
                    reason: "Invalid message received"
                );

                return;
            }

            switch (msgType.CommandType)
            {
                case CommandType.Authenticate:
                    {
                        int deviceId = AuthenticationManager.AuthenticateDevice(message);

                        if (deviceId > 0)
                        {
                            this.InstallationId = deviceId;

                            lock (this.lockTimerInactivity)
                            {
                                if (this.timerInactivity.Enabled)
                                {
                                    this.timerInactivity.Stop();
                                }
                            }

                            AddConnection();

                            SendMessage(
                                JsonConvert.SerializeObject(
                                    new AuthenticationSucceededResponse(
                                        messageID: msgType.MessageID
                                    )
                                ),
                                WebSocketActions.Send
                            );

                            lock (this.lockTimerInactivity)
                            {
                                this.timerInactivity.Interval = CONNECTION_INACTIVITY_TIMEOUT_MSEC_AFTER_ADDCONNECTION;
                                this.timerInactivity.Start();
                            }
                        }
                        else
                        {
                            // ac.Valid is false

                            Disconnect(
                                isHardStop: false,
                                caller: "WebSocketClient_THW.ProcessHardwareAuthentication()",
                                reason: "Authentication failed"
                            );

                        } // if (ac.Valid)
                    }

                    break;

                case CommandType.Registration:

                    SendMessage(
                        message: JsonConvert.SerializeObject(AuthenticationManager.RegisterDevice(message)),
                        action: WebSocketActions.Send
                    );

                    break;

                case CommandType.GetRegisteredProperties:

                    SendMessage(
                        message: JsonConvert.SerializeObject(AuthenticationManager.RegisteredProperties(message)),
                        action: WebSocketActions.Send
                    );

                    break;

                default:
                    Disconnect(
                        isHardStop: false,
                        caller: "WebSocketClient_THW.ProcessHardwareAuthentication()",
                        reason: "Forbidden before authentication: " + msgType.CommandType
                    );
                    break;

            } // switch (msgType.CommandType)

        } // ProcessHardwareAuthentication()

        /// <exception cref="Exception" />
        private async Task ProcessHardwareMessageAsync(
            string message)
        {
            if (message == "Close Connection")
            {
                Disconnect(
                    isHardStop: false,
                    caller: "WebSocketClient_THW.ProcessHardwareMessage()",
                    reason: "Close Connection received"
                );

                return;

            } // if (message == "Close Connection")

            else if (message == "ping")
            {
                SendMessage("pong", WebSocketActions.Send);

                return;

            } // if (message == "ping")

            else
            {
                MessageType msgType;
                try
                {
                    msgType = WebSocketMessage.ParseMessageType(message, EndpointType.THW);
                }
                catch (Exception e)
                {
                    msgType = new MessageType(EndpointType.THW, CommandType.Invalid, null);

                    LogManager.LogErrorAsync(
                        source: this.EventSourceName,
                        eventBriefDescription: "ProcessHardwareMessageAsync failed",
                        installationId: this.InstallationId,
                        userId: this.UserId,
                        connectionId: this.ConnectionId,
                        eventFullDescription:
                            ExceptionManager.CustomizeException(e).ErrorMessage(
                                errorHeader: null,
                                addStackTrace: true
                            )
                    );

                    SendMessage(
                        JsonConvert.SerializeObject(
                            new ErrorResponse(
                                messageID: Guid.Empty.ToString(),
                                response: ResponseString.BadRequest,
                                description: "Invalid request"
                            )
                        ),
                        WebSocketActions.Send
                    );

                    return;
                }

                switch (msgType.CommandType)
                {
                    case CommandType.Registration:
                    case CommandType.Authenticate:
                        Disconnect(
                            isHardStop: false,
                            caller: "WebSocketClient_THW.ProcessHardwareMessage()",
                            reason: "Registration/Authenticate received"
                        );
                        break;

                    case CommandType.GetRegisteredProperties:

                        SendMessage(
                            message: JsonConvert.SerializeObject(AuthenticationManager.RegisteredProperties(message)),
                            action: WebSocketActions.Send
                        );

                        break;

                    case CommandType.InviteUser:
                        SendMessage(
                            message: MessageProcessor.InviteNewUser(message, this.InstallationId, this.UserId, this.ConnectionId),
                            action: WebSocketActions.Send
                        );
                        break;

                    case CommandType.Ping:
                        System.Diagnostics.Debug.WriteLine("Sending PONG to: " + InstallationId);

                        SendMessage(
                            "{\"command\":\"pong\", \"messageId\":\"" + msgType.MessageID + "\"}", WebSocketActions.Send
                        );
                        break;

                    case CommandType.NotifyList:
                        await RouteNotificationsAsync(message).ConfigureAwait(false);
                        break;

                    case CommandType.WriteParamList:
                        await UpdateCacheWriteParamListAsync(message).ConfigureAwait(false);
                        break;

                    case CommandType.ReadLastHistoryTime:
                        SendLastHistoryTime(message);
                        break;

                    case CommandType.WriteHistoryMessage:
                        UpdateCacheHistoryList(message);
                        break;

                    case CommandType.ClearHistoryMessage:
                        ClearHistoryMessage(message);
                        break;

                    // Process ResponseCommand subclasses
                    case CommandType.GetParamListResponse:
                    case CommandType.SendQuery:
                    case CommandType.SuccessMessage:
                    case CommandType.ObjectCreated:
                    case CommandType.SetCommand:
                    case CommandType.SetParamList:
                    case CommandType.ClearParam:
                    case CommandType.ReleaseParamList:
                    case CommandType.WriteStatusMessage:
                        {
                            ResponseCommand responseCommand = null;
                            try
                            {
                                responseCommand =
                                    ResponseCommand.ResponseCommandFactory(msgType.CommandType, message);

                                if (!responseCommand.IsValid)
                                {
                                    throw new Exception(
                                        responseCommand.DeserializationError ?? "Unknown deserialization error"
                                    );
                                }

                                if (responseCommand.Response != ResponseString.OK &&
                                    responseCommand.Response != ResponseString.Created)
                                {
                                    LogManager.LogWarningAsync(
                                        source: this.EventSourceName,
                                        eventBriefDescription: "ResponseCommand is error",
                                        installationId: this.InstallationId,
                                        userId: this.UserId,
                                        connectionId: this.ConnectionId,
                                        eventFullDescription:
                                            String.Format("THW failed to process {0} originated by {1}.\n\nResponse: {2}",
                                                msgType.CommandType,
                                                responseCommand.IsOriginatedByApi ? "API" : "Client",
                                                JsonConvert.SerializeObject(responseCommand)
                                            )
                                    );
                                }
                            }
                            catch (Exception e)
                            {
                                LogManager.LogErrorAsync(
                                    source: this.EventSourceName,
                                    eventBriefDescription: "Process ResponseCommand failed",
                                    installationId: this.InstallationId,
                                    userId: this.UserId,
                                    connectionId: this.ConnectionId,
                                    eventFullDescription:
                                        String.Format("THW response processing failed\n\n{0}\n\nReceived: {1}",
                                            ExceptionManager.CustomizeException(e).ErrorMessage(
                                                errorHeader: null,
                                                addStackTrace: true
                                            ),
                                            message
                                        )
                                );
                            }

                            if (responseCommand != null && !responseCommand.IsOriginatedByApi)
                            {
                                await RouteToClientAsync(responseCommand).ConfigureAwait(false);
                            }
                        }
                        break;

                    // 1.0.0.14
                    case CommandType.AlertOn:
                    case CommandType.AlertOff:
                        {
                            AlertNotificationCommand alertNotificationCommand = null;
                            try
                            {
                                alertNotificationCommand =
                                    AlertNotificationCommand.AlertNotificationCommandFactory(msgType.CommandType, message);

                                if (!alertNotificationCommand.IsValid)
                                {
                                    throw new Exception(
                                        alertNotificationCommand.DeserializationError ?? "Unknown deserialization error"
                                    );
                                }
                            }
                            catch (Exception e)
                            {
                                LogManager.LogErrorAsync(
                                    source: this.EventSourceName,
                                    eventBriefDescription: "Process AlertNotificationCommand failed",
                                    installationId: this.InstallationId,
                                    userId: this.UserId,
                                    connectionId: this.ConnectionId,
                                    eventFullDescription:
                                        String.Format("THW alert notification processing failed\n\n{0}\n\nReceived: {1}",
                                            ExceptionManager.CustomizeException(e).ErrorMessage(
                                                errorHeader: null,
                                                addStackTrace: true
                                            ),
                                            message
                                        )
                                );
                            }

                            SendEmailNotifications(alertNotificationCommand);
                        }
                        break;

                    case CommandType.ErrorMessage:
                        {
                            ErrorResponse responseCommand = JsonConvert.DeserializeObject<ErrorResponse>(message);

                            LogManager.LogWarningAsync(
                                source: this.EventSourceName,
                                eventBriefDescription: "THW returned error",
                                installationId: this.InstallationId,
                                userId: this.UserId,
                                connectionId: this.ConnectionId,
                                eventFullDescription: responseCommand.Response + " " + responseCommand.Description
                            );

                            if (responseCommand != null && !responseCommand.IsOriginatedByApi)
                            {
                                await RouteToClientAsync(responseCommand).ConfigureAwait(false);
                            }
                        }
                        break;

                    default:
                        LogManager.LogErrorAsync(
                            source: this.EventSourceName,
                            eventBriefDescription: "Unknown message type",
                            installationId: this.InstallationId,
                            userId: this.UserId,
                            connectionId: this.ConnectionId,
                            eventFullDescription: String.Format("Received from THW: '{0}'", msgType.CommandType)
                        );

                        break;

                } // switch (msgType.Command)

            } // Not "Close Connection", not "ping"

        } // ProcessHardwareMessageAsync()

        /// <exception cref="Exception" />
        private async Task RouteToClientAsync(
            ResponseCommand responseCommand)
        {
            string serializedMessage = responseCommand.Serialize();

            WebSocketClient_Client client =
                FindClientByConnectionId(responseCommand.TargetConnectionId) as WebSocketClient_Client;

            if (client != null)
            {
                // Client connected to this server, communicate directly
                client.SendMessage(serializedMessage, WebSocketActions.SendClient);
            }
            else
            {
                // Client might be connected to different server, try ESB

                try
                {
                    string clientHostMachine = null;

                    using (DbContextPentair db = DbContextPentair.New())
                    {
                        Connection connectionClient = db.GetConnection(responseCommand.TargetConnectionId);

                        if (connectionClient != null)
                        {
                            clientHostMachine = connectionClient.HostMachine;
                        }
                        else
                        {
                            LogManager.LogWarningAsync(
                                source: this.EventSourceName,
                                eventBriefDescription: "Routing to Client failed",
                                installationId: this.InstallationId,
                                userId: this.UserId,
                                connectionId: this.ConnectionId,
                                eventFullDescription:
                                    String.Format("Client already disconnected\n\nMessage lost:\n{0}",
                                        serializedMessage
                                    )
                            );
                        }

                    } // using (DbContextPentair db)

                    if (!String.IsNullOrWhiteSpace(clientHostMachine))
                    {
                        if (clientHostMachine == WebApiApplication.HOSTNAME)
                        {
                            // It connected to this server, but not found in the Clients list: we have data corrupted!
                            throw new Exception(
                                "Data corrupted: according to the DB, Client connected to this server, but not found in the Clients list"
                            );
                        }

                        // Broadcast to make another server to handle the message
                        // TODO: VB This shouldn't be broadcast, we know the addressee server
                        await BroadcastEsbAsync(
                            destHostMachine: clientHostMachine,
                            destType: EndpointType.Client,
                            installationId: this.InstallationId,
                            clientConnectionId: responseCommand.TargetConnectionId,
                            serializedMessage: serializedMessage
                        )
                        .ConfigureAwait(false);
                    }
                }
                catch (Exception e)
                {
                    LogManager.LogErrorAsync(
                        source: this.EventSourceName,
                        eventBriefDescription: "Routing to Client failed",
                        installationId: this.InstallationId,
                        userId: this.UserId,
                        connectionId: this.ConnectionId,
                        eventFullDescription:
                            String.Format("{0}\n\nMessage lost:\n{1}",
                                ExceptionManager.CustomizeException(e).ErrorMessage(
                                    errorHeader: null,
                                    addStackTrace: true
                                ),
                                serializedMessage
                            )
                    );
                }
            }

        } // RouteToClientAsync()

        private async Task RouteNotificationsAsync(
            string message)
        {
            try
            {
                NotifyListResponse responseNotifyList =
                    JsonConvert.DeserializeObject<NotifyListResponse>(message);

                responseNotifyList.TrimMessageId(); // Should not be needed for notifications, just in case

                // Route the message to all connected clients

                using (DbContextPentair db = DbContextPentair.New())
                {
                    IEnumerable<Connection> destinations = db.GetThwClientConnections(this.InstallationId);

                    foreach (Connection connection in destinations)
                    {
                        responseNotifyList.TargetConnectionId = connection.ConnectionId;
                        await RouteToClientAsync(responseNotifyList).ConfigureAwait(false);

                    } // foreach (Connection connection)

                } // using (DbContextPentair db)
            }
            catch (Exception e)
            {
                LogManager.LogErrorAsync(
                    source: this.EventSourceName,
                    eventBriefDescription: "Routing notification to Client failed",
                    installationId: this.InstallationId,
                    userId: this.UserId,
                    connectionId: this.ConnectionId,
                    eventFullDescription:
                        String.Format("{0}\n\nMessage lost:\n{1}",
                            ExceptionManager.CustomizeException(e).ErrorMessage(
                                errorHeader: null,
                                addStackTrace: true
                            ),
                            message
                        )
                );
            }

        } // RouteNotificationsAsync()

        private async Task UpdateCacheWriteParamListAsync(
            string message)
        {
            WriteParamListResponse writeParamListCommand = WriteParamListResponse.Deserialize(message);

            if (!writeParamListCommand.IsValid)
            {
                throw new Exception(
                    String.Format("WriteParamListResponse deserialization failed with error: {0}",
                        writeParamListCommand.DeserializationError
                    )
                );
            }

            long timeSince;
            if (!System.Int64.TryParse(writeParamListCommand.TimeSince, out timeSince))
            {
                throw new Exception("timeSince received invalid: " + writeParamListCommand.TimeSince);
            }

            long timeNow;
            if (!System.Int64.TryParse(writeParamListCommand.TimeNow, out timeNow))
            {
                throw new Exception("timeNow received invalid: " + writeParamListCommand.TimeNow);
            }

            //
            // Message deserialization succeeded, writeParamListCommand must be valid object, update the DB.
            //

            try
            {
                using (DbContextPentair db = DbContextPentair.New())
                {
                    db.TouchInstallation(this.InstallationId, timeNow); // TODO: UTC instead?

                    if (writeParamListCommand.ObjectList != null)
                    {
                        db.UpdateControlObjects(
                            installationId: this.InstallationId,
                            objList: writeParamListCommand.ObjectList,
                            deleteNotPresented: (timeSince == 0)
                        );
                    }

                } // using (DbContextPentair db)

                //
                // Route response to all the THW clients
                //

                if (timeSince != 0)
                {
                    using (DbContextPentair db = DbContextPentair.New())
                    {
                        IEnumerable<Connection> destinations = db.GetThwClientConnections(this.InstallationId);

                        foreach (Connection connection in destinations)
                        {
                            writeParamListCommand.TargetConnectionId = connection.ConnectionId;
                            await RouteToClientAsync(writeParamListCommand).ConfigureAwait(false);

                        } // foreach (Connection connection)

                    } // using (DbContextPentair db)
                }
            }
            catch (Exception e)
            {
                LogManager.LogErrorAsync(
                    source: this.EventSourceName,
                    eventBriefDescription: "Handling WriteParamList failed",
                    installationId: this.InstallationId,
                    userId: this.UserId,
                    connectionId: this.ConnectionId,
                    eventFullDescription:
                        String.Format("{0}\n\nMessage lost:\n{1}",
                            ExceptionManager.CustomizeException(e).ErrorMessage(
                                errorHeader: null,
                                addStackTrace: true
                            ),
                            message
                        )
                );
            }

        } // UpdateCacheWriteParamListAsync()

        private void SendLastHistoryTime(
            string request)
        {
            HistoryReadLastTimeCommand formattedMessage =
                JsonConvert.DeserializeObject<HistoryReadLastTimeCommand>(request);

            long lastHistoryTime = 0L;

            try
            {
                using (DbContextPentair db = DbContextPentair.New())
                {
                    lastHistoryTime = db.GetThwLastHistoryTime(this.InstallationId);

                } // using (DbContextPentair db)
            }
            catch (Exception e)
            {
                LogManager.LogErrorAsync(
                    source: this.EventSourceName,
                    eventBriefDescription: "Cannot get LastHistory from DB",
                    installationId: this.InstallationId,
                    userId: this.UserId,
                    connectionId: this.ConnectionId,
                    eventFullDescription:
                        ExceptionManager.CustomizeException(e).ErrorMessage(
                            errorHeader: null,
                            addStackTrace: true
                        )
                );

                SendMessage(
                    JsonConvert.SerializeObject(
                        new ErrorResponse(
                            messageID: formattedMessage.MessageID,
                            response: ResponseString.InternalServerError,
                            description: "Cannot get LastHistory from DB"
                        )
                    ),
                    WebSocketActions.Send
                );

                return;
            }

            SendMessage(
                JsonConvert.SerializeObject(
                    new HistoryWriteLastTimeResponse(
                        messageID: formattedMessage.MessageID,
                        response: ResponseString.OK,
                        lastHistoryTime: lastHistoryTime
                    )
                ),
                WebSocketActions.Send
            );

        } // SendLastHistoryTime()

        /// <exception cref="Exception" />
        private void UpdateCacheHistoryList(
            string message)
        {
            HistoryWriteCommandResponse historyWriteCommand = HistoryWriteCommandResponse.Deserialize(message);

            if (!historyWriteCommand.IsValid)
            {
                throw new Exception(
                    String.Format("HistoryWriteCommandResponse deserialization failed with error: {0}",
                        historyWriteCommand.DeserializationError
                    )
                );
            }

            //
            // Message deserialization succeeded, historyWriteCommand must be valid object, update the DB.
            //

            try
            {
                using (DbContextPentair db = DbContextPentair.New())
                {
                    // TODO: Replace with raw SQL

                    Installation installation = db.GetInstallation(this.InstallationId);

                    if (installation == null)
                    {
                        throw new Exception("Not found: InstallationId = " + this.InstallationId);
                    }

                    if (historyWriteCommand.ObjectList != null)
                    {
                        db.UpdateHistory(
                            this.InstallationId,
                            historyWriteCommand.ObjectList,
                            Convert.ToInt64(historyWriteCommand.TimeNow)
                        );

                    } // if (historyWriteCommand.ObjectList != null)

                } // using (DbContextPentair db)

                SendMessage(
                    JsonConvert.SerializeObject(
                        new WriteHistoryMessageResponse(
                            messageID: historyWriteCommand.MessageID,
                            response: ResponseString.OK,
                            messageNo: historyWriteCommand.MessageNo,
                            timestamp: historyWriteCommand.TimeNow,
                            error: string.Empty
                        )
                    ),
                    WebSocketActions.Send
                );
            }
            catch (Exception e)
            {
                //
                // History updating failed
                //

                LogManager.LogErrorAsync(
                    source: this.EventSourceName,
                    eventBriefDescription: "Failed to update history",
                    installationId: this.InstallationId,
                    userId: this.UserId,
                    connectionId: this.ConnectionId,
                    eventFullDescription:
                        ExceptionManager.CustomizeException(e).ErrorMessage(errorHeader: null, addStackTrace: true)
                );

                SendMessage(
                    JsonConvert.SerializeObject(
                        new WriteHistoryMessageResponse(
                            messageID: historyWriteCommand.MessageID,
                            response: ResponseString.InternalServerError,
                            messageNo: historyWriteCommand.MessageNo,
                            timestamp: historyWriteCommand.TimeNow,
                            error: e.Message
                        )
                    ),
                    WebSocketActions.Send
                );

            } // catch (Exception e)

        } // UpdateCacheHistoryList()

        /// <exception cref="Exception" />
        private void ClearHistoryMessage(
            string message)
        {
            ClearHistoryMessageResponse clearHistoryCommand = ClearHistoryMessageResponse.Deserialize(message);

            if (!clearHistoryCommand.IsValid)
            {
                throw new Exception("ClearHistoryResponse deserialization failed: " + clearHistoryCommand.DeserializationError);
            }

            long timeStart;
            if (!System.Int64.TryParse(clearHistoryCommand.TimeStart, out timeStart))
            {
                throw new Exception("timeStart received invalid: " + clearHistoryCommand.TimeStart);
            }

            long timeEnd;
            if (!System.Int64.TryParse(clearHistoryCommand.TimeEnd, out timeEnd))
            {
                throw new Exception("timeNow received invalid: " + clearHistoryCommand.TimeEnd);
            }

            //
            // Message deserialization succeeded, clearHistoryCommand must be valid object, update the DB.
            //

            try
            {
                using (DbContextPentair db = DbContextPentair.New())
                {
                    db.ClearHistory(this.InstallationId, timeStart, timeEnd);
                }
            }
            catch (Exception e)
            {
                //
                // Clear History failed
                //

                LogManager.LogErrorAsync(
                    source: this.EventSourceName,
                    eventBriefDescription: "Failed to clear history",
                    installationId: this.InstallationId,
                    userId: this.UserId,
                    connectionId: this.ConnectionId,
                    eventFullDescription:
                        ExceptionManager.CustomizeException(e).ErrorMessage(
                            errorHeader: null,
                            addStackTrace: true
                        )
                );

            } // catch (Exception e)

        } // ClearHistoryMessage()

        // 1.0.0.14
        /// <exception cref="Exception" />
        private void SendEmailNotifications(
            AlertNotificationCommand alertNotificationCommand)
        {
            if (alertNotificationCommand.Mode != AlertNotificationCommand.Category.Error &&
                alertNotificationCommand.Mode != AlertNotificationCommand.Category.Warning)
            {
                LogManager.LogErrorAsync(
                    source: this.EventSourceName,
                    eventBriefDescription: "Invalid alert notification", 
                    installationId: this.InstallationId,
                    userId: this.UserId,
                    connectionId: this.ConnectionId,
                    eventFullDescription: String.Format("Invalid parameter: Mode = {0}", (int)alertNotificationCommand.Mode)
                );

                return;
            }

            using (DbContextPentair db = DbContextPentair.New())
            {
                //InstallationWithDetails userInstallation =
                //    db.GetInstallationWithDetails(this.InstallationId);

                IEnumerable<NotificationRecipient> recipients = db.GetInstallationNotificationRecipients(this.InstallationId);
               
                //string subject = "Pentair IntelliCenter Alert";

                //AlertNote alertNote = db.GetAlertNote(alertNotificationCommand.Mode.ToString(), alertNotificationCommand.Alert);

                //string alertDescription = String.Format("{0}:<BR><B>{1}</B>",
                //    alertNote.AlertMode,
                //    alertNote.AlertMessage
                //);


                //DateTime dtIssued = new DateTime(
                //    alertNotificationCommand.TimeStamp * 10000000L + new DateTime(1970, 1, 1).Ticks
                //);

                //string alertStatus = (alertNotificationCommand is AlertNotificationOn)
                //    ? " is <B>ACTIVE</B>"
                //    : (alertNotificationCommand is AlertNotificationOff) ? " has <B>CLEARED</B>" : String.Empty;

                //string message =
                //    String.Format("<P>Alert for <B>{0}</B> {1} </P><P>{2}</P><P>{3}</P><P>Time & Date of Alert: {4} {5}</P><P>{6}<BR>{7}, {8} {9}<BR>{10}<BR>{11}<BR></P><P>You can login to IntelliCenter at this link: https://www.intellicenter.com</P><P>Thank you,</P><P><BR>Pentair</P>",
                //        userInstallation.Name,
                //        alertStatus,
                //        alertDescription,
                //        alertNote.Note,
                //        dtIssued.ToLocalTime(),         // convert to local time       
                //        TimeZone.CurrentTimeZone.StandardName,
                //        userInstallation.Address,
                //        userInstallation.City,
                //        userInstallation.State,
                //        userInstallation.Zip,
                //        userInstallation.Phone,
                //        userInstallation.Email
                //    );

                // Move the building of the message to the backend
                int alertStatus = (alertNotificationCommand is AlertNotificationOn) ? 1 : (alertNotificationCommand is AlertNotificationOff) ? 0 : -1;

                AlertNotification alertNotification = db.GetAlertNotification(
                                                this.InstallationId,
                                                alertStatus,
                                                alertNotificationCommand.Mode.ToString(), 
                                                alertNotificationCommand.Alert
                                            );

                // TODO: Make EmailApi_Smtp.SendEmail() to accept a list of recipients
                //       and get rid of the foreach here.
                if (!alertNotification.Message.Equals(""))
                {
                    foreach (NotificationRecipient recipient in recipients)
                    {
                        // 1.0.0.17
                        EmailApi_Smtp.SendEmail(
                            //subject: subject,
                            //body: message,
                            subject: alertNotification.Subject,
                            body: alertNotification.Message,
                            addressTo: recipient.EmailAddress1,
                            isBodyHtml: true
                        );

                    } // foreach(NotificationRecipient recipient)
                }

            } // using (DbContextPentair db)

        } // SendEmailNotifications()

        #endregion Implementation

    } // class WebSocketClient_THW
}
