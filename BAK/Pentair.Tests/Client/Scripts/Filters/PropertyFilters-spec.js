﻿describe('Property Filters', function () {
    beforeEach(module('PentairWebClient'));

    describe('Circuits By Name', function () {
        var mockObjects = SpecHelper.mocks.AllTypes;
        it('should return 1 item when matches are found"', function () {
            inject(function (objectsByNameFilter) {
                var result = objectsByNameFilter(mockObjects, 'CSpaRemote');
                expect(result).toEqual([SpecHelper.mocks.Settings[1]]);
                expect(result.length).toEqual(1);
            });

        });

        it('should return no items when no matches are found', function () {
            inject(function (objectsByNameFilter) {
                var result = objectsByNameFilter(mockObjects, 'INVALID');
                expect(result).toEqual([]);
                expect(result.length).toEqual(0);
            });
        });

        it('should return all circuits when undefined arguments are passed"', function () {
            inject(function (objectsByNameFilter) {
                var result = objectsByNameFilter(mockObjects, undefined);
                expect(result).toEqual(mockObjects);
            });
        });

    });

    describe('Circuits By Section', function () {
        var mockObjects = SpecHelper.mocks.AllTypes;

        it('should return all(2) of the Feature items when matches are found"', function () {
            inject(function (objectsBySectionFilter) {
                var result = objectsBySectionFilter(mockObjects, 'f');
                expect(result).toEqual(SpecHelper.mocks.Features);
                expect(result.length).toEqual(2);
            });

        });

        it('should return all(2) of the Light items when matches are found"', function () {
            inject(function (objectsBySectionFilter) {
                var result = objectsBySectionFilter(mockObjects, 'l');
                expect(result).toEqual(SpecHelper.mocks.Lights.concat(SpecHelper.mocks.ColorLights));
                expect(result.length).toEqual(5);
            });

        });

        it('should return all(2) of the Setting items when matches are found"', function () {
            inject(function (objectsBySectionFilter) {
                var result = objectsBySectionFilter(mockObjects, 's');
                expect(result).toEqual(SpecHelper.mocks.Settings);
                expect(result.length).toEqual(2);
            });
        });


        it('should return no items when no matches are found"', function () {
            inject(function (objectsBySectionFilter) {
                var result = objectsBySectionFilter(mockObjects, 'zx');
                expect(result).toEqual([]);
                expect(result.length).toEqual(0);
            });
        });

        it('should return all circuits when undefined arguments are passed"', function () {
            inject(function (objectsBySectionFilter) {
                var result = objectsBySectionFilter(mockObjects, undefined);
                expect(result).toEqual(mockObjects);
            });
        });

    });


    describe('translateTemperature', function () {

        var _translateTemperatureFilter;
        var _translate;
        describe('preferred unit of measurement is not English', function () {
            beforeEach(inject(function (translateTemperatureFilter, $translate) {
                _translateTemperatureFilter = translateTemperatureFilter;
                _translate = $translate;
            }));

            it('should convert value to degree fahrenheit when provided a celsius temp"', function () {
                var result = _translateTemperatureFilter("24", 'METRIC');
                expect(result.TEMPERATURE).toEqual('24°');
                expect(result.UNIT).toEqual('C');
            });


            it('should return metric values value if unsupported UOM is provided', function () {
                var result = _translateTemperatureFilter("75", '&');
                expect(result.TEMPERATURE).toEqual('75°');
                expect(result.UNIT).toEqual('C');
            });

            it('should return metric values when no UOM is provided', function () {
                var result = _translateTemperatureFilter("79");
                expect(result.TEMPERATURE).toEqual('79°');
                expect(result.UNIT).toEqual('C');
            });

            it('should work with integers as well', function () {
                var result = _translateTemperatureFilter(26, 'METRIC');
                expect(result.TEMPERATURE).toEqual('26°');
                expect(result.UNIT).toEqual('C');
            });

            it('should work ignore all strings', function () {
                var result = _translateTemperatureFilter("2iii6*F", 'METRIC');
                expect(result.TEMPERATURE).toEqual('26°');
                expect(result.UNIT).toEqual('C');
            });
        });

        describe('preferred unit of measurement is English', function () {
            beforeEach(inject(function (translateTemperatureFilter, $translate) {
                _translateTemperatureFilter = translateTemperatureFilter;
            }));

            it('should convert value to degree celsius when provided a fahrenheit temp and preferred lang is not english', function () {
                var result = _translateTemperatureFilter("75", 'ENGLISH');
                expect(result.TEMPERATURE).toEqual('167°');
                expect(result.UNIT).toEqual('F');
            });

            it('should work with integers as well', function () {
                var result = _translateTemperatureFilter(78, 'ENGLISH');
                expect(result.TEMPERATURE).toEqual('172°');
                expect(result.UNIT).toEqual('F');
            });
        });

    });

    describe('convert number to string', function () {

        it('should return One when passed 1', function () {
            inject(function (intToWordFilter) {
                var result = intToWordFilter(1);
                expect(result).toEqual("One");
            });
        });
        it('should return Two when passed 2', function () {
            inject(function (intToWordFilter) {
                var result = intToWordFilter(2);
                expect(result).toEqual("Two");
            });
        });
        it('should return Three when passed 3', function () {
            inject(function (intToWordFilter) {
                var result = intToWordFilter(3);
                expect(result).toEqual("Three");
            });
        });
        it('should return Four when passed 4', function () {
            inject(function (intToWordFilter) {
                var result = intToWordFilter(4);
                expect(result).toEqual("Four");
            });
        });
        it('should return Five when passed 5', function () {
            inject(function (intToWordFilter) {
                var result = intToWordFilter(5);
                expect(result).toEqual("Five");
            });
        });
        it('should return Six when passed 6', function () {
            inject(function (intToWordFilter) {
                var result = intToWordFilter(6);
                expect(result).toEqual("Six");
            });
        });
        it('should return Seven when passed 7', function () {
            inject(function (intToWordFilter) {
                var result = intToWordFilter(7);
                expect(result).toEqual("Seven");
            });
        });
        it('should return Eight when passed 8', function () {
            inject(function (intToWordFilter) {
                var result = intToWordFilter(8);
                expect(result).toEqual("Eight");
            });
        });
        it('should return Nine when passed 9', function () {
            inject(function (intToWordFilter) {
                var result = intToWordFilter(9);
                expect(result).toEqual("Nine");
            });
        });
        it('should return Ten when passed 10', function () {
            inject(function (intToWordFilter) {
                var result = intToWordFilter(10);
                expect(result).toEqual("Ten");
            });
        });
        it('should return One hundred and one when passed 101', function () {
            inject(function (intToWordFilter) {
                var result = intToWordFilter(101);
                expect(result).toEqual("One hundred and one");
            });
        });
        it('should return Lots when passed 1001', function () {
            inject(function (intToWordFilter) {
                var result = intToWordFilter(1001);
                expect(result).toEqual("Lots");
            });
        });

    });

    describe('Connection Status', function () {

        it('should return Connecting when passed 0', function () {
            inject(function (connectionStatusFilter) {
                var result = connectionStatusFilter(0);
                expect(result).toEqual('Connecting');
            });
        });
        it('should return Connected when passed 1', function () {
            inject(function (connectionStatusFilter) {
                var result = connectionStatusFilter(1);
                expect(result).toEqual('Connected');
            });
        });
        it('should return Closing when passed 2', function () {
            inject(function (connectionStatusFilter) {
                var result = connectionStatusFilter(2);
                expect(result).toEqual('Closing');
            });
        });
        it('should return a message when passed 3', function () {
            inject(function (connectionStatusFilter) {
                var result = connectionStatusFilter(3);
                expect(result).toEqual('Closed');
            });
        });
        it('should return nothing when passed 4', function () {
            inject(function (connectionStatusFilter) {
                var result = connectionStatusFilter(4);
                expect(result).toEqual('');
            });
        });
        it('should return nothing when passed null or undefined', function () {
            inject(function (connectionStatusFilter) {
                var result = connectionStatusFilter(null);
                expect(result).toEqual('');

                result = connectionStatusFilter(undefined);
                expect(result).toEqual('');
            });
        });

    });

    describe('prgrammable Circuits', function () {
        it('should return 4 circuits that contain "e" in the showmnu', function () {
            inject(function (programmableCircuitsFilter) {
                var allProgrammable = true;
                var result = programmableCircuitsFilter(SpecHelper.mocks.AllTypes);

                angular.forEach(result, function (circuit) {
                    if (circuit.OBJTYP != 'BODY') {
                        if (circuit.SHOMNU && circuit.SHOMNU.indexOf('e') < 0)
                            allProgrammable = false;
                    }

                });
                expect(allProgrammable).toBe(true);
                expect(result.length).toBe(4);
            });
        });

        it('should return nothing when passed null or undefined', function () {
            inject(function (programmableCircuitsFilter) {
                var allProgrammable = true;
                var result = programmableCircuitsFilter(null);

                angular.forEach(result, function (circuit) {
                    if (circuit.OBJTYP != 'BODY') {
                        if (circuit.SHOMNU && circuit.SHOMNU.indexOf('e') < 0)
                            allProgrammable = false;
                    }

                });
                expect(allProgrammable).toBe(true);
                expect(result).toBe(null);
            });
        });

        it('should return nothing when passed an empty collection', function () {
            inject(function (programmableCircuitsFilter) {
                var allProgrammable = true;
                var result = programmableCircuitsFilter();

                angular.forEach(result, function (circuit) {
                    if (circuit.OBJTYP != 'BODY') {
                        if (circuit.SHOMNU && circuit.SHOMNU.indexOf('e') < 0)
                            allProgrammable = false;
                    }

                });
                expect(allProgrammable).toBe(true);
                expect(result).not.toBeDefined(0);
            });
        });
    });


    describe('scheduleDays', function () {

        it('Should retun readable abbreviations for Sat, Sun and Thur', function () {
            inject(function (scheduleDaysFilter) {
                var result = scheduleDaysFilter('AUR');
                expect(result).toBe('Thu, Sat, Sun');
            });
        });

        it('Should retun all when all days are provided', function () {
            inject(function (scheduleDaysFilter) {
                var result = scheduleDaysFilter('AURMTWF');
                expect(result).toBe('Every Day');
            });
        });

        it('Should retun all when all days are provided', function () {
            inject(function (scheduleDaysFilter) {
                var result = scheduleDaysFilter('A1');
                expect(result).toBe('Sat');
            });
        });
        it('Should retun empty string when falsy is provided', function () {
            inject(function (scheduleDaysFilter) {
                var result = scheduleDaysFilter(null);
                expect(result).toBe('');

                var result = scheduleDaysFilter('');
                expect(result).toBe('');

                var result = scheduleDaysFilter();
                expect(result).toBe('');
            });
        });
    });

    describe('scheduleType', function () {

        it('Should retun "One Time Only" when 1 is found in the string', function () {
            inject(function (scheduleTypeFilter) {
                var result = scheduleTypeFilter('ON');
                expect(result).toBe('One Time Only');
            });
        });

        it('Should retun "Weekly" when 1 is not found in the string', function () {
            inject(function (scheduleTypeFilter) {
                var result = scheduleTypeFilter('OFF');
                expect(result).toBe('Weekly');
            });
        });

        it('Should retun empty string when falsy is provided', function () {
            inject(function (scheduleTypeFilter) {
                var result = scheduleTypeFilter(null);
                expect(result).toBe('');

                var result = scheduleTypeFilter('');
                expect(result).toBe('');

                var result = scheduleTypeFilter();
                expect(result).toBe('');
            });
        });
    });

    describe('scheduleType', function () {

        it('Should return 12:55PM  when 12,55,00 is provided', function () {
            inject(function (translateTimeFilter) {
                var result = translateTimeFilter('12,55,00');
                expect(result).toBe('12:55pm');
            });
        });
        it('Should return 10:23PM  when 22,23,00 is provided', function () {
            inject(function (translateTimeFilter) {
                var result = translateTimeFilter('22,23,00');
                expect(result).toBe('10:23pm');
            });
        });

        it('Should return 10:23AM  when 10,23,00 is provided', function () {
            inject(function (translateTimeFilter) {
                var result = translateTimeFilter('10,23,00');
                expect(result).toBe('10:23am');
            });
        });
        it('Should retun NaN when falsy is provided', function () {
            inject(function (translateTimeFilter) {
                var result = translateTimeFilter(null);
                expect(result).toBe('--');

                var result = translateTimeFilter('');
                expect(result).toBe('--');

                var result = translateTimeFilter();
                expect(result).toBe('--');
            });
        });
    });
});

