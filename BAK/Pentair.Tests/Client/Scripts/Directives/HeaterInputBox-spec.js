﻿describe('heaterInputBox', function () {
    var $scope, $compile;
    var elm;
    var body;
    var baseTemplate = '<heater-input-box heater-model="heaterModel"  min-value="40" max-value="104" body-object="body" on-text-input="SetValue(obj,prop,value)"></heater-input-box>';
    var inputBox;
    var elmScope;
    beforeEach(module('PentairWebClient'));

    beforeEach(inject(function ($rootScope, _$compile_, _PropertyModel_) {
        // Get an isolated scope
        $scope = $rootScope.$new();
        $compile = _$compile_;
        $scope.heaterModel = "99";
        _PropertyModel_.Objects["_5451"] = { MODE: "METRIC" };

        $scope.body = SpecHelper.mocks.Bodies[0];
        elm = SpecHelper.createDirective($compile, baseTemplate, $scope);
        inputBox = $(elm[0].lastChild);
        elmScope = elm.isolateScope();
    }));

    describe('validate values', function () {

        beforeEach(function () {
            spyOn(elmScope, 'onTextInput');
        });
        it('should send message to set temp to 100 when 100 is sent', function () {
            elmScope.temp = "100";
            elmScope.onExit();
            var args = { obj: 'BPool', prop: 'LOTMP', value: 100 };
            expect(elmScope.onTextInput).toHaveBeenCalledWith(args);
        });

        it('should send message to set temp to 104 when 105 is sent', function () {
            elmScope.temp = "105";
            elmScope.onExit();
            var args = { obj: 'BPool', prop: 'LOTMP', value: 104 };
            expect(elmScope.onTextInput).toHaveBeenCalledWith(args);
        });

        it('should send message to set temp to 40 when 39 is sent', function () {
            elmScope.temp = "39";
            elmScope.onExit();
            var args = { obj: 'BPool', prop: 'LOTMP', value: 40 };
            expect(elmScope.onTextInput).toHaveBeenCalledWith(args);
        });
        it('should not send message if value has not changed', function () {
            elm.triggerHandler('click');
            elmScope.onExit();
            expect(elmScope.onTextInput).not.toHaveBeenCalled();
        });
    });

    describe('show/hide input box', function () {
        it('should display input box on click', function () {

            elm = SpecHelper.createDirective($compile, baseTemplate, $scope);
            expect(inputBox).toHaveClass('ng-hide');

            elm.triggerHandler('click');

            //todo update so a redefine is not required
            inputBox = $(elm[0].lastChild);

            expect(inputBox).not.toHaveClass('ng-hide');

            expect(elm[0].children[0].textContent).toEqual('99°');

        });


        it('should invoke OnExit on blur', function () {
            spyOn(elmScope, 'onExit');

            angular.element(elm[0].lastChild).triggerHandler('blur');
            expect(elmScope.onExit).toHaveBeenCalled();
        });

        xit('should invoke OnExit on enter keyPress', function () {
            spyOn(elmScope, 'onExit');

            var enterKeyEvent = jQuery.Event("keypress");
            enterKeyEvent.which = 13;

            angular.element(elm[0].lastChild).triggerHandler(enterKeyEvent);
            expect(elmScope.onExit).toHaveBeenCalled();
        });


        it('should invoke hide element when OnExit is called', function () {
            spyOn(elmScope, 'onTextInput');
            elmScope.temp = "100";
            elmScope.onExit();
            expect(inputBox).toHaveClass('ng-hide');
        });

    });
    describe('when model changes in the background', function () {
        it('should set TEMP to 100 when model is set to 100', function () {
            $scope.heaterModel = "100";
            $scope.$apply();
            expect(elmScope.temp).toBe(100);
        });
        it('should set TEMP to 45 when model is set to 45^F', function () {
            $scope.heaterModel = "45^F";
            $scope.$apply();
            expect(elmScope.temp).toBe(45);
        });
    });

});
