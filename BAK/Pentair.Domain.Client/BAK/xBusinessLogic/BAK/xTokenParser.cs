﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Pentair.Domain.Client.Helpers.Entity;
using Pentair.Domain.Client.Infrastructure;

namespace Pentair.Domain.Client.BusinessLogic
{
    /* TODO 2b deleted 2017-12-09
    /// <summary>
    /// Parser to provide a list of filter search term objects.
    /// </summary>
    enum Op
    {
        Equals,
        NotEquals,
        Contains,
    }

    enum Co
    {
        None,
        And,
        Or
    }

    /// <summary>
    /// Process a dynamic linq query
    /// </summary>
    public class ProcessQueryString
    {
        private List<ObjectView> _queryResult;
        private readonly ISimulatorContext _data;
        private readonly int _installation;
        private readonly TokenParser _parser;

        public ProcessQueryString(string query, ISimulatorContext data, int installation)
        {
            _parser = new TokenParser(query);
            _data = data;
            _installation = installation;
        }

        public List<string> Query()
        {
            _queryResult = new List<ObjectView>();
            RetrieveResults(_parser.ParseTree);
            return (from i in _queryResult select i.OBJNAM).ToList();
        }
        private void RetrieveResults(List<Filter> request)
        {
            // Process the items from right to left
            for (var i = request.Count(); --i >= 0; )
            {
                if (null != request[i].Child)
                {
                    RetrieveResults(request[i].Child);
                }

                // create the request
                var filter = new List<WhereFilter>()
                {
                    new WhereFilter()
                    {
                        Property = request[i].Property,
                        Operation = request[i].Operator,
                        Value =  request[i].Value
                    }
                };

                var whereClause = ExpressionBuilder.GetExpression<ObjectView>(filter).Compile();
                switch (request[i].Conditional)
                {
                    case Co.And:
                        request[i].Result = _data.ObjectViews.Where(whereClause).ToList();
                        var partialResult =
                            request[i].Result.Where(x => x.INSTALLATION == _installation && !x.Deleted).ToList();
                        var combinedResult = _queryResult.Where(a => partialResult.Where(x => x.OBJNAM == a.OBJNAM).ToList().Count > 0).ToList();
                        _queryResult.Clear();
                        _queryResult = combinedResult;
                        break;
                    default:
                        request[i].Result = _data.ObjectViews.Where(whereClause).ToList();
                        break;
                }

                _queryResult.AddRange(request[i].Result.Where(x => x.INSTALLATION == _installation && !x.Deleted).ToList());
            }
        }

    } // class ProcessQueryString

    /// <summary>
    /// Parser to provide a list of filter search term objects.
    /// </summary>
    internal class TokenParser
    {
        #region Private Properties
        readonly string[] _operators = { "&", "|", "=", "!", "*", "(", ")" };
        #endregion

        #region Public Properties
        public List<Filter> ParseTree { get; private set; }
        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="tokens">String containing conditional tokens</param>
        public TokenParser(string tokens)
        {
            ParseTree = new List<Filter>();
            NextToken(tokens, ParseTree);
        }

        // Parse from right to left
        private string NextToken(string token, List<Filter> parent)
        {
            int state = 0;
            int index = 0;
            int listItem = 0;
            bool Pop = false;
            bool Skip = false;
            parent.Insert(listItem, new Filter());
            var filter = parent[listItem++];
            while (token.Length > index)
            {
                var p = token[index++];
                switch (p)
                {
                    case '(':
                        filter.Child = new List<Filter>();
                        token = NextToken(token.Substring(index), filter.Child);
                        index = 0;
                        Skip = true;
                        break;
                    case ')':
                        Pop = true;
                        break;
                    case '&':
                        if (Pop)
                        {
                            return token.Substring(--index);
                        }
                        filter.Conditional = Co.And;
                        if (Skip)
                        {
                            Skip = false;
                        }
                        else
                        {
                            parent.Insert(listItem, new Filter());
                            filter = parent[listItem++];
                            state = 0;
                        }
                        break;
                    case '|':
                        if (Pop)
                        {
                            return token.Substring(--index);
                        }
                        filter.Conditional = Co.Or;
                        if (Skip)
                        {
                            Skip = false;
                        }
                        else
                        {
                            parent.Insert(listItem, new Filter());
                            filter = parent[listItem++];
                            state = 0;
                        }
                        break;
                    case '!':
                        ++state;
                        ++index;
                        filter.Operator = Op.NotEquals;
                        break;
                    case '=':
                        filter.Operator = Op.Equals;
                        ++state;
                        break;
                    case '*':
                        filter.Operator = Op.Contains;
                        ++state;
                        break;
                    case ' ':
                        break;
                    default:
                        if (0 == state)
                        {
                            filter.Property = filter.Property + p;
                        }
                        else
                        {
                            filter.Value = filter.Value + p;
                        }
                        break;
                }
            }
            return token.Substring(index);
        }

        #endregion

        #region Private Methods
        // parse the input string for a vaild operator and return the operator or a null.
        private string IsOperator(string value)
        {
            return _operators.FirstOrDefault(value.Contains);
        }

        #endregion
    
    } // class TokenParser

    internal class ExpressionBuilder
    {
        private static readonly MethodInfo ContainsMethod = typeof(string).GetMethod("Contains");

        public static Expression<Func<T, bool>> GetExpression<T>(IList<WhereFilter> filters)
        {
            if (filters.Count == 0)
                return null;

            ParameterExpression param = Expression.Parameter(typeof(ObjectView), "t");
            Expression exp = null;
            while (filters.Count > 1)
            {
                var f1 = filters[0];
                var f2 = filters[1];

                if (exp == null)
                    exp = GetExpression<T>(param, filters[0], filters[1]);
                else
                    exp = Expression.AndAlso(exp, GetExpression<T>(param, filters[0], filters[1]));

                filters.Remove(f1);
                filters.Remove(f2);
            }
            if (filters.Count > 0)
            {
                if (null == exp)
                {
                    exp = GetExpression<T>(param, filters[0]);
                }
                else
                {
                    exp = Expression.AndAlso(exp, GetExpression<T>(param, filters[0]));
                    filters.RemoveAt(0);
                }
            }
            return Expression.Lambda<Func<T, bool>>(exp, param);
        }

        static Expression GetExpression<T>(ParameterExpression param, WhereFilter filter)
        {
            MemberExpression member = Expression.Property(param, filter.Property);
            ConstantExpression constant = Expression.Constant(filter.Value);

            switch (filter.Operation)
            {
                case Op.Equals:
                    return Expression.Equal(member, constant);

                case Op.Contains:
                    return Expression.Call(member, ContainsMethod, constant);
            }

            return null;
        }

        static BinaryExpression GetExpression<T>
            (ParameterExpression param, WhereFilter filter1, WhereFilter filter2)
        {
            Expression bin1 = GetExpression<T>(param, filter1);
            Expression bin2 = GetExpression<T>(param, filter2);

            return Expression.AndAlso(bin1, bin2);
        }
    
    } // class ExpressionBuilder

    /// <summary>
    /// Dynamic Linq Where Clause Filter
    /// </summary>
    internal class WhereFilter
    {
        public string Property { get; set; }
        public Op Operation { get; set; }
        public object Value { get; set; }
    }

    /// <summary>
    /// Helper class provides a search term
    /// </summary>
    internal class Filter
    {
        public string Property { get; set; }
        public string Value { get; set; }
        public Op Operator { get; set; }
        public Co Conditional { get; set; }
        public List<Filter> Child { get; set; }
        public int Parent { get; set; }
        public List<ObjectView> Result { get; set; }
    }
    */
}
