﻿'use strict';
var PWC = PWC || {};
PWC.directive('autoComplete', [function () {
    return {
        restrict: 'A',
        replace: true,
        scope: {
            selection: '=',
            suggestions: '='
        },
        template: "<div style='height: 27px; position:relative; border-collapse:collapse;' ng-blur='matchingSuggestions = []'> " +
        "<input style='width:100%; height:26px; padding:1px 1px 1px 6px' type='text' ng-model=selection ng-keydown='checkKeyDown($event)' ng-focus='search()' ng-blur='matchingSuggestions = []' class='form-control' ng-change='search()'/>" +
        "<ul ng-show='matchingSuggestions.length' id='suggestions' class='dropdown' style='line-height:1; max-height:240px; overflow-y:scroll; font-size:18px;' >" +
        "<li ng-repeat='suggestion in matchingSuggestions'>" +
        "<a class='dropdown-item' ng-mousedown='addToModel($index)' style='display: block;'>{{suggestion}}</a></li></ul></div>",
        link: function (scope) {

            scope.matchingSuggestions = [];
            scope.suggestions = scope.suggestions || [];
            scope.selectedIndex = -1;

            scope.search = function () {
                scope.selectedIndex = -1;
                scope.matchingSuggestions = [];
                angular.forEach(scope.suggestions, function (suggestion, key) {
                    if (scope.selection.length < 1) {
                        scope.matchingSuggestions.push(suggestion);
                    } else {
                        if (scope.selection && suggestion.toLowerCase().startsWith(scope.selection.toLowerCase())) {
                            scope.matchingSuggestions.push(suggestion);
                        }
                    }
                });
            }

            scope.addToModel = function (index) {
                if (index > -1) {
                    scope.selection = angular.copy(scope.matchingSuggestions[index]);
                    scope.selectedIndex = index;
                }
                 scope.matchingSuggestions = [];
            }

            scope.checkKeyDown = function (event) {
                if (event.keyCode === 40) {
                    event.preventDefault();
                    if (scope.selectedIndex + 1 !== scope.matchingSuggestions.length) {
                        scope.selectedIndex++;
                    }
                }
                else if (event.keyCode === 38) {
                    event.preventDefault();
                    if (scope.selectedIndex - 1 !== -1) {
                        scope.selectedIndex--;
                    }
                }
                else if (event.keyCode === 13) {
                    if (scope.selectedIndex > -1) {
                        scope.addToModel(scope.selectedIndex);
                    } else {
                        if (scope.matchingSuggestions.length > 0) {
                            scope.addToModel(0);
                        }
                    }
                }
            }
        }
    }
}]);