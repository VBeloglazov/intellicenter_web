﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;

using Newtonsoft.Json;

namespace Pentair.Web.Api
{
    public class AlertNotificationOn : AlertNotificationCommand
    {
        public AlertNotificationOn(
            string messageID,
            int category,
            string objName,
            string objType,
            string parent,
            string alert,
            long timeStamp
            )
            : base(CommandString.AlertOn, messageID, category, objName, objType, parent, alert, timeStamp)
        {
        } // AlertNotificationOn()

        /// <summary>
        /// Always returns an object, valid or not. If invalid, check the DeserializationError property value.
        /// </summary>
        public static new AlertNotificationOn Deserialize(
            string serializedMessage)
        {
            AlertNotificationOn command = null;

            try
            {
                command = JsonConvert.DeserializeObject<AlertNotificationOn>(serializedMessage);
                command.IsValid = true;
            }
            catch (Exception e)
            {
                // Deserialization as AlertNotificationOn failed

                StringBuilder sbError = new StringBuilder();

                sbError.Append("Cannot deserialize as AlertNotificationOn. " + e.Message);

                // Try to get at least the base message data
                AlertNotificationCommand commandBase = AlertNotificationCommand.Deserialize(serializedMessage);

                if (!commandBase.IsValid)
                {
                    sbError.Append("\n" + commandBase.DeserializationError);
                }

                command = new AlertNotificationOn(
                    messageID: commandBase.MessageID,
                    category: (int)commandBase.Mode,
                    objName: commandBase.ObjName,
                    objType: commandBase.ObjType,
                    parent: commandBase.Parent,
                    alert: commandBase.Alert,
                    timeStamp: 0L)
                {
                    IsValid = false,
                    DeserializationError = sbError.ToString()
                };
            }

            return command;

        } // Deserialize()

    } // class AlertNotificationOn
}