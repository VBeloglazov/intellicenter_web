﻿using Machine.Fakes;
using Machine.Specifications;
using Pentair.Desktop.Simulator.Services;
using Pentair.Domain.Client.Entity;
using Pentair.Domain.Client.Infrastructure;

namespace Pentair.Tests.Simulator.Desktop
{
    [Subject("ClientSocket")]
    public class WhenAskedForObjects
    {
        #region Database Records

        private static readonly ISimulatorContext SimulatorFakeContext = new FakeContext()
        {
            ControlObjects =
            {
                new ControlObject()
                {
                    INSTALLATION = 1,
                    OBJNAM = "C1355",
                    //OBJTYP = "BODY",
                    //SUBTYP = "BODY",
                    //SNAME = "Pool Circuit",
                    //RECENT = "0",
                    //FREQ = 0,
                    //CUSTOM = 0,
                    //LISTORD = 1,
                    //SHOMNU = "s",
                    //STATUS = "ON",
                    //TIMOUT = "0",
                    //TIME = "0",
                    //SINDEX = 0,
                    //COUNT = 0
                },
                new ControlObject()
                {
                    INSTALLATION = 1,
                    OBJNAM = "C1356",
                    //OBJTYP = "BODY",
                    //SUBTYP = "SPA",
                    //SNAME = "Spa Circuit",
                    //RECENT = "0",
                    //FREQ = 0,
                    //CUSTOM = 0,
                    //LISTORD = 1,
                    //SHOMNU = "s",
                    //STATUS = "OFF",
                    //TIMOUT = "0",
                    //TIME = "0",
                    //SINDEX = 0,
                    //COUNT = 0
                },
                new ControlObject()
                {
                    INSTALLATION = 1,
                    OBJNAM = "C5433",
                    //OBJTYP = "CIRCUIT",
                    //SUBTYP = "INTELLI",
                    //SNAME = "Night Spa",
                    //RECENT = "0",
                    //FREQ = 0,
                    //CUSTOM = 0,
                    //LISTORD = 1,
                    //SHOMNU = "l",
                    //STATUS = "OFF",
                    //TIMOUT = "0",
                    //TIME = "0",
                    //SINDEX = 0,
                    //COUNT = 0
                },
                new ControlObject()
                {
                    INSTALLATION = 1,
                    OBJNAM = "C5435",
                    //OBJTYP = "CIRCUIT",
                    //SUBTYP = "MAGIC2",
                    //SNAME = "Night Laminar",
                    //RECENT = "0",
                    //FREQ = 0,
                    //CUSTOM = 0,
                    //LISTORD = 2,
                    //SHOMNU = "l",
                    //STATUS = "OFF",
                    //TIMOUT = "0",
                    //TIME = "0",
                    //SINDEX = 0,
                    //COUNT = 0
                },
                new ControlObject()
                {
                    INSTALLATION = 1,
                    OBJNAM = "C5436",
                    //OBJTYP = "CIRCUIT",
                    //SUBTYP = "GENERIC",
                    //SNAME = "Water Fall",
                    //RECENT = "0",
                    //FREQ = 0,
                    //CUSTOM = 0,
                    //LISTORD = 1,
                    //SHOMNU = "f",
                    //STATUS = "OFF",
                    //TIMOUT = "0",
                    //TIME = "0",
                    //SINDEX = 0,
                    //COUNT = 0
                },
                new ControlObject()
                {
                    INSTALLATION = 1,
                    OBJNAM = "C5437",
                    //OBJTYP = "CIRCUIT",
                    //SUBTYP = "FRZ",
                    //SNAME = "Freeze Mode",
                    //RECENT = "0",
                    //FREQ = 0,
                    //CUSTOM = 0,
                    //LISTORD = 1,
                    //SHOMNU = "s",
                    //STATUS = "ON",
                    //TIMOUT = "0",
                    //TIME = "0",
                    //SINDEX = 0,
                    //COUNT = 0
                }
            }
        };
        #endregion
        public class WhenIOpenAClientSocketToAValidAddress : WithSubject<ClientSocket>
        {
            //private static ClientSocket _clientSocket;
            //private static Property _property;
            //private static ConnectionStatus _response;

            //private Establish context = () =>
            //{
            //    _clientSocket = new ClientSocket(new MessageProcessor(OcpFakeContext as FakeContext));
            //    _property = new Property();
            //};

            //private Because of = () =>
            //{
            //    _clientSocket.OpenConnection(_property);
            //    _response = _clientSocket.Status;
            //};

            //private It should_be_in_open_state_connecting_to_echoserver =
            //        () => _response.ShouldBeTheSameAs(WebSocketState.Open);
        }

    }
}