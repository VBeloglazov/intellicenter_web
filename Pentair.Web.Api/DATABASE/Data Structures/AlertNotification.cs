﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pentair.Web.Api
{
    // Alert email message
    public class AlertNotification
    {
        // subject and body of the notification
        public string Subject { get; set; }
        public string Message { get; set; }

    } // class AlertNotification
}