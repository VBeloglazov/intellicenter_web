Generating migrations:
There is some custom work that needs to be done to the resulting migration files after generating them with Entity Framework Version 6.1.  The simple way to do this is to create new database tables each time a change is made as follows:

1) Delete the two databases
    a) Client
    b) Simulator

2) Delete the migration files
    a) Pentair.Domain.Client/ClientMigrations/ all files ACCEPT Configuration1.cs
    b) Pentair.Domain.Client/UserMigrations/ all files ACCEPT Configuration2.cs
    c) Pentair.Domain.Client/SimulatorMigrations/ all files ACCEPT Configuration3.cs

3) Generate the migration file in the Package Manager Console as follows:
    a) add-migration -n <name it> -configuration Pentair.Domain.Client.ClientMigrations.Configuration1 -ProjectName Pentair.Domain.Client -StartUpProjectName Pentair.Web.Api
    b) add-migration -n <name it> -configuration Pentair.Domain.Client.UserMigrations.Configuration2 -ProjectName Pentair.Domain.Client -StartUpProjectName Pentair.Web.Api
    c) add-migration -n <name it> -configuration Pentair.Domain.Client.SimulatorMigrations.Configuration3 -ProjectName Pentair.Domain.Client -StartUpProjectName Pentair.Desktop.Simulator

Running the three scripts in step three above will create three new sets of files replacing the files created in step two above.  The new files created in the Pentair.Domain.Client/ClientMigrations and the Pentair.Domain.SimulatorMigrations require further manual modifications to create default values for certain fields.  In these two files modify each line containing the field CreateDateTime and UpdateDateTime by appending the following:
defaultValueSql: "GETUTCDATE()" to create default values for these fields.

So:
    CreateDateTime = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
    UpdateDateTime = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
Becomes:
    CreateDateTime = c.DateTime(nullable: false, precision: 7, storeType: "datetime2", defaultValueSql: "GETUTCDATE()"),
    UpdateDateTime = c.DateTime(nullable: false, precision: 7, storeType: "datetime2", defaultValueSql: "GETUTCDATE()"),

DO NOT modify any other DateTime fields.

Next delete the table ObjectView in the file created for hte Pentair.Domain.Client.ClientMigrations/ file that was created above.  The ObjectView is a View created in the file Pentair.Domain.Client/Repository/CreateDatabase.cs when Entity Framework creates the database.  Also delete the line DropTable("dbo.ObjectViews") under the Down() method at the bottom of each file.

NOTE: This view needs to be kept in sync with any changes to any new object types or parameter types on Pentairs Database Specification.

