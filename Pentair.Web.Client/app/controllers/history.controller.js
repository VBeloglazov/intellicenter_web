﻿'use strict';
var PWC = PWC || {};
PWC.controller('HistoryController', [
    '$rootScope', '$scope', '$location', '$filter', 'PropertyService', 'PropertyModel', 'LocalStorageService', 'MessageProcessor',
    function($rootScope, $scope, $location, $filter, propertyService, propertyModel, LocalStorageService, MessageProcessor) {
        $scope.property = propertyModel;
        $scope.property.History = [];
        $scope.AllBodies = [];
        $scope.propertyService = propertyService;
        $scope.intervalActive = 'today';        
        $scope.loadingHistory = true;
        $scope.currentTab = "None";
        $scope.startPeriod = angular.copy($rootScope.currentDate).toDate(); //angular.copy($rootScope.currentDate).subtract(14, 'days').toDate();    // the last 14 days
        $scope.stopPeriod = angular.copy($rootScope.currentDate).add(1, 'days').toDate(); //angular.copy($rootScope.currentDate).toDate();

        $scope.startPeriod2 = angular.copy($rootScope.currentDate).subtract(14, 'days').toDate();   // the last 14 days
        $scope.stopPeriod2 = angular.copy($rootScope.currentDate).toDate();

        $scope.minStartDate = moment().subtract(365,'days').toDate();   // start up to 1 year back
        $scope.minStopDate = moment().add(365, 'days').toDate();        // stop up to tomorrow.
        // used to replot data
        $scope.starDatetHistory = 0;
        $scope.stopDateHistory = 0;
        $scope.forceRetrieve = false


        // selected plots
        $scope.orpOn = true;
        $scope.phOn = true;
        $scope.poolTempOn = true;
        $scope.spaTempOn = true;
        $scope.solarTempOn = true;
        $scope.airTempOn = true;
        $scope.zoomOn = false;
        $scope.zeroOn = false;   // includes zeros in ph, Orp
        $scope.freezeOn = false; // 0 or 32 degrees for temperature
        

        $scope.viewSelection = "Temperature";   // "view selection"
        $scope.periodInitialized = false;


        $scope.start_opened = false;
        $scope.stop_opened = false;

        $scope.displayTypes = [{
            SNAME: 'Temperature',//'Temperature / Usage',
            SHORT: 'temp'
        }, {
            SNAME: 'Chemistry',//'Chemistry / Feeds',
            SHORT: 'chem'
        }];

        //$scope.usageData = usageData;
        //$scope.usageData2 = usageData2;
        

        //NOTE comment this out when live data is ready
        //$rootScope.currentDate = moment('2016-04-30 00:00:00');
        // $rootScope.currentDate = moment('2018-02-26 00:00:00');
        var tempStart = angular.copy($rootScope.currentDate);
        if(tempStart === undefined || tempStart === null) {
            tempStart = moment(new Date());
        }
        tempStart = tempStart.add(0, 'days').toDate();
        var tempEnd = angular.copy($rootScope.currentDate);
        if(tempEnd === undefined || tempEnd === null) {
            tempEnd = moment(new Date());
        }
        tempEnd = tempEnd.add(1, 'days').toDate();
        $scope.xAxis = {
            domain: [tempStart, tempEnd],
            ticks: { timeScale: d3.time.hours, timeInterval: 2 },
            temperatureFormat: getModeTemperature(), //$scope.property.mode, 
            
            // chart items
            orpOn: $scope.orpOn,
            phOn: $scope.phOn,
            poolTempOn: $scope.poolTempOn,
            spaTempOn: $scope.spaTempOn,
            solarTempOn: $scope.solarTempOn,
            airTempOn: $scope.airTempOn,
            zoomOn: $scope.zoomOn,
            zeroOn: $scope.zeroOn,
            freezeOn: $scope.freezeOn,
            
        };

        getBodies();

        init();  // process init

        // Sat, 25 Jun 2016 17:46:18 GMT to Wed, 27 Jul 2016 17:46:18 GMT
        // propertyService.GetHistory(1466876778, 1469641578);
        // get today's history data
        // 25th to 26th
        propertyService.GetHistory(moment($scope.xAxis.domain[0]).unix(), moment($scope.xAxis.domain[1]).unix()).then(function() {
            $scope.loadingHistory = false;
        });

        $scope.processViewSelection = function (param) {
            $scope.viewSelection = param;
            
        };

        $scope.isTabSelected = function (param) {
            if ($scope.viewSelection === param)
                return true;
            else
                return false;
        };

        $scope.jumpToday = function (param) {
            // $scope.intervalActive = 'daily';
            $rootScope.currentDate = moment(propertyModel.Objects._C10C.DAY, 'MM,DD,YY');
            if (param === 'today') {
                $scope.startPeriod = angular.copy($rootScope.currentDate).toDate();
                $scope.stopPeriod = angular.copy($rootScope.currentDate).add(1, 'days').toDate();
                $scope.intervalActive = 'today';
            }
            else {
                $scope.startPeriod = angular.copy($rootScope.currentDate).subtract(1, 'days').toDate();
                $scope.stopPeriod = angular.copy($rootScope.currentDate).toDate();
                $scope.intervalActive = 'yesterday';
            }

            $scope.xAxis = {
                //domain: [angular.copy($rootScope.currentDate).toDate(), angular.copy($rootScope.currentDate).add(1, 'days').toDate()],
                domain: [$scope.startPeriod, $scope.stopPeriod],
                ticks: { timeScale: d3.time.hours, timeInterval: 2 },
                temperatureFormat: getModeTemperature(),
                orpOn: $scope.orpOn,
                phOn: $scope.phOn,
                poolTempOn: $scope.poolTempOn,
                spaTempOn: $scope.spaTempOn,
                solarTempOn: $scope.solarTempOn,
                airTempOn: $scope.airTempOn,
                zoomOn: $scope.zoomOn,
                zeroOn: $scope.zeroOn,
                freezeOn: $scope.freezeOn,
            };
            $scope.loadingHistory = true;

            MessageProcessor.InitHistoryInterval();

            $scope.retrieveHistory(moment($scope.xAxis.domain[0]).unix(), moment($scope.xAxis.domain[1]).unix());
            /*
            propertyService.GetHistory(moment($scope.xAxis.domain[0]).unix(), moment($scope.xAxis.domain[1]).unix()).then(function () {
                $scope.loadingHistory = false;
            });
            */

            
        };

        

        $scope.retrieveHistory = function (start, stop) {
            
            $scope.loadingHistory = true;
            // check if dates are same then just replot save arrived data.!
            if ((start != $scope.starDatetHistory) ||
                (stop != $scope.stopDateHistory) || $scope.forceRetrieve
               ) {
                // save current days
                $scope.starDatetHistory = start;
                $scope.stopDateHistory = stop;
                $scope.forceRetrieve = false
                
                propertyService.GetHistory(start, stop).then(function () {
                    $scope.loadingHistory = false;
                });
            } else {
                // is plotting same days no need to retrieve data from server just replot data
                MessageProcessor.replotHistoryData();
                $scope.loadingHistory = false;
            }
        };


        $scope.setCurrentTab = function (name) {
            $scope.currentTab = name;
        };

        $scope.isInterval = function (value) {
            if ($scope.intervalActive === value)
                return true
            else
                return false
        };

        $scope.getSelectedInterval = function (value) {
            if ($scope.intervalActive === value)
                return 'black';
            else
                return 'white';
        };

        $scope.getSelectedBackgroundInterval = function (value) {
            if ($scope.intervalActive === value)
                return '#2678f2';
            else
                return '#91d4ce';
        };


        $scope.intervalDown = function() {
            $scope.loadingHistory = true;
            if (($scope.intervalActive === 'monthly') ||
                ($scope.intervalActive === 'lastmonth')) {
                $scope.xAxis.domain[0] = angular.copy(moment($scope.xAxis.domain[0]).subtract(1, 'weeks').toDate());
                $scope.xAxis.domain[1] = angular.copy(moment($scope.xAxis.domain[1]).subtract(1, 'weeks').toDate());
                //historyCaching();
                //return;
            }
            if (($scope.intervalActive === 'today') ||
                ($scope.intervalActive === 'yesterday')) {
                $scope.xAxis.domain[0] = angular.copy(moment($scope.xAxis.domain[0]).subtract(3, 'hours').toDate());
                $scope.xAxis.domain[1] = angular.copy(moment($scope.xAxis.domain[1]).subtract(3, 'hours').toDate());
            }
            if (($scope.intervalActive === 'weekly') ||
                ($scope.intervalActive === 'lastweek') ||
                ($scope.intervalActive === 'period')) {
                $scope.xAxis.domain[0] = angular.copy(moment($scope.xAxis.domain[0]).subtract(1, 'days').toDate());
                $scope.xAxis.domain[1] = angular.copy(moment($scope.xAxis.domain[1]).subtract(1, 'days').toDate());
            }
            $scope.retrieveHistory(moment($scope.xAxis.domain[0]).unix(), moment($scope.xAxis.domain[1]).unix());
    /*            
            propertyService.GetHistory(moment($scope.xAxis.domain[0]).unix(), moment($scope.xAxis.domain[1]).unix()).then(function() {
                $scope.loadingHistory = false;
            });
*/

            $scope.startPeriod = $scope.xAxis.domain[0];
            $scope.stopPeriod = $scope.xAxis.domain[1];

            if ($scope.intervalActive === 'period') {   // save also if current selection is period
                $scope.startPeriod2 = $scope.xAxis.domain[0];
                $scope.stopPeriod2 = $scope.xAxis.domain[1];
    }
        };

        $scope.select = function (value) {
            console.log(" user selected:", value);

            $scope.start_opened = false;
            $scope.stop_opened = false;

            // lauch the interval period after 1 second allow the calendar to disapear.
            setTimeout(function () { $scope.intervalPeriod('na'); }, 1000);

        }

        $scope.Keypress = function (event) {
            console.log(" user press key : ", event.keyCode);
            if (event.keyCode < 48 || event.keyCode > 57) {
                event.preventDefault();
            }
        }


        $scope.stopPropagation = function ($event) {
            $scope.start_opened = false;
            $scope.stop_opened = false;
            console.log(" user press key : ", event.keyCode);
            $event.stopPropagation();
        };

        $scope.Close = function (param) {
            console.log(" closed calendar ", param);
            $scope.start_opened = false;
            $scope.stop_opened = false;
        }

        $scope.Close = function () {
            console.log(" closed calendar ", param);
            $scope.start_opened = false;
            $scope.stop_opened = false;
        }


        $scope.close = function (param) {
            console.log(" closed calendar ",param);
            $scope.start_opened = false;
            $scope.stop_opened = false;
        }

        $scope.close = function () {
            console.log(" closed calendar ");
            $scope.start_opened = false;
            $scope.stop_opened = false;
        }

        $scope.closing_view = function () {
            console.log(" closed calendar ");
            $scope.close();
        }
        

        $scope.intervalUp = function() {
            $scope.loadingHistory = true;
            if (($scope.intervalActive === 'monthly') ||
                ($scope.intervalActive === 'lastmonth')) {
                $scope.xAxis.domain[0] = angular.copy(moment($scope.xAxis.domain[0]).add(1, 'weeks').toDate());
                $scope.xAxis.domain[1] = angular.copy(moment($scope.xAxis.domain[1]).add(1, 'weeks').toDate());
               // historyCaching();
               // return;
            }

            if (($scope.intervalActive === 'today') ||
                ($scope.intervalActive === 'yesterday')) {
                $scope.xAxis.domain[0] = angular.copy(moment($scope.xAxis.domain[0]).add(3, 'hours').toDate());
                $scope.xAxis.domain[1] = angular.copy(moment($scope.xAxis.domain[1]).add(3, 'hours').toDate());
            }

            if (($scope.intervalActive === 'weekly') ||
                ($scope.intervalActive === 'lastweek') ||
                ($scope.intervalActive === 'period') ) {
                $scope.xAxis.domain[0] = angular.copy(moment($scope.xAxis.domain[0]).add(1, 'days').toDate());
                $scope.xAxis.domain[1] = angular.copy(moment($scope.xAxis.domain[1]).add(1, 'days').toDate());
            }
            $scope.retrieveHistory(moment($scope.xAxis.domain[0]).unix(), moment($scope.xAxis.domain[1]).unix());
            /*
            propertyService.GetHistory(moment($scope.xAxis.domain[0]).unix(), moment($scope.xAxis.domain[1]).unix()).then(function() {
                $scope.loadingHistory = false;
            });
            */

            $scope.startPeriod = $scope.xAxis.domain[0];
            $scope.stopPeriod = $scope.xAxis.domain[1];

            if ($scope.intervalActive === 'period') {   // save also if current selection is period
                $scope.startPeriod2 = $scope.xAxis.domain[0];
                $scope.stopPeriod2 = $scope.xAxis.domain[1];
            }
        };

        $scope.getSelected_Period = function () {
           return $scope.intervalActive
        }

        $scope.intervalPeriod = function (src) {
            if ($scope.periodInitialized === false) {   // not initialized
                $scope.startPeriod = angular.copy($rootScope.currentDate).startOf('week').toDate();
                $scope.stopPeriod = angular.copy($rootScope.currentDate).endOf('week').toDate();
                $scope.periodInitialized = true;
            }

            if (src === 'select') {
                $scope.startPeriod = $scope.startPeriod2
                $scope.stopPeriod = $scope.stopPeriod2
            }

            $scope.intervalActive = 'period';
            $scope.xAxis = {
                domain: [$scope.startPeriod, $scope.stopPeriod],
                //ticks: { timeScale: d3.time.days, timeInterval: 1 },
                ticks: { timeScale: 10, timeInterval: 1 },
                temperatureFormat: getModeTemperature(),
                orpOn: $scope.orpOn,
                phOn: $scope.phOn,
                poolTempOn: $scope.poolTempOn,
                spaTempOn: $scope.spaTempOn,
                solarTempOn: $scope.solarTempOn,
                airTempOn: $scope.airTempOn,
                zoomOn: $scope.zoomOn,
                zeroOn: $scope.zeroOn,
                freezeOn: $scope.freezeOn,
            };
            $scope.loadingHistory = true;

            MessageProcessor.InitHistoryInterval();
            $scope.retrieveHistory(moment($scope.xAxis.domain[0]).unix(), moment($scope.xAxis.domain[1]).unix());
            /*
            propertyService.GetHistory(moment($scope.xAxis.domain[0]).unix(), moment($scope.xAxis.domain[1]).unix()).then(function () {
                $scope.loadingHistory = false;
            });
            */
        };

        $scope.dateChanged = function (isStart, value) {
            $scope.start_opened = false;
            $scope.stop_opened = false;

            if (value === null) {
                // recover previous values
                $scope.startPeriod = $scope.startPeriod2;
                $scope.stopPeriod = $scope.stopPeriod2;
                console.log("date value is null")
                return;
            }
            
            if (isStart) {
                $scope.startPeriod = value;
                $scope.startPeriod2 = value;
                if ($scope.startPeriod && ($scope.startPeriod >= $scope.stopPeriod)) {                    
                    // then reassign the stop date to be a day later.
                    $scope.stopPeriod = moment($scope.startPeriod).add(1, 'days').toDate();
                    
                }
            } else { // Then the stop date changed.
                $scope.stopPeriod = value;
                $scope.stopPeriod2 = value;
                if ($scope.startPeriod && ($scope.stopPeriod <= $scope.startPeriod)) {                    
                    // then reassign the start date to be a day earlier.
                    $scope.startPeriod = moment($scope.stopPeriod).subtract(1, 'days').toDate();
                }
            }            
            
            // lauch the interval period after 1 second allow the calendar to disapear.
            setTimeout(function () { $scope.intervalPeriod('na'); }, 1000);
        };




        $scope.dateBlurred = function (isStart) {
            if (isStart && $scope.startPeriod === undefined) {
                $scope.startPeriod = moment().toDate();
            } else {
                if ($scope.stopPeriod === undefined) {
                    $scope.stopPeriod = moment().add(1, 'days').toDate();
                }
            }

            $scope.start_opened = false;
            $scope.stop_opened = false;
      
        };


        $scope.intervalWeek = function(param) {
            
            if (param === 'weekly') {
                $scope.intervalActive = 'weekly';
                $scope.startPeriod = angular.copy($rootScope.currentDate).startOf('week').toDate();
                $scope.stopPeriod = angular.copy($rootScope.currentDate).endOf('week').toDate();
                
            } else {
                $scope.intervalActive = 'lastweek';
                $scope.startPeriod = angular.copy($rootScope.currentDate).startOf('week').subtract(7,'days').toDate();
                $scope.stopPeriod = angular.copy($rootScope.currentDate).endOf('week').subtract(7, 'days').toDate();
                
            }

            $scope.xAxis = {
                //domain: [angular.copy($rootScope.currentDate).startOf('week').toDate(), angular.copy($rootScope.currentDate).endOf('week').toDate()],
                domain: [$scope.startPeriod, $scope.stopPeriod],
                ticks: { timeScale: d3.time.days, timeInterval: 1 },                
                temperatureFormat: getModeTemperature(),
                orpOn: $scope.orpOn,
                phOn: $scope.phOn,
                poolTempOn: $scope.poolTempOn,
                spaTempOn: $scope.spaTempOn,
                solarTempOn: $scope.solarTempOn,
                airTempOn: $scope.airTempOn,                
                zoomOn: $scope.zoomOn,
                zeroOn: $scope.zeroOn,
                freezeOn: $scope.freezeOn,
            };
            
            $scope.loadingHistory = true;

            MessageProcessor.InitHistoryInterval();
            $scope.retrieveHistory(moment($scope.xAxis.domain[0]).unix(), moment($scope.xAxis.domain[1]).unix());
            /*
            propertyService.GetHistory(moment($scope.xAxis.domain[0]).unix(), moment($scope.xAxis.domain[1]).unix()).then(function() {
                $scope.loadingHistory = false;
            });
            */
            
        };

        $scope.isActivePeriod = function (period) {
            if ($scope.intervalActive === period)
                return true
            else
                return false
        };
        
        $scope.fetchAndPlot = function () {
            if (($scope.intervalActive === 'today') || ($scope.intervalActive === 'yesterday'))
                $scope.jumpToday($scope.intervalActive)

            if (($scope.intervalActive === 'monthly')  || ($scope.intervalActive === 'lastmonth'))
                $scope.intervalMonth($scope.intervalActive)

            if (($scope.intervalActive === 'weekly') || ($scope.intervalActive === 'lastweek'))
                $scope.intervalWeek($scope.intervalActive)

            if (($scope.intervalActive === 'period') )
                $scope.intervalPeriod($scope.intervalActive)

        };

        $scope.intervalMonth = function (param) {
            if (param === 'monthly') {
                $scope.intervalActive = 'monthly';
                $scope.startPeriod = angular.copy($rootScope.currentDate).startOf('month').toDate();
                $scope.stopPeriod = angular.copy($rootScope.currentDate).endOf('month').toDate();
            } else {
                $scope.intervalActive = 'lastmonth';
                $scope.startPeriod = angular.copy($rootScope.currentDate).startOf('month').subtract(31, 'days').toDate();
                $scope.stopPeriod = angular.copy($rootScope.currentDate).endOf('month').subtract(31, 'days').toDate();
            }

            $scope.xAxis = {
                //domain: [angular.copy($rootScope.currentDate).startOf('month').toDate(), angular.copy($rootScope.currentDate).endOf('month').toDate()],
                domain: [$scope.startPeriod, $scope.stopPeriod],
                ticks: { timeScale: d3.time.weeks, timeInterval: 1 },
                temperatureFormat: getModeTemperature(),
                orpOn: $scope.orpOn,
                phOn: $scope.phOn,
                poolTempOn: $scope.poolTempOn,
                spaTempOn: $scope.spaTempOn,
                solarTempOn: $scope.solarTempOn,
                airTempOn: $scope.airTempOn,
                zoomOn: $scope.zoomOn,
                zeroOn: $scope.zeroOn,
                freezeOn: $scope.freezeOn,
            };
            MessageProcessor.InitHistoryInterval();
            $scope.loadingHistory = true;
            $scope.retrieveHistory(moment($scope.xAxis.domain[0]).unix(), moment($scope.xAxis.domain[1]).unix());
            //historyCaching();
        };

        $scope.getPeriodFrom = function (msg) {
            var display = msg;            
            display = display + $filter('date')($scope.startPeriod, 'EEEE MMM dd, yyyy');                        
            return display;
        }

        $scope.getPeriodTo = function (msg) {
            var display = msg;            
            display = display + $filter('date')($scope.stopPeriod, 'EEEE MMM dd, yyyy');
            return display;
        }

        $scope.processIdleHistory = function (param) {
            console.log("processing Idle History", param);

        }

        $scope.open = function (type) {
                        
            if (($scope.start_opened === true) ||   
                 ($scope.stop_opened === true)) {   // close window if is opened
                $scope.start_opened = false;
                $scope.stop_opened = false;
                // delay click for 400 milliseconds
                setTimeout(function () { $scope.processIdleHistory('na'); }, 400); // 400 milliseconds
            }

            if (type === 'start')
                $scope.start_opened = true;
            else
                $scope.stop_opened = true;
        };

        $scope.getZoomText = function () {
            if ($scope.zoomOn)
                return " On"
            else
                return " Off"
        }
        $scope.optionIsOn = function (plotid) {
            
            switch (plotid) {
                case 'orp':
                    return $scope.orpOn;

                case 'ph':
                    return $scope.phOn

                case 'pool':
                    return $scope.poolTempOn 

                case 'spa':
                    return $scope.spaTempOn 

                case 'solar':
                    return $scope.solarTempOn 

                case 'air':
                    return $scope.airTempOn 

                case 'zoom':
                    return $scope.zoomOn

                case 'zero':
                    return $scope.zeroOn

                case 'freeze':
                    return $scope.freezeOn
            }

            return false;

        };

        $scope.togglePlot = function (plotid) {
            
                switch (plotid) {
                    case 'orp':                        
                            $scope.orpOn = !$scope.orpOn;
                        break;

                    case 'ph':                        
                            $scope.phOn = !$scope.phOn;
                        break;
                    case 'pool':
                            $scope.poolTempOn = !$scope.poolTempOn;
                        break;

                    case 'spa':
                            $scope.spaTempOn = !$scope.spaTempOn;
                        break;

                    case 'solar':
                            $scope.solarTempOn = !$scope.solarTempOn;
                        break;

                    case 'air':
                            $scope.airTempOn = !$scope.airTempOn;
                        break;

                    case 'zoom':
                        $scope.zoomOn = !$scope.zoomOn;
                        break;

                    case 'zero':
                        $scope.zeroOn = !$scope.zeroOn;
                        $scope.forceRetrieve = true;
                        break;

                    case 'freeze':
                        $scope.freezeOn = !$scope.freezeOn;
                        $scope.forceRetrieve = true;
                        break;

            }
            
            $scope.fetchAndPlot();

        };

        $scope.getSelectedPlot = function (plotid) {
            var color = "white";
            switch (plotid) {
                case 'select1':
                    
                    if ($scope.isTabSelected ("Chemistry") && $scope.orpOn)
                        color = "black";
                    if ($scope.isTabSelected("Temperature") && $scope.poolTempOn)
                        color = "black";

                    break;

                case 'select2':
                    if ($scope.isTabSelected("Chemistry") && $scope.phOn)
                        color = "black";
                    if ($scope.isTabSelected("Temperature") && $scope.spaTempOn)
                        color = "black";

                    break;

                case 'select3':
                    if ($scope.isTabSelected("Temperature") && $scope.solarTempOn)
                        color = "black";
                    break;

                case 'select4':
                    if ($scope.isTabSelected("Temperature") && $scope.airTempOn)
                        color = "black";
                    break;

                case 'select5':
                    if ($scope.zoomOn)
                        color = "black";
                    break;

                case 'zero':
                    if ($scope.zeroOn)
                        color = "black";
                    break;

                case 'freeze':
                    if ($scope.freezeOn)
                        color = 'black';
                    break;

            }
            
            return color;
        };

        

        $scope.getHistoryBySNAME = function (sName) {
            /*
            return $scope.property.History.filter(function(historyItem) {
                return historyItem.bodyName === sName;
            })[0];
            */
            //console.log("------------------  HISTORY 1 ----------------------", sName)

            var value = $scope.property.History.filter(function (historyItem) {
                return historyItem.bodyName === sName;
            })[0];

            if ($scope.currentTab != "None") {
                console.log(" CURRENT TAB : ", $scope.currentTab)
            }
            //console.log("------------------  HISTORY 2 ----------------------", value)
            return value;

        };


        function init() {
            console.log("----- Init: displaying History view -----")                                  
        }

        function getModeTemperature(){
            var mode = "ENGLISH";
            if ($scope.property) {
                if ($scope.property.Objects) {
                    if ($scope.property.Objects["_5451"]) {
                        var obj = $scope.property.Objects["_5451"];
                        if (obj.MODE) { // check if exist
                            mode = obj.MODE;
                        }
                    }
                }
            }
            return mode;
        }

        function getBodies() {
            var result = [];
            
            if (propertyModel.SharedBodies) {   // check of sharedBodidies exist!
                angular.forEach(propertyModel.SharedBodies, function (value, key) {
                    var relationShip = {
                        parent: {},
                        child: {},
                        SNAME: '',
                        LISTORD: "0"
                    };
                    if (propertyModel.Objects[key].LISTORD < propertyModel.Objects[value].LISTORD) {
                        relationShip = {
                            parent: propertyModel.Objects[key],
                            child: propertyModel.Objects[value]
                        };
                    } else {
                        relationShip = {
                            parent: propertyModel.Objects[value],
                            child: propertyModel.Objects[key]
                        };
                    }
                    relationShip.SNAME = relationShip.parent.SNAME + '/' + relationShip.child.SNAME;
                    relationShip.LISTORD = relationShip.parent.LISTORD;
                    result.push(relationShip);
                });
            }

            var SingleBodies = propertyModel.SingleBodies.map(function(item) {
                var obj = propertyModel.Objects[item];
                return obj;
            });

            $scope.AllBodies = result.concat(SingleBodies);
        }

        function getHistoryDataCache() {
            var localStorageKeys = LocalStorageService.keys();
            var historyCache = null;
            angular.forEach(localStorageKeys, function(localStorageKey) {
                if (localStorageKey.indexOf('historycache') > -1) {
                    if (localStorageKey.split('-')[2] === LocalStorageService.get('selectedInstallation')) {
                        if (parseInt(localStorageKey.split('-')[3]) === moment($scope.xAxis.domain[0]).unix()) {
                            if (parseInt(localStorageKey.split('-')[4]) === moment($scope.xAxis.domain[1]).unix()) {
                                historyCache = LocalStorageService.get(localStorageKey);
                            }
                        }
                    }
                }
            });
            return historyCache;
        }

        function cacheHistoryData(start, end, data) {
            var installationId = LocalStorageService.get('selectedInstallation');
            //NOTE: Commenting out this line will prevent caching
            // LocalStorageService.set('historycache-' + moment().unix() + '-' + installationId + '-' + start + '-' + end, data);
        }

        function historyCaching() {
            // see if we have a cache for the history we are fetching
            var historyCache = getHistoryDataCache();
            if (historyCache) {
                MessageProcessor.ProcessHistoryMessage(historyCache);
                $scope.loadingHistory = false;
            } else {
                propertyService.GetHistory(moment($scope.xAxis.domain[0]).unix(), moment($scope.xAxis.domain[1]).unix()).then(function(result) {
                    // cache it into local storage
                    cacheHistoryData(moment($scope.xAxis.domain[0]).unix(), moment($scope.xAxis.domain[1]).unix(), result);
                    $scope.loadingHistory = false;
                });
            }
        }

    }
]);
