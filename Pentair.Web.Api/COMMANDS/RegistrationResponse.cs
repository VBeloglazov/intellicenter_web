﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Pentair.Web.Api
{
    public class RegistrationResponse : ResponseCommand
    {
        [JsonProperty("propertyID", Required = Required.Always)]
        public string PropertyID { get; private set; }

        [JsonProperty("sharedSecret", Required = Required.Always)]
        public string SharedSecret { get; private set; }

        public RegistrationResponse(
            string command,
            string messageID,
            string response,
            string propertyID,
            string sharedSecret
            )
            : base(command, messageID, response)
        {
            this.PropertyID = propertyID;
            this.SharedSecret = sharedSecret;

        } // RegistrationResponse()

        /// <summary>
        /// Always returns an object, valid or not. If invalid, check the DeserializationError property value.
        /// </summary>
        public static new RegistrationResponse Deserialize(
            string serializedMessage)
        {
            RegistrationResponse command = null;

            try
            {
                command = JsonConvert.DeserializeObject<RegistrationResponse>(serializedMessage);
                command.IsValid = true;
            }
            catch (Exception e)
            {
                // Deserialization as RegistrationResponse failed

                StringBuilder sbError = new StringBuilder();

                sbError.Append("Cannot deserialize as RegistrationResponse. " + e.Message);

                // Try to get at least the base message data
                ResponseCommand baseCommand = ResponseCommand.Deserialize(serializedMessage);

                if (!baseCommand.IsValid)
                {
                    sbError.Append("\n" + baseCommand.DeserializationError);
                }

                command = new RegistrationResponse(
                    command: baseCommand.Command,
                    messageID: baseCommand.MessageID,
                    response: ResponseString.BadRequest,
                    propertyID: String.Empty,
                    sharedSecret: String.Empty)
                {
                    IsValid = false,
                    DeserializationError = sbError.ToString()
                };
            }

            return command;

        } // Deserialize()

    } // class RegistrationResponse
}
