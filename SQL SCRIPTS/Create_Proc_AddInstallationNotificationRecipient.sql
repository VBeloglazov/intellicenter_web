-- =============================================================
--
-- Author:		Vladimir Beloglazov
-- Create date: 04/05/2018
--
-- Creates:
--
--		Table Type:			None
--
--		Stored Procedure:	PAddInstallationNotificationRecipient
--
-- =============================================================

USE PathfinderIntelliCenter
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (
	SELECT * FROM sys.objects
	WHERE object_id = OBJECT_ID(N'dbo.PAddInstallationNotificationRecipient')
	AND type in (N'P', N'PC')
)
DROP PROCEDURE dbo.PAddInstallationNotificationRecipient
GO

CREATE PROCEDURE dbo.PAddInstallationNotificationRecipient
	 @installationId	AS int
	,@name				AS nvarchar(255)
	,@email				AS nvarchar(255)

AS
BEGIN

	SET NOCOUNT OFF

	DECLARE @RecipientNewId TABLE (RecipientId int)

	INSERT INTO NotificationRecipients (
		 InstallationId
	    ,Name
	    ,EmailAddress1
    )
	OUTPUT INSERTED.RecipientId INTO @RecipientNewId
    SELECT
	     @installationId	-- InstallationId
        ,@name				-- Name
	    ,@email				-- EmailAddress1
    WHERE NOT EXISTS (
	    SELECT 1 FROM NotificationRecipients
	    WHERE InstallationId = @installationId
        AND Name = @name
	    AND EmailAddress1 = @email
    )

	-- Result set
	SELECT RecipientId
	FROM @RecipientNewId

END
GO
