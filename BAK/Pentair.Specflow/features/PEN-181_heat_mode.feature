Feature: Heat Mode (PEN-181)
As a Pool Owner
I want to Check and Change the Head Mode of a Body of Water
So That I can enjoy my pool at the desired temperature

  Background user is connected to a pool system
  And user is viewing pool details

Scenario: SELECT HEAT MODE
  When the user selects a heat mode
  Then the appropriate message is sent to the pool system
  And app display reflects the user's choice

Scenario: VIEW HEAT MODE CHANGES
  When the heat mode is changed at the pool system
  Then that change will be reflected in the app's display