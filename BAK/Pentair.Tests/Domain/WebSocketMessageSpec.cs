﻿using System;
using System.Windows.Documents;
using Machine.Specifications;
using Pentair.Domain.Client.Helpers;
using Pentair.Domain.Client.Helpers.Commands;
using System.Collections.Generic;
using FizzWare.NBuilder;
using It = Machine.Specifications.It;
using Newtonsoft.Json;

namespace Pentair.Tests.Domain
{
    public class WebSocketMessageSpec
    {
        [Subject("WebSocketMessage.IsAnAcknowledgementMessage")]
        public class WhenTheServerReceivesShortNonAcknowledgementMessage
        {
            static string _message;
            static bool _result;

            Establish _context = () =>
            {
                _message = "msg";
            };

            Because _of = () => _result = WebSocketMessage.IsAnAcknowledgementMessage(_message);

            It _shouldReturnFalse = () => _result.ShouldEqual(false);
        }

        [Subject("WebSocketMessage.IsAnAcknowledgementMessage")]
        public class WhenTheServerReceivesLongNonAcknowledgementMessage
        {
            static string _message;
            static bool _result;

            Establish _context = () =>
            {
                _message = "This is a message";
            };

            Because _of = () => _result = WebSocketMessage.IsAnAcknowledgementMessage(_message);

            It _shouldReturnFalse = () => _result.ShouldEqual(false);
        }

        [Subject("WebSocketMessage.IsAnAcknowledgementMessage")]
        public class WhenTheServerReceivesAnOkAcknowledgementMessage
        {
            static string _message;
            static bool _result;

            Establish _context = () =>
            {
                _message = "OK";
            };

            Because _of = () => _result = WebSocketMessage.IsAnAcknowledgementMessage(_message);

            It _shouldReturnTrue = () => _result.ShouldEqual(true);
        }

        [Subject("WebSocketMessage.IsAnAcknowledgementMessage")]
        public class WhenTheServerReceivesAnErrorAcknowledgementMessage
        {
            static string _message;
            static bool _result;

            Establish _context = () =>
            {
                _message = "ERROR: Bad Message";
            };

            Because _of = () => _result = WebSocketMessage.IsAnAcknowledgementMessage(_message);

            It _shouldReturnTrue = () => _result.ShouldEqual(true);
        }

        [Subject("WebSocketMessage.IsSuccessAcknowledgement")]
        public class WhenIsSuccessAcknowledgementIsCalledWithNonOkMessage
        {
            static string _message;
            static bool _result;

            Establish _context = () =>
            {
                _message = "msg";
            };

            Because _of = () => _result = WebSocketMessage.IsSuccessAcknowledgement(_message);

            It _shouldReturnFalse = () => _result.ShouldEqual(false);
        }

        [Subject("WebSocketMessage.IsSuccessAcknowledgement")]
        public class WhenIsSuccessAcknowledgementIsCalledWithOkMessage
        {
            static string _message;
            static bool _result;

            Establish _context = () =>
            {
                _message = "OK";
            };

            Because _of = () => _result = WebSocketMessage.IsSuccessAcknowledgement(_message);

            It _shouldReturnTrue = () => _result.ShouldEqual(true);
        }

        [Subject("WebSocketMessage.IsErrorAcknowledgement")]
        public class WhenIsErrorAcknowledgementIsCalledWithShortMessage
        {
            static string _message;
            static bool _result;

            Establish _context = () =>
            {
                _message = "msg";
            };

            Because _of = () => _result = WebSocketMessage.IsErrorAcknowledgement(_message);

            It _shouldReturnFalse = () => _result.ShouldEqual(false);
        }

        [Subject("WebSocketMessage.IsErrorAcknowledgement")]
        public class WhenIsErrorAcknowledgementIsCalledWithNonErrorMessage
        {
            static string _message;
            static bool _result;

            Establish _context = () =>
            {
                _message = "This is a message";
            };

            Because _of = () => _result = WebSocketMessage.IsErrorAcknowledgement(_message);

            It _shouldReturnFalse = () => _result.ShouldEqual(false);
        }

        [Subject("WebSocketMessage.IsErrorAcknowledgement")]
        public class WhenIsErrorAcknowledgementIsCalledWithAnErrorMessage
        {
            static string _message;
            static bool _result;

            Establish _context = () =>
            {
                _message = "ERROR: Bad Message";
            };

            Because _of = () => _result = WebSocketMessage.IsAnAcknowledgementMessage(_message);

            It _shouldReturnTrue = () => _result.ShouldEqual(true);
        }

        // 2017-04-24 VB ParseMessageType() throws now
        /*
        [Subject("WebSocketMessage.ParseMessageType")]
        public class WhenInboundMessageIsCalledWithNullMessage
        {
            private static string _message;
            private static MessageType _result;

            private Establish _context = () =>
            {
                _message = "";
            };

            Because _of = () => _result = WebSocketMessage.ParseMessageType(_message, EndpointType.Property);

            It _shouldReturnType = () => _result.ShouldBeOfExactType<MessageType>();
            It _shouldReturnOrigin = () => _result.Origin.ShouldEqual(EndpointType.Property);
            It _shouldReturnCommand = () => _result.CommandType.ShouldEqual(CommandType.Invalid);
        }

        [Subject("WebSocketMessage.ParseMessageType")]
        public class WhenInboundMessageIsCalledWithValidMessage
        {
            private static string _message;
            private static MessageType _result;

            private Establish _context = () =>
            {
                _message = string.Format(
                    "{{\"command\": \"{0}\",\"messageID\": \"{1}\"}}",
                    CommandString.GetParamList,
                    "DeadBeef");
            };

            Because _of = () => _result = WebSocketMessage.ParseMessageType(_message, EndpointType.Property);

            It _shouldReturnType = () => _result.ShouldBeOfExactType<MessageType>();
            It _shouldReturnOrigin = () => _result.Origin.ShouldEqual(EndpointType.Property);
            It _shouldReturnCommand = () => _result.CommandType.ShouldEqual(CommandType.GetParamList);
        }

        [Subject("WebSocketMessage.ParseMessageType")]
        public class WhenInboundMessageIsCalledWithInvalidMessage
        {
            private static string _message;
            private static MessageType _result;

            private Establish _context = () =>
            {
                _message = string.Format(
                    "{{\"command\": \"{0}\",\"messageID\": \"{1}\"}}",
                    "DeadBeef",
                    "BadCommand");
            };

            Because _of = () => _result = WebSocketMessage.ParseMessageType(_message, EndpointType.Client);

            It _shouldReturnType = () => _result.ShouldBeOfExactType<MessageType>();
            It _shouldReturnOrigin = () => _result.Origin.ShouldEqual(EndpointType.Client);
            It _shouldReturnCommand = () => _result.CommandType.ShouldEqual(CommandType.Invalid);
        }
        */

        [Subject("WebSocketMessage.IssueCommand")]
        public class WhenTheServerIssuesGetParamListForSingleParameter
        {
            static string _result;
            static GetParamList _getParamList;

            Establish _context = () =>
            {
                _getParamList = new GetParamList(
                    messageID: String.Empty,
                    condition: String.Empty,
                    objectList: new List<KeyObject>()
                );
            };

            Because _of = () => _result = JsonConvert.SerializeObject(_getParamList);

            It _shouldReturnJsonFormattedString = () => _result.ShouldNotBeEmpty();
            It _shouldReturnToTypeGetParamList = () => JsonConvert.DeserializeObject<GetParamList>(_result).ShouldBeOfExactType<GetParamList>();
        }

        [Subject("WebSocketMessage.IssueCommand")]
        public class WhenTheServerIssuesRequestParamListForSingleParameter
        {
            static string _result;
            static RequestParamList _requestParamList;

            Establish _context = () =>
            {
                _requestParamList = new RequestParamList(
                    messageID: String.Empty,
                    condition: String.Empty,
                    objectList: new List<KeyObject>()
                );
            };

            Because _of = () => _result = JsonConvert.SerializeObject(_requestParamList);

            It _shouldReturnJsonFormattedString = () => _result.ShouldNotBeEmpty();
            It _shouldReturnToTypeRequestParamList = () => JsonConvert.DeserializeObject<RequestParamList>(_result).ShouldBeOfExactType<RequestParamList>();
        }

        [Subject("WebSocketMessage.IssueCommand")]
        public class WhenTheServerIssuesReleaseParamListForSingleParameter
        {
            static string _result;
            static ReleaseParamList _releaseParamList;

            Establish context = () =>
            {
                _releaseParamList = new ReleaseParamList(
                    messageID: String.Empty,
                    condition: String.Empty,
                    objectList: new List<KeyObject>()
                );
            };

            Because _of = () => _result = JsonConvert.SerializeObject(_releaseParamList);

            It _shouldReturnJsonFormattedString = () => _result.ShouldNotBeEmpty();
            It _shouldReturnToTypeReleaseParamList = () => JsonConvert.DeserializeObject<ReleaseParamList>(_result).ShouldBeOfExactType<ReleaseParamList>();
        }

        [Subject("WebSocketMessage.IssueCommand")]
        public class WhenTheServerIssuesSetParamListForSingleParameter
        {
            static string _result;
            static SetParamListResponse _setParamList;

            Establish _context = () =>
            {
                _setParamList = new SetParamListResponse(
                    messageID: String.Empty,
                    response: ResponseString.OK,
                    objectList: new List<ParamObject>()
                );
            };

            Because _of = () => _result = JsonConvert.SerializeObject(_setParamList);

            It _shouldReturnJsonFormattedString = () => _result.ShouldNotBeEmpty();
            It _shouldReturnToTypeSetParamList = () => JsonConvert.DeserializeObject<SetParamListResponse>(_result).ShouldBeOfExactType<SetParamListResponse>();
        }

        [Subject("WebSocketMessage.IssueCommand")]
        public class WhenTheServerIssuesClearParamCommand
        {
            static string _result;
            static ClearParamResponse _clearParam;

            Establish _context = () =>
            {
                _clearParam = new ClearParamResponse(
                    messageID: String.Empty,
                    response: ResponseString.OK
                );
            };

            Because _of = () => _result = JsonConvert.SerializeObject(_clearParam);

            It _shouldReturnJsonFormattedString = () => _result.ShouldNotBeEmpty();
            It _shouldReturnToTypeClearParam = () => JsonConvert.DeserializeObject<ClearParamResponse>(_result).ShouldBeOfExactType<ClearParamResponse>();
        }

        [Subject("WebSocketMessage.IssueCommand")]
        public class WhenTheServerInvitesUser
        {
            static PoolAccessUser _inviteUser;

            Establish _context = () =>
            {
                _inviteUser = 
                    new PoolAccessUser(
                        command: CommandString.InviteUser,
                        messageID: "",
                        param: new Dictionary<string, string>()
                        {
                            {"user", "id"}
                        }
                    );
            };

            private It shouldBe = () => _inviteUser.Command.ShouldEqual("INVITEUSER");
        }

        [Subject("WebSocketMessage.IssueCommand")]
        public class WhenTheServerCreatesANotify
        {
            private static NotifyListResponse _notifyList;

            Establish _context = () =>
            {
                _notifyList = new NotifyListResponse(
                    messageID: String.Empty,
                    response: ResponseString.OK,
                    timeNow: String.Empty,
                    timeSince: String.Empty,
                    objectList: new List<ParamObject>()
                    {
                        new ParamObject(
                            objNam: "objnam",
                            parameters: new Dictionary<string, string>()
                            {
                                {"user", "id"}
                            }
                        )
                    }
                );
            };

            //Establish _context = () =>
            //{
            //    _notifyList = new NotifyListResponse(
            //        messageID: String.Empty,
            //        response: ResponseString.OK,
            //        timeNow: String.Empty,
            //        timeSince: String.Empty,
            //        objectList: new List<ExtendedParamObject>()
            //    );

            //    _notifyList.ObjectList.Add(
            //       new ExtendedParamObject(
            //           changes: new List<ParamObject>()
            //            {
            //                new ParamObject(
            //                    objNam: "objnam",
            //                    parameters: new Dictionary<string, string>()
            //                    {
            //                        { "user", "id" }
            //                    }
            //                )
            //            }
            //       )
            //   );
            //};

            private It shouldBe = () => _notifyList.Command.ShouldEqual("NOTIFYLIST");
        }

        [Subject("WebSocketMessage.IssueCommand")]
        public class WhenTheServerCreatesAWriteParamList
        {
            private static WriteParamListResponse _writeParamList;

            Establish _context = () =>
            {
                _writeParamList = new WriteParamListResponse(
                    messageID: String.Empty,
                    response: ResponseString.OK,
                    timeNow: String.Empty,
                    timeSince: String.Empty,
                    objectList: new List<ExtendedParamObject>()
                );

                _writeParamList.ObjectList.Add(
                    new ExtendedParamObject(
                        changes: new List<ParamObject>()
                        {
                            new ParamObject(
                                objNam: "objnam",
                                parameters: new Dictionary<string, string>()
                                {
                                    { "user", "id" }
                                }
                            )
                        }
                    )
                );
            };

            private It shouldBe = () => _writeParamList.Command.ShouldEqual("WRITEPARAMLIST");
        }
    }
}
