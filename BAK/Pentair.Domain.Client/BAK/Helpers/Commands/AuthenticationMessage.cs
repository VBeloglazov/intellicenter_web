﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Newtonsoft.Json;

namespace Pentair.Domain.Client.Helpers.Commands
{
    public class AuthenticationMessage
    {
        [JsonProperty("messageID")]
        public string MessageID { get; private set; }

        [JsonProperty("propertyID")]
        public string PropertyID { get; private set; }

        [JsonProperty("sharedSecret")]
        public string SharedSecret { get; private set; }

        [JsonProperty("apiVersion")]
        public string ApiVersion { get; private set; }

    } // class AuthenticationMessage
}