﻿describe('AppService', function () {
    var AppService;
    var $q;
    var $httpBackend;
    var mockInstallations;
    var url;
    var HTTP_SERVICE_URL;

    beforeEach(module('PentairWebClient'));

    beforeEach(inject(function (_AppService_, _$q_, _$httpBackend_, _SERVICE_HOST_NAME_) {
        AppService = _AppService_;
        $q = _$q_;
        $httpBackend = _$httpBackend_;
        HTTP_SERVICE_URL = 'https://' + _SERVICE_HOST_NAME_;
        mockInstallations = [
            {
                "property": "1f92ba9c-077a-4cac-bdd2-1dee3579911a",
                "name": "hewitts pool 1",
                "connected": false
            },
            {
                "property": "c077a376-e4f5-40b9-a495-ace172297e1c",
                "name": "Hewitt Pool 4",
                "connected": true
            },
            {
                "property": "f467e58f-307f-43f4-a9dc-df7bc3828a88",
                "name": "Hewitt Pool 2",
                "connected": false
            }
        ];
    }));

    describe('Installations', function () {

        it('should return a list of installations', function () {
            $httpBackend.expectGET(HTTP_SERVICE_URL + 'Api/Installations').respond(200, mockInstallations);

            var installations;
            AppService.Installations().then(function (response) {
                installations = response.data;
            });
            $httpBackend.flush();

            expect(installations).toEqual(mockInstallations);
        });

        it('should be undefined', function () {
            $httpBackend.expectGET(HTTP_SERVICE_URL + 'Api/Installations').respond(500);

            var installations;
            AppService.Installations().then(function (response) {
                installations = response.data;
            });
            $httpBackend.flush();

            expect(installations).not.toBeDefined();
        });
    });

    describe('GetData', function () {
        url = "SomeUrl";

        it('should return with sucess', function () {
            $httpBackend.expectGET(HTTP_SERVICE_URL + url).respond(200, "success");

            var result;
            AppService.GetData(url).then(function (response) {
                result = response.data;
            });
            $httpBackend.flush();

            expect(result).toEqual("success");
        });


        it('should return with sucess', function () {
            $httpBackend.expectGET(HTTP_SERVICE_URL + url).respond(500, "success");

            var result;
            AppService.GetData(url).then(function (response) {
                result = response.data;
            });
            $httpBackend.flush();

            expect(result).not.toBeDefined();
        });
    });

    describe('PostData', function () {
        url = "SomeUrl";
        var postData = "postData";

        it('should return with sucess', function () {
            $httpBackend.expectPOST(HTTP_SERVICE_URL + url, postData).respond(200, "success");

            var result;
            AppService.PostData(url, postData).then(function (response) {
                result = response.data;
            });
            $httpBackend.flush();

            expect(result).toEqual("success");
        });

        it('should return undefined', function () {
            $httpBackend.expectPOST(HTTP_SERVICE_URL + url, postData).respond(500, "error");

            var result;
            AppService.PostData(url, postData).then(function (response) {
                result = response.data;
            });
            $httpBackend.flush();

            expect(result).not.toBeDefined();
        });
    });
});