﻿using System;


namespace Pentair.Domain.Client
{
    public static class Globals
    {
        public static UInt32 CurrentTime()
        {
            var t = DateTime.UtcNow - new DateTime(1970, 1, 1);
            return (UInt32) t.TotalSeconds;
        }

    }
}
