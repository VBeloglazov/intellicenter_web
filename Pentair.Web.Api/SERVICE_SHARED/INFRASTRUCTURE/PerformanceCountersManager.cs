﻿/*
--------------------------------------------------------------------------------

    PerformanceCountersManager.cs

    Copyright © Pentair Water Pool and Spa, Inc. 2015

--------------------------------------------------------------------------------
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace SERVICE_SHARED
{
    public class PerformanceCountersManager : IDisposable
    {
        private string categoryName;

        private Dictionary<string, PerformanceCounter> counterInstances = new Dictionary<string, PerformanceCounter>();

        #region Construction/Dispose

        /// <exception cref="Exception" />
        public PerformanceCountersManager(
            string categoryName,
            string categoryHelp,
            CounterCreationData[] counters)
        {
            //
            // Create category
            //

            if (PerformanceCounterCategory.Exists(categoryName))
            {
                PerformanceCounterCategory.Delete(categoryName);
            }

            CounterCreationDataCollection counterData = new CounterCreationDataCollection();

            foreach (var counter in counters)
            {
                counterData.Add(
                    new CounterCreationData(counter.CounterName, counter.CounterHelp, counter.CounterType)
                );
            }

            PerformanceCounterCategory.Create(
                categoryName,
                categoryHelp,
                PerformanceCounterCategoryType.SingleInstance,
                counterData
            );

            this.categoryName = categoryName;

            //
            // Create counter instances
            //

            foreach (var counter in counters)
            {
                this.counterInstances[counter.CounterName] =
                    new PerformanceCounter(categoryName, counter.CounterName, false);
            }

        } // PerformanceCountersManager()

        public void Dispose()
        {
            foreach (PerformanceCounter counterInstance in this.counterInstances.Values)
            {
                counterInstance.Dispose();
            }

            this.counterInstances.Clear();

            PerformanceCounterCategory.Delete(this.categoryName);

        } // Dispose()

        #endregion Construction/Dispose

        #region Public Interface

        public virtual void SetCounterValue(
            string counterName,
            long value)
        {
            if (this.counterInstances.ContainsKey(counterName))
            {
                this.counterInstances[counterName].RawValue = value;
            }

        } // SetCounterValue()

        public virtual void IncrementCounter(
            string counterName)
        {
            if (this.counterInstances.ContainsKey(counterName))
            {
                this.counterInstances[counterName].Increment();
            }

        } // IncrementCounter()

        public virtual void DecrementCounter(
            string counterName)
        {
            if (this.counterInstances.ContainsKey(counterName))
            {
                this.counterInstances[counterName].Decrement();
            }

        } // DecrementCounter()

        public virtual void IncrementCounterBy(
            string counterName,
            long value)
        {
            if (this.counterInstances.ContainsKey(counterName))
            {
                this.counterInstances[counterName].IncrementBy(value);
            }

        } // IncrementCounterBy()

        #endregion Public Interface

    } // class PerformanceCountersManager
}
