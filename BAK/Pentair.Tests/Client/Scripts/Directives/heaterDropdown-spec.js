﻿xdescribe('Heater DropDown Menu', function () {
    var $scope, $compile;
    var elm;
    var Enumerations;
    var baseTemplate = '<div heater-dropdown dropdown-onchange="OnChange(body,heatMode)" body-object="body"> {{body.OBJNAM}}</div>';

    beforeEach(module('PentairWebClient'));

    beforeEach(inject(function ($rootScope, _$compile_, _Enumerations_) {
        Enumerations = _Enumerations_;
        // Get an isolated scope
        $scope = $rootScope.$new();
        $scope.body = SpecHelper.mocks.Bodies[0];
        $scope.body.HEATERS = {
            GENERIC: {
                HEATER: SpecHelper.mocks.Heaters[0],
                HCOMBO: SpecHelper.mocks.HeaterCombos[0]
            },
            SECONDARY: {
                HEATER: SpecHelper.mocks.Heaters[1],
                HCOMBO: SpecHelper.mocks.HeaterCombos[1]
            },
            VIRTUAL: SpecHelper.mocks.Heaters[2]
        };
        $compile = _$compile_;
        elm = SpecHelper.createDirective($compile, baseTemplate, $scope);
        $scope.OnChange = jasmine.createSpy();
    }));


    describe('set selection via model', function () {
        var elm;
        beforeEach(function () {
            elm = SpecHelper.createDirective($compile, baseTemplate, $scope);
        });

        it('should set selected Item to Heater if body.heater is Generic Heater ', function () {
            $scope.body.HEATER = "H1000";
            $scope.$apply();
            expect(elm[0].children[0].textContent).toBe('Heater');
        });

        it('should update selected item to Heat Pump Only if body.heater is Secondary Heater ', function () {
            $scope.body.HEATER = "H1001";
            $scope.$apply();
            expect(elm[0].children[0].textContent).toBe('Solar Only');
        });

        it('should update selected item to Heat Pump Only if body.heater is HCOMBO Heater ', function () {
            $scope.body.HEATER = "H1002";
            $scope.$apply();
            expect(elm[0].children[0].textContent).toBe('Solar Preferred');
        });

        it('should update selected item to Heat Pump Only if body.heater is 00000', function () {
            $scope.body.HEATER = "00000";
            $scope.$apply();
            expect(elm[0].children[0].textContent).toBe('Heat Off');
        });

        it('should update selected item to Heat Pump Only if body.heater is 000FF', function () {
            $scope.body.HEATER = "000FF";
            $scope.$apply();
            expect(elm[0].children[0].textContent).toBe('Heat Off');
        });
    });

    describe('set model via selection', function () {
        var elm;
        var selected;
        beforeEach(function () {
            elm = SpecHelper.createDirective($compile, baseTemplate, $scope);
            $scope.body.HEATER = "H1000";
            $scope.$apply();
        });

        it('should call on change with GENERIC When Heater is chosen', function () {
            elm[0].children[1].children[1].children[0].click();
            expect($scope.OnChange).toHaveBeenCalledWith($scope.body.OBJNAM, "GENERIC");
        });

        it('should call on change with GENERIC When Heater is chosen', function () {
            elm[0].children[1].children[2].children[0].click();
            expect($scope.OnChange).toHaveBeenCalledWith($scope.body.OBJNAM, "PREF");
        });

        it('should call on change with GENERIC When Heater is chosen', function () {
            elm[0].children[1].children[3].children[0].click();
            expect($scope.OnChange).toHaveBeenCalledWith($scope.body.OBJNAM, "ONLY");
        });

        it('should call on change with GENERIC When Heater is chosen', function () {
            elm[0].children[1].children[0].children[0].click();
            expect($scope.OnChange).toHaveBeenCalledWith($scope.body.OBJNAM, "NONE");
        });

        it('should toggle element class to active when clicked', function () {
            expect(elm.hasClass('active')).toBe(false);
            elm[0].click();
            expect(elm.hasClass('active')).toBe(true);
            elm[0].click();
            expect(elm.hasClass('active')).toBe(false);
        });

    });

    describe('set active class', function () {
        var elm;
        var selected;
        beforeEach(function () {
            elm = SpecHelper.createDirective($compile, baseTemplate, $scope);
        });

        it('should toggle element class to active when clicked', function () {
            expect(elm.hasClass('active')).toBe(false);
            elm[0].click();
            expect(elm.hasClass('active')).toBe(true);
            elm[0].click();
            expect(elm.hasClass('active')).toBe(false);
        });

    });
});