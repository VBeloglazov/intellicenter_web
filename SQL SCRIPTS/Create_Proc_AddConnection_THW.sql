-- =============================================================
--
-- Author:		Vladimir Beloglazov
-- Create date: 01/26/2018
--
-- Creates:
--
--		Table Type:			None
--
--		Stored Procedure:	PAddConnectionThw
--
-- 2018-03-16 VB: [Installations] redesign.
--
-- =============================================================

USE PathfinderIntelliCenter
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (
	SELECT * FROM sys.objects
	WHERE object_id = OBJECT_ID(N'dbo.PAddConnectionThw')
	AND type in (N'P', N'PC')
)
DROP PROCEDURE dbo.PAddConnectionThw
GO

-- -------------------------------------------------------------
--
-- Returns [Connections] record just created.
--
-- -------------------------------------------------------------
CREATE PROCEDURE dbo.PAddConnectionThw
	 @hostMachine		AS nvarchar(max)
	,@installationId	AS int
	,@eventId			AS int
	,@userId			AS int

AS
BEGIN

	SET NOCOUNT OFF

	BEGIN TRANSACTION

	BEGIN TRY

		-- Validate input
		IF NOT EXISTS (SELECT * FROM dbo.Installations WHERE InstallationId = @installationId)
			THROW 50000, 'Installation not found', 1;

		-- Add [Connections] record, return new record ID

		DECLARE @ConnectionNewId TABLE (ConnectionId uniqueidentifier)

		INSERT INTO dbo.Connections (HostMachine, InstallationId, UserId)
		OUTPUT INSERTED.ConnectionId INTO @ConnectionNewId
		VALUES (@hostMachine, @installationId, CAST(0x0 AS uniqueidentifier))

		DECLARE @newConnectionId uniqueidentifier
		SET @newConnectionId = (SELECT TOP 1 ConnectionId FROM @ConnectionNewId)

		-- Add Event
        INSERT INTO dbo.[Events] (EventId, InstallationId, UserId)
        VALUES (@eventId, @installationId, @userId)

		-- Result set
		SELECT * FROM dbo.Connections
		WHERE ConnectionId = @newConnectionId

	END TRY
	BEGIN CATCH

		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION

		DECLARE @err_message nvarchar(255)
		SET @err_message = ERROR_MESSAGE()
		RAISERROR (@err_message, 11, 1) -- severity 11-19 will cause returning exception to caller

	END CATCH

	IF @@TRANCOUNT > 0
		COMMIT TRANSACTION

END
GO
