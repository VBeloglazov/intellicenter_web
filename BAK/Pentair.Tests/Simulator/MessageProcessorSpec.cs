﻿using Machine.Fakes;
using Machine.Specifications;
using Newtonsoft.Json;
using Pentair.Domain.Client.Entity;
using Pentair.Domain.Client.Helpers.Commands;
using Pentair.Domain.Client.Infrastructure;

namespace Pentair.Tests.Simulator
{
    [Subject("ProcessMessages")]
    class MessageProcessorSpec
    {
        #region Json Commands

        private const string GetParamMessage =
            @"{'command':'GetParamList','objectList':[{'objnam':'ALL','keys':['OBJNAM : OBJREV : OBJTYP : STATIC : SUBTYP : RECENT : FREQ : CUSTOM : LISTORD : HNAME : SNAME : READY : PRIM : SEC : COMUART : BODY : SHARE : PARENT : COOL : START : PHTYP : ORPTYP : PHMOD : ORPMOD? OBJTYP=BODY']}]}";

        private const string RequestParamMessage =
            @"{'command':'RequestParamList','objectList':[{'objnam':'ALL','keys':['START : STOP : DLY : PERMIT : FILTER : PUMP : MASTER : HTSRC : HEATER : VOL : SHOMNU : ALPHA : TIMZON : FEATR : USAGE : CYCTIM : DAY : GROUP : HITMP : LOTMP : SETTMP : PRIMFLO : FLOWDLY : PHPRIOR : MODE ? OBJTYP=BODY & SUBTYP=SPA']}]}";

        private const string ClearParamMessage =
            @"{'command':'ClearParam','objectList':[{'objnam':'ALL','keys':['HTMODE : SERVICE : PASSWRD : ENABLE : SENSE : PRIMTIM : PRESS : SYSTIM : BAKFLO : BAKTIM : RNSTIM : VACFLO : VACTIM : CIRCUIT : RPM : FLOW : PWR : PRIOR : SPEED : BOOST : IPADY : SUBNET : PHFED1 : CHLOR ? OBJTYP=BODY & SUBTYP=SPA']}]}";

        private const string ReleaseParamMessage =
            @"{'command':'ReleaseParamList','objectList':[{'objnam':'ALL','keys':['DFGAT : DNSSERV : WAPNET : WAPPW : PANID : LEGACY : STATUS : TEMP : EMAIL : MIN : MAX : CHEM : SYSTEM : PHONE : URL : LOGO : BADGE : TIMOUT : SOURCE : ZIP : LOCX : LOCY : DLSTIM : USE : CLK24A : STATUS1 ? OBJTYP=BODY & SUBTYP=SPA']}]}";

        private const string SetParamMessage =
            @"{'command':'SetParamList','objectList':[{'objnam':'ALL','keys':['OFFSET : ACCEL : ABSTIM : SRIS : SSET : MENU : RESET : PARTY : ON : OFF : RLY : FREEAE : VER : SERNUM : MNFDAT : NORMAL : DIMMER : MANUAL : TIME : BTT : RADIO : AIR : PORT : CNFG : PANEL ? OBJTYP=BODY & SUBTYP=SPA']}]}";

        private const string UnknownMessage =
            @"{'command':'UnknownMessage','objectList':[{'objnam':'ALL','keys':['RUNON : ASSIGN : DFLT : ACT : POSIT : VALVE : MODULE : COUNT : UNITS : SUPER : SALT : DEVICE : SALTLO : VERYLO : CURRENT : VOLT : PHSET : ORPSET : PHTNK : ORPTNK : CALC : CYACIT : ALK : SINDEX ? OBJTYP=BODY & SUBTYP=SPA']}]}";

        #endregion

        #region Database Records

        private static readonly ISimulatorContext SimulatorFakeContext = new FakeContext()
        {
            ControlObjects =
            {
                new ControlObject()
                {
                    INSTALLATION = 1,
                    OBJNAM = "C1355",
                    //OBJTYP = "BODY",
                    //SUBTYP = "BODY",
                    //SNAME = "Pool Circuit",
                    //RECENT = "0",
                    //FREQ = 0,
                    //CUSTOM = 0,
                    //LISTORD = 1,
                    //SHOMNU = "s",
                    //STATUS = "ON",
                    //TIMOUT = "0",
                    //TIME = "0",
                    //SINDEX = 0,
                    //COUNT = 0
                },
                new ControlObject()
                {
                    INSTALLATION = 1,
                    OBJNAM = "C1356",
                    //OBJTYP = "BODY",
                    //SUBTYP = "SPA",
                    //SNAME = "Spa Circuit",
                    //RECENT = "0",
                    //FREQ = 0,
                    //CUSTOM = 0,
                    //LISTORD = 1,
                    //SHOMNU = "s",
                    //STATUS = "OFF",
                    //TIMOUT = "0",
                    //TIME = "0",
                    //SINDEX = 0,
                    //COUNT = 0
                },
                new ControlObject()
                {
                    INSTALLATION = 1,
                    OBJNAM = "C5433",
                    //OBJTYP = "CIRCUIT",
                    //SUBTYP = "INTELLI",
                    //SNAME = "Night Spa",
                    //RECENT = "0",
                    //FREQ = 0,
                    //CUSTOM = 0,
                    //LISTORD = 1,
                    //SHOMNU = "l",
                    //STATUS = "OFF",
                    //TIMOUT = "0",
                    //TIME = "0",
                    //SINDEX = 0,
                    //COUNT = 0
                },
                new ControlObject()
                {
                    INSTALLATION = 1,
                    OBJNAM = "C5435",
                    //OBJTYP = "CIRCUIT",
                    //SUBTYP = "MAGIC2",
                    //SNAME = "Night Laminar",
                    //RECENT = "0",
                    //FREQ = 0,
                    //CUSTOM = 0,
                    //LISTORD = 2,
                    //SHOMNU = "l",
                    //STATUS = "OFF",
                    //TIMOUT = "0",
                    //TIME = "0",
                    //SINDEX = 0,
                    //COUNT = 0
                },
                new ControlObject()
                {
                    INSTALLATION = 1,
                    OBJNAM = "C5436",
                    //OBJTYP = "CIRCUIT",
                    //SUBTYP = "GENERIC",
                    //SNAME = "Water Fall",
                    //RECENT = "0",
                    //FREQ = 0,
                    //CUSTOM = 0,
                    //LISTORD = 1,
                    //SHOMNU = "f",
                    //STATUS = "OFF",
                    //TIMOUT = "0",
                    //TIME = "0",
                    //SINDEX = 0,
                    //COUNT = 0
                },
                new ControlObject()
                {
                    INSTALLATION = 1,
                    OBJNAM = "C5437",
                    //OBJTYP = "CIRCUIT",
                    //SUBTYP = "FRZ",
                    //SNAME = "Freeze Mode",
                    //RECENT = "0",
                    //FREQ = 0,
                    //CUSTOM = 0,
                    //LISTORD = 1,
                    //SHOMNU = "s",
                    //STATUS = "ON",
                    //TIMOUT = "0",
                    //TIME = "0",
                    //SINDEX = 0,
                    //COUNT = 0
                }
            }
        };
        #endregion

        #region Tests
        //public class WhenTheClientRequestsAllBodyObjectsThenSendTheData : WithFakes
        //{
        //    private static MessageProcessor _subject;
        //    private const string Config = "Config";
        //    private static BusinessLogic _businessLogic;
        //    private static string _response;

        //    private Establish context = () =>
        //    {
        //        _subject = new MessageProcessor(OcpFakeContext as FakeContext) { Configuration = Config };
        //        Configure(x => x.For<BusinessLogic>().Use<IBusinessLogic>() as FakeApi());
        //    };

        //    private Because of = () =>
        //    {
        //        _response = _subject.GetParamList(JsonConvert.DeserializeObject<GetParamList>(GetBodyObjects));
        //    };

        //    private It should_be_all_body_objects = () => _response.Count.ShouldEqual(2);
        //}

        #endregion
    }
}
