// Karma configuration
// Generated on Wed Apr 16 2014 13:28:34 GMT-0500 (Central Daylight Time)

module.exports = function (config) {
    var sourcePreprocessors = 'coverage';
    function isDebug(argument) {
        return argument === '--debug';
    }

    if (process.argv.some(isDebug)) {
        sourcePreprocessors = [];
    }

    config.set({
        // base path that will be used to resolve all patterns (eg. files, exclude)
        basePath: '',


        // frameworks to use
        // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
        frameworks: ['jasmine'],

        plugins: [
         'karma-jasmine',
         'karma-xml-reporter',
         'karma-coverage',
         'karma-teamcity-reporter',
         'karma-chrome-launcher',
         'karma-phantomjs-launcher',
         'karma-ie-launcher'
         //'karma-firefox-launcher'
        ],


        files: [
            '../../Pentair.Web.Client/app/i18n/*.js',
            '../../Pentair.Web.Client/scripts/jquery-2.1.1.js',

            '../Scripts/angular.js',
            '../Scripts/Helpers/*.js',
            '../Scripts/angular-mocks.js',
            '../Scripts/angular-animate.js',
            '../Scripts/angular-route.js',
            '../Scripts/jasmine-jquery.js',

            '../../Pentair.Web.Client/scripts/angular-animate.js',
            '../../Pentair.Web.Client/scripts/angular-cookies.js',
            '../../Pentair.Web.Client/scripts/angular-dateParser.js',
            '../../Pentair.Web.Client/scripts/angular-local-storage.js',
            '../../Pentair.Web.Client/scripts/angular-translate.js',
            '../../Pentair.Web.Client/scripts/angular-round-progress.js',
            '../../Pentair.Web.Client/scripts/angular-route.js',
            '../../Pentair.Web.Client/scripts/angular-timePicker.js',
            '../../Pentair.Web.Client/scripts/angular-translate.js',
            '../../Pentair.Web.Client/scripts/checklist-model.js',
            '../../Pentair.Web.Client/scripts/d3-v3.5.17.min.js',
            '../../Pentair.Web.Client/scripts/d3-gantt.js',
            '../../Pentair.Web.Client/scripts/d3-legend.js',
            '../../Pentair.Web.Client/scripts/html5shiv.js',
            '../../Pentair.Web.Client/scripts/loading-bar.js',
            '../../Pentair.Web.Client/scripts/moment.js',
            '../../Pentair.Web.Client/scripts/select.min.js',
            '../../Pentair.Web.Client/scripts/sortable.min.js',
            '../../Pentair.Web.Client/scripts/angular-ui-notification.min.js',
            '../../Pentair.Web.Client/scripts/ui-bootstrap-tpls-0.11.2.min.js',
            '../../Pentair.Web.Client/scripts/ui-grid.js',

            '../../Pentair.Web.Client/app/app.js',
            '../../Pentair.Web.Client/app/*/*.js',
            '../Client/Scripts/*/*-spec.js'
        ],


        // list of files to exclude
        exclude: [
      '../../Pentair.Web.Client/app/directives/ngEnter.js',
      '../../Pentair.Web.Client/app/directives/ngNumbersOnly.js'
        ],

        // preprocess matching files before serving them to the browser
        // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
        preprocessors: {
            '../../Pentair.Web.Client/app/app.js': sourcePreprocessors,
            '../../Pentair.Web.Client/app/*/*.js': sourcePreprocessors,
            '../../Pentair.Web.Client/app/directives/ngEnter.js': []
        },


        // available reporters: https://npmjs.org/browse/keyword/karma-reporter
        // test results reporter to use
        // possible values: 'dots', 'progress', 'junit', 'growl', 'coverage'
        reporters: ['progress', 'xml', 'coverage', 'teamcity'],
        //reporters: ['progress'],


        // web server port
        port: 9876,


        // coverage type and directory
        coverageReporter: {
            type: 'html',
            dir: 'coverage/'
        },


        // enable / disable colors in the output (reporters and logs)
        colors: true,


        // level of logging
        // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
        logLevel: config.LOG_INFO,


        // enable / disable watching file and executing tests whenever any file changes
        autoWatch: true,


        // start these browsers
        // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
        browsers: ['PhantomJS'],
        // browsers: ['Chrome'],
        // browsers: ['Firefox', 'Chrome'],
        // browsers: ['IE'],
        // browsers: ['Firefox', 'PhantomJS'],

        captureTimeout: 140000,

        // Continuous Integration mode
        // if true, Karma captures browsers, runs the tests and exits
        singleRun: false
    });
};
