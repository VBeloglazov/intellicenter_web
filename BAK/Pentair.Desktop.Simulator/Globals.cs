﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pentair.Domain.Client.Entity;

namespace Pentair.Desktop.Simulator
{
    public static class Globals
    {

        public static UInt32 CurrentTime()
        {
            var t = DateTime.UtcNow - new DateTime(1970, 1, 1);
            return (UInt32)t.TotalSeconds;
        }

        public static Dictionary<string, string> Hosts = new Dictionary<string, string>()
        {
            {"Local", "wss://localhost:44300/api/websocket"},
            {"Development", "wss://pentdev.pathf.com/service/api/websocket"},
            {"Quality Assurance", "wss://pentqa.pathf.com/service/api/websocket"},
            {"User Acceptance ", "wss://pentuat.pathf.com/service/api/websocket"},
            {"Production ", "wss://IntelliCenter.com/service/api/websocket"}
        };

        public static Dictionary<PropertyConfigurations, string> Configurations = new Dictionary
            <PropertyConfigurations, string>()
        {
            {PropertyConfigurations.SharedPoolAndSpa, "Shared Pool and Spa"},
            {PropertyConfigurations.SeparatePoolAndSpa, "Separate Pool and Spa"},
            {PropertyConfigurations.PoolOnly, "Pool only"},
            {PropertyConfigurations.SpaOnly, "Spa only"},
            {PropertyConfigurations.SimpleBlankConfiguration, "i5P"},
            {PropertyConfigurations.ComplexBlankConfiguration, "Complex"},
            {PropertyConfigurations.Import, "Import from json file"}
        };
    }
}
