﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Pentair.Domain.Client.Helpers.Commands
{
    public class RegistrationMessage : CommandBase
    {
        [JsonProperty("userName", Required = Required.Always)]
        public string UserName { get; private set; }

        [JsonProperty("password", Required = Required.Always)]
        public string Password { get; private set; }

        [JsonProperty("name", Required = Required.Always)]
        public string Name { get; private set; }

        [JsonProperty("hasMoved", Required = Required.Always)]
        public string HasMoved { get; private set; }

        [JsonProperty("createNew", Required = Required.Always)]
        public string CreateNew { get; private set; }

        [JsonProperty("propertyID", Required = Required.Always)]
        public string PropertyID { get; private set; }

        public RegistrationMessage(
            string command,
            string messageID,
            string userName,
            string password,
            string name,
            string hasMoved,
            string createNew,
            string propertyID
            )
            : base(command, messageID)
        {
            this.UserName = userName;
            this.Password = password;
            this.Name = name;
            this.HasMoved = hasMoved;
            this.CreateNew = createNew;
            this.PropertyID = propertyID;

        } // RegistrationMessage()

        /// <summary>
        /// Always returns an object, valid or not. If invalid, check the DeserializationError property value.
        /// </summary>
        public static new RegistrationMessage Deserialize(
            string serializedMessage)
        {
            RegistrationMessage command = null;

            try
            {
                command = JsonConvert.DeserializeObject<RegistrationMessage>(serializedMessage);
                command.IsValid = true;
            }
            catch (Exception e)
            {
                // Deserialization as RegistrationMessage failed

                StringBuilder sbError = new StringBuilder();

                sbError.Append("Cannot deserialize as RegistrationMessage. " + e.Message);

                // Try to get at least the base message data
                CommandBase baseCommand = CommandBase.Deserialize(serializedMessage);

                if (!baseCommand.IsValid)
                {
                    sbError.Append("\n" + baseCommand.DeserializationError);
                }

                command = new RegistrationMessage(
                    command: baseCommand.Command,
                    messageID: baseCommand.MessageID,
                    userName: String.Empty,
                    password: String.Empty,
                    name: String.Empty,
                    hasMoved: String.Empty,
                    createNew: String.Empty,
                    propertyID: String.Empty)
                {
                    IsValid = false,
                    DeserializationError = sbError.ToString()
                };
            }

            return command;

        } // Deserialize()

    } // class RegistrationMessage
}
