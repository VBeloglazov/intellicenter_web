﻿'use strict';
var PWC = PWC || {};

PWC.factory('AuthService', ['LocalStorageService', '$location', '$q', 'AppService', 'Enumerations',
    function (LocalStorageService, $location, $q, appService, enumerations) {
        var factory = {};

        var authentication = {
            isAuth: false,
            userName: "",
            lastUserName: "",
            accountCreated: false,
            passwordChanged: false,
            userNameChanged :false,
            loginError: false
        };

        factory.authorization = {
            SHOMNU: ''
        };

        function resetAuthData(clearErrors) {
            authentication.isAuth = false;
            authentication.userName = "";

            sessionStorage.clear();
            LocalStorageService.clearAll();

            if (clearErrors) {
                authentication.passwordChanged = false;
                authentication.accountCreated = false;
                authentication.userNameChanged = false;
                authentication.loginError = false;
            }
        }

        function cacheAuthData(token, userName, rememberMe) {
            LocalStorageService.clearAll();

            if (rememberMe) {
                LocalStorageService.set('token', token);
                LocalStorageService.set('userName', userName);
                LocalStorageService.set('saveSession', rememberMe);
            }

            sessionStorage.token = token;
            sessionStorage.userName = userName;
        }

        function login(loginData) {
            var d = $q.defer();
            resetAuthData(true);

            var data = "grant_type=password&username=" + loginData.userName + "&password=" + loginData.password;
            appService.PostData('login', data, { 'Content-Type': 'application/x-www-form-urlencoded' }).then(function (response) {
                cacheAuthData(response.data.access_token, loginData.userName, loginData.rememberMe);
                authentication.isAuth = true;
                authentication.userName = loginData.userName;
                d.resolve(response);
            }, function (err) {
                authentication.loginError = true;
                d.reject(err);
            });

            return d.promise;

        };

        function logOut() {
            resetAuthData(false);
            $location.path('/login');
        };


        function restorePreviousSession() {
            if (LocalStorageService.get('token') && LocalStorageService.get('userName')) {
                sessionStorage.token = LocalStorageService.get('token');
                sessionStorage.userName = LocalStorageService.get('userName');
                if (LocalStorageService.get('selectedInstallation'))
                    sessionStorage.selectedInstallation = LocalStorageService.get('selectedInstallation') || enumerations.DefaultInstallation;
            }
            fillAuthData();
        }


        function fillAuthData() {
            if (sessionStorage.token && sessionStorage.userName) {
                authentication.isAuth = true;
                authentication.userName = sessionStorage.userName;
            }

        };


        factory.login = login;
        factory.logOut = logOut;
        factory.resetAuthData = resetAuthData;
        factory.restorePreviousSession = restorePreviousSession;
        factory.authentication = authentication;

        return factory;
    }]);