'use strict';
var PWC = PWC || {};

PWC.factory('SocketService', ['SERVICE_HOST_NAME', '$http', '$q', '$window', '$rootScope', 'PropertyModel',
function (SERVICE_HOST_NAME, $http, $q, $window, $rootScope, propertyModel) {
    var factory = {};
    var propertyId;
    var socket = {};
    var messageQueue = {};
    
    var message_duplicated = []; // define array to store duplicated messages

    factory.ping = function() {
      var defer = $q.defer();
      if (propertyModel.Ready)
        propertyModel.ConnectionStatus = 'Sending...';

      var messageID = Math.uuidCompact();
      // var messageJson = JSON.stringify(message);
      var messageQueueObj = {
        callBack: defer,
        time: new Date(),
        message: 'ping'
      };

      if (PWC.isEmptyObject(socket)) {
        defer.reject("No Socket");
        console.log("No Socket");
      } else if (socket.readyState != 1) {
        defer.reject("Socket is not Open");
        console.log("Socket is not Open");
      }
      else {
        console.log("Sending: '" + 'ping' + "' : " + new Date());
        messageQueue[messageID] = messageQueueObj;
        socket.send('ping');
      }
      return defer.promise;
    };

    var sendMessage = function (message, retry) {
        var defer = $q.defer();
        if (propertyModel.Ready)
            propertyModel.ConnectionStatus = 'Sending...';

        if (!message.messageID) {
            message.messageID = Math.uuidCompact();
        }
        var messageJson = JSON.stringify(message);
        var messageQueueObj = {
            callBack: defer,
            time: new Date(),
            message: message
        };

        if (PWC.isEmptyObject(socket)) {
            defer.reject("No Socket");
            console.log("No Socket");
        } else if (socket.readyState != 1) {
            defer.reject("Socket is not Open");
            console.log("Socket is not Open");
        }
        else {            
            messageQueue[message.messageID] = messageQueueObj;
            socket.send(messageJson);        
            consoleLog(">> Sending: ", messageJson)
        }
        return defer.promise;
    }
    
    // display log message Tx (Sending message) or Rx (Receiving message)
    function consoleLog(type,message) {
        var d = new Date()
        var header = "\n" + type + " [" + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds() + "." + d.getMilliseconds() + "]: \n"
        // avoid crash here message is not json format
        if (message == 'pong') {
            console.log(header, message);
        } else {
            var jsonPretty = JSON.stringify(JSON.parse(message), null, 2)
            console.log(header, jsonPretty)
        }
    }


    function filterDuplicated(message) {        
            
        if (message_duplicated[message.messageID])
            return true; // message is already processed discard this message now

        // add  to list
        message_duplicated.push(message.messageID)

        // check if list have more than 64 items, remove the first and mantain only 32 entries.
        if (message_duplicated.length > 64)
            message_duplicated.shift()


        return false
    }

    function handler(message) {
        if (filterDuplicated(message) === true) {
            console.log("*******************************************************************  duplicated messageID: ",message.messageID, " command : ",message.command)
            return; // do nothing
        }
        var queuedMsg = messageQueue[message.messageID];
        if (queuedMsg) {
            if(message.command.toUpperCase() === 'NOTIFYLIST' ||
                message.command.toUpperCase() === 'WRITESTATUSMESSAGE') {
                queuedMsg.callBack.resolve(message);
            } else if (message.response == 200 || message.response == 201) {
                queuedMsg.callBack.resolve(message);
                //console.log("\nSERVER ACCEPTED : ");//console.log("\nSERVER ACCEPTED : ", message);

            } else {
                queuedMsg.callBack.reject(message);
                console.log("\n***** SERVER REJECTED : ", message);
            }

            delete messageQueue[message.callbackID];
            $rootScope.$apply();
        }
    }

    var newSocket = function (propId) {

        console.log("-------------------------------------------------------------------------------");
        console.log("------------------------------------ NEW SOCKECT propID: ", propId);

        var accessToken = "";

        //copy to parent Scope
        propertyId = propId;

        //close if socket is open
        if (socket.readyState < 2) {
            socket.close();
            messageQueue = {};
        } 

        if (sessionStorage.token) {
            accessToken = '?access-token=' + sessionStorage.token;
        }

        try {
            socket = new WebSocket('wss://' + SERVICE_HOST_NAME + 'api/websocket/' + propertyId + accessToken);

            socket.onclose = function (event) {
                socket = {};
                console.log("Connection Terminated : " + new Date());
            }

            socket.onerror = function (err) {
                console.log('Error: ' + new Date() + ' ---' + err.data);
            }

            socket.onopen = function (event) {
                console.log("Connection Established : " + new Date());
            };

            socket.onmessage = function (message) {

                consoleLog("<< Receiving ", message.data)            
                if (angular.isString(message.data) && message.data === 'pong') {
                }
                else {
                  handler(JSON.parse(message.data));
                }
            };
        } catch (e) {
            console.log("SOCKET OPEN ERROR: " + e);
            socket = {};
        }
        return socket;
    };

    factory.sendPendingSubscriptionRequest = function () {
        propertyModel.PendingSubscriptionRequest.objectList = [];
      return factory.subscribe(propertyModel.PendingSubscriptionRequest.objectList);
    }

    //TODO:Optimization - Add timer to concatente request sent within x milliseconds?
    factory.subscribe = function (subscriptionRequest) {
        if (!angular.isArray(subscriptionRequest)) {
            subscriptionRequest = [subscriptionRequest];
        }
        var message = {
            command: "RequestParamList",
            objectList: subscriptionRequest
        }
        if (message.objectList.length > 0) {
            return factory.sendMessage(message);
        } else {
            return $q.defer().reject('no objects to subscribe to.');
        }
    }

    //TODO:Optimization - Add timer to concatente request sent within x milliseconds?
    factory.unSubscribe = function (unsubscribeRequest) {
        if (!angular.isArray(unsubscribeRequest)) {
            unsubscribeRequest = [unsubscribeRequest];
        }
        var message = {
            command: "RELEASEPARAMLIST",
            objectList: []
        };

        for (var i = 0; i < unsubscribeRequest.length; i++) {
            var request = unsubscribeRequest[i];
            message.objectList.push({
                objnam: request.objnam,
                keys: request.keys
            });
        }
       return factory.sendMessage(message);
    }

    factory.clearSubscriptions = function () {
        var message = {
            command: "CLEARPARAM"
        };
        return factory.sendMessage(message);
    }

    factory.messageQueue = function () {
        return messageQueue;
    }

    factory.socket = socket;
    factory.newSocket = newSocket;
    factory.sendMessage = sendMessage;
    factory.closeSocket = function () {
        if (!PWC.isEmptyObject(socket) && socket.readyState < 2)
            socket.close();
    }

    return factory;
}
]);
