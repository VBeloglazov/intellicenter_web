﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Pentair.Domain.Client.Entity;
using Pentair.Domain.Client.Infrastructure;

namespace Pentair.Domain.Client.DataLayer
{

    public class UserAccountHelper
    {
        public UserInvitation GetUserInvitation(string invitationId)
        {
            UserInvitation returnValue;
            var id = new Guid(invitationId);
            using (var db = new ApiContext())
            {
                returnValue = db.UserInvitations.FirstOrDefault(x => x.InvitationId == id);
            }
            return returnValue;
        }

        public void UserInvitationAccepted(UserInvitation invite)
        {
            using (var db = new ApiContext())
            {
                var i = db.UserInvitations.FirstOrDefault(x => x.InvitationId == invite.InvitationId);
                if (null != i)
                {
                    i.Status = "ACCEPTED";
                    db.SaveChanges();
                }
            }
        }

        public bool AssociateAccount(Guid userid, UserInvitation invite)
        {
            var newAssociation = new Association
            {
                UserId = userid,
                InstallationId = invite.InstallationId.Value,
                AccessToken = invite.AccessToken
            };
            using (var db = new ApiContext())
            {
                db.Associations.AddOrUpdate(newAssociation);
                db.SaveChanges();
            }
            return true;

        }
    }
}
