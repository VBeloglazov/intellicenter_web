﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pentair.Domain.Client.BusinessLogic;
using Pentair.Domain.Client.Entity;
using Pentair.Domain.Client.Helpers;
using Pentair.Domain.Client.Infrastructure;

namespace Pentair.Tests.Domain
{
    class TokenParserSpec
    {
        #region Database Records

        private static int property1 = 1;

        public class TestContent
        {
            public IApiContext Data { get; private set; }

            public TestContent()
            {
                Data = new FakeContext()
                {
                    ControlObjects =
                    {
                        new ControlObject()
                        {
                            INSTALLATION = property1,
                            OBJNAM = StaticData.Guest,
                            //OBJTYP = "PERMIT",
                            //SNAME = "Guest",
                            //SHOMNU = "slb",
                            //LISTORD = 1,
                            //VER = 1.0f
                        },
                        new ControlObject()
                        {
                            INSTALLATION = property1,
                            OBJNAM = StaticData.Admin,
                            //OBJTYP = "PERMIT",
                            //SNAME = "Admin",
                            //SHOMNU = "slb",
                            //LISTORD = 1,
                            //VER = 1.0f
                        }

                    },
                };
            }
        }

        #endregion

        #region Tests

        //public class WhenProcessingQuerySTring
        //{
        //    private static ProcessQueryString subject;
        //    private static TestContext data;
        //}

        #endregion
    }
}
