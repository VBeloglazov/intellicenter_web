﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Pentair.Domain.Client.Entity
{

    public enum NotificationType
    {
        Email,
        SMS
    }

    public class UserNotification
    {
        [Key]
        [Column(Order = 1)]
        public Guid UserId { get; set; }

        [Key]
        [Column(Order = 2)]
        public int InstallationId { get; set; }

        [Key]
        [Column(Order = 3)]
        public int EventId { get; set; }

        [ForeignKey("InstallationId")]
        public virtual Installation Installation { get; set; }

        [ForeignKey("EventId")]
        public virtual EventType Event { get; set; }

        public TimeSpan Duration { get; set; }

        public int NotificationType { get; set; }

        [Column(TypeName = "DateTime2")]
        public DateTime NotificationSent { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(TypeName = "DateTime2")]
        public DateTime CreateDateTime { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [Column(TypeName = "DateTime2")]
        public DateTime UpdateDateTime { get; set; }
    }
}

