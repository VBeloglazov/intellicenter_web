﻿'use strict';
var PWC = PWC || {};
PWC.controller('ManagePropertiesController', [
    '$scope', '$location', '$filter', '$route', 'PropertyService', 'AppService', 'AuthService', 'PropertyModel',
    function ($scope, $location, $filter, $route, propertyService, appService, authService, propertyModel) {
        var nextPage = 0;
        var prevFilter = "All";
        var search = {
            filter: "All",
            term: ""
        }
        var allItemsLoaded = false;
        propertyService.FetchInstallations();
        $scope.Filter = search.filter;
        $scope.DeleteError = false;
        $scope.FilterPanelVisible = false;
        $scope.DataLoading = false;
        $scope.Installations = [];
        $scope.SelectedInstallation = false;
        $scope.DeleteModalVisible = false;
        $scope.$parent.Layout = "layout-base";
        $scope.DeletePool= function (id) {
            var data = {
                PropertyId: id
            }

            appService.PostData('Api/Account/RemoveInstallation/', data).then(function () {
                $scope.DeleteError = false;
                loadFreshData();
                $scope.ToggleDeleteModal();
                $scope.SelectedInstallation = false;

                if (id === propertyService.SelectedInstallation) {
                    propertyService.Reset();
                }
            }).catch(function (response) {
                $scope.DeleteError = (response.status === 400) ? $filter('translate')('USERNOTFOUNDERROR')
                    : $filter('translate')('LASTADMINDELETEERROR');

                loadFreshData();
                $scope.ToggleDeleteModal();
            });
        }

        $scope.GoToDashboard = function () {
            $location.path("/installations");
        }

        $scope.LoadDetails = function (index) {
            $scope.SelectedInstallation = $scope.Installations[index];
        }

        $scope.FilterInstallations = function (filter) {
            //set desired seach filter but revert back if failed
            search.filter = filter;

            $scope.Filter = search.filter;
            $scope.ToggleFilterPanel();

            return loadFreshData().then(function () {
                prevFilter = search.filter;
            }).catch(function () {
                $scope.Filter = prevFilter;
            });
        }

        $scope.SearchInstallations = function () {
            search.term = $scope.SearchTerm;
            return loadFreshData();
        }

        $scope.ClearSearchTerm = function () {
            $scope.SearchTerm = "";
            search.term = "";
            return loadFreshData();
        }

        $scope.LoadMoreInstallations = function () {
            if (!allItemsLoaded && !$scope.DataLoading) {
                $scope.DataLoading = true;
                return appService.InstallationsWithDetails(15, nextPage, search.term, search.filter).then(loadTable);
            }
        }

        $scope.ToggleDeleteModal = function () {
            $scope.DeleteModalVisible = !$scope.DeleteModalVisible;
        }

        $scope.ToggleFilterPanel = function () {
            $scope.FilterPanelVisible = !$scope.FilterPanelVisible;
        }

        function loadFreshData() {
            nextPage = 0;
            $scope.Installations = [];
            allItemsLoaded = false;
            $scope.SelectedInstallation = false;
            return $scope.LoadMoreInstallations();
        }

        function loadTable(result) {
            var totalItems = 0;
            if (result && result.data) {
                totalItems = JSON.parse(result.headers('x-pagination'))['TotalCount'];
                $scope.Installations = $scope.Installations.concat(result.data);
                nextPage++;
            }

            if ($scope.Installations.length >= totalItems) {
                allItemsLoaded = true;
            }
            $scope.DataLoading = false;
        }

        //set up initial grid data
        if ($route.current && $route.current.locals && $route.current.locals.installations) {
            loadTable($route.current.locals.installations);
        }
    }
]);