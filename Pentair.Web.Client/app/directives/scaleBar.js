﻿var PWC = PWC || {};
PWC.directive('scaleBar', ['$rootScope', function ($rootScope) {
    'use strict';
    return {
        restrict: 'A',
        scope: {
            range: '=',
            segmentColors: '=',
            markerClass: '@',
            labelClass: '@',
            chemType: '@',
            model: '=scaleBar'
        },
        template: '<div style="position:relative;">' +
                     '<span class="{{markerClass}}" ng-style="markerPosition"></span>' +
                     '<span class="{{labelClass}}" ng-style="labelPosition">{{(model | number:precision) || "--"}}</span>' +
                        '<div class="chem-gradient-scale-{{chemType}}">' +
                           // '<div ng-repeat="seg in segmentColors" ng-style="segmentPosition" class="chem-scale-segment" style="background-color:{{seg}};"></div>' +
                        '</div>' +
                    '</div>',
        link: function ($scope, $element, attr) {
            $scope.precision = parseInt(attr.precision) || 0;

            $scope.labelPosition = { left: '0px' };
            $scope.markerPosition = { left: '0px' };
            $scope.segmentPosition = { width: '1px', height: '5px' };


            $scope.$watch('model', function () {
                buildStyles();
            });

            //watch to restyle when element width is defined then unwatch
            var unWatcher = $scope.$watch(function () {
                return $element[0].offsetWidth !== 0;
            }, function (notZero) {
                if (notZero) {
                    buildStyles();
                    unWatcher();
                }
            });

            function buildStyles() {
                $scope.markerPosition = updateMakerPosition(0);
                $scope.labelPosition = updateMakerPosition(1);
                $scope.segmentPosition = updateSegmentStyles();
            };

            function updateMakerPosition(childIndex) {
                var width = $element[0].offsetWidth;
                var value = checkRange();

                var markerWidth = $element[0].children[0].children[childIndex].offsetWidth;
                var left = (((value - $scope.range.min) / ($scope.range.max - $scope.range.min)) * width) - 10 - (markerWidth / 1.3);
                if (left < 5) {
                    left = 5;
                }
                return { left: left + "px" };
            }

            function updateSegmentStyles() {
                var width = $element[0].offsetWidth;
                var segWidth = (width / $scope.segmentColors.length) - 2;
                return { width: segWidth + "px", height: "35px" };
            }

            function checkRange() {
                var value = $scope.model || 0;
                if (value < $scope.range.min) {
                    value = $scope.range.min;
                } else if (value > $scope.range.max) {
                    value = $scope.range.max;
                }
                return value;
            }
        }
    };
}]);