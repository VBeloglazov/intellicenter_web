﻿using Pentair.Domain.Client.Entity;
using Pentair.Domain.Client.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pentair.Domain.Client.Helpers
{
    public static class Utilities
    {
        public static bool IsConnectionValid(
            int installationId)
        {
            ApiContext db = new ApiContext();

            Connection connection = db.Connections.FirstOrDefault(
                x => x.InstallationId == installationId
            );

            return (connection != null);

        } // IsConnectionValid
    }
}
