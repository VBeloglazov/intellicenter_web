﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pentair.Domain.Client.Entity
{
    public class Connection
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ConnectionId { get; set; }

        [Required]
        public string HostMachine { get; set; }

        [Required]
        public int InstallationId { get; set; }

        [ForeignKey("InstallationId")]
        public virtual Installation Installation { get; set; }

        [Required]
        public Guid UserId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(TypeName = "DateTime2")]
        public DateTime CreateDateTime { get; set; }
    }
}