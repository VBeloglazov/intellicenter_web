﻿var PWC = angular.module('PentairWebClient', ['ngRoute', 'LocalStorageModule', 'angular-loading-bar'
                                            , 'ngCookies', 'pascalprecht.translate', 'checklist-model', 'dnTimepicker', 'ui.bootstrap', 'ui.grid', 'ui.grid.paging', 'ui.grid.cellNav', 'ui.grid.pagination']);

PWC.provider('translator', myTranslateProvider);

PWC.config(function ($routeProvider, translatorProvider, $translateProvider, $httpProvider) {

    var hostedEndpoint = '';
    var hostname = window.location.hostname;
    var endpoint = (function (hostname) {
        return (hostname === 'localhost') ? '' : hostedEndpoint;
    })(hostname);

    //Set up routes object
    PWC.routes =
    {
        //"/local": {
        //    contoller: "LocalController",
        //    templateUrl: endpoint + "/app/templates/local.html",
        //    requireLogin: false
        //},
        //"/property": {
        //    controller: "DashboardController",
        //    templateUrl: endpoint + "/app/templates/property.html",
        //    requireLogin: true
        //},

    };

    //add routes to RouteProvider
    //for (var path in PWC.routes) {
    //    $routeProvider.when(path, PWC.routes[path]);
    //}

    ////Default Page
    //$routeProvider.otherwise({ redirectTo: "/local" });

    //I18N translation provider
    translatorProvider.init($translateProvider);
});


PWC.run([
    '$location', '$rootScope', function ($location, $rootScope) {

        // intercept location changes to enforce required login
        $rootScope.$on("$locationChangeStart", function (event, next, current) {
            for (var i in PWC.routes) {
                if (next.indexOf(i) != -1) {
                    if (PWC.routes[i].requireLogin) {
                        $location.path("/local");
                        event.preventDefault();
                    }
                }
            }
        });
    }
]);
