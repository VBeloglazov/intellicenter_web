﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Pentair.Domain.Client.Helpers.Commands
{
    public class InvalidRequestResponse : ResponseCommand
    {
        [JsonProperty("rejectedRequest", Required = Required.Always)]
        public string RejectedRequest { get; private set; }

        public InvalidRequestResponse(
            string rejectedRequest
            )
            : base(
                command: CommandString.Invalid,
                messageID: String.Empty,
                response: ResponseString.BadRequest)
        {
            this.RejectedRequest = rejectedRequest;

        } // InvalidRequestResponse()

        /// <summary>
        /// Always returns an object, valid or not. If invalid, check the DeserializationError property value.
        /// </summary>
        public static new InvalidRequestResponse Deserialize(
            string serializedMessage)
        {
            InvalidRequestResponse command = null;

            try
            {
                command = JsonConvert.DeserializeObject<InvalidRequestResponse>(serializedMessage);
                command.IsValid = true;
            }
            catch (Exception e)
            {
                // Deserialization as InvalidRequestResponse failed

                StringBuilder sbError = new StringBuilder();

                sbError.Append("Cannot deserialize as InvalidRequestResponse. " + e.Message);

                // Try to get at least the base message data
                ResponseCommand baseCommand = ResponseCommand.Deserialize(serializedMessage);

                if (!baseCommand.IsValid)
                {
                    sbError.Append("\n" + baseCommand.DeserializationError);
                }

                command = new InvalidRequestResponse(
                    rejectedRequest: String.Empty)
                {
                    IsValid = false,
                    DeserializationError = sbError.ToString()
                };
            }

            return command;

        } // Deserialize()

    } // class InvalidRequestResponse
}
