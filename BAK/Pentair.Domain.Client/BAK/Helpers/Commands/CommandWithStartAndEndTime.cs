﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Pentair.Domain.Client.Helpers.Commands
{
    public class CommandWithStartAndEndTime : CommandBase
    {
        [JsonProperty("timeStart")]
        public string TimeStart { get; private set; }

        [JsonProperty("timeEnd")]
        public string TimeEnd { get; private set; }

        public CommandWithStartAndEndTime(
            string command,
            string messageID,
            string timeStart,
            string timeEnd
            )
            : base(command, messageID)
        {
            this.TimeStart = timeStart;
            this.TimeEnd = timeEnd;
        }

    } // class CommandWithStartAndEndTime
}
