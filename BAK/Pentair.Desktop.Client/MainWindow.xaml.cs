﻿using System;
using System.ComponentModel;
using System.Configuration;
using System.Threading;
using System.Windows;

namespace Pentair.Desktop.Client
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static readonly object LockData = new object();

        private Discovery svc = null;
        private BrowserService svr = null;

        public MainWindow()
        {
            try
            {
                InitializeComponent();
                svc = new Discovery();
                svc.StartService();
                DisplayError("");
                var root = AppDomain.CurrentDomain.BaseDirectory + ConfigurationManager.AppSettings["root"];
                svr = new BrowserService(svc, root, ConfigurationManager.AppSettings["EndPoint"]);
                Browser.Navigate(ConfigurationManager.AppSettings["EndPoint"] + ConfigurationManager.AppSettings["homePage"]);
            }
            catch (Exception ex)
            {
                DisplayError(ex.Message);
            }
        }

        public void DisplayError(string msg)
        {
            this.Title = msg.Length > 0 ? msg : "Pentair Desktop Control Center";
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            svr.Dispose();
            svc.Dispose();
            base.OnClosing(e);
        }
    }
}
