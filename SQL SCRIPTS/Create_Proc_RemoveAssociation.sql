USE [PathfinderIntellicenter]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (
	SELECT * FROM sys.objects
	WHERE object_id = OBJECT_ID(N'dbo.PRemoveAssociation')
	AND type in (N'P', N'PC')
)
DROP PROCEDURE dbo.PRemoveAssociation
GO

-- =============================================
-- Author			:	Vladimir Beloglazov
-- Create date		:	??/??/????
-- Parameters		:
--	@propertyId		:	Device ID
--	@userId			:	User ID
--	@tokenSuperAdmin:	Super admin access code
--	@tokenAdmin		:	Admin access code
--	@userEmail		:	User Email
-- Description		:	Deletes [Associations] and [UserInvitations] records.
--
-- Modified By			Date		Description
--	Wayne Hui			03/08/2019	Modified logic to not allow owners to remove themself
--	Wayne Hui			05/30/2019	Disallow association to demo accounts to be removed
-- =============================================
CREATE PROCEDURE [dbo].[PRemoveAssociation]
	 @propertyId		AS int
	,@userId			AS uniqueidentifier
	,@tokenSuperAdmin	AS nvarchar(max)
	,@tokenAdmin		AS nvarchar(max)
	,@userEmail			AS nvarchar(max)
AS
BEGIN

	SET NOCOUNT OFF

----TEST
--DECLARE
--	@propertyId		AS int
--	,@userId			AS uniqueidentifier
--	,@tokenSuperAdmin	AS nvarchar(max)
--	,@tokenAdmin		AS nvarchar(max)
--	,@userEmail			AS nvarchar(max)

--SET @propertyId		= 3983
--SET @userId			= 'e40d467e-e112-4434-a101-8fd367c47a12'
--SET @tokenSuperAdmin	= 'UFFFF'
--SET @tokenAdmin		= 'UFFFE'
--SET @userEmail		= ''
----END TEST

	----Disallow demo associations to be removed
	IF NOT (@userId IN ('7f3cab21-d406-4c5a-9b88-17d67f2f51e8') AND @propertyId IN (7788,7799) )
	BEGIN
		BEGIN TRANSACTION

		BEGIN TRY
	
			 --If both [Associations] & [UserInvitations] records presented, they both shall be removed.
			 --If only one of them presented, it shall be removed nevertheless.
			 --Single administrator's [Associations] record cannot be removed.

			DELETE 
			FROM Associations 
			WHERE InstallationId = @propertyId
			AND UserId = @userId
			AND
			(
				(
					(AccessToken = @tokenSuperAdmin OR AccessToken = @tokenAdmin)
					AND
					--(
					--	SELECT COUNT(*) FROM Associations
					--	WHERE InstallationId = @propertyId
					--	AND (AccessToken = @tokenSuperAdmin OR AccessToken = @tokenAdmin)
					--) > 1
					(
						SELECT COUNT(*) 
						FROM [dbo].[AspNetUsers] AU
							JOIN [dbo].[Installations] I
								ON I.InstallationId = @propertyId
						WHERE 
							I.OwnerUserId = @userId
					) < 1
				)
				OR
				(AccessToken <> @tokenSuperAdmin AND AccessToken <> @tokenAdmin)
			)

			DELETE FROM UserInvitations
			WHERE InstallationId = @propertyId
			AND EmailAddress = @userEmail

		END TRY
		BEGIN CATCH

			IF @@TRANCOUNT > 0
				ROLLBACK TRANSACTION

			DECLARE @err_message nvarchar(255)
			SET @err_message = ERROR_MESSAGE()
			RAISERROR (@err_message, 11, 1) -- severity 11-19 will cause returning exception to caller

		END CATCH

		IF @@TRANCOUNT > 0
			COMMIT TRANSACTION
	END


END
GO


