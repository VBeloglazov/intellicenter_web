﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Pentair.Web.Api
{
    public class AuthenticationSucceededResponse : ResponseCommand
    {
        public AuthenticationSucceededResponse(
            string messageID
            )
            : base(CommandString.SuccessMessage, messageID, ResponseString.OK)
        {
        } // AuthenticationSucceededResponse()

        /// <summary>
        /// Always returns an object, valid or not. If invalid, check the DeserializationError property value.
        /// </summary>
        public static new AuthenticationSucceededResponse Deserialize(
            string serializedMessage)
        {
            AuthenticationSucceededResponse command = null;

            try
            {
                command = JsonConvert.DeserializeObject<AuthenticationSucceededResponse>(serializedMessage);
                command.IsValid = true;
            }
            catch (Exception e)
            {
                // Deserialization as AuthenticationSucceededResponse failed

                StringBuilder sbError = new StringBuilder();

                sbError.Append("Cannot deserialize as AuthenticationSucceededResponse. " + e.Message);

                // Try to get at least the base message data
                ResponseCommand baseCommand = ResponseCommand.Deserialize(serializedMessage);

                if (!baseCommand.IsValid)
                {
                    sbError.Append("\n" + baseCommand.DeserializationError);
                }

                command = new AuthenticationSucceededResponse(
                    messageID: baseCommand.MessageID)
                {
                    IsValid = false,
                    DeserializationError = sbError.ToString()
                };
            }

            return command;

        } // Deserialize()

    } // class AuthenticationSucceededResponse
}