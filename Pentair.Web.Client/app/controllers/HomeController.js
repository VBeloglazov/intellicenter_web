'use strict';
var PWC = PWC || {};
PWC.controller('HomeController', [
    '$rootScope', '$scope', '$location', '$filter', 'PropertyService', 'PropertyModel', 'Enumerations',
    function ($rootScope, $scope, $location, $filter, propertyService, propertyModel, enumerations) {
        $scope.Busy = false;
        $scope.AdminSecurity = false;
        $scope.Property = propertyService;
        $scope.Model = propertyModel;
        $scope.$parent.Layout = "layout-base";
        $scope.enumerations = enumerations;
        $scope.heatModes = enumerations.heatModes;
        $scope.heaterStatus = enumerations.heaterStatus;
        $scope.DashboardTabs = propertyModel.dashboardTabs;
        $scope.sysPref = propertyModel.Objects._5451;
        $scope.pumps = propertyModel.Pumps;
        $scope.Schedules = [];
        $scope.securityEnabled = propertyModel.SecurityEnabled;

        // REQUEST NOTIFICATION ON CHANGED.
        $scope.Property.getUpdateClock();   // UPDATE CLOCK
        $scope.Property.getUpdateAlarms(); // request alarms
        $scope.Property.getRemotesList();      // request remotes
        $scope.Property.getNotificationNewFirmwareAvailable();    // Request new firmware available                
        $scope.Property.registertAllPumpStatus($scope.pumps);
        $scope.Property.getUpdateHeatMode();
        $scope.Property.GetSchedules();

        // $scope.processChemistry();

        // display sections 
        $scope.displayPumps = $scope.Property.isCookieOn("main_pumps",false);        //true;
        $scope.displayCircuits = $scope.Property.isCookieOn("main_circuits",true);  // true
        $scope.displayBodies = $scope.Property.isCookieOn("main_bodies",true);     // true
        $scope.schedules = $scope.Property.isCookieOn("main_schedules", false);     //  false;

        // : background and forground
        $scope.getCircuitFormat = function (circuit) {
            if (!circuit)
                return "AreaOFF";
            if (circuit.STATUS ==='ON')
                return "AreaON";
            else
                return "AreaOFF";
        }

        $scope.getCircuit = function (OBJNAM) {
            var circuit;
            angular.forEach($scope.Model.Circuits, function (item) {
                if (item.OBJNAM == OBJNAM)
                    circuit = item;
            });
            return circuit
        }
        
        $scope.isFreezeProtectionOn = function () {
            if ($scope.Model.freezeStatus === "1")
                return true;
            else
                return false;
        };

        $scope.getProtectedMode = function (body) {
            var status = ""
            if (body.FILTER) {
                var circuit = $scope.getCircuit(body.FILTER) // find circuit object
                if (circuit != null && circuit.FREEZE)
                    if ((circuit.FREEZE.toUpperCase() === "ON") )
                        status = "Enabled"
                else
                        status = "Disabled"
            }
            else {
                status = "Disabled"
            }
            return status;
        }

        $scope.getbodybackground = function (body) {
            var running = false;
            switch (body.SUBTYP) {
                case 'SPA':
                case 'POOL':
                    if (body.STATUS === "ON")
                        running = true;  // body is active
                    break;
            }
            if (running === true)
                return "bg-on";
            else
                return "bg-off";

        }

        // convert string to uppercase
        $scope.getUpperCase = function (status) {            
            var str = status;
            return str.toUpperCase();
        }
        // check if body is active
        $scope.getbodyIcon = function (body) {
            var running = false;
            switch (body.SUBTYP) {
                case 'SPA':
                case 'POOL':
                    if (body.STATUS === "ON") 
                        running = true;  // body is active
                    break;                    
            }

            var bodyname = body.SUBTYP;

            /* uncomment next 2 lines when animation image is ready.
            if (running === true)
                bodyname = bodyname + "on"; // is running
            */

            return bodyname.toLowerCase();
        }

        // features icon 
        $scope.getfeaturesIcon= function(){                      

            if (!$scope.Features)
                return "features";
           
            var status = false;
            angular.forEach($scope.Features, function (item) {
                if (item.STATUS){
                    if (item.STATUS === "ON")
                        status = true;
                }
            });

            /* uncomment next 4 lines when animation image is ready.
            if (status === true)
                return "featureson";    //at least 1 feature is active
            else
                return "features";
             */
            return "features";
        }

        // features icon 
        $scope.getlightsIcon = function () {

            if (!$scope.Lights)
                return "lights";

            var status = false;
            angular.forEach($scope.Lights, function (item) {
                if (item.STATUS) {
                    if (item.STATUS === "ON")
                        status = true;
                }
            });

            /* uncomment next 4 lines when animation image is ready.
            if (status === true)
                return "lightson";    //at least 1 feature is active
            else
                return "lights";
             */
            return "lights";

        }

        $scope.isBodyScheduled = function (body) {
            var scheduled = ""
            if (body === null)
                return scheduled;

            propertyModel.Schedules.forEach(function (schedule) {
                if (schedule.ACT === "ON") {  // checking if is active                    
                    if ((schedule.CIRCUIT === "C0006") && (body.SUBTYP === "POOL")) {   // is POOL body and circuit C0006
                        scheduled = "(Scheduled) ";
                    }
                    if ((schedule.CIRCUIT === "C0001") && (body.SUBTYP === "SPA")) {   // is SPA body and circuit C0001
                        scheduled = "(Scheduled) ";
                    }
                }
            });

            // check body status
            if (body.STATUS === "OFF") {
                scheduled = ""; // removing scheduled
            }

            return scheduled;
        }

        $scope.getSolarSensor = function () {   // return solar sensor temperature
            var value = ""            
            if (propertyModel.SolarSensors) {
                propertyModel.SolarSensors.forEach(function (obj) {
                    if (obj.OBJNAM === "SSS11") {  // checking if is active                    
                        if ((obj.SUBTYP === "SOLAR") && (obj.PROBE)) {   // is POOL body and circuit C0006
                            value = " (" + obj.PROBE + '\xB0' +")"; // " add degrees char \xB0 = �";
                        }
                    }
                });
            }
            return value;
        }

        $scope.supportGasConnected = function () {
            // checking version for new format using tMODE for heatMode 
            var version = $scope.Model.getOCPVersion();
            return (version >= $scope.enumerations.CONNECTEDGAS_VERSION)   // check minimum connected version                
        }


        $scope.checkExceptionUltraTemp = function (body, name) {// checking dependency for ultra all names on heaters selection are default by Heat Pump
            var ultra = false
            angular.forEach(body.HEATERS, function (obj) {
                if (obj.SUBTYP === 'ULTRA')
                    ultra = true
            });
            if (ultra)
                name = name.replace("Heat Pump", "UltraTemp")
            return name

        }

        $scope.getHeaterNameSelected = function (body, heaterID, currentHeatMode) {
            var name = "Off";
            
            if (!body)
                return name;    // heater does not exist.           
           
            if ($scope.supportGasConnected()) {
                var mode = 1; // Off
                if (body.MODE) {    // checking if exists
                    mode = body.MODE
                }                
                name = $scope.enumerations.getHeaterModeNameShort(mode);

                name = $scope.checkExceptionUltraTemp(body, name) // checking dependency for ultra all names on heaters selection are default by Heat Pump

                return name;
            }

            angular.forEach(body.HEATERS, function (obj) {
                if (obj.OBJNAM == heaterID) {
                    if (obj.SNAME) {

                        name = obj.SNAME
                    }
                    // overwrite name with fix display name
                    if (obj.DISPLAY) {
                        name = obj.DISPLAY
                    }

                }
            });            
            

            // to fix space size cut the lenth of the following:
            if (name === "Heat Pump Preferred")
                name = "Heat Pump Pref."

            if (name === "Solar Preferred")
                name = "Solar Pref."

            if (name.search("Solar") >= 0)    // check is solar value
                name = name + $scope.getSolarSensor();

            return name;
        }

        $scope.getPumpLedStatus = function (pump) {
            if (!pump)
                return "pump-led ledOff";

            // PUMP STATUS
            // ON = 10
            // OFF = 0

            if (pump.STATUS === '10')
                return "pump-led ledOn";
            else
                return "pump-led ledOff";
        }


        $scope.getPumpFormat = function (pump) {
            if (!pump)
                return "AreaOFF";

            // PUMP STATUS
            // ON = 10
            // OFF = 0

            if (pump.STATUS === '10')
                return "AreaON";
            else
                return "AreaOFF";
        }

        // [begin] listeners -------------------
        (function () {
            buildBodies();
        }());

        // LISTENING BROADCAST 
        $scope.$on('UpdatedHeaters', function (objname) {

            console.log("BodiesController:: processing broadcast UpdatedHeaters: ", objname);
            buildBodies();
        });

        $scope.$on('ObjectDeleted', function (objname) {

            console.log("BodiesController:: processing broadcast ObjectDeleted : ", objname);

            buildBodies();
        });


        $scope.$on('SchedulesRequestParam', function (event, message) {
            console.log(">>>> HomeController - register Schedule for status (ACT) change : ", message);
            //$scope.Property.registerScheduleItem(message);
        });

        

        function buildBodies() {
            console.log("CONTRUCTOR : ---------------------------building bodies  -----------------------")
            var result = [];
            for (var key in propertyModel.SharedBodies) {
                var value = propertyModel.SharedBodies[key];
                var relationShip = {
                    parent: {},
                    child: {}
                };
                if (!propertyModel.Objects[value]) {
                    $rootScope.configurationIssue = false;  // fix bug : pop up message "configuration on the hardware"
                    relationShip = {
                        parent: propertyModel.Objects[key],
                        child: { SNAME: '' }
                    }
                }
                else if (propertyModel.Objects[key].LISTORD < propertyModel.Objects[value].LISTORD) {
                    relationShip = {
                        parent: propertyModel.Objects[key],
                        child: propertyModel.Objects[value]
                    };

                } else {
                    relationShip = {
                        parent: propertyModel.Objects[value],
                        child: propertyModel.Objects[key]
                    };
                }
                relationShip.parent.HEATERS = getHeaters(relationShip.parent.OBJNAM);
                relationShip.child.HEATERS = getHeaters(relationShip.child.OBJNAM);
                relationShip.CHEMISTRY = getChemistries(relationShip.parent.OBJNAM);
                result.push(relationShip);
            }

            $scope.SharedBodies = result;

            $scope.SingleBodies = propertyModel.SingleBodies.map(function (item, index) {
                var obj = propertyModel.Objects[item];
                obj.CHEMISTRY = getChemistries(obj.OBJNAM);
                obj.HEATERS = getHeaters(obj.OBJNAM);
                return obj;
            });

        }

        function getHeaters(bodyName) {
            /*
            var heaters = $filter('objectsByParamValue')(propertyModel.Heaters, ['BODY', 'SHARE'], bodyName);
            heaters.unshift({ SNAME: 'Heater Off', OBJNAM: '00000' });            
            return heaters; 
            */
            var filterHeaters = [];
            angular.forEach(propertyModel.Heaters, function (heater) {
                if (heater.BODY) { // check if BODY exists.
                    if (heater.BODY.indexOf(bodyName) >= 0) {
                        var h1 = heater;
                        if ($scope.supportGasConnected()) { // CHECK NEW SUPPORTED GAS HEATERS IS AVAILABLE
                            switch (h1.SUBTYP) {
                                case "HCOMBO":

                                    h1.DISPLAY = "Hybrid-Gas Only Mode"
                                    h1.MODE = '7';
                                    filterHeaters.push(h1);

                                    var h2 = angular.copy(heater);
                                    h2.DISPLAY = "Hybrid-Heat Pump Only Mode"
                                    h2.MODE = '8';
                                    filterHeaters.push(h2);

                                    var h3 = angular.copy(heater);
                                    h3.DISPLAY = "Hybrid-Hybrid Mode"
                                    h3.MODE = '9';
                                    filterHeaters.push(h3);

                                    var h4 = angular.copy(heater);
                                    h4.DISPLAY = "Hybrid-Dual Mode"
                                    h4.MODE = '10';
                                    filterHeaters.push(h4);

                                    break;
                                default:
                                    h1.MODE = '0';
                                    filterHeaters.push(h1);
                                    break;
                            }
                        }
                        else {
                            h1.MODE = '0';
                            filterHeaters.push(h1);
                        }

                    }
                }
            });

            filterHeaters.unshift({ SNAME: 'OFF', OBJNAM: '00000', DISPLAY: "OFF" });
            return filterHeaters;

        };
        // [end] listeners ****************************

        $scope.showLastTemp = function(body) {
            if(!$scope.sysPref)
                return false;
            return (enumerations.bodyOffStatusList.indexOf(body.STATUS) >= 0 && $scope.sysPref.TEMPNC === 'ON');
        };

        $scope.setStatusLightCircuit = function (obj, checkbox) {
            // checking if security is enabled 
            if ($scope.Model.ACL.SHOMNU.indexOf('l') >= 0) {
                $scope.setStatusCircuit(obj, checkbox);
            }
        }

        // is light menu enabled 
        $scope.isMenuLightEnabled = function () {

            if  ($scope.model.ACL.SHOMNU.indexOf('l') >= 0) 
                return true;
            else
                return false;           
        }

        // is light menu enabled 
        $scope.isMenuFeatureEnabled = function () {

            if ($scope.model.ACL.SHOMNU.indexOf('f') >= 0)
                return true;
            else
                return false;
        }

        $scope.isMenuEnabled = function (flag) {
            if ($scope.model.ACL.SHOMNU.indexOf(flag) >= 0)
                return "";              // normal 
            else
                return "disabledItem";  // format to disabled item
        }

        $scope.isMenuEnabledSchedules = function () {
            if ($scope.model.ACL.SHOMNU.indexOf('e') >= 0) 
                return "display-sched-title";              // normal 
            else
                return "display-sched-title-disable";  // format to disabled item
        }

        
        
        $scope.isMenuEnabledPumps = function () {
            // pool : p
            // spa  : m
            if ( ($scope.model.ACL.SHOMNU.indexOf('p') >= 0) && ($scope.model.ACL.SHOMNU.indexOf('m') >= 0) )                
                return "display-pump-title";              // normal 
            else
                return "display-pump-title-disabled";  // format to disabled item
        }

        $scope.setStatusCircuit = function (obj, checkbox) {
            if (obj && obj.OBJNAM && obj.STATUS) {
                if (obj.STATUS === "OFF")
                    $scope.Property.setTokenCircuit(obj, 'STATUS', "OFF")
                else
                    $scope.Property.setTokenCircuit(obj, 'STATUS', "ON")

            }
        }

        $scope.isChemistryAvailable = function (body) {
            if (!body)
                return false;
            if (body.CHEMISTRY && body.CHEMISTRY.ICHEM)
                return true;

            if (body.CHEMISTRY && body.CHEMISTRY.ICHLOR)
                return true;

            return false;
        }
        $scope.getSaltValue = function (body) {
            var value = "0";
            if (body.CHEMISTRY.ICHLOR)
                value = body.CHEMISTRY.ICHLOR.SALT;
            else {
                if (body.CHEMISTRY.ICHEM)      
                    value = body.CHEMISTRY.ICHEM.SALT;
            }

            return value;            
        };

        $scope.getSectionIcon = function (section) {
            var icon = "down";
            switch (section) {
                case "PUMPS":
                    icon = $scope.displayPumps ? "down" : "right";
                    break;
                case "CIRCUITS":
                    icon = $scope.displayCircuits ? "down" : "right";
                    break;
                case "BODIES":
                    icon = $scope.displayBodies ? "down" : "right";
                    break;
                case "SCHEDULES":
                    icon = $scope.schedules ? "down" : "right";
                    break;                    
            }
            return icon;
        }

        $scope.displaySection = function (section) {
            switch (section) {
                case "PUMPS":
                    return $scope.displayPumps;
                    break;
                case "CIRCUITS":
                    return $scope.displayCircuits;
                    break;
                case "BODIES":
                    return $scope.displayBodies;
                    break;
                case "SCHEDULES":
                    return $scope.schedules;
                    break;

                default:
                    return true;
            }
        }

        // display or hide section
        $scope.toggleSection = function (section) {
            switch (section) {
                case "PUMPS":
                    $scope.displayPumps = !$scope.displayPumps;
                    $scope.Property.setCookieOn("main_pumps", $scope.displayPumps);
                    break;
                case "CIRCUITS":
                    $scope.displayCircuits = !$scope.displayCircuits;
                    $scope.Property.setCookieOn("main_circuits", $scope.displayCircuits);
                    break;
                case "BODIES":
                    $scope.displayBodies = !$scope.displayBodies;
                    $scope.Property.setCookieOn("main_bodies", $scope.displayBodies);
                    break;
                case "SCHEDULES":
                    $scope.schedules = !$scope.schedules;
                    $scope.Property.setCookieOn("main_schedules", $scope.schedules);
                    break;
            }
        }


        $scope.getPumpType = function (pump) {
            if (!pump)
                return ""
            switch(pump.SUBTYP){
                case 'SINGLE':  return "Single Speed";
                case 'DUAL':    return "Dual Speed";
                case 'SPEED':   return "VS";
                case 'FLOW':    return "VF";
                case 'VSF':     return "VSF";
                case 'SPEED': 
                case 'FLOW': return 'VSF';
                default: return "Pump";
            }
        }

        $scope.getPumpRunningFormat = function (value) {
            if (!value)
                return " "
            switch (pump.SUBTYP) {
                case 'SINGLE': return "Single Speed";
                case 'DUAL': return "Dual Speed";
                case 'SPEED': return "VS";
                case 'FLOW': return "VF";
                case 'VSF': return "VSF";
                case 'SPEED':
                case 'FLOW': return 'VSF';
                default: return "Pump";
            }
        }

        $scope.getPumpItem = function (pump,param) {
            if (!pump)
                return ""
            var value = 0;
            switch (param) {
                case 'RPM':
                    if (pump.RPM)
                        value = "Speed " + pump.RPM + " RPM";
                    else
                        value = "Speed 0 RPM";
                    break;

                case 'GPM':
                    if (pump.GPM)
                        value = "Flow Rate "+ pump.GPM + " GPM";
                    else
                        value = "Flow Rate 0 GPM";
                    break;

                case 'PWR':
                    if (pump.PWR)
                        value = "Power Usage "+pump.PWR + " Watts";
                    else
                        value = "Power Usage 0 Watts";
                    break;

                case 'SNAME':
                    if (pump.SNAME)
                        value = pump.SNAME;
                    else
                        value = $scope.getPumpType(pump);
                    break;

                case 'STATUS':
                    if (pump.STATUS)
                        value = "";
                    else
                        value = "";
                    break;

                default:
                    value = "0";
                    break;
            }
            return value;
        }

        $scope.isSharePresent = function (body) {
            //return propertyModel.ShareBody;       // this a global variable
            
            if (body.parent === undefined)   // check if parent exist  // check if parent exist
                return false;

            //console.log("-+++++++++++++++++++++ HOME PAGE BODY = " + body.parent.SHARE);
            if (body.parent.SHARE === "00000")
                return false;
            else
                return true;
            
        };

        
        $scope.getBodyIconImage = function (body) {
            var name = "";
            name = body.SNAME; //removing parent so is passed on the caller. body.parent.SNAME   
            if (body.HTMODE) {//HTSRCTYP) {
                if (body.STATUS === "ON") {
                    switch (body.HTMODE) {

                        case "1": name = name + " Flame"; break;
                        case "2": name = name + " Solar"; break;
                        case "3": name = name + " Flake"; break;
                        // FLAME ICONS
                        case "4": name = name + " Flame"; break;    // ultra
                        case "5": name = name + " Flame"; break;    // hybrid
                        case "6": name = name + " Flame"; break;    // mastertemp
                        case "7": name = name + " Flame"; break;    // max-e temp

                        case "0": default: name = name + " None"; break;
                    }
                }
            }
            return name;

        }

        $scope.getHeatingStatus = function (body) {
            var name = "OFF";
            if (body.HTMODE) {
                if (body.STATUS === "ON") {
                    var SRC = body.HTSRC;
                    switch (body.HTMODE) {
                        case "0":   // off
                            name = "OFF";
                            break;

                        case "1":   // gas on   "flameblinking";                            
                        case "2":   // solar on :                   "sunblinking";                            
                        case "3":   // heatpump on, ultratemp on:   "flameblinking"
                        case "4":   // Ultra on:                    "flameblinking"
                        case "5":   // hybrid on:                   "flameblinking"
                        case "6":   // master temp on:              "flameblinking"
                        case "7":   // Max-e temp on:               "flameblinking"
                        case "10":  // solar cooling :              "flakeblinking"
                        case "11":  // ultratemp cooling:           "flakeblinking" 
                            name = "ON" //
                            break;
                        default:
                            name = "OFF"; break;
                    }
                }
            }
            return name;
        };

        $scope.getCurrentUnits = function () {
            var units = "ENGLISH";
            if ($scope.sysPref && $scope.sysPref.MODE) {
                //console.log("-----------------Units : ", $scope.sysPref.MODE);
                units = $scope.sysPref.MODE;
            } else {
               // console.log("-----------------ERROR: Units is not ready yet ------")
            }

            return units;
        }

        
        $scope.getBodyStatusIconName = function (body) {
            var name = body.SNAME;
            if (body.HTMODE) {
                if (body.STATUS === "ON") {
                    var SRC = body.HTSRC;
                    switch (body.HTMODE) { 
                        case "0":   // off
                            name = ""; break;
                        case "1":   // gas on
                            name = "flameblinking"; break;
                        case "2":   // solar on
                            name = "sunblinking"; break;
                        case "3":   // heatpump on, ultratemp on
                            name = "flameblinking"; break;
                        case "4":   // ultra temp on
                            name = "flameblinking"; break;
                        case "5":   // hybrid on
                            name = "flameblinking"; break;
                        case "6":   // master temp on
                            name = "flameblinking"; break;
                        case "7":   // MAX-E temp on
                            name = "flameblinking"; break;
                        case "10":  // solar cooling
                            name = "flakeblinking"; break;
                        case "11":  // ultratemp cooling
                            name = "flakeblinking"; break;
                            /*
                            angular.forEach(body.HEATERS, function (item) {
                                if (body.HTSRC === item.HNAME) {
                                }
                            });
                            */
                        default:
                            name = ""; break;
                    }

                }
            }
            return name;
        };


        //***** schedules begin

        $scope.$on('SchedulesUpdated', function (event, message) {
            refreshListSchedules();
        });
        function refreshListSchedules() {
            $scope.Schedules = [];
            propertyModel.Schedules.forEach(function (schedule) {
                var newSchedule = {
                    OBJNAM: schedule.OBJNAM,
                    OBJTYP: schedule.OBJTYP,
                    SNAME: schedule.SNAME,
                    LISTORD: schedule.LISTORD,
                    CIRCUIT: schedule.CIRCUIT,
                    DAY: schedule.DAY,
                    START: schedule.START,
                    TIME: $filter('translateTime')(schedule.TIME),
                    STOP: schedule.STOP,
                    SINGLE: schedule.SINGLE,
                    TIMOUT: $filter('translateTime')(schedule.TIMOUT),
                    GROUP: schedule.GROUP,
                    HEATER: schedule.HEATER,
                    COOLING: schedule.COOLING,
                    LOTMP: schedule.LOTMP,
                    SMTSRT: schedule.SMTSRT,
                    DNTSTP: schedule.DNTSTP,
                    STATUS: schedule.STATUS,
                    ACT: schedule.ACT,
                    startTime: moment(schedule.TIME, 'HHmmSS').toDate(),
                    stopTime: moment(schedule.TIMOUT, 'HHmmSS').toDate(),
                    days: schedule.DAY.split(''),
                    heatMode: '',     // don't change default value            
                    bodyObject: {},
                    body: false,
                    smartStart: false,
                    deleteFlag: false,
                    open: false
                };
                if (newSchedule.startTime === 'Invalid Date') {
                    newSchedule.startTime = PWC.convertToDate(propertyModel.Objects["_C10C"].MIN, propertyModel.Objects["_C10C"].TIMZON);
                    newSchedule.stopTime = addTwohours(PWC.convertToDate(propertyModel.Objects["_C10C"].MIN, propertyModel.Objects["_C10C"].TIMZON));
                }
                if (newSchedule.STATUS === 'OFF') {
                    newSchedule.disableStatus = true;
                }

                var obj = propertyModel.Objects[newSchedule.CIRCUIT];
                if (obj) {
                    if (obj.SUBTYP === 'POOL' || obj.SUBTYP === 'SPA') {
                        if (newSchedule.HEATER && newSchedule.HEATER !== '00000' && newSchedule.HEATER !== '00001') {
                            newSchedule.bodyObject = $filter('objectsByParamValue')(propertyModel.Bodies, ['BODY', 'SHARE'], obj.BODY)[0];
                            switch (newSchedule.HEATER) {
                                case "00000":
                                    newSchedule.heatMode = "Off";
                                    break;

                                case "00001":
                                case "HOLD":
                                    newSchedule.heatMode = $scope.dontchange;
                                    break;

                                default:
                                    //newSchedule.heatMode = propertyModel.Objects[newSchedule.HEATER].SNAME;
                                    newSchedule.heatMode = propertyModel.Objects[newSchedule.HEATER].DISPLAY;
                                    break;
                            }
                            var heater = $filter('objectsByName')(propertyModel.Heaters, newSchedule.HEATER);
                            newSchedule.coolingCapable = $filter('coolingCapableHeater')(heater);
                            newSchedule.heatingCapable = $filter('heatingCapable')(heater);
                        }
                        newSchedule.body = true;
                    }
                    if (newSchedule.HEATER === '00000') {
                        newSchedule.coolingCapable = false;
                        newSchedule.heatingCapable = false;
                        newSchedule.heatMode = "Off";
                    }

                    if (newSchedule.HEATER === 'HOLD') { // adding hold = don't Change
                        newSchedule.coolingCapable = false;
                        newSchedule.heatingCapable = false;
                        newSchedule.heatMode = $scope.dontchange;
                    }

                    if (obj.USAGE && obj.USAGE !== 'USAGE') {
                        newSchedule.smartStart = obj.USAGE.indexOf('E') > -1;
                    }
                }

                if (schedule.VACFLO === 'OFF') {
                    $scope.Schedules.push(newSchedule);
                }
            });
        }
        //***** schedules end

       
        

        (function () {
            var result = [];
            for (var key in propertyModel.SharedBodies) {
                var value = propertyModel.SharedBodies[key];
                var relationShip = {
                    parent: {},
                    child: {},
                    SNAME: '',
                    LISTORD: "0"
                };
                if(!propertyModel.Objects[value]) {
                    $rootScope.configurationIssue = false;  // fix bug : pop up message "configuration on the hardware"
                    relationShip = {
                        parent: propertyModel.Objects[key],
                        child: {SNAME:''}
                    }
                }
                else if (propertyModel.Objects[key].LISTORD < propertyModel.Objects[value].LISTORD) {
                    relationShip = {
                        parent: propertyModel.Objects[key],
                        child: propertyModel.Objects[value]
                    };
                } else {
                    relationShip = {
                        parent: propertyModel.Objects[value],
                        child: propertyModel.Objects[key]
                    };
                }
                relationShip.SNAME = relationShip.parent.SNAME + '/' + relationShip.child.SNAME;
                relationShip.LISTORD = relationShip.parent.LISTORD;
                relationShip.CHEMISTRY = getChemistries(relationShip.parent.OBJNAM);
                result.push(relationShip);
            }

            $scope.SharedBodies = result;

            $scope.SingleBodies = propertyModel.SingleBodies.map(function (item, index) {
                var obj = propertyModel.Objects[item];
                obj.CHEMISTRY = getChemistries(obj.OBJNAM);
                return obj;
            });


        }());
        $scope.Lights = $filter('objectsBySection')(propertyModel.Circuits, 'l');
        $scope.Features = $filter('objectsByCharSet')(propertyModel.Circuits, 'fw');

        function initAllSingleBodies() {
            console.log("---init all objects--- ")
            $scope.SingleBodies = propertyModel.SingleBodies.map(function (item, index) {
                var obj = propertyModel.Objects[item];
                obj.CHEMISTRY = getChemistries(obj.OBJNAM);
                return obj;
            });
        }

        function getChemistries(bodyName) {
            var chemistry = {
                ICHEM: null,
                ICHLOR: null
            };
            var panelType = 'SHARE';
            if (propertyModel.PANID) { // "SINGLE",    // NONE, SINGLE, SHARE, DUAL
                console.log("========== paneltype ", propertyModel.PANID);
                panelType = propertyModel.PANID;
            }
        
            //var chemistries = $filter('objectsByParamValue')(propertyModel.Chemistry, ['BODY', 'SHARE'], bodyName);
            //chemistry.ICHLOR = $filter('objectsBySubType')(chemistries, 'ICHLOR')[0];
            //chemistry.ICHEM = $filter('objectsBySubType')(chemistries, 'ICHEM')[0];

            console.log("============= CHEMISTRY CHANGES");

            /*
            var allChemistries = $filter('filterOutByObjectType')(propertyModel.Objects, 'CHEM');
            var chemistries = $filter('objectsByParamValue')(allChemistries, ['BODY', 'SHARE'], bodyName);
            chemistry.ICHLOR = $filter('objectsBySubType')(chemistries, 'ICHLOR')[0];
            chemistry.ICHEM = $filter('objectsBySubType')(chemistries, 'ICHEM')[0];
            */

            /*
            var allChemistries = $filter('filterOutByObjectType')($scope.Model, 'CHEM');
                    
            var chemistries;
            switch(panelType){
                case 'SINGLE':
                    chemistries = $filter('objectsByParamValue')(allChemistries, ['BODY', 'SINGLE'], bodyName);
                    break;
                
                case 'DUAL':
                case 'SHARE':
                default:
                    chemistries = $filter('objectsByParamValue')(allChemistries, ['BODY', 'SHARE'], bodyName);
                    break;
            }            
                        
            chemistry.ICHLOR = $filter('objectsBySubType')(chemistries, 'ICHLOR')[0];
            chemistry.ICHEM = $filter('objectsBySubType')(chemistries, 'ICHEM')[0];

            */

            angular.forEach($scope.Model.Chemistry, function (obj) {                

                if (
                    ((obj.BODY === bodyName) || (panelType === 'SHARE')) &&
                     (obj.OBJTYP  === 'CHEM')                         
                    ) {
                    switch(obj.SUBTYP){
                        case 'ICHEM':
                            chemistry.ICHEM = propertyModel.Objects[obj.OBJNAM];
                            break;
                        case 'ICHLOR':
                            chemistry.ICHLOR = propertyModel.Objects[obj.OBJNAM];
                            break;
                    }        
                }
            });

            return chemistry;
        }
    }
]);
