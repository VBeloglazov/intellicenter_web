﻿// var PWC = PWC || {};
// PWC.directive('head', ['$rootScope', '$compile',
//     function ($rootScope, $compile) {
//         return {
//             restrict: 'E',
//             link: function (scope, elem) {
//                 var newStyles = ["content/css/select.min.css", "content/css/bootstrap.min.css", "content/fonts/iconstyles.css", "content/css/main.css"];
//
//                 var oldStyles = ["content/css/loading-bar.css", "content/css/grid.css", "content/css/normalize.min.css", "content/css/pentair.css", "content/css/ui-grid.css", "content/css/ang-accordion.css", "content/css/dropdowns.css"];
//                 var html = '<link rel="stylesheet" ng-repeat="(routeCtrl, cssUrl) in routeStyles" ng-href="{{cssUrl}}" />';
//                 elem.append($compile(html)(scope));
//                 scope.routeStyles = {};
//                 $rootScope.UseOldSkin = false;
//
//                 $rootScope.$on('$routeChangeStart', function (e, next, current) {
//                     if (next && next.$$route) {
//                         manageDesignMerge(next.$$route.originalPath);
//                     }
//
//                     if (current && current.$$route && current.$$route.css) {
//                         if (!angular.isArray(current.$$route.css)) {
//                             current.$$route.css = [current.$$route.css];
//                         }
//                         angular.forEach(current.$$route.css, function (sheet) {
//                             delete scope.routeStyles[sheet];
//                         });
//                     }
//                     if (next && next.$$route && next.$$route.css) {
//                         if (!angular.isArray(next.$$route.css)) {
//                             next.$$route.css = [next.$$route.css];
//                         }
//                         angular.forEach(next.$$route.css, function (sheet) {
//                             scope.routeStyles[sheet] = sheet;
//                         });
//                     }
//
//                     function manageDesignMerge(route) {
//                         var updatedRoutes = ['/login', '/passwordAssistance', '/retrievePassword/:user', '/systemInformation', '/property', '/installations', '/schedules', '/manageUsers', '/settings', '/configurations'];
//
//                         if (updatedRoutes.indexOf(route) > -1) {
//                             angular.forEach(oldStyles, function (sheet) {
//                                 delete scope.routeStyles[sheet];
//                             });
//                             angular.forEach(newStyles, function (sheet) {
//                                 scope.routeStyles[sheet] = sheet;
//                             });
//                         } else {
//                             scope.routeStyles = {};
//                             //  $rootScope.UseOldSkin = true;
//                             angular.forEach(oldStyles, function (sheet) {
//                                 scope.routeStyles[sheet] = sheet;
//                             });
//                             angular.forEach(newStyles, function (sheet) {
//                                 delete scope.routeStyles[sheet];
//                             });
//                         }
//                     }
//                 });
//             }
//         };
//     }
// ]);
