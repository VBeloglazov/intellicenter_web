﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pentair.Web.Api
{
    public class ReRegisterPropertyReturn
    {
        public int HttpStatusCode { get; set; }
        public string NewSecret { get; set; }

    } // class ReRegisterPropertyReturn
}