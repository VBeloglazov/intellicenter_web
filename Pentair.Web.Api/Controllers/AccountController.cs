﻿using System;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using System.Web;
using System.Data.Entity.Migrations;
using System.Text;

using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;

using SERVICE_SHARED;
using System.Text.RegularExpressions;

namespace Pentair.Web.Api
{
    [Authorize]
    [RoutePrefix("api/Account")]
    public class AccountController : ApiController
    {
        private const string EVENT_SOURCE = "AccountController";

        private const string LocalLoginProvider = "Local";

        public ApplicationUserManager UserManager
        {
            get { return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>(); }
        }
        private ApplicationUserManager _userManager;

        public AccountController()
        {
        }

        public AccountController(
            ApplicationUserManager userManager)
        {
            _userManager = userManager;

            UserManager.UserValidator = new UserValidator<ApplicationUser>(UserManager)
            {
                AllowOnlyAlphanumericUserNames = false
            };
        }

        // POST api/Account/Logout
        [Route("Logout")]
        public IHttpActionResult Logout()
        {
            Authentication.SignOut(CookieAuthenticationDefaults.AuthenticationType);
            return Ok();
        }

        // POST api/Account/Register
        [Route("Register")]
        [AllowAnonymous]
        public async Task<IHttpActionResult> Register(RegisterViewModel model)
        {
            // Deprecate?
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = new ApplicationUser()
            {
                UserName = model.Email.ToLowerInvariant(),
                Email = model.Email.ToLowerInvariant(),
                FirstName = model.FirstName,
                LastName = model.LastName
            };

            IdentityResult result = await this.UserManager.CreateAsync(user, model.Password);

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }


        // POST api/Account/ForgotPassword
        [Route("ForgotPassword")]
        [AllowAnonymous]
        public async Task<IHttpActionResult> ForgotPassword(
            ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                ApplicationUser user = await this.UserManager.FindByNameAsync(model.Email.ToLowerInvariant());

                if (user == null)
                {
                    return NotFound();
                }

                // Set password expiration
                string code = await this.UserManager.GenerateEmailConfirmationTokenAsync(user.Id);

                string resetUrl = "";
                string host = "";

                //build inviteUrl & Email
                if (Request.RequestUri.Host.ToLower() == "localhost")
                {
                    host = "http://localhost:44300";
                }
                else
                {
                    host = Request.RequestUri.Scheme + "://" + Request.RequestUri.Host;
                }

                resetUrl = host + "/#/retrievePassword/";

                // 1.0.0.17
                try
                {
                    EmailApi_Smtp.SendEmail(
                        subject: "Reset password",
                        addressTo: user.Email,
                        isBodyHtml: false,
                        body:
                            String.Format("You have requested to reset your password, \n\nvisit {0}{1}?code={2} to enter a new password",
                                resetUrl,
                                user.UserName,
                                System.Web.HttpUtility.UrlEncode(code)
                            )
                    );

                    return Ok("OK");
                }
                catch (Exception e)
                {
                    LogManager.LogErrorAsync(
                        source: EVENT_SOURCE,
                        eventBriefDescription: "AccountController.ForgotPassword() error",
                        installationId: 0,
                        userId: Guid.Empty,
                        connectionId: Guid.Empty,
                        eventFullDescription:
                            ExceptionManager.CustomizeException(e).ErrorMessage(errorHeader: null, addStackTrace: false)
                    );

                    return InternalServerError(e);
                }
            }

            // If we got this far, something failed, redisplay form
            return BadRequest(ModelState);
        
        } // ForgotPassword()

        [Route("ValidateEmail")]
        [AllowAnonymous]
        [HttpPost]
        public async Task<IHttpActionResult> ValidateToken(ValidateTokenViewModel model)
        {
            // Deprecate?
            if (ModelState.IsValid)
            {
                var user = await this.UserManager.FindByNameAsync(model.User.ToLowerInvariant());
                if (user == null)
                {
                    return NotFound();
                }

                var result = await this.UserManager.ConfirmEmailAsync(user.Id, model.Code);
                if (result.Succeeded)
                {
                    model.Code = await this.UserManager.GeneratePasswordResetTokenAsync(user.Id);
                    return Ok(model);
                }
                return NotFound();
            }
            return BadRequest(ModelState);
        }

        // POST api/Account/ResetPassword
        [Route("ResetPassword")]
        [AllowAnonymous]
        [HttpPost]
        public async Task<IHttpActionResult> ResetPassword(
            ResetPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await this.UserManager.FindByNameAsync(model.Email.ToLowerInvariant());
                if (user == null)
                {
                    return NotFound();
                }

                IdentityResult result = await this.UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);

                if (result.Succeeded)
                {
                    SendPasswordChangeConfirmation(user);
                    return Ok("OK");
                }
                else
                {
                    StringBuilder sb = new StringBuilder();
                    foreach(string error in result.Errors)
                    {
                        if (sb.Length > 0) sb.Append("\n");
                        sb.Append(error);
                    }

                    LogManager.LogErrorAsync(
                        source: EVENT_SOURCE,
                        eventBriefDescription: "AccountController.ResetPassword() error",
                        installationId: 0,
                        userId: Guid.Empty,
                        connectionId: Guid.Empty,
                        eventFullDescription: sb.ToString()
                    );

                    return GetErrorResult(result);
                }
            }

            return BadRequest(ModelState);

        } // ResetPassword()

        // POST api/Account/ValidatePassword
        [Route("ValidatePassword")]
        [HttpPost]
        public async Task<IHttpActionResult> ValidatePassword(ManageUserViewModel model)
        {
            //?Deprecate
            var user = await this.UserManager.FindByIdAsync(User.Identity.Name);
            if (await this.UserManager.CheckPasswordAsync(user, model.OldPassword))
            {
                return Ok("OK");
            }
            return NotFound();
        }

        // POST api/Account/ValidateEmail
        [Route("VerifyEmail")]
        [HttpPost]
        public async Task<IHttpActionResult> VerifyEmail(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await this.UserManager.FindByIdAsync(User.Identity.Name);
                if (user.Email.ToLower() == model.Email.ToLower())
                {
                    return Ok("OK");
                }
                return NotFound();
            }
            return BadRequest(ModelState);
        }

        // POST api/Account/ChangePassword
        [Route("ChangePassword")]
        public async Task<IHttpActionResult> ChangePassword(
            ManageUserViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IdentityResult result = await this.UserManager.ChangePasswordAsync(User.Identity.Name, model.OldPassword, model.NewPassword);

                    if (result.Succeeded)
                    {
                        var user = this.UserManager.FindById(User.Identity.Name);
                        SendPasswordChangeConfirmation(user);
                        return Ok("OK");
                    }
                    return GetErrorResult(result);
                }
                return BadRequest(ModelState);
            }
            catch (Exception e)
            {
                LogManager.LogErrorAsync(
                    source: EVENT_SOURCE,
                    eventBriefDescription: "AccountController.ChangePassword() error",
                    installationId: 0,
                    userId: Guid.Empty,
                    connectionId: Guid.Empty,
                    eventFullDescription:
                        ExceptionManager.CustomizeException(e).ErrorMessage(errorHeader: null, addStackTrace: false)
                );

                return InternalServerError(e);
            }
        
        } // ChangePassword()

        // GET api/Account/InviteInfo/id
        [Route("InviteInfo")]
        [AllowAnonymous]
        [HttpGet]
        public IHttpActionResult InviteInfo(
            string Id)
        {
            try
            {
                using (DbContextPentair db = DbContextPentair.New())
                {
                    TUserInvitation invitationDB = db.GetUserInvitation(new Guid(Id));

                    if (invitationDB == null)
                    {
                        return NotFound();
                    }

                    if (invitationDB.Status != "UNKNOWN")
                    {
                        return BadRequest("Used");
                    }

                    if (invitationDB.UpdateDateTime.AddHours(StaticData.InviteValidAge) < DateTime.Now)
                    {
                        return BadRequest("Expired");
                    }

                    return Ok(
                        new
                        {
                            PropertyName = invitationDB.InstallationName, // VB: This is [Installations].Name -- see PGetUserInvitation
                            Email = invitationDB.EmailAddress
                        }
                    );
                }
            }
            catch (Exception e)
            {
                LogManager.LogErrorAsync(
                    source: EVENT_SOURCE,
                    eventBriefDescription: "AccountController.InviteInfo() error",
                    installationId: 0,
                    userId: Guid.Empty,
                    connectionId: Guid.Empty,
                    eventFullDescription:
                        ExceptionManager.CustomizeException(e).ErrorMessage(errorHeader: null, addStackTrace: false)
                );

                return InternalServerError(e);
            }
        
        } // InviteInfo()

        // POST api/Account/RemoveInstallation
        [Route("RemoveInstallation")]
        [HttpPost]
        public IHttpActionResult RemoveInstallation(
            ManagePropertyViewModel model)
        {
            try
            {
                var userId = User.Identity.GetOwinUserId();

                int propertyId;
                if (int.TryParse(model.PropertyId, out propertyId))
                {
                    using (DbContextPentair db = DbContextPentair.New())
                    {
                        db.RemoveUserAssociation(userId, propertyId);

                    } // using (DbContextPentair db)

                    /* TODO: VB LastAdminError check can and must be done on the client side.
                    if (result != UserManagementResult.Success)
                    {
                        if (result == UserManagementResult.LastAdminError)
                        {
                            return new BadRequestResult(Request);
                        }
                        else
                        {
                            return new NotFoundResult(Request);
                        }
                    }
                    */
                }

                return new OkResult(Request);
            }
            catch
            {
                return new InternalServerErrorResult(Request);
            }
        
        } // RemoveInstallation()

        // POST api/Account/AcceptInvite
        [Route("AcceptInvite")]
        [AllowAnonymous]
        public async Task<IHttpActionResult> AcceptInvite(
            AcceptInviteViewModel model)
        {
            if (ModelState.IsValid)
            {
                TUserInvitation invitationDB = null;

                using (DbContextPentair db = DbContextPentair.New())
                {
                    invitationDB = db.GetUserInvitation(new Guid(model.InviteId));

                } // using (DbContextPentair db)

                if (invitationDB == null)
                    return NotFound();

                if (invitationDB.Status != "UNKNOWN")
                    return BadRequest("Used");

                if (invitationDB.UpdateDateTime.AddHours(StaticData.InviteValidAge) < DateTime.Now)
                    return BadRequest("Expired");

                string userName = model.Email;

                // Get user from authenticated session
                var user = await this.UserManager.FindByIdAsync(User.Identity.Name);

                // Check if the provided user exist since the session is not authenticated
                if (user == null)
                {

                    user = await this.UserManager.FindByNameAsync(userName.ToLowerInvariant());

                    //check Password
                    if (user != null && !await this.UserManager.CheckPasswordAsync(user, model.Password))
                        return BadRequest("Invalid Password");

                }

                // If user does not exist, create user from email address
                if (user == null)
                {
                    user = new ApplicationUser
                    {
                        UserName = userName.ToLowerInvariant(),
                        Email = model.Email.ToLowerInvariant(),
                        FirstName = model.FirstName,
                        LastName = model.LastName
                    };

                    IdentityResult result = await this.UserManager.CreateAsync(user, model.Password);

                    if (!result.Succeeded)
                        return InternalServerError(new Exception("Failed to Create User"));

                    // Set up email confirmation TODO Why are we automatically confirming this email?
                    var code = await this.UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                    var confirm = await this.UserManager.ConfirmEmailAsync(user.Id, code);
                    if (confirm != IdentityResult.Success)
                        return InternalServerError(new Exception("Failed to confirm email."));
                }

                // Associate user with a property 

                using (DbContextPentair db = DbContextPentair.New())
                {
                    db.AddUpdateAssociation(
                        userId: new Guid(user.Id),
                        installationId: invitationDB.InstallationId ?? 0,
                        accessToken: invitationDB.AccessToken,
                        invitationId: invitationDB.InvitationId
                    );

                } // using (DbContextPentair db)

                var response = new InviteResponse
                {
                    Email = user.Email,
                    InstallationId = invitationDB.InstallationId ?? 0,
                    UserName = user.UserName
                };

                return Ok(response);

            } // if (ModelState.IsValid)

            return BadRequest(ModelState);

        } // AcceptInvite()

        // POST api/Account/ChangeUserName
        [Route("ChangeUserName")]
        [HttpPost]
        public async Task<IHttpActionResult> ChangeUserName(
            ChangeUserNameViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    ApplicationUser user = await this.UserManager.FindByNameAsync(model.OldEmail.ToLowerInvariant());

                    if (!await this.UserManager.CheckPasswordAsync(user, model.Password))
                        return BadRequest("Invalid Password");

                    if (user == null)
                        return NotFound();

                    ApplicationUser existing = await this.UserManager.FindByNameAsync(model.NewEmail.ToLowerInvariant());

                    if (existing != null)
                        return BadRequest("Username Already In use");

                    if (model.OldEmail != user.UserName)
                        return BadRequest("Old Email does not match");

                    UserInvitation invitationNew = new UserInvitation()
                    {
                        InvitationId = Guid.NewGuid(),
                        AccessToken = "",
                        EmailAddress = model.NewEmail,
                        InstallationId = null,
                        EmailId = ""
                    };

                    using (DbContextPentair db = DbContextPentair.New())
                    {
                        db.AddUserInvitation(invitationNew);
                    }

                    string response = SendChangeEmailConfirmation(invitationNew, model.OldEmail);

                    if (!(response == ResponseString.OK || response == ResponseString.Created))
                        return InternalServerError();

                    return Ok("OK");
                }
                catch (Exception e)
                {
                    LogManager.LogErrorAsync(
                        source: EVENT_SOURCE,
                        eventBriefDescription: "AccountController.ChangeUserName() error",
                        installationId: 0,
                        userId: Guid.Empty,
                        connectionId: Guid.Empty,
                        eventFullDescription:
                            ExceptionManager.CustomizeException(e).ErrorMessage(errorHeader: null, addStackTrace: false)
                    );

                    return InternalServerError(e);
                }

            } // if (ModelState.IsValid)

            return BadRequest(ModelState);

        } // ChangeUserName()

        // Get api/Account/ConfirmUserNameChange
        [Route("ConfirmUserNameChange")]
        [AllowAnonymous]
        public Task<IHttpActionResult> ConfirmUserNameChange(
            ConfirmUserNameChangeViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (DbContextPentair db = DbContextPentair.New())
                    {
                        TUserInvitation invitationDB = db.GetUserInvitation(new Guid(model.InviteId));

                        if (invitationDB == null)
                            return Task.FromResult((IHttpActionResult)NotFound());

                        if (invitationDB.Status != "UNKNOWN")
                            return Task.FromResult((IHttpActionResult)BadRequest("Used"));

                    } // using (DbContextPentair db)

                    string parsedOldEmail = model.OldUserName;

                    using (ApplicationUserContext identity = new ApplicationUserContext())
                    {
                        ApplicationUser user = identity.Users.FirstOrDefault(x => x.UserName == parsedOldEmail);

                        if (user != null)
                        {
                            string oldUserName = user.Email;

                            ApplicationUser existing = identity.Users.FirstOrDefault(x => x.UserName == model.UserName);

                            if (existing != null)
                                return Task.FromResult((IHttpActionResult)BadRequest("Username Already In use"));

                            if (model.OldUserName != user.UserName)
                                return Task.FromResult((IHttpActionResult)BadRequest("Old Email does not match"));

                            user.Email = model.UserName;
                            user.UserName = model.UserName;
                            identity.Users.Attach(user);
                            var entry = identity.Entry(user);
                            entry.Property(u => u.Email).IsModified = true;
                            entry.Property(u => u.UserName).IsModified = true;
                            identity.SaveChanges();

                            // 1.0.0.17
                            EmailApi_Smtp.SendEmail(
                                subject: "Email Change",
                                addressTo: user.Email,
                                isBodyHtml: false,
                                body:
                                    "Congratulations, you have successfully changed your email address. I look forward to seeing you again."
                            );

                            // 1.0.0.17
                            EmailApi_Smtp.SendEmail(
                                subject: "Email Change",
                                addressTo: oldUserName,
                                isBodyHtml: false,
                                body:
                                    "Congratulations, you have successfully changed your email address to: " + user.Email
                            );

                            using (DbContextPentair db = DbContextPentair.New())
                            {
                                // 1.0.0.17
                                db.RemoveUserInvitation(new Guid(model.InviteId));
                            }

                            return Task.FromResult((IHttpActionResult)Ok("OK"));
                        }

                        return Task.FromResult((IHttpActionResult)NotFound());

                    } // using (ApplicationUserContext identity)
                }
                catch (Exception e)
                {
                    LogManager.LogErrorAsync(
                        source: EVENT_SOURCE,
                        eventBriefDescription: "AccountController.ConfirmUserNameChange() error",
                        installationId: 0,
                        userId: Guid.Empty,
                        connectionId: Guid.Empty,
                        eventFullDescription:
                            ExceptionManager.CustomizeException(e).ErrorMessage(errorHeader: null, addStackTrace: false)
                    );

                    return Task.FromResult((IHttpActionResult)InternalServerError(e));
                }

            } // if (ModelState.IsValid)

            return Task.FromResult((IHttpActionResult)BadRequest(ModelState));

        } // ConfirmUserNameChange()

        // Get api/Account/VersionNumber
        [Route("VersionNumber")]
        [AllowAnonymous]
        public string GetVersionNumber()
        {
            return Properties.Settings.Default.VersionNumber;

        } // GetVersionNumber()

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (UserManager != null)
                    this.UserManager.Dispose();
            }

            base.Dispose(disposing);
        }

        public string ValidateUser(string userName, string password)
        {
            // 05/17/2019 Check for email rules first
            if (userName == string.Empty)
            {
                return null;
            }

            // Separate the email to recipient and domain to make sure we have a legit email address
            string[] theMessage = userName.Split('@');
            if (theMessage.Length != 2)
            {
                return null;
            }

            var recipient = theMessage[0];
            var domain = theMessage[1];

            // Check for correct domain style which allows alphanumeric and . -
            var regexDomain = @"^\w[\w\.-]*\w$";
            Match matchDomain = Regex.Match(domain, regexDomain, RegexOptions.IgnoreCase);
            if (!matchDomain.Success)
            {
                return null;
            }

            // make sure it has a top-level domain
            string[] theDomain = domain.Split('.');
            if (theDomain.Length < 2 || theDomain[0] == string.Empty || theDomain[1] == string.Empty)
            {
                return null;
            }

            // Check for recipient name which allows alphanumeric and ! # $ % & ' * + - / = ? ^ _ ` { . |
            // The first and last character has to be alphanumeric
            var regexRecipient = @"^\w[\w\!$%'*/=?^_`{|.+&#-]*\w$";
            Match matchRecipient = Regex.Match(recipient, regexRecipient, RegexOptions.IgnoreCase);
            if (!matchRecipient.Success)
            {
                return null;
            }

            // Check username/password pair
            var user = this.UserManager.Find(userName, password);
            return null != user ? user.Id : null;
        }

        public string CreateUser(string userName, string password)
        {
            var result = this.UserManager.Create(new ApplicationUser()
            {
                UserName = userName,
                Email = userName,
                FirstName = "",
                LastName = ""
            },
                password);
            return result.Succeeded
                ? ValidateUser(userName, password)
                : null;
        }

        private IAuthenticationManager Authentication
        {
            get { return Request.GetOwinContext().Authentication; }
        }

        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }
                return BadRequest(ModelState);
            }

            return null;
        }

        private void SendPasswordChangeConfirmation(
            ApplicationUser user)
        {
            // 1.0.0.17
            try
            {
                EmailApi_Smtp.SendEmail(
                    subject: "Password Change",
                    addressTo: user.Email,
                    isBodyHtml: false,
                    body:
                    // recommendation by David M.:
                    //The password for remote access to your Pentair Pool(s) has been successfully changed. 
                    //If you feel this is in error please contact Pentair Tech Support at 800-831-7133.

                      //String.Format("The password for remote access to your Pentair Pool(s) has been changed.\n\nIf you feel this is in error please contact Pentair at {0}",
                      String.Format("The password for remote access to your Pentair Pool(s) has been successfully changed.\n\nIf you feel this is in error please contact Pentair Tech Support at {0}",                        
                            StaticData.PentairPhoneContact
                        )
                );
            }
            catch(Exception e)
            {
                LogManager.LogErrorAsync(
                    source: EVENT_SOURCE,
                    eventBriefDescription: "AccountController.PasswordChangeConfirmation() error",
                    installationId: 0,
                    userId: Guid.Empty,
                    connectionId: Guid.Empty,
                    eventFullDescription:
                        String.Format("{0}\n\n{1}",
                            user.Email,
                            ExceptionManager.CustomizeException(e).ErrorMessage(errorHeader: null, addStackTrace: false)
                        )
                );
            }

        } // SendPasswordChangeConfirmation()

        private string SendChangeEmailConfirmation(
            UserInvitation invite,
            string oldEmail)
        {
            string inviteUrl = "";
            string host = "";
            var request = HttpContext.Current.Request;
            var response = ResponseString.BadRequest;

            //build inviteUrl & Email
            //TODO: Remove local host support in production?
            if (HttpContext.Current.Request.Url.Host.ToLower() == "localhost")
            {
                host = "http://localhost:44300";
            }
            else
            {
                inviteUrl = request.IsSecureConnection ? "https://" : "http://";
                host = request["HTTP_HOST"];
            }

            inviteUrl += host + "/#/confirmUserNameChange";

            // 1.0.0.17
            try
            {
                EmailApi_Smtp.SendEmail(
                    subject: "Confirm UserName Change",
                    addressTo: invite.EmailAddress,
                    isBodyHtml: false,
                    body:
                        String.Format("You Have a new name now, visit {0}/{1}/{2} to confirm your username/email change.",
                            inviteUrl,
                            oldEmail,
                            invite.InvitationId
                        )
                );

                response = ResponseString.OK;
            }
            catch(Exception e)
            {
                LogManager.LogErrorAsync(
                    source: EVENT_SOURCE,
                    eventBriefDescription: "AccountController.SendChangeEmailConfirmation() error",
                    installationId: 0,
                    userId: Guid.Empty,
                    connectionId: Guid.Empty,
                    eventFullDescription:
                        ExceptionManager.CustomizeException(e).ErrorMessage(errorHeader: null, addStackTrace: false)
                );

                response = ResponseString.InternalServerError;
            }

            return response;

        } // SendChangeEmailConfirmation()
    }
}
