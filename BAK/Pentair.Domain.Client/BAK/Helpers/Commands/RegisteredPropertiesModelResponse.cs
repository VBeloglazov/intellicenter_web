﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Pentair.Domain.Client.Helpers.Commands
{
    public class InstallationModel
    {
        public string InstallationID { get; private set; }
        public string PoolName { get; private set; }
        public string Address { get; private set; }
        public string City { get; private set; }
        public string State { get; private set; }
        public string Zip { get; private set; }
        public string OnLine { get; private set; }

        public InstallationModel(
            string installationID,
            string poolName,
            string address,
            string city,
            string state,
            string zip,
            string onLine)
        {
            this.InstallationID = installationID;
            this.PoolName = poolName;
            this.Address = address;
            this.City = city;
            this.State = State;
            this.Zip = Zip;
            this.OnLine = onLine;
        }

    } // class InstallationModel

    public class RegisteredPropertiesModelResponse : ResponseCommand
    {
        [JsonProperty("objectList", Required = Required.Default)]
        public List<InstallationModel> ObjectList { get; private set; }

        public RegisteredPropertiesModelResponse(
            string command,
            string messageID,
            string response,
            List<InstallationModel> objectList
            )
            : base(command, messageID, response)
        {
            this.ObjectList = objectList;

        } // RegisteredPropertiesModelResponse()

        /// <summary>
        /// Always returns an object, valid or not. If invalid, check the DeserializationError property value.
        /// </summary>
        public static new RegisteredPropertiesModelResponse Deserialize(
            string serializedMessage)
        {
            RegisteredPropertiesModelResponse command = null;

            try
            {
                command = JsonConvert.DeserializeObject<RegisteredPropertiesModelResponse>(serializedMessage);
                command.IsValid = true;
            }
            catch (Exception e)
            {
                // Deserialization as RegisteredPropertiesModelResponse failed

                StringBuilder sbError = new StringBuilder();

                sbError.Append("Cannot deserialize as RegisteredPropertiesModelResponse. " + e.Message);

                // Try to get at least the base message data
                ResponseCommand baseCommand = ResponseCommand.Deserialize(serializedMessage);

                if (!baseCommand.IsValid)
                {
                    sbError.Append("\n" + baseCommand.DeserializationError);
                }

                command = new RegisteredPropertiesModelResponse(
                    command: baseCommand.Command,
                    messageID: baseCommand.MessageID,
                    response: ResponseString.BadRequest,
                    objectList: null)
                {
                    IsValid = false,
                    DeserializationError = sbError.ToString()
                };
            }

            return command;

        } // Deserialize()

    } // class RegisteredPropertiesModelResponse
}
