'use strict';
var PWC = PWC || {};
PWC.controller('ConfigureRemoteController', [
  '$rootScope', '$scope', '$location', '$interpolate', 'PropertyService', 'PropertyModel', 'Enumerations', 'Notification',
  function ($rootScope, $scope, $location, $interpolate, propertyService, propertyModel, Enumerations, Notification) {
    $scope.property = propertyModel;
    $scope.accordionStateByObjectNames = {};

    $rootScope.editMode.remotes = {};

    $scope.remotes = [];
    $scope.remoteSubtypes = angular.copy(Enumerations.remoteSubtypes);
    $scope.allRemoteSubtypes = Enumerations.getRemoteSubtypeObjects();
    $scope.circuits = propertyModel.Circuits;
    $scope.Objects = propertyModel.Objects;
    $scope.bodies = propertyModel.Bodies;
    $scope.pumps = propertyModel.Pumps;
    $scope.isCreatingRemote = false;
    $rootScope.editMode.remotes.mode = false;
    $scope.remoteSelectedCount = 0;
    $scope.maxRemotes = 9;
    $scope.newRemote = {
      params: {
        BUTTONS: []
      }
    };
    updateRemote();

    $scope.errorSave = function(errorMessage) {
      Notification.error({message: 'Failed!' + ' ' + errorMessage, delay: 1000});
    };

    $scope.successSave = function() {
      Notification.success({message: 'Saved!', delay: 1000});
    };

    $scope.successCancel = function() {
      Notification.success({message: 'Cancelled!', delay: 1000});
    };

    // console.log($scope.pumps)
    $scope.cancelEdit = function(index) {

      $scope.remotes = angular.copy($scope.remotesBeforeEdit);
      $rootScope.forms.remotes[index].$setPristine();
      $scope.successCancel();
    };
    //
    //   $scope.add = function() {
    //     var chem = {
    //       OBJNAM: Math.uuid(4),
    //       editMode: true,
    //       createMode: true,
    //       addressMap: $scope.iChemAddressMap,
    //       PRIM: 50,
    //       SEC: 0,
    //       enableChlorination: false,
    //       chlorTimeLeftHours: 12
    //     };
    //     $scope.chems.push(chem);
    //     $scope.accordionStateByObjectNames[chem.objnam] = {};
    //     $scope.accordionStateByObjectNames[chem.objnam].open = true;
    //   };
    //
    $scope.removeItems = function() {
      // populate the list of object names we are going to delete
      $scope.objectNamesToDelete = $scope.remotes.map(function(obj) {
        if (obj.isSelected) {
          return obj.objnam;
        }
      });
      propertyService.DeleteObject($scope.objectNamesToDelete).then(function(response) {
        updateRemote();
      });
    };

    $scope.save = function(remote, index) {
      var params = {};
      var objnam = remote.objnam;
      params.SNAME = remote.params.SNAME;
      params.SUBTYP = remote.params.SUBTYP;
      if(remote.params.SUBTYP === 'IS10') {
        params.COMUART = remote.params.COMUART.toString();
      }
      
      if (remote.params.SUBTYP === 'IS4') {
          params.BODY = remote.params.BODY;          
          params.PUMP = remote.params.PUMP;
      }

      if(remote.params.SUBTYP === 'SPACMD') {
        params.BODY = remote.params.BODY;
        params.COMUART = remote.params.COMUART.toString();
        params.PUMP = remote.params.PUMP;
      }
      params.ENABLE = remote.isActive ? 'ON' : 'OFF';
      params.BUTTONS = remote.params.BUTTONS.map(function(button) {
        var newButton = {};
        newButton.OBJNAM = button.objnam;
        newButton.LISTORD = button.params.LISTORD;
        newButton.CIRCUIT = button.params.CIRCUIT;
        return newButton;
      });

      propertyService.editRemote(objnam, params).then(function() {
        updateRemote();
        $rootScope.forms.remotes[index].$setPristine();
        $scope.successSave();
      });

      // propertyService.SetParams(objnam, params).then(function(result) {
      //   updateRemote();
      // });
    };

    $scope.create = function(remote) {

    };

    // $scope.setEditMode = function(remoteObject) {
    //   remoteObject.editMode = true;
    // };
    //
    //   $scope.getDictionaryValue = function(actualValue) {
    //     if (!actualValue)
    //       return actualValue;
    //     var match = $scope.chemSubtypes.filter(function(chemSubtype) {
    //       if (chemSubtype.actualValue === actualValue) {
    //         return chemSubtype;
    //       }
    //     });
    //
    //     if (match && match.length > 0 && match[0].dictionaryValue) {
    //       return match[0].dictionaryValue;
    //     }
    //     return actualValue;
    //   };
    //
    //   function getAddressMapByChem(chem) {
    //     var addressMap = [];
    //     if(chem.SUBTYP === 'ICHEM') {
    //       addressMap = $scope.iChemAddressMap;
    //     } else {
    //       addressMap = $scope.iChlorAddressMap;
    //     }
    //     // add the one that it already has
    //     //addressMap.push(chem.COMUART);
    //     // sort
    //     addressMap.sort(function(a, b) {
    //       var keyA = parseInt(a),
    //         keyB = parseInt(b);
    //       if (keyA < keyB) return -1;
    //       if (keyA > keyB) return 1;
    //       return 0;
    //     });
    //     return addressMap;
    //   }
    //

    $scope.addNewRemote = function() {
      if(!$scope.isCreatingRemote) {
        $scope.showRemoteForm = true;
        getAvailableNewRemoteTypes()
      }
    }

    $scope.cancelNewRemote = function(index) {
      $scope.showRemoteForm = false;
      $scope.newRemote = {
        params: {
          BUTTONS: []
        }
      };
      $rootScope.forms.remotes[index].$setPristine();
      $scope.successCancel();
    };

    $scope.getCircuitAddresses = function(type) {
      propertyService.getAddressMap('CIRCUIT', type).then(function(res) {
        $scope.addresses = res.answer[0].assignments;
        if(type === 'IS10' || type === 'SPACMD') {
          $scope.addresses = [1,2,3,4]
        }

      })
    };

    $scope.isReadyToSaveNewRemote = function (index) {
        if (!$scope.newRemote)
            return true;

        var remoteCopy = angular.copy($scope.newRemote);
        //remoteCopy.params.ENABLE = (newRemote.isActive) ? "ON" : "OFF";
        //Populate Empty Buttons with empty circuit
        var setbuttons = true;
        for (var i = 0; i < remoteCopy.params.BUTTONS.length; i++) {
            if (remoteCopy.params.BUTTONS[i].CIRCUIT == undefined) {
                setbuttons = false // at least 1 button is not set    
            }
        }
        if (($scope.newRemoteValid === true) && (setbuttons === true))
            return true
        else
            return false;
                
    };

    $scope.saveNewRemote = function(newRemote, index) {
      var remoteCopy = angular.copy(newRemote);
      remoteCopy.params.ENABLE = (newRemote.isActive) ? "ON" : "OFF";

      //Populate Empty Buttons with empty circuit
      for(var i = 0; i < remoteCopy.params.BUTTONS.length; i++) {
        remoteCopy.params.BUTTONS[i].LISTORD = (i + 1).toString();
        remoteCopy.params.BUTTONS[i].OBJNAM = 'b100' + i.toString();
        if(remoteCopy.params.BUTTONS[i].CIRCUIT == undefined) {
          remoteCopy.params.BUTTONS[i].CIRCUIT = '00000';
        }
      }


      propertyService.createRemote(remoteCopy).then(function() {
        $scope.newRemote = {
          params: {
            BUTTONS: []
          }
        };
        $scope.showRemoteForm = false;
        $scope.newRemoteValid = false;
        updateRemote();
        $scope.successSave();
        $rootScope.forms.remotes[index].$setPristine();
      });
    };

    $scope.deleteRemote = function(obj) {
      propertyService.deleteRemote(obj.objnam).then(function() {
        updateRemote();
      });
    };

    $scope.getDefaultRemoteName = function (type) {

        var count = 1   // next remote

        angular.forEach($scope.remotes, function (remote) {
            if (remote.params.SUBTYP === type)
                count = count +1
        });
        
        var name = ""
        switch (type) {                  
          case 'QT':
            name = "QT4 #" + count
            break;

          case 'IS10':
            name  = "iS10 #" + count
            break;

          case 'SPACMD':
              name = "SpaCommand #" + count
              break;

            default:
                name = "Remote #" + count
                break;

        }
        return name
    };


    $scope.getDefaultSpaCmdBody = function(){
        var body = "B1202"
        switch(propertyModel.PANID) { // "SINGLE",    // NONE, SINGLE, SHARE, DUAL
            case "SINGLE": 
                body = "B1101"
                break

            case "SHARE": 
            case "DUAL": 
                body = "B1202"
                break

            default:
                body = "B1101"
                break

        }
        return body;
    }

    $scope.$watch('newRemote.objtyp', function(type) {
      var btnCount;
      if($scope.showRemoteForm) {
        switch(type) {
          case 'QT':
            btnCount = 4;
            break;
          case 'IS10':
            btnCount = 10;
            break;
          case 'SPACMD':
            btnCount = 10;
            break;
        }
          // create buttons 
        $scope.newRemote.params.BUTTONS = [];
        for (var i = 0; i < btnCount; i++) {
          $scope.newRemote.params.BUTTONS.push({});
        }
          // now set defaults for those buttons
        for (var i = 0; i < btnCount; i++) {
            if (i === 9)    // set 4 DIGIT FORMAT CIRCUIT C1234 
                $scope.newRemote.params.BUTTONS[i].CIRCUIT = "C00"  + (i + 1);
            else
                $scope.newRemote.params.BUTTONS[i].CIRCUIT = "C000" + (i + 1);
        }

        // adding default com UART port
        switch (type) {
            case 'SPACMD':
                $scope.newRemote.params.COMUART = 1
                $scope.newRemote.params.BODY = $scope.getDefaultSpaCmdBody()  // SPA BODY
                break;

            case 'IS10':                
                $scope.newRemote.params.COMUART = 1
                break;
        }
        
        $scope.newRemote.params.SNAME = $scope.getDefaultRemoteName(type)

      }
    })

    $scope.$watch('newRemote', function(newRemote) {
      if(newRemote) {
        if(!newRemote.params.SNAME || !newRemote.objtyp) {
          $scope.newRemoteValid = false;
        }
        else if(newRemote.objtyp === 'IS10' && !newRemote.params.COMUART) {
          $scope.newRemoteValid =  false;
        }
        else if(newRemote.objtyp === 'SPACMD'
            //&& (!newRemote.params.BODY || !newRemote.params.PUMP)) {
            && (!newRemote.params.BODY)) {
          $scope.newRemoteValid =  false;
        } else {
          $scope.newRemoteValid =  true;
        }
      }
    }, true);

    // $scope.toggleEdits = function() {
    //   $rootScope.editMode.remotes.mode = true;
    // }

    $scope.toggleSelectedRemote = function (remoteSelected) {
        $scope.remoteSelectedCount += (remoteSelected) ? 1 : -1;
    };

    $scope.getRemotesCount = function () {
        var count = 0
        if ( $scope.remotes)
            count = $scope.remotes.length;
        return count;
    };

    $scope.getMaxRemotes = function () {
        
        return $scope.maxRemotes;
    };

    $scope.canAddRemotes = function () {
        
        var total = $scope.maxRemotes - $scope.getRemotesCount()
        if (total > 0)
            return true
        else
            return false        
    };



    function updateRemote() {
      propertyService.getRemotes().then(function(remotes) {
        $scope.remotes = remotes.answer.map(function(item) {
          item.params.BUTTONS = item.params.BUTTONS.sort(function(a, b) {
            var keyA = parseInt(a.params.LISTORD),
              keyB = parseInt(b.params.LISTORD);
            if (keyA < keyB) return -1;
            if (keyA > keyB) return 1;
            return 0;
          });
          item.isActive = (item.params.ENABLE === 'ON') ? true : false;
          return item;
        }).sort(function(a, b) {
          var keyA = parseInt(a.params.LISTORD),
            keyB = parseInt(b.params.LISTORD);
          if (keyA < keyB) return -1;
          if (keyA > keyB) return 1;
          return 0;
        });
        $scope.remotesBeforeEdit = angular.copy($scope.remotes);
        $rootScope.editMode.remotes.mode = false;
        getAvailableNewRemoteTypes();
      });
    }

    $scope.getDictionaryValue = function (actualValue) {

      if (!actualValue)
        return actualValue;
      var match = $scope.allRemoteSubtypes.filter(function (remoteSubtype) {
        if (remoteSubtype.actualValue === actualValue) {
          return remoteSubtype;
        }
      });
      if(match && match.length > 0 && match[0].dictionaryValue) {
        return match[0].dictionaryValue;  
      }
      return actualValue;
    };

    function getAvailableNewRemoteTypes() {
      $scope.remoteSubtypes = angular.copy(Enumerations.remoteSubtypes);
      var IS10_SPACMDC_Count = 0;
      var hasQT = false;

      //Remove IS4, since those can only be created on THW
      index = $scope.remoteSubtypes.indexOf('IS4');
      $scope.remoteSubtypes.splice(index, 1);

            //Only allow one QT remote
      for (var i = 0; i < $scope.remotes.length; i++) {
        if($scope.remotes[i].params.SUBTYP === 'QT') {
          var index = $scope.remoteSubtypes.indexOf('QT');
          $scope.remoteSubtypes.splice(index, 1);
          hasQT = true;
        }

        if($scope.remotes[i].params.SUBTYP === 'IS10' || $scope.remotes[i].params.SUBTYP === 'SPACMD') {
          IS10_SPACMDC_Count++
        }

      }

      if(!hasQT && $scope.remoteSubtypes.indexOf('QT') == -1) $scope.remoteSubtypes.push('QT');
      //Limit to 4 IS10 & SPACMD total.
      if(IS10_SPACMDC_Count < 4) {
        if($scope.remoteSubtypes.indexOf('IS10') == -1) $scope.remoteSubtypes.push('IS10');
        if($scope.remoteSubtypes.indexOf('SPACMD') == -1) $scope.remoteSubtypes.push('SPACMD');
      } else {
        var index = $scope.remoteSubtypes.indexOf('IS10');
        $scope.remoteSubtypes.splice(index, 1);
        var index = $scope.remoteSubtypes.indexOf('SPACMD');
        $scope.remoteSubtypes.splice(index, 1);
      }


    }

  }
]);




