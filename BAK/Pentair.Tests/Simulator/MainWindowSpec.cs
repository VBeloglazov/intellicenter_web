﻿
using Machine.Specifications;
using Pentair.Desktop.Simulator;

namespace Pentair.Tests.Simulator.Desktop
{
    [Subject("ClientSocket")]

    public class WhenIOpenTheSimulator
    {
        private static App app;

        private Establish context = () =>
        {
            app = new App();
            app.InitializeComponent();
            app.Run();

        };

//        It should_have_the_right_title = () => app.MainWindow.Title.ShouldStartWith("THW");
    }
}
