﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Pentair.Domain.Client.Helpers.Commands
{
    public class SetCommandResponse : ResponseCommand
    {
        [JsonProperty("objectList", Required = Required.Default)]
        public object ObjectList { get; private set; }

        public SetCommandResponse(
            string messageID,
            string response,
            object objectList
            )
            : base(CommandString.SetCommand, messageID, response)
        {
            this.ObjectList = objectList;

        } // SetCommandResponse()

        /// <summary>
        /// Always returns an object, valid or not. If invalid, check the DeserializationError property value.
        /// </summary>
        public static new SetCommandResponse Deserialize(
            string serializedMessage)
        {
            SetCommandResponse command = null;

            try
            {
                command = JsonConvert.DeserializeObject<SetCommandResponse>(serializedMessage);
                command.IsValid = true;
            }
            catch (Exception e)
            {
                // Deserialization as SetCommandResponse failed

                StringBuilder sbError = new StringBuilder();

                sbError.Append("Cannot deserialize as SetCommandResponse. " + e.Message);

                // Try to get at least the base message data
                ResponseCommand baseCommand = ResponseCommand.Deserialize(serializedMessage);

                if (!baseCommand.IsValid)
                {
                    sbError.Append("\n" + baseCommand.DeserializationError);
                }

                command = new SetCommandResponse(
                    messageID: baseCommand.MessageID,
                    response: ResponseString.BadRequest,
                    objectList: null)
                {
                    IsValid = false,
                    DeserializationError = sbError.ToString()
                };
            }

            return command;

        } // Deserialize()

    } // class SetCommandResponse
}
