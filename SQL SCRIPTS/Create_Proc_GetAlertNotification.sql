USE [PathfinderIntellicenter]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (
	SELECT * FROM sys.objects
	WHERE object_id = OBJECT_ID(N'dbo.pr_GetAlertNotification')
	AND type in (N'P', N'PC')
)
DROP PROCEDURE dbo.pr_GetAlertNotification
GO


-- =============================================
-- Author			:	Wayne Hui
-- Create date		:	04/09/2019
-- Description		:	Returns the full alert message for email notification
-- Input			:	InstallationId, AlertStatus, AlertType, AlertDescription
-- Output			:	The full alert message for email notification
--
-- Modified By			Date		Description
---- =============================================
CREATE PROCEDURE [dbo].[pr_GetAlertNotification]
	@InstallationId		INT
	, @AlertStatus		INT
    , @AlertType		NVARCHAR(30)
	, @AlertMessage		NVARCHAR(256)
AS
BEGIN

	SET NOCOUNT OFF

------TEST
--DECLARE	 
--	@InstallationId		INT
--	, @AlertStatus		INT
--	, @AlertType		NVARCHAR(30)
--	, @AlertMessage		NVARCHAR(256)

--SET @InstallationId	= 3931
--SET @AlertStatus	= 0
--SET @AlertType		= 'Warning'
--SET @AlertMessage	= 'IntelliChem 1: ORP Low'

---END TEST

DECLARE	
	@DeviceName			NVARCHAR(30)
	, @DeviceStatus		NVARCHAR(50)
	, @DeviceMessage	NVARCHAR(256)

	, @AlertMode		NVARCHAR(20)
	, @AlertShortMessage	NVARCHAR(256)
	, @AlertNote		NVARCHAR(MAX)
	, @CurrentTimeZone	NVARCHAR(50)
	, @CurrentTime		NVARCHAR(100)


	----Get the current time and time zone
	EXEC MASTER.dbo.xp_regread 'HKEY_LOCAL_MACHINE', 'SYSTEM\CurrentControlSet\Control\TimeZoneInformation', 'TimeZoneKeyName', @CurrentTimeZone OUT
	SET @CurrentTime = FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm:ss tt ', 'en-US' ) + @CurrentTimeZone


	----Get device status
	SET @DeviceStatus = CASE @AlertStatus 
							WHEN 1 THEN ' is <B>ACTIVE</B>'
							WHEN 0 THEN ' has <B>CLEARED</B>'			
							ELSE ''
						END
		
		
	----Parse out the device name and alert message
	SELECT 
		@DeviceName = REPLACE(SUBSTRING(@AlertMessage, 0, CHARINDEX(' ', @AlertMessage)), ':', '')
		, @DeviceMessage = SUBSTRING(@AlertMessage, CHARINDEX(':', @AlertMessage)+2, LEN(@AlertMessage)) 


	----Base on device name and alert message, get the note. If it is a warning, do not display the alert type
	IF ((SELECT COUNT(*) FROM [dbo].[AlertNotes] WHERE DeviceName = @DeviceName AND AlertMessage = @DeviceMessage) > 0)
	BEGIN
		SELECT 
			@AlertMode = CASE WHEN AlertType = 'Error' THEN AlertType ELSE @AlertType END 
			, @AlertShortMessage = @AlertMessage 
			, @AlertNote = Note
		FROM [dbo].[AlertNotes]
		WHERE
			DeviceName = @DeviceName
			AND AlertMessage = @DeviceMessage
	END
	ELSE
	BEGIN
		SELECT 
			@AlertMode = @AlertType 
			, @AlertShortMessage = @DeviceMessage  
			, @AlertNote = NULL
	END	
	

	----Get device user info
	IF OBJECT_ID('tempdb.dbo.#UserInfo', 'U') IS NOT NULL DROP TABLE #UserInfo

		SELECT
			 I.Name AS DeviceName
			,(SELECT TOP 1 Value FROM [ControlObjects] WHERE INSTALLATION = I.InstallationId AND OBJNAM = '_5451' AND [Key] = 'ADDRESS' ORDER BY Revision DESC) [Address]
			,(SELECT TOP 1 Value FROM [ControlObjects] WHERE INSTALLATION = I.InstallationId AND OBJNAM = '_5451' AND [Key] = 'CITY'    ORDER BY Revision DESC) [City]
			,(SELECT TOP 1 Value FROM [ControlObjects] WHERE INSTALLATION = I.InstallationId AND OBJNAM = '_5451' AND [Key] = 'STATE'	ORDER BY Revision DESC) [State]
			,(SELECT TOP 1 Value FROM [ControlObjects] WHERE INSTALLATION = I.InstallationId AND OBJNAM = '_5451' AND [Key] = 'ZIP'     ORDER BY Revision DESC) [Zip]
			,(SELECT TOP 1 Value FROM [ControlObjects] WHERE INSTALLATION = I.InstallationId AND OBJNAM = '_5451' AND [Key] = 'PHONE'   ORDER BY Revision DESC) [Phone]
			,(SELECT TOP 1 Value FROM [ControlObjects] WHERE INSTALLATION = I.InstallationId AND OBJNAM = '_5451' AND [Key] = 'EMAIL'   ORDER BY Revision DESC) [Email]
		INTO #UserInfo
		FROM dbo.Installations I
		WHERE I.InstallationId = @InstallationId


	----The complete alert message with HTML tags
	SELECT 
		[Subject] = 'Pentair IntelliCenter Alert'
		, [Message] = 
			'<P>Alert for <B>' + [DeviceName] + '</B> ' + @DeviceStatus + ' </P><P>' + @AlertMode + ':<BR><B>' + @AlertShortMessage + '</B></P><P>' + @AlertNote + 
			'</P><P>Time & Date of Alert: ' + @CurrentTime + '</P><P>' + [Address] + '<BR>' + [City] + ', ' + [State] + ' ' + [Zip] + '<BR>' + [Phone] + 
			'<BR>' + [Email] + '<BR></P><P>You can login to IntelliCenter at this link: https://www.intellicenter.com</P><P>Thank you,</P><P><BR>Pentair</P>'
    FROM    
		#UserInfo

END
GO

GRANT EXECUTE ON [dbo].[pr_GetAlertNotification] TO PUBLIC
GO
