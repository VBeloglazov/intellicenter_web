﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Configuration;
using System.Web.Helpers;
using System.Web.Http.Results;
using Pentair.Web.Api.Models;

namespace Pentair.Web.Api.Helpers
{
    public class EmailApi
    {
        #region Private Properties
        private readonly string _mailServer;
        private readonly string _key;
        private readonly string _from;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public EmailApi()
        {
            var configSettings = WebConfigurationManager.OpenWebConfiguration("/service");
            _mailServer = configSettings.AppSettings.Settings["mailserver"].Value;
            _key = configSettings.AppSettings.Settings["emailkey"].Value;
            _from = configSettings.AppSettings.Settings["fromaddress"].Value;
        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Send an email to the person specified by the email object passed in.
        /// </summary>
        /// <param name="email">email object</param>
        /// <returns>string representing the result of the process.</returns>
        public List<EmailResponse> SendEmail(Email email)
        {
            var emailObject = new JsonEmail()
            {
                from_email = String.IsNullOrEmpty(email.From) ? _from : email.From,
                raw_message = string.Format("Subject: {0}\n\n{1}", email.Subject, email.Message),
                key = _key,
                async = false,
                from_name = "",
                ip_pool = "Main Pool",
                return_path_domain = "",
                send_at = "",
                to = new List<string>()
                {
                    email.To
                }
            };

            var response = new List<EmailResponse>();
            using (var wc = new WebClient())
            {
                var result = wc.UploadString(_mailServer + "/messages/send-raw.json", Json.Encode(emailObject));
                response = Json.Decode<List<EmailResponse>>(result);

            }
            return response;
        }

        /// <summary>
        /// Ping the email server to validate connectivity and a valid key
        /// </summary>
        /// <returns>boolean</returns>
        public bool PingServer()
        {
            var ping = string.Format("{{\"key\" : \"{0}\"}}", _key);
            var response = "";
            using (var wc = new WebClient())
            {
                response = wc.UploadString(_mailServer + "/users/ping.json", ping);
            }
            return (response.Contains("PONG!"));
        }
        #endregion
    }

    internal class JsonEmail
    {
        /// <summary>
        /// JSon used to send a raw-send type email.
        /// </summary>
        public string key { get; set; }
        public string raw_message { get; set; }
        public string from_email { get; set; }
        public string from_name { get; set; }
        public List<String> to { get; set; }
        public bool async { get; set; }
        public string ip_pool { get; set; }
        public string send_at { get; set; }
        public string return_path_domain { get; set; }
    }
}