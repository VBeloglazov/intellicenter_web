﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Hosting;
using System.Threading.Tasks;
using System.Diagnostics;

using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Client;
using Microsoft.Web.Administration;

using Newtonsoft.Json;

using SERVICE_SHARED;

namespace Pentair.Web.Api
{
    public class EsbConnection : PersistentConnection
    {
    } // class EsbConnection

    public partial class EsbClientConnection : Microsoft.AspNet.SignalR.Client.Connection
    {
        private const string EVENT_SOURCE = "EsbClientConnection";

        // It will be defined in Startup.ConfigureSignalR()
        public static bool IsEsbRequired { get; set; }

        public static object lockClientESB = new object();
        public static EsbClientConnection ClientESB { get; set; }

        public static bool IsEsbConnected
        {
            get
            {
                lock (EsbClientConnection.lockClientESB)
                {
                    return EsbClientConnection.ClientESB != null && EsbClientConnection.ClientESB.IsConnected;
                }
            }
        }

        private static Microsoft.AspNet.SignalR.Infrastructure.Connection ConnectionESB { get; set; }

        /// <summary>
        /// Not thread safe
        /// </summary>
        public bool IsConnected
        {
            get
            {
                return
                    this.State == ConnectionState.Connected
                ;
            }

        } // IsConnected

        private RecycleWatcher recycleWatcher = new RecycleWatcher();

        private EsbClientConnection(
            string esbUrl
            )
            : base(esbUrl)
        {
            EsbClientConnection.ConnectionESB =
                GlobalHost.ConnectionManager.GetConnectionContext<EsbConnection>().Connection
                 as Microsoft.AspNet.SignalR.Infrastructure.Connection;

            this.StateChanged += ConnectionStateChanged;
            this.Received += HandleReceived;
            this.Error += (e) => HandleException(-1, Guid.Empty, e);

            LogManager.LogInformationAsync(
                source: EVENT_SOURCE,
                eventBriefDescription: "Connecting ESB...",
                installationId: 0,
                userId: Guid.Empty,
                connectionId: Guid.Empty,
                eventFullDescription: String.Format("ESB: {0}", esbUrl),
                saveEventLog: true
            );

            HostingEnvironment.RegisterObject(this.recycleWatcher);

        }

        #region Implementation

        public static Task ConnectEsbAsync()
        {
            try
            {
                System.Configuration.Configuration configSettings =
                System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("/service");

                lock (EsbClientConnection.lockClientESB)
                {
                    if (EsbClientConnection.ClientESB != null)
                    {
                        EsbClientConnection.ClientESB.Dispose();
                        EsbClientConnection.ClientESB = null;
                    }

                    EsbClientConnection.ClientESB = new EsbClientConnection(
                        esbUrl: configSettings.AppSettings.Settings["EsbUrl"].Value
                    );

                    return EsbClientConnection.ClientESB.Start();
                }
            }
            catch (Exception e)
            {
                LogManager.LogErrorAsync(
                    source: EVENT_SOURCE,
                    eventBriefDescription: "ClientESB starting failed",
                    installationId: 0,
                    userId: Guid.Empty,
                    connectionId: Guid.Empty,
                    eventFullDescription:
                        ExceptionManager.CustomizeException(e).ErrorMessage(
                            errorHeader: null,
                            addStackTrace: true
                        )
                );

                EsbClientConnection.ClientESB = null;

                return Task.FromResult(0);
            }

        } // ConnectEsbAsync()

        private Task ReconnectEsbAsync()
        {
            try
            {
                ServerManager serverManager = new ServerManager();

                WorkerProcess workerProcess =
                    serverManager.WorkerProcesses.GetWorkerProcess(Process.GetCurrentProcess().Id);

                ApplicationPool applicationPool =
                (
                    from p in serverManager.ApplicationPools
                    where p.Name == workerProcess.AppPoolName
                    select p
                 )
                 .First();

                if (applicationPool.State == ObjectState.Started)
                {
                    lock (EsbClientConnection.lockClientESB)
                    {
                        return EsbClientConnection.ConnectEsbAsync();
                    }
                }
            }
            catch (Exception e)
            {
                HandleException(
                    installationId: -1,
                    clientConnectionId: Guid.Empty,
                    exception: e
                );
            }

            return Task.FromResult(0);

        } // ReconnectEsbAsync()

        public Task BroadcastAsync(
            string destHostMachine,  // Can be null for real broadcast
            EndpointType destType,
            int installationId,
            Guid clientConnectionId, // Can be Guid.Empty for destType = EndpointType.THW
            string serializedMessage)
        {
            try
            {
                if (!this.IsConnected)
                {
                    throw new Exception("Not connected to ESB (" + this.State.ToString() + ")");
                }

                EsbMessage esbMessage =
                    new EsbMessage(
                        destHostName: destHostMachine,
                        destType: destType,
                        sender: WebApiApplication.HOSTNAME,
                        installationId: installationId,
                        clientConnectionId: clientConnectionId,
                        message: serializedMessage
                    );

                if (WebApiApplication.LogVerbose)
                {
                    LogManager.LogInformationAsync(
                        source: EVENT_SOURCE,
                        eventBriefDescription: "SENT ESB",
                        installationId: installationId,
                        userId: Guid.Empty,
                        connectionId: clientConnectionId,
                        eventFullDescription:
                            String.Format("Destination Type: {0}\n\nMessage:\n{1}",
                                destType,
                                esbMessage
                            )
                    );
                }

                lock (EsbClientConnection.lockClientESB)
                {
                    return EsbClientConnection.ConnectionESB.Send(
                        new Microsoft.AspNet.SignalR.ConnectionMessage(
                            EsbClientConnection.ConnectionESB.DefaultSignal,
                            JsonConvert.SerializeObject(esbMessage)
                        )
                    );
                }
            }
            catch(Exception e)
            {
                HandleException(
                    installationId: installationId,
                    clientConnectionId: clientConnectionId,
                    exception: new Exception(
                        message: "Send failed. Message lost: " + serializedMessage,
                        innerException: e
                    )
                );

                return Task.FromResult(0);
            }

        } // BroadcastAsync()

        public static void ProcessMessage(
            bool isBroadcast,
            EndpointType destType,
            int installationId,
            Guid clientConnectionId,
            string serializedMessage)
        {
            switch (destType)
            {
                case EndpointType.THW:

                    WebSocketClient_THW thw =
                        WebSocketClient.FindHostByInstallationId(installationId) as WebSocketClient_THW;

                    if (thw != null)
                    {
                        // This is local connection
                        thw.SendMessage(serializedMessage, WebSocketActions.Send);
                    }
                    else
                    {
                        if (!isBroadcast)
                        {
                            throw new Exception("THW not found: #" + installationId);
                        }
                    }

                    break;

                case EndpointType.Client:

                    WebSocketClient_Client client =
                        WebSocketClient.FindClientByConnectionId(clientConnectionId) as WebSocketClient_Client;

                    if (client != null)
                    {
                        // This is local connection
                        client.SendMessage(serializedMessage, WebSocketActions.SendClient);
                    }
                    else
                    {
                        if (!isBroadcast)
                        {
                            throw new Exception("Client not found: #" + clientConnectionId);
                        }
                    }

                    break;

                case EndpointType.Server:

                    MessageType msgType;
                    try
                    {
                        msgType = WebSocketMessage.ParseMessageType(serializedMessage, EndpointType.THW);
                    }
                    catch (Exception e)
                    {
                        throw new Exception("Cannot parse MessageType: " + serializedMessage + ". " + e.Message);
                    }

                    ServerCommand serverCommand =
                        ServerCommand.ServerCommandFactory(msgType.CommandType, serializedMessage);

                    if (serverCommand == null)
                    {
                        throw new Exception("Unknown Server Command: " + serializedMessage);
                    }

                    if (!serverCommand.IsValid)
                    {
                        throw new Exception("Invalid Server Command: " + serializedMessage + ". Error: " + serverCommand.DeserializationError);
                    }

                    serverCommand.Handle();

                    break;

                default:
                    throw new Exception("Unknown DestType: " + destType);

            } // switch (message.destType)

        } // ProcessMessage()

        private void ConnectionStateChanged(
            StateChange stateChange)
        {
            try
            {
                if (!this.recycleWatcher.IsRecycled)
                {
                    LogManager.LogInformationAsync(
                        source: EVENT_SOURCE,
                        eventBriefDescription: "ESB State Changed",
                        installationId: 0,
                        userId: Guid.Empty,
                        connectionId: Guid.Empty,
                        eventFullDescription: String.Format("{0} -> {1}", stateChange.OldState, stateChange.NewState),
                        saveEventLog: true
                    );

                    switch(stateChange.NewState)
                    {
                        case ConnectionState.Disconnected:
                            HandleDisconnected();
                            break;
                    }
                }
            }
            catch (Exception e)
            {
                HandleException(
                    installationId: -1,
                    clientConnectionId: Guid.Empty,
                    exception: e
                );
            }

        } // ConnectionStateChanged()

        private void HandleDisconnected()
        {
            // TODO: Disconnect stranger Clients (try-catch, something like WebSocketClient.RemoveThwClients)

            ReconnectEsbAsync().Wait(15000);

        } // HandleDisconnected()

        private void HandleReceived(
            string serializedEsbMessage)
        {
            EsbMessage esbMessage = null;

            try
            {
                esbMessage = JsonConvert.DeserializeObject<EsbMessage>(serializedEsbMessage);
            }
            catch (Exception e)
            {
                HandleException(
                    installationId: -1,
                    clientConnectionId: Guid.Empty,
                    exception: new Exception(
                        message: "Deserialization failed. Message lost: " + serializedEsbMessage,
                        innerException: e
                    )
                );

                return;
            }

            try
            {
                bool isBroadcast = (esbMessage.DestinationHostName == null);

                if (!isBroadcast &&
                    String.Compare(esbMessage.DestinationHostName, WebApiApplication.HOSTNAME, ignoreCase: true) != 0)
                {
                    // Adressed to another server, ignore.
                    return;
                }

                if (WebApiApplication.LogVerbose)
                {
                    LogManager.LogInformationAsync(
                        source: EVENT_SOURCE,
                        eventBriefDescription: "RCVD ESB",
                        installationId: 0,
                        userId: Guid.Empty,
                        connectionId: Guid.Empty,
                        eventFullDescription: esbMessage.ToString()
                    );
                }

                ProcessMessage(
                    isBroadcast,
                    esbMessage.DestinationType,
                    esbMessage.InstallationId,
                    esbMessage.ClientConnectionId,
                    esbMessage.SerializedMessage
                );
            }
            catch (Exception e)
            {
                HandleException(
                    installationId: esbMessage.InstallationId,
                    clientConnectionId: esbMessage.ClientConnectionId,
                    exception: new Exception(
                        message: "HandleReceived failed. Message lost: " + esbMessage,
                        innerException: e
                    )
                );
            }

        } // HandleReceived()

        private void HandleException(
            int installationId,
            Guid clientConnectionId,
            Exception exception)
        {
            if (!this.recycleWatcher.IsRecycled)
            {
                lock (EsbClientConnection.lockClientESB)
                {
                    LogManager.LogErrorAsync(
                       source: EVENT_SOURCE,
                       eventBriefDescription: "ESB Error",
                       installationId: 0,
                       userId: Guid.Empty,
                       connectionId: Guid.Empty,
                       eventFullDescription:
                        String.Format("ESB Client State: {0}\n\n{1}",
                            this.State,
                            ExceptionManager.CustomizeException(exception).ErrorMessage(errorHeader: null, addStackTrace: true)
                        )
                    );
                }

                if (exception is StackExchange.Redis.RedisConnectionException ||
                    (exception.InnerException != null && exception.InnerException is StackExchange.Redis.RedisConnectionException))
                {
                    ReconnectEsbAsync().Wait(15000);
                }
            }

        } // HandleException()

        #endregion Implementation

    } // class EsbClientConnection
}