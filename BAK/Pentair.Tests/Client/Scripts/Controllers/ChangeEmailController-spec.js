﻿'use strict';
describe('ChangeEmailController', function () {
    var AppService;
    var $location;
    var $scope;
    var ChangeEmailController;
    var $controller;
    var $httpBackend;
    var HTTP_SERVICE_URL;
    beforeEach(module('PentairWebClient'));

    beforeEach(inject(function (_$controller_, _$rootScope_, _AppService_, _$location_, _$httpBackend_,
                                _SERVICE_HOST_NAME_) {
        $scope = _$rootScope_.$new();
        AppService = _AppService_;
        $location = _$location_;
        $controller = _$controller_;
        $httpBackend = _$httpBackend_;
        HTTP_SERVICE_URL = 'https://' + _SERVICE_HOST_NAME_;

        ChangeEmailController = $controller("ChangeEmailController", { $scope: $scope });
        spyOn($location, 'path');

    }));

    describe('Default Vars', function () {
        it('should have the following default variables', function () {

            expect($scope.validateOldEmail).toBe(false);
            expect($scope.changeEmailData).toEqual({
                oldEmail: '',
                newEmail: '',
                confirmEmail: '',
                password: '',
                error: false,
                success: false
            });
        });
    });

    describe('GoToDashboard', function () {
        it('should take the user the main page', function () {
            $scope.GoToDashboard();
            expect($location.path).toHaveBeenCalledWith('/property');
        });
    });

    describe('ChangeEmail', function () {
        it('should send a valid request for an email change', function () {
            var changeEmailRequest = {
                oldEmail: "old@email.com",
                newEmail: "new@email.com",
                confirmEmail: "new@email.com"
            };
            $httpBackend.expectPOST(HTTP_SERVICE_URL + 'Api/Account/ChangeUserName').respond(200, changeEmailRequest);
            $scope.ChangeEmail();
            $httpBackend.flush();
            expect($scope.changeEmailData.error).toBe(false);
            expect($scope.changeEmailData.success).toBe(true);

            expect($scope.validateOldEmail).toBe(false);
            expect($scope.changeEmailData.oldEmail).toBe("");
            expect($scope.changeEmailData.newEmail).toBe("");
            expect($scope.changeEmailData.confirmEmail).toBe("");
        });
        it('should send an invalid request for an email change', function () {
            $httpBackend.expectPOST(HTTP_SERVICE_URL + 'Api/Account/ChangeUserName').respond(403);
            $scope.ChangeEmail();
            $httpBackend.flush();
            expect($scope.changeEmailData.error).toBe(true);
            expect($scope.changeEmailData.success).toBe(false);

            expect($scope.validateOldEmail).toBe(false);
            expect($scope.changeEmailData.oldEmail).toBe("");
            expect($scope.changeEmailData.newEmail).toBe("");
            expect($scope.changeEmailData.confirmEmail).toBe("");
        });
    });
    describe('verifyEmail', function () {
        it('should send a valid request for an email change', function () {
            var verifyEmailRequest = {
                Email: "old@email.com"
            }
            $scope.changeEmailData.oldEmail = "old@email.com";

            $httpBackend.expectPOST(HTTP_SERVICE_URL + 'Api/Account/VerifyEmail').respond(200, verifyEmailRequest);
            $scope.verifyEmail();
            $httpBackend.flush();
            expect($scope.validateOldEmail).toBe(true);
        });
        it('should send an invalid request for an email change', function () {
            $httpBackend.expectPOST(HTTP_SERVICE_URL + 'Api/Account/VerifyEmail').respond(403);
            $scope.verifyEmail();
            $httpBackend.flush();
            expect($scope.validateOldEmail).toBe(false);

        });
    });

    describe('clearOldEmail', function () {
        it('should set changeEmailDta.oldEmail to empty', function () {
            $scope.changeEmailData.oldEmail = "NotEmpty";
            $scope.clearOldEmail();
            expect($scope.changeEmailData.oldEmail).toBe("");
        });
    });

    describe('Verify Methods', function () {
        beforeEach(function () {
            $scope.ChangeEmailForm = {
                confirmEmail: { $invalid: "a value" },
                newEmail: { $error: {} }
            }
        });


        describe('verifyConfirmationEmail', function () {

            it('should return true when email is valid and emails match', function () {
                $scope.ChangeEmailForm.confirmEmail.$invalid = false;
                $scope.changeEmailData.newEmail = "new@email.com";
                $scope.changeEmailData.confirmEmail = "new@email.com";

                expect($scope.verifyConfirmationEmail()).toBe(true);
            });

            it('should return false when email is invalid ', function () {
                $scope.ChangeEmailForm.confirmEmail.$invalid = true;
                $scope.changeEmailData.newEmail = "new@email.com";
                $scope.changeEmailData.confirmEmail = "new@email.com";

                expect($scope.verifyConfirmationEmail()).toBe(false);
            });


            it('should return false when emails do not match', function () {
                $scope.ChangeEmailForm.confirmEmail.$invalid = false;
                $scope.changeEmailData.newEmail = "another@email.com";
                $scope.changeEmailData.confirmEmail = "new@email.com";

                expect($scope.verifyConfirmationEmail()).toBe(false);
            });

            it('should return false when confirm email is invalid', function () {
                $scope.ChangeEmailForm.confirmEmail.$invalid = true;
                $scope.changeEmailData.newEmail = "another@email.com";
                $scope.changeEmailData.confirmEmail = "new@email.com";

                expect($scope.verifyConfirmationEmail()).toBe(false);
            });
        });

        describe('validateEmailForm', function () {
            it('should return true when old email is valid and emails match and new email is a valid email', function () {
                $scope.validateOldEmail = true;
                $scope.changeEmailData.newEmail = "new@email.com";
                $scope.ChangeEmailForm.newEmail.$error.email = false;
                spyOn($scope, "verifyConfirmationEmail").and.returnValue(true);

                expect($scope.validateEmailForm()).toBe('');
            });
            it('should return false when new email is an invalid email', function () {
                $scope.validateOldEmail = true;
                $scope.changeEmailData.newEmail = "new@email.com";
                $scope.ChangeEmailForm.newEmail.$error.email = true;
                spyOn($scope, "verifyConfirmationEmail").and.returnValue(true);

                expect($scope.validateEmailForm()).toBe(false);
            });
            it('should return false when old email is invalid ', function () {
                $scope.validateOldEmail = false;
                $scope.changeEmailData.newEmail = "new@email.com";
                $scope.ChangeEmailForm.newEmail.$error.email = false;
                spyOn($scope, "verifyConfirmationEmail").and.returnValue(true);

                expect($scope.validateEmailForm()).toBe('');
            });
            it('should return false when emails do not match ', function () {
                $scope.validateOldEmail = true;
                $scope.changeEmailData.newEmail = "new@email.com";
                $scope.ChangeEmailForm.newEmail.$error.email = false;
                spyOn($scope, "verifyConfirmationEmail").and.returnValue(false);

                expect($scope.validateEmailForm()).toBe(false);
            });

            it('should return fallse when change email form is not populated', function () {
                $scope.validateOldEmail = true;
                $scope.changeEmailData.newEmail = "new@email.com";
                $scope.ChangeEmailForm = null;
                spyOn($scope, "verifyConfirmationEmail").and.returnValue(true);

                expect($scope.validateEmailForm()).toBe(false);
            });
        });
    });
});
