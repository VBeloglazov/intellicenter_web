﻿'use strict';
var PWC = PWC || {};
PWC.controller('UserNotificationController', [
    '$scope', '$location', 'PropertyService', 'PropertyModel',
    function ($scope, $location, propertyService, propertyModel) {

        //SCOPE vars
        $scope.Property = propertyService;
        $scope.Model = propertyModel;
        $scope.$parent.Layout = "layout-base";
        //END

        //SCOPE Methods 
        $scope.GoToConfigPage = function () {
            $location.path("/configure");
        };

        $scope.ToggleItem = function (value, item) {
            if (value === "OFF") {
                propertyService.SaveUserNotification(item, 0);
            } else {
                propertyService.SaveUserNotification(item, 24);
            }
        };
    }
]);