﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pentair.Domain.Client.Entity
{
    public class UserInvitation
    {
        [Key]
        public Guid InvitationId { get; set; }

        public string EmailAddress { get; set; }

        public int? InstallationId { get; set; }

        [ForeignKey("InstallationId")]
        public virtual Installation Installation { get; set; }

        public string AccessToken { get; set; }

        public string Status { get; set; }

        public string EmailId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(TypeName = "DateTime2")]
        public DateTime CreateDateTime { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [Column(TypeName = "DateTime2")]
        public DateTime UpdateDateTime { get; set; }

    }
}
