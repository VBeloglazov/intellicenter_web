﻿'use strict';
var PWC = PWC || {};
PWC.controller('FeaturesController', [
    '$scope', '$location', '$filter', 'PropertyService', 'PropertyModel',
    function ($scope, $location, $filter, propertyService, propertyModel) {

        $scope.Property = propertyService;
        $scope.Model = propertyModel;
        $scope.$parent.Layout = "layout-base";

        $scope.IntelliHelp = false;
        $scope.MagicHelp = false;


        $scope.ToggleIntelliHelp = function () {
            $scope.IntelliHelp = !$scope.IntelliHelp;
        };


        $scope.ToggleMagicHelp = function () {
            $scope.MagicHelp = !$scope.MagicHelp;
        };


        $scope.GoToDashboard = function () {
            $location.path("/property");
        }

        $scope.setStatusCircuit = function (obj, checkbox) {
            if (obj && obj.OBJNAM && obj.STATUS) {
                if (obj.STATUS === "OFF")
                    $scope.Property.setTokenCircuit(obj, 'STATUS', "OFF")   
                else
                    $scope.Property.setTokenCircuit(obj, 'STATUS', "ON")

            }
        }
    }
]);