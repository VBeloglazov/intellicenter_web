﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using System.Threading.Tasks;

namespace Pentair.Web.Api
{
    // TODO: Splitting the ControlObjects into current and history
    public class ControlObjectsHistory
    {
        public int Installation { get; set; }
        public long Revision { get; set; }
        public string ObjNam { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
        public DateTime CreateDateTime { get; set; }
        public DateTime UpdateDateTime { get; set; }

    } // class ControlObjectsHistory
}