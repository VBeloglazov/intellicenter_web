﻿using System;
using System.Data.Entity.Infrastructure;
using System.Linq;
using Pentair.Domain.Client.Entity;
using Pentair.Domain.Client.Helpers;
using Pentair.Domain.Client.Helpers.Entity;
using Pentair.Domain.Client.Infrastructure;
using System.Diagnostics;

namespace Pentair.Domain.Client.Repository
{
    public static class CreateDatabase
    {
        public static void ApplyUpdates(
            ApiContext db)
        {
            try
            {
                db.Database.ExecuteSqlCommand(
                    @"ALTER TABLE [dbo].[ControlObjects] DROP CONSTRAINT [PK_dbo.ControlObjects]"
                );

                db.Database.ExecuteSqlCommand(
                    @"DROP INDEX [IX_ControlObjects_OBJNAM_Key] ON [dbo].[ControlObjects]"
                );

                db.Database.ExecuteSqlCommand(
                    @"ALTER TABLE [dbo].[ControlObjects] ALTER COLUMN OBJNAM nvarchar(16) COLLATE SQL_Latin1_General_CP1_CS_AS NOT NULL"
                );

                db.Database.ExecuteSqlCommand(
                    @"ALTER TABLE [dbo].[ControlObjects] ADD CONSTRAINT [PK_dbo.ControlObjects] PRIMARY KEY ([INSTALLATION] ASC, [Revision] ASC, [OBJNAM] ASC, [Key] ASC)"
                );

                db.Database.ExecuteSqlCommand(
                    @"CREATE NONCLUSTERED INDEX [IX_ControlObjects_OBJNAM_Key] ON [dbo].[ControlObjects] ([OBJNAM] ASC, [Key] ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80)"
                );

                db.Database.ExecuteSqlCommand(
                    @"ALTER TABLE [dbo].[UserInvitations] ALTER COLUMN AccessToken nvarchar(16) COLLATE SQL_Latin1_General_CP1_CS_AS NOT NULL"
                );

                db.Database.ExecuteSqlCommand(
                    @"CREATE TRIGGER Trg_UpdateControlObjects ON [dbo].[ControlObjects] AFTER UPDATE AS UPDATE [dbo].[ControlObjects] SET UpdateDateTime = GETUTCDATE() WHERE (OBJNAM IN (SELECT DISTINCT OBJNAM FROM Inserted) AND INSTALLATION IN (SELECT DISTINCT INSTALLATION FROM Inserted) AND Revision IN (SELECT DISTINCT Revision FROM Inserted) AND [Key] IN (SELECT DISTINCT [Key] FROM Inserted))"
                );

                db.Database.ExecuteSqlCommand(
                    @"CREATE TRIGGER [Trg_UpdateAssociation] ON [dbo].[Associations] AFTER UPDATE AS UPDATE [dbo].[Associations] SET UpdateDateTime = GETUTCDATE() WHERE (InstallationId IN (SELECT DISTINCT InstallationId FROM Inserted) AND UserId IN (SELECT DISTINCT UserId FROM Inserted))"
                );

                db.Database.ExecuteSqlCommand(
                    @"CREATE TRIGGER [Trg_UpdateInstallation] ON [dbo].[Installations] AFTER UPDATE AS UPDATE [dbo].[Installations] SET UpdateDateTime = GETUTCDATE() WHERE InstallationId IN (SELECT DISTINCT InstallationId FROM Inserted)"
                );

                db.Database.ExecuteSqlCommand(
                    @"CREATE TRIGGER [Trg_UpdateInvite] ON [dbo].[UserInvitations] AFTER UPDATE AS UPDATE [dbo].[UserInvitations] SET UpdateDateTime = GETUTCDATE() WHERE InvitationId IN (SELECT DISTINCT InvitationId FROM Inserted)"
                );

                db.Database.ExecuteSqlCommand(
                    @"CREATE TRIGGER [Trg_UpdateUserNotification] ON [dbo].[UserNotifications] AFTER UPDATE AS UPDATE [dbo].[UserNotifications] SET UpdateDateTime = GETUTCDATE() WHERE (UserId IN (SELECT DISTINCT UserId FROM Inserted) AND InstallationId IN (SELECT DISTINCT InstallationId FROM Inserted) AND EventId IN (SELECT DISTINCT EventId FROM Inserted))"
                );
            }
            catch (Exception e)
            {
                LogEvent(
                    e.Message + ((e.InnerException != null) ? "\n\nInnerException:\n" + e.InnerException.Message : String.Empty),
                    EventLogEntryType.Error
                );
            }

        } // ApplyUpdates()

        public static void SeedEnumerators(
            ApiContext db)
        {
            var eventItems = Enum.GetValues(typeof(EventTypeCodes));

            foreach (var item in eventItems)
            {
                if ((from e in db.EventTypes where e.EventId == (int)item select e).FirstOrDefault() == null)
                {
                    db.EventTypes.Add(
                        new EventType() { Name = item.ToString() }
                    );
                }
            }

            db.NotificationMessages.Add(new NotificationMessage() { MessageId = "FRZ", OBJTYP = StaticData.CircuitType, Notes = "Freeze protection is ON." });
            db.NotificationMessages.Add(new NotificationMessage() { MessageId = "VOLT", OBJTYP = StaticData.PumpType, Notes = "Pump voltage alert notes" });
            db.NotificationMessages.Add(new NotificationMessage() { MessageId = "CURRENT", OBJTYP = StaticData.PumpType, Notes = "Pump current alert notes." });
            db.NotificationMessages.Add(new NotificationMessage() { MessageId = "FAULT", OBJTYP = StaticData.PumpType, Notes = "Pump Fault notes." });
            db.NotificationMessages.Add(new NotificationMessage() { MessageId = "ALARM", OBJTYP = StaticData.PumpType, Notes = "Pump Alarm notes." });
            db.NotificationMessages.Add(new NotificationMessage() { MessageId = "SYSTEM", OBJTYP = StaticData.PumpType, Notes = "Pump System alert notes." });
            db.NotificationMessages.Add(new NotificationMessage() { MessageId = "PRIME", OBJTYP = StaticData.PumpType, Notes = "Pump Priming alarm notes." });
            db.NotificationMessages.Add(new NotificationMessage() { MessageId = "DRVALM", OBJTYP = StaticData.PumpType, Notes = "Pump Drive alarm notes." });
            db.NotificationMessages.Add(new NotificationMessage() { MessageId = "FILTER", OBJTYP = StaticData.PumpType, Notes = "Pump Filter alarm notes." });
            db.NotificationMessages.Add(new NotificationMessage() { MessageId = "COMERR", OBJTYP = StaticData.PumpType, Notes = "Pump Communications error notes." });
            db.NotificationMessages.Add(new NotificationMessage() { MessageId = "DEVICE", OBJTYP = "ICHLOR", Notes = "IntelliChlor Device error notes." });
            db.NotificationMessages.Add(new NotificationMessage() { MessageId = "VOLT", OBJTYP = "ICHLOR", Notes = "Intellichlor voltage alert notes." });
            db.NotificationMessages.Add(new NotificationMessage() { MessageId = "CURRENT", OBJTYP = "ICHLOR", Notes = "Intellichlor current alert notes." });
            db.NotificationMessages.Add(new NotificationMessage() { MessageId = "CUTOFF", OBJTYP = "ICHLOR", Notes = "Intellichlor cutoff alert notes." });
            db.NotificationMessages.Add(new NotificationMessage() { MessageId = "SALTLO", OBJTYP = "ICHLOR", Notes = "Intellichlor salt low notes." });
            db.NotificationMessages.Add(new NotificationMessage() { MessageId = "VERYLO", OBJTYP = "ICHLOR", Notes = "Intellichlor salt very low notes." });
            db.NotificationMessages.Add(new NotificationMessage() { MessageId = "CLEAN", OBJTYP = "ICHLOR", Notes = "Intellichlor clean alert notes." });
            db.NotificationMessages.Add(new NotificationMessage() { MessageId = "COMERR", OBJTYP = "ICHLOR", Notes = "Intellichlor comunications error notes." });
            db.NotificationMessages.Add(new NotificationMessage() { MessageId = "NOFLO", OBJTYP = "ICHEM", Notes = "Intellichem no flow notes." });
            db.NotificationMessages.Add(new NotificationMessage() { MessageId = "PHHI", OBJTYP = "ICHEM", Notes = "Intellichem pH high notes." });
            db.NotificationMessages.Add(new NotificationMessage() { MessageId = "PHLO", OBJTYP = "ICHEM", Notes = "Intellichem pH low notes." });
            db.NotificationMessages.Add(new NotificationMessage() { MessageId = "ORPLO", OBJTYP = "ICHEM", Notes = "Intellichem ORP low notes." });
            db.NotificationMessages.Add(new NotificationMessage() { MessageId = "PHCHK", OBJTYP = "ICHEM", Notes = "Intellichem pH check notes." });
            db.NotificationMessages.Add(new NotificationMessage() { MessageId = "ORPCHK", OBJTYP = "ICHEM", Notes = "Intellichem ORP check notes." });
            db.NotificationMessages.Add(new NotificationMessage() { MessageId = "LOCKOUT", OBJTYP = "ICHEM", Notes = "Intellichem lock out alert notes." });
            db.NotificationMessages.Add(new NotificationMessage() { MessageId = "PHLIM", OBJTYP = "ICHEM", Notes = "Intellichem pH limit notes." });
            db.NotificationMessages.Add(new NotificationMessage() { MessageId = "ORPLIM", OBJTYP = "ICHEM", Notes = "Intellichem ORP limit notes." });
            db.NotificationMessages.Add(new NotificationMessage() { MessageId = "INVALID", OBJTYP = "ICHEM", Notes = "Intellichem invalid exception notes." });
            db.NotificationMessages.Add(new NotificationMessage() { MessageId = "CALIB", OBJTYP = "ICHEM", Notes = "Intellichem calibration alert notes." });
            db.NotificationMessages.Add(new NotificationMessage() { MessageId = "COMERR", OBJTYP = "ICHEM", Notes = "Intellichem communications error notes." });
            db.NotificationMessages.Add(new NotificationMessage() { MessageId = "ORPHI", OBJTYP = "ICHEM", Notes = "Intellichem ORP high notes." });
            db.NotificationMessages.Add(new NotificationMessage() { MessageId = "PROBE", OBJTYP = "ICHEM", Notes = "Intellichem probe exception notes." });
            db.NotificationMessages.Add(new NotificationMessage() { MessageId = "COMLNK", OBJTYP = "ICHEM", Notes = "Intellichem communications link alert notes." });
            db.NotificationMessages.Add(new NotificationMessage() { MessageId = "DEVICE", OBJTYP = "ICHEM", Notes = "Intellichem device alert notes." });
            db.NotificationMessages.Add(new NotificationMessage() { MessageId = "HITMP", OBJTYP = StaticData.HeaterType, Notes = "Ultra T heater high temp alert notes." });
            db.NotificationMessages.Add(new NotificationMessage() { MessageId = "LOTMP", OBJTYP = StaticData.HeaterType, Notes = "Ultra T heater low temp alert notes." });
            db.NotificationMessages.Add(new NotificationMessage() { MessageId = "FLOW", OBJTYP = StaticData.HeaterType, Notes = "Ultra T heater flow alert notes." });
            db.NotificationMessages.Add(new NotificationMessage() { MessageId = "RLY", OBJTYP = StaticData.HeaterType, Notes = "Ultra T heater relay alert notes." });
            db.NotificationMessages.Add(new NotificationMessage() { MessageId = "OFF", OBJTYP = StaticData.HeaterType, Notes = "Ultra T heater off alert notes." });
            db.NotificationMessages.Add(new NotificationMessage() { MessageId = "ON", OBJTYP = StaticData.HeaterType, Notes = "Ultra T heater on alert notes." });
            db.NotificationMessages.Add(new NotificationMessage() { MessageId = "OVROFF", OBJTYP = StaticData.HeaterType, Notes = "Ultra T heater override off alert notes." });
            db.NotificationMessages.Add(new NotificationMessage() { MessageId = "OVRON", OBJTYP = StaticData.HeaterType, Notes = "Ultra T heater overridd on alert notes." });
            db.NotificationMessages.Add(new NotificationMessage() { MessageId = "OUT", OBJTYP = StaticData.HeaterType, Notes = "Ultra T heater out alert notes." });
            db.NotificationMessages.Add(new NotificationMessage() { MessageId = "HI", OBJTYP = StaticData.HeaterType, Notes = "Ultra T heater high alert notes." });
            db.NotificationMessages.Add(new NotificationMessage() { MessageId = "LO", OBJTYP = StaticData.HeaterType, Notes = "Ultra T heater low alert notes." });
        
        } // SeedEnumerators()

        private static void LogEvent(
            String message,
            EventLogEntryType type)
        {
            if (!EventLog.SourceExists("IntelliCenter"))
            {
                EventLog.CreateEventSource("IntelliCenter", "Application");
            }

            // Create an EventLog instance and assign its source.
            EventLog myLog = new EventLog()
            {
                Source = "IntelliCenter"
            };

            // Write an informational entry to the event log.    
            myLog.WriteEntry(message, type);

        } // LogEvent()

    } // class CreateDatabase
}
