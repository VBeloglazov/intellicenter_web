﻿using System.Diagnostics;
using System.IO;
using System.Web.Caching;
using System.Web.UI;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Text;
using System.Threading;
using System.Web.Script.Services;
using System.Web.Services;

namespace Pentair.Desktop.Client
{
    internal class BrowserService : IDisposable
    {
        private volatile string _uriPort = "http://localhost:12001/";
        private volatile string _rootAddress;
        private bool _keepWorking;

        private readonly Thread _myThread;
        private volatile Discovery _discoveryService;
        private volatile HttpListener _server;

        public BrowserService(Discovery discoveryService, string root, string endpoint)
        {
            _rootAddress = root;
            _uriPort = endpoint;
            _discoveryService = discoveryService;
            _keepWorking = true;
            _myThread = new Thread(DoWork);
            _myThread.Start();
        }

        ~BrowserService()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (null != _server)
                {
                    _keepWorking = false;
                    _server.Stop();
                    _myThread.Join();
                }
            }
        }

        private void DoWork()
        {
            try
            {
                _server = new HttpListener();
                _server.Prefixes.Add(_uriPort);
                _server.Start();
                while (_keepWorking)
                {
                    // Do blocking communications.
                    var context = _server.GetContext();
                    var request = context.Request;
                    var response = context.Response;
                    var encoder = new UTF8Encoding();
                    if (request.RawUrl == "/localConnections")
                    {
                        var buffer = encoder.GetBytes(GenerateResponse(request));
                        response.ContentLength64 = buffer.Length;
                        response.AddHeader("Cache-Control", "no-cache,private");
                        response.AddHeader("Pragma", "no-cache");
                        response.OutputStream.Write(buffer, 0, buffer.Length);
                        _discoveryService.BrowserValid();
                    }
                    else
                    {
                        var requestAddress = (_rootAddress + request.RawUrl).Replace("/", "\\");
                        var file = new FileInfo(requestAddress);
                        if (file.Exists)
                        {
                            using (var sr = new StreamReader(file.FullName))
                            {
                                var buffer = encoder.GetBytes(sr.ReadToEnd());
                                response.ContentLength64 = buffer.Length;
                                response.ContentType = getContentType(file.Extension);
                                response.OutputStream.Write(buffer, 0, buffer.Length);
                            }
                        }
                    }
                    response.OutputStream.Close();
                    response.OutputStream.Flush();
                    Thread.Yield();
                }
            }
            catch (Exception ex)
            {
                var error = ex.Message;
            }
            finally
            {
                if (null != _server)
                {
                    _server.Stop();
                }
            }
        }

        private string getContentType(string extension)
        {
            switch (extension)
            {
                case ".avi":
                    return "video/x-msvideo";
                case ".png":
                    return "image/png";
                case ".svg":
                    return "image/svg+xml";
                case ".html":
                    return "text/html";
                case ".js":
                    return "application/x-javascript";
                default:
                    return "text/css";
            }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        private string GenerateResponse(HttpListenerRequest request)
        {
            var list = new Connections();
            lock (MainWindow.LockData)
            {
                foreach (var item in _discoveryService.Services)
                {
                    if (item.Name.StartsWith("Pentair: "))
                    {
                        var connection = new Connection()
                        {
                            name = item.Name.Substring(9),
                            address = null == item.Address ? null : item.Address.ToString(),
                            port = item.Port
                        };
                        if (!list.connections.Contains(connection))
                        {
                            list.connections.Add(connection);
                        }
                    }
                }
            }
            return (JsonConvert.SerializeObject(list));
        }
    }
}
