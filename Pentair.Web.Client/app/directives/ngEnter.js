﻿var PWC = PWC || {};
PWC.directive('ngEnter', function () {
    return function (scope, element, attrs) {

        //invokes method when the enter key is pressed
        element.bind("keydown keypress", function (event) {
            if (event.which === 13) {
                scope.$apply(function () {
                    event.target.blur();
                    if (attrs.ngEnter) {
                        scope.$eval(attrs.ngEnter);
                    }
                });
                event.preventDefault();
            }
        });
    };
});