﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Threading.Tasks;

using Microsoft.AspNet.SignalR.Client;

using SERVICE_SHARED;

namespace Pentair.Web.Api
{
    public partial class EsbClientConnection
    {
        private class RecycleWatcher : IRegisteredObject
        {
            public bool IsRecycled { get; private set; }

            public RecycleWatcher()
            {
                this.IsRecycled = false;

            } // RecycleWatcher()

            public void Stop(
                bool immediate)
            {
                this.IsRecycled = true;

                HostingEnvironment.UnregisterObject(this);

            } // Stop()

        } // class RecycleWatcher

    } // class EsbClientConnection
}