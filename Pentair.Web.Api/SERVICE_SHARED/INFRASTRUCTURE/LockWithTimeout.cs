/*
--------------------------------------------------------------------------------

    LockWithTimeout.cs

    Copyright � Pentair Water Pool and Spa, Inc. 2015

--------------------------------------------------------------------------------
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SERVICE_SHARED
{
    public static class LockWithTimeout
    {
        /// <exception cref="Exception" />
        public static void DoWithLock(
            object lockObj,
            int? timeoutMSec,
            Action action)
        {
            bool lockTaken = false;

            try
            {
                Monitor.TryEnter(lockObj, timeoutMSec ?? Timeout.Infinite, ref lockTaken);

                if (!lockTaken)
                {
                    throw new Exception(String.Format("Lock timeout: {0} msec", timeoutMSec));
                }

                action();
            }
            finally
            {
                if (lockTaken)
                {
                    Monitor.Exit(lockObj);
                }
            }

        } // DoWithLock()

        // TODO: 2b deleted! Use LockAsyncSlim.LockAsync() instead.
        /// <exception cref="Exception" />
        //public static Task DoWithLockAsync(
        //    object lockObj,
        //    int? timeoutMSec,
        //    Func<Task> actionAsync)
        //{
        //    bool lockTaken = false;
        //
        //    try
        //    {
        //        Monitor.TryEnter(lockObj, timeoutMSec ?? Timeout.Infinite, ref lockTaken);
        //
        //        if (!lockTaken)
        //        {
        //            throw new Exception(String.Format("Lock timeout: {0:0.000} msec", timeoutMSec));
        //        }
        //
        //        // ERROR: Returning here causes the lock release before the actionAsync completion!
        //        return actionAsync();
        //    }
        //    finally
        //    {
        //        if (lockTaken)
        //        {
        //            Monitor.Exit(lockObj);
        //        }
        //    }
        //
        //} // DoWithLockAsync()

    } // class LockWithTimeout
}