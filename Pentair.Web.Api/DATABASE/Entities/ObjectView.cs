﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pentair.Web.Api
{
    // TODO: 2b removed
    /*
    public class ObjectView
    {
        [Key]
        [Column(Order = 1)]
        public int INSTALLATION { get; set; }

        [Key]
        [Column(Order = 2)]
        [MaxLength(16)]
        public string OBJNAM { get; set; }

        public string OBJTYP { get; set; }
        public string ABSMAX { get; set; }
        public string ABSMIN { get; set; }
        public string ABSTIM { get; set; }
        public string ACCEL { get; set; }
        public string ACT { get; set; }
        public string ACT1 { get; set; }
        public string ACT2 { get; set; }
        public string ACT3 { get; set; }
        public string ACT4 { get; set; }
        public string ADDRESS { get; set; }
        public string ALPHA { get; set; }
        public string ASSIGN { get; set; }
        public string AVAIL { get; set; }
        public string BADGE { get; set; }
        public string BAKFLO { get; set; }
        public string BAKTIM { get; set; }
        public string BASE { get; set; }
        public string BODY { get; set; }
        public string BOOST { get; set; }
        public string CALIB { get; set; }
        public string CHEM { get; set; }
        public string CIRCUIT { get; set; }
        public string CITY { get; set; }
        public string CLK24A { get; set; }
        public string CNFG { get; set; }
        public string COLOR { get; set; }
        public string COMETHER { get; set; }
        public string COMSPI { get; set; }
        public string COMUART { get; set; }
        public string COUNT { get; set; }
        public string COUNTRY { get; set; }
        public string CUSTOM { get; set; }
        public string CYCTIM { get; set; }
        public string DAY { get; set; }
        public string DEVCONN { get; set; }
        public string DFGATE { get; set; }
        public string DFLT { get; set; }
        public string DHCP { get; set; }
        public string DIMMER { get; set; }
        public string DLSTIM { get; set; }
        public string DLY { get; set; }
        public string DNTSTP { get; set; }
        public string EMAIL { get; set; }
        public string EMAIL2 { get; set; }
        public string ENABLE { get; set; }
        public string FEATR { get; set; }
        public string FILTER { get; set; }
        public string FREEZE { get; set; }
        public string FREQ { get; set; }
        public string GROUP { get; set; }
        public string HEATER { get; set; }
        public string HITMP { get; set; }
        public string HNAME { get; set; }
        public string IPADY { get; set; }
        public string LEGACY { get; set; }
        public string LIMIT { get; set; }
        public string LISTORD { get; set; }
        public string LOCX { get; set; }
        public string LOCY { get; set; }
        public string LOGO { get; set; }
        public string LOTMP { get; set; }
        public string LSTTMP { get; set; }
        public string MACADY { get; set; }
        public string MANHT { get; set; }
        public string MANOVR { get; set; }
        public string MANUAL { get; set; }
        public string MASKSM { get; set; }
        public string MAX { get; set; }
        public string MENU { get; set; }
        public string MIN { get; set; }
        public string MNFDAT { get; set; }
        public string MODE { get; set; }
        public string MODULE { get; set; }
        public string NAME { get; set; }
        public string NORMAL { get; set; }
        public string OBJREV { get; set; }
        public string OFF { get; set; }
        public string ON { get; set; }
        public string PARENT { get; set; }
        public string PARTY { get; set; }
        public string PASSWRD { get; set; }
        public string PERMIT { get; set; }
        public string PHONE { get; set; }
        public string PHONE2 { get; set; }
        public string PORT { get; set; }
        public string POSIT { get; set; }
        public string PRIM { get; set; }
        public string PRIMFLO { get; set; }
        public string PRIMTIM { get; set; }
        public string PRIOR { get; set; }
        public string PUMP { get; set; }
        public string RECENT { get; set; }
        public string RINSTIM { get; set; }
        public string RLY { get; set; }
        public string SEC { get; set; }
        public string SELECT { get; set; }
        public string SENSE { get; set; }
        public string SERNUM { get; set; }
        public string SERVICE { get; set; }
        public string SHARE { get; set; }
        public string SHOMNU { get; set; }
        public string SINDEX { get; set; }
        public string SINGLE { get; set; }
        public string SMTSRT { get; set; }
        public string SNAME { get; set; }
        public string SOURCE { get; set; }
        public string SPEED { get; set; }
        public string START { get; set; }
        public string STATE { get; set; }
        public string STATIC { get; set; }
        public string STOP { get; set; }
        public string SUBNET { get; set; }
        public string SUBTYP { get; set; }
        public string SUPER { get; set; }
        public string SYSTEM { get; set; }
        public string SYSTIM { get; set; }
        public string TIME { get; set; }
        public string TIMOUT { get; set; }
        public string TIMZON { get; set; }
        public string URL { get; set; }
        public string USAGE { get; set; }
        public string USE { get; set; }
        public string VACFLO { get; set; }
        public string VACTIM { get; set; }
        public string VER { get; set; }
        public string VOL { get; set; }
        public string WAPNET { get; set; }
        public string ZIP { get; set; }
        public bool Deleted { get; set; }
    }
     */
}
