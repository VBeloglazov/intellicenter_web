﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Pentair.Web.Api
{
    public class KeyValueCommand : CommandBase
    {
        [JsonProperty("params")]
        public Dictionary<string, string> Parameters { get; private set; }

        public KeyValueCommand(
            string command,
            string messageID,
            Dictionary<string, string> parameters
            )
            : base(command, messageID)
        {
            this.Parameters = parameters;
        }

    } // class KeyValueCommand
}
