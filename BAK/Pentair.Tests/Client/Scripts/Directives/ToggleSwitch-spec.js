﻿describe('Toggle Switch', function () {
    var $scope, $compile;

    var baseTemplate = '<old-toggle-switch toggle-onclick="ToggleSwitch(value)" toggle-model="testVal" /></old-toggle-switch>';
    var disabledTemplate = '<old-toggle-switch toggle-onclick="ToggleSwitch(value)" disabled="isDisabled" toggle-model="testVal"></old-toggle-switch>';

    beforeEach(module('PentairWebClient'));

    beforeEach(inject(function ($rootScope, _$compile_) {
        // Get an isolated scope
        $scope = $rootScope.$new();
        $compile = _$compile_;
        $scope.testVal = 'OFF';
        $scope.ToggleSwitch = jasmine.createSpy('ToggleSwitch');
    }));

    describe('when model is null', function () {
        beforeEach(function () {
            $scope.testVal = null;
        });
        it('changes model to ON when clicked', function () {
            $scope.ToggleSwitch = jasmine.createSpy();
            var elm = SpecHelper.createDirective($compile, baseTemplate, $scope);

            elm.triggerHandler('click');
            expect($scope.ToggleSwitch).toHaveBeenCalledWith('ON');
        });
    });

     describe('when model is undefined', function () {
        beforeEach(function () {
            $scope.testVal = undefined;
        });
        it('should not do anything when clicked', function () {
            $scope.ToggleSwitch = jasmine.createSpy();
            var elm = SpecHelper.createDirective($compile, baseTemplate, $scope);

            elm.triggerHandler('click');
            expect($scope.ToggleSwitch).not.toHaveBeenCalled();
        });
     });

    describe('when state is ON', function () {
        beforeEach(function () {
            $scope.testVal = "ON";
        });

        it('changes model to OFF when clicked', function () {
            var elm = SpecHelper.createDirective($compile, baseTemplate, $scope);
            elm.triggerHandler('click');
            expect($scope.ToggleSwitch).toHaveBeenCalledWith('OFF');
        });
    });

    describe('when state is OFF', function () {
        beforeEach(function () {
            $scope.testVal = "OFF";
        });

        it('changes model to ON when clicked', function () {
            var elm = SpecHelper.createDirective($compile, baseTemplate, $scope);
            elm.triggerHandler('click');
            expect($scope.ToggleSwitch).toHaveBeenCalledWith('ON');
        });
    });

    describe('when model changes', function () {
        var elm;

        beforeEach(function () {
            $scope.testVal = "OFF";
            elm = SpecHelper.createDirective($compile, baseTemplate, $scope);
        });

        it('shout toggle the switch ON when model is updated', function () {
            var switchCtrl = $(elm[0]);
            expect(switchCtrl.hasClass("switch-on")).toEqual(false);
            expect($scope.testVal).toEqual("OFF");

            $scope.$apply(function () {
                $scope.testVal = "ON";
            });
            expect(switchCtrl.hasClass("switch-on")).toEqual(true);
        });

    });


    describe('when toggle is disabled', function () {
        it('model does not change on click', function () {
            $scope.testVal = "OFF";
            $scope.isDisabled = true;
            var elm = SpecHelper.createDirective($compile, disabledTemplate, $scope);
            elm.triggerHandler('click');
            expect($scope.testVal).toEqual("OFF");
        });
    });


});