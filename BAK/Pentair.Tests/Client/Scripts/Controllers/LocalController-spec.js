﻿'use strict';
describe('LocalController', function () {
    var AppService;
    var $location;
    var $scope;
    var Index2Controller;
    var $controller;
    beforeEach(module('PentairWebClient'));

    beforeEach(inject(function (_$controller_, _$rootScope_, _$location_) {
        $scope = _$rootScope_.$new();
        $location = _$location_;
        $controller = _$controller_;

        Index2Controller = $controller("LocalController", { $scope: $scope });
    }));

    describe('Default Vars', function () {
        it('should have the following default variables', function () {
            expect($scope.Busy).toBe(false);
            expect($scope.Properties).toEqual([]);
        });
    });

});
