﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web.Http;
using System.Windows;
using System.Collections.ObjectModel;
using System.Windows.Media;
using Newtonsoft.Json;
using Pentair.Desktop.Simulator.BusinessLogic;
using Pentair.Desktop.Simulator.Services;
using Pentair.Desktop.Simulator.Views;
using Pentair.Domain.Client.Entity;
using Pentair.Domain.Client.Helpers;
using Pentair.Domain.Client.Helpers.Commands;
using Pentair.Domain.Client.Infrastructure;
using Pentair.Domain.Client.Repository;

namespace Pentair.Desktop.Simulator.Models
{
    public class MainViewModel : INotifyPropertyChanged, IMainViewModel
    {
        #region Private Properties

        private MainWindow main;
        private List<ControlObject> _config;
        private LocalServer _httpServer = null;
        private Discovery _discoveryService = null;
        private ObservableCollection<string> _properties = null;
        private string _selectedProperty = null;
        private ClientSocket _clientWebSocket = null;
        private SolidColorBrush _serverStatus = BusinessLogic.Helpers.StatusBrush(ConnectionStatus.Disabled);
        private SolidColorBrush _discoveryStatus = BusinessLogic.Helpers.StatusBrush(ConnectionStatus.Disabled);
        private SolidColorBrush _hostStatus = BusinessLogic.Helpers.StatusBrush(ConnectionStatus.Disabled);
        private bool _serverEnabled = false;
        private bool _discoveryEnabled = false;
        private bool _hostEnabled = false;
        private readonly MessageProcessor _messageProcessor;
        private DateTime _systemClock;
        private Timer _clockTimer;

        #endregion

        #region Events and Delegates

        /// <summary>
        /// Property Changed event handler
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        public event OnPropertyChangedEvent OnPropertyChange;

        public delegate void OnPropertyChangedEvent(string name);

        #endregion

        #region Public Properties
        public Property RegisteringProperty { get; set; }

        public LogModel RemoteLogModel { get; set; }
        public LogModel BrowserLogModel { get; set; }
        public LogModel ServerLogModel { get; set; }

        public ClientSocket Host
        {
            get { return _clientWebSocket; }
        }

        // Log entries displayed in the Messages tab
        public ObservableCollection<LogEntry> Messages { get; set; }

        public MessageProcessor MessageProc
        {
            get { return _messageProcessor; }
        }

        public Discovery Bonjour
        {
            get { return _discoveryService; }
        }

        public int Configuration
        {
            get { return null == _clientWebSocket ? -1 : _clientWebSocket.Configuration; }
            set { if (null != _clientWebSocket) { _clientWebSocket.Configuration = value; } }
        }

        public string SelectedProperty
        {
            get { return _selectedProperty; }
            set
            {
                _selectedProperty = value;
                OnPropertyChanged("SelectedProperty");
            }
        }

        public ObservableCollection<string> Properties
        {
            get { return _properties; }
            private set
            {
                _properties = value;
                OnPropertyChanged("Properties");
            }
        }

        public string SelectedConfiguration { get; set; }

        public SolidColorBrush ServerStatus
        {
            get { return _serverStatus; }
            private set
            {
                if (_serverStatus.Color != value.Color)
                {
                    _serverStatus = value;
                    OnPropertyChanged("ServerStatus");
                }
            }
        }

        public SolidColorBrush DiscoveryStatus
        {
            get { return _discoveryStatus; }
            private set
            {
                if (_discoveryStatus.Color != value.Color)
                {
                    _discoveryStatus = value;
                    OnPropertyChanged("DiscoveryStatus");
                }
            }
        }

        public SolidColorBrush HostStatus
        {
            get { return _hostStatus; }
            private set
            {
                if (_hostStatus.Color != value.Color)
                {
                    _hostStatus = value;
                    OnPropertyChanged("HostStatus");
                }
            }
        }

        public bool ServerEnabled
        {
            get { return _serverEnabled; }
            set
            {
                if (_serverEnabled != value)
                {
                    _serverEnabled = value;
                    OnPropertyChanged("ServerEnabled");
                }
            }
        }

        public bool DiscoveryEnabled
        {
            get { return _discoveryEnabled; }
            set
            {
                if (_discoveryEnabled != value)
                {
                    _discoveryEnabled = value;
                    OnPropertyChanged("DiscoveryEnabled");
                }
            }
        }

        public bool HostEnabled
        {
            get { return _hostEnabled; }
            set
            {
                if (_hostEnabled != value)
                {
                    _hostEnabled = value;
                    OnPropertyChanged("HostEnabled");
                }
            }
        }

        public Dictionary<string, DataModel> Tabs { get; set; }
        public SystemModel SystemConfig { get; set; }
        public ManageUserModel User { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public MainViewModel(MainWindow host)
        {
            main = host;
            const int configId = -1;
            Properties = new ObservableCollection<string>();

            using (var data = new SimulatorContext())
            {
                if (!data.Database.Exists())
                {
                    using (var createData = new CreateDatabase())
                    {
                        try
                        {
                            createData.ApplySimulatorUpdates(data);
                            data.SaveChanges();
                        }
                        catch (Exception ex)
                        {
                            var message = string.Format("Error: {0}", ex.Message);
                            MessageBox.Show(message);
                        }
                    }
                }
                // Check for valid seed data.
                if (!data.ObjectParameterTypes.Any())
                {
                    SeedData(data);
                }

                _messageProcessor = new MessageProcessor();
                _messageProcessor.OnDataChangeEvent += RefreshData;
                SetDataSets(data, configId);

                if (null != SystemConfig)
                {
                    CreateEndpoints();
                }
                SystemConfig = new SystemModel();
            }
            SetConfigurations();
        }

        #endregion

        #region Public Methods

        public void ClearLogItems(LogId log)
        {
            switch (log)
            {
                case LogId.Client:
                    RemoteLogModel.Messages.Clear();
                    break;
                case LogId.Server:
                    ServerLogModel.Messages.Clear();
                    break;
                case LogId.Discovery:
                    BrowserLogModel.Messages.Clear();
                    break;
            }
        }

        public bool ParseConfiguration(string configuration)
        {
            bool result = false;
            var fi = new FileInfo(configuration);
            _config = new List<ControlObject>();

            try
            {
                if (fi.Exists)
                {
                    dynamic json = null;
                    string input = null;
                    using (var rdr = fi.OpenText())
                    {
                        input = rdr.ReadToEnd();
                    }
                    try
                    {
                        json = JsonConvert.DeserializeObject(input);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(string.Format("Error reading json input, {0}", ex.Message),
                            "Error creating custom configuration",
                            MessageBoxButton.OK,
                            MessageBoxImage.Error);
                        result = false;
                    }

                    var _data = new SimulatorContext();

                    var revision = Globals.CurrentTime();
                    if (null != json)
                    {
                        foreach (var o in json.oLst)
                        {
                            string oName = o.oNm.ToString();
                            string oType = o.oTp.ToString();
                            _config.Add(new ControlObject()
                            {
                                OBJNAM = oName,
                                Key = StaticData.ObjectType,
                                Value = oType,
                                Revision = revision
                            });
                            foreach (var p in o.pRms)
                            {
                                foreach (var i in p)
                                {
                                    if (i.Name != "r" && i.Name != "t")
                                    {
                                        _config.Add(new ControlObject()
                                        {
                                            OBJNAM = oName,
                                            Key = i.Name,
                                            Value = i.Value,
                                            Revision = revision
                                        });
                                    }
                                }
                            }

                            var l = (from q in _data.ObjectParameterTypes
                                where q.OBJTYP == oType && !q.Saved
                                select q).ToList();
                            foreach (var i in l)
                            {
                                // Allow the import file to define live parameters
                                if (null != (from j in _config
                                    where j.Key == i.PARAM &&
                                          j.OBJNAM == oName
                                    select j).FirstOrDefault())
                                {
                                    continue;
                                }
                                _config.Add(new ControlObject()
                                {
                                    OBJNAM = oName,
                                    Key = i.PARAM,
                                    Value = i.DEFAULT,
                                    Revision = revision
                                });
                            }
                        }
                        result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format("Error reading json input, {0}", ex.Message),
                    "Error creating custom configuration",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
                result = false;
            }
            return result;
        }

        /// <summary>
        /// Data has been edited, update the database and generate any
        /// Notificatons or Configuration changes.
        /// </summary>
        /// <param name="e">An EditData object containing the changed data and related schema</param>
        public void OnDataChanged(EditData e)
        {
            using (var data = new SimulatorContext())
            {
                var item = (from c in data.ControlObjects
                    where c.OBJNAM == e.Key
                    select c).FirstOrDefault();
                if (null != item)
                {
                    var ctl = new BusinessLogic.BusinessLogic(data, Configuration);
                    
                    var obj = new SetParamListResponse(
                        messageID: String.Empty,
                        response: ResponseString.OK,
                        objectList: new List<ParamObject>()
                    );

                    obj.ObjectList.Add(
                        new ParamObject(
                            objNam: e.Key,
                            parameters: new Dictionary<string, object> {{e.Column.Header.ToString().ToUpper(), e.Value}}
                        )
                    );

                    ctl.SetParamList(obj);
                    
                    var changes = ctl.GetConfigChanges();
                    if (changes.ObjectList.Count > 0 &&
                        changes.ObjectList[0].Changes.Count > 0)
                    {
                        var msg = (JsonConvert.SerializeObject(changes));
                        _clientWebSocket.SendMessage(JsonConvert.SerializeObject(msg));
                        _httpServer.SendMessage(msg);
                    }
                    var mods = ctl.GetNotifications();
                    if (mods.ObjectList.Count > 0)
                    {
                        var msg = (JsonConvert.SerializeObject(mods));
                        _clientWebSocket.SendMessage(JsonConvert.SerializeObject(msg));
                        _httpServer.SendMessage(JsonConvert.SerializeObject(msg));
                    }
                }
            }
        }

        private void SetDataSets(SimulatorContext data, int configId)
        {
            Tabs = new Dictionary<string, DataModel>();
            // Get a copy of all objects with object type for this configuration.
            var objects =
                data.ControlObjects.Where(item => item.INSTALLATION == configId && item.Key == StaticData.ObjectType)
                    .ToList();

            foreach (var item in data.ObjectParameterTypes.GroupBy(x => x.OBJTYP))
            {
                var l = objects.Where(x => x.Value == item.Key).Select(x => x.OBJNAM).ToList();
                var o = data.ControlObjects.Where(x => x.INSTALLATION == configId && l.Contains(x.OBJNAM)).ToList();
                Tabs.Add(item.Key, new DataModel(o, _messageProcessor));
            }
        }

        public void ChangeProperty(int propertyId)
        {
            if (null != _clientWebSocket)
            {
                ClearBrowsability();
                ClearRemoteMode();
            }
            using (var data = new SimulatorContext())
            {
                SystemConfig.Property = data.Properties.FirstOrDefault(x => x.LocalPropertyId == propertyId);
                if (null == SystemConfig.Property)
                {
                    MessageBox.Show("Error, internal data inconsistency.",
                        "Properies table error",
                        MessageBoxButton.OK,
                        MessageBoxImage.Error);
                    return;
                }
                main.RegistrationView.PropertyName = SystemConfig.Property.Name;
                main.RegistrationView.PropertyId = SystemConfig.Property.PropertyId.ToString();
                main.RegistrationView.Secret = SystemConfig.Property.Secret;
                ReplaceConfiguration(data);
            }

            SetHost(SystemConfig.Property);
            _systemClock = DateTime.Now;
            _messageProcessor.UpdateSystemClock(_systemClock);

        }

        public void CloseConnections()
        {
            if (null != _httpServer)
            {
                _httpServer.CloseSessions();
            }
            if (null != _clientWebSocket)
            {
                ClearRemoteMode();
                
            }
            if (null != _discoveryService)
            {
                ClearBrowsability();
            }
        }

        /// <summary>
        /// The host has been changed, set up the client websocket on the new host.
        /// </summary>
        /// <param name="property"property object</param>
        public void SetHost(Property property)
        {
            if (null == _discoveryService ||
                null == _httpServer ||
                null == _clientWebSocket)
            {
                CreateEndpoints();
            }

            try
            {
                _httpServer.ChangeHost(property);
                _clientWebSocket.ChangeHost(property);
                _discoveryService.ChangeHost(property);
            }
            catch
            {
                MessageBox.Show("Error starting web services.", "Services", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }


        private void CreateEndpoints()
        {
            SetBrowsability();
            SetRemoteMode();
            _httpServer = new LocalServer(_messageProcessor);
            _httpServer.Start();
            _httpServer.LogMessageLocalEvent += LogServerMessage;
            _httpServer.OnOpenEvent += ServerOpenEvent;
//            _httpServer.OnErrorEvent += ServerErrorEvent;
            _httpServer.OnCloseEvent += ServerCloseEvent;
//            _httpServer.OnMessageEvent += ServerMessageEvent;
            _serverStatus = BusinessLogic.Helpers.StatusBrush(ConnectionStatus.Enabled);
            //_messageProcessor.Configuration = SystemConfig.Property.LocalPropertyId;
            _systemClock = DateTime.Now;
            _messageProcessor.UpdateSystemClock(_systemClock);
            _clockTimer = new Timer(clockCallback, null, 60000, 60000);
            _clientWebSocket.Configuration = SystemConfig.Property.PropertyId;
        }

        /// <summary>
        /// Toggle Remote mode hooks up or unhooks the events for the client websocket object.
        /// </summary>
        public void SetRemoteMode()
        {
            if (null == _clientWebSocket)
            {
                _clientWebSocket = new ClientSocket(_messageProcessor);
                _clientWebSocket.OnOpenEvent += ClientOpenEvent;
                _clientWebSocket.OnErrorEvent += ClientErrorEvent;
                _clientWebSocket.OnCloseEvent += ClientCloseEvent;
                _clientWebSocket.OnMessageEvent += ClientMessageEvent;
                _clientWebSocket.LogMessageEvent += LogClientMessage;
                _clientWebSocket.RegisterDeviceEvent += OnRegisterDevice;
                _clientWebSocket.AuthenticateErrorEvent += AuthenticateError;
                _messageProcessor.OnConfigurationChangeEvent += _clientWebSocket.ConfigurationUpdate;
                _messageProcessor.OnCloseRequestEvent += OnCloseRequest;
                if (Application.Current.Dispatcher.CheckAccess())
                {
                    HostStatus = BusinessLogic.Helpers.StatusBrush(_clientWebSocket.Status);
                }
            }
        }

        private void ClearRemoteMode()
        {
            if (null != _clientWebSocket)
            {
                _clientWebSocket.CloseConnection();
            }
            _clientWebSocket.OnOpenEvent -= ClientOpenEvent;
            _clientWebSocket.OnErrorEvent -= ClientErrorEvent;
            _clientWebSocket.OnCloseEvent -= ClientCloseEvent;
            _clientWebSocket.OnMessageEvent -= ClientMessageEvent;
            _clientWebSocket.LogMessageEvent -= LogClientMessage;
            _clientWebSocket.RegisterDeviceEvent -= OnRegisterDevice;
            _clientWebSocket.AuthenticateErrorEvent -= AuthenticateError;
            _messageProcessor.OnConfigurationChangeEvent -= _clientWebSocket.ConfigurationUpdate;
            _clientWebSocket.Dispose();
            _clientWebSocket = null;
            _messageProcessor.OnCloseRequestEvent -= OnCloseRequest;
            if (Application.Current.Dispatcher.CheckAccess())
            {
                HostStatus = BusinessLogic.Helpers.StatusBrush(ConnectionStatus.Disabled);
            }
        }

        /// <summary>
        /// Toggle browsability hooks up or unhooks the events for the autoDiscovery object.
        /// </summary>
        public void SetBrowsability()
        {
            if (null == _discoveryService)
            {
                _discoveryService = new Discovery();
                _discoveryService.OnRegisterEvent += BonjourRegisterEvent;
                _discoveryService.OnMessageEvent += BonjourMessageEvent;
                DiscoveryStatus = BusinessLogic.Helpers.StatusBrush(_discoveryService.Status);
                DiscoveryStatus = BusinessLogic.Helpers.StatusBrush(_discoveryService.Status);
            }
        }

        private void ClearBrowsability()
        {
            if (null != _discoveryService)
            {
                _discoveryService.OnRegisterEvent -= BonjourRegisterEvent;
                _discoveryService.OnMessageEvent -= BonjourMessageEvent;
                _discoveryService.Dispose();
                _discoveryService = null;
                DiscoveryStatus = BusinessLogic.Helpers.StatusBrush(ConnectionStatus.Disabled);
            }
        }

        #endregion

        #region Private Methods

        private void RefreshConfigurationData()
        {
            using (var data = new SimulatorContext())
            {
                RefreshConfiguration(data);
            }
        }

        private void RefreshConfiguration(SimulatorContext data)
        {
            var objects =
                data.ControlObjects.Where(
                    item =>
                        item.INSTALLATION == SystemConfig.Property.LocalPropertyId && item.Key == StaticData.ObjectType)
                    .ToList();

            foreach (var item in data.ObjectParameterTypes.GroupBy(x => x.OBJTYP))
            {
                var l = objects.Where(x => x.Value == item.Key).Select(x => x.OBJNAM).ToList();
                var o =
                    data.ControlObjects.Where(
                        x => x.INSTALLATION == SystemConfig.Property.LocalPropertyId && l.Contains(x.OBJNAM)).ToList();
                Tabs[item.Key].SwapData(o);
            }

        }

        private void ReplaceConfiguration(SimulatorContext data)
        {
            var objects =
                data.ControlObjects.Where(
                    item =>
                        item.INSTALLATION == SystemConfig.Property.LocalPropertyId && item.Key == StaticData.ObjectType)
                    .ToList();

            foreach (var item in data.ObjectParameterTypes.GroupBy(x => x.OBJTYP))
            {
                var l = objects.Where(x => x.Value == item.Key).Select(x => x.OBJNAM).ToList();
                var o =
                    data.ControlObjects.Where(
                        x => x.INSTALLATION == SystemConfig.Property.LocalPropertyId && l.Contains(x.OBJNAM)).ToList();
                Tabs[item.Key].RebuildData(o, SystemConfig.Property.LocalPropertyId);
            }
        }

        public void OpenAndSend(string message, string remoteHost)
        {
            // Force the socket closed.
            if (null != _clientWebSocket)
            {
                ClearRemoteMode();
            }

            SetRemoteMode();
            _clientWebSocket.OpenConnection(message, remoteHost);
        }

        private void OpenHost()
        {
            if (null != SystemConfig.Property)
            {
                main.RegistrationView.PropertyName = SystemConfig.Property.Name;
                main.RegistrationView.PropertyId = SystemConfig.Property.PropertyId.ToString();
                main.RegistrationView.Secret = SystemConfig.Property.Secret;
                _clientWebSocket.OpenConnection(SystemConfig.Property);
            }
            HostStatus = BusinessLogic.Helpers.StatusBrush(_clientWebSocket.Status);
        }

        #endregion

        #region Event Handlers

        private void RefreshData(object sender, EventArgs e)
        {
            Application.Current.Dispatcher.Invoke(RefreshConfigurationData);
        }

        private void OnPropertyChanged(String info)
        {
            if (null != PropertyChanged)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

        // Bonjour Registration event handler
        private void BonjourRegisterEvent(Object sender, DiscoveryEventArgs e)
        {
            Application.Current.Dispatcher.Invoke(() => DiscoveryEnabled = e.Status != ConnectionStatus.Disabled);
            Application.Current.Dispatcher.Invoke(() => DiscoveryStatus = BusinessLogic.Helpers.StatusBrush(e.Status));
            if (!String.IsNullOrEmpty(e.Message))
            {
                Application.Current.Dispatcher.Invoke(() => BrowserLogModel.Add(e.Message, true));
            }
        }

        // Bonjour message event handler
        private void BonjourMessageEvent(object sender, DiscoveryEventArgs e)
        {
            Application.Current.Dispatcher.Invoke(() => DiscoveryEnabled = e.Status != ConnectionStatus.Disabled);
            Application.Current.Dispatcher.Invoke(() => DiscoveryStatus = BusinessLogic.Helpers.StatusBrush(e.Status));
            if (!String.IsNullOrEmpty(e.Message))
            {
                Application.Current.Dispatcher.Invoke(() => BrowserLogModel.Add(e.Message, true));
            }
        }

        private void OnRegisterDevice(RegistrationResponse message)
        {
            int propertyId;
            if (!int.TryParse(message.PropertyID, out propertyId))
            {
                propertyId = -1;
            }

            using (var data = new SimulatorContext())
            {
                var property =
                    data.Properties.FirstOrDefault(x => x.PropertyId == RegisteringProperty.PropertyId && x.RemoteHost == RegisteringProperty.RemoteHost);

                if (null == property)
                {
                    property = RegisteringProperty;
                    property.PropertyId = propertyId;
                    property.Secret = message.SharedSecret;
                    data.Properties.Add(property);
                    // Save to get the localPropertyId assignment.
                    data.SaveChanges();
                    AddConfiguration(data, property);
                }
                else
                {
                    property.PropertyId = propertyId;
                    property.Secret = message.SharedSecret;
                }
                data.SaveChanges();
                SystemConfig.Property = property;
            }
            main.RegistrationView.PropertyId = propertyId.ToString();
            main.RegistrationView.Secret = message.SharedSecret;

            Application.Current.Dispatcher.Invoke(SetSelectedProperty);
        }

        private void SetSelectedProperty()
        {
            var value = string.Format("{0} - {1}",
                SystemConfig.Property.Name,
                Globals.Hosts.FirstOrDefault(x => x.Value == SystemConfig.Property.RemoteHost).Key);
            _properties.Add(value);
            SelectedProperty = value;
            if (null != OnPropertyChange)
            {
                OnPropertyChange(main.RegistrationView.PropertyName);
            }
            if (null == _discoveryService ||
                null == _httpServer ||
                null == _clientWebSocket)
            {
                CreateEndpoints();
            } 
            _discoveryService.ChangeHost(SystemConfig.Property);
            _httpServer.ChangeHost(SystemConfig.Property);
            _clientWebSocket.ChangeHost(SystemConfig.Property);

            // Finally, refresh the data view
            using (var db = new SimulatorContext())
            {
                ReplaceConfiguration(db);
            }
        }

        private void OnCloseRequest(Object sender)
        {
            ClearRemoteMode();
        }

        private void AuthenticateError(Object sender, ClientSocketEventArgs e)
        {
            using (var data = new SimulatorContext())
            {
                var p = data.Properties.FirstOrDefault(x => x.LocalPropertyId == SystemConfig.Property.LocalPropertyId);
                //if (null != p)
                //{
                //    p.Secret = "";
                //    data.SaveChanges();
                //}
                SystemConfig.Property = p;
            }
            Application.Current.Dispatcher.Invoke(OpenHost);
        }

        private void ServerOpenEvent(Object sender, ClientSocketEventArgs e)
        {
            Application.Current.Dispatcher.Invoke(() => ServerEnabled = e.Status != ConnectionStatus.Disabled);
            Application.Current.Dispatcher.Invoke(() => ServerStatus = BusinessLogic.Helpers.StatusBrush(e.Status));
            if (!String.IsNullOrEmpty(e.Message))
            {
                Application.Current.Dispatcher.Invoke(() => ServerLogModel.Add(e.Message, true));
            }
        }

        private void ClientOpenEvent(Object sender, ClientSocketEventArgs e)
        {
            Application.Current.Dispatcher.Invoke(() => HostEnabled = e.Status != ConnectionStatus.Disabled);
            Application.Current.Dispatcher.Invoke(() => HostStatus = BusinessLogic.Helpers.StatusBrush(e.Status));
            if (!String.IsNullOrEmpty(e.Message))
            {
                Application.Current.Dispatcher.Invoke(() => RemoteLogModel.Add(e.Message, true));
            }
        }

        private void clockCallback(object sender)
        {
            _systemClock = _systemClock.AddMinutes(1.0);
            _messageProcessor.UpdateSystemClock(DateTime.MinValue);
            Application.Current.Dispatcher.Invoke(UpdateSystemData);
        }

        private void UpdateSystemData()
        {
            if (null != SystemConfig)
            {
                Configuration = SystemConfig.Property.LocalPropertyId;
                RefreshConfigurationData();
            }
        }

        private void SetConfigurations()
        {
            using (var data = new SimulatorContext())
            {
                foreach (var p in data.Properties)
                {
                    _properties.Add(string.Format("{0} - {1}",
                        p.Name, 
                        Globals.Hosts.FirstOrDefault(x => x.Value == p.RemoteHost).Key));
                }
            }
        }

        private void ServerErrorEvent(Object sender, ClientSocketEventArgs e)
        {
            Application.Current.Dispatcher.Invoke(() => ServerEnabled = e.Status != ConnectionStatus.Disabled);
            Application.Current.Dispatcher.Invoke(() => ServerStatus = BusinessLogic.Helpers.StatusBrush(e.Status));
            if (!String.IsNullOrEmpty(e.Message))
            {
                Application.Current.Dispatcher.Invoke(() => ServerLogModel.Add(e.Message, true));
            }
        }

        private void ClientErrorEvent(Object sender, ClientSocketEventArgs e)
        {
            Application.Current.Dispatcher.Invoke(() => HostEnabled = e.Status != ConnectionStatus.Disabled);
            Application.Current.Dispatcher.Invoke(() => HostStatus = BusinessLogic.Helpers.StatusBrush(e.Status));
            if (!String.IsNullOrEmpty(e.Message))
            {
                Application.Current.Dispatcher.Invoke(() => RemoteLogModel.Add(e.Message, true));
            }
        }

        private void ClientCloseEvent(Object sender, ClientSocketEventArgs e)
        {
            Application.Current.Dispatcher.Invoke(() => HostEnabled = e.Status != ConnectionStatus.Disabled);
            Application.Current.Dispatcher.Invoke(() => HostStatus = BusinessLogic.Helpers.StatusBrush(e.Status));
            if (!String.IsNullOrEmpty(e.Message))
            {
                Application.Current.Dispatcher.Invoke(() => RemoteLogModel.Add(e.Message, true));
            }
            // Try reopening.
            Application.Current.Dispatcher.Invoke(OpenHost);
        }

        private void ServerCloseEvent(Object sender, ClientSocketEventArgs e)
        {
            Application.Current.Dispatcher.Invoke(() => ServerEnabled = e.Status != ConnectionStatus.Disabled);
            Application.Current.Dispatcher.Invoke(() => ServerStatus = BusinessLogic.Helpers.StatusBrush(e.Status));
            if (!String.IsNullOrEmpty(e.Message))
            {
                Application.Current.Dispatcher.Invoke(() => ServerLogModel.Add(e.Message, true));
            }
        }

        private void ClientMessageEvent(Object sender, ClientSocketEventArgs e)
        {
            Application.Current.Dispatcher.Invoke(() => HostEnabled = e.Status != ConnectionStatus.Disabled);
            Application.Current.Dispatcher.Invoke(() => HostStatus = BusinessLogic.Helpers.StatusBrush(e.Status));
            if (!String.IsNullOrEmpty(e.Message))
            {
                Application.Current.Dispatcher.Invoke(() => RemoteLogModel.Add(e.Message, true));
            }
        }

        private void ServerMessageEvent(Object sender, ClientSocketEventArgs e)
        {
            Application.Current.Dispatcher.Invoke(() => ServerEnabled = e.Status != ConnectionStatus.Disabled);
            Application.Current.Dispatcher.Invoke(() => ServerStatus = BusinessLogic.Helpers.StatusBrush(e.Status));
            if (!String.IsNullOrEmpty(e.Message))
            {
                Application.Current.Dispatcher.Invoke(() => ServerLogModel.Add(e.Message, true));
            }
        }

        private void LogClientMessage(string message, bool Request)
        {
            if (null != RemoteLogModel)
            {
                Application.Current.Dispatcher.Invoke(() => RemoteLogModel.Add(message, Request));
            }
        }

        private void LogServerMessage(string message, bool Request)
        {
            Application.Current.Dispatcher.Invoke(() => ServerLogModel.Add(message, Request));
        }

        #endregion

        #region Seed Data
        private void SeedData(SimulatorContext data)
        {
//            data.Database.ExecuteSqlCommand("TRUNCATE TABLE [dbo].[ControlObjects]");
            AddPumpParameters(data);
            AddPumpAffectsParameters(data);
            AddScheduleParameters(data);
            AddHeaterParameters(data);
            AddHeaterComboParameters(data);
            AddValveParameters(data);
            AddValveAffectsParameters(data);
            AddRemoteControlParameters(data);
            AddRemoteButtonParameters(data);
            AddUserClockParameters(data);
            AddCircuitParameters(data);
            AddCircuitEffectParameters(data);
            AddBodyParameters(data);
            AddChemistryParameters(data);
            AddSupportParameters(data);
            AddEmailAlertParameters(data);
            AddPermitParameters(data);
            AddStatusMessageMasterParameters(data);
            AddStatusMessageParameters(data);
            AddSensorParameters(data);
            AddSensorSingletonParameters(data);
            AddMenuParameters(data);
            AddServiceParameters(data);
            AddPanelParameters(data);
            AddModuleParameters(data);
//            AddRelayParameters(data);
            AddSystemParameters(data);
            AddBoardParameters(data);
            AddNetworkParameters(data);
            AddUserParameters(data);
        }

        private void AddPumpParameters(SimulatorContext data)
        {
            try
            {
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PumpType,
                    PARAM = StaticData.ObjectName,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PumpType,
                    PARAM = StaticData.ObjectType,
                    DEFAULT = StaticData.PumpType,
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PumpType,
                    PARAM = StaticData.Static,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PumpType,
                    PARAM = StaticData.ObjectVersion,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PumpType,
                    PARAM = StaticData.ListOrder,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PumpType,
                    PARAM = StaticData.SubType,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PumpType,
                    PARAM = StaticData.HardName,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PumpType,
                    PARAM = StaticData.SystemName,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PumpType,
                    PARAM = StaticData.RelayType,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PumpType,
                    PARAM = StaticData.Parent,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PumpType,
                    PARAM = StaticData.BodyType,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PumpType,
                    PARAM = StaticData.Share,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PumpType,
                    PARAM = StaticData.Filter,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PumpType,
                    PARAM = StaticData.MenuParam,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PumpType,
                    PARAM = "COMUART",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PumpType,
                    PARAM = "PRIOR",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PumpType,
                    PARAM = "MAX",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PumpType,
                    PARAM = "MIN",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PumpType,
                    PARAM = "ABSMAX",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PumpType,
                    PARAM = "ABSMIN",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PumpType,
                    PARAM = "PRIMFLO",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PumpType,
                    PARAM = "PRIMTIM",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PumpType,
                    PARAM = "SYSTIM",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PumpType,
                    PARAM = "PASSWRD",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PumpType,
                    PARAM = "ENABLE",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PumpType,
                    PARAM = "SENSE",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PumpType,
                    PARAM = "BASE",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PumpType,
                    PARAM = "BAKFLO",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PumpType,
                    PARAM = "BAKTIM",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PumpType,
                    PARAM = "RINSTIM",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PumpType,
                    PARAM = "VACFLO",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PumpType,
                    PARAM = "VACTIM",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PumpType,
                    PARAM = StaticData.CircuitType,
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PumpType,
                    PARAM = "RPM",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PumpType,
                    PARAM = "GPM",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PumpType,
                    PARAM = "PWR",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PumpType,
                    PARAM = "COUNT",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PumpType,
                    PARAM = StaticData.Status,
                    DEFAULT = "OFF",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PumpType,
                    PARAM = "ALARM",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PumpType,
                    PARAM = "VOLT",
                    DEFAULT = "OFF",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PumpType,
                    PARAM = "CURRENT",
                    DEFAULT = "OFF",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PumpType,
                    PARAM = "FAULT",
                    DEFAULT = "OFF",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PumpType,
                    PARAM = "SYSTEM",
                    DEFAULT = "OFF",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PumpType,
                    PARAM = "PRIME",
                    DEFAULT = "OFF",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PumpType,
                    PARAM = "DRVALM",
                    DEFAULT = "OFF",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PumpType,
                    PARAM = "DRVWRN",
                    DEFAULT = "OFF",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PumpType,
                    PARAM = "COMERR",
                    DEFAULT = "OFF",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PumpType,
                    PARAM = StaticData.StatusMessageMask,
                    DEFAULT = "",
                    Saved = true
                });
                data.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error creating pump parameter types {0}", ex.Message));
            }
        }

        private void AddPumpAffectsParameters(SimulatorContext data)
        {
            try
            {
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PumpAffectsType,
                    PARAM = StaticData.ObjectName,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PumpAffectsType,
                    PARAM = StaticData.ObjectType,
                    DEFAULT = StaticData.PumpAffectsType,
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PumpAffectsType,
                    PARAM = StaticData.Parent,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PumpAffectsType,
                    PARAM = StaticData.Static,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PumpAffectsType,
                    PARAM = StaticData.ObjectVersion,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PumpAffectsType,
                    PARAM = StaticData.ListOrder,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PumpAffectsType,
                    PARAM = StaticData.CircuitType,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PumpAffectsType,
                    PARAM = "SPEED",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PumpAffectsType,
                    PARAM = "BOOST",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PumpAffectsType,
                    PARAM = "SELECT",
                    DEFAULT = "",
                    Saved = true
                });
                data.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error creating pump affect parameter types {0}", ex.Message));
            }
        }

        private void AddScheduleParameters(SimulatorContext data)
        {
            try
            {
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ScheduleType,
                    PARAM = StaticData.ObjectName,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ScheduleType,
                    PARAM = StaticData.ObjectType,
                    DEFAULT = StaticData.ScheduleType,
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ScheduleType,
                    PARAM = StaticData.ObjectVersion,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ScheduleType,
                    PARAM = StaticData.ListOrder,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ScheduleType,
                    PARAM = StaticData.CircuitType,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ScheduleType,
                    PARAM = StaticData.HardName,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ScheduleType,
                    PARAM = StaticData.SystemName,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ScheduleType,
                    PARAM = StaticData.DateParam,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ScheduleType,
                    PARAM = "SINGLE",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ScheduleType,
                    PARAM = "START",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ScheduleType,
                    PARAM = StaticData.ClockParam,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ScheduleType,
                    PARAM = "STOP",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ScheduleType,
                    PARAM = "TIMOUT",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ScheduleType,
                    PARAM = "GROUP",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ScheduleType,
                    PARAM = StaticData.Status,
                    DEFAULT = "OFF",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ScheduleType,
                    PARAM = "CLK24A",
                    DEFAULT = "HR24",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ScheduleType,
                    PARAM = StaticData.HeaterType,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ScheduleType,
                    PARAM = StaticData.HiTemp,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ScheduleType,
                    PARAM = StaticData.LowTemp,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ScheduleType,
                    PARAM = "VOL",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ScheduleType,
                    PARAM = "SMTSRT",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ScheduleType,
                    PARAM = "VACFLO",
                    DEFAULT = "",
                    Saved = true
                }); data.SaveChanges();
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ScheduleType,
                    PARAM = "DNTSTP",
                    DEFAULT = "",
                    Saved = true
                }); data.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error creating schedule parameter types {0}", ex.Message));
            }
        }

        private void AddHeaterParameters(SimulatorContext data)
        {
            try
            {
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.HeaterType,
                    PARAM = StaticData.ObjectName,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.HeaterType,
                    PARAM = StaticData.ObjectType,
                    DEFAULT = StaticData.HeaterType,
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.HeaterType,
                    PARAM = StaticData.ObjectVersion,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.HeaterType,
                    PARAM = StaticData.Static,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.HeaterType,
                    PARAM = StaticData.ListOrder,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.HeaterType,
                    PARAM = StaticData.Parent,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.HeaterType,
                    PARAM = StaticData.BodyType,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.HeaterType,
                    PARAM = StaticData.Share,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.HeaterType,
                    PARAM = StaticData.SubType,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.HeaterType,
                    PARAM = StaticData.SystemName,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.HeaterType,
                    PARAM = StaticData.HardName,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.HeaterType,
                    PARAM = StaticData.RelayType,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.HeaterType,
                    PARAM = "COMUART",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.HeaterType,
                    PARAM = "START",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.HeaterType,
                    PARAM = "STOP",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.HeaterType,
                    PARAM = StaticData.Delay,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.HeaterType,
                    PARAM = StaticData.Status,
                    DEFAULT = "OFF",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.HeaterType,
                    PARAM = "PERMIT",
                    DEFAULT = "OFF",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.HeaterType,
                    PARAM = "TIMOUT",
                    DEFAULT = "0",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.HeaterType,
                    PARAM = StaticData.Action,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.HeaterType,
                    PARAM = "HTMODE",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.HeaterType,
                    PARAM = StaticData.ShowMenu,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.HeaterType,
                    PARAM = StaticData.StatusMessageMask,
                    DEFAULT = "",
                    Saved = true
                });
                data.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error creating heater parameter types {0}", ex.Message));
            }
        }

        private void AddHeaterComboParameters(SimulatorContext data)
        {
            try
            {
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.HeatComboType,
                    PARAM = StaticData.ObjectName,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.HeatComboType,
                    PARAM = StaticData.ObjectType,
                    DEFAULT = StaticData.HeatComboType,
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.HeatComboType,
                    PARAM = StaticData.ObjectVersion,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.HeatComboType,
                    PARAM = StaticData.ListOrder,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.HeatComboType,
                    PARAM = StaticData.Parent,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.HeatComboType,
                    PARAM = StaticData.Use,
                    DEFAULT = "",
                    Saved = true
                });
                data.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error creating heat combo parameter types {0}", ex.Message));
            }
        }

        private void AddValveParameters(SimulatorContext data)
        {
            try
            {
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ValveType,
                    PARAM = StaticData.ObjectName,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ValveType,
                    PARAM = StaticData.ObjectType,
                    DEFAULT = StaticData.ValveType,
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ValveType,
                    PARAM = StaticData.ObjectVersion,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ValveType,
                    PARAM = StaticData.Static,
                    DEFAULT = "ON",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ValveType,
                    PARAM = StaticData.ListOrder,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ValveType,
                    PARAM = StaticData.Parent,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ValveType,
                    PARAM = StaticData.RelayType,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ValveType,
                    PARAM = "ASSIGN",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ValveType,
                    PARAM = "USE",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ValveType,
                    PARAM = StaticData.SubType,
                    DEFAULT = "LEGACY",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ValveType,
                    PARAM = StaticData.SystemName,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ValveType,
                    PARAM = StaticData.HardName,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ValveType,
                    PARAM = "COMUART",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ValveType,
                    PARAM = StaticData.Delay,
                    DEFAULT = "OFF",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ValveType,
                    PARAM = "DFLT",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ValveType,
                    PARAM = "TIMOUT",
                    DEFAULT = "0",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ValveType,
                    PARAM = "TIME",
                    DEFAULT = "0",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ValveType,
                    PARAM = StaticData.Status,
                    DEFAULT = "OFF",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ValveType,
                    PARAM = StaticData.CircuitType,
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ValveType,
                    PARAM = "POSIT",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ValveType,
                    PARAM = "COUNT",
                    DEFAULT = "",
                    Saved = false
                });
                data.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error creating valve parameter types {0}", ex.Message));
            }
        }

        private void AddValveAffectsParameters(SimulatorContext data)
        {
            try
            {
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ValveAffectsType,
                    PARAM = StaticData.ObjectName,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ValveAffectsType,
                    PARAM = StaticData.ObjectType,
                    DEFAULT = StaticData.ValveAffectsType,
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ValveAffectsType,
                    PARAM = StaticData.ObjectVersion,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ValveAffectsType,
                    PARAM = StaticData.Static,
                    DEFAULT = "OFF",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ValveAffectsType,
                    PARAM = "PRIOR",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ValveAffectsType,
                    PARAM = StaticData.Parent,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ValveAffectsType,
                    PARAM = StaticData.CircuitType,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ValveAffectsType,
                    PARAM = "POSIT",
                    DEFAULT = "",
                    Saved = true
                });
                data.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error creating valve affect parameter types {0}", ex.Message));
            }
        }

        private void AddRemoteControlParameters(SimulatorContext data)
        {
            try
            {
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.RemoteType,
                    PARAM = StaticData.ObjectName,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.RemoteType,
                    PARAM = StaticData.ObjectType,
                    DEFAULT = StaticData.RemoteType,
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.RemoteType,
                    PARAM = StaticData.ObjectVersion,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.RemoteType,
                    PARAM = StaticData.Static,
                    DEFAULT = "ON",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.RemoteType,
                    PARAM = StaticData.ListOrder,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.RemoteType,
                    PARAM = StaticData.SubType,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.RemoteType,
                    PARAM = StaticData.SystemName,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.RemoteType,
                    PARAM = StaticData.HardName,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.RemoteType,
                    PARAM = "COMUART",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.RemoteType,
                    PARAM = "MODULE",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.RemoteType,
                    PARAM = StaticData.BodyType,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.RemoteType,
                    PARAM = "PUMP",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.RemoteType,
                    PARAM = "ENABLE",
                    DEFAULT = "OFF",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.RemoteType,
                    PARAM = "ACT",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.RemoteType,
                    PARAM = "MODE",
                    DEFAULT = "OFF",
                    Saved = false
                });
                data.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error creating remote parameter types {0}", ex.Message));
            }
        }

        private void AddRemoteButtonParameters(SimulatorContext data)
        {
            try
            {
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.RemoteButtonType,
                    PARAM = StaticData.ObjectName,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.RemoteButtonType,
                    PARAM = StaticData.ObjectType,
                    DEFAULT = StaticData.RemoteButtonType,
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.RemoteButtonType,
                    PARAM = StaticData.ObjectVersion,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.RemoteButtonType,
                    PARAM = StaticData.Static,
                    DEFAULT = "ON",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.RemoteButtonType,
                    PARAM = StaticData.ListOrder,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.RemoteButtonType,
                    PARAM = StaticData.Parent,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.RemoteButtonType,
                    PARAM = StaticData.CircuitType,
                    DEFAULT = "",
                    Saved = true
                });
                data.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error creating remote button parameter types {0}", ex.Message));
            }
        }

        private void AddUserClockParameters(SimulatorContext data)
        {
            try
            {
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SystemTimeType,
                    PARAM = StaticData.ObjectName,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SystemTimeType,
                    PARAM = StaticData.ObjectType,
                    DEFAULT = StaticData.SystemTimeType,
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SystemTimeType,
                    PARAM = StaticData.ObjectVersion,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SystemTimeType,
                    PARAM = StaticData.Static,
                    DEFAULT = "ON",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SystemTimeType,
                    PARAM = "ACCEL",
                    DEFAULT = "OFF",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SystemTimeType,
                    PARAM = "ZIP",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SystemTimeType,
                    PARAM = "LOCX",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SystemTimeType,
                    PARAM = "LOCY",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SystemTimeType,
                    PARAM = StaticData.SystemName,
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SystemTimeType,
                    PARAM = "DLSTIM",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SystemTimeType,
                    PARAM = "TIMZON",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SystemTimeType,
                    PARAM = "OFFSET",
                    DEFAULT = "0",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SystemTimeType,
                    PARAM = "CLK24A",
                    DEFAULT = "HR24",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SystemTimeType,
                    PARAM = "TIME",
                    DEFAULT = "0,0,0",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SystemTimeType,
                    PARAM = "MIN",
                    DEFAULT = "0,0,0",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SystemTimeType,
                    PARAM = "DAY",
                    DEFAULT = "1,1,1",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SystemTimeType,
                    PARAM = "SRIS",
                    DEFAULT = "0,0,0",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SystemTimeType,
                    PARAM = "SSET",
                    DEFAULT = "0,0,0",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SystemTimeType,
                    PARAM = StaticData.Source,
                    DEFAULT = "LOCAL",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SystemTimeType,
                    PARAM = "ALPHA",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SystemTimeType,
                    PARAM = "ABSTIM",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SystemTimeType,
                    PARAM = "CALIB",
                    DEFAULT = "",
                    Saved = true
                }); data.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error creating time parameter types {0}", ex.Message));
            }
        }

        private void AddCircuitParameters(SimulatorContext data)
        {
            try
            {
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.CircuitType,
                    PARAM = StaticData.ObjectName,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.CircuitType,
                    PARAM = StaticData.ObjectType,
                    DEFAULT = StaticData.CircuitType,
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.CircuitType,
                    PARAM = StaticData.Static,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.CircuitType,
                    PARAM = StaticData.ObjectVersion,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.CircuitType,
                    PARAM = StaticData.SubType,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.CircuitType,
                    PARAM = StaticData.HardName,
                     DEFAULT = "",
                   Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.CircuitType,
                    PARAM = StaticData.SystemName,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.CircuitType,
                    PARAM = StaticData.Parent,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.CircuitType,
                    PARAM = StaticData.BodyType,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.CircuitType,
                    PARAM = "RECENT",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.CircuitType,
                    PARAM = "FREQ",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.CircuitType,
                    PARAM = "ALPHA",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.CircuitType,
                    PARAM = "CUSTOM",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.CircuitType,
                    PARAM = StaticData.ListOrder,
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.CircuitType,
                    PARAM = StaticData.ShowMenu,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.CircuitType,
                    PARAM = "FREEZE",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.CircuitType,
                    PARAM = "CYCTIM",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.CircuitType,
                    PARAM = "TIMZON",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.CircuitType,
                    PARAM = "MANUAL",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.CircuitType,
                    PARAM = "TIME",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.CircuitType,
                    PARAM = "FEATR",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.CircuitType,
                    PARAM = "RLY",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.CircuitType,
                    PARAM = "SERVICE",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.CircuitType,
                    PARAM = "ACT",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.CircuitType,
                    PARAM = "USE",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.CircuitType,
                    PARAM = "USAGE",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.CircuitType,
                    PARAM = "PROGRES",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.CircuitType,
                    PARAM = "SINDEX",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.CircuitType,
                    PARAM = "INVALID",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.CircuitType,
                    PARAM = "SELECT",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.CircuitType,
                    PARAM = "CHILD",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.CircuitType,
                    PARAM = "COUNT",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.CircuitType,
                    PARAM = StaticData.Status,
                    DEFAULT = "OFF",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.CircuitType,
                    PARAM = "SOURCE",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.CircuitType,
                    PARAM = "QUEUE",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.CircuitType,
                    PARAM = "DLY",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.CircuitType,
                    PARAM = "DLYCNCL",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.CircuitType,
                    PARAM = "TIMOUT",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.CircuitType,
                    PARAM = "PARTY",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.CircuitType,
                    PARAM = "SUPORT",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.CircuitType,
                    PARAM = StaticData.StatusMessageMask,
                    DEFAULT = "",
                    Saved = true
                }); data.SaveChanges();
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.CircuitType,
                    PARAM = "DNTSTP",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.CircuitType,
                    PARAM = "LIMIT",
                    DEFAULT = "",
                    Saved = false
                }); 
                data.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error creating circuit parameter types {0}", ex.Message));
            }
        }

        private void AddCircuitEffectParameters(SimulatorContext data)
        {
            try
            {
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.CircuitGroupType,
                    PARAM = StaticData.ObjectName,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.CircuitGroupType,
                    PARAM = StaticData.ObjectType,
                    DEFAULT = StaticData.CircuitGroupType,
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.CircuitGroupType,
                    PARAM = StaticData.ObjectVersion,
                    DEFAULT = "",
                    Saved = true
                });               
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.CircuitGroupType,
                    PARAM = StaticData.Parent,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.CircuitGroupType,
                    PARAM = StaticData.CircuitType,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.CircuitGroupType,
                    PARAM = "ACT",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.CircuitGroupType,
                    PARAM = "DLY",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.CircuitGroupType,
                    PARAM = StaticData.ListOrder,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.CircuitGroupType,
                    PARAM = "COLOR",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.CircuitGroupType,
                    PARAM = "START",
                    DEFAULT = "",
                    Saved = true
                });
                data.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error creating circuit group parameter types {0}", ex.Message));
            }
        }

        private void AddBodyParameters(SimulatorContext data)
        {
            try
            {
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.BodyType,
                    PARAM = StaticData.ObjectName,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.BodyType,
                    PARAM = StaticData.ObjectType,
                    DEFAULT = StaticData.BodyType,
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.BodyType,
                    PARAM = StaticData.ObjectVersion,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.BodyType,
                    PARAM = StaticData.Static,
                    DEFAULT = "ON",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.BodyType,
                    PARAM = StaticData.SubType,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.BodyType,
                    PARAM = StaticData.Parent,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.BodyType,
                    PARAM = StaticData.Share,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.BodyType,
                    PARAM = StaticData.ListOrder,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.BodyType,
                    PARAM = StaticData.Filter,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.BodyType,
                    PARAM = "PRIMFLO",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.BodyType,
                    PARAM = "PUMP",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.BodyType,
                    PARAM = StaticData.HardName,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.BodyType,
                    PARAM = StaticData.SystemName,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.BodyType,
                    PARAM = "VOL",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.BodyType,
                    PARAM = "PRIM",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.BodyType,
                    PARAM = "SEC",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.BodyType,
                    PARAM = "ACT1",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.BodyType,
                    PARAM = "ACT2",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.BodyType,
                    PARAM = "ACT3",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.BodyType,
                    PARAM = "ACT4",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.BodyType,
                    PARAM = "BADGE",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.BodyType,
                    PARAM = "CHIPSEL",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.BodyType,
                    PARAM = StaticData.Status,
                    DEFAULT = "OFF",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.BodyType,
                    PARAM = "TEMP",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.BodyType,
                    PARAM = "MANHT",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.BodyType,
                    PARAM = StaticData.HeaterType,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.BodyType,
                    PARAM = StaticData.HiTemp,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.BodyType,
                    PARAM = StaticData.LowTemp,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.BodyType,
                    PARAM = "HTMODE",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.BodyType,
                    PARAM = "SRCTYP",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.BodyType,
                    PARAM = StaticData.HeatSource,
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.BodyType,
                    PARAM = "SETTMP",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.BodyType,
                    PARAM = "LSTTMP",
                    DEFAULT = "",
                    Saved = true
                });
                data.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error creating body parameter types {0}", ex.Message));
            }
        }

        private void AddChemistryParameters(SimulatorContext data)
        {
            try
            {
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ChemistryType,
                    PARAM = StaticData.ObjectName,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ChemistryType,
                    PARAM = StaticData.ObjectVersion,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ChemistryType,
                    PARAM = StaticData.ObjectType,
                    DEFAULT = StaticData.ChemistryType,
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ChemistryType,
                    PARAM = StaticData.SubType,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ChemistryType,
                    PARAM = StaticData.ListOrder,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ChemistryType,
                    PARAM = "COMUART",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ChemistryType,
                    PARAM = "COMSPI",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ChemistryType,
                    PARAM = "ASSIGN",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ChemistryType,
                    PARAM = StaticData.BodyType,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ChemistryType,
                    PARAM = StaticData.Share,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ChemistryType,
                    PARAM = "DEVCONN",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ChemistryType,
                    PARAM = "PRIM",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ChemistryType,
                    PARAM = "SEC",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ChemistryType,
                    PARAM = "TIMOUT",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ChemistryType,
                    PARAM = "SUPER",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ChemistryType,
                    PARAM = "ABSMAX",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ChemistryType,
                    PARAM = "SALT",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ChemistryType,
                    PARAM = "DEVICE",
                    DEFAULT = "OFF",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ChemistryType,
                    PARAM = "SALTLO",
                    DEFAULT = "OFF",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ChemistryType,
                    PARAM = "VERYLO",
                    DEFAULT = "OFF",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ChemistryType,
                    PARAM = "CURRENT",
                    DEFAULT = "OFF",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ChemistryType,
                    PARAM = "VOLT",
                    DEFAULT = "OFF",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ChemistryType,
                    PARAM = "COMERR",
                    DEFAULT = "OFF",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ChemistryType,
                    PARAM = StaticData.StatusMessageMask,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ChemistryType,
                    PARAM = "PHSET",
                    DEFAULT = "7.40",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ChemistryType,
                    PARAM = "ORPSET",
                    DEFAULT = "725",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ChemistryType,
                    PARAM = "PHTNK",
                    DEFAULT = "3",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ChemistryType,
                    PARAM = "ORPTNK",
                    DEFAULT = "4",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ChemistryType,
                    PARAM = "CALC",
                    DEFAULT = "350",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ChemistryType,
                    PARAM = "CYACID",
                    DEFAULT = "110",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ChemistryType,
                    PARAM = "ALK",
                    DEFAULT = "460",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ChemistryType,
                    PARAM = "SINDEX",
                    DEFAULT = "0",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ChemistryType,
                    PARAM = "PHFED",
                    DEFAULT = "2",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ChemistryType,
                    PARAM = "ORPFED",
                    DEFAULT = "2",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ChemistryType,
                    PARAM = "PHVAL",
                    DEFAULT = "6.5",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ChemistryType,
                    PARAM = "ORPVAL",
                    DEFAULT = "655",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ChemistryType,
                    PARAM = "PHTIM",
                    DEFAULT = "20",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ChemistryType,
                    PARAM = "ORPTIM",
                    DEFAULT = "20",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ChemistryType,
                    PARAM = "PHVOL",
                    DEFAULT = "34200",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ChemistryType,
                    PARAM = "ORPVOL",
                    DEFAULT = "33450",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ChemistryType,
                    PARAM = "TEMP",
                    DEFAULT = "OFF",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ChemistryType,
                    PARAM = "NOFLO",
                    DEFAULT = "OFF",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ChemistryType,
                    PARAM = "PHHI",
                    DEFAULT = "OFF",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ChemistryType,
                    PARAM = "PHLO",
                    DEFAULT = "OFF",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ChemistryType,
                    PARAM = "ORPHI",
                    DEFAULT = "OFF",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    PARAM = "ORPLO",
                    OBJTYP = StaticData.ChemistryType,
                    DEFAULT = "OFF",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ChemistryType,
                    PARAM = "PHCHK",
                    DEFAULT = "OFF",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ChemistryType,
                    PARAM = "ORPCHK",
                    DEFAULT = "OFF",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ChemistryType,
                    PARAM = "PROBE",
                    DEFAULT = "OFF",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ChemistryType,
                    PARAM = "LOCKOUT",
                    DEFAULT = "OFF",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ChemistryType,
                    PARAM = "PHLIM",
                    DEFAULT = "OFF",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ChemistryType,
                    PARAM = "ORPLIM",
                    DEFAULT = "OFF",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ChemistryType,
                    PARAM = "INVALID",
                    DEFAULT = "OFF",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ChemistryType,
                    PARAM = "COMMLNK",
                    DEFAULT = "OFF",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ChemistryType,
                    PARAM = "CALIB",
                    DEFAULT = "OFF",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ChemistryType,
                    PARAM = "ALARM",
                    DEFAULT = "OFF",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ChemistryType,
                    PARAM = "PHTYP",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ChemistryType,
                    PARAM = "ORPTYP",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ChemistryType,
                    PARAM = "PHMOD",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ChemistryType,
                    PARAM = "ORPMOD",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ChemistryType,
                    PARAM = "FLOWDLY",
                    DEFAULT = "OFF",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ChemistryType,
                    PARAM = "PHPRIOR",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ChemistryType,
                    PARAM = "MODE",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ChemistryType,
                    PARAM = "CHLOR",
                    DEFAULT = "OFF",
                    Saved = false
                });
                data.SaveChanges();
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ChemistryType,
                    PARAM = "QUALITY",
                    DEFAULT = "Ideal",
                    Saved = false
                });
                data.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error creating chemisty parameter types {0}", ex.Message));
            }
        }

        private void AddSupportParameters(SimulatorContext data)
        {
            try
            {
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SupportType,
                    PARAM = StaticData.ObjectName,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SupportType,
                    PARAM = StaticData.ObjectVersion,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SupportType,
                    PARAM = StaticData.ObjectType,
                    DEFAULT = StaticData.SupportType,
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SupportType,
                    PARAM = StaticData.ListOrder,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SupportType,
                    PARAM = StaticData.SystemName,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SupportType,
                    PARAM = "PHONE",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SupportType,
                    PARAM = "EMAIL",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SupportType,
                    PARAM = "URL",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SupportType,
                    PARAM = "LOGO",
                    DEFAULT = "",
                    Saved = true
                });
                data.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error creating support parameter types {0}", ex.Message));
            }
        }

        private void AddEmailAlertParameters(SimulatorContext data)
        {
            try
            {
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.EmailAlertType,
                    PARAM = StaticData.ObjectName,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.EmailAlertType,
                    PARAM = StaticData.ObjectVersion,
                    DEFAULT = "",
                    Saved = true
                });                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.EmailAlertType,
                    PARAM = StaticData.ObjectType,
                    DEFAULT = StaticData.EmailAlertType,
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.EmailAlertType,
                    PARAM = StaticData.SystemName,
                    DEFAULT = "",
                    Saved = true
                });

                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.EmailAlertType,
                    PARAM = StaticData.HardName,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.EmailAlertType,
                    PARAM = StaticData.PumpType,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.EmailAlertType,
                    PARAM = StaticData.ChemistryType,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.EmailAlertType,
                    PARAM = StaticData.SystemType,
                    DEFAULT = "",
                    Saved = true
                });
                data.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error creating email alert parameter types {0}", ex.Message));
            }
        }

        private void AddPermitParameters(SimulatorContext data)
        {
            try
            {
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PermitType,
                    PARAM = StaticData.ObjectName,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PermitType,
                    PARAM = StaticData.ObjectVersion,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PermitType,
                    PARAM = StaticData.ObjectType,
                    DEFAULT = StaticData.PermitType,
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PermitType,
                    PARAM = StaticData.ListOrder,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PermitType,
                    PARAM = StaticData.HardName,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PermitType,
                    PARAM = StaticData.SystemName,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PermitType,
                    PARAM = StaticData.ShowMenu,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PermitType,
                    PARAM = "ENABLE",
                    DEFAULT = "OFF",
                    Saved = true
                }); data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PermitType,
                    PARAM = "PASSWRD",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PermitType,
                    PARAM = "LEGACY",
                    DEFAULT = "",
                    Saved = true
                });
                // Status is added to permit for secutity enable, it is not part of the database 
                // but is used to determine whether security is enabled on the THW
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PermitType,
                    PARAM = "STATUS",
                    DEFAULT = "OFF",
                    Saved = false
                });
                data.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error creating permit parameter types {0}", ex.Message));
            }
        }

        private void AddStatusMessageMasterParameters(SimulatorContext data)
        {
            try
            {
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.StatusMessageMasterType,
                    PARAM = StaticData.ObjectName,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.StatusMessageMasterType,
                    PARAM = StaticData.ObjectVersion,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.StatusMessageMasterType,
                    PARAM = StaticData.ObjectType,
                    DEFAULT = StaticData.StatusMessageMasterType,
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.StatusMessageMasterType,
                    PARAM = StaticData.Static,
                    DEFAULT = "ON",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.StatusMessageMasterType,
                    PARAM = "RESET",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.StatusMessageMasterType,
                    PARAM = "ACT",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.StatusMessageMasterType,
                    PARAM = "BADGE",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.StatusMessageMasterType,
                    PARAM = "PARTY",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.StatusMessageMasterType,
                    PARAM = "ALARM",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.StatusMessageMasterType,
                    PARAM = "ROYAL",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.StatusMessageMasterType,
                    PARAM = "RECALL",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.StatusMessageMasterType,
                    PARAM = "TIMZON",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.StatusMessageMasterType,
                    PARAM = "TIME",
                    DEFAULT = "",
                    Saved = false
                });
                data.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error creating master status message parameter types {0}", ex.Message));
            }
        }
        private void AddStatusMessageParameters(SimulatorContext data)
        {
            try
            {
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.StatusMessageType,
                    PARAM = StaticData.ObjectName,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.StatusMessageType,
                    PARAM = StaticData.ObjectVersion,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.StatusMessageType,
                    PARAM = StaticData.ObjectType,
                    DEFAULT = StaticData.StatusMessageType,
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.StatusMessageType,
                    PARAM = StaticData.Parent,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.StatusMessageType,
                    PARAM = "MODE",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.StatusMessageType,
                    PARAM = StaticData.SystemName,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.StatusMessageType,
                    PARAM = "COUNT",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.StatusMessageType,
                    PARAM = "PARTY",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.StatusMessageType,
                    PARAM = "SINDEX",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.StatusMessageType,
                    PARAM = "TIME",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.StatusMessageType,
                    PARAM = "ALARM",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.StatusMessageType,
                    PARAM = "SHOMNU",
                    DEFAULT = "ON",
                    Saved = true
                });
                data.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error creating status message parameter types {0}", ex.Message));
            }
        }

        private void AddSensorParameters(SimulatorContext data)
        {
            try
            {
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SensorType,
                    PARAM = StaticData.ObjectName,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SensorType,
                    PARAM = StaticData.ObjectVersion,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SensorType,
                    PARAM = StaticData.ObjectType,
                    DEFAULT = StaticData.SensorType,
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SensorType,
                    PARAM = StaticData.Static,
                    DEFAULT = "ON",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SensorType,
                    PARAM = StaticData.SubType,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SensorType,
                    PARAM = StaticData.ListOrder,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SensorType,
                    PARAM = StaticData.Parent,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SensorType,
                    PARAM = StaticData.HardName,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SensorType,
                    PARAM = StaticData.SystemName,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SensorType,
                    PARAM = "CALIB",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SensorType,
                    PARAM = StaticData.Status,
                    DEFAULT = "ON",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SensorType,
                    PARAM = StaticData.Source,
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SensorType,
                    PARAM = "PROBE",
                    DEFAULT = "",
                    Saved = false
                });
                data.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error creating sensor parameter types {0}", ex.Message));
            }
        }

        private void AddSensorSingletonParameters(SimulatorContext data)
        {
            try
            {
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SensorSingletonType,
                    PARAM = StaticData.ObjectName,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SensorSingletonType,
                    PARAM = StaticData.ObjectVersion,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SensorSingletonType,
                    PARAM = StaticData.ObjectType,
                    DEFAULT = StaticData.SensorSingletonType,
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SensorSingletonType,
                    PARAM = StaticData.Static,
                    DEFAULT = "ON",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SensorSingletonType,
                    PARAM = StaticData.SubType,
                    DEFAULT = "AIR",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SensorSingletonType,
                    PARAM = StaticData.Status,
                    DEFAULT = "OK",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SensorSingletonType,
                    PARAM = "SOURCE",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SensorSingletonType,
                    PARAM = "PROBE",
                    DEFAULT = "",
                    Saved = false
                });
                data.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error creating singleton sensor parameter types {0}", ex.Message));
            }
        }

        private void AddMenuParameters(SimulatorContext data)
        {
            try
            {
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.MenuType,
                    PARAM = StaticData.ObjectName,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.MenuType,
                    PARAM = StaticData.ObjectType,
                    DEFAULT = StaticData.MenuType,
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.MenuType,
                    PARAM = StaticData.Static,
                    DEFAULT = "ON",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.MenuType,
                    PARAM = StaticData.ObjectVersion,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.MenuType,
                    PARAM = StaticData.ListOrder,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.MenuType,
                    PARAM = StaticData.PermitType,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.MenuType,
                    PARAM = "MENU",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.MenuType,
                    PARAM = "BADGE",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.MenuType,
                    PARAM = "LIGHT",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.MenuType,
                    PARAM = "RESET",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.MenuType,
                    PARAM = "PARTY",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.MenuType,
                    PARAM = "MAX",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.MenuType,
                    PARAM = "COUNT",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.MenuType,
                    PARAM = "MANOVR",
                    DEFAULT = "OFF",
                    Saved = true
                });
                data.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error creating menu parameter types {0}", ex.Message));
            }
        }

        private void AddServiceParameters(SimulatorContext data)
        {
            try
            {
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ServiceType,
                    PARAM = StaticData.ObjectName,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ServiceType,
                    PARAM = StaticData.ObjectType,
                    DEFAULT = StaticData.ServiceType,
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ServiceType,
                    PARAM = StaticData.Static,
                    DEFAULT = "ON",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ServiceType,
                    PARAM = StaticData.ObjectVersion,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ServiceType,
                    PARAM = "TIME",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ServiceType,
                    PARAM = "ENABLE",
                    DEFAULT = "OFF",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ServiceType,
                    PARAM = "TEMP",
                    DEFAULT = "OFF",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ServiceType,
                    PARAM = StaticData.Status,
                    DEFAULT = "OFF",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ServiceType,
                    PARAM = "ON",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ServiceType,
                    PARAM = "OFF",
                    DEFAULT = "",
                    Saved = true
                });
                data.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error creating service parameter types {0}", ex.Message));
            }
        }

        private void AddPanelParameters(SimulatorContext data)
        {
            try
            {
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PanelType,
                    PARAM = StaticData.ObjectName,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PanelType,
                    PARAM = StaticData.ObjectVersion,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PanelType,
                    PARAM = StaticData.ObjectType,
                    DEFAULT = StaticData.PanelType,
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PanelType,
                    PARAM = StaticData.Static,
                    DEFAULT = "ON",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PanelType,
                    PARAM = StaticData.SubType,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PanelType,
                    PARAM = StaticData.ListOrder,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PanelType,
                    PARAM = StaticData.HardName,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PanelType,
                    PARAM = StaticData.SystemName,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PanelType,
                    PARAM = "COMUART",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PanelType,
                    PARAM = "COMETHER",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PanelType,
                    PARAM = "VER",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PanelType,
                    PARAM = "SERNUM",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PanelType,
                    PARAM = "MNFDAT",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PanelType,
                    PARAM = "TIME",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PanelType,
                    PARAM = "NORMAL",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PanelType,
                    PARAM = "DIMMER",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PanelType,
                    PARAM = "MODE",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PanelType,
                    PARAM = "START",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PanelType,
                    PARAM = "RECENT",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PanelType,
                    PARAM = "SENSE",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PanelType,
                    PARAM = "AIR",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PanelType,
                    PARAM = "MASTER",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PanelType,
                    PARAM = "PORT",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PanelType,
                    PARAM = "VALVE",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PanelType,
                    PARAM = "SPILL",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PanelType,
                    PARAM = "CURRENT",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PanelType,
                    PARAM = "HITMP",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PanelType,
                    PARAM = "BATT",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.PanelType,
                    PARAM = "PHMOD",
                    DEFAULT = "",
                    Saved = false
                });
                data.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error creating panel parameter types {0}", ex.Message));
            }
        }

        private void AddModuleParameters(SimulatorContext data)
        {
            try
            {
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ModuleType,
                    PARAM = StaticData.ObjectName,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ModuleType,
                    PARAM = StaticData.ObjectVersion,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ModuleType,
                    PARAM = StaticData.ObjectType,
                    DEFAULT = StaticData.ModuleType,
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ModuleType,
                    PARAM = StaticData.Static,
                    DEFAULT = "ON",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ModuleType,
                    PARAM = StaticData.SubType,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ModuleType,
                    PARAM = StaticData.Parent,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ModuleType,
                    PARAM = StaticData.HardName,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ModuleType,
                    PARAM = StaticData.SystemName,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ModuleType,
                    PARAM = "PORT",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ModuleType,
                    PARAM = StaticData.ListOrder,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ModuleType,
                    PARAM = "VER",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ModuleType,
                    PARAM = "SERNUM",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ModuleType,
                    PARAM = "MNFDAT",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ModuleType,
                    PARAM = "CNFG",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.ModuleType,
                    PARAM = "BADGE",
                    DEFAULT = "",
                    Saved = true
                });
                data.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error creating module parameter types {0}", ex.Message));
            }
        }

        //private void AddRelayParameters(SimulatorContext data)
        //{
        //    try
        //    {
        //        data.ObjectParameterTypes.Add(new ObjectParameterType
        //        {
        //            OBJTYP = StaticData.RelayType,
        //            PARAM = StaticData.ObjectName,
        //            DEFAULT = "",
        //            Saved = false
        //        });
        //        data.ObjectParameterTypes.Add(new ObjectParameterType
        //        {
        //            OBJTYP = StaticData.RelayType,
        //            PARAM = StaticData.ObjectType,
        //            DEFAULT = StaticData.RelayType,
        //            Saved = false
        //        });
        //        data.ObjectParameterTypes.Add(new ObjectParameterType
        //        {
        //            OBJTYP = StaticData.RelayType,
        //            PARAM = StaticData.Static,
        //            DEFAULT = "ON",
        //            Saved = false
        //        });
        //        data.ObjectParameterTypes.Add(new ObjectParameterType
        //        {
        //            OBJTYP = StaticData.RelayType,
        //            PARAM = StaticData.SubType,
        //            DEFAULT = "",
        //            Saved = false
        //        });
        //        data.ObjectParameterTypes.Add(new ObjectParameterType
        //        {
        //            OBJTYP = StaticData.RelayType,
        //            PARAM = StaticData.HardName,
        //            DEFAULT = "",
        //            Saved = false
        //        });
        //        data.ObjectParameterTypes.Add(new ObjectParameterType
        //        {
        //            OBJTYP = StaticData.RelayType,
        //            PARAM = StaticData.SystemName,
        //            DEFAULT = "",
        //            Saved = false
        //        });
        //        data.ObjectParameterTypes.Add(new ObjectParameterType
        //        {
        //            OBJTYP = StaticData.RelayType,
        //            PARAM = "PANEL",
        //            DEFAULT = "",
        //            Saved = false
        //        });
        //        data.ObjectParameterTypes.Add(new ObjectParameterType
        //        {
        //            OBJTYP = StaticData.RelayType,
        //            PARAM = StaticData.Parent,
        //            DEFAULT = "",
        //            Saved = false
        //        });
        //        data.ObjectParameterTypes.Add(new ObjectParameterType
        //        {
        //            OBJTYP = StaticData.RelayType,
        //            PARAM = "TIMOUT",
        //            DEFAULT = "",
        //            Saved = false
        //        });
        //        data.ObjectParameterTypes.Add(new ObjectParameterType
        //        {
        //            OBJTYP = StaticData.RelayType,
        //            PARAM = "RUNON",
        //            DEFAULT = "",
        //            Saved = false
        //        });
        //        data.ObjectParameterTypes.Add(new ObjectParameterType
        //        {
        //            OBJTYP = StaticData.RelayType,
        //            PARAM = StaticData.Status,
        //            DEFAULT = "OFF",
        //            Saved = false
        //        });
        //        data.ObjectParameterTypes.Add(new ObjectParameterType
        //        {
        //            OBJTYP = StaticData.RelayType,
        //            PARAM = "SOURCE",
        //            DEFAULT = "",
        //            Saved = false
        //        });                data.ObjectParameterTypes.Add(new ObjectParameterType
        //        {
        //            OBJTYP = StaticData.RelayType,
        //            PARAM = StaticData.ListOrder,
        //            DEFAULT = "",
        //            Saved = false
        //        });

        //        data.ObjectParameterTypes.Add(new ObjectParameterType
        //        {
        //            OBJTYP = StaticData.RelayType,
        //            PARAM = "ASSIGN",
        //            DEFAULT = "",
        //            Saved = false
        //        });
        //        data.SaveChanges();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(string.Format("Error creating relay parameter types {0}", ex.Message));
        //    }
        //}

        private void AddSystemParameters(SimulatorContext data)
        {
            try
            {
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SystemType,
                    PARAM = StaticData.ObjectName,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SystemType,
                    PARAM = StaticData.ObjectVersion,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SystemType,
                    PARAM = StaticData.ObjectType,
                    DEFAULT = StaticData.SystemType,
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SystemType,
                    PARAM = StaticData.Static,
                    DEFAULT = "YES",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SystemType,
                    PARAM = "MODE",
                    DEFAULT = "ENGLISH",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SystemType,
                    PARAM = "ALPHA",
                    DEFAULT = "ENGLISH",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SystemType,
                    PARAM = "ENABLE",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SystemType,
                    PARAM = "PASSWRD",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SystemType,
                    PARAM = StaticData.HardName,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SystemType,
                    PARAM = StaticData.SystemName,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SystemType,
                    PARAM = "PROPLST",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SystemType,
                    PARAM = "ACT",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SystemType,
                    PARAM = "NAME",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SystemType,
                    PARAM = "ADDRESS",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SystemType,
                    PARAM = "CITY",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SystemType,
                    PARAM = "STATE",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SystemType,
                    PARAM = "ZIP",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SystemType,
                    PARAM = "TIMZON",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SystemType,
                    PARAM = "PHONE",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SystemType,
                    PARAM = "PHONE2",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SystemType,
                    PARAM = "EMAIL",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SystemType,
                    PARAM = "EMAIL2",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SystemType,
                    PARAM = "COUNTRY",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SystemType,
                    PARAM = "PERMIT",
                    DEFAULT = "OFF",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SystemType,
                    PARAM = "AVAIL",
                    DEFAULT = "OFF",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SystemType,
                    PARAM = "VACFLO",
                    DEFAULT = "OFF",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SystemType,
                    PARAM = "VACTIM",
                    DEFAULT = "OFF",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SystemType,
                    PARAM = "START",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.SystemType,
                    PARAM = "STOP",
                    DEFAULT = "",
                    Saved = true
                });
                data.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error creating valve system types {0}", ex.Message));
            }
        }

        private void AddNetworkParameters(SimulatorContext data)
        {
            try
            {
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.NetworkType,
                    PARAM = StaticData.ObjectName,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.NetworkType,
                    PARAM = StaticData.ObjectVersion,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.NetworkType,
                    PARAM = StaticData.Static,
                    DEFAULT = "ON",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.NetworkType,
                    PARAM = "DHCP",
                    DEFAULT = "AUTO",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.NetworkType,
                    PARAM = "IPADY",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.NetworkType,
                    PARAM = "SUBNET",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.NetworkType,
                    PARAM = "DFGATE",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.NetworkType,
                    PARAM = "PRIM",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.NetworkType,
                    PARAM = "SEC",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.NetworkType,
                    PARAM = "MACADY",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.NetworkType,
                    PARAM = "ACT",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.NetworkType,
                    PARAM = "IN",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.NetworkType,
                    PARAM = "AVAIL",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.NetworkType,
                    PARAM = "STATE",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.NetworkType,
                    PARAM = "REMOTE",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.NetworkType,
                    PARAM = "PROGRES",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.NetworkType,
                    PARAM = "LSTTMP",
                    DEFAULT = "",
                    Saved = false
                });
                //data.ObjectParameterTypes.Add(new ObjectParameterType
                //{
                //    OBJTYP = StaticData.NetworkType,
                //    PARAM = "WAPPW",
                //    DEFAULT = "",
                //    Saved = true
                //});
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.NetworkType,
                    PARAM = "WAPNET",
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.NetworkType,
                    PARAM = "WCP",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.NetworkType,
                    PARAM = "PASSWRD",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.NetworkType,
                    PARAM = "LOGIN",
                    DEFAULT = "",
                    Saved = false
                });
                data.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error creating valve network types {0}", ex.Message));
            }
        }

        private void AddBoardParameters(SimulatorContext data)
        {
            try
            {
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.BoardType,
                    PARAM = StaticData.ObjectName,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.BoardType,
                    PARAM = StaticData.ObjectVersion,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.BoardType,
                    PARAM = StaticData.ObjectType,
                    DEFAULT = StaticData.BoardType,
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.BoardType,
                    PARAM = "RESET",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.BoardType,
                    PARAM = "MONITOR",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.BoardType,
                    PARAM = "HTMODE",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.BoardType,
                    PARAM = "MASTER",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.BoardType,
                    PARAM = "PORT",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.BoardType,
                    PARAM = "VALVE",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.BoardType,
                    PARAM = "SPILL",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.BoardType,
                    PARAM = "CURRENT",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.BoardType,
                    PARAM = "HITMP",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.BoardType,
                    PARAM = "BATT",
                    DEFAULT = "",
                    Saved = false
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.BoardType,
                    PARAM = "PHMOD",
                    DEFAULT = "",
                    Saved = false
                });
                data.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error creating board parameter types {0}", ex.Message));
            }
        }
        private void AddUserParameters(SimulatorContext data)
        {
            try
            {
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.UserType,
                    PARAM = StaticData.ObjectName,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.UserType,
                    PARAM = StaticData.ObjectVersion,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.UserType,
                    PARAM = StaticData.ObjectType,
                    DEFAULT = StaticData.UserType,
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.UserType,
                    PARAM = StaticData.SystemName,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.UserType,
                    PARAM = StaticData.HardName,
                    DEFAULT = "",
                    Saved = true
                });
                data.ObjectParameterTypes.Add(new ObjectParameterType
                {
                    OBJTYP = StaticData.UserType,
                    PARAM = "PASSWRD",
                    DEFAULT = "",
                    Saved = true
                });
                data.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error creating user parameter types {0}", ex.Message));
            }
        }
        private void Config1LightCircuits(SimulatorContext context, int name, UInt32 revision)
        {
            try
            {
                AddObject(context, revision, "C5933", name, new Dictionary<string, string>
                {
                    {StaticData.ObjectType, StaticData.CircuitType},
                    {StaticData.SubType, StaticData.IntellibrightSubtype},
                    {StaticData.SystemName, "Pool Light 1"},
                    {StaticData.Status, StaticData.On},
                    {StaticData.ListOrder, "1"},
                    {StaticData.ShowMenu, "le"},
                    {"USAGE", "D"}
                });

                AddObject(context, revision, "C5433", name, new Dictionary<string, string>
                {
                    {StaticData.ObjectType, StaticData.CircuitType},
                    {StaticData.SubType, StaticData.IntellibrightSubtype},
                    {StaticData.SystemName, "Pool Light 2"},
                    {StaticData.Status, StaticData.On},
                    {StaticData.ListOrder, "1"},
                    {StaticData.ShowMenu, "le"},
                    {"USAGE", "EignqBARrpcs"}
                });

                AddObject(context, revision, "C5436", name, new Dictionary<string, string>
                {
                    {StaticData.ObjectType, StaticData.CircuitType},
                    {StaticData.SubType, StaticData.IntellibrightSubtype},
                    {StaticData.SystemName, "Pool Light 3"},
                    {StaticData.Status, StaticData.Off},
                    {StaticData.ListOrder, "2"},
                    {StaticData.ShowMenu, "le"},
                    {"USAGE", "EigmqBArcs"}
                });

                //magic stream Lights
                AddObject(context, revision, "C5435", name, new Dictionary<string, string>
                {
                    {StaticData.ObjectType, StaticData.CircuitType},
                    {StaticData.SubType, StaticData.Magic2Subtype},
                    {StaticData.SystemName, "laminar"},
                    {StaticData.Status, StaticData.Off},
                    {StaticData.ListOrder, "3"},
                    {StaticData.ShowMenu, "le"},
                    {"USAGE", "EinBRpcs"}
                });

                // All lights group
                AddObject(context, revision, StaticData.AllLights, name, new Dictionary<string, string>
                {
                    {StaticData.ObjectType, StaticData.CircuitType},
                    {StaticData.SubType, StaticData.CircuitGroupType},
                    {StaticData.SystemName, "All Lights"},
                    {StaticData.Status, StaticData.Off},
                    {StaticData.ListOrder, "1"},
                    {StaticData.ShowMenu, "le"}
                });

                // Generic Lights
                AddObject(context, revision, "C5500", name, new Dictionary<string, string>
                {
                    {StaticData.ObjectType, StaticData.CircuitType},
                    {StaticData.SubType, StaticData.LightSubtype},
                    {StaticData.SystemName, "Pool Light 1"},
                    {StaticData.Status, StaticData.Off},
                    {StaticData.ListOrder, "4"},
                    {StaticData.ShowMenu, "le"},
                    {"USAGE", "igBAcs"}
                });

                AddObject(context, revision, "C5501", name, new Dictionary<string, string>
                {
                    {StaticData.ObjectType, StaticData.CircuitType},
                    {StaticData.SubType, StaticData.LightSubtype},
                    {StaticData.SystemName, "Spa Light"},
                    {StaticData.Status, StaticData.Off},
                    {StaticData.ListOrder, "5"},
                    {StaticData.ShowMenu, "le"},
                    {"USAGE", "inBRcs"}
                });

                AddObject(context, revision, "C5502", name, new Dictionary<string, string>
                {
                    {StaticData.ObjectType, StaticData.CircuitType},
                    {StaticData.SubType, StaticData.LightSubtype},
                    {StaticData.SystemName, "Waterfall"},
                    {StaticData.Status, StaticData.Off},
                    {StaticData.ListOrder, "6"},
                    {StaticData.ShowMenu, "le"},
                    {"USAGE", "pcs"}
                });

                // Pool Light Groups
                AddObject(context, revision, "C5503", name, new Dictionary<string, string>
                {
                    {StaticData.ObjectType, StaticData.CircuitType},
                    {StaticData.SubType, StaticData.LightShowType},
                    {StaticData.SystemName, "Pool Lights"},
                    {StaticData.Status, StaticData.Off},
                    {StaticData.ListOrder, "1"},
                    {StaticData.ShowMenu, "le"},
                    {"CHILD", "C5433, C5500, C5436"},
                    {"USAGE", "EignqBARrpcs"}
                });
                AddObject(context, revision, "C5504", name, new Dictionary<string, string>
                {
                    {StaticData.ObjectType, StaticData.CircuitGroupType},
                    {StaticData.Parent, "C5503"},
                    {StaticData.CircuitType, "C5433"},
                    {StaticData.Action, "WHITE"},
                    {StaticData.Delay, ""},
                    {StaticData.ListOrder, "1"},
                    {StaticData.ShowMenu, "le"},                    {StaticData.Color, "WHITE"},
                    {StaticData.Start, StaticData.Off}
                });
                AddObject(context, revision, "C5505", name, new Dictionary<string, string>
                {
                    {StaticData.ObjectType, StaticData.CircuitGroupType},
                    {StaticData.Parent, "C5503"},
                    {StaticData.CircuitType, "C5500"},
                    {StaticData.Action, "WHITE"},
                    {StaticData.Delay, ""},
                    {StaticData.ListOrder, "1"},
                    {StaticData.ShowMenu, "le"},
                    {StaticData.Color, "WHITE"},
                    {StaticData.Start, StaticData.Off}
                });
                AddObject(context, revision, "C5506", name, new Dictionary<string, string>
                {
                    {StaticData.ObjectType, StaticData.CircuitGroupType},
                    {StaticData.Parent, "C5503"},
                    {StaticData.CircuitType, "C5436"},
                    {StaticData.Action, "WHITE"},
                    {StaticData.Delay, ""},
                    {StaticData.ListOrder, "1"},
                    {StaticData.ShowMenu, "le"},
                    {StaticData.Color, "WHITE"},
                    {StaticData.Start, StaticData.Off}
                });
                // Non pool lights group
                AddObject(context, revision, "C5508", name, new Dictionary<string, string>
                {
                    {StaticData.ObjectType, StaticData.CircuitType},
                    {StaticData.SubType, StaticData.LightShowType},
                    {StaticData.SystemName, "Other Lights"},
                    {StaticData.Status, StaticData.Off},
                    {StaticData.ListOrder, "2"},
                    {StaticData.ShowMenu, "le"},
                    {"CHILD", "C5500, C5502"},
                    {"USAGE", "inBRps"}
                });
                AddObject(context, revision, "C5509", name, new Dictionary<string, string>
                {
                    {StaticData.ObjectType, StaticData.CircuitGroupType},
                    {StaticData.Parent, "C5508"},
                    {StaticData.CircuitType, "C5500"},
                    {StaticData.Action, "WHITE"},
                    {StaticData.Delay, ""},
                    {StaticData.ListOrder, "1"},
                    {StaticData.ShowMenu, "le"},
                    {StaticData.Color, "WHITE"},
                    {StaticData.Start, StaticData.Off}
                });
                AddObject(context, revision, "C5510", name, new Dictionary<string, string>
                {
                    {StaticData.ObjectType, StaticData.CircuitGroupType},
                    {StaticData.Parent, "C5504"},
                    {StaticData.CircuitType, "C5508"},
                    {StaticData.Action, "WHITE"},
                    {StaticData.Delay, ""},
                    {StaticData.ListOrder, "1"},
                    {StaticData.ShowMenu, "le"},
                    {StaticData.Color, "WHITE"},
                    {StaticData.Start, StaticData.Off}
                });
                AddObject(context, revision, "C5511", name, new Dictionary<string, string>
                {
                    {StaticData.ObjectType, StaticData.CircuitGroupType},
                    {StaticData.Parent, "C5508"},
                    {StaticData.CircuitType, "C5502"},
                    {StaticData.Action, "WHITE"},
                    {StaticData.Delay, ""},
                    {StaticData.ListOrder, "1"},
                    {StaticData.ShowMenu, "le"},
                    {StaticData.Color, "WHITE"},
                    {StaticData.Start, StaticData.Off}
                });
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error setting default light circuits, exception: {0}", ex.Message));
            }
        }

        private void Config1FeaturesAndSettings(SimulatorContext context, int name, UInt32 revision)
        {
            try
            {
                AddObject(context, revision, "C5476", name, new Dictionary<string, string>
                {
                    {StaticData.ObjectType, StaticData.CircuitType},
                    {StaticData.SubType, StaticData.GenericSubtype},
                    {StaticData.SystemName, "Water Fall"},
                    {StaticData.Status, StaticData.Off},
                    {StaticData.ListOrder, "1"},
                    {StaticData.ShowMenu, "fe"}
                });
                AddObject(context, revision, "C5477", name, new Dictionary<string, string>
                {
                    {StaticData.ObjectType, StaticData.CircuitType},
                    {StaticData.SubType, "FRZ"},
                    {StaticData.SystemName, "Freeze Mode"},
                    {StaticData.Status, StaticData.On},
                    {StaticData.ListOrder, "1"},
                    {StaticData.ShowMenu, "s"}
                });
                AddObject(context, revision, "C5478", name, new Dictionary<string, string>
                {
                    {StaticData.ObjectType, StaticData.CircuitType},
                    {StaticData.SubType, "MASTER"},
                    {StaticData.SystemName, "Cleaner"},
                    {StaticData.Status, StaticData.Off},
                    {StaticData.ListOrder, "1"},
                    {StaticData.ShowMenu, "se"}
                });
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format(
                    "Error setting default features and settings circuits, exception: {0}", ex.Message));
            }
        }

        /// <summary>
        /// Default shared Pool and spa Circuit for configuration
        /// </summary>
        /// <param name="context"></param>
        /// <param name="name"></param>
        /// <param name="revision"></param>
        private void Config1BodyCircuits(SimulatorContext context, int name, UInt32 revision)
        {
            try
            {
                AddObject(context, revision, "C1355", name, new Dictionary<string, string>
                {
                    {StaticData.ObjectType, StaticData.CircuitType},
                    {StaticData.SubType, StaticData.BodyType},
                    {StaticData.SystemName, "Pool Circuit"},
                    {StaticData.Status, StaticData.On},
                    {StaticData.ListOrder, "1"},
                    {StaticData.BodyType, "B1000"},
                    {StaticData.ShowMenu, "s"}
                });
                AddObject(context, revision, "C1356", name, new Dictionary<string, string>
                {
                    {StaticData.ObjectType, StaticData.CircuitType},
                    {StaticData.SubType, StaticData.BodyType},
                    {StaticData.SystemName, "Spa Circuit"},
                    {StaticData.Status, StaticData.Off},
                    {StaticData.ListOrder, "1"},
                    {StaticData.BodyType, "B1001"},
                    {StaticData.ShowMenu, "s"}
                });
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error setting default body circuits, exception: {0}", ex.Message));
            }
        }

        /// Default unshared Spa Circuit for configuration
        /// </summary>
        /// <param name="context"></param>
        /// <param name="name"></param>
        /// <param name="revision"></param>
        private void Config1Spa(SimulatorContext context, int name, UInt32 revision)
        {
            try
            {
                AddObject(context, revision, "B1001", name, new Dictionary<string, string>
                {
                    {StaticData.ObjectType, StaticData.BodyType},
                    {StaticData.SubType, "SPA"},
                    {StaticData.SystemName, "Spa"},
                    {StaticData.Status, StaticData.On},
                    {StaticData.Share, ""},
                    {StaticData.ListOrder, "2"},
                    {StaticData.HeaterType, "H1000"},
                    {StaticData.HiTemp, "10"},
                    {StaticData.LowTemp, "35"},
                    {StaticData.Temperature, "30"},
                    {StaticData.Prime, "C1356"},
                    {StaticData.Filter, "C1356"},
                    {StaticData.HeatSource, "H1001"}
                });
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error setting default light circuits, exception: {0}", ex.Message));
            }
        }

        /// Default shared Spa Circuit for configuration
        /// </summary>
        /// <param name="context"></param>
        /// <param name="name"></param>
        /// <param name="revision"></param>
        private void Config2Spa(SimulatorContext context, int name, UInt32 revision)
        {
            try
            {
                AddObject(context, revision, "B1001", name, new Dictionary<string, string>
                {
                    {StaticData.ObjectType, StaticData.BodyType},
                    {StaticData.SubType, "SPA"},
                    {StaticData.SystemName, "Spa"},
                    {StaticData.Status, StaticData.On},
                    {StaticData.Share, "B1000"},
                    {StaticData.ListOrder, "2"},
                    {StaticData.HeaterType, "H1000"},
                    {StaticData.HiTemp, "10"},
                    {StaticData.LowTemp, "35"},
                    {StaticData.Temperature, "30"},
                    {StaticData.Prime, "C1356"},
                    {StaticData.Filter, "C1356"},
                    {StaticData.HeatSource, "H1001"}
                });
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error setting default light circuits, exception: {0}", ex.Message));
            }
        }

        /// <summary>
        /// Default unshared Pool 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="name"></param>
        /// <param name="revision"></param>
        private void Config1Pool(SimulatorContext context, int name, UInt32 revision)
        {
            try
            {
                AddObject(context, revision, "B1000", name, new Dictionary<string, string>
                {
                    {StaticData.ObjectType, StaticData.BodyType},
                    {StaticData.SubType, "POOL"},
                    {StaticData.SystemName, "Pool"},
                    {StaticData.Status, StaticData.On},
                    {StaticData.Share, ""},
                    {StaticData.ListOrder, "1"},
                    {StaticData.HeaterType, "H1000"},
                    {StaticData.HiTemp, "20"},
                    {StaticData.LowTemp, "26"},
                    {StaticData.Temperature, "24"},
                    {StaticData.Prime, "C1355"},
                    {StaticData.Filter, "C1355"},
                    {StaticData.HeatSource, "H1001"}
                });
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error setting default light circuits, exception: {0}", ex.Message));
            }
        }

        /// <summary>
        /// Default shared Pool 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="name"></param>
        /// <param name="revision"></param>
        private void Config2Pool(SimulatorContext context, int name, UInt32 revision)
        {
            try
            {
                AddObject(context, revision, "B1000", name, new Dictionary<string, string>
                {
                    {StaticData.ObjectType, StaticData.BodyType},
                    {StaticData.SubType, "POOL"},
                    {StaticData.SystemName, "Pool"},
                    {StaticData.Status, StaticData.On},
                    {StaticData.Share, "B1001"},
                    {StaticData.ListOrder, "1"},
                    {StaticData.HeaterType, "H1000"},
                    {StaticData.HiTemp, "20"},
                    {StaticData.LowTemp, "26"},
                    {StaticData.Temperature, "24"},
                    {StaticData.Prime, "C1355"},
                    {StaticData.Filter, "C1355"},
                    {StaticData.HeatSource, "H1001"}
                });
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error setting default light circuits, exception: {0}", ex.Message));
            }
        }

        /// <summary>
        /// Default Body for configuration
        /// </summary>
        /// <param name="context"></param>
        private void Config1Heater(SimulatorContext context, int name, UInt32 revision)
        {
            try
            {
                AddObject(context, revision, "G1000", name, new Dictionary<string, string>
                {
                    {StaticData.ObjectType, StaticData.HeatComboType},
                    {StaticData.ListOrder, "2"},
                    {StaticData.HeaterType, "H1000"},
                    {StaticData.Use, "H1001"},
                    {StaticData.Action, "PRIOR"}
                });
                AddObject(context, revision, "G1001", name, new Dictionary<string, string>
                {
                    {StaticData.ObjectType, StaticData.HeatComboType},
                    {StaticData.ListOrder, "1"},
                    {StaticData.HeaterType, "H1000"},
                    {StaticData.Use, "H1002"},
                    {StaticData.Action, "PRIOR"}
                });
                AddObject(context, revision, "H1000", name, new Dictionary<string, string>
                {
                    {StaticData.ObjectType, StaticData.HeaterType},
                    {StaticData.ListOrder, "1"},
                    {StaticData.SubType, StaticData.HeatComboType},
                    {StaticData.SystemName, "Body Heater"},
                    {StaticData.BodyType, "B1000"},
                    {StaticData.Share, "B1001"},
                    {StaticData.Status, StaticData.Off},
                    {StaticData.PermitType, StaticData.On},
                    {StaticData.Delay, "160"},
                    {StaticData.ShowMenu, "hc"},
                    {StaticData.Ready, StaticData.True}
                });
                AddObject(context, revision, "H1002", name, new Dictionary<string, string>
                {
                    {StaticData.ObjectType, StaticData.HeaterType},
                    {StaticData.ListOrder, "1"},
                    {StaticData.SubType, StaticData.SolarSubtype},
                    {StaticData.SystemName, "Spa Heater"},
                    {StaticData.BodyType, "B1000"},
                    {StaticData.Share, "B1001"},
                    {StaticData.Status, StaticData.Off},
                    {StaticData.PermitType, StaticData.On},
                    {StaticData.Delay, "120"},
                    {StaticData.ShowMenu, "h"},
                    {StaticData.Ready, StaticData.True}
                });
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error setting default heater1 circuits, exception: {0}", ex.Message));
            }
        }

        private void Config2Heater(SimulatorContext context, int name, UInt32 revision)
        {
            try
            {
                AddObject(context, revision, "G1000", name, new Dictionary<string, string>
                {
                    {StaticData.ObjectType, StaticData.HeatComboType},
                    {StaticData.ListOrder, "2"},
                    {StaticData.Use, "H1001"},
                    {StaticData.HeaterType, "H1000"},
                    {StaticData.Action, "PRIOR"}
                });
                AddObject(context, revision, "G1001", name, new Dictionary<string, string>
                {
                    {StaticData.ObjectType, StaticData.HeatComboType},
                    {StaticData.ListOrder, "1"},
                    {StaticData.HeaterType, "H1000"},
                    {StaticData.Use, "H1002"},
                    {StaticData.Action, "PRIOR"}
                });
                AddObject(context, revision, "H1000", name, new Dictionary<string, string>
                {
                    {StaticData.ObjectType, StaticData.HeaterType},
                    {StaticData.ListOrder, "1"},
                    {StaticData.SubType, StaticData.HeatComboType},
                    {StaticData.SystemName, "Body Heater"},
                    {StaticData.BodyType, "B1000"},
                    {StaticData.Share, "B1001"},
                    {StaticData.Status, StaticData.Off},
                    {StaticData.PermitType, StaticData.On},
                    {StaticData.ShowMenu, "hc"},
                    {StaticData.Delay, "120"},
                    {StaticData.Ready, StaticData.True}
                });
                AddObject(context, revision, "H1001", name, new Dictionary<string, string>
                {
                    {StaticData.ObjectType, StaticData.HeaterType},
                    {StaticData.ListOrder, "1"},
                    {StaticData.SubType, StaticData.GenericSubtype},
                    {StaticData.SystemName, "Generic Heater"},
                    {StaticData.BodyType, "B1000"},
                    {StaticData.Share, "B1001"},
                    {StaticData.Status, StaticData.Off},
                    {StaticData.PermitType, StaticData.On},
                    {StaticData.ShowMenu, "h"},
                    {StaticData.Delay, "120"},
                    {StaticData.Ready, StaticData.True}
                });
                AddObject(context, revision, "H1002", name, new Dictionary<string, string>
                {
                    {StaticData.ObjectType, StaticData.HeaterType},
                    {StaticData.ListOrder, "1"},
                    {StaticData.SubType, "HTPMP"},
                    {StaticData.SystemName, "Spa Heater"},
                    {StaticData.BodyType, "B1000"},
                    {StaticData.Share, "B1001"},
                    {StaticData.Status, StaticData.Off},
                    {StaticData.PermitType, StaticData.On},
                    {StaticData.Delay, "120"},
                    {StaticData.ShowMenu, "hc"},
                    {StaticData.Ready, StaticData.True}
                });
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error setting default heater2 circuits, exception: {0}", ex.Message));
            }
        }

        private void Config3Heater(SimulatorContext context, int name, UInt32 revision)
        {
            try
            {
                AddObject(context, revision, "H1001", name, new Dictionary<string, string>
                {
                    {StaticData.ObjectType, StaticData.HeaterType},
                    {StaticData.ListOrder, "1"},
                    {StaticData.SubType, StaticData.GenericSubtype},
                    {StaticData.SystemName, "Generic Heater"},
                    {StaticData.BodyType, "B1000"},
                    {StaticData.Share, "B1001"},
                    {StaticData.PermitType, StaticData.On},
                    {StaticData.ShowMenu, "hc"},
                    {StaticData.Delay, "120"},
                    {StaticData.Ready, StaticData.True},
                    {StaticData.Status, "INC"}
                });
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error setting default heater3 circuits, exception: {0}", ex.Message));
            }
        }

        private void Config1Pumps(SimulatorContext context, int name, UInt32 revision)
        {
            try
            {
                AddObject(context, revision, "P1000", name, new Dictionary<string, string>
                {
                    {StaticData.ObjectType, StaticData.PumpType},
                    {StaticData.ListOrder, "1"},
                    {StaticData.SubType, "SINGLE"},
                    {StaticData.SystemName, "Master Pump"},
                    {StaticData.Parent, "B1000"},
                    {StaticData.Share, "B1001"},
                    {StaticData.Status, StaticData.Off},
                    {StaticData.PermitType, StaticData.On},
                    {StaticData.Delay, "120"}
                });
                AddObject(context, revision, "C9000", name, new Dictionary<string, string>
                {
                    {StaticData.ObjectType, StaticData.CircuitType},
                    {StaticData.ListOrder, "1"},
                    {StaticData.SubType, StaticData.PumpType},
                    {StaticData.SystemName, "Simple"},
                    {StaticData.Share, "B1001"},
                    {StaticData.Status, StaticData.On},
                    {StaticData.Delay, "120"}
                });
                AddObject(context, revision, "V1000", name, new Dictionary<string, string>
                {
                    {StaticData.ObjectType, StaticData.ValveType},
                    {StaticData.ListOrder, "1"},
                    {StaticData.SubType, "SINGLE"},
                    {StaticData.SystemName, "Main Pool Valve"},
                    {StaticData.Parent, "B1000"},
                    {StaticData.Status, StaticData.Off},
                    {StaticData.PermitType, StaticData.On},
                    {StaticData.Delay, "120"}
                });
                AddObject(context, revision, "C9001", name, new Dictionary<string, string>
                {
                    {StaticData.ObjectType, StaticData.CircuitType},
                    {StaticData.ListOrder, "1"},
                    {StaticData.SubType, StaticData.ValveType},
                    {StaticData.SystemName, "Simple"},
                    {StaticData.Status, StaticData.Off},
                    {StaticData.Delay, "120"}
                });
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error setting default heater3 circuits, exception: {0}", ex.Message));
            }
        }

        private void Config1Boards(SimulatorContext context, int name, UInt32 revision)
        {
            try
            {
                AddObject(context, revision, "M1000", name, new Dictionary<string, string>
                {
                    {StaticData.ObjectType, StaticData.PanelType},
                    {StaticData.ListOrder, "1"},
                    {StaticData.SubType, StaticData.PanelType},
                    {StaticData.SystemName, "OCP1"},
                    {StaticData.HardName, "OCP1"},
                    {StaticData.Static, StaticData.On},
                    {StaticData.ObjectVersion, "0"},
                    {"VER", "1.0"}
                });
                AddObject(context, revision, "m1000", name, new Dictionary<string, string>
                {
                    {StaticData.ObjectType, StaticData.ModuleType},
                    {StaticData.ListOrder, "1"},
                    {StaticData.SubType, "I5P"},
                    {StaticData.SystemName, "I5P"},
                    {StaticData.HardName, "I5P"},
                    {StaticData.Static, StaticData.On},
                    {StaticData.Port, "1"},
                    {StaticData.Parent, "M1000"},
                    {StaticData.ObjectVersion, "0"},
                    {"VER", "1.0"}
                });
                AddObject(context, revision, "m1001", name, new Dictionary<string, string>
                {
                    {StaticData.ObjectType, StaticData.ModuleType},
                    {StaticData.ListOrder, "1"},
                    {StaticData.SubType, "I8PS"},
                    {StaticData.SystemName, "I8PS"},
                    {StaticData.HardName, "I8PS"},
                    {StaticData.Static, StaticData.On},
                    {StaticData.Port, "1"},
                    {StaticData.Parent, "M1000"},
                    {StaticData.ObjectVersion, "0"},
                    {"VER", "1.0"}
                });
                AddObject(context, revision, "m1002", name, new Dictionary<string, string>
                {
                    {StaticData.ObjectType, StaticData.ModuleType},
                    {StaticData.ListOrder, "1"},
                    {StaticData.SubType, "ANGLEXP"},
                    {StaticData.SystemName, "ANGLEXP"},
                    {StaticData.HardName, "ANGLEXP"},
                    {StaticData.Static, StaticData.On},
                    {StaticData.Port, "1"},
                    {StaticData.Parent, "M1000"},
                    {StaticData.ObjectVersion, "0"},
                    {"VER", "1.0"}
                });
                AddObject(context, revision, "r1000", name, new Dictionary<string, string>
                {
                    {StaticData.ObjectType, StaticData.RelayType},
                    {StaticData.ListOrder, "1"},
                    {StaticData.SubType, StaticData.BodyType},
                    {StaticData.SystemName, "PMP"},
                    {StaticData.HardName, "PMP"},
                    {StaticData.PanelType, "M1000"},
                    {StaticData.Parent, "m1000"},
                    {StaticData.Timeout, "1000"},
                    {StaticData.RunOn, "10:00"},
                    {StaticData.Status, StaticData.On},
                    {StaticData.Source, ""}
                });
                AddObject(context, revision, "r1001", name, new Dictionary<string, string>
                {
                    {StaticData.ObjectType, StaticData.RelayType},
                    {StaticData.ListOrder, "1"},
                    {StaticData.SubType, StaticData.ValveType},
                    {StaticData.SystemName, "VLV"},
                    {StaticData.HardName, "VLV"},
                    {StaticData.PanelType, "M1000"},
                    {StaticData.Parent, "m1000"},
                    {StaticData.Timeout, "1000"},
                    {StaticData.RunOn, "10:00"},
                    {StaticData.Status, StaticData.On},
                    {StaticData.Source, ""}
                });
                AddObject(context, revision, "r1002", name, new Dictionary<string, string>
                {
                    {StaticData.ObjectType, StaticData.RelayType},
                    {StaticData.ListOrder, "1"},
                    {StaticData.SubType, StaticData.HeaterType},
                    {StaticData.SystemName, "HTR"},
                    {StaticData.HardName, "HTR"},
                    {StaticData.PanelType, "M1000"},
                    {StaticData.Parent, "m1000"},
                    {StaticData.Timeout, "1000"},
                    {StaticData.RunOn, "10:00"},
                    {StaticData.Status, StaticData.On},
                    {StaticData.Source, ""}
                });
                AddObject(context, revision, "r1003", name, new Dictionary<string, string>
                {
                    {StaticData.ObjectType, StaticData.RelayType},
                    {StaticData.ListOrder, "1"},
                    {StaticData.SubType, StaticData.CircuitType},
                    {StaticData.SystemName, "AUX1"},
                    {StaticData.HardName, "AUX1"},
                    {StaticData.PanelType, "M1000"},
                    {StaticData.Parent, "m1000"},
                    {StaticData.Timeout, "1000"},
                    {StaticData.RunOn, "10:00"},
                    {StaticData.Status, StaticData.On},
                    {StaticData.Source, ""}
                });
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error setting default Config1Boards circuits, exception: {0}", ex.Message));
            }
        }

        private void Config2Boards(SimulatorContext context, int name, UInt32 revision)
        {
            try
            {
                AddObject(context, revision, "M1000", name, new Dictionary<string, string>
                {
                    {StaticData.ObjectType, StaticData.PanelType},
                    {StaticData.ListOrder, "1"},
                    {StaticData.SubType, StaticData.PanelType},
                    {StaticData.SystemName, "OCP1"},
                    {StaticData.HardName, "OCP1"},
                    {StaticData.Static, StaticData.On},
                    {StaticData.ObjectVersion, "0"},
                    {"VER", "1.0"}
                });
                AddObject(context, revision, "m1000", name, new Dictionary<string, string>
                {
                    {StaticData.ObjectType, StaticData.ModuleType},
                    {StaticData.ListOrder, "1"},
                    {StaticData.SubType, "I10D"},
                    {StaticData.SystemName, "I10D"},
                    {StaticData.HardName, "I10D"},
                    {StaticData.Static, StaticData.On},
                    {StaticData.Port, "1"},
                    {StaticData.Parent, "M1000"},
                    {StaticData.ObjectVersion, "0"},
                    {"VER", "1.0"}
                });
                AddObject(context, revision, "r1000",name, new Dictionary<string, string>
                {
                    {StaticData.ObjectType, StaticData.RelayType},
                    {StaticData.ListOrder, "1"},
                    {StaticData.SubType, StaticData.BodyType},
                    {StaticData.SystemName, "PMP"},
                    {StaticData.HardName, "PMP"},
                    {StaticData.PanelType, "M1000"},
                    {StaticData.Parent, "m1000"},
                    {StaticData.Timeout, "1000"},
                    {StaticData.RunOn, "10:00"},
                    {StaticData.Status, StaticData.On},
                    {StaticData.Source, ""}
                });
                AddObject(context, revision, "r1001", name, new Dictionary<string, string>
                {
                    {StaticData.ObjectType, StaticData.RelayType},
                    {StaticData.ListOrder, "1"},
                    {StaticData.SubType, StaticData.ValveType},
                    {StaticData.SystemName, "VLV"},
                    {StaticData.HardName, "VLV"},
                    {StaticData.PanelType, "M1000"},
                    {StaticData.Parent, "m1000"},
                    {StaticData.Timeout, "1000"},
                    {StaticData.RunOn, "10:00"},
                    {StaticData.Status, StaticData.On},
                    {StaticData.Source, ""}
                });
                AddObject(context, revision, "r1002", name, new Dictionary<string, string>
                {
                    {StaticData.ObjectType, StaticData.RelayType},
                    {StaticData.ListOrder, "1"},
                    {StaticData.SubType, StaticData.HeaterType},
                    {StaticData.SystemName, "HTR"},
                    {StaticData.HardName, "HTR"},
                    {StaticData.PanelType, "M1000"},
                    {StaticData.Parent, "m1000"},
                    {StaticData.Timeout, "1000"},
                    {StaticData.RunOn, "10:00"},
                    {StaticData.Status, StaticData.On},
                    {StaticData.Source, ""}
                });
                AddObject(context, revision, "r1003", name, new Dictionary<string, string>
                {
                    {StaticData.ObjectType, StaticData.RelayType},
                    {StaticData.ListOrder, "1"},
                    {StaticData.SubType, StaticData.CircuitType},
                    {StaticData.SystemName, "AUX1"},
                    {StaticData.HardName, "AUX1"},
                    {StaticData.PanelType, "M1000"},
                    {StaticData.Parent, "m1000"},
                    {StaticData.Timeout, "1000"},
                    {StaticData.RunOn, "10:00"},
                    {StaticData.Status, StaticData.On},
                    {StaticData.Source, ""}
                });
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error setting default config2Boards circuits, exception: {0}", ex.Message));
            }
        }


        private void Config1Chemistry(SimulatorContext context, int name, UInt32 revision)
        {
            try
            {
                AddObject(context, revision, "I2572", name, new Dictionary<string, string>
                {
                    {StaticData.ObjectType,StaticData.ChemistryType},
                    {StaticData.SubType, "ICHLOR"},
                    {StaticData.SystemName, "Pool Chlorinator"},
                    {StaticData.BodyType, "B1000"},
                    {StaticData.Share, "B1001"},{StaticData.Ready, StaticData.True},
                    {"SALT", "2450"},
                    {"CALC", "250"},
                    {"CYACID", "1200"}
                });
                AddObject(context, revision, "I2573", name, new Dictionary<string, string>
                {
                    {StaticData.ObjectType,StaticData.ChemistryType},
                    {StaticData.SubType, "ICHEM"},
                    {StaticData.SystemName, "Pool Chemistry"},
                    {StaticData.BodyType, "B1000"},
                    {StaticData.Share, "B1001"},
                    {StaticData.Ready, StaticData.True},
                    {"SALT", "2450"},
                    {"CALC", "250"},
                    {"CYACID", "1200"},
                    {"ALK", "70"},
                    {"PHSET", "7.5"},
                    {"PHVAL", "8.94"},
                    {"PHTNK", "70"},
                    {"ORPSET", "740"},
                    {"ORPVAL", "780"},
                    {"ORPTNK", "10"}
                });
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error setting default chemistry circuits, exception: {0}", ex.Message));
            }
        }

        private void Config1System(SimulatorContext context, int name, UInt32 revision)
        {
            try
            {
                AddObject(context, revision, "_A135", name, new Dictionary<string, string>
                {
                    {StaticData.ObjectType, StaticData.SensorType},
                    {StaticData.SubType, "AIR"},
                    {StaticData.ListOrder, "1"},
                    {"CALIB", "3.5"},
                    {StaticData.Source, "24.74"},
                    {"PROBE", "30.59"},
                    {StaticData.Status, "OK"}
                });
                AddObject(context, revision, StaticData.Location, name, new Dictionary<string, string>
                {
                    {StaticData.ObjectType, StaticData.SystemTimeType},
                    {"ACCEL", StaticData.Off},
                    {"ZIP", "60654"},
                    {"LOCX", "41.895485"},
                    {"LOCY", "87.634880"},
                    {StaticData.SystemName, "Chicago, IL"},
                    {"DLSTIM", StaticData.On},
                    {"TIMZON", "-5"},
                    {"OFFSET", "-5"},
                    {"CLK24A", "AMPM"},
                    {"SRIS", "06:15:00"},
                    {"SSET", "17:38:00"}
                });
                AddObject(context, revision, StaticData.Clock,name, new Dictionary<string, string>
                {
                    {StaticData.ObjectType, StaticData.SystemTimeType},
                    {StaticData.Source, "LOCAL"}
                });
                AddObject(context, revision, StaticData.Preferences, name, new Dictionary<string, string>
                {
                    {StaticData.ObjectType, StaticData.SystemType},
                    {"MODE", "ENGLISH"}
                });
                
                AddObject(context, revision, StaticData.FeatureMenuSingleton, name, new Dictionary<string, string>
                {
                    {StaticData.ObjectType, StaticData.MenuType},
                    {"STATIC", "ON"},
                    {"PERMIT", "BASIC"},
                    {"MANOVR", "OFF"}
                });
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error setting default sensor circuits, exception: {0}", ex.Message));
            }
        }

        private void ConfigPermit(SimulatorContext context, int name, UInt32 revision)
        {
            try
            {
                AddObject(context, revision, StaticData.Admin, name, new Dictionary<string, string>
                {
                    {StaticData.ObjectType, StaticData.PermitType},
                    {StaticData.HardName, "ADMIN"},
                    {StaticData.SystemName, "ADMIN"},
                    {"ENABLE", StaticData.Off},
                    {StaticData.ShowMenu, "abcdefhlmnprstu"}
                });
                AddObject(context, revision, StaticData.Guest, name, new Dictionary<string, string>
                {
                    {StaticData.ObjectType, StaticData.PermitType},
                    {StaticData.HardName, "GUEST"},
                    {StaticData.SystemName, "GUEST"},
                    {StaticData.ShowMenu, "abcdefhlmnprstu"}
                });
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error setting the permissions objects, exception: {0}", ex.Message));
            }
        }
        private void Config2System(SimulatorContext context, int name, UInt32 revision)
        {
            try
            {
                AddObject(context, revision, "_A135", name, new Dictionary<string, string>
                {
                    {StaticData.ObjectType, StaticData.SensorType},
                    {StaticData.SubType, "AIR"},
                    {StaticData.ListOrder, "1"},
                    {"CALIB", "3.5"},
                    {StaticData.Source, "76.54"},
                    {"PROBE", "80.04"},
                    {StaticData.Status, "OK"}
                });
                AddObject(context, revision, StaticData.Location, name, new Dictionary<string, string>
                {
                    {StaticData.ObjectType, StaticData.SystemTimeType},
                    {"ACCEL", StaticData.Off},
                    {"ZIP", "60654"},
                    {"LOCX", "41.895485"},
                    {"LOCY", "87.634880"},
                    {StaticData.SystemName, "Chicago, IL"},
                    {"DLSTIM", StaticData.On},
                    {"TIMZON", "-5"},
                    {"OFFSET", "-5"},
                    {"CLK24A", "AMPM"},
                    {"SRIS", "06:15:00"},
                    {"SSET", "17:38:00"}
                });
                AddObject(context, revision, StaticData.Clock, name, new Dictionary<string, string>
                {
                    {StaticData.ObjectType, StaticData.SystemTimeType},
                    {StaticData.Source, "NETWORK"}
                });
                AddObject(context, revision, StaticData.Preferences, name, new Dictionary<string, string>
                {
                    {StaticData.ObjectType, StaticData.SystemType},
                    {"MODE", "METRIC"}
                });
                AddObject(context, revision, StaticData.FeatureMenuSingleton, name, new Dictionary<string, string>
                {
                    {StaticData.ObjectType, StaticData.MenuType},
                    {"STATIC", "ON"},
                    {"PERMIT", "BASIC"},
                    {"MANOVR", "OFF"}
                });
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error setting default sensor circuits, exception: {0}", ex.Message));
            }
        }

        private void ConfigMasterP5(SimulatorContext context, int name, UInt32 revision)
        {
            AddModule(context, name, 1, "Main Enclosure", "i5P", 5, revision);
            AddModule(context, name, 2, "Enclosure 2", "i5X", 5, revision);
            AddModule(context, name, 3, "Enclosure 3", "i5X", 5, revision);
            AddModule(context, name, 4, "Enclosure 4", "i5X", 5, revision);
        }

        private void AddModule(SimulatorContext context, int name, int number, string panelName, string type, int auxRelays, UInt32 revision)
        {
            var parent = string.Format("m{0}000", number);
            var panel = string.Format("M{0}000", number);
            try
            {
                AddObject(context, revision, panel, name, new Dictionary<string, string>
                {
                    {StaticData.ObjectType, StaticData.PanelType},
                    {StaticData.SubType, panelName},
                    {StaticData.ListOrder, number.ToString()},
                    {StaticData.HardName, panelName},
                    {StaticData.SystemName, panelName}
                });
                AddObject(context, revision, parent, name, new Dictionary<string, string>
                {
                    {StaticData.ObjectType, StaticData.ModuleType},
                    {StaticData.SubType, type},
                    {StaticData.ListOrder, number.ToString()},
                    {StaticData.HardName, type},
                    {StaticData.SystemName, type},
                    {StaticData.Parent, panel}
                });
                context.SaveChanges();
                for (var count = 1; count <= auxRelays; count++)
                {
                    AddAuxRelay(context, name, number, count, parent, panel, revision);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error setting default sensor circuits, exception: {0}", ex.Message));
            }
        }

        private void AddAuxRelay(SimulatorContext context, int name, int number, int relayEnum, string parent, string panel, UInt32 revision)
        {
            try
            {
                var relayName = string.Format("AUX{0}", relayEnum.ToString("D2"));
                var objectName = string.Format("r{0}{1}", number, relayEnum.ToString("D3"));
                AddObject(context, revision, objectName, name, new Dictionary<string, string>
                {
                    {StaticData.ObjectType, StaticData.RelayType},
                    {StaticData.SubType, StaticData.CircuitType},
                    {StaticData.HardName, relayName},
                    {StaticData.SystemName, relayName},
                    {StaticData.PanelType, panel},
                    {StaticData.Parent, parent}
                });
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error setting default sensor circuits, exception: {0}", ex.Message));
            }
        } 
        // TODO Reorganize and add the singletons here
        private void AddSingletons(SimulatorContext context, int configuration, uint revision)
        {
            
        }

 
        /// <summary>
        /// Seed data for the default configuration
        /// </summary>
        /// <param name="context"></param>
        public void AddConfiguration(SimulatorContext context, Property property)
        {
            var revision = Globals.CurrentTime();
 
            try
            {
                switch (property.Configuration)
                {
                    case PropertyConfigurations.SharedPoolAndSpa:
                        Config1FeaturesAndSettings(context, property.LocalPropertyId, revision);
                        Config1LightCircuits(context, property.LocalPropertyId, revision);
                        Config1BodyCircuits(context, property.LocalPropertyId, revision);
                        Config2Spa(context, property.LocalPropertyId, revision);
                        Config2Pool(context, property.LocalPropertyId, revision);
                        Config3Heater(context, property.LocalPropertyId, revision);
                        Config1Pumps(context, property.LocalPropertyId, revision);
                        Config1Boards(context, property.LocalPropertyId, revision);
                        Config1System(context, property.LocalPropertyId, revision);
                        Config1Chemistry(context, property.LocalPropertyId, revision);
                        ConfigPermit(context, property.LocalPropertyId, revision);
                        break;

                    case PropertyConfigurations.SeparatePoolAndSpa:
                        Config1FeaturesAndSettings(context, property.LocalPropertyId, revision);
                        Config1LightCircuits(context, property.LocalPropertyId, revision);
                        Config1BodyCircuits(context, property.LocalPropertyId, revision);
                        Config1Spa(context, property.LocalPropertyId, revision);
                        Config1Pool(context, property.LocalPropertyId, revision);
                        Config3Heater(context, property.LocalPropertyId, revision);
                        Config1Pumps(context, property.LocalPropertyId, revision);
                        Config1Boards(context, property.LocalPropertyId, revision);
                        Config1System(context, property.LocalPropertyId, revision);
                        Config1Chemistry(context, property.LocalPropertyId, revision);
                        ConfigPermit(context, property.LocalPropertyId, revision);
                        break;
                
                    case PropertyConfigurations.PoolOnly:
                        Config1FeaturesAndSettings(context, property.LocalPropertyId, revision);
                        Config1LightCircuits(context, property.LocalPropertyId, revision);
                        Config1BodyCircuits(context, property.LocalPropertyId, revision);
                        Config1Pool(context, property.LocalPropertyId, revision);
                        Config3Heater(context, property.LocalPropertyId, revision);
                        Config1Pumps(context, property.LocalPropertyId, revision);
                        Config2Boards(context, property.LocalPropertyId, revision);
                        Config2System(context, property.LocalPropertyId, revision);
                        Config1Chemistry(context, property.LocalPropertyId, revision);
                        ConfigPermit(context, property.LocalPropertyId, revision);
                        break;

                    case PropertyConfigurations.SpaOnly:
                        Config1FeaturesAndSettings(context, property.LocalPropertyId, revision);
                        Config1LightCircuits(context, property.LocalPropertyId, revision);
                        Config1BodyCircuits(context, property.LocalPropertyId, revision);
                        Config1Spa(context, property.LocalPropertyId, revision);
                        Config3Heater(context, property.LocalPropertyId, revision);
                        Config1Pumps(context, property.LocalPropertyId, revision);
                        Config2Boards(context, property.LocalPropertyId, revision);
                        Config1System(context, property.LocalPropertyId, revision);
                        Config1Chemistry(context, property.LocalPropertyId, revision);
                        ConfigPermit(context, property.LocalPropertyId, revision);
                        break;

                    case PropertyConfigurations.SimpleBlankConfiguration:
                        ConfigMasterP5(context, property.LocalPropertyId, revision);
                        ConfigPermit(context, property.LocalPropertyId, revision);
                        Config1System(context, property.LocalPropertyId, revision);
                        break;
                    case PropertyConfigurations.ComplexBlankConfiguration:
                        ConfigMasterP5(context, property.LocalPropertyId, revision);
                        ConfigPermit(context, property.LocalPropertyId, revision);
                        Config1System(context, property.LocalPropertyId, revision);
                        break;
                    default:
                        foreach (var o in _config)
                        {
                            context.ControlObjects.Add(new ControlObject()
                            {
                                INSTALLATION = property.LocalPropertyId,
                                OBJNAM = o.OBJNAM,
                                Key = o.Key,
                                Value = o.Value,
                                Revision = o.Revision
                            });
                        }
                        context.SaveChanges();
                        break;
                }
            }
            catch (Exception ex)
            {
                var message = string.Format("{0}", ex.Message);
                MessageBox.Show(message, "Error setting default Configuration Data", MessageBoxButton.OK);
            }
        }

        private void AddObject(SimulatorContext context, UInt32 revision, string Name, int Configuration, Dictionary<string, string> parameters)
        {
            foreach (var parameter in parameters)
            {
                context.ControlObjects.Add(new ControlObject
                {
                    INSTALLATION = Configuration,
                    OBJNAM = Name,
                    Key = parameter.Key,
                    Value = parameter.Value,
                    Revision = revision
                });
            }
        }
        #endregion

        #region Private Methods
        private IEnumerable<object> systemDataset(SimulatorContext data, int configId)
        {
            var system = (from c in data.ControlObjects
                          where (c.Key == StaticData.ObjectType && c.Value == StaticData.SystemType) ||
                                (c.Key == StaticData.ObjectType && c.Value == StaticData.SystemTimeType) ||
                                (c.Key == StaticData.ObjectType && c.Value == StaticData.SensorType) ||
                                (c.Key == StaticData.ObjectType && c.Value == StaticData.PermitType)
                          select c.OBJNAM).ToList();

            var o = (from c in data.ControlObjects
                    where system.Contains(c.OBJNAM) &&
                          c.INSTALLATION == configId
                    select c).ToList();
            return o;
        }

        private IEnumerable<object> circuitDataset(SimulatorContext data, int configId)
        {
            var circuits = (from c in data.ControlObjects
                            where c.Key == StaticData.ObjectType && c.Value == StaticData.CircuitType
                            select c.OBJNAM).ToList();

            return (from c in data.ControlObjects
                    where circuits.Contains(c.OBJNAM) &&
                          c.INSTALLATION == configId
                    select c).ToList();
        }

        private IEnumerable<object> bodiesDataset(SimulatorContext data, int configId)
        {
            var bodies = (from c in data.ControlObjects
                          where c.Key == StaticData.ObjectType && c.Value == StaticData.BodyType
                          select c.OBJNAM).ToList();

            return (from c in data.ControlObjects
                    where bodies.Contains(c.OBJNAM) &&
                          c.INSTALLATION == configId
                    select c).ToList();
        }

        private IEnumerable<object> heatersDataset(SimulatorContext data, int configId)
        {
            var heaters = (from c in data.ControlObjects
                           where (c.Key == StaticData.ObjectType && c.Value == StaticData.HeaterType) ||
                                 (c.Key == StaticData.ObjectType && c.Value == StaticData.HeatComboType)
                           select c.OBJNAM).ToList();

            return (from c in data.ControlObjects
                    where heaters.Contains(c.OBJNAM) &&
                          c.INSTALLATION == configId
                    select c).ToList();
        }

        private IEnumerable<object> actuatorsDataset(SimulatorContext data, int configId)
        {
            var actuators = (from c in data.ControlObjects
                             where (c.Key == StaticData.ObjectType && c.Value == StaticData.PumpType) ||
                                   (c.Key == StaticData.ObjectType && c.Value == StaticData.ValveType)
                             select c.OBJNAM).ToList();

            return (from c in data.ControlObjects
                    where actuators.Contains(c.OBJNAM) &&
                          c.INSTALLATION == configId
                    select c).ToList();
        }

        private IEnumerable<object> schedulesDataset(SimulatorContext data, int configId)
        {
            var schedules = (from c in data.ControlObjects
                             where c.Key == StaticData.ObjectType && c.Value == StaticData.ScheduleType
                             select c.OBJNAM).ToList();

            return (from c in data.ControlObjects
                    where schedules.Contains(c.OBJNAM) &&
                          c.INSTALLATION == configId
                    select c).ToList();
        }

        private IEnumerable<object> hardwareDataset(SimulatorContext data, int configId)
        {
            var hardware = (from c in data.ControlObjects
                            where (c.Key == StaticData.ObjectType && c.Value == StaticData.PanelType) ||
                                  (c.Key == StaticData.ObjectType && c.Value == StaticData.ModuleType) ||
                                  (c.Key == StaticData.ObjectType && c.Value == StaticData.RelayType)
                            select c.OBJNAM).ToList();

            return (from c in data.ControlObjects
                    where hardware.Contains(c.OBJNAM) &&
                          c.INSTALLATION == configId
                    select c).ToList();
        }

        private IEnumerable<object> chemistryDataset(SimulatorContext data, int configId)
        {
            var chemicals = (from c in data.ControlObjects
                             where c.Key == StaticData.ObjectType && c.Value == StaticData.ChemistryType
                             select c.OBJNAM).ToList();

            return (from c in data.ControlObjects
                    where chemicals.Contains(c.OBJNAM) &&
                          c.INSTALLATION == configId
                    select c).ToList();
        }

        #endregion
    }
}
