Feature: View Status (PEN-146)
As a Pool Owner
I want to see the status of equipmenmt in my system
So That I have a good idea of what's going on with my pool

  Background user is connected to a pool system
  And user is viewing pool details

Scenario: ON OFF DISPLAY
  * elements in the list which have an on or off status display that status

Scenario: OTHER DISPLAY
  * elements in the list which have other "status" to display will be displayed
  * temperature readings will be displayed 
  * heat mode will be displayed

Scenario: CHANGES DISPLAY
  When the status of a pool element changes 
  Then that change will be reflected in the app's display