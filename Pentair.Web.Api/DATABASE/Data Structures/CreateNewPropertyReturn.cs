﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pentair.Web.Api
{
    public class CreateNewPropertyReturn
    {
        public int HttpStatusCode { get; set; }
        public int InstallationId { get; set; }
        public string NewSecret { get; set; }

        public CreateNewPropertyReturn()
        {
            this.HttpStatusCode = -1;
            this.InstallationId = -1;
            this.NewSecret = null;

        } // CreateNewPropertyReturn()

    } // class CreateNewPropertyReturn
}