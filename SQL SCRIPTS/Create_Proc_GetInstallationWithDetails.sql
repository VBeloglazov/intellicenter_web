﻿-- =============================================================
--
-- Author:		Vladimir Beloglazov
-- Create date: 04/04/2018
--
-- Creates:
--
--		Table Type:			None
--
--		Stored Procedure:	PGetInstallationWithDetails
--
-- =============================================================

USE PathfinderIntelliCenter
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (
	SELECT * FROM sys.objects
	WHERE object_id = OBJECT_ID(N'dbo.PGetInstallationWithDetails')
	AND type in (N'P', N'PC')
)
DROP PROCEDURE dbo.PGetInstallationWithDetails
GO

-- -------------------------------------------------------------
--
-- Returns [Installations], Connections, [ControlObjects] record field values.
--
-- -------------------------------------------------------------
CREATE PROCEDURE dbo.PGetInstallationWithDetails
     @installationId AS int
AS
BEGIN

	SET NOCOUNT OFF

	-- Result set
	SELECT
		 I.*
		,(SELECT CASE WHEN EXISTS (SELECT 1 FROM [Connections] WHERE InstallationId = I.InstallationId AND UserId = CAST(0x0 AS uniqueidentifier)) THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END) [IsConnected]
		,(SELECT TOP 1 Value FROM [ControlObjects] WHERE INSTALLATION = I.InstallationId AND OBJNAM = '_5451' AND [Key] = 'ADDRESS' ORDER BY Revision DESC) [Address]
		,(SELECT TOP 1 Value FROM [ControlObjects] WHERE INSTALLATION = I.InstallationId AND OBJNAM = '_5451' AND [Key] = 'CITY'    ORDER BY Revision DESC) [City]
		,(SELECT TOP 1 Value FROM [ControlObjects] WHERE INSTALLATION = I.InstallationId AND OBJNAM = '_5451' AND [Key] = 'STATE'	ORDER BY Revision DESC) [State]
		,(SELECT TOP 1 Value FROM [ControlObjects] WHERE INSTALLATION = I.InstallationId AND OBJNAM = '_5451' AND [Key] = 'ZIP'     ORDER BY Revision DESC) [Zip]
		,(SELECT TOP 1 Value FROM [ControlObjects] WHERE INSTALLATION = I.InstallationId AND OBJNAM = '_5451' AND [Key] = 'COUNTRY' ORDER BY Revision DESC) [Country]
		,(SELECT TOP 1 Value FROM [ControlObjects] WHERE INSTALLATION = I.InstallationId AND OBJNAM = '_5451' AND [Key] = 'NAME'    ORDER BY Revision DESC) [OwnerName]
		,(SELECT TOP 1 Value FROM [ControlObjects] WHERE INSTALLATION = I.InstallationId AND OBJNAM = '_5451' AND [Key] = 'PHONE'   ORDER BY Revision DESC) [Phone]
		,(SELECT TOP 1 Value FROM [ControlObjects] WHERE INSTALLATION = I.InstallationId AND OBJNAM = '_5451' AND [Key] = 'EMAIL'   ORDER BY Revision DESC) [Email]
	FROM dbo.Installations I
	WHERE I.InstallationId = @installationId
                                                     
END
GO
