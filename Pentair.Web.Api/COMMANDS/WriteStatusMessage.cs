﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Pentair.Web.Api
{
    public class WriteStatusMessage : ExtendedParamListResponse
    {
        public WriteStatusMessage(
            string messageID,
            string response,
            string timeNow,
            string timeSince,
            List<ExtendedParamObject> objectList
            )
            : base(CommandString.WriteStatusMessage, messageID, response, timeNow, timeSince, objectList)
        {
        } // WriteStatusMessage()

        /// <summary>
        /// Always returns an object, valid or not. If invalid, check the DeserializationError property value.
        /// </summary>
        public static new WriteStatusMessage Deserialize(
            string serializedMessage)
        {
            WriteStatusMessage command = null;

            try
            {
                command = JsonConvert.DeserializeObject<WriteStatusMessage>(serializedMessage);
                command.IsValid = true;
            }
            catch (Exception e)
            {
                // Deserialization as WriteStatusMessage failed

                StringBuilder sbError = new StringBuilder();

                sbError.Append("Cannot deserialize as WriteStatusMessage. " + e.Message);

                // Try to get at least the base message data
                ExtendedParamListResponse baseCommand = ExtendedParamListResponse.Deserialize(serializedMessage);

                if (!baseCommand.IsValid)
                {
                    sbError.Append("\n" + baseCommand.DeserializationError);
                }

                command = new WriteStatusMessage(
                    messageID: baseCommand.MessageID,
                    response: ResponseString.BadRequest,
                    timeNow: baseCommand.TimeNow,
                    timeSince: baseCommand.TimeSince,
                    objectList: baseCommand.ObjectList)
                {
                    IsValid = false,
                    DeserializationError = sbError.ToString()
                };
            }

            return command;

        } // Deserialize()

    } // class WriteStatusMessage
}
