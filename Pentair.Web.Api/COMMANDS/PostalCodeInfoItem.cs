﻿using Newtonsoft.Json;

namespace Pentair.Web.Api
{
    public class PostalCodeInfoItem
    {
        [JsonProperty("Country", Required = Required.Always)]
        public string Country { get; private set; }

        [JsonProperty("City", Required = Required.Always)]
        public string City { get; private set; }

        [JsonProperty("State", Required = Required.Always)]
        public string State { get; private set; }

        [JsonProperty("TimeZone", Required = Required.Always)]
        public string TimeZone { get; private set; }

        [JsonProperty("UTC", Required = Required.Always)]
        public decimal UTC { get; private set; }

        [JsonProperty("DST", Required = Required.Always)]
        public string DST { get; private set; }

        [JsonProperty("Latitude", Required = Required.Always)]
        public decimal Latitude { get; private set; }

        [JsonProperty("Longitude", Required = Required.Always)]
        public decimal Longitude { get; private set; }

        [JsonProperty("PostalCode", Required = Required.Always)]
        public string PostalCode{ get; private set; }

        public PostalCodeInfoItem(
            string country,
            string city,
            string state,
            string timeZone,
            decimal utc,
            string dst,
            decimal latitude,
            decimal longitude,
            string postalCode)
        {
            this.Country = country;
            this.City = city;
            this.State = state;
            this.TimeZone = timeZone;
            this.UTC = utc;
            this.DST = dst;
            this.Latitude = latitude;
            this.Longitude = longitude;
            this.PostalCode = postalCode;
        }

    } // class HistoryItem
}
