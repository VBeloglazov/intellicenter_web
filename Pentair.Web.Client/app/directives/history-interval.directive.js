'use strict';
var PWC = PWC || {};
PWC.directive('historyInterval', function($parse) {

  function drawInterval(scope, element) {
    var uniqueId = 'a' + Math.uuid(15); // id's can't start with numbers
    // remove existing content
    element.html('');
    element.append('<div id="' + uniqueId + '"></div>');
    d3.select('#' + uniqueId).attr('class', 'history-chart');
    var historyAnchor = d3.select("#" + uniqueId);
    var svg = historyAnchor.append("svg")
      .attr("class", "line-interval").attr("width", 988).attr("height", 20);
    var margin = {
      top: 0,
      right: 15,
      bottom: 15,
      left: 15
    };
    var parseDate = d3.time.format("%d-%b-%y").parse;
    var width = svg.attr("width") - margin.left - margin.right;
    var height = svg.attr("height") - margin.top - margin.bottom;
    var g = svg.append("g").attr("transform", "translate(" + margin.left + "," + margin.top + ")");
    var timeFormat = d3.time.format("%Y%m%d");
    var x = d3.time.scale().range([0, width]);
    x.domain(scope.xAxis.domain);

    var xAxis = d3.svg.axis();
    xAxis.scale(x);
    xAxis.orient("bottom");
    xAxis.innerTickSize(-height);
    xAxis.outerTickSize(0);
    xAxis.ticks(scope.xAxis.ticks.timeScale, scope.xAxis.ticks.timeInterval);

    g.append("g")
      .attr("class", "axis axis--x")
      .attr("transform", "translate(0," + height + ")")
      .call(xAxis);
      // .selectAll("text")  
      //       .style("text-anchor", "end")
      //       .attr("dx", "-.8em")
      //       .attr("dy", ".15em")
      //       .attr("transform", function(d) {
      //           return "rotate(-30)" 
      //           });
  }

  var directiveDefinitionObject;
  directiveDefinitionObject = {
    restrict: 'E',
    replace: false,
    scope: {
      xAxis: '=chartXAxis'
    },
    link: function (scope, element, attrs) {
      scope.$watch("xAxis", function (newValue, oldValue) {
        drawInterval(scope, element);
      }, true);
    }
  };

  return directiveDefinitionObject;
});
