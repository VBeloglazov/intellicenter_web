﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Pentair.Domain.Client.Helpers.Commands
{
    public class ResponseCommand : CommandBase
    {
        [JsonProperty("response", Required = Required.Default, Order = 10)]
        public string Response { get; private set; }

        [JsonIgnore]
        public Guid TargetConnectionId { get; set; }

        [JsonIgnore]
        public bool IsOriginatedByApi { get; private set; }

        public ResponseCommand(
            string command,
            string messageID,
            string response
            )
            : base(command, messageID)
        {
            this.Response = response;

        } // ResponseCommand()

        public bool TrimMessageId()
        {
            if (this.MessageID != null)
            {
                string[] ids = this.MessageID.Split(new char[] { ':' });

                this.MessageID = ids[0];
                this.TargetConnectionId = (ids.Length > 1) ? new Guid(ids[1]) : Guid.Empty;

                return true;
            }
            else
            {
                this.TargetConnectionId = Guid.Empty;
            }

            return false;

        } // TrimMessageId()

        /// <exception cref="Exception" />
        public static ResponseCommand ResponseCommandFactory(
            CommandType commandType,
            string serializedMessage)
        {
            ResponseCommand responseCommand;

            switch (commandType)
            {
                case CommandType.GetParamListResponse:
                    responseCommand = GetParamListResponse.Deserialize(serializedMessage);
                    break;

                case CommandType.SendQuery:
                    responseCommand = GetQueryResponse.Deserialize(serializedMessage);
                    break;

                case CommandType.SuccessMessage:
                    responseCommand = SuccessCommandResponse.Deserialize(serializedMessage);
                    break;

                case CommandType.ObjectCreated:
                    responseCommand = ObjectCreatedResponse.Deserialize(serializedMessage);
                    break;

                case CommandType.SetCommand:
                    responseCommand = SetCommandResponse.Deserialize(serializedMessage);
                    break;

                case CommandType.SetParamList:
                    responseCommand = SetParamListResponse.Deserialize(serializedMessage);
                    break;

                case CommandType.ClearParam:
                    responseCommand = ClearParamResponse.Deserialize(serializedMessage);
                    break;

                case CommandType.ReleaseParamList:
                    responseCommand = ReleaseParamListResponse.Deserialize(serializedMessage);
                    break;

                case CommandType.WriteStatusMessage:
                    responseCommand = WriteStatusMessage.Deserialize(serializedMessage);
                    break;

                default:
                    throw new Exception("Unsupported response type: " + commandType);

            } // switch (commandType)

            if (!responseCommand.TrimMessageId())
            {
                throw new Exception(
                    String.Format("{0} doesn't have TargetConnectionId", responseCommand.GetType().Name)
                );
            }

            // See WebSocketClient_Client
            responseCommand.IsOriginatedByApi = Guid.Parse(responseCommand.MessageID).Equals(Guid.Empty);

            return responseCommand;

        } // ResponseCommandFactory()

        /// <summary>
        /// Always returns an object, valid or not. If invalid, check the DeserializationError property value.
        /// </summary>
        public static new ResponseCommand Deserialize(
            string serializedMessage)
        {
            ResponseCommand command = null;

            try
            {
                command = JsonConvert.DeserializeObject<ResponseCommand>(serializedMessage);
                command.IsValid = true;
            }
            catch (Exception e)
            {
                // Deserialization as ResponseCommand failed

                StringBuilder sbError = new StringBuilder();

                sbError.Append("Cannot deserialize as ResponseCommand. " + e.Message);

                // Try to get at least the base message data
                CommandBase commandBase = CommandBase.Deserialize(serializedMessage);

                if (!commandBase.IsValid)
                {
                    sbError.Append("\n" + commandBase.DeserializationError);
                }

                command = new ResponseCommand(
                    commandBase.Command,
                    commandBase.MessageID,
                    response: String.Empty)
                {
                    IsValid = false,
                    DeserializationError = sbError.ToString()
                };
            }

            return command;

        } // Deserialize()

    } // class ResponseCommand
}
