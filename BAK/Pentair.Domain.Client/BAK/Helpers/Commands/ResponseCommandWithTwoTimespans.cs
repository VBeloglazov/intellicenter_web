﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Pentair.Domain.Client.Helpers.Commands
{
    public class ResponseCommandWithTwoTimespans : ResponseCommand
    {
        [JsonProperty("timeNow", Required = Required.Default)]
        public string TimeNow { get; private set; }

        [JsonProperty("timeSince", Required = Required.Default)]
        public string TimeSince { get; private set; }

        public ResponseCommandWithTwoTimespans(
            string command,
            string messageID,
            string response,
            string timeNow,
            string timeSince
            )
            : base(command, messageID, response)
        {
            this.TimeNow = timeNow;
            this.TimeSince = timeSince;

        } // ResponseCommandWithTwoTimespans()

        /// <summary>
        /// Always returns an object, valid or not. If invalid, check the DeserializationError property value.
        /// </summary>
        public static new ResponseCommandWithTwoTimespans Deserialize(
            string serializedMessage)
        {
            ResponseCommandWithTwoTimespans command = null;

            try
            {
                command = JsonConvert.DeserializeObject<ResponseCommandWithTwoTimespans>(serializedMessage);
                command.IsValid = true;
            }
            catch (Exception e)
            {
                // Deserialization as ResponseCommandWithTwoTimespans failed

                StringBuilder sbError = new StringBuilder();

                sbError.Append("Cannot deserialize as ResponseCommandWithTwoTimespans. " + e.Message);

                // Try to get at least the base message data
                ResponseCommand baseCommand = ResponseCommand.Deserialize(serializedMessage);

                if (!baseCommand.IsValid)
                {
                    sbError.Append("\n" + baseCommand.DeserializationError);
                }

                command = new ResponseCommandWithTwoTimespans(
                    command: baseCommand.Command,
                    messageID: baseCommand.MessageID,
                    response: ResponseString.BadRequest,
                    timeNow: String.Empty,
                    timeSince: String.Empty)
                {
                    IsValid = false,
                    DeserializationError = sbError.ToString()
                };
            }

            return command;

        } // Deserialize()

    } // class ResponseCommandWithTwoTimespans
}
