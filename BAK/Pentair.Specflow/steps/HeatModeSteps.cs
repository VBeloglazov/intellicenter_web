﻿using System;
using TechTalk.SpecFlow;

namespace Pentair.Specflow.steps
{
    [Binding]
    public class HeatModeSteps
    {
        [When(@"the user selects a heat mode")]
        public void WhenTheUserSelectsAHeatMode()
        {
            ScenarioContext.Current.Pending();
        }

        [When(@"the heat mode is changed at the pool system")]
        public void WhenTheHeatModeIsChangedAtThePoolSystem()
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"the appropriate message is sent to the pool system")]
        public void ThenTheAppropriateMessageIsSentToThePoolSystem()
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"app display reflects the user's choice")]
        public void ThenAppDisplayReflectsTheUserSChoice()
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"that change will be reflected in the app's display")]
        public void ThenThatChangeWillBeReflectedInTheAppSDisplay()
        {
            ScenarioContext.Current.Pending();
        }
    }
}
