﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Pentair.Domain.Client.Helpers.Commands
{
    public class ParamCommandResponse : ResponseCommand
    {
        [JsonProperty("objectList", Required = Required.Default, Order = 100)]
        public List<ParamObject> ObjectList { get; private set; }

        public ParamCommandResponse(
            string command,
            string messageID,
            string response,
            List<ParamObject> objectList
            )
            : base(command, messageID, response)
        {
            this.ObjectList = objectList;

        } // ParamCommandResponse()

        /// <summary>
        /// Always returns an object, valid or not. If invalid, check the DeserializationError property value.
        /// </summary>
        public static new ParamCommandResponse Deserialize(
            string serializedMessage)
        {
            ParamCommandResponse command = null;

            try
            {
                command = JsonConvert.DeserializeObject<ParamCommandResponse>(serializedMessage);
                command.IsValid = true;
            }
            catch (Exception e)
            {
                // Deserialization as ParamCommandResponse failed

                StringBuilder sbError = new StringBuilder();

                sbError.Append("Cannot deserialize as ParamCommandResponse. " + e.Message);

                // Try to get at least the base message data
                ResponseCommand baseCommand = ResponseCommand.Deserialize(serializedMessage);

                if (!baseCommand.IsValid)
                {
                    sbError.Append("\n" + baseCommand.DeserializationError);
                }

                command = new ParamCommandResponse(
                    command: baseCommand.Command,
                    messageID: baseCommand.MessageID,
                    response: ResponseString.BadRequest,
                    objectList: null)
                {
                    IsValid = false,
                    DeserializationError = sbError.ToString()
                };
            }

            return command;

        } // Deserialize()
    
    } // class ParamCommandResponse
}