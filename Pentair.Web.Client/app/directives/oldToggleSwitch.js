'use strict';
var PWC = PWC || {};

///DEPRECATE!
PWC.directive('oldToggleSwitch', function () {
    return {
        restrict: 'EA',
        replace: true,
        scope: {
            toggleModel: '=',
            disabled: '@',
            toggleOnclick: '&'
        },
        template: '<div class="switch-animate {{value}}"></div>',
        link: function (scope, element, attrs) {
            if (scope.toggleModel === undefined)
                return;

            if (!attrs.disabled) {
                attrs.disabled = false;
            }

            scope.value = getClassName();

            element.on('click', function () {
                if (!scope.disabled) {
                    scope.toggleModel = scope.toggleModel === 'ON' ? 'OFF' : 'ON';
                    var result = { value: scope.toggleModel };
                    scope.toggleOnclick(result);
                    scope.$apply();

                }
            });

            //Update UI Value when model Changes
            scope.$watch('toggleModel', function (newVal, oldVal) {
                if (newVal && newVal !== oldVal)
                    scope.value = getClassName();
            });


            function getClassName() {
                return (scope.toggleModel === 'ON') ? "switch-on" : "switch-off";
            }

        }
    };
});
