'use strict';
var PWC = PWC || {};
PWC.controller('ScheduleController', [
  '$scope', '$location', '$filter', 'PropertyService', 'AuthService', 'Enumerations', 'PropertyModel', 'Notification',
  function ($scope, $location, $filter, propertyService, authService, enumerations, propertyModel, Notification) {
      var stopTimePristine = true;
      $scope.InitialTimeFormat = getTimeFormat;
      $scope.Model = propertyModel;
      $scope.Property = propertyService;
      $scope.systemSettings = $scope.Model.Objects._5451;
      $scope.Authentication = authService;
      $scope.Enumerations = enumerations; 
      $scope.ProgrammableCircuits = [];
      $scope.HeatModes = [];
      $scope.Schedules = []; //     propertyModel.Schedules;      
      $scope.SelectedSchedule = null;
      $scope.UoM = 'C';
      $scope.dontchange = "Don't Change";
      $scope.HoldHeater = "00001";
      $scope.minTemp = 0;
      $scope.maxTemp = 105;
      $scope.maxSchedules = 100;
      $scope.ConfirmDelete = false;
      $scope.sysPref = propertyModel.Objects._5451;
      $scope.securityEnabled = propertyModel.SecurityEnabled;
      $scope.SHOMNU = propertyModel.ACL.SHOMNU;
      $scope.SelectedIndex = 0;
      $scope.timeModes = enumerations.timeModes;

      // REQUEST NOTIFICATION ON CHANGED.
      //$scope.Property.GetSchedules();

      init();

   
      $scope.$on('SchedulesUpdated', function (event, message) {
          console.log(">>> ScheduleController - $scope.$on('SchedulesUpdated ");
          //refreshList();
      });

      $scope.$on('Schedules_UPDATES', function (event, message) {
          console.log(">>> BROADCAST SERVING NORMAL Schedules_UPDATES");
          $scope.updatingSchedules();
          refreshList();          
      });

      $scope.updatingSchedules = function () {
          console.log("DELAY 4 3000")
          Notification.success({ message: 'Updating Schedules!', delay: 3000 });
      };

      $scope.$on('SchedulesRequestParam', function (event, message) {        
          console.log(">>> ScheduleController - register Schedule for status (ACT) change : ", message);
          //$scope.Property.registerScheduleItem(message);
      });    

      $scope.errorSave = function (errorMessage) {
          console.log("DELAY 1 1000")
          Notification.error({message: 'Failed!' + ' ' + errorMessage, delay: 1000});
      };

      $scope.successSave = function () {
          console.log("DELAY 2 1000")
          Notification.success({message: 'Saved!', delay: 1000});
      };

      $scope.successCancel = function () {
          console.log("DELAY 3 1000")
          Notification.success({message: 'Cancelled!', delay: 1000});
      };

      $scope.AddSchedule = function () {
          $scope.SelectedSchedule = newSchedule();
          console.log("-------------------------- $scope.Schedules.push(newSchedule")
          $scope.Schedules.push($scope.SelectedSchedule);
          stopTimePristine = true;
          $scope.checkDefaults($scope.SelectedSchedule);
      };
      // return how many schedules are free 
      $scope.getSchedulesFree = function () {        
          var free = $scope.maxSchedules - $scope.Schedules.length;
          return free;        
      };
      // check can add more schedules
      $scope.canAddSchedules = function () {
          var free = $scope.maxSchedules - $scope.Schedules.length;
          if (free > 0)
              return true;
          else
              return false;
      };

      $scope.getSchedControlID = function (id, index) {
          var value = 1;
          switch (id) {
              case 'OCCURANCE': value = 3000 + index; break;
              case 'ONETIME':   value = 3100 + index; break;
              case 'REPEAT':    value = 3200 + index; break;
              case 'STARTTIME': value = 3300 + index; break;
              case 'STOPTIME':  value = 3400 + index; break;
              case 'DONTSTOP': value = 3500 + index; break;
              case 'DAYONCE': value = 3600 + index; break;
              case 'DAYREPEATS': value = 3700 + index; break;                  
              default:                  
                  value = 1;
                  break;
                  
          }          
          return value;
      }


      $scope.changeScheduleDay = function (schedule, range) {
          switch (range) {
              case 'weekends':
                  schedule.days = ['A', 'U'];
                  break;
              case 'weekdays':
                  schedule.days = ['M', 'T', 'W', 'R', 'F'];
                  break;
              case 'every day':
                  schedule.days = ['M', 'T', 'W', 'R', 'F', 'A', 'U'];
                  break;
              case 'clear':
                  schedule.days = [];
                  break;
          }
      };

      $scope.SetEditMode = function (schedule) {
          if (schedule) {
              var obj = propertyModel.Objects[schedule.CIRCUIT];
              if (!obj) {
                  obj = newSchedule();
              }
              $scope.SelectedSchedule = schedule;
              $scope.SelectedSchedule.bodyObject = {};
              $scope.SelectedSchedule.body = false;
              $scope.SelectedSchedule.smartStart = false;
              // check for type circuit fitler
              //if (obj.SUBTYP === 'POOL' || obj.SUBTYP === 'SPA') {
              if (obj.HNAME === 'Pool' || obj.HNAME === 'Spa') {
                  $scope.SelectedSchedule.bodyObject = $filter('objectsByParamValue')(propertyModel.Bodies, ['BODY', 'SHARE'], obj.BODY)[0];
                  $scope.SelectedSchedule.body = true;
          
                  $scope.checkDefaults(schedule);

                  if (schedule.HEATER && schedule.HEATER !== '00000' && schedule.HEATER !== 'HOLD') {
                      var heater = $filter('objectsByName')(propertyModel.Heaters, schedule.HEATER);
                      //$scope.SelectedSchedule.heatMode = propertyModel.Objects[schedule.HEATER].SNAME;
                      $scope.SelectedSchedule.heatMode = propertyModel.Objects[schedule.HEATER].DISPLAY;
                      schedule.coolingCapable = $filter('coolingCapableHeater')(heater);
                      schedule.heatingCapable = $filter('heatingCapable')(heater);
                  }
              }
              if (obj.USAGE && obj.USAGE !== 'USAGE') {
                  $scope.SelectedSchedule.smartStart = obj.USAGE.indexOf('E') > -1;
              }
              stopTimePristine = false;
          }
      };

      $scope.ExpandItem = function (event, schedule, index) {
          if (schedule != null) {
              $scope.SelectedSchedule = schedule;
              $scope.SelectedIndex = index;
        
              $scope.SetEditMode(schedule);
              $scope.Schedules.forEach(function (s) {
                  if (s.OBJNAM === schedule.OBJNAM) {
                      s.open = !s.open;
                  }
              });

              $scope.checkDefaults(schedule);
          }
          event.stopPropagation();
      };

      $scope.getCircuitPerID = function(circuitID){
          var circuit
          angular.forEach($scope.Model.Circuits, function (item) {
              if (item.OBJNAM == circuitID) {
                  circuit = item;
              }
          });
          return circuit;        
      };


      $scope.getCircuitPerBody = function (circuitID) {
          var circuit
          angular.forEach($scope.Model.Bodies, function (item) {
              if (item.OBJNAM == circuitID) {
                  circuit = item;
              }
          });
          return circuit;
      };

      $scope.AddToHeatMode = function (insert) {
          var present = false;
          angular.forEach($scope.HeatModes, function (item) {
              if (item.MODE === insert.MODE)
                  present = true
          });

          if (!present)
              $scope.HeatModes.push(insert);            
      }

      $scope.setHeatModesAvailable = function () {

          var comboItem;
          var comboPresent = false;
          angular.forEach($scope.HeatModes, function (item) {
              switch (item.OBJNAM) {
                  case '00000':
                      insertOff = false;
                      item.ORDER = 1;
                      break;
                  case 'HOLD':
                      insertDont = false;
                      item.ORDER = 0;                    
                      break;
                  case 'HXSLR':                    
                      item.DISPLAY = "Solar Preferred";
                      item.MODE = '4';
                      item.ORDER = 4;
                      break;
                  case 'HXHTP':
                      item.DISPLAY = "Heat Pump Preferred";
                      item.MODE = '6';
                      break;

                  case 'HXULT':
                      item.DISPLAY = "UltraTemp Preferred";
                      item.MODE = '6';
                      break;
              }

              switch (item.SUBTYP) {
                  case 'GENERIC':
                      item.DISPLAY = "Heater";
                      item.MODE = '2';
                      break;

                  case 'SOLAR':
                      item.DISPLAY = "Solar Only";
                      item.MODE = '3';
                      item.ORDER = 3;
                      break;

                  case 'ULTRA':
                      item.DISPLAY = "UltraTemp Only";
                      item.MODE = '5';
                      item.ORDER = 5;
                      break;

                  case 'HTPMP':
                      item.DISPLAY = "Heat Pump Only";
                      item.MODE = '5';
                      item.ORDER = 5;
                      break;
                  case 'MASTER':
                      item.DISPLAY = "MasterTemp";
                      item.MODE = '11';
                      item.ORDER = 7;
                      break;
                  case 'MAX':
                      item.DISPLAY = "Max-E-Therm";
                      item.MODE = '12';
                      item.ORDER = 8;
                      break;
                  case "HCOMBO":
                      item.DISPLAY = "Hybrid-Gas Only Mode"
                      item.MODE = '7';
                      item.ORDER = 9;
                      comboPresent = true;
                      comboItem = angular.copy(item);                    
                      break;

              }
          });
    
          if (comboPresent) {
              var item2 = angular.copy(comboItem);
              var item3 = angular.copy(comboItem);
              var item4 = angular.copy(comboItem);

              item2.DISPLAY = "Hybrid-Heat Pump Only Mode"
              item2.MODE = '8';
              item2.ORDER = 10;
              $scope.AddToHeatMode(item2);

              item3.DISPLAY = "Hybrid-Hybrid Mode"
              item3.MODE = '9';
              item3.ORDER = 11;
              $scope.AddToHeatMode(item3);

              item4.DISPLAY = "Hybrid-Dual Mode"
              item4.MODE = '10';
              item4.ORDER = 12;
              $scope.AddToHeatMode(item4);
          }


      }

      $scope.checkDefaults = function (schedule) {    // checking defaults values
          if (schedule != null) {
              $scope.HeatModes = [];
              var selCir = schedule.CIRCUIT;
              if (selCir === "") // select by default AUX 1
                  selCir = "C0002"

              var circuit = $scope.getCircuitPerID(selCir);
              angular.forEach(propertyModel.Heaters, function (item) {
                
                  var bodyCircuit = $scope.getCircuitPerBody(item.BODY);
                  item.MODE = "0";
                  if (
                      (circuit.SUBTYP == bodyCircuit.SUBTYP) ||       // POOL = POOL  or SPA = SPA
                      (item.BODY === "B1101 B1202") ||                // shared body  case 1
                      (item.BODY === "B1202 B1101")  )                // shared body  case 2
                      $scope.HeatModes.push(item);
              });
    
          } else {
              $scope.HeatModes = propertyModel.Heaters;
          }
          var insertOff = true;
          var insertDont = true;           

          if (propertyModel.supportConnectedHeaters()) {
              $scope.setHeatModesAvailable();
          } else {
              angular.forEach($scope.HeatModes, function (item) {
                  if (item.OBJNAM === '00000') {
                      insertOff = false;
                  }

                  if (item.OBJNAM === 'HOLD') {
                      insertDont = false;
                  }
              });
          }

          var _objnam = 'HOLD';
          var _sname = $scope.dontchange;

          if (insertDont === true)
              $scope.HeatModes.unshift({ SNAME: _sname, OBJNAM: _objnam, DISPLAY: _sname, MODE:'0', ORDER:0 });               

          if (insertOff === true)
              $scope.HeatModes.unshift({ SNAME: 'OFF', OBJNAM: '00000', DISPLAY: 'OFF', MODE:'1', ORDER:1 });
        

          if (propertyModel.supportConnectedHeaters()) {
              $scope.HeatModes.sort(function (a, b) {
                  var keyA = a.ORDER,
                      keyB = b.ORDER;
                  if (keyA < keyB) return -1;
                  if (keyA > keyB) return 1;
                  return 0;
              });
          }
      }


      $scope.CircuitSelected = function (obj,schedule) {
          if ($scope.SelectedSchedule) {            
              delete $scope.SelectedSchedule.HEATER;
              delete $scope.SelectedSchedule.coolingCapable;
              delete $scope.SelectedSchedule.heatingCapable;
              delete $scope.SelectedSchedule.heatMode;
          }
          if (obj) {
              if ($scope.SelectedSchedule == null) {
                  $scope.SelectedSchedule = schedule;
              }
              $scope.SelectedSchedule.CIRCUIT = obj.OBJNAM;
              $scope.SelectedSchedule.bodyObject = {
              };
              $scope.SelectedSchedule.body = false;
              $scope.SelectedSchedule.smartStart = false;
              if (obj.HNAME === 'Pool' || obj.HNAME === 'Spa') {
                  $scope.SelectedSchedule.bodyObject = propertyModel.Objects[obj.BODY];
                  $scope.SelectedSchedule.body = true;
                  //$scope.HeatModes = $filter('objectsByParamValue')(propertyModel.Heaters, ['BODY', 'SHARE'], obj.BODY);
                  $scope.checkDefaults($scope.SelectedSchedule);

                  if ($scope.HeatModes.length > 0) {
                      // don't show heaters if we don't have any
                      $scope.SelectedSchedule.LOTMP = 78;
                      $scope.SelectedSchedule.COOLING = 81;
                  } else {
                      $scope.SelectedSchedule.body = false;
                      delete $scope.SelectedSchedule.LOTMP;
                      delete $scope.SelectedSchedule.COOLING;
                  }
              } else {
                  delete $scope.SelectedSchedule.LOTMP;
                  delete $scope.SelectedSchedule.COOLING;
              }
              if (obj.USAGE && obj.USAGE !== 'USAGE') {
                  $scope.SelectedSchedule.smartStart = obj.USAGE.indexOf('E') > -1;
              }
              $scope.SelectedSchedule.SNAME = obj.SNAME;
          }
      };

      $scope.CancelEdit = function () {
          $scope.SelectedSchedule = null;
          $scope.successCancel();
          refreshList();
      };

      $scope.setToScheduleMode = function (heatMode, index) {

          if (propertyModel.supportConnectedHeaters()) {
              if ($scope.Schedules[index]) {
                  $scope.Schedules[index].MODE = heatMode.MODE;
                  // get name
                  name = getHeatNameModeOnSchedule(heatMode.MODE, $scope.Schedules[index].CIRCUIT);                  
                  $scope.Schedules[index].heatMode = name
              }
          }
      }

      function checkExceptionUltraTemp(bodyID, name) {// checking dependency for ultra all names on heaters selection are default by 'Heat Pump'
          var ultra = false
          angular.forEach(propertyModel.Heaters, function (heater) {         // Transverse to all heaters.
              if ((heater.BODY === bodyID)&&(heater.SUBTYP === "ULTRA"))
                  ultra = true
          })
          if (ultra)
              name = name.replace("Heat Pump", "UltraTemp")
          return name
      }

      function getBodyPerCircuit(circuit){
        var BODY = ""
          angular.forEach(propertyModel.Bodies, function (body) {
              if (circuit.SUBTYP === body.SUBTYP)
                BODY = body                                        
              })
          return BODY;
      }

      function  getHeatNameModeOnSchedule (heatMode,scheduleCircuit) {

          var name = enumerations.getHeaterModeName(heatMode)
          if ((scheduleCircuit == 'C0006') || (scheduleCircuit == 'C0001')) {   // POOL OR SPA
              angular.forEach(propertyModel.Circuits, function (circuit) {
                  if (circuit.OBJNAM == scheduleCircuit) {
                      var bodyObj = getBodyPerCircuit(circuit)
                      name = checkExceptionUltraTemp(bodyObj.OBJNAM, name)
                  }
              })              
          }
          return name;
      }

      function checkExceptionUltraTemp(bodyID, name) {// checking dependency for ultra all names on heaters selection are default by 'Heat Pump'
          var ultra = false
          angular.forEach(propertyModel.Heaters, function (heater) {         // Transverse to all heaters.
              if ((heater.BODY === bodyID) && (heater.SUBTYP === "ULTRA"))
                  ultra = true
          })
          if (ultra)
              name = name.replace("Heat Pump", "UltraTemp")
          return name
      }

      $scope.getScheduleFormatted = function (time, mode) {
        var value = time
        switch (mode){
            case 'SRIS':
                value= 'SUNRISE'
                break;
            case 'SSET':
                value =  'SUNSET'
                break;
            default:
            case 'ABSTIM':
                value = time;
                break;
        }
        return value;
      }

    $scope.setStartTimeMode = function (mode, sched) {
          sched.START = mode.key;
    }

    $scope.setStopTimeMode = function (mode, sched) {
        sched.STOP = mode.key;
    }

    $scope.setTimeModeSelected = function (mode, index,sched) {
          $scope.SelectedSchedule.START = mode.key;
  }

    $scope.HeatModeSelected = function (heatMode, index) {
      var body = $scope.SelectedSchedule.CIRCUIT;
      if (!PWC.isEmptyObject(body && heatMode)) {
        $scope.SelectedSchedule.HEATER = heatMode.OBJNAM;
        if (heatMode.SNAME === $scope.dontchange) { // check is hold = Don't Change             
            $scope.SelectedSchedule.heatMode = $scope.dontchange; //"HOLD";
            $scope.SelectedSchedule.MODE = "0";// DONT CHANGE
        }
        else {

            switch (heatMode.OBJNAM) {
                case "00000":
                    $scope.SelectedSchedule.heatMode = "Off";
                    $scope.SelectedSchedule.MODE = "1"; // OFF
                    break;

                case "HOLD":
                case "00001":
                    $scope.SelectedSchedule.heatMode = $scope.dontchange;
                    $scope.SelectedSchedule.MODE = "0";// DONT CHANGE
                    break;

                default:
                    if (propertyModel.supportConnectedHeaters()) {
                        $scope.SelectedSchedule.heatMode = getHeatNameModeOnSchedule(heatMode.MODE, $scope.SelectedSchedule.CIRCUIT);
                        $scope.SelectedSchedule.MODE = heatMode.MODE;
                    } else {
                        $scope.SelectedSchedule.heatMode = propertyModel.Objects[heatMode.OBJNAM].DISPLAY;
                        $scope.SelectedSchedule.MODE = heatMode.MODE;
                }
                    break;
        }
            $scope.setToScheduleMode(heatMode, index)
      }

        $scope.SelectedSchedule.coolingCapable = $filter('coolingCapableHeater') ([heatMode]);
        $scope.SelectedSchedule.heatingCapable = $filter('heatingCapable') ([heatMode]);

          /* KEEPING  OLD VALUES 
        $scope.SelectedSchedule.LOTMP = 78;
        $scope.SelectedSchedule.COOLING = 81;
        */

        if ($scope.SelectedSchedule.coolingCapable) {
            if ($scope.SelectedSchedule.bodyObject) // check if exist
                $scope.SelectedSchedule.COOLING = $scope.SelectedSchedule.bodyObject.COOLING;
      }
      }
    };

    $scope.getDaysForSchedule = function(days){
        var name = days;
        if (days.length == 0) {
            name = "No Days"
        }
        return name
    }

    $scope.TypeChanged = function (value) {
      $scope.SelectedSchedule.days = [];
      $scope.SelectedSchedule.SINGLE = value;
      if (value === 'OFF') {
        $scope.SelectedSchedule.days = ['M', 'T', 'W', 'R', 'F', 'A', 'U']
      } else {
        $scope.SelectedSchedule.days = $scope.getCurrentDayLetter();
      }
    };

    $scope.getCurrentDayLetter = function () {
      var d = new Date().getDay();
      var currentDayLetter = ['M'];
      switch (d) {
        case 0:
          currentDayLetter = ['U'];
          break;
        case 1:
          currentDayLetter = ['M'];
          break;
        case 2:
          currentDayLetter = ['T'];
          break;
        case 3:
          currentDayLetter = ['W'];
          break;
        case 4:
          currentDayLetter = ['R'];
          break;
        case 5:
          currentDayLetter = ['F'];
          break;
        case 6:
          currentDayLetter = ['A'];
          break;
        default:
          currentDayLetter = ['M'];
      }
      return currentDayLetter;
    };

    $scope.ToggleDayOfWeek = function (value) {
      if ($scope.SelectedSchedule.SINGLE === 'ON') {
        $scope.SelectedSchedule.days = [];
        $scope.SelectedSchedule.days.push(value);
      } else {
        var index = $scope.SelectedSchedule.days.indexOf(value);
        if (index < 0) {
          $scope.SelectedSchedule.days.push(value);
        } else {
          $scope.SelectedSchedule.days.splice(index, 1);
        }
      }
    }

    $scope.DeleteSchedules = function () {
      $scope.ConfirmDelete = true;
    };

    $scope.ConfirmDeleteSchedules = function () {
      $scope.Schedules.forEach(function (schedule) {
        if (schedule.deleteFlag) {
          propertyService.SetValue(schedule.OBJNAM, 'STATUS', 'DSTROY');
        }
      });
     // $scope.Schedules = [];
      propertyService.GetSchedules();
      $scope.ConfirmDelete = false;      
      $scope.CreatingSchedule = false;
    };

    $scope.checkAllDays = function () {
      $scope.Schedule.Days = enumerations.DaysOfWeek.map(function (item) {
        return item.id;
      });
    };

    $scope.SetTemp = function (value, prop) {
      if (value) {
        var newValue = Number(value) || Number(value.replace(/\D/g, ''));
        if (newValue > $scope.maxValue) {
          newValue = $scope.maxValue;
        }
        if (newValue < $scope.minValue) {
          newValue = $scope.minValue;
        }
        if (prop === 'COOLING') {
          $scope.SelectedSchedule.COOLING = newValue;
        } else {
          $scope.SelectedSchedule.LOTMP = newValue;
        }
      }
    };

    $scope.scheduleIsRunning = function (sched) {        
        if (sched.ACT === "ON")
            return true;
        else
            return false;        
    }


    $scope.scheduleIsActive = function (sched) {
        var value = "Inactive"
        if (sched.ACT === "ON") {
            value = "Active"
        }
        if (sched.ACT === "OFF") {
            value = "Inactive"
        }
        return value;
    }
    

    $scope.IncrementTemp = function (obj, prop) {
      if (obj) {
        var newValue = Number(obj) || Number(obj.replace(/\D/g, ''));
        newValue++;
        if (newValue <= $scope.maxTemp) {
            if (prop === 'COOLING') {
            $scope.SelectedSchedule.COOLING = newValue;
          } else {
            $scope.SelectedSchedule.LOTMP = newValue;
          }
        }
      }
    };

    $scope.DecrementTemp = function (obj, prop) {
      if (obj) {
        var newValue = Number(obj) || Number(obj.replace(/\D/g, ''));
        newValue--;
        if (newValue >= $scope.minTemp) {
            if (prop === 'COOLING') {
             $scope.SelectedSchedule.COOLING = newValue;
          } else {
             $scope.SelectedSchedule.LOTMP = newValue;
          }
        }
      }
    };

    $scope.Keypress = function (event) {
      if (event.keyCode < 48 || event.keyCode > 57) {
        event.preventDefault();
      }
    }

    $scope.CheckAllDays = function () {
      $scope.SelectedSchedule.days = enumerations.DaysOfWeek.map(function (item) {
        return item.id;
      });
    };

    $scope.getHeaterModeSelected = function (schedule) {
        var name = "Off";
        var scheduled = "";
        if (propertyModel.supportConnectedHeaters()) {
            var mode = 0; // dont change
            if (schedule.MODE) {    // checking if exists
                mode = schedule.MODE
            }                        
            name = getHeatNameModeOnSchedule(mode,schedule.CIRCUIT)                        

            return name;
        }
        name = schedule.heatMode;
        return name;
    }

    $scope.getTimeModeSelected = function(mode){
        var result = "Manual"        

        switch(mode){
            case 'ABSTIM':
                result ='Manual'; 
                break;

            case 'SRIS': 
                result = 'Sunrise';
                break;

            case 'SSET': 
                result = 'Sunset';
                break;
         }

        $scope.timeModes.forEach(function (item) {
            
         });
         return result;
    }

    $scope.SaveSchedule = function () {      
      var staTimeMin = $scope.SelectedSchedule.startTime.getMinutes();
      var staTimeHr = $scope.SelectedSchedule.startTime.getHours();
      var stoTimeMin = $scope.SelectedSchedule.stopTime.getMinutes();
      var stoTimeHr = $scope.SelectedSchedule.stopTime.getHours();

      $scope.SelectedSchedule.startTime.setMinutes((parseInt(staTimeMin ) ) % 60);
      $scope.SelectedSchedule.startTime.setHours(staTimeHr > 24 ? 23 : staTimeHr);
      $scope.SelectedSchedule.stopTime.setMinutes((parseInt(stoTimeMin) ) % 60);
      $scope.SelectedSchedule.stopTime.setHours(stoTimeHr > 24 ? 23 : stoTimeHr);


      var params = {
        SNAME: $scope.SelectedSchedule.SNAME,
        CIRCUIT: $scope.SelectedSchedule.CIRCUIT,
        DAY: getDayMsgString($scope.SelectedSchedule.days),
        SINGLE: $scope.SelectedSchedule.SINGLE,
        START: $scope.SelectedSchedule.START,
        TIME: $filter('date')($scope.SelectedSchedule.startTime, 'HH,mm,ss'),
        STOP: $scope.SelectedSchedule.STOP,
        TIMOUT: $filter('date')($scope.SelectedSchedule.stopTime, 'HH,mm,ss'),
        GROUP: $scope.SelectedSchedule.GROUP,
        STATUS: $scope.SelectedSchedule.STATUS,
        HEATER: $scope.SelectedSchedule.HEATER,
        COOLING: $scope.SelectedSchedule.COOLING,
        LOTMP: $scope.SelectedSchedule.LOTMP,
        SMTSRT: $scope.SelectedSchedule.SMTSRT,
        VACFLO: 'OFF',
        MODE: $scope.SelectedSchedule.MODE,
        DNTSTP: $scope.SelectedSchedule.DNTSTP               
      };
        // Check params.HEATER for don't stop
      if (params.HEATER === "HOLD")
          params.HEATER = $scope.HoldHeater; //"00001";

      angular.forEach(params, function(value, key) {
          if(value === 'undefined') {
              delete params[key];
          }
      });

      if (!$scope.SelectedSchedule.OBJNAM) {
        $scope.CreatingSchedule = true;
        propertyService.CreateNewObject('SCHED', params).then(function () {
          // $scope.Schedules = [];          
          //propertyService.GetSchedules();          
          $scope.CreatingSchedule = false;
        });
      } else {
        if ($scope.SelectedSchedule.OBJNAM) {
          if(params.STATUS !== 'OFF') {
            delete params.STATUS;
          }
          delete params.VACFLO;
          propertyService.SetParams($scope.SelectedSchedule.OBJNAM, params).then(function () {
              //  $scope.Schedules = [];            
            //propertyService.GetSchedules();           
          });
        }
      }
      $scope.successSave();      
    };

    $scope.IncrementStartTime = function () {
      $scope.SelectedSchedule.startTime = PWC.addMinutes($scope.SelectedSchedule.startTime, 30);
      //syncStopTime();
    };

    $scope.DecrementStartTime = function () {
      $scope.SelectedSchedule.startTime = PWC.addMinutes($scope.SelectedSchedule.startTime, -30);
      //syncStopTime();
    };

    $scope.StartTimeChanged = function (startTime) {
      $scope.SelectedSchedule.startTime = startTime;
      //syncStopTime(startTime); 
    };

    $scope.StopTimeChanged = function (stopTime) {
      stopTimePristine = false;
      // $scope.SelectedSchedule.TIMOUT = $filter('translateTime')($filter('date')(stopTime, 'HH,mm'));
      $scope.SelectedSchedule.stopTime = stopTime;
      validateTimes();
    }

    $scope.IncrementStopTime = function () {
      stopTimePristine = false;
      $scope.SelectedSchedule.stopTime = PWC.addMinutes($scope.SelectedSchedule.stopTime, 30);
      validateTimes();
    }
    $scope.DecrementStopTime = function () {
      stopTimePristine = false;
      $scope.SelectedSchedule.stopTime = PWC.addMinutes($scope.SelectedSchedule.stopTime, -30);
      validateTimes();
    };

    function syncStopTime(startTime) {
      startTime = startTime || $scope.SelectedSchedule.startTime;

      if (stopTimePristine || isNaN($scope.SelectedSchedule.stopTime)) {
        $scope.SelectedSchedule.stopTime = addTwohours(startTime);
      }
      validateTimes();
    }

    function validateTimes() {
      var selectedStartTime = new Date($scope.SelectedSchedule.startTime);
      var selectedStopTime = new Date($scope.SelectedSchedule.stopTime);
      selectedStartTime.setDate(1);
      selectedStopTime.setDate(1);

      if (selectedStopTime < selectedStartTime || selectedStopTime.getHours() < selectedStartTime.getHours()) {
        // fix bug 199 STOP value can go over 11:59 PM 
        //$scope.SelectedSchedule.stopTime = selectedStartTime;
      }
    }

    $scope.ValidateNewSchedule = function () {
      return ($scope.SelectedSchedule && $scope.SelectedSchedule.days && $scope.SelectedSchedule.CIRCUIT) ? true : false;
    }

    $scope.DayChanged = function (day) {
      $scope.SelectedSchedule.days = [];
      $scope.SelectedSchedule.days.push(day);
    };

    $scope.ToggleUseCurrent = function (type) {
      switch (type) {
        case 'HeatMode':
          if ($scope.SelectedSchedule.UseCurrentHeatMode)
            $scope.SelectedSchedule.bodyObject.HEATER = propertyModel.Objects[$scope.SelectedSchedule.CIRCUIT].HEATER;
          break;
        case 'Temp':
          if ($scope.SelectedSchedule.UseCurrentTemp)
            $scope.SelectedSchedule.bodyObject.LOTMP = propertyModel.Objects[$scope.SelectedSchedule.CIRCUIT].LOTMP;
      }
    }

    $scope.ScheduleDeletes = function () {
      var i = 0;
      $scope.Schedules.forEach(function (item) {
        if (item.deleteFlag) {
          i++;
        }
      });
      return i > 0;
    };

    $scope.SetDeleteFlag = function (schedule) {
      if (schedule) {
        schedule.deleteFlag = !schedule.deleteFlag;
      }
    };


    function addTwohours(dateTime) {
      var result = new Date(dateTime);
      if (result.getHours() <= 21) {
        result.setHours(result.getHours() + 2);
      } else if (result.getHours() >= 22) {
        result.setHours(23);
        result.setMinutes(59);
      }
      return result;
    }


    function getTimeFormat() {
      var system = propertyModel.Objects["_C10C"];
      var mode = ((system) ? system.CLK24A : "AMPM");

      return (mode === "AMPM") ? "h:mm a" : "HH:mm";
    }

    function refreshList() {
       console.log(">>>>>-------------------------- enter refreshList ----------------------")      
       $scope.Schedules = [];
       $scope.Schedules_TEMP = [];
       var counter = 0
       console.log("-------------------------- counter = ", counter)
       var mode = "0";
       propertyModel.Schedules.forEach(function (schedule) {
           
        if (schedule.MODE) // CHECK IF MODE EXIST
            mode = schedule.MODE;

        var newSchedule = {
          OBJNAM: schedule.OBJNAM,
          OBJTYP: schedule.OBJTYP,
          SNAME: schedule.SNAME,
          LISTORD: schedule.LISTORD,
          CIRCUIT: schedule.CIRCUIT,
          DAY: schedule.DAY,
          START: schedule.START,
          TIME: $filter('translateTime')(schedule.TIME),
          STOP: schedule.STOP,
          SINGLE: schedule.SINGLE,
          TIMOUT: $filter('translateTime')(schedule.TIMOUT),
          GROUP: schedule.GROUP,
          HEATER: schedule.HEATER,
          COOLING: schedule.COOLING,
          LOTMP: schedule.LOTMP,
          SMTSRT: schedule.SMTSRT,
          DNTSTP: schedule.DNTSTP,
          STATUS: schedule.STATUS,
          MODE : mode,
          ACT: schedule.ACT,
          startTime: moment(schedule.TIME, 'HHmmSS').toDate(),
          stopTime: moment(schedule.TIMOUT, 'HHmmSS').toDate(),
          days: schedule.DAY.split(''),
          heatMode: '',     // don't change default value            
          bodyObject: {},
          body: false,
          smartStart: false,
          deleteFlag: false,
          open: false
        };
        counter++
        console.log("-------------------------- counter = ", counter)
        if (newSchedule.startTime === 'Invalid Date') {
          newSchedule.startTime = PWC.convertToDate(propertyModel.Objects["_C10C"].MIN, propertyModel.Objects["_C10C"].TIMZON);
          newSchedule.stopTime = addTwohours(PWC.convertToDate(propertyModel.Objects["_C10C"].MIN, propertyModel.Objects["_C10C"].TIMZON));
        }
        if(newSchedule.STATUS === 'OFF') {
          newSchedule.disableStatus = true;
        }

        var obj = propertyModel.Objects[newSchedule.CIRCUIT];
        if (obj) {
          if (obj.SUBTYP === 'POOL' || obj.SUBTYP === 'SPA') {
              if (newSchedule.HEATER && newSchedule.HEATER !== '00000' && newSchedule.HEATER !== '00001') {
                newSchedule.bodyObject = $filter('objectsByParamValue')(propertyModel.Bodies, ['BODY', 'SHARE'], obj.BODY)[0];
                switch (newSchedule.HEATER) {
                    case "00000":
                        newSchedule.heatMode = "Off";
                        break;

                    case "00001":
                    case "HOLD":
                        newSchedule.heatMode = $scope.dontchange; 
                        break;

                    default:                                                                        
                        if (propertyModel.supportConnectedHeaters()) {
                            newSchedule.heatMode = getHeatNameModeOnSchedule(newSchedule.MODE, newSchedule.CIRCUIT)
                        } else {
                            newSchedule.heatMode = propertyModel.Objects[newSchedule.HEATER].DISPLAY;    
                        }
                        break;
                }
              var heater = $filter('objectsByName')(propertyModel.Heaters, newSchedule.HEATER);
              newSchedule.coolingCapable = $filter('coolingCapableHeater')(heater);
              newSchedule.heatingCapable = $filter('heatingCapable')(heater);
            }
            newSchedule.body = true;
          }
          if (newSchedule.HEATER === '00000') {
              newSchedule.coolingCapable = false;
              newSchedule.heatingCapable = false;
              newSchedule.heatMode = "Off";
          }

          if (newSchedule.HEATER === 'HOLD') { // adding hold = don't Change
              newSchedule.coolingCapable = false;
              newSchedule.heatingCapable = false;
              newSchedule.heatMode = $scope.dontchange;
          }

          if (obj.USAGE && obj.USAGE !== 'USAGE') {
            newSchedule.smartStart = obj.USAGE.indexOf('E') > -1;
          }
        }

        if (schedule.VACFLO === 'OFF') {
          console.log("-------------------------- $scope.Schedules.push(newSchedule")
          
          $scope.Schedules_TEMP.push(newSchedule); //$scope.Schedules.push(newSchedule);
        }
       });

       console.log(" COPY SCHEDULES")
       $scope.Schedules = $scope.Schedules_TEMP

       console.log(" REQUEST CLOCK")
       $scope.Property.getUpdateClock();   // UPDATE CLOCK

    
      console.log("<<<-------------------------- exit refreshList ----------------------")
    }

    function getDayMsgString(days) {
      return (angular.isArray(days) ? days.join('') : days);
    }


      /*********** SECURITY MODE ********
      VacationMode		    ,  v
      Support  		        ,  o    
      GeneralSettings		,  g        done  , under testing
      AlertsNotification	,  q
      UserPortal		    ,  i        ONLY  ADMIN : THW
      Groups			    ,  k
      Remotes			    ,  w        done
      AdvancedSettings	    ,  a        done   , under testing
      Chemistry		        ,  c        done
      Status			    ,  t
      DeleteStatus		    ,  r
      Schedules		        ,  e        done
      Features		        ,  f        done
      Lights			    ,  l        done
      Pool			        ,  p        done
      PoolHeatSource		,  h        **
      PoolTemp		        ,  P        **
      Spa			        ,  m        done
      SpaHeatSource		    ,  n        **
      SpaTemp			    ,  S        **
      History			    ,  u        done
      MultiBodyDrawer		,  b    
      ServiceMode		    ,  x
      SystemConfig		    ,  C
      ********************************/
    $scope.isScheduleSecurityLocked = function () {

        // here security is not enable 
        if (!$scope.securityEnabled)
            return false

        // check if allow schedules
        if ($scope.SHOMNU.indexOf('e') < 0) {
            return true;
        }

        return false;
    }

    $scope.isCircuitNotHeatingCapable = function () {

        var capable = $scope.isCircuitHeatingCapable();
        
        return !capable;   // negative of capable
    }

    
    $scope.isCircuitHeatingCapable = function () {
        if ($scope.SelectedSchedule === null) //"undefined")
            return false;

        var isCapable = false;
        
        var obj = propertyModel.Objects[$scope.SelectedSchedule.CIRCUIT];
        if (obj) {
            if (obj.HNAME === 'Pool' || obj.HNAME === 'Spa') {
                isCapable = true;
            }
        }        

        return isCapable;
    }


    $scope.isHeatControlEnable = function () {        
        if ($scope.SelectedSchedule) {

            if ($scope.SelectedSchedule.heatMode)
                if ($scope.SelectedSchedule.heatMode === "Off") // checking if is off the selection
                    return false;
                 
            if ($scope.SelectedSchedule.heatMode === $scope.dontchange) // checking if is off the selection
                    return false;

            if ($scope.SelectedSchedule.heatingCapable) {
                return $scope.SelectedSchedule.heatingCapable;
            }
        }

        return false;
    }

    $scope.isCoolingCapable = function () {
        if ($scope.SelectedSchedule) {
            if ($scope.SelectedSchedule.coolingCapable) {
                return $scope.SelectedSchedule.coolingCapable;
            }
        }

        return false;   // is cooling capable
    }



    function newSchedule() {
      var schedule = {
        OBJNAM: null,
        OBJTYP: 'SCHED',
        SNAME: '',
        LISTORD: '',
        CIRCUIT: '',
        DAY: '',
        START: 'ABSTIM',
        TIME: '',
        STOP: 'ABSTIM',
        SINGLE: 'OFF',
        TIMOUT: '',
        GROUP: '',
        HEATER: '',
        COOLING: '',
        LOTMP: '',
        SMTSRT: 'OFF',
        DNTSTP: 'OFF',
        STATUS: 'ON',
        ACT: 'OFF',
        days: ['M', 'T', 'W', 'R', 'F', 'A', 'U'],
        heatMode: '',
        startTime: moment().hour(8).minute(0).toDate(),
        stopTime: moment().hour(17).minute(0).toDate(),
        bodyObject: {},
        body: false,
        smartStart: false,
        deleteFlag: false,
        open: true
      };
      return schedule;
    }

    function init() {
      refreshList();

      var preference = propertyModel.Objects._5451;
      if (preference) {
        if (preference.MODE.toUpperCase() === "ENGLISH") {
          $scope.UoM = 'F';
          $scope.minTemp = 0;
          $scope.maxTemp = 104;
        }
      }

      $scope.ProgrammableCircuits = $filter('programmableCircuits')(propertyModel.Objects);
    }

  }
]);
