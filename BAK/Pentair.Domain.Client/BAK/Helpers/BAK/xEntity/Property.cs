﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pentair.Domain.Client.Entity
{
    public enum PropertyConfigurations
    {
        SharedPoolAndSpa,
        SeparatePoolAndSpa,
        PoolOnly,
        SpaOnly,
        SimpleBlankConfiguration,
        ComplexBlankConfiguration,
        Import
    };

    public class Property
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int LocalPropertyId { get; set; }
        public int PropertyId { get; set; }
        [MaxLength(128)]
        public string RemoteHost { get; set; }
        public PropertyConfigurations Configuration { get; set; }
        [MaxLength(16)]
        public string ProtocolVersion { get; set; }
        [MaxLength(128)]
        public string Secret { get; set; }
        [MaxLength(128)]
        public string Name { get; set; }
        public long LastChange { get; set; }
        public long LastNotify { get; set; }
        public long LastAlert { get; set; }
        public long LastHistory { get; set; }
    }
}
