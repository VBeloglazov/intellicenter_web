﻿describe('ManagePropertiesController', function () {
    var PropertyService;
    var $location;
    var $scope;
    var LightShowController;
    var $controller;
    var appService;
    var $httpBackend;
    var HTTP_SERVICE_URL;
    beforeEach(module('PentairWebClient'));

    beforeEach(inject(function (_$controller_, _$rootScope_, _$httpBackend_, _PropertyService_, _$location_,
                                _AppService_, _SERVICE_HOST_NAME_) {
        $scope = _$rootScope_.$new();
        PropertyService = _PropertyService_;
        $location = _$location_;
        $controller = _$controller_;
        appService = _AppService_;
        $httpBackend = _$httpBackend_;
        HTTP_SERVICE_URL = 'https://' + _SERVICE_HOST_NAME_;
        spyOn($location, 'path');
        LightShowController = $controller("ManagePropertiesController", { $scope: $scope });
    }));

    describe('GoToDashboard', function () {
        it('should take the user the main page', function () {
            $scope.GoToDashboard();
            expect($location.path).toHaveBeenCalledWith('/installations');
        });
    });

   xdescribe('Delete Pool', function () {
        beforeEach(function () {
            $scope.DeleteError = true;
            spyOn(PropertyService, 'FetchInstallations');
            spyOn(PropertyService, 'Reset');
            PropertyService.SelectedInstallation = 2;
            $httpBackend.expectGET(HTTP_SERVICE_URL + 'Api/Installations').respond(200);
        });
        it('should Send a message to delete ', function () {
            $httpBackend.expectPOST(HTTP_SERVICE_URL + 'Api/Account/RemoveInstallation/', { PropertyId: 1 }).respond(200);
            $scope.DeletePool(1);
            $httpBackend.flush();
            expect($scope.DeleteError).toBe(false);
            expect(PropertyService.FetchInstallations).toHaveBeenCalled();
        });


        it('should call fetchInstallations', function () {
            $httpBackend.expectPOST(HTTP_SERVICE_URL + 'Api/Account/RemoveInstallation/', { PropertyId: 1 }).respond(500);
            $scope.DeletePool(1);
            $httpBackend.flush();
            expect($scope.DeleteError).toBe('Can not remove the last ADMIN on a pool.');
        });

        it('should call fetchInstallations', function () {
            $httpBackend.expectPOST(HTTP_SERVICE_URL + 'Api/Account/RemoveInstallation/', { PropertyId: 1 }).respond(400);
            $scope.DeletePool(1);
            $httpBackend.flush();
            expect(PropertyService.FetchInstallations).toHaveBeenCalled();
            expect($scope.DeleteError).toBe('This pool has already been removed from your account.');
        });

        it('should reset the property service if deleting the property your connected to', function () {

            $httpBackend.expectPOST(HTTP_SERVICE_URL + 'Api/Account/RemoveInstallation/', { PropertyId: 2 }).respond(200);
            $scope.DeletePool(2);
            $httpBackend.flush();
            expect(PropertyService.Reset).toHaveBeenCalled();
        });

    });
});