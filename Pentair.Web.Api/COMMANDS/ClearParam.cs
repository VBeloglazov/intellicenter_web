﻿namespace Pentair.Web.Api
{
    public class ClearParam : CommandBase
    {
        public ClearParam(
            string messageID
            )
            : base(CommandString.ClearParam, messageID)
        {
        }

    } // class ClearParam
}