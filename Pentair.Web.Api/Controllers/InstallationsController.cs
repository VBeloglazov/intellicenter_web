﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using System.Linq.Dynamic;
using System.Threading.Tasks;

using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;

using SERVICE_SHARED;

namespace Pentair.Web.Api
{
    public class InstallationsController : ApiController
    {
        private const string EVENT_SOURCE = "InstallationsController";

        public ApplicationUserManager UserManager
        {
            get { return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>(); }
            private set { _userManager = value; }
        }
        private ApplicationUserManager _userManager;

        /// <summary>
        /// GET api/installations provides pagination for a list of installations associated with the logged in user.
        /// Pagination is active if pageSize is given and it is greater than 0.
        /// </summary>
        [Authorize]
        public IEnumerable<UserInstallation> Get(
            int pageSize = 100,
            int page = 0)
        {
            IEnumerable<UserInstallation> results = null;

            Guid userId = Guid.Empty;

            try
            {
                userId = User.Identity.GetOwinUserId();

                if (pageSize > 100)
                {
                    pageSize = 100;
                }

                using (DbContextPentair db = DbContextPentair.New())
                {
//                    results = db.UserInstallationListWithDetails(userId, term, filter, sort, descend);
                    results = db.UserInstallationListWithDetails(userId);
                }

                if (results != null && pageSize > 0)
                {
                    results = results.Skip(pageSize * (page > 0 ? page - 1 : 0)).Take(pageSize);

                    int totalCount = results.Count();
                    int totalPages = (int)(totalCount / pageSize + 0.5);

                    var paginationHeader = new
                    {
                        TotalCount = totalCount,
                        TotalPages = totalPages,
                    };

                    System.Web.HttpContext.Current.Response.Headers.Add(
                        "Access-Control-Expose-Headers",
                        "X-Pagination"
                    );

                    System.Web.HttpContext.Current.Response.Headers.Add(
                        "X-Pagination",
                        Newtonsoft.Json.JsonConvert.SerializeObject(paginationHeader)
                    );
                }
            }
            catch(Exception e)
            {
                LogManager.LogErrorAsync(
                    source: EVENT_SOURCE,
                    eventBriefDescription: "GET api/installations failed",
                    installationId: 0,
                    userId: userId,
                    connectionId: Guid.Empty,
                    eventFullDescription:
                        ExceptionManager.CustomizeException(e).ErrorMessage(
                            errorHeader: null,
                            addStackTrace: true
                        )
                );

                results = null;
            }

            return results;
        
        } // GET api/installations

        /// <summary>
        /// GET api/installations/id provides details about a single installation for the logged in user.
        /// </summary>
        /// <param name="installationId"></param>
        /// <returns></returns>
        [Authorize]
        public UserInstallation Get(
            int id)
        {
            UserInstallation result = null;

            Guid userId = Guid.Empty;

            try
            {
                userId = User.Identity.GetOwinUserId();

                using (DbContextPentair db = DbContextPentair.New())
                {
                    result = db.UserSingleInstallationWithDetails(id, userId);

                } // using (DbContextPentair)
            }
            catch (Exception e)
            {
                LogManager.LogErrorAsync(
                    source: EVENT_SOURCE,
                    eventBriefDescription: "GET api/installations/" + id + " failed",
                    installationId: 0,
                    userId: userId,
                    connectionId: Guid.Empty,
                    eventFullDescription:
                        ExceptionManager.CustomizeException(e).ErrorMessage(
                            errorHeader: null,
                            addStackTrace: true
                        )
                );

                result = null;
            }

            return result;

        } // GET api/installations/id

        [Authorize]
        [HttpDelete]
        public IHttpActionResult RemovePools(
            InstallationRemovePoolsModel pools)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                Guid userId = User.Identity.GetOwinUserId();

                // TODO: Replace with one SQL call
                foreach (string pool in pools.PropertyIds)
                {
                    int poolId;
                    if (Int32.TryParse(pool, out poolId))
                    {
                        using (DbContextPentair db = DbContextPentair.New())
                        {                         
                            db.RemoveUserAssociation(userId, poolId);

                        } // using (DbContextPentair db)
                    }
                }

                return Ok();
            }
            catch
            {
                return new InternalServerErrorResult(Request);
            }

        } // RemovePools()

        [Authorize]
        [HttpPost]
        public IHttpActionResult NameChange(
            InstallationsNameChangeModel postParameters)
        {
            // 2018-03-12 VB: Pathfinder's ugly heritage. As clients still might send the request,
            //                this return left to prevent them from getting frozen waiting for response.
            //                (Has Pathfinder heard about requests timeouts? ;)

            LogManager.LogWarningAsync(
                source: EVENT_SOURCE,
                eventBriefDescription: "Deprecated API method \"NameChange\" called by client",
                installationId: postParameters.PropertyId,
                userId: Guid.Empty,
                connectionId: Guid.Empty,
                eventFullDescription: "\"POST /service/Api/Installations/NameChange/\" deprecated. SetParamList from client and WriteParamListResponse from THW must do the job."
            );

            return BadRequest();

            /* TODO: 2018-03-12 VB: 2b deleted

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            HttpStatusCode responseCode = HttpStatusCode.InternalServerError;

            try
            {
                using (DbContextPentair db = DbContextPentair.New())
                {
                    responseCode = db.UpdateInstallationName(postParameters.PropertyId, postParameters.Name);

                } // using (DbContextPentair db)
            }
            catch(Exception e)
            {
                LogManager.LogErrorAsync(
                    source: EVENT_SOURCE,
                    eventBriefDescription: "NameChange failed",
                    installationId: postParameters.PropertyId,
                    userId: Guid.Empty,
                    connectionId: Guid.Empty,
                    eventFullDescription:
                        ExceptionManager.CustomizeException(e).ErrorMessage(
                            errorHeader: null,
                            addStackTrace: true
                        )
                );

                responseCode = HttpStatusCode.InternalServerError;
            }

            switch (responseCode)
            {
                case HttpStatusCode.OK:
                    return Ok();

                case HttpStatusCode.NotFound:
                    return NotFound();

                default:
                    return InternalServerError();

            } // switch (responseCode)
            */
        
        } // NameChange()

    } // class InstallationsController
}