﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pentair.Domain.Client.BusinessLogic.Classes
{
    public class AssociatedUser
    {
        #region Public Properties
        public Guid UserId { get; set; }
        public string UserName { get; set; }
        public string EmailAddress { get; set; }
        public string AccessToken { get; set; }
        public string SecurityName { get; set; }
        public String Status { get; set; }
        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public AssociatedUser()
        {
            UserId = Guid.Empty;
            AccessToken = "";
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="Id">Installation / Property ID</param>
        /// <param name="name">Property name</param>
        /// <param name="token">User access token</param>
        /// <param name="online">Property on line</param>
        public AssociatedUser(string Id, string name, string emailAddress, string token)
        {
            UserId = new Guid(Id);
            UserName = name;
            EmailAddress = emailAddress;
            AccessToken = token;
        }
        #endregion
    }
}
