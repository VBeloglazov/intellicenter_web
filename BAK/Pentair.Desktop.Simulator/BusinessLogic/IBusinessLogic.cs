﻿using System;
using System.Collections.Generic;
using Pentair.Domain.Client.Helpers.Commands;

namespace Pentair.Desktop.Simulator.BusinessLogic
{
    public interface IBusinessLogic
    {
        /// <summary>
        /// GetParamList retrieves and returns the requested parameters to the caller
        /// </summary>
        /// <param name="request">List of parameters requested</param>
        /// <returns></returns>
        List<ParamObject> GetParamList(GetParamList request);

        /// <summary>
        /// SetParameterList sets the parameters given by the requestor.
        /// </summary>
        /// <param name="request">Parameters to change</param>
        void SetParamList(SetParamListResponse request);

        /// <summary>
        /// Retrieve the list of all configuration changes.
        /// </summary>
        /// <returns></returns>
        WriteParamListResponse GetConfigChanges();

        /// <summary>
        /// Retrieve the list of all configuration changes.
        /// </summary>
        /// <returns></returns>
        WriteParamListResponse GetConfigChanges(String lastChange);
        /// <summary>
        /// Retrieve the list of notification changes.
        /// </summary>
        /// <returns></returns>

        NotifyList GetNotifications();

        /// <summary>
        /// Create subscriptions
        /// </summary>
        /// <param name="request">List of itmems being subscribed to</param>
        /// <param name="client">Client subscribimg</param>
        /// <param name="installation">Installation being subscribed to</param>
        NotifyList RequestParamList(RequestParamList request, Guid client, int installation);

        /// <summary>
        /// Clear all subscriptions for this client
        /// </summary>
        /// <param name="client">Client Id</param>
        /// <param name="installation">Installation Id</param>
        void ClearParamList(Guid client, int installation);
    }
}
