-- =============================================================
--
-- Author:		Vladimir Beloglazov
-- Create date: 04/17/2018
--
-- Creates:
--
--		Table Type:			None
--
--		Stored Procedure:	PUpdateUserInvitation
--
-- =============================================================

USE PathfinderIntelliCenter
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (
	SELECT * FROM sys.objects
	WHERE object_id = OBJECT_ID(N'dbo.PUpdateUserInvitation')
	AND type in (N'P', N'PC')
)
DROP PROCEDURE dbo.PUpdateUserInvitation
GO

-- -------------------------------------------------------------
--
-- Updates [UserInvitations] and returns updated record field values.
--
-- -------------------------------------------------------------
CREATE PROCEDURE dbo.PUpdateUserInvitation
	 @invitationId AS uniqueidentifier
AS
BEGIN

	SET NOCOUNT OFF

	UPDATE dbo.UserInvitations
	SET  UpdateDateTime = GETDATE()
		,EmailId = NULL
	WHERE InvitationId = @invitationId

	-- Result set
	SELECT
		 UI.InvitationId	AS InvitationId
		,UI.EmailAddress	AS EmailAddress
		,UI.InstallationId	AS InstallationId
		,I.Name				AS InstallationName
		,UI.AccessToken		AS AccessToken
		,UI.[Status]		AS [Status]
		,UI.EmailId			AS EmailId
		,UI.CreateDateTime	AS CreateDateTime
		,UI.UpdateDateTime	AS UpdateDateTime
	FROM dbo.UserInvitations UI
	INNER JOIN dbo.Installations I ON I.InstallationId = UI.InstallationId
	WHERE UI.InvitationId = @invitationId

END
GO
