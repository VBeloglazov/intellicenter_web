﻿describe('SocketService', function () {
    var mockSocket;
    var client;
    var $httpBackend;
    var successSpy;
    var failureSpy;
    var SocketService;
    var propertyId;
    var response = { data: '' };
    var message = { command: "RequestParamList", objectList: [{ objnam: "S1001", keys: ["STATUS"] }] };
    var SERVICE_HOSTNAME;

    beforeEach(module('PentairWebClient'));

    beforeEach(inject(function (_SocketService_, _$httpBackend_, _$q_, _SERVICE_HOST_NAME_) {
        $httpBackend = _$httpBackend_;
        SocketService = _SocketService_;
        propertyId = "PROPID";
        successSpy = jasmine.createSpy('successSpy');
        failureSpy = jasmine.createSpy('failureSpy');
        SERVICE_HOSTNAME = _SERVICE_HOST_NAME_;

        mockSocket = {
            close: jasmine.createSpy('close'),
            send: jasmine.createSpy('send'),
            open: jasmine.createSpy('open')
        };

        spyOn(window, 'WebSocket').and.returnValue(mockSocket);

        //spy on console log to avoid log message 
        //Uncomment for debugging 
        spyOn(console, 'log');
        client = SocketService.newSocket(propertyId);
    }));

    describe('new Socket', function () {

        it('should create a new Web socket', function () {
            sessionStorage.token = "FAKETOKEN";
            client = SocketService.newSocket(propertyId);
            // expect(window.WebSocket).toHaveBeenCalledWith('wss://' + SERVICE_HOSTNAME + '/api/websocket/PROPID?access-token=FAKETOKEN');
            // expect(client).toBe(mockSocket);
        });

        it('should close the socket if it was already open before creating a new Web socket', function () {
            sessionStorage.token = "FAKETOKEN";
            mockSocket.readyState = 1;
            client = SocketService.newSocket(propertyId);
            expect(mockSocket.close).toHaveBeenCalled();
            // expect(window.WebSocket).toHaveBeenCalledWith('wss://' + SERVICE_HOSTNAME + '/api/websocket/PROPID?access-token=FAKETOKEN');
            // expect(client).toBe(mockSocket);
        });

        it('should create a new Web socket without an access token', function () {
            sessionStorage.token = '';
            client = SocketService.newSocket(propertyId);
            // expect(window.WebSocket).toHaveBeenCalledWith('wss://' + SERVICE_HOSTNAME + '/api/websocket/PROPID');
            // expect(client).toBe(mockSocket);
        });
    });
    describe('send Message', function () {
        describe('when socket not ready', function () {
            beforeEach(function () {
                mockSocket.readyState = 1;
            });

            it('should add a message Id and send message', function () {

                SocketService.sendMessage(message);
                var args = JSON.parse(mockSocket.send.calls.argsFor(0)[0]);

                expect(args.command).toBe(message.command);
                expect(args.objectList).toEqual(message.objectList);
            });
        });

        describe('when socket not ready', function () {
            beforeEach(function () {
                mockSocket.readyState = 0;
            });

            it('should not send message', function () {
                SocketService.sendMessage(message);
                expect(mockSocket.send).not.toHaveBeenCalled();
            });
        });

        xdescribe('when retry is true', function () {
            it('should store message as lastMessage ', function () {
                SocketService.sendMessage(message, true);
                expect(SocketService.hasPendingMessage()).toBe(true);
            });
        });
    });

    describe('events', function () {
        it('should define an onopen event handler', function () {
            expect(client.onopen).toBeDefined();
        });
        it('should define an onclose event handler', function () {
            expect(client.onclose).toBeDefined();
        });
        it('should define an onerror event handler', function () {
            expect(client.onerror).toBeDefined();
        });
        it('should define an onmessage event handler', function () {
            expect(client.onmessage).toBeDefined();
        });
    });

    describe('on open', function () {
    });

    describe('on close', function () {
        it('should clear the socket Object', function () {
            client.onclose();
            expect(SocketService.socket).toEqual({});
        });
    });

    describe('on message', function () {

        describe('with valid message', function () {
            beforeEach(function () {
                response.data = '{"command": "ClearParam"}';
            });
        });

        describe('with invalid message', function () {
            beforeEach(function () {
                response.data = '"INVALID"';
            });
        });

    });

    describe('close Socket', function () {
        it('should call close socket if socket is valid and not already closing', function () {
            mockSocket.readyState = 1;
            SocketService.closeSocket();
            expect(mockSocket.close).toHaveBeenCalled();
        });

        it('should not call close socket if socket is valid and already closing', function () {
            mockSocket.readyState = 3;
            SocketService.closeSocket();
            expect(mockSocket.close).not.toHaveBeenCalled();
        });

        it('should not call close socket if socket is not valid', function () {
            //call on error to close socket and empty object
            client.onerror(response);
            SocketService.closeSocket();
            expect(mockSocket.close).not.toHaveBeenCalled();
        });
    });
})