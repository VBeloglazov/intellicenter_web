﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pentair.Web.Api
{
    public class PostalCodeInfo
    {
        public string CountryName { get; set; }
        public string CityName { get; set; }
        public string ProvinceAbbr { get; set; }
        public string TimeZone { get; set; }
        public decimal UTC { get; set; }
        public string DST { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public string PostalCode { get; set; }

    } // class PostalCodeInfo
}