﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Pentair.Domain.Client.Helpers.Commands
{
    public class ParamCommandWithTimeSpanResponse : ResponseCommandWithTwoTimespans
    {
        [JsonProperty("objectList", Required = Required.Default)]
        public List<ParamObject> ObjectList { get; private set; }

        public ParamCommandWithTimeSpanResponse(
            string command,
            string messageID,
            string response,
            string timeNow,
            string timeSince,
            List<ParamObject> objectList
            )
            : base(command, messageID, response, timeNow, timeSince)
        {
            this.ObjectList = objectList;

        } // ParamCommandWithTimeSpanResponse()

        /// <summary>
        /// Always returns an object, valid or not. If invalid, check the DeserializationError property value.
        /// </summary>
        public static new ParamCommandWithTimeSpanResponse Deserialize(
            string serializedMessage)
        {
            ParamCommandWithTimeSpanResponse command = null;

            try
            {
                command = JsonConvert.DeserializeObject<ParamCommandWithTimeSpanResponse>(serializedMessage);
                command.IsValid = true;
            }
            catch (Exception e)
            {
                // Deserialization as ParamCommandWithTimeSpanResponse failed

                StringBuilder sbError = new StringBuilder();

                sbError.Append("Cannot deserialize as ParamCommandWithTimeSpanResponse. " + e.Message);

                // Try to get at least the base message data
                ResponseCommandWithTwoTimespans baseCommand = ResponseCommandWithTwoTimespans.Deserialize(serializedMessage);

                if (!baseCommand.IsValid)
                {
                    sbError.Append("\n" + baseCommand.DeserializationError);
                }

                command = new ParamCommandWithTimeSpanResponse(
                    command: baseCommand.Command,
                    messageID: baseCommand.MessageID,
                    response: ResponseString.BadRequest,
                    timeNow: String.Empty,
                    timeSince: String.Empty,
                    objectList: null)
                {
                    IsValid = false,
                    DeserializationError = sbError.ToString()
                };
            }

            return command;

        } // Deserialize()

    } // class ParamCommandWithTimeSpanResponse
}
