﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pentair.Web.Api
{
    public class CreateObjectFromParam : NewParamCommand
    {
        public CreateObjectFromParam(
            string messageID,
            List<NewParamObject> objectList
            )
            : base(CommandString.CreateObject, messageID, objectList)
        {
        }

    } // class CreateObjectFromParam
}
