﻿using System;
using System.Windows;
using System.Windows.Threading;

namespace Pentair.Desktop.Simulator
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
        }

        public bool DoHandle { get; set; }

        private void AppUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            if (this.DoHandle)
            {
                MessageBox.Show(e.Exception.Message, "Exception Caught", MessageBoxButton.OK, MessageBoxImage.Error);
                e.Handled = true;
            }
            else
            {
                MessageBox.Show(e.Exception.Message, "Application is going to close!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            if (null != e)
            {
                var ex = e.ExceptionObject as Exception;
                MessageBox.Show(ex.Message, "Uncaught Thread Exception",
                            MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
