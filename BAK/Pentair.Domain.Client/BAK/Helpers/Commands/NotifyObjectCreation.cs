﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pentair.Domain.Client.Helpers.Commands;

namespace Pentair.Domain.Client.Helpers
{
    [Obsolete("Not used anymore", true)]
    public class NotifyObjectCreation : KeyCommand
    {
        public NotifyObjectCreation(
            string messageID,
            string condition,
            List<KeyObject> objectList
            )
            : base(CommandString.ObjectCreated, messageID, condition, objectList)
        {
        }

    } // class NotifyObjectCreation
}
