'use strict';
var PWC = PWC || {};

PWC.factory('MessageProcessor', [
    '$filter', '$rootScope', 'PropertyModel', 'Enumerations', 'SocketService',
    function ($filter, $rootScope, propertyModel, enumerations, socketService) {
        var factory = {};

        var temp_graphData = {
            poolTemp: [],
            spaTemp: [],
            poolSideTemp: [],
            solarTemp: [],
            spaUsage: [],
            poolUsage: [],
            lightUsage: [],
            phLevel: [],
            phFeed: [],
            orpLevel: [],
            orpFeed: [],
            solarUsage: [],
            heaterUsage: []
        };

        //History Buffer
        var resultHistoryBuffer;

        // remove current history statistics when users selects new interval such as: jumptoday, intervalWeek, intervalMonth.
        factory.InitHistoryInterval = function () {
            console.log("------------------- removing temp buffers for history------------");
            // remove temporary data
            temp_graphData.poolTemp = [];            
            temp_graphData.spaTemp = [];
            temp_graphData.poolSideTemp = [];
            temp_graphData.solarTemp = [];
            temp_graphData.spaUsage =  [];
            temp_graphData.poolUsage = [];
            temp_graphData.lightUsage = [];
            temp_graphData.phLevel = [];
            temp_graphData.phFeed = [];
            temp_graphData.orpLevel = [];
            temp_graphData.orpFeed = [];
            temp_graphData.solarUsage = [];
            temp_graphData.heaterUsage = [];
        };


        // Process Schedule
        factory.ProcessSchedule = function (schedule) {
            var broadcast = false;
            if (schedule.objnam && schedule.params) {
                var act = "OFF";
                if (schedule.params.ACT)    // check if exists
                    act = schedule.params.ACT

                var mode = "0"; // heat mode
                if (schedule.params.MODE)    
                    mode = schedule.params.MODE                

                var newObject = {
                    OBJNAM: schedule.objnam,
                    OBJTYP: schedule.params.OBJTYP,
                    SNAME: schedule.params.SNAME,
                    LISTORD: schedule.params.LISTORD,
                    CIRCUIT: schedule.params.CIRCUIT,
                    DAY: schedule.params.DAY,
                    START: schedule.params.START,
                    TIME: schedule.params.TIME,
                    STOP: schedule.params.STOP,
                    SINGLE: schedule.params.SINGLE,
                    TIMOUT: schedule.params.TIMOUT,
                    GROUP: schedule.params.GROUP,
                    HEATER: schedule.params.HEATER,
                    COOLING: schedule.params.COOLING,
                    LOTMP: schedule.params.LOTMP,
                    SMTSRT: schedule.params.SMTSRT,
                    VACFLO: schedule.params.VACFLO,
                    STATUS: schedule.params.STATUS,
                    DNTSTP: schedule.params.DNTSTP,
                    ACT: act,
                    MODE : mode
                };

                // check if object exist
                if (!isObjectAvailable(propertyModel.Schedules, schedule.objnam)) {
                    propertyModel.Schedules.push(newObject);
                    broadcast = true;
                }
            }
            if (broadcast) {
                $rootScope.$broadcast('SchedulesUpdated', newObject);
                //$rootScope.$broadcast('SchedulesRequestParam', newObject);
            }
        };        

        //function replotHistoryData() {
        factory.replotHistoryData = function () {        
            console.log("-------------replot data : save previous results ------")
            var temp1 =  angular.copy(resultHistoryBuffer);            
            var temp = angular.copy(temp_graphData)

             var  result = [];
            //Combine Results by body of water
             result = combineHistoryByBodyOfWater(result, temp);
        }        

        factory.ProcessSystemInfo = function (info) {
            if (info && info.params) {
                var newObject = {
                    INSTALLATION: info.params.INSTALLATION,
                    OWNER: info.params.OWNER,
                    ADDRESS: info.params.ADDRESS
                }
                //TODO: do we need to 'STORE' this object?
                // storeObject("SystemInfo", newObject);
                propertyModel.SystemInfo[newObject.INSTALLATION] = newObject;
            }
        }


        factory.ProcessManageUserMessage = function (message) {
            if (!PWC.isEmptyObject(message) && message.status === 'OK') {
                propertyModel.DeleteUserError = false;
                $rootScope.$broadcast('UserUpdated', message);
            } else {
                propertyModel.DeleteUserError = (message.status === "NOTFOUND") ? $filter('translate')('USERNOTFOUNDERROR') : $filter('translate')('LASTADMINDELETEERROR');
                $rootScope.$broadcast('UserUpdateError', DeleteUserError);

            }
        };
        factory.ProcessUserListMessage = function (message) {
            var users = message.objectList;
            propertyModel.ListCount = message.totalCount;
            propertyModel.Users = [];
            if (users) {
                angular.forEach(users, function (user) {
                    var newUser = {
                        email: user.objnam,
                        id: user.params.id,
                        name: user.params.name,
                        level: user.params.level,
                        securityToken: user.params.security,
                        status: user.params.status,
                    };
                    // fix for when level doesn't get populated by custom roles.
                    angular.forEach(propertyModel.Security, function(securityItem) {
                        if(securityItem.OBJNAM === newUser.securityToken)
                            newUser.level = securityItem.SNAME;
                    });
                    propertyModel.Users.push(newUser);
                });
            }
            $rootScope.$broadcast('UsersRefreshed', message);
        };

        factory.ProcessAlerts = function (message) {
            angular.forEach(message.answer, function (alertToProcess) {
                var alert = alertToProcess.params;
                if (alert) {
                    alert.OBJNAM = alertToProcess.objnam;

                    // checking  lost communication
                    factory.ProcessCheckLostCommunication(alert);
                }
                // here in the status sname we swap out values needed from the parent denoted by '%PARAM'
                alert.SNAME = alert.SNAME.split(' ').reduce(function (a, b) {
                    if (a.indexOf('%') === 0) {
                        try {
                            a = propertyModel.Objects[alert.PARENT][(a.substring(1))];
                        }
                        catch (e) {
                        }
                    }
                    if (b.indexOf('%') === 0) {
                        try {
                            b = propertyModel.Objects[alert.PARENT][(b.substring(1))];
                        }
                        catch (e) {
                        }
                    }
                    return a + ' ' + b;
                });
                storeObject("Alerts", alert);
            });
        };

        factory.ProcessSystemConfig = function (message) {
            var settings = message.params;
            if (settings) {
                var userSettings = {
                    OBJNAM: message.objnam,
                    MODE: settings.MODE,
                    NAME: settings.NAME,
                    ADDRESS: settings.ADDRESS,
                    CITY: settings.CITY,
                    STATE: settings.STATE,
                    ZIP: settings.ZIP,
                    COUNTRY: settings.COUNTRY,
                    TIMZON: settings.TIMZON,
                    PHONE: settings.PHONE,
                    PHONE2: settings.PHONE2,
                    EMAIL: settings.EMAIL,
                    EMAIL2: settings.EMAIL2,
                    TEMPNC: settings.TEMPNC,
                    VACFLO: settings.VACFLO,
                    VACTIM: settings.VACTIM,
                    START: settings.START,
                    STOP: settings.STOP,
                    VER: settings.VER,
                    MANHT: settings.MANHT, //true,
                    PROPNAME: settings.PROPNAME,
                    UPDATE: 'no',       // firmware update 
                    NEWFWVER: '',       // new fw version
                    SERVICE: '',        // service mode
                };

                if (settings.MANHT) {
                    userSettings.MANHT = settings.MANHT;
                }

                if (settings.UPDATE){
                    userSettings.UPDATE = settings.UPDATE;
                }

                if (settings.NEWFWVER) {
                    userSettings.NEWFWVER = settings.NEWFWVER;
                }                
                if (settings.SERVICE) {
                    userSettings.SERVICE = settings.SERVICE;
                }

                propertyModel.Objects[userSettings.OBJNAM] = userSettings;
                if (userSettings.MODE) {
                    console.log("current Mode:  ", userSettings.MODE)
                }
                else {                    
                    console.log("Error:   User Settings has NO MODE ????")
                }

                $rootScope.$broadcast('SystemConfigUpdated', userSettings);
            }
        }

        factory.ProcessUserNotificationListMessage = function (message) {
            var notifications = message.objectList;

            propertyModel.UserNotifications = {};
            if (notifications) {
                angular.forEach(notifications, function (notification) {
                    var newNotification = {
                        name: notification.objnam,
                        id: notification.params.id,
                        duration: Number(notification.params.duration),
                        enabled: notification.params.duration === "0" ? "OFF" : "ON",
                        valid: true,
                        type: notification.params.type,
                    };
                    //TODO: do we need to 'STORE' this object?
                    //storeObject("UserNotifications", newObject);
                    propertyModel.UserNotifications[newNotification.name] = newNotification;
                });
            }
            $rootScope.$broadcast('UsersNotificationsUpdated', message);
        };

        factory.ProcessConfiguration = function (config) {
            if (!PWC.isEmptyObject(config) && config.answer) {
                angular.forEach(config.answer, function (obj) {
                    recurseNewObjects(obj, null, null);
                });
            }
        };

        factory.processHardwareDefinition = function (result) {
            if (!PWC.isEmptyObject(result) && result.answer) {
                angular.forEach(result.answer, function (obj) {
                    recurseNewObjects(obj, null, null);
                });
            }
        };

        factory.ProcessNotification = function (message) {
            if (!PWC.isEmptyObject(message) && message.command) {
                var command = message.command.toUpperCase();

                switch (command) {
                    case 'APIRESPONSE':
                        factory.ProcessUserListMessage(message);
                        break;
                    case 'USERLIST':
                        factory.ProcessUserListMessage(message);
                        break;
                    case 'USERNOTIFICATIONLIST':
                        factory.ProcessUserNotificationListMessage(message);
                        break;
                    case 'WRITESTATUSMESSAGE':
                        processStatusMessage(message);
                        break;
                    case 'OBJECTCREATED':
                        detectObjectCreation(message);
                        break;
                    case 'WRITEHISTORYMESSAGE':
                        factory.ProcessHistoryMessage(message);
                        break
                }

                if (message.objectList) {
                    angular.forEach(message.objectList, function (notification) {
                        switch (command) {
                            case "SETCOMMAND":
                                // processing setcommand
                                processingSetCommand(notification); // 
                                break;

                            case 'WRITEPARAMLIST':
                                //console.log("-----Processing: " + command);
                                // checking for changes
                                if (notification.changes) {
                                    notifyExistingObject(notification.changes[0]);
                                    // broadcast to all opened windows
                                    $rootScope.$broadcast('WriteParamList', notification.changes[0]);
                                }
                                
                                if (notification.objnam && notification.objnam === '_57A7') {
                                    factory.ProcessStatusMaster(notification);
                                }
                                if (!detectExistingObject(notification)) {
                                    detectNewObject(notification);
                                }
                                // checking deleted objects
                                detectObjectDeletion(notification);
                                // checking created objects

                                // processing writeparamlist
                                processingWriteParamList(notification);

                                if (command === 'NOTIFYLIST') { // notify to exising objects
                                    notifyExistingObject(notification);
                                }
                                
                                break;

                            case 'NOTIFYLIST':
                            case 'SENDPARAMLIST':
                                //console.log("-----Processing: " + command);
                                if (notification.objnam && notification.objnam === '_57A7') {
                                    factory.ProcessStatusMaster(notification);
                                }
                                if (notification.objnam && notification.objnam === '_5451') {
                                    processFreezeObject(notification);

                                    processServiceObject(notification);

                                    processFirmwareUpdateObject(notification);

                                    //factory.ProcessSystemConfig(notification)
                                }
                                if (notification.objnam && notification.objnam === '_C10C') {
                                    console.log("-----Processing CLOCK _C10C: " );
                                    processClockObject(notification);
                                }
                                if (notification.objnam && notification.objnam === '_C105') {
                                    console.log("-----Processing _C105: ");
                                    notifyExistingObject(notification);
                                }
                                
                                if (notification.objnam.substring(0, 3) === "SCH") {    // check if object is schedule                        
                                    // update schedule
                                    notifyListSchedule(notification)
                                }

                                if (!detectExistingObject(notification)) {
                                    detectNewObject(notification);
                                }

                                if (notification.objnam && notification.objnam === 'X0046') {   // FREEZE 
                                    console.log("-----Processing X0046: ");
                                    notifyExistingObject(notification);
                                }

                                detectObjectDeletion(notification);
                                
                                notifyExistingObject(notification);


                                break;
                            case 'SYSTEMINFO':
                                factory.ProcessSystemInfo(notification);
                                break;
                        }
                    });

                    switch(command){
                        case 'SENDPARAMLIST':
                            $rootScope.$broadcast('CircuitUpdate', message);
                            break;
                        case 'WRITEPARAMLIST':
                            // a new list has been updated please update parameters : delete, remove, add items
                            $rootScope.$broadcast('UpdateParamList', message);
                            break;
                    }

                }
            }
            socketService.sendPendingSubscriptionRequest();
        };

        factory.ProcessStatusMaster = function (statusMaster) {
               
            if (statusMaster.params && statusMaster.params.PARTY && statusMaster.params.PARTY.length > 0) {
                var statusObjNames = statusMaster.params.PARTY.split(' ');
                var objList = statusObjNames.map(function (statusObjName) {
                    if (!propertyModel.Objects[statusObjName]) {
                        return {
                            objnam: statusObjName,
                            keys: ['SNAME', 'TIME', 'SHOMNU']
                        };
                    }
                });
                var message = {
                    command: 'RequestParamList',
                    objectList: objList
                };
                if (objList && objList.length > 0 && objList[0]) {
                    socketService.sendMessage(message);
                }
            }
        };

        factory.ProcessPanel = function (panel) {
            if (panel && panel.params) {
                var newObject = {
                    OBJNAM: panel.objnam,
                    LISTORD: panel.params.LISTORD,
                    SUBTYP: panel.params.SUBTYP,
                    HNAME: panel.params.HNAME,
                    SNAME: panel.params.SNAME,
                    PANID: panel.params.PANID
                };
                if (panel.params.PANID && panel.params.SUBTYP) {   // Check if existing
                    if (panel.params.SUBTYP === 'OCP')  // SET PANEL id if subtype is OCP only
                        propertyModel.PANID = panel.params.PANID;
                    if (propertyModel.PANID != "SINGLE") {  // remove single bodies if PANID is not single
                       // var clear = [];
                       // propertyModel.SingleBodies = clear;
                    }
                }
                storeObject("Panels", newObject);
            }
        };

        factory.ProcessModule = function (module) {
            if (module && module.params) {
                var newObject = {
                    OBJNAM: module.objnam,
                    OBJTYP: module.params.OBJTYP,
                    SUBTYP: module.params.SUBTYP,
                    SNAME: module.params.SNAME,
                    LISTORD: module.params.LISTORD,
                    PORT: module.params.PORT,
                    PARENT: module.params.PARENT,
                    READY: module.params.READY
                };

                storeObject("Modules", newObject);
            }
        };

        factory.ProcessRelay = function (relay) {
            if (relay && relay.params) {
                var newObject = {
                    OBJNAM: relay.objnam,
                    OBJTYP: relay.params.OBJTYP,
                    SUBTYP: relay.params.SUBTYP,
                    SNAME: relay.params.SNAME,
                    PANEL: relay.params.PANEL,
                    PARENT: relay.params.PARENT,
                    RUNON: relay.params.RUNON,
                    STATUS: relay.params.STATUS,
                    SOURCE: relay.params.SOURCE,
                    LISTORD: relay.params.LISTORD,
                    CIRCUIT: relay.params.CIRCUIT
                };

                storeObject("Relays", newObject);
            }
        };

        //TODO:Review how we are handling security tokens in the dictionary. OBJNAM was added to ensure it is stored but token.value is also the OBJNAM
        factory.ProcessSecurityToken = function (token) {
            if (token && token.params) {
                var newObject = {
                    OBJNAM: token.objnam,
                    value: token.objnam,
                    name: token.params.SNAME,
                    SNAME: token.params.SNAME,
                    SHOMNU: token.params.SHOMNU,
                    showMenu: token.params.SHOMNU,
                    enable: token.params.ENABLE,
                    ENABLE: token.params.ENABLE,
                    PASSWRD: token.params.PASSWRD,
                    TIMOUT: token.params.TIMOUT
                };
                if (token.objnam !== 'UFFFD' && token.objnam !== 'UFFFF' && token.params.SNAME !== '') {
                    storeObject("Security", newObject);
                }
                if (token.objnam === 'UFFFE') {
                    if (token.params.ENABLE === 'ON') {
                        propertyModel.SecurityEnabled = true;
                    } else {
                        propertyModel.SecurityEnabled = false;
                    }
                }
                if (token.objnam === 'U0000') {
                    if (token.params.ENABLE === 'ON') {
                        propertyModel.GuestEnabled = true;
                    } else {
                        propertyModel.GuestEnabled = false;
                    }
                }
            }
        };

        factory.ProcessSecurityTokens = function (tokens) {
            if (tokens && tokens.objectList) {
                // reorder values
                var count = tokens.objectList.length;
                if (count >= 2) {
                    // save on temporaty
                    var temp = tokens.objectList[1];
                    var last = count - 1
                    // save administrator
                    tokens.objectList[1] = tokens.objectList[last];
                    // exchange last object
                    tokens.objectList[last] = temp;
                }
                propertyModel.Security = [];
                angular.forEach(tokens.objectList, function (token) {
                    factory.ProcessSecurityToken(token);
                });
                $rootScope.$broadcast('SecurityUpdated', tokens);
            }
        };

        factory.ProcessBody = function (body) {
            if (body && body.params) {
                var newObject = {
                    OBJNAM: body.objnam,
                    SNAME: body.params.SNAME,
                    TEMP: body.params.TEMP,
                    HITMP: body.params.HITMP,
                    LOTMP: body.params.LOTMP,
                    LISTORD: body.params.LISTORD,
                    OBJTYP: body.params.OBJTYP,
                    SUBTYP: body.params.SUBTYP,
                    HEATER: body.params.HEATER,
                    SHARE: body.params.SHARE,
                    PRIM: body.params.PRIM,
                    SEC: body.params.SEC,
                    ACT1: body.params.ACT1,
                    ACT2: body.params.ACT2,
                    ACT3: body.params.ACT3,
                    ACT4: body.params.ACT4,
                    VOL: body.params.VOL,
                    MODULE: body.params.MODULE,
                    PANEL: body.params.PANEL,
                    PANID: body.params.PANID,
                    FILTER: body.params.FILTER,
                    //HTSRCTYP: body.params.HTSRCTYP
                };

                storeBody(newObject);
            }
        };


        factory.ProcessUpdatedBody = function (body) {
            if (body && body.params) {
                var broadcast = false;
                var newObject = {
                    OBJNAM: body.objnam,
                    SNAME: body.params.SNAME,
                    TEMP: body.params.TEMP,
                    HITMP: body.params.HITMP,
                    LOTMP: body.params.LOTMP,
                    LISTORD: body.params.LISTORD,
                    OBJTYP: body.params.OBJTYP,
                    SUBTYP: body.params.SUBTYP,
                    HEATER: body.params.HEATER,
                    SHARE: body.params.SHARE,
                    PRIM: body.params.PRIM,
                    SEC: body.params.SEC,
                    ACT1: body.params.ACT1,
                    ACT2: body.params.ACT2,
                    ACT3: body.params.ACT3,
                    ACT4: body.params.ACT4,
                    VOL: body.params.VOL,
                    MODULE: body.params.MODULE,
                    PANEL: body.params.PANEL,
                    PANID: body.params.PANID,
                    FILTER: body.params.FILTER,
                };

                broadcast = true;
            }
            if (broadcast) {
                $rootScope.$broadcast('UpdatedBody', newObject);
            }
        };

        //--------
        factory.ProcessCircuitUpdate = function (circuit) {
            if (circuit && circuit.params) {
                var broadcast = false;
                var USE_COLOR = "WHITER"    
                if (circuit.params.USE)
                    USE_COLOR = circuit.params.USE
                // Process Circuit
                var newObject = {
                    OBJNAM: circuit.objnam,
                    OBJTYP: circuit.params.OBJTYP,
                    HNAME: circuit.params.HNAME,
                    SNAME: circuit.params.SNAME,
                    SHOMNU: circuit.params.SHOMNU,
                    STATUS: circuit.params.STATUS,
                    SUBTYP: circuit.params.SUBTYP,
                    SOURCE: circuit.params.SOURCE,
                    TIMOUT: circuit.params.TIMOUT,
                    USAGE: circuit.params.USAGE,
                    ACT: circuit.params.ACT,
                    FREEZE: circuit.params.FREEZE,
                    FEATR: circuit.params.FEATR,
                    LISTORD: circuit.params.LISTORD,
                    DNTSTP: circuit.params.DNTSTP,
                    TIME: circuit.params.TIME,
                    BODY: circuit.params.BODY,
                    RLY: circuit.params.RLY,
                    USE: USE_COLOR,
                    Delay: {
                        VISIBLE: false,
                        MESSAGE: ""
                    }
                };

                if (newObject.SUBTYP === 'CIRCGRP') {
                    factory.ProcessCircuitGroup(circuit)
                }else if (newObject.SUBTYP === 'LITSHO') {
                    factory.ProcessCircuitGroupLights(circuit)
                    $rootScope.$broadcast('UPDATE_CIRCUITS', circuit, "GROUPS");
                }
                else {
                    //console.log(" ================ ----------------------------------------- OBJECT NAME =" + newObject.SNAME);
                    storeObject("Circuits", newObject);
                    broadcast = true;
                }
                if (broadcast) {
                    $rootScope.$broadcast('CircuitsUpdated', newObject);
                }
                // NEW FEATURE CIRCUIT
                if (newObject.SUBTYP === 'CIRCUIT' || newObject.RLY === '00000') {
                    // broadcast adding new feature
                    $rootScope.$broadcast('UPDATE_CIRCUITS', newObject, "FEATURES");
                }

                if ((newObject.SUBTYP === 'CIRCGRP') || (newObject.SUBTYP === 'LITSHO')) {
                    $rootScope.$broadcast('UPDATE_CIR_GRPS', newObject, "GROUPS");
                }
            }
        };
        //--------

        factory.ProcessCircuit = function (circuit) {
            if (circuit && circuit.params) {
                var broadcast = false;
                if ((circuit.objnam === "C0001") || (circuit.objnam === "C0006")) {
                    circuit = circuit
                }

                var newObject = {
                    OBJNAM: circuit.objnam,
                    OBJTYP: circuit.params.OBJTYP,
                    HNAME: circuit.params.HNAME,
                    SNAME: circuit.params.SNAME,
                    SHOMNU: circuit.params.SHOMNU,
                    STATUS: circuit.params.STATUS,
                    SUBTYP: circuit.params.SUBTYP,
                    SOURCE: circuit.params.SOURCE,
                    TIMOUT: circuit.params.TIMOUT,
                    USAGE: circuit.params.USAGE,
                    ACT: circuit.params.ACT,
                    FREEZE: circuit.params.FREEZE,
                    FEATR: circuit.params.FEATR,
                    LISTORD: circuit.params.LISTORD,
                    DNTSTP: circuit.params.DNTSTP,
                    TIME: circuit.params.TIME,
                    BODY: circuit.params.BODY,
                    RLY: circuit.params.RLY,
                    Delay: {
                        VISIBLE: false,
                        MESSAGE: ""
                    }
                };

                if (newObject.SUBTYP === 'CIRCGRP' ) {
                    factory.ProcessCircuitGroup(circuit)
                    $rootScope.$broadcast('UPDATE_CIRCUITS', circuit, "GROUPS");
                } else
                if (newObject.SUBTYP === 'LITSHO') {
                    factory.ProcessCircuitGroupLights(circuit)
                    $rootScope.$broadcast('UPDATE_CIRCUITS', circuit, "GROUPS");
                } else {                    
                    //console.log(" ================ ----------------------------------------- OBJECT NAME =" + newObject.SNAME);
                    storeObject("Circuits", newObject);
                    broadcast = true;
                }
                if (broadcast) {
                    $rootScope.$broadcast('CircuitsUpdated', newObject);
                }
            }
        };


        factory.ProcessCircuitGroupLights = function (circuitGroup) {
            if (circuitGroup && circuitGroup.params) {
                var newObject = {
                    OBJNAM: circuitGroup.objnam,
                    OBJTYP: circuitGroup.params.OBJTYP,
                    SNAME: circuitGroup.params.SNAME,
                    SHOMNU: circuitGroup.params.SHOMNU,
                    STATUS: circuitGroup.params.STATUS,
                    USAGE: circuitGroup.params.USAGE,
                    SUBTYP: circuitGroup.params.SUBTYP,
                    SOURCE: circuitGroup.params.SOURCE,
                    TIMOUT: circuitGroup.params.TIMOUT,
                    CHILD: circuitGroup.params.CHILD,
                    OBJLIST: circuitGroup.params.OBJLIST,
                    //SELECT: circuitGroup.params.SELECT,
                    FREEZE: circuitGroup.params.FREEZE,
                    USE: circuitGroup.params.USE,
                    Delay: {
                        VISIBLE: false,
                        MESSAGE: ""
                    }
                };

                //Do not store All Lights Object
                if (newObject.OBJNAM !== enumerations.AllLightsObj) {
                    storeObject("CircuitGroups", newObject);
                }
            }
        }

        factory.ProcessCircuitGroup = function (circuitGroup) {
            if (circuitGroup && circuitGroup.params) {
                var newObject = {
                    OBJNAM: circuitGroup.objnam,
                    OBJTYP: circuitGroup.params.OBJTYP,
                    SNAME: circuitGroup.params.SNAME,
                    SHOMNU: circuitGroup.params.SHOMNU,
                    STATUS: circuitGroup.params.STATUS,
                    USAGE: circuitGroup.params.USAGE,
                    SUBTYP: circuitGroup.params.SUBTYP,
                    SOURCE: circuitGroup.params.SOURCE,
                    TIMOUT: circuitGroup.params.TIMOUT,
                    CHILD: circuitGroup.params.CHILD,
                    //SELECT: circuitGroup.params.SELECT,
                    FREEZE: circuitGroup.params.FREEZE,
                    Delay: {
                        VISIBLE: false,
                        MESSAGE: ""
                    }
                };

                //Do not store All Lights Object
                if (newObject.OBJNAM !== enumerations.AllLightsObj) {
                    storeObject("CircuitGroups", newObject);
                }
            }
        }

        factory.ProcessSensor = function (sensor) {
            if (sensor && sensor.params) {
                switch (sensor.params.SUBTYP) {
                    case 'AIR':
                        factory.ProcessAirSensor(sensor)
                        break;

                    case 'POOL':
                    case 'WATER':
                        factory.ProcessWaterSensor(sensor)
                        break;
                    case 'SOLAR':
                        factory.ProcessSolarSensor(sensor)
                        break;
                }
            }
        }

        factory.ProcessAirSensor = function (airSensor) {
            if (airSensor && airSensor.params) {
                var newObject = {
                    OBJNAM: airSensor.objnam,
                    OBJTYP: airSensor.params.OBJTYP,
                    STATUS: airSensor.params.STATUS,
                    PROBE: airSensor.params.PROBE,
                    SUBTYP: airSensor.params.SUBTYP,
                    CALIB: airSensor.params.CALIB,
                    NAME: airSensor.params.SNAME,
                    BODY: airSensor.params.BODY,
                    MODULE: airSensor.params.MODULE,
                    PANEL: airSensor.params.PANEL,
                    SOURCE: airSensor.params.SOURCE

                };

                storeObject("AirSensors", newObject);
            }
        };

        factory.ProcessWaterSensor = function (waterSensor) {
            if (waterSensor && waterSensor.params) {
                var newObject = {
                    OBJNAM: waterSensor.objnam,
                    OBJTYP: waterSensor.params.OBJTYP,
                    STATUS: waterSensor.params.STATUS,
                    PROBE: waterSensor.params.PROBE,
                    SUBTYP: waterSensor.params.SUBTYP,
                    CALIB: waterSensor.params.CALIB,
                    NAME: waterSensor.params.SNAME,
                    BODY: waterSensor.params.BODY,
                    MODULE: waterSensor.params.MODULE,
                    PANEL: waterSensor.params.PANEL,
                    SOURCE: waterSensor.params.SOURCE
                };

                storeObject("WaterSensors", newObject);
            }
        };

        factory.ProcessSolarSensor = function (solarSensor) {
            if (solarSensor && solarSensor.params) {
                var newObject = {
                    OBJNAM: solarSensor.objnam,
                    OBJTYP: solarSensor.params.OBJTYP,
                    STATUS: solarSensor.params.STATUS,
                    PROBE: solarSensor.params.PROBE,
                    SUBTYP: solarSensor.params.SUBTYP,
                    CALIB: solarSensor.params.CALIB,
                    NAME: solarSensor.params.SNAME,
                    BODY: solarSensor.params.BODY,
                    MODULE: solarSensor.params.MODULE,
                    PANEL: solarSensor.params.PANEL,
                    SOURCE: solarSensor.params.SOURCE
                };

                storeObject("SolarSensors", newObject);
            }
        };

        factory.ProcessHeater = function (heater) {
            if (heater && heater.params) {
                var newObject = heater.params;
                newObject.OBJNAM = heater.objnam;

                // set default name.
                if (heater.params && heater.params.SNAME) {
                    newObject.DISPLAY = heater.params.SNAME;
                    newObject.ORDER = 0 // THE POSITION TO DISPLAY THIS ITEM
                }

                // Update display names
                
                if (heater.params.SUBTYP === "GENERIC") {          // GAS HEATER
                    newObject.DISPLAY = "Heater"
                    newObject.ORDER = 1 // THE POSITION TO DISPLAY THIS ITEM
                }

                if (heater.params.OBJNAM === "HXSLR") {           // SOLAR PREFERRED
                    newObject.DISPLAY = 'Solar Preferred'
                    newObject.ORDER = 3 // THE POSITION TO DISPLAY THIS ITEM
                }

                if (heater.params.SUBTYP === 'SOLAR') {            // SOLAR ONLY
                    newObject.DISPLAY = "Solar Only"
                    newObject.ORDER = 2 // THE POSITION TO DISPLAY THIS ITEM
                }

                if (heater.params.OBJNAM === "HXHTP") {            // HEAT PUMP PREFERRED
                    newObject.DISPLAY = 'Heat Pump Preferred'
                    newObject.ORDER = 5 // THE POSITION TO DISPLAY THIS ITEM
                }

                if (heater.params.SUBTYP === 'HTPMP') {// HEAT PUMP ONLY
                    newObject.DISPLAY = 'Heat Pump Only'
                    newObject.ORDER = 4 // THE POSITION TO DISPLAY THIS ITEM
                }

                if (heater.params.OBJNAM === "HXULT") {            // ULTRATEMP PREFERRED
                    newObject.DISPLAY = 'UltraTemp Preferred'
                    newObject.ORDER = 5 // THE POSITION TO DISPLAY THIS ITEM
                }

                if (heater.params.SUBTYP === "ULTRA") {            // ULTRATEMP
                    newObject.DISPLAY = 'UltraTemp Only'
                    newObject.ORDER = 4 // THE POSITION TO DISPLAY THIS ITEM
                }

                if (heater.params.SUBTYP === "HCOMBO") {           // ULTRATEMP PREFERRED
                    newObject.DISPLAY = 'Hybrid'
                    newObject.ORDER = 8 // THE POSITION TO DISPLAY THIS ITEM
                }

                if (heater.params.SUBTYP === "MASTER") {           // ULTRATEMP PREFERRED
                    newObject.DISPLAY = 'MasterTemp'
                    newObject.ORDER = 6 // THE POSITION TO DISPLAY THIS ITEM
                }

                if (heater.params.SUBTYP === "MAX") {           // ULTRATEMP PREFERRED
                    newObject.DISPLAY = 'Max-E-Therm'
                    newObject.ORDER = 7 // THE POSITION TO DISPLAY THIS ITEM
                }

                storeObject("Heaters", newObject);

                linkHeaterToBody(newObject);

            }
        };


        factory.ProcessStatus = function (status) {
            if (status && status.params) {
                var newObject = status.params;
                newObject.OBJNAM = status.objnam;

                storeObject("Alerts", newObject);

                linkHeaterToBody(newObject);

            }
        };

        factory.ProcessSingleton = function (obj) {
            if (obj && obj.params) {
                switch (obj.objnam) {
                    case '_5451':
                        factory.ProcessSystemConfig(obj)
                        break;
                    case '_C10C':
                        detectLocationObject(obj);
                        //detectClockObject(obj);
                        break;
                    case '_C105':
                        detectClockObject(obj);
                        break;

                    case 'UFFFE':
                        detectSecurityObject(obj);
                        break;
                }
            }
        }

        factory.ProcessChemistry = function (chem) {
            if (chem && chem.params) {
                var newObject = chem.params;
                newObject.OBJNAM = chem.objnam;
 
                storeObject("Chemistry", newObject);

                linkChemistryToBody(newObject);
            }
        };
        factory.ProcessRemote = function (remote) {
            if (remote && remote.params) {
                var newObject = {
                    OBJNAM: remote.objnam,
                    OBJTYP: remote.params.OBJTYP,
                    SUBTYP: remote.params.SUBTYP,
                    ENABLE: remote.params.ENABLE,
                    SNAME: remote.params.SNAME,
                };

                storeObject("Remotes", newObject);
            }
        };


        factory.ProcessPumpAffect = function (pumpAffect) {
            if (pumpAffect && pumpAffect.params) {
                var newObject = {
                    OBJNAM: pumpAffect.objnam,
                    OBJTYP: pumpAffect.params.OBJTYP,
                    CIRCUIT: pumpAffect.params.CIRCUIT,
                    LISTORD: pumpAffect.params.LISTORD,
                    SPEED: pumpAffect.params.SPEED,                    
                    BOOST: pumpAffect.params.BOOST,
                    SELECT: pumpAffect.params.SELECT,
                    PARENT: pumpAffect.params.PARENT
                };

                storeObject("PumpAffects", newObject);
            }
        };

        factory.ProcessValve = function (valve) {
            if (valve && valve.params) {
                var newObject = {
                    OBJNAM: valve.objnam,
                    OBJTYP: valve.params.OBJTYP,
                    SUBTYP: valve.params.SUBTYP,
                    DLY: valve.params.DLY
                };

                storeObject("Valves", newObject);
            }
        };

        factory.ProcessPump = function (pump) {
            if (pump && pump.params) {
                var newObject = {
                    OBJNAM: pump.objnam,
                    OBJTYP: pump.params.OBJTYP,
                    SUBTYP: pump.params.SUBTYP,
                    SNAME: pump.params.SNAME,
                    COMUART: pump.params.COMUART,
                    PRIMFLO: pump.params.PRIMFLO,
                    PRIMTIM: pump.params.PRIMTIM,
                    SYSTIM: pump.params.SYSTIM,
                    MIN: pump.params.MIN,
                    MAX: pump.params.MAX,
                    MINF: pump.params.MINF,     //** ADDED TO SUPPORT VSF
                    MAXF: pump.params.MAXF,     //** ADDED TO SUPPORT VSF
                    RPM: pump.params.RPM,
                    GPM: pump.params.GPM,
                    PRIOR: pump.params.PRIOR,
                    BODY: pump.params.BODY,
                    SHARE: pump.params.SHARE,
                    DLY: pump.params.SHARE,
                    SETTMP: pump.params.SETTMP,
                    SETTMPNC: pump.params.SETTMPNC,
                    pumpAffect:[]
                };
                
                if (newObject.SETTMP) {     // update SETMP is 10 times value
                    newObject.SETTMP = newObject.SETTMP ;
                }
                if (pump.params.OBJLIST) {  // check if objectlist exist first for SINGLE PUMPS
                    if (pump.params.OBJLIST.length > 0) {
                        angular.forEach(pump.params.OBJLIST, function (item) {
                            var objectItem = {
                                OBJNAM: item.objnam,
                                BOOST: item.params.BOOST,
                                CIRCUIT: item.params.CIRCUIT,
                                LISTORD: item.params.LISTORD,
                                SELECT: item.params.SELECT,
                                SPEED: item.params.SPEED,
                                PARENT: pump.objnam,
                                STATIC: "OFF"
                            };

                            newObject.pumpAffect.push(objectItem);

                            // adding tp pumpAffect feb-14-2018
                            var newObjectAffect = {
                                OBJNAM: item.objnam,
                                OBJTYP: pump.params.OBJTYP,
                                CIRCUIT: item.params.CIRCUIT,
                                LISTORD: item.params.LISTORD,
                                SPEED: item.params.SPEED,
                                BOOST: item.params.BOOST,
                                SELECT: item.params.SELECT,
                                PARENT: pump.objnam
                            };

                            storeObject("PumpAffects", newObjectAffect);                                                       
                        });
                    }
                }
                storeObject("Pumps", newObject);
            }
        };

        factory.ProcessClock = function (clock) {
            if (clock && clock.params) {                                
                
                if (propertyModel.Objects[clock.objnam]) {
                    if (clock.params.MIN) {
                        propertyModel.Objects[clock.objnam].MIN = clock.params.MIN;
                    }
                }
                
            }
        }

        factory.ProcessFreeze = function (obj) {
            if (obj && obj.params) {
                if (obj.params.FRZ) {
                    //console.log("-------------------------------------- FREEZE VALUE :", obj.params.FRZ);
                    propertyModel.freezeStatus = obj.params.FRZ;
                    // set X0046 status
                    if (propertyModel.Objects["X0046"]) {   // modify the status for this
                        if (obj.params.FRZ === "1")
                            propertyModel.Objects["X0046"].STATUS = "ON";
                        else
                            propertyModel.Objects["X0046"].STATUS = "OFF";
                    }
                }
            }
        };


        factory.ProcessService = function (obj) {
            if (obj && obj.params) {
                if (obj.params.SERVICE) {
                    //console.log("-------------------------------------- SERVICE VALUE :", obj.params.SERVICE);
                    propertyModel.serviceStatus = obj.params.SERVICE;
                }
            }
        };

        factory.ProcessFirmwareUpdate = function (obj) {
            if (obj && obj.params) {
                if (obj.params.UPDATE) {
                   // console.log("-------------------------------------- FIRMWARE UPDATE VALUE :", obj.params.UPDATE);

                    //obj.params.UPDATE; // here is the new version. formatted as YES:1.012
                    var yesPos = obj.params.UPDATE.search("YES");   // "YES" position
                    var versionPos = obj.params.UPDATE.search(":");   // "YES" position 
                    
                    if (yesPos === 0) {
                        propertyModel.firmwareUpdateStatus = "YES";
                        if (versionPos > 0) {
                            propertyModel.firmwareUpdateVersion = obj.params.UPDATE.substring(versionPos+1, 10);                            
                        }
                    }
                    var noPos = obj.params.UPDATE.search("NO");   // "YES" position
                    if (noPos === 0) {
                        propertyModel.firmwareUpdateStatus = "NO";
                    }
                    var noPos = obj.params.UPDATE.search("PROGRES");   // "YES" position
                    if (noPos === 0) {
                        propertyModel.firmwareUpdateStatus = "PROGRES";
                    }
                       
                }
                // process user clicks ON REMOTE DEVICE yes/no on OCP,ICP or WCP
                if (obj.params.IN) {
                    //console.log("-------------------------------------- REMOTE FIRMWARE IN VALUE :", obj.params.IN);
                    var yesPosRemote = obj.params.IN.search("YES");   // "YES" position
                    if (yesPosRemote === 0) {

                    }
                    yesPosRemote = obj.params.IN.search("NO");   // "YES" position
                    if (yesPosRemote === 0) {
                        propertyModel.firmwareUpdateStatus = "NO";
                    }
                    $rootScope.$broadcast('UpdateFirmwareIn', obj);     // now broadcast time update
                }
            }

        };

        factory.ProcessClockSource = function (source) {
            if (source && source.params) {                
                if (source.params.OBJTYP && source.params.SOURCE) {                                                
                    var newObject = {
                        OBJNAM: source.params.OBJTYP,
                        SOURCE: source.params.SOURCE
                    }
                    propertyModel.Objects[source.objnam] = newObject;
                }                
            }
        }

        factory.ProcessCheckLostCommunication = function (params) {
            //  adding status for communication lost
            if (params.SNAME) { // check if exist 
                if (params.SNAME.indexOf("Communication Lost") > -1 ) {
                    // adding object for communication lost
                    propertyModel.lostcommunication.push(params);
                    console.log("**** adding comm lost: ", params)
                }
            }
        }

        factory.ProcessNewStatusMessage = function (obj) {
            if (obj && obj.objnam) {
                // processing just status messages
                if (obj.params.OBJTYP && obj.params.OBJTYP === 'STATUS') {
                    var newObject = obj.params;
                    propertyModel.Objects[obj.objnam] = newObject;

                    // checking  lost communication
                    factory.ProcessCheckLostCommunication(newObject);
  
                }
                
            }
        }

        factory.ProcessLocation = function (location) {
            if (location && location.params) {
                var date = $filter('translateDate')(location.params.DAY);
                var time = $filter('translateTime')(location.params.MIN);
                var newObject = {
                    OBJNAM: location.objnam,
                    OBJTYP: location.params.OBJTYP,
                    ACCEL: location.params.ACCEL,
                    ZIP: location.params.ZIP,
                    LOCX: location.params.LOCX,
                    LOCY: location.params.LOCY,
                    SNAME: location.params.SNAME,
                    DLSTIM: location.params.DLSTIM,
                    TIMZON: location.params.TIMZON,
                    OFFSET: location.params.OFFSET,
                    CLK24A: location.params.CLK24A,
                    TIME: location.params.TIME,
                    MIN: time,
                    DAY: date,
                    SRIS: location.params.SRIS,
                    SSET: location.params.SSET
                }
                //TODO: do we need to 'STORE' this object?
                // storeObject("Location", newObject);
                propertyModel.Objects[newObject.OBJNAM] = newObject;
            }
        }

        factory.ProcessInviteMessage = function (message) {
            propertyModel.InviteStatus = {
                Status: message.status,
                Message: message.messageDetail
            };
            $rootScope.$broadcast('InviteProcessed', notification);
        };

        factory.ProcessDelayState = function (circuit, timout, source) {
            if (circuit.STATUS == "DLYON" || circuit.STATUS == "DLYOFF") {
                if (!circuit.Delay.VISIBLE) {
                    getDelayByOBJNAM(circuit.OBJNAM);
                }
                circuit.Delay.VISIBLE = true;
                if (timout && source) {
                    var delay = Number(timout);
                    var units = "seconds";
                    if (delay > 60) {
                        units = "minutes";
                        delay = Math.round(delay / 60);
                    }
                    var value = $filter('intToWord')(delay).toLowerCase();
                    switch (propertyModel.Objects[source].OBJTYP) {
                        case "HEATER":
                            circuit.Delay.MESSAGE = "The pump will turn off after a delay of " + value + " (" + delay + ") " + units + " to ensure the heater has cooled sufficiently to avoid damage to equipment.";
                            break;
                        case "VALVE":
                            circuit.Delay.MESSAGE = "The pump is off for a period of " + value + " (" + delay + ") " + units + " while valves are repositioned.";
                            break;
                        case "PUMP":
                            circuit.Delay.MESSAGE = "The Iflo pump is running at a higher speed for up to " + value + " (" + delay + ") " + units + " in order to prime it for normal operation.";
                            break;
                        default:
                            circuit.Delay.MESSAGE = "The cleaner will start after a delay of " + value + " (" + delay + ") " + units + " to ensure pipes are properly filled.";
                            break;
                    }

                } else {
                    circuit.Delay.MESSAGE = "Loading ...";
                }
            } else {
                if (circuit.delay){
                    circuit.Delay.VISIBLE = false;
                }
            }
        };

        factory.ProcessFeatureObject = function (valve) {
            if (valve && valve.params) {
                var newObject = {
                    OBJNAM: valve.objnam,
                    OBJTYP: valve.params.OBJTYP,
                    MANOVR: valve.params.MANOVR,
                    PERMIT: valve.params.PERMIT
                };

                storeObject("FeatureMenu", newObject);
            }
        };
//// PROCESS HISTORY
        factory.ProcessHistoryMessage_original = function (msg) {
            var result = [];
            // checking if current mode is fahrenheit 
            //var fahrenheit = (propertyModel.Objects["_5451"].MODE === "ENGLISH");  // CHECK IF MODE IS NOT ENGLISH 
            var fahrenheit = true;
            if (propertyModel.Objects["_5451"]) {
                if (propertyModel.Objects["_5451"].MODE === "ENGLISH") {  // CHECK IF MODE IS NOT ENGLISH
                    fahrenheit = true;

                } else {
                    fahrenheit = false;
                }            
            }

            angular.forEach(msg['objectList'], function (object) {
                /*
                var temp_graphData = {
                    poolTemp: [],
                    spaTemp: [],
                    poolSideTemp: [],
                    solarTemp: [],
                    spaUsage: [],
                    poolUsage: [],
                    lightUsage: [],
                    phLevel: [],
                    phFeed: [],
                    orpLevel: [],
                    orpFeed: [],
                    solarUsage: [],
                    heaterUsage: []
                };
                */
                  
                angular.forEach(object, function (historyList, objnam) {
                    var entity = propertyModel.Objects[objnam];

                    if (entity != null) {

                        angular.forEach(historyList, function (history) {
                            var historyEntry = {time: history['T'], value: history['V'], objnam: entity['OBJNAM']};
                            var objTyp1 = entity['OBJTYP'].toUpperCase();
                            switch (entity['OBJTYP'].toUpperCase()) {
                                case 'BODY':
                                    switch (entity["SUBTYP"].toLowerCase()) {
                                        case 'B1101':
                                        case 'pool':
                                            if (history['P'] === 'STATUS') {
                                                temp_graphData.poolUsage.push(historyEntry);
                                            }
                                            else if (history['P'] === 'TEMP') {
                                                                                             
                                                if (fahrenheit) { // raw data is stored in celcius. here we adjusted data if current format is fahrenheit 
                                                    historyEntry.value = ((historyEntry.value * 9) / 5) + 32;
                                                }
                    
                                                temp_graphData.poolTemp.push(historyEntry);
                                            }
                                            break;
                                        case 'B1202':   // share body spa
                                        case 'B1102':
                                        case 'spa':
                                            if (history['P'] === 'STATUS') {
                                                temp_graphData.spaUsage.push(historyEntry);
                                            }
                                            else if (history['P'] === 'TEMP') {
                                                if (fahrenheit) { // raw data is stored in celcius. here we adjusted data if current format is fahrenheit 
                                                    historyEntry.value = ((historyEntry.value * 9) / 5) + 32;
                                                }
                                                temp_graphData.spaTemp.push(historyEntry);
                                            }
                                            break;
                                    }
                                    break;
                                case 'CIRCUIT':
                                    if (entity.SHOMNU && entity.SHOMNU.toLowerCase().indexOf('l') > -1) {
                                        if (history['P'] === 'STATUS') {
                                            temp_graphData.lightUsage.push(historyEntry);
                                        }
                                    }
                                    break;
                                case 'CHM01':
                                case 'CHEM':
                                    switch (history['P']) {
                                        case 'ORPVAL':
                                            temp_graphData.orpLevel.push(historyEntry);
                                            break;
                                        case 'PHVAL':
                                            temp_graphData.phLevel.push(historyEntry);
                                            break;
                                    /* do not display ph feed and orp feed
                                        case 'PHFED':
                                            historyEntry.value = historyEntry.value.toString() === '1' ? 'ON' : 'OFF';
                                            temp_graphData.phFeed.push(historyEntry);
                                            break;
                                        case 'ORPFED':
                                            historyEntry.value = historyEntry.value.toString() === '1' ? 'ON' : 'OFF';
                                            temp_graphData.orpFeed.push(historyEntry);
                                            break;
                                   */
                                    }
                                    break;
                               // AIR [BEGIN]
                                case '_A135':
                                    if (history['P'] === 'TEMP') {
                                        if (fahrenheit) { // raw data is stored in celcius. here we adjusted data if current format is fahrenheit 
                                            historyEntry.value = ((historyEntry.value * 9) / 5) + 32;
                                        }
                                        temp_graphData.solarTemp.push(historyEntry);                                            
                                    }
                                    break;
                                // AIR [END]

                                // SOLAR 1 [BEGIN]
                                case 'SSS11':
                                    switch ( history['P'] ){
                                        case 'PROBE': 
                                            if (fahrenheit) { // raw data is stored in celcius. here we adjusted data if current format is fahrenheit 
                                                historyEntry.value = ((historyEntry.value * 9) / 5) + 32;
                                            }
                                            temp_graphData.solarTemp.push(historyEntry);                                            
                                            break
                                    }
                                    break;
                               // SOLAR 1 [END]

                               // SOLAR 2 [BEGIN]
                               case 'SSS21':
                                    switch (history['P']) {
                                        case 'PROBE':
                                            if (fahrenheit) { // raw data is stored in celcius. here we adjusted data if current format is fahrenheit 
                                                historyEntry.value = ((historyEntry.value * 9) / 5) + 32;
                                            }
                                            temp_graphData.solarTemp.push(historyEntry);
                                            break
                                    }
                                    break;
                              // SOLAR 2 [END]

                                case 'HEATER':
                                    if (entity['SUBTYP'] === 'SOLAR' && history['P'] === 'STATUS') {
                                        temp_graphData.solarUsage.push(historyEntry);
                                    } else if (history['P'] === 'STATUS') {
                                        temp_graphData.heaterUsage.push(historyEntry);
                                    }
                                    break;
                                
                                case 'SENSE' :
                                    if (history['P'] === 'PROBE') {
                                        switch (entity["SUBTYP"].toLowerCase()) {
                                            // SUPPORT AIR SAMPLES
                                            case '_A135':
                                            case 'air':
                                                temp_graphData.poolSideTemp.push(historyEntry);
                                                break;
                                            case 'SSS11': // solar1 sensor 
                                            case 'SSS21': // solar2 sensor                                             
                                            case 'solar':
                                                if (fahrenheit) { // raw data is stored in celcius. here we adjusted data if current format is fahrenheit 
                                                    historyEntry.value = ((historyEntry.value * 9) / 5) + 32;
                                                }
                                                temp_graphData.solarTemp.push(historyEntry);
                                                break;
                                        }
                                    }
                                    break;
                            }
                        });
                    }
                });

                //Combine Results by body of water
                result = combineHistoryByBodyOfWater(result, temp_graphData);
            });

            propertyModel.History = result;
        };



        factory.ProcessHistoryMessage = function (msg) {
            var result = [];
            // checking if current mode is fahrenheit 
            
            var fahrenheit = true;
            var fahrenheit = (propertyModel.Objects["_5451"].MODE === "ENGLISH");  // CHECK IF MODE IS NOT ENGLISH 

            angular.forEach(msg['objectList'], function (object) {

                angular.forEach(object, function (historyList, objnam) {
                    var entity = propertyModel.Objects[objnam];

                    if (entity != null) {

                        angular.forEach(historyList, function (history) {
                            var historyEntry = { time: history['T'], value: history['V'], objnam: entity['OBJNAM'] };
                            var objTyp1 = entity['OBJTYP'].toUpperCase();
                            switch (objnam) {
                                
                                case 'B1101':                    
                                    if (history['P'] === 'STATUS') {
                                        temp_graphData.poolUsage.push(historyEntry);
                                    }
                                    else if (history['P'] === 'TEMP') {

                                        if (fahrenheit) { // raw data is stored in celcius. here we adjusted data if current format is fahrenheit 
                                            historyEntry.value = ((historyEntry.value * 9) / 5) + 32;
                                        }

                                       temp_graphData.poolTemp.push(historyEntry);
                                    }
                                    break;
                                case 'B1202':   // share body spa
                                case 'B1102':                                
                                    if (history['P'] === 'STATUS') {
                                        temp_graphData.spaUsage.push(historyEntry);
                                    }
                                    else if (history['P'] === 'TEMP') {
                                        if (fahrenheit) { // raw data is stored in celcius. here we adjusted data if current format is fahrenheit 
                                            historyEntry.value = ((historyEntry.value * 9) / 5) + 32;
                                        }
                                        temp_graphData.spaTemp.push(historyEntry);
                                    }
                                    break;

                                case 'CHM01':
                                    switch (history['P']) {
                                        case 'ORPVAL':
                                            temp_graphData.orpLevel.push(historyEntry);

                                            //console.log("*************** ORP VALUE: ", historyEntry)
                                            break;
                                        case 'PHVAL':
                                            if (historyEntry.value > 100) {
                                                historyEntry.value = historyEntry.value / 100;
                                            }
                                            //console.log("*************** PH VALUE: ", historyEntry)
                                            temp_graphData.phLevel.push(historyEntry);
                                           // console.log("*************** PH VALUE: ", historyEntry)
                                            break;
                                            /* do not display ph feed and orp feed
                                                case 'PHFEED':
                                                    historyEntry.value = historyEntry.value.toString() === '1' ? 'ON' : 'OFF';
                                                    temp_graphData.phFeed.push(historyEntry);
                                                    break;
                                                case 'ORPFEED':
                                                    historyEntry.value = historyEntry.value.toString() === '1' ? 'ON' : 'OFF';
                                                    temp_graphData.orpFeed.push(historyEntry);
                                                    break;
                                          */
                                    }
                                    break;
                                case '_A135':
                                    // FIXING AIR TEMPERATURE : 
                                    if (history['P'] === 'PROBE') { //if (history['P'] === 'TEMP') {
                                        if (fahrenheit) { // raw data is stored in celcius. here we adjusted data if current format is fahrenheit 
                                            historyEntry.value = ((historyEntry.value * 9) / 5) + 32;
                                            

                                        }
                                        
                                        temp_graphData.poolSideTemp.push(historyEntry);
                                    }
                                    break;
                                case 'SSS11':
                                    switch (history['P']) {
                                        case 'PROBE':
                                            if (fahrenheit) { // raw data is stored in celcius. here we adjusted data if current format is fahrenheit 
                                                historyEntry.value = ((historyEntry.value * 9) / 5) + 32;
                                            }                                            
                                            temp_graphData.solarTemp.push(historyEntry);
                                            break
                                    }
                                    break;

                                case 'SSS21':
                                    switch (history['P']) {
                                        case 'PROBE':
                                            if (fahrenheit) { // raw data is stored in celcius. here we adjusted data if current format is fahrenheit 
                                                historyEntry.value = ((historyEntry.value * 9) / 5) + 32;
                                            }                                            
                                            temp_graphData.solarTemp.push(historyEntry);
                                            break
                                    }
                                    break;

                                case 'HEATER':
                                    if (entity['SUBTYP'] === 'SOLAR' && history['P'] === 'STATUS') {
                                        temp_graphData.solarUsage.push(historyEntry);
                                    } else if (history['P'] === 'STATUS') {
                                       temp_graphData.heaterUsage.push(historyEntry);
                                    }
                                    break;
                            }
                        });
                    }
                });

                // save current results
                resultHistoryBuffer = temp_graphData;
                var temp = angular.copy(temp_graphData)
                //Combine Results by body of water
                //result = combineHistoryByBodyOfWater(result, temp_graphData);                
                result = combineHistoryByBodyOfWater(result, temp);                
            });

            propertyModel.History = result;
        };

        function combineHistoryByBodyOfWater(historyList, temp_graphData) {
            var bodiesOfWater = [];
            angular.forEach(propertyModel.Bodies, function (body) {

                if (body !== null) {
                    var share = propertyModel.Objects[body['SHARE']];
                    var isSingleBody = false;
                    if(share === undefined) {
                        isSingleBody = true;
                    }
                    var bodyOfWater = createBodyOfWater(body, share);
                    var isUnique = addUniqueBodyOfWater(bodyOfWater, bodiesOfWater);

                    if (isUnique) {
                        var bodyHistory = {};
                        var chemistries = getChemistries(body.OBJNAM);
                        var heaters = getHeaters(body.OBJNAM);
                        bodyHistory.bodyName = bodyOfWater["SNAME"];

                        historyList.push(bodyHistory);

                        var graphData = {
                            spaTemp: {SNAME: '', history: []},
                            poolTemp: {SNAME: '', history: []},
                            poolUsage: {SNAME: '', history: []},
                            spaUsage: {SNAME: '', history: []},
                            lightUsage: [],
                            phLevel: [],
                            orpLevel: [],
                            solarUsage: [],
                            heaterUsage: [],
                            solarTemp: [],
                            poolSideTemp: [],
                            orpFeed: [],
                            phFeed: []
                        };

                        bodyHistory.graphData = graphData;

                        graphData.lightUsage = temp_graphData.lightUsage.filter(function(lightUsageItem) {
                            var bodyObj = propertyModel.Objects[body.OBJNAM];
                            var circuitObj = propertyModel.Objects[lightUsageItem.objnam];
                            return bodyObj.MODULE === circuitObj.MODULE;
                        });
                        graphData.solarTemp = temp_graphData.solarTemp.filter(function(solarTempItem) {
                            var bodyObj = propertyModel.Objects[body.OBJNAM];
                            var solarObj = propertyModel.Objects[solarTempItem.objnam];
                            return bodyObj.MODULE === solarObj.MODULE;
                        });
                        graphData.poolSideTemp = temp_graphData.poolSideTemp; // this one is global

                        angular.forEach(temp_graphData.poolTemp, function (bodyTempHistory) {
                            if (body.OBJNAM === bodyTempHistory.objnam) {
                                graphData.poolTemp.SNAME = body["SNAME"];
                                graphData.poolTemp.history.push(bodyTempHistory);
                            } else if (!isSingleBody && share.OBJNAM === bodyTempHistory.objnam) {
                                graphData.poolTemp.SNAME = share["SNAME"];
                                graphData.poolTemp.history.push(bodyTempHistory);
                            }

                        });

                        angular.forEach(temp_graphData.spaTemp, function (bodyTempHistory) {
                            if (body.OBJNAM === bodyTempHistory.objnam) {
                                graphData.spaTemp.SNAME = body["SNAME"];
                                graphData.spaTemp.history.push(bodyTempHistory);
                            } else if (!isSingleBody && share.OBJNAM === bodyTempHistory.objnam) {
                                graphData.spaTemp.SNAME = share["SNAME"];
                                graphData.spaTemp.history.push(bodyTempHistory);
                            }
                        });

                        angular.forEach(temp_graphData.poolUsage, function (bodyUsageHistory) {
                            if (body.OBJNAM === bodyUsageHistory.objnam) {
                                graphData.poolUsage.SNAME = body["SNAME"];
                                graphData.poolUsage.history.push(bodyUsageHistory);
                            } else if (!isSingleBody && share.OBJNAM === bodyUsageHistory.objnam) {
                                graphData.poolUsage.SNAME = share["SNAME"];
                                graphData.poolUsage.history.push(bodyUsageHistory);
                            }
                        });


                        angular.forEach(temp_graphData.spaUsage, function (bodyUsageHistory) {
                            if (body.OBJNAM === bodyUsageHistory.objnam) {
                                graphData.spaUsage.SNAME = body["SNAME"];
                                graphData.spaUsage.history.push(bodyUsageHistory);
                            } else if (!isSingleBody && share.OBJNAM === bodyUsageHistory.objnam) {
                                graphData.spaUsage.SNAME = share["SNAME"];
                                graphData.spaUsage.history.push(bodyUsageHistory);
                            }
                        });

                        angular.forEach(temp_graphData.orpLevel, function (orpHistory) {
                            if (chemistries.ICHEM) {
                                if (chemistries.ICHEM.OBJNAM === orpHistory.objnam) {
                                    graphData.orpLevel.push(orpHistory);
                                }
                            }
                        });

                        angular.forEach(temp_graphData.phLevel, function (phHistory) {
                            if (chemistries.ICHEM) {
                                if (chemistries.ICHEM.OBJNAM === phHistory.objnam) {
                                    graphData.phLevel.push(phHistory);
                                }
                            }
                        });

                        angular.forEach(temp_graphData.orpFeed, function (orpHistory) {
                            if (chemistries.ICHEM) {
                                if (chemistries.ICHEM.OBJNAM === orpHistory.objnam) {
                                    graphData.orpFeed.push(orpHistory);
                                }
                            }
                        });

                        angular.forEach(temp_graphData.phFeed, function (phHistory) {
                            if (chemistries.ICHEM) {
                                if (chemistries.ICHEM.OBJNAM === phHistory.objnam) {
                                    graphData.phFeed.push(phHistory);
                                }
                            }
                        });

                        angular.forEach(temp_graphData.solarTemp, function (solarHistory) {
                            var heaterEntity = propertyModel.Objects[solarHistory.objnam];
                            if (heaterEntity && (heaterEntity.OBJNAM === solarHistory.objnam)) {
                                graphData.solarTemp.push(solarHistory);
                            }
                        });

                        angular.forEach(temp_graphData.solarUsage, function (solarHistory) {
                            var heaterEntity = propertyModel.Objects[solarHistory.objnam];
                            if (heaterEntity && heaters.indexOf(heaterEntity) > -1) {
                                graphData.solarUsage.push(solarHistory);
                            }
                        });

                        angular.forEach(temp_graphData.heaterUsage, function (heaterHistory) {
                            var heaterEntity = propertyModel.Objects[heaterHistory.objnam];
                            if (heaterEntity && heaters.indexOf(heaterEntity) > -1) {
                                graphData.heaterUsage.push(heaterHistory);
                            }
                        });

                        graphData.lightUsage.sort(function (a, b) {
                            return a.time - b.time;
                        });
                        graphData.solarTemp.sort(function (a, b) {
                            return a.time - b.time;
                        });
                        graphData.poolSideTemp.sort(function (a, b) {
                            return a.time - b.time;
                        });
                        graphData.poolTemp.history.sort(function (a, b) {
                            return a.time - b.time;
                        });
                        graphData.spaTemp.history.sort(function (a, b) {
                            return a.time - b.time;
                        });
                        graphData.poolUsage.history.sort(function (a, b) {
                            return a.time - b.time;
                        });
                        graphData.spaUsage.history.sort(function (a, b) {
                            return a.time - b.time;
                        });
                        graphData.orpLevel.sort(function (a, b) {
                            return a.time - b.time;
                        });
                        graphData.phLevel.sort(function (a, b) {
                            return a.time - b.time;
                        });
                        graphData.orpFeed.sort(function (a, b) {
                            return a.time - b.time;
                        });
                        graphData.phFeed.sort(function (a, b) {
                            return a.time - b.time;
                        });
                        graphData.solarUsage.sort(function (a, b) {
                            return a.time - b.time;
                        });
                        graphData.heaterUsage.sort(function (a, b) {
                            return a.time - b.time;
                        });
                    }
                }
            });
            return historyList;
        }

        // check if object is available : exist on list already?
        function isObjectAvailable(list, _objName) {
            var Available = false
            angular.forEach(list, function (obj) {
                if (obj.OBJNAM && obj.OBJNAM == _objName)
                    Available = true
            });

            return Available
        }

        function createBodyOfWater(b1, b2) {
            var relationShip = {
                parent: {},
                child: {},
                SNAME: '',
                LISTORD: "0"
            };
            var body = propertyModel.Objects[b1.OBJNAM];
            var share = null;
            if(b2 !== undefined) {
                share = propertyModel.Objects[b2.OBJNAM];
            }

            if (body == share) {
                share = null;
            }

            if (body && share) {

                if (body.LISTORD < share.LISTORD) {
                    relationShip = {
                        parent: body,
                        child: share
                    };
                } else {
                    relationShip = {
                        parent: share,
                        child: body
                    };
                }
                relationShip.SNAME = relationShip.parent.SNAME + '/' + relationShip.child.SNAME;
            } else if (body && !share) {
                relationShip = {
                    parent: body,
                    child: null,
                    SNAME: body.SNAME
                }
            }

            relationShip.LISTORD = relationShip.parent.LISTORD;
            return relationShip;
        }

        function addUniqueBodyOfWater(bodyOfWater, bodiesOfWater) {
            var index = -1;
            var found = false;
            angular.forEach(bodiesOfWater, function (BOW, i) {
                index = i;
                /*
                if (BOW.parent.OBJNAM){
                    if (bodyOfWater.parent.OBJNAM){
                        if (BOW.parent.OBJNAM == bodyOfWater.parent.OBJNAM) {

                            if (BOW.child.OBJNAM){
                                if (bodyOfWater.child.OBJNAM) {
                                    if (BOW.child.OBJNAM == bodyOfWater.child.OBJNAM) {
                                        found = true;
                                    }
                                }
                            }

                        }
                    }
                }
                */
                if (BOW && bodyOfWater) {
                    if (BOW.parent.OBJNAM == bodyOfWater.parent.OBJNAM && BOW.child.OBJNAM == bodyOfWater.child.OBJNAM) {
                        found = true;
                    }
                }
            });

            if (!found) { // add
                bodiesOfWater.push(bodyOfWater);
                return true;
            } else if (index > -1) { // replace
                bodiesOfWater.push(index, bodyOfWater);
                return false;
            }
        }

        function getChemistries(bodyName) {
            var chemistry = {
                ICHEM: null,
                ICHLOR: null
            };

            var allChemistries = $filter('filterOutByObjectType')(propertyModel.Objects, 'CHEM');
            var chemistries = $filter('objectsByParamValue')(allChemistries, ['BODY', 'SHARE'], bodyName);
            chemistry.ICHLOR = $filter('objectsBySubType')(chemistries, 'ICHLOR')[0];
            chemistry.ICHEM = $filter('objectsBySubType')(chemistries, 'ICHEM')[0];
            return chemistry;
        }

        function getHeaters(bodyName) {
            var heaters = $filter('objectsByParamValue')(propertyModel.Heaters, ['BODY', 'SHARE'], bodyName);
            heaters.unshift({SNAME: 'Heater Off', OBJNAM: '00000'});
            return heaters;
        }


        ////END HISTORY
        function storeBody(newObject) {
            if (newObject.SHARE) {
                if (newObject.SHARE !== "00000") {
                    if (!propertyModel.SharedBodies[newObject.OBJNAM] && !propertyModel.SharedBodies[newObject.SHARE]) {
                        propertyModel.SharedBodies[newObject.OBJNAM] = newObject.SHARE
                        //propertyModel.SharedBodies[newObject.OBJNAM] = newObject;
                    }
                }
                else {                    
                    propertyModel.SingleBodies.push(newObject.OBJNAM);                    
                }                                                
            }
                    
            // check share body
            if (newObject.OBJTYP) {
                if (newObject.OBJTYP === "BODY"){
                    if (newObject.SHARE){
                        if(newObject.SHARE !=="00000"){
                            propertyModel.ShareBody = true;
                        }
                    }
                }
            }


            storeObject("Bodies", newObject);

            // checking for shared body adding a new Pool/Spa option
            
            if (propertyModel.ShareBody && (newObject.OBJNAM === "B1202")) {
                var PoolSpa =  angular.copy(propertyModel.Objects["B1101"]);   // get first item
                
                if (PoolSpa) {    // of body exist then added                    
                    PoolSpa.SNAME = "Pool/Spa";
                    PoolSpa.OBJNAM = "B1101 B1202";                    
                    storeObject("Bodies", PoolSpa);
                }
            }
            
        }

        function storeObject(listName, object) {
            if (object) {
                if (listName) {
                    var list = propertyModel[listName];
                    if (angular.isArray(list)) {
                        // Updated logic for updating an existing object, if the object has params we only update the params. Otherwise we update the entire object
                        var match = list.filter(function (value) {
                            if (value.OBJNAM === object.OBJNAM) {
                                return value;
                            }
                        });
                        if (match.length > 0) {
                            list = list.map(function (value) {
                                if (value.OBJNAM === object.OBJNAM) {
                                    var dst = {};
                                    return angular.merge(dst, value, object);
                                } else {
                                    return value;
                                }
                            });
                        } else {
                            list.push(object);
                        }
                        propertyModel[listName] = list;
                    } else if (object.OBJNAM) {
                        if (list) {
                            if (list[objnam]) {
                                if (list[objnam].params) {
                                    angular.extend(list[objnam].params, object.params);
                                } else {
                                    angular.extend(list[objnam], object);
                                }
                            } else {
                                list[object.OBJNAM] = object
                            }
                        }
                        propertyModel[listName][object.OBJNAM] = object
                    }
                }
                if (object.OBJNAM) {
                    propertyModel.Objects[object.OBJNAM] = object;
                }
            }
        }


        function removeObject(listName, objName) {
            if (objName) {
                if (listName) {
                    // retrieve listName
                    var list = propertyModel[listName];
                    if (propertyModel[listName][objName])   // check if exit
                        propertyModel[listName][objName] = null;
                        delete propertyModel[listName][objName];
                    }
            }
        }

        function getDelayByOBJNAM(objName) {
            if (objName) {
                var message = {
                    command: "getParamList",
                    objectList: [{
                        objnam: objName,
                        keys: ["TIMOUT : SOURCE"]
                    }]
                };
                return socketService.sendMessage(message);
            }
        };

        function getLightColorByOBJNAM(objName) {
            if (objName) {
                var message = {
                    command: "getParamList",
                    objectList: [{
                        objnam: objName,
                        keys: ["USE : ACT"]
                    }]
                };
                return socketService.sendMessage(message);
            }
        };
 


        function detectObjectCreation(notification) {
            angular.forEach(propertyModel.RequestedObjects, function (requestedObject) {
                var requestedParams = Object.keys(requestedObject.params);
                if (notification.params.OBJTYP === requestedObject.objtyp) {
                    if (Object.keys(notification.params).length - 1 === requestedParams.length) {
                        var allValuesMatch = true;
                        angular.forEach(requestedParams, function (key) {
                            if (notification.params[key] !== requestedObject.params[key]) {
                                allValuesMatch = false;
                            }
                        });
                        if (allValuesMatch) {
                            propertyModel.RequestedObjects.splice(i, 1);
                            $rootScope.$broadcast('NotifyObjectCreation', notification);
                        }
                    }
                }
            });
        }

        function detectLocationObject(message) {
            if (message.objnam === "_C10C") {
                factory.ProcessLocation(message);
            }
        }

        function detectClockObject(message) {
            if (message.objnam === "_C105") {
                factory.ProcessClockSource(message);
            }
        }

        function processClockObject(message) {
            if (message.objnam === "_C10C") {
                factory.ProcessClock(message);
                // now broadcast time update
                $rootScope.$broadcast('UpdateMinutes', message);
            }
        }

        function processFreezeObject(message) {
            if (message.objnam === "_5451") {
                factory.ProcessFreeze(message);                
                $rootScope.$broadcast('UpdateFreeze', message);     // now broadcast time update
            }
        }

        function processServiceObject(message) {
            if (message.objnam === "_5451") {
                factory.ProcessService(message);
                $rootScope.$broadcast('UpdateService', message);     // now broadcast time update
            }
        }

        function processFirmwareUpdateObject(message) {
            if (message.objnam === "_5451") {
                factory.ProcessFirmwareUpdate(message);
                $rootScope.$broadcast('UpdateFirmwareIn', message);     // now broadcast time update
            }
        }

        function detectSecurityObject(message) {
            if (message.objnam === "UFFFE" && message.params["ENABLE"]) {
                propertyModel.SecurityEnabled = message.params["ENABLE"] === 'ON';
                propertyModel.SecurityInitiated = true;
            }
            if (message.objnam === "U0000" && message.params["ENABLE"]) {
                propertyModel.GuestEnabled = message.params["ENABLE"] === 'ON';
            }
        };

        //TODO will not work with Tree structure unless
        //     shared objects are repeated on each body
        function linkHeaterToBody(newObject) {
            if (newObject) {
                var body = propertyModel.Objects[newObject.BODY];
                var share = propertyModel.Objects[newObject.SHARE];

                if (body) {
                    body.HEATERS_debug = body.HEATERS_debug || [];
                    body.HEATERS_debug.push(newObject);
                }

                if (share) {
                    share.HEATERS_debug = share.HEATERS_debug || [];
                    share.HEATERS_debug.push(newObject);
                }
            }
        }

        //TODO will not work with Tree structure unless
        //     shared objects are repeated on each body
        function linkChemistryToBody(newObject) {
            if (newObject) {
                var body = propertyModel.Objects[newObject.BODY];
                var share = propertyModel.Objects[newObject.SHARE];

                if (body) {
                    body.CHEMISTRY_debug = body.CHEMISTRY_debug || {};
                    body.CHEMISTRY_debug[newObject.SUBTYP] = newObject;
                }

                if (share) {
                    share.CHEMISTRY_debug = share.CHEMISTRY_debug || {};
                    share.CHEMISTRY_debug[newObject.SUBTYP] = newObject;
                }
            }
        }


        function processStatusMessage(message) {
            if (message.objectList) {
                angular.forEach(message.objectList, function (update) {
                    if (update.deleted) {
                        angular.forEach(update.deleted, function (deletedObjnam) {
                            //TODO deleting an object doesnot delete the refrences, consider
                            //accesing all items by the Objects Dictionary instead of the
                            //respective sub arrays.
                            if (propertyModel.Objects[deletedObjnam]) {
                                propertyModel.Objects[deletedObjnam].SHOMNU = 'ON';
                                delete propertyModel.Objects[deletedObjnam];
                            }
                            $rootScope.$broadcast('StatusMessageDeleted', deletedObjnam);
                        });
                    }
                    if (update.changes) {
                        angular.forEach(update.changes, function (change) {
                            addOrUpdateStatusMessage(change);
                        });
                    }
                });
            }
        }

        function addOrUpdateStatusMessage(obj) {
            if (obj && obj.params) {
                var match = propertyModel.Objects[obj.objnam];
                if (!PWC.isEmptyObject(match)) {
                    angular.extend(match, obj.params);
                    $rootScope.$broadcast('StatusMessageUpdated', match);
                } else {
                    var newObject = {
                        OBJNAM: obj.objnam,
                        MODE: obj.params.MODE,
                        OBJTYP: obj.params.OBJTYP,
                        PARENT: obj.params.PARENT,
                        SINDEX: obj.params.SINDEX,
                        SNAME: obj.params.SNAME,
                        SHOMNU: obj.params.SHOMNU
                    };
                    storeObject("StatusMessages", newObject);
                    $rootScope.$broadcast('StatusMessageAdded', newObject);
                }
            }
        }


        function removeHeaters_debug(body, objnam, heaterstodelete) {
            if (!body)
                return;                        
            var index = 0
            angular.forEach(body.HEATERS_debug, function (one) {                    
                angular.forEach(heaterstodelete, function (heaterObjNam) {   // CHECK EACH ITEM 
                    if (body.HEATERS_debug[index]) {
                        if (body.HEATERS_debug[index].OBJNAM === heaterObjNam) {
                            body.HEATERS_debug[index] = null
                            delete (body.HEATERS_debug[index]);            
                        }
                    }
                });
                index = index + 1
            });            
        };

        // removing object from list using splice using parameter OBJNAM
        function removeObjectFromList(list, objnam) {

            if (!list)  // check if null
                return list;

            if (!objnam)    // check if null
                return list;

            var index = 0;
            var size = list.length;
            angular.forEach(list, function (item) {
                if (item.OBJNAM && (item.OBJNAM === objnam))
                    if (list[index]) {
                        if (list[index].OBJNAM === objnam) {
                            list.splice(index,1)
                            return list;
                        }
                    }
                index = index + 1;
            });
            return list;
        }

        function removeLayerOnePropertyModel(objnam,tobedeleted) {

            // processing HEATERS deletions            
            if (propertyModel.Heaters) {        // checking heaters to be remove                                 
                var index = 0;                
                // removing heaters
                propertyModel.Heaters = removeObjectFromList(propertyModel.Heaters, objnam);               
            }

            // now removing body from heaters
            if (propertyModel.Objects[objnam]) {
                var temp = propertyModel.Objects[objnam]
                if (temp) {
                    var body = propertyModel.Objects[temp.BODY]
                    var shared = propertyModel.Objects[temp.SHARE]

                    if (body) removeHeaters_debug(body, objnam, tobedeleted)
                    if (shared) removeHeaters_debug(body, objnam, tobedeleted)
                    
                }
            }

            // TBD other object to be removed
            
        };

        function detectObjectDeletion(message) {

            //TODO deleting an object doesnot delete the refrences, consider
            //accesing all items by the Objects Dictionary instead of the
            //respective sub arrays.
            if (message.params) {                
                if (message.params["STATUS"] && message.params["STATUS"] == "DSTROY") {
                    if (propertyModel.Schedules[message.objnam]) {
                        delete propertyModel.Schedules[message.objnam];
                    } else if (propertyModel.Objects[message.objnam]) {
                        propertyModel.Objects[message.objnam] = null;
                        delete propertyModel.Objects[message.objnam];
                    } 

                    $rootScope.$broadcast('ObjectDeleted', message);

                }                
            }

            // check if deleted messages
            if (message.deleted) {
                // 1- removing propertyModel.Heaters, propertyModel.Pumps, etc/
                angular.forEach(message.deleted, function (objnam) {
                    
                    // now delete per type                        
                    removeLayerOnePropertyModel(objnam, message.deleted)
                    
                });

                // 2- removing propertyModel.Objects
                angular.forEach(message.deleted, function (objnam) {
                    if (propertyModel.Objects[objnam]) {                        
                        propertyModel.Objects[objnam] = null
                        delete (propertyModel.Objects[objnam])
                    }                
                });
                $rootScope.$broadcast('ObjectDeleted', message);
            }  
        }

        function detectExistingCircuits(obj) {
            angular.forEach(propertyModel.Circuits, function (circuit) {
                if (circuit.OBJNAM === obj.objnam) {
                    if (obj.params && obj.params.STATUS) {
                        circuit.STATUS = obj.params.STATUS;
                    }
                }
            });
        }


        function detectExistingObject(obj) {
            var match = propertyModel.Objects[obj.objnam];
            var result = false;
            if (!PWC.isEmptyObject(match)) {
                angular.extend(match, obj.params);
                if (match.OBJTYP == "CIRCUIT") {
                    factory.ProcessDelayState(match, "", "");
                    detectExistingCircuits(obj)
                }
                $rootScope.$broadcast('ObjectUpdated', obj);
                result = true;
            }
            return result;
        }

        function notifyExistingObject(obj) {

            var match;
            if (obj != null){
                if (obj.objnam != null) {//if (obj.objnam !== "undefined") {
                    match = propertyModel.Objects[obj.objnam];
                }
                else {
                    if (obj.OBJNAM != null) {//if (obj.OBJNAM !== "undefined") {
                        match = propertyModel.Objects[obj.OBJNAM];
                    }
                }
            }

            var result = false;
            if (!PWC.isEmptyObject(match)) {
                angular.extend(match, obj.params);
                $rootScope.$broadcast('NotifyList',match, obj);
                result = true;
                updateProperyModel(obj)
            }
            return result;
        }


        function updateProperyModel(obj) {
            
            var match = propertyModel.Objects[obj.objnam];
            if (match) {
                angular.forEach(obj.params, function (param) {                    
                 //console.log(" new params :", param)
                });
            }
        }

        function detectAirCircuit(obj) {
            if (obj.params.SUBTYP && obj.params.SUBTYP === 'AIR') {
                factory.ProcessAirSensor(obj);
            }
        }

        function recurseNewObjects(obj, body, share) {
            if (body) {
                obj.params['BODY'] = body;
            }
            if (share) {
                obj.params['SHARE'] = share;
            }
            if (obj.params["OBJTYP"] == 'BODY') {
                body = obj.objnam;
                if (obj.params["SHARE"]) {
                    share = obj.params["SHARE"];
                }
            }

            detectNewObject(obj);
            updateExistingObject(obj);

            if (obj.params['OBJLIST']) {
                for (var i = 0; i < obj.params['OBJLIST'].length; i++) {
                    var o = obj.params['OBJLIST'][i];
                    if (obj.params['OBJTYP'] == 'PUMP') {
                        o.params['OBJTYP'] = 'PMPCIRC';
                    }
                    recurseNewObjects(o, body, share);
                }
            } else if (obj.params['CIRCUITS']) {
                for (var i = 0; i < obj.params['CIRCUITS'].length; i++) {
                    var o = obj.params['CIRCUITS'][i];
                    o.params['PANEL'] = obj.params['PARENT'];
                    o.params['MODULE'] = obj.objnam;
                    if (obj.params['OBJTYP'] == 'PUMP') {
                        o.params['OBJTYP'] = 'PMPCIRC';
                    }
                    recurseNewObjects(o, body, share);
                }
            }
        }

        function processingSetCommand(obj) {                                   
            // checking creationg of LIGHT GROUP                
            if ( (obj.params.OBJTYP === "CIRCUIT") && (obj.params.SUBTYP === "LITSHO") ) {
                console.log("processing group circuit with SETCOMMAND")                
                var match = propertyModel.Objects[obj.objnam];      // check if does  not exits
                if (!match) {   
                    detectNewObject(obj)                // object does not exist 
                }
                else {
                    WriteParamChanges(obj, "CHANGES")   // modify object
                }
            }
           
        }

        function processingWriteParamList(message) {

            var settingsUpdated = false;
            var pumpUpdated = false;
            var scheduleUpdated = false;
            // process delete objects             
            if (message.deleted) {
                console.log("---------------------------------- deleting objects ----------------------------------")
                angular.forEach(message.deleted, function (objname) {
                    console.log(" removing object: ", objname)                    

                    // deleting objects
                    if (propertyModel.Objects[objname]) {
                        propertyModel.Objects[objname] = null;
                        delete propertyModel.Objects[objname];
                    }
                    // deleting communication lost object
                    var count = propertyModel.lostcommunication.length - 1
                    // remove in reverse order
                    for (var i = count; i >= 0; i--) {
                        if (propertyModel.lostcommunication[i]) {
                            if (propertyModel.lostcommunication[i].OBJNAM === objname) {
                                console.log("*** Deleting commu lost : ", propertyModel.lostcommunication[i])
                                propertyModel.lostcommunication[i] = null;
                                delete propertyModel.lostcommunication[i];
                            }
                        }
                    }
                    
                    //checking deleting feature circuits in reverse order                    
                    for (var i = propertyModel.Circuits.length; i >= 0; i--) {
                        if (propertyModel.Circuits[i]) {
                            if (propertyModel.Circuits[i].OBJNAM === objname) {
                                console.log("******** Deleting circuit : ", propertyModel.Circuits[i].objName)
                                propertyModel.Circuits[i] = null;
                                delete propertyModel.Circuits[i];
                                $rootScope.$broadcast('UPDATE_CIRCUITS', message, "FEATURES");
                                break;
                            }
                        }
                    }
/*
                    // deleting hardware definition
                    for (var i = propertyModel.hardwareDefinition.params.OBJLIST.length; i >= 0; i--) {
                        if (propertyModel.hardwareDefinition.params.OBJLIST[i].objnam) {
                            if (propertyModel.hardwareDefinition.params.OBJLIST[i].objnam === objname) {
                                console.log("******** Deleting hardware definition object: ", propertyModel.hardwareDefinition.params.OBJLIST[i].objnam)
                                propertyModel.hardwareDefinition.params.OBJLIST[i] = null;
                                break;  // ONLY 1 ITEM
                            }
                        }
                    }
*/

                    // deleting group circuits in reverse order                    
                    var removedGroups = false;
                    for (var i = propertyModel.CircuitGroups.length; i >= 0; i--) {
                        if (propertyModel.CircuitGroups[i]) {
                            if (propertyModel.CircuitGroups[i].OBJNAM === objname) {
                                console.log("******** Deleting group circuit : ", objname)
                                propertyModel.CircuitGroups[i] = null;
                                delete propertyModel.CircuitGroups[i];                                
                                removedGroups = true;
                            }
                        }
                    }

                    if (removedGroups === true) {    // at least 1 group was removed

                        updateHardwareDefinitionObjects("",objname,"REMOVE");

                        $rootScope.$broadcast('UPDATE_CIRCUITS', message, "GROUPS");                        
                    }

                    WriteParamChanges(objname,"REMOVE")
                    
                    checkingUpdatedHeaters(objname, "REMOVE")
                    
                    // checking pump objects removal
                    if (findToken(objname, 'p', 0) === true) {
                        //removeObject('PumpAffects', objname);
                        if (propertyModel['PumpAffects']) { // check if exist
                            propertyModel['PumpAffects'] = removeObjectFromList(propertyModel['PumpAffects'], objname);                        
                        }
                        if (propertyModel['PumpAffect']) { // check if exist
                            propertyModel['PumpAffect'] = removeObjectFromList(propertyModel['PumpAffect'], objname);
                        }
                    }                 

                    // removing pump 
                    if (propertyModel.Pumps) {
                        propertyModel.Pumps = removeObjectFromList(propertyModel.Pumps, objname);
                    }

                    // removing schedule
                    if (objname.substring(0, 3) === "SCH") {
                        scheduleUpdated = true;
                        if (propertyModel.Schedules) {
                            propertyModel.Schedules = removeObjectFromList(propertyModel.Schedules, objname);
                            $rootScope.$broadcast('SchedulesUpdated', objname);
                        }
                    }

                  
                    settingsUpdated = true;
                });
            }
            // process  created objects 
            if (message.created) {
                console.log("---------------------------------- created objects ----------------------------------")
                angular.forEach(message.created, function (obj) {
                    if (obj.objnam) {
                        console.log(" create object: ", obj.objnam)
                        // adding object                        
                        detectNewObject(obj) // propertyModel.Objects.push(obj);

                        //$rootScope.$broadcast('UpdateCircuits', newObject);
                        // work in progress
                        WriteParamChanges(obj,"CREATED")
                        switch (obj.params.SUBTYP){
                           case 'HEATER' :
                                if (obj.objnam)
                                    checkingUpdatedHeaters(obj, "CREATE")
                                break;

                            case  'CIRCGRP':                                
                                updateHardwareDefinitionObjects(obj,obj.objnam,"CREATE");
                                $rootScope.$broadcast('UPDATE_CIRCUITS', message, "GROUPS");
                                break;
                            case 'PUMP':
                                pumpUpdated = true;
                                break;
                            case 'SCHED':
                                scheduleUpdated = true;
                                break;
                        }

                        switch (obj.params.OBJTYP) {
                            case 'CIRCGRP':                                
                                $rootScope.$broadcast('UPDATE_CIRCUITS', message, "GROUPS");    // UPDATE GROUPS CONFIGURATION
                                break;
                            case 'PUMP':
                                pumpUpdated = true;
                                break;
                            case 'SCHED':
                                scheduleUpdated = true;
                                break;
                        }
                    }
                    settingsUpdated = true;
                });
            }

            if (message.changes) {
                console.log("---------------------------------- changes objects---------------------------- ")
                angular.forEach(message.changes, function (obj) {
                    if (obj.objnam){
                        console.log(" changes object: ", obj.objnam)
                    
                        // check if object is schedule                        
                        if (obj.objnam.substring(0, 3) === "SCH") {
                            // update schedule
                            updateSchedule(obj)
                        }
                        // update current objects
                        detectExistingObject(obj)
                        // deleting objects
                        notifyExistingObject(obj)
                        // work in progress
                        WriteParamChanges(obj,"CHANGES")

                        if (obj.objnam && obj.params && obj.params.OBJTYP && obj.params.OBJTYP === 'HEATER') {                            
                            checkingUpdatedHeaters(obj, "UPDATE",obj.params);
                        }                        

                        switch(obj.params.SUBTYP){  // update for subtype
                            case "CIRCGRP":
                                updateHardwareDefinitionObjects(obj, obj.objnam, "CHANGE");
                                $rootScope.$broadcast('UPDATE_CIRCUITS', message, "GROUPS");
                                break;
                            case "PUMP":
                                pumpUpdated = true
                                break;
                            case 'SCHED':
                                scheduleUpdated = true;
                                break;
                        }
                        // check for schedules
                        switch (obj.params.OBJTYP) {  // update for objtyp
                            case "PUMP":
                                pumpUpdated = true
                                break;
                            case 'SCHED':
                                scheduleUpdated = true;
                                break;
                        }
                    }
                    settingsUpdated = true;
                });
            }

            // broadcast to all active views if settings was updated.
            if (settingsUpdated === true) {
                $rootScope.$broadcast('SETTINGSUPDATED', message, "Updated");
                
            }

            if (pumpUpdated == true) {  // broadcast to update pumps
                $rootScope.$broadcast('PUMPS_UPDATED', message, "Updated");
            }

            if (scheduleUpdated == true) {  // broadcast to update pumps
                $rootScope.$broadcast('Schedules_UPDATES', message, "Updated");
            }
        }
        
        function updateSchedule(obj) {
            propertyModel.Schedules.forEach(function (schedule) {
                if (schedule.OBJNAM === obj.objnam) {
                    console.log("Updating schedule object: ", obj.objnam)
                    schedule.OBJTYP     = obj.params.OBJTYP;
                    schedule.SNAME = obj.params.SNAME;
                    schedule.LISTORD = obj.params.LISTORD;
                    schedule.CIRCUIT = obj.params.CIRCUIT;
                    schedule.DAY = obj.params.DAY;
                    schedule.START = obj.params.START;
                    schedule.TIME = obj.params.TIME;
                    schedule.STOP = obj.params.STOP;
                    schedule.SINGLE = obj.params.SINGLE
                    schedule.TIMOUT = obj.params.TIMOUT;
                    schedule.GROUP = obj.params.GROUP;
                    schedule.HEATER = obj.params.HEATER;
                    schedule.COOLING = obj.params.COOLING;
                    schedule.LOTMP = obj.params.LOTMP;
                    schedule.SMTSRT = obj.params.SMTSRT;
                    schedule.DNTSTP = obj.params.DNTSTP
                    schedule.STATUS = obj.params.STATUS;
                    schedule.ACT = obj.params.ACT;
                    schedule.DAY = obj.params.DAY;
                    if (obj.params.MODE)
                        schedule.MODE = obj.params.MODE;
                

                    $rootScope.$broadcast('SchedulesUpdated', obj);                    
                }
            });
        }

        function notifyListSchedule(obj) {
            propertyModel.Schedules.forEach(function (schedule) {
                if (schedule.OBJNAM === obj.objnam) {
                    console.log("notifylist schedule object: ", obj.objnam, " ACT = ", obj.params.ACT)
                    schedule.ACT = obj.params.ACT;
                    $rootScope.$broadcast('SchedulesUpdated', obj);
                }
            });
        }


        function updateHardwareDefinitionObjects(obj,objnam,operation) {

            angular.forEach(propertyModel.hardwareDefinition, function (panel) {
                if (panel.params.OBJLIST) {                    
                    switch (operation) {
                        case "REMOVE":
                            panel.params.OBJLIST = removeItemHardwareDefinition(panel.params.OBJLIST, objnam);
                            break;
                        case "CHANGE":
                            panel.params.OBJLIST = replaceItemHardwareDefinition(panel.params.OBJLIST, objnam,obj);
                            break;
                        case  "CREATE":                            
                            panel.params.OBJLIST = createItemHardwareDefinition(panel.params.OBJLIST, objnam, obj);
                            break;
                    }                                                            
                }
            });
        }


        function removeItemHardwareDefinition(array, objnam) {
            return array.filter(function (item) {
                return item.objnam != objnam;
            });
        }

        // replace 
        function replaceItemHardwareDefinition(array, objnam, obj) {
            return array.filter(function (item) {
                if (item.objnam === objnam) {
                    switch (obj.params.SUBTYP) {
                        case "CIRCGRP": // updating the main parameters on CIRCGRP
                            item.params.SNAME = obj.params.SNAME;
                            item.params.TIME = obj.params.TIME;
                            break;
                    }
                    
                }
                return item
            });
        }


        function createItemHardwareDefinition(array, objnam, obj) {
            
            var exist = false;

            array.filter(function (item) {
                if (item.objnam === objnam) {
                    exist = true;
                }                
            });
          
            if (exist === false){   // append to list
                array.push(obj);
            }

            return array;
        }

        function checkingUpdatedHeaters(obj, action, params) {
            var objname = obj;
            if (obj.objnam != null)
                objname = obj.objnam;

            if (objname && objname.indexOf('H') === 0) {        // CHECKING IF OBJECT IS HEATER 'H'
                switch (action) {
                    case "REMOVE":
                        // removeObject("Heaters", objname);                          
                        var count = propertyModel.Heaters.length - 1
                        // remove in reverse order
                        for (var i = count; i >= 0; i--) {
                            if (propertyModel.Heaters[i]) {
                                if (propertyModel.Heaters[i].OBJNAM === objname) {
                                    console.log("*** Deleting heater item : ", objname)
                                    propertyModel.Heaters[i] = null;
                                    delete propertyModel.Heaters[i];
                                }
                            }
                        }          
                        break;
                    case "UPDATE":
                        for (var i = 0; i < propertyModel.Heaters.length; i++) {
                            if (propertyModel.Heaters[i]) {
                                if (propertyModel.Heaters[i].OBJNAM === objname) {
                                    console.log("*** Updating heater item : ", objname)
                                    propertyModel.Heaters[i] = params;
                                }
                            }
                        }
                        break;
                }
                $rootScope.$broadcast('UpdatedHeaters', params);
            }
        }


        function findToken(param, search, location) {
            var value = param.indexOf(search);
            return (value === location) // return true or false
        }

        // update current circuits
        function WriteParamChanges(obj,action) {
            var match = propertyModel.Objects[obj.objnam];
            if (match) {

                //Process new object
                if(obj.params){
                    if (obj.params.OBJTYP) {    // check if objtyp exist...
                        switch (obj.params.OBJTYP) {
                            case 'BODY':
                             //   factory.ProcessBody(obj);
                                break;
                            case 'CIRGRP':
                                factory.ProcessCircuitUpdate(obj);
                                break;
                            case 'CIRCUIT':
                                factory.ProcessCircuitUpdate(obj);                                
                                break;
                    
                            case 'HEATER':
                                factory.ProcessHeater(obj);
                                break;

                            case 'PMPCIRC':
                                factory.ProcessPumpAffect(obj);                                                                
                                break;
                    
                            case 'SCHED':
                                factory.ProcessSchedule(obj);
                                break;
                   /*
                            case 'PANEL':
                                factory.ProcessPanel(obj);
                                break;
                            case 'SENSE':
                                factory.ProcessSensor(obj);
                                break;
                            case 'MODULE':
                                factory.ProcessModule(obj);
                                break;
                            case 'RLY':
                                factory.ProcessRelay(obj);
                                break;
                            case 'PERMIT':
                                detectSecurityObject(obj);
                                break;
                            case 'PUMP':
                                factory.ProcessPump(obj);
                                break;
                            case 'PMPCIRC':
                                factory.ProcessPumpAffect(obj);
                                break;
                            case 'SYSTIM':
                                detectLocationObject(obj);
                                detectClockObject(obj);
                                break;
                            case 'SYSTEM':
                                factory.ProcessSystemConfig(obj);
                                break;
                            case 'FEATR':
                                factory.ProcessFeatureObject(obj);
                                break;
                            case 'CHEM':
                                factory.ProcessChemistry(obj);
                            case 'REMOTE':
                                factory.ProcessRemote(obj);
                                break;
                            case 'VAL01':
                            case 'VAL02':
                            case 'VAL03':
                            case 'VAL04':
                            case 'VALVE':
                                factory.ProcessValve(obj);
                                break;
                            case 'STATUS':
                                factory.ProcessNewStatusMessage(obj);
                                break;
                              
                            default:
                                factory.ProcessSingleton(obj);
                                break;
                      */
                        }
                    }
                }
                                
            }
        }        
        //-----------------------

        function updateExistingObject(obj) {
          var match = propertyModel.Objects[obj.objnam];
          if(match) {
            if(obj.objnam.charAt(0) == 'V') {
              factory.ProcessValve(obj);
            }
            if(obj.params.OBJTYP === 'BODY' || obj.params.OBJTYP === 'CIRCUIT') {
                // need to get PANEL and MODULE from hardware definition                                
              if(obj.params.OBJTYP === 'BODY')
                  factory.ProcessUpdatedBody(obj);

              if(propertyModel.Objects[obj.objnam] && obj.params.MODULE) {
                propertyModel.Objects[obj.objnam].MODULE = obj.params.MODULE;
                propertyModel.Objects[obj.objnam].PANEL = obj.params.PANEL;
              }
            }
          }
        }



        function detectNewObject(obj) {
            var match = propertyModel.Objects[obj.objnam];
            if (!match) {

                //Process new object
                if(obj.params){
                    if (obj.params.OBJTYP) {    // check if objtyp exist...
                        switch (obj.params.OBJTYP) {
                            case 'BODY':
                                factory.ProcessBody(obj);
                                break;
                            case 'CIRCUIT':
                                factory.ProcessCircuit(obj);
                                break;
                            case 'CIRCGRP':
                                factory.ProcessCircuitGroup(obj);
                                break;
                            case 'HEATER':
                                factory.ProcessHeater(obj);
                                break;
                            case 'SCHED':
                                factory.ProcessSchedule(obj);
                                break;
                            case 'PANEL':
                                factory.ProcessPanel(obj);
                                break;
                            case 'SENSE':
                                factory.ProcessSensor(obj);
                                break;
                            case 'MODULE':
                                factory.ProcessModule(obj);
                                break;
                            case 'RLY':
                                factory.ProcessRelay(obj);
                                break;
                            case 'PERMIT':
                                detectSecurityObject(obj);
                                break;
                            case 'PUMP':
                                factory.ProcessPump(obj);
                                break;
                            case 'PMPCIRC':
                                factory.ProcessPumpAffect(obj);
                                break;
                            case 'SYSTIM':
                                detectLocationObject(obj);
                                detectClockObject(obj);
                                break;
                            case 'SYSTEM':
                                factory.ProcessSystemConfig(obj);
                                break;
                            case 'FEATR':
                                factory.ProcessFeatureObject(obj);
                                break;
                            case 'CHEM':
                                factory.ProcessChemistry(obj);
                                break;
                            case 'REMOTE':
                                factory.ProcessRemote(obj);
                                break;
                            case 'VAL01':
                            case 'VAL02':
                            case 'VAL03':
                            case 'VAL04':
                            case 'VALVE':
                                factory.ProcessValve(obj);
                                break;
                            case 'STATUS':
                                factory.ProcessNewStatusMessage(obj);
                                break;
                              
                            default:
                                factory.ProcessSingleton(obj);
                                break;
                        }
                    }
                }
                propertyModel.addPendingSubscription(obj);
                //$rootScope.$broadcast('NewObjectFound', obj);
            }
        }

        return factory;
    }
]);
