﻿'use strict';
describe('ChangeEmailController', function () {
    var AppService;
    var $location;
    var $scope;
    var Index2Controller;
    var $controller;
    beforeEach(module('PentairWebClient'));

    beforeEach(inject(function (_$controller_, _$rootScope_, _$location_) {
        $scope = _$rootScope_.$new();
        $location = _$location_;
        $controller = _$controller_;

        Index2Controller = $controller("Index2Controller", { $scope: $scope });
    }));

    describe('Default Vars', function () {
        it('should have the following default variables', function () {
            expect($scope.DisplayAccountMenu).toBe(true);
        });
    });

    describe('LogOut', function () {
        it('should call toggleAccount Menu', function () {
            spyOn($scope, 'ToggleAccountMenu');
            $scope.LogOut();
            expect($scope.ToggleAccountMenu).toHaveBeenCalled();
        });
    });
    
    describe('ToggleAccountMenu', function () {
        it('should call set DisplayAccountMenu to true', function () {
            $scope.DisplayAccountMenu = false;
            $scope.ToggleAccountMenu();
             expect($scope.DisplayAccountMenu).toBe(true);
        });
          it('should call set DisplayAccountMenu to true', function () {
            $scope.DisplayAccountMenu = true;
            $scope.ToggleAccountMenu();
             expect($scope.DisplayAccountMenu).toBe(false);
        });
    });

});
