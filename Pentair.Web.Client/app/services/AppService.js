﻿/// <reference path="AppService.js" />
'use strict';
var PWC = PWC || {};

PWC.factory('AppService', [
    'SERVICE_HOST_NAME', '$http', '$q', function (SERVICE_HOST_NAME, $http, $q) {
        var serviceFactory = {};

        serviceFactory.GetData = function (endpoint) {
            var d = $q.defer();
            if (endpoint) {
                $http.get('https://' + SERVICE_HOST_NAME + endpoint).success(function (data, status, hdrs, config) {
                    var response = { data: data, status: status, headers: hdrs, config: config };
                    d.resolve(response);
                }).error(function (err, status, hdrs, config) {
                    var response = { error: err, status: status, headers: hdrs, config: config };
                    d.reject(response);
                });
            }
            return d.promise;
        };


        serviceFactory.GetLocalData = function (endpoint) {
            var d = $q.defer();
            if (endpoint) {
                $http.get(endpoint).success(function (data, status, hdrs, config) {
                    var response = { data: data, status: status, headers: hdrs, config: config };
                    d.resolve(response);
                }).error(function (err, status, hdrs, config) {
                    var response = { error: err, status: status, headers: hdrs, config: config };
                    d.reject(response);
                });
            }
            return d.promise;
        };


        serviceFactory.PostData = function (endpoint, data, headers) {
            var d = $q.defer();
            if (endpoint) {
                $http.post('https://' + SERVICE_HOST_NAME + endpoint, data, { headers: headers || {} }).success(function (data, status, hdrs, config) {
                    var response = { data: data, status: status, headers: hdrs, config: config };
                    d.resolve(response);
                }).error(function (err, status, hdrs, config) {
                    var response = { error: err, status: status, headers: hdrs, config: config };
                    d.reject(response);
                });
            }
            return d.promise;
        };


        serviceFactory.PutData = function (endpoint, data, headers) {
            var d = $q.defer();
            if (endpoint) {
                $http.put('https://' + SERVICE_HOST_NAME + endpoint, data, { headers: headers || {} }).success(function (data, status, hdrs, config) {
                    var response = { data: data, status: status, headers: hdrs, config: config };
                    d.resolve(response);
                }).error(function (err, status, hdrs, config) {
                    var response = { error: err, status: status, headers: hdrs, config: config };
                    d.reject(response);
                });
            }
            return d.promise;
        };

        serviceFactory.DeleteData = function (endpoint, data, headers) {
            var d = $q.defer();
            if (endpoint) {
                $http({method: "DELETE", url:'https://' + SERVICE_HOST_NAME + endpoint, data: data, headers: headers || {'Content-Type' : 'application/json' } }).success(function (data, status, hdrs, config) {
                    var response = { data: data, status: status, headers: hdrs, config: config };
                    d.resolve(response);
                }).error(function (err, status, hdrs, config) {
                    var response = { error: err, status: status, headers: hdrs, config: config };
                    d.reject(response);
                });
            }
            return d.promise;
        };


        serviceFactory.Installations = function (pageSize, page, term, sortCol, descend) {
            var params = buildParams({ pageSize: pageSize, page: page, sort: sortCol, term: term, descend: descend });
            return serviceFactory.GetData('Api/Installations' + params);
        };

        serviceFactory.InstallationsWithDetails = function (term, filter) {
            var params = buildParams({ term: term, filter: filter });
            return serviceFactory.GetData('Api/Installations' + params);
        };

        function buildParams(params) {
            return Object.keys(params).filter(function (key) {
                return (params[key]);
            }).map(function (key, index) {
                var val = index == 0 ? "?" : "";
                return val + [key, params[key]].map(encodeURIComponent).join("=");
            }).join("&");
        }

        serviceFactory.InviteInfo = function (inviteId) {
            return serviceFactory.GetData('Api/Account/InviteInfo?id=' + inviteId);
        };

        serviceFactory.validateEmail = function (user, code) {
            return serviceFactory.PostData('Api/Account/ValidateEmail', { User: user, Code: code });
        };

        serviceFactory.getWebApi = function () {
            return serviceFactory.GetData('Api/Account/VersionNumber');
        };

        

        return serviceFactory;

    }]);