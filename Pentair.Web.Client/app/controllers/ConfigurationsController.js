'use strict';
var PWC = PWC || {};
PWC.controller('ConfigurationsContoller', [
    '$scope', '$rootScope', '$location', 'PropertyService', 'PropertyModel', '$timeout', 'SocketService',
    function ($scope, $rootScope, $location, propertyService, propertyModel, $timeout, socketService) {
        $scope.Busy = false;
        $scope.AdminSecurity = false;
        $scope.ConfigurationsTabs = propertyModel.configurationsTabs;
        $scope.$parent.Layout = "layout-base";
        $scope.securityEnabled = propertyModel.SecurityEnabled;
        $scope.SHOMNU = propertyModel.ACL.SHOMNU;
        $scope.ACL = propertyModel.ACL;
        $scope.Property = propertyModel;


        $rootScope.selectedTab = 'general';

        PWC.setActiveTab($scope.ConfigurationsTabs, 'general');

        $scope.preventIfNecessary = function (page, isTab) {
            
            
            if($rootScope.selectedTab.toUpperCase() !== page.toUpperCase()) {
                var activeEdit = false;
                if (page.toUpperCase() === 'HOME') {    // adding home
                    $location.path("/property");
                    console.log("==========================================  moving to Home page =======================================================");
                    return;
                }

                if($rootScope.selectedTab.toLowerCase() === 'general') {
                  angular.forEach($rootScope.forms[$rootScope.selectedTab.toLowerCase()], function(sectionEdit) {
                      if (sectionEdit.$dirty) {                      
                      activeEdit = true;
                    }
                  });
                  console.log("==========================================  moving to general =======================================================");
                }
                if($rootScope.selectedTab.toLowerCase() === 'circuits') {
                  angular.forEach($rootScope.forms[$rootScope.selectedTab.toLowerCase()], function(sectionEdit, key) {
                      if(key === 'aux') {
                        angular.forEach(sectionEdit, function(panel) {
                          angular.forEach(panel, function(circuit) {
                            if(circuit.$dirty) {
                              activeEdit = true;
                            }
                          }); // circuits
                        }); // panels
                      }
                      if(key === 'feature' || key === 'group') {
                        angular.forEach(sectionEdit, function(item) {
                          if(item.$dirty) {
                            activeEdit = true;
                          }
                        });
                      }
                  });
                  console.log("==========================================  moving to circuits =======================================================");
                }
              if($rootScope.selectedTab.toLowerCase() === 'otherequipment') {
                angular.forEach($rootScope.forms[$rootScope.selectedTab.toLowerCase()], function(sectionEdit, key) {
                  if(key === 'valve') {
                    angular.forEach(sectionEdit, function(panel) {
                      angular.forEach(panel, function(circuit) {
                        if(circuit.$dirty) {
                          activeEdit = true;
                        }
                      }); // valves
                    }); // panels
                  }
                  if(key === 'heater' || key === 'heatergroup' || key === 'cover') {
                    angular.forEach(sectionEdit, function(item) {
                      if(item.$dirty) {
                        activeEdit = true;
                      }
                    });
                  }
                });
                console.log("==========================================  moving to otherequip =======================================================");
              }
                if($rootScope.selectedTab.toLowerCase() === 'bodies') {
                  angular.forEach($rootScope.forms[$rootScope.selectedTab.toLowerCase()], function(panel) {
                    angular.forEach(panel, function(body) {
                      if(body.$dirty) {
                        activeEdit = true;
                      }
                    }); // bodies
                  }); // panels
                  console.log("==========================================  moving to bodies =======================================================");
                }
                if($rootScope.selectedTab.toLowerCase() === 'chemistrycontrol'
                  || $rootScope.selectedTab.toLowerCase() === 'pumps' || $rootScope.selectedTab.toLowerCase() === 'remotes') {
                  angular.forEach($rootScope.forms[$rootScope.selectedTab.toLowerCase()], function(item) {
                    if(item.$dirty) {
                      activeEdit = true;
                    }
                  }); // items
                }
                if(activeEdit) {
                    $timeout(function() { // NOTE this is a temporary workaround since I couldn't get a before select to work for tabs
                        PWC.setActiveTab($scope.ConfigurationsTabs, $rootScope.selectedTab);
                    }, 100);
                    $scope.bodyStyle.overflow = 'hidden';
                    $scope.cancelNavigateModal = true;
                    $scope.navigateTo = page;
                    $scope.isTab = isTab;
                } else {
                    // the form wasn't dirty so just proceed to the tab.
                    socketService.clearSubscriptions();
                    propertyService.SubscribeToPage(page).then(function () {
                      setFormsOnTabToPristine(page);
                    });
                }
            }

        };

        $scope.gotohomePage = function () {
            $location.path("/property");

        }
        
        $scope.getTabName = function (tab) {
            var name = tab.heading
            //if ($scope.isSecurityLocked(tab))
            //    name = name + " *"

            return name
        }
        /*********** SECURITY MODE July-18-2018 ********
        VacationMode		,  v        done 
        Support  		    ,  o        NA    
        GeneralSettings		,  g        done  , under testing
        AlertsNotification	,  q        done
        UserPortal		    ,  i        ONLY  ADMIN : THW
        Groups			    ,  k        done 
        Remotes			    ,  w        Removed     *** this option has been removed July-2018 : replace 'w' to 'a' option as per steve recomendation
        AdvancedSettings	,  a        done   , under testing
        Chemistry		    ,  c        done
        Status			    ,  t        done
        DeleteStatus		,  r        Removed    *** this option has been removed July-2018 : replace 'r' to 't' option as per steve recomendation
        Schedules		    ,  e        done
        Features		    ,  f        done
        Lights			    ,  l        done
        Pool			    ,  p        done
        PoolHeatSource		,  h        **
        PoolTemp		    ,  P        **
        Spa			        ,  m        done
        SpaHeatSource		,  n        **
        SpaTemp			    ,  S        **
        History			    ,  u        done
        MultiBodyDrawer		,  b        NA
        ServiceMode		    ,  x        done 
        SystemConfig		,  C        done 
        ********************************/

        $scope.isSecurityEnabled = function () {

            // checking
            if ($scope.ACL) {    // ckecking if ACL exist
                if ($scope.ACL.OBJNAM) {    // ckecking if objnam exist
                    if ($scope.ACL.OBJNAM === "U0000") {   // checking GUEST IS SIGNED
                        return true //security is enabled by GUEST user OR HAS NO RIGTHS ...
                    }
                }
            }

            if ($scope.Property && ($scope.Property.SecurityEnabled === true))     
                return true
            else
                return false
        }

        $scope.isSecurityLocked = function (tab) {
           // console.log(" ::   tab: ", tab.heading)
            if (tab.heading === 'HOME') {       // allow always home page view only..
                return false;
            }
            
            if (tab.heading === 'SCHEDULES' && ($scope.isSecurityEnabled() && $scope.SHOMNU.indexOf('e') < 0)) {
                return true;
            }            

            if (tab.heading === 'SYSTEM CONFIGURATION') {

                /*    if (($scope.SHOMNU.indexOf('a') >= 0) ||    // AdvancedSettings
                         ($scope.SHOMNU.indexOf('q') >= 0) ||    // AlertsNotification
                         ($scope.SHOMNU.indexOf('g') >= 0))       // general settings */
                        
                return false    // allow to reach this menu
            }

            if (tab.heading === 'GENERAL SETTINGS') {
                if ($scope.isSecurityEnabled())
                    if (($scope.SHOMNU.indexOf('a') >= 0) ||    // AdvancedSettings
                         ($scope.SHOMNU.indexOf('q') >= 0) ||    // AlertsNotification
                         ($scope.SHOMNU.indexOf('g') >= 0)       // general settings
                        )
                        return false;
                    else
                        return true;
            }

            if (tab.heading === 'REMOTES') {
                if ($scope.isSecurityEnabled())
                    if ($scope.SHOMNU.indexOf('a') >= 0)    // remotes                                                
                        return false;
                    else
                        return true;
            }

            // secure this items
            if ($scope.isSecurityEnabled()) {
                // check if this user is supervisor :  
                if ($scope.SHOMNU.indexOf('C') < 0) {     //   SystemConfig, C

                    switch (tab.heading) {
                        case 'CIRCUITS': return true
                        case 'BODIES': return true
                        case 'CHEMISTRY CONTROL': return true                    
                        case 'OTHER EQUIPMENT': return true
                        case 'PUMPS': return true
                    }
                }
            }

           return false;
        }

        $scope.proceedNavigate = function() {
            // pass in whether or not we are switching tabs or pages and where we are trying to navigate to.
            if($scope.isTab) {
                socketService.clearSubscriptions();
                propertyService.SubscribeToPage($scope.navigateTo).then(function () {
                  setFormsOnTabToPristine($scope.navigateTo);
                  PWC.setActiveTab($scope.ConfigurationsTabs, $scope.navigateTo);
                });
            } else {
                // continue
            }
            $scope.cancelNavigateModal = false;
            $scope.bodyStyle.overflow = 'none';
        };

        function setFormsOnTabToPristine(page) {
          angular.forEach($rootScope.forms[page.toLowerCase()], function(sectionEdit) {
            // Reset the form since we re fetch data when switching tabs.
            // the hierarchy might be 2 or 3 levels deep
            try {
              sectionEdit.$setPristine();
            } catch(e) {
              angular.forEach(sectionEdit, function(subSectionEdit) {
                try {
                  subSectionEdit.$setPristine();
                } catch(e) {
                  angular.forEach(subSectionEdit, function(subSubSectionEdit) {
                    subSubSectionEdit.$setPristine();
                  })
                }
              });
            }
            $rootScope.selectedTab = page.toLowerCase();
          });
        }

        $scope.cancelNavigate = function() {
            $scope.cancelNavigateModal = false;
            $scope.bodyStyle.overflow = 'none';
        };
    }
]);