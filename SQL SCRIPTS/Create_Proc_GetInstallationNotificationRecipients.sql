﻿-- =============================================================
--
-- Author:		Vladimir Beloglazov
-- Create date: 04/04/2018
--
-- Creates:
--
--		Table Type:			None
--
--		Stored Procedure:	PGetInstallationNotificationRecipients
--
-- =============================================================

USE PathfinderIntelliCenter
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (
	SELECT * FROM sys.objects
	WHERE object_id = OBJECT_ID(N'dbo.PGetInstallationNotificationRecipients')
	AND type in (N'P', N'PC')
)
DROP PROCEDURE dbo.PGetInstallationNotificationRecipients
GO

-- -------------------------------------------------------------
--
-- Returns [NotificationRecipients] record field values.
--
-- -------------------------------------------------------------
CREATE PROCEDURE dbo.PGetInstallationNotificationRecipients
     @installationId AS int
AS
BEGIN

	SET NOCOUNT OFF

	-- Result set
	SELECT *
	FROM dbo.NotificationRecipients
	WHERE InstallationId = @installationId
                                                     
END
GO
