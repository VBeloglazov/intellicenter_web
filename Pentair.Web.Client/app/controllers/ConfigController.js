﻿'use strict';
var PWC = PWC || {};
//  saving this file  
PWC.controller('ConfigController', [
    '$scope', '$location', 'PropertyService', 'PropertyModel',
    function ($scope, $location, propertyService, propertyModel) {
        $scope.$parent.Layout = "layout-base";
        $scope.GoToDashboard = function () {
            $location.path("/property");
        }

        $scope.GoToSystemProtectionPage = function () {
            $location.path("/systemProtection");

        }

        $scope.GoToUserNotificationPage = function () {
            $location.path("/userNotifications");

        }

        $scope.GoToSystemInformationPage = function () {
            $location.path("/systemInformation");
        }

        $scope.GoToSystemPersonalityPage = function () {
            $location.path("/systemPersonality");
        }

        
        $scope.GoToConfigureSystemPage = function () {
            $location.path("/systemConfigureHardware");
        }

    }
]);
