PWC.directive('usageChart', function($parse) {
  var directiveDefinitionObject = {
    restrict: 'E',
    replace: false,
    scope: {
      data: '=chartData',
      xAxis: '=chartXAxis',
      dataTopic: '=chartDataTopic'
    },
    link: function(scope, element, attrs) {
      var taskNames = [];
      var taskStatus = {};
      var xAxisMinutes = getTaskTimeRange();
      var usageTypes;
      var usageData = [];
      scope.$watch("dataTopic", function (newValue, oldValue) {
        if(newValue !== oldValue) {
          drawChart(scope, element);
        }
      }, true);

      scope.$watch("data", function (newValue, oldValue) {
        if(newValue !== oldValue) {
          drawChart(scope, element);
        }
      }, true);


      function drawChart(scope, element) {

        xAxisMinutes = getTaskTimeRange();
        usageData = [];
        var data = scope.data;
        if (data) {
          var uniqueId = 'a' + Math.uuid(15);

          element.html('');
          element.append('<div style="position:relative" id="' + uniqueId + '"></div>');
          var historyAnchor = d3.select("#" + uniqueId).attr('class', 'usages');

          var format = "%H:%M";
          var containerWidth = 978;
          var containerHeight;

          if(scope.dataTopic === 'temp') {
            if (scope.data.poolUsage) {
              mapUsages(scope.data.poolUsage);
            }
            if (scope.data.spaUsage) {
              mapUsages(scope.data.spaUsage);
            }
            if (scope.data.heaterUsage) {
              scope.data.heaterUsage.SNAME = 'Heater'
              scope.data.heaterUsage.history = scope.data.heaterUsage
              mapUsages(scope.data.heaterUsage);
            }
            if (scope.data.lightUsage) {
              scope.data.lightUsage.SNAME = 'Lights'
              scope.data.lightUsage.history = angular.copy(scope.data.lightUsage)
              mapUsages(scope.data.lightUsage);
            }
            if (scope.data.solarUsage) {
              scope.data.solarUsage.SNAME = 'Solar'
              scope.data.solarUsage.history = angular.copy(scope.data.solarUsage)
              mapUsages(scope.data.solarUsage);
            }

          } else {
            if (scope.data.orpFeed) {
              scope.data.orpFeed.SNAME = 'ORP Feed';
              scope.data.orpFeed.history = angular.copy(scope.data.orpFeed)
              mapUsages(scope.data.orpFeed);
            }
            if (scope.data.phFeed) {
              scope.data.phFeed.SNAME = 'pH Feed';
              scope.data.phFeed.history = angular.copy(scope.data.phFeed)
              mapUsages(scope.data.phFeed);
            }
          }

          getTaskProperties();

          if (scope.dataTopic === 'temp') {
            containerHeight = usageTypes.length * 40;
          } else {
            containerHeight = usageTypes.length * 40;
          }

          addVerticalLines(uniqueId, scope, containerHeight);

          var legendContainer = historyAnchor.append("div").attr("class", "usage").attr("height", containerHeight);
          legendContainer.attr("style", 'height: ' + containerHeight + 'px');
          var legendItem = legendContainer.selectAll("div")
            .data(usageTypes).enter().append("div").attr('class', 'legendItem').append('svg')
          legendItem
            .append("rect")
            .attr("rx", 0)
            .attr("ry", 0)
            .attr("class", function(d){
              if(taskStatus[d.name] == null){ return "bar";}
              return taskStatus[d.name].toLowerCase();
            })
            .attr("transform","translate(220, 3)")
            .attr("height", 30)
            .attr("width", 30)
          legendItem
            .append("text")
            .text(function(d) { return d.name})
            .attr("x", 100)
            .attr("y", 25)
            .attr("font-size", "18px")
            .attr("fill", "black")
            .attr("text-anchor", "middle");
          legendItem
            .append("text")
            .text(function(d) { return d.usage + '%'})
            .attr("x", 215)
            .attr("y", 25)
            .attr("font-size", "18px")
            .attr("fill", "black")
            .attr("text-anchor", "end");

          var gantt = d3.gantt(uniqueId, containerWidth, containerHeight).taskTypes(taskNames).taskStatus(taskStatus).tickFormat(format).xAxisDomain(scope.xAxis);
          //gantt.timeDomainMode("fixed");
          gantt(usageData);
        }

      }
      function mapUsages(originalUsage) {
        var usage = [];
        if(originalUsage.history.length > 0) {
          //if first item in history is status off, remove it.
          if(originalUsage.history[0].value === 'OFF') originalUsage.history.splice(0, 1);
          //if last item in history is on, remove it.
          if(originalUsage.history.length > 0 && originalUsage.history[originalUsage.history.length - 1].value === 'ON') 
          {
                originalUsage.history.splice(originalUsage.history.length - 1, 1);
          }

          for (var i = 0; i < originalUsage.history.length; i++) {
            //Prevent two ON states in a row
            if(originalUsage.history[i].value === 'ON' && originalUsage.history[i-1] && originalUsage.history[i-1].value === 'ON' ) {
              continue;
            }
            //If value is ON, then add to usage array with startTime
            else if(originalUsage.history[i].value === 'ON' ) {
              usage.push({
                "startDate": moment.unix(parseInt(originalUsage.history[i].time)).toDate(),
                "taskName": originalUsage.SNAME,
                "status": originalUsage.SNAME
              })
            }
            //If value is OFF, then get the last item of usage and add endTime
            else if(usage[usage.length - 1]
              && originalUsage.history[i].value === 'OFF') {
              usage[usage.length - 1].endDate = moment.unix(parseInt(originalUsage.history[i].time)).toDate()
            }
          }
          usageData.push.apply(usageData, usage)
        }

      }

      function getTaskProperties() {
        var names = [];
        for(i = 0; i < usageData.length; i++) {
          if(names.indexOf(usageData[i].status) < 0 ) names.push(usageData[i].status);
        }
        taskNames = names;
        getTaskStatus(names);
        calculateUsages();
      }
      function getTaskStatus(names) {
        for(i = 0; i < names.length; i++) {
          var name = names[i];
          taskStatus[name] = name.toLowerCase();
        }
      }
      function calculateUsages() {
        usageTypes = taskNames.map(function(usage) {
          var rObj = {};
          rObj.name = usage;
          rObj.usage = getUsageForTypeInXDomain(usage);
          return rObj;
        })
      }
      function getTaskTimeRange() {
        var time1 = moment(scope.xAxis.domain[0]);
        var time2 = moment(scope.xAxis.domain[1]);
        var duration = moment.duration(time2.diff(time1));
        var days = duration.asMinutes();
        return days;
      }
      function getUsageForTypeInXDomain(type) {
        var typeTasksMinutes = 0;
        var xAxisStartTime =  moment(scope.xAxis.domain[0]);
        var xAxisEndTime =  moment(scope.xAxis.domain[1]);
        for(i = 0; i < usageData.length; i++) {
          var minutes = 0;
          var taskStartTime = moment(usageData[i].startDate);
          var taskEndTime = moment(usageData[i].endDate);
          // Get Tasks where times fall perfectly between xAxis domain times
          if( usageData[i].taskName == type
            && moment(usageData[i].startDate).isAfter(xAxisStartTime)
            && moment(usageData[i].endDate).isBefore(xAxisEndTime)) {
            minutes = moment.duration(taskEndTime.diff(taskStartTime)).asMinutes();
          }
          // Get Tasks where startTime before xAxisStart && endTime after xAxisStart
          else if( usageData[i].taskName == type
            && moment(usageData[i].startDate).isBefore(xAxisStartTime)
            && moment(usageData[i].endDate).isAfter(xAxisStartTime)) {
            minutes = moment.duration(taskEndTime.diff(xAxisStartTime)).asMinutes();
          }
          // Get Tasks where startTime is before xAxisEndTime && endTIme is after xAxisEndTime
          else if( usageData[i].taskName == type
            && moment(usageData[i].startDate).isBefore(xAxisEndTime)
            && moment(usageData[i].endDate).isAfter(xAxisEndTime)) {
            minutes = moment.duration(xAxisEndTime.diff(taskStartTime)).asMinutes();
          }
          // Calculate minutes between task startdate && endDate
          typeTasksMinutes += minutes;
        }
        return Math.ceil((typeTasksMinutes/xAxisMinutes)*100);
      }
      function addVerticalLines(uniqueId, dataTopic, containerHeight) {
        //### for the vertical lines
        var usageAnchor = d3.select("#" + uniqueId);
        var usageSvg = usageAnchor.append("svg")
          .attr("style", "position:absolute;left:262px")
          .attr('height',containerHeight)
          .attr('width',988);
        var margin = {
          top: 0,
          right: 15,
          bottom: 15,
          left: 15
        };
        var width = usageSvg.attr("width") - margin.left - margin.right;
        var height = usageSvg.attr("height");

        var x = d3.time.scale().range([0, width]);
        x.domain(scope.xAxis.domain);
        var xAxis = d3.svg.axis().scale(x);
        xAxis.orient("bottom");
        xAxis.innerTickSize(-height);
        xAxis.outerTickSize(0);
        xAxis.ticks(scope.xAxis.ticks.timeScale, scope.xAxis.ticks.timeInterval);
        xAxis.tickFormat("");
        var g = usageSvg.append("g");
        g.append("g")
          .attr("class", "axis axis--x")
          .attr("transform", "translate(" + margin.left + "," + height + ")")
          .call(xAxis);
        //### end vertical lines
      }


    }
  }
  return directiveDefinitionObject;
});