﻿using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using ListView = Pentair.Desktop.Simulator.Views.ListView;

namespace Pentair.Desktop.Simulator
{
    public static class Helpers
    {
        public static Window GetParentWindow(DependencyObject child)
        {
            DependencyObject parentObject = VisualTreeHelper.GetParent(child);

            if (parentObject == null)
            {
                return null;
            }

            Window parent = parentObject as Window;
            if (parent != null)
            {
                return parent;
            }
            else
            {
                return GetParentWindow(parentObject);
            }
        }

        public static TabControl GetListView(DependencyObject child)
        {
            DependencyObject parentObject = VisualTreeHelper.GetParent(child);

            if (parentObject == null)
            {
                return null;
            }

            var item = parentObject as TabControl;
            if (null != item)
            {
                return item;
            }
            else
            {
                return GetListView(parentObject);
            }
        }
    }
}
