'use strict';
var PWC = PWC || {};
PWC.controller('SettingsController', [
    '$scope', '$rootScope', '$location', 'PropertyService', 'PropertyModel',
    function ($scope, $rootScope, $location, propertyService, propertyModel) {
        $scope.Busy = false;
        $scope.AdminSecurity = false;
        $scope.SettingsTabs = propertyModel.settingsTabs;
        $scope.$parent.Layout = "layout-base";
        $scope.securityEnabled = propertyModel.SecurityEnabled;
        $scope.SHOMNU = propertyModel.ACL.SHOMNU;

        PWC.setActiveTab($scope.SettingsTabs, 'schedules');

        $scope.GoToDashboard = function() {
            $location.path("/property");
        };

        $scope.GoToConfiguration = function() {
            $location.path("/configurations");
        };


        $scope.Visibility = function (tab) {
            //console.log("--------------------------------------****  VACATION : " + tab.heading);
            if (tab.heading === 'VACATION') {
                if (propertyModel.Objects['_5451']) {
                    //return propertyModel.Objects['_5451'].AVAIL == 'ON';
                    return propertyModel.Objects['_5451'].VACFLO == 'ON';
                }
                else {
                    return false;
                }
            }
            return true;
        };

        $scope.getTabName = function (Tab) {
            var name = Tab.heading
            //if ($scope.isSecurityLocked(Tab)) {
            //    name = name + " *"
            //}
            return name 
        };


        /*********** SECURITY MODE July-18-2018 ********
        VacationMode		,  v        done 
        Support  		    ,  o        NA    
        GeneralSettings		,  g        done  , under testing
        AlertsNotification	,  q        done
        UserPortal		    ,  i        ONLY  ADMIN : THW
        Groups			    ,  k        done 
        Remotes			    ,  w        Removed     *** this option has been removed July-2018 : replace 'w' to 'a' option as per steve recomendation
        AdvancedSettings	,  a        done   , under testing
        Chemistry		    ,  c        done
        Status			    ,  t        done
        DeleteStatus		,  r        Removed    *** this option has been removed July-2018 : replace 'r' to 't' option as per steve recomendation
        Schedules		    ,  e        done
        Features		    ,  f        done
        Lights			    ,  l        done
        Pool			    ,  p        done
        PoolHeatSource		,  h        **
        PoolTemp		    ,  P        **
        Spa			        ,  m        done
        SpaHeatSource		,  n        **
        SpaTemp			    ,  S        **
        History			    ,  u        done
        MultiBodyDrawer		,  b        NA
        ServiceMode		    ,  x        done 
        SystemConfig		,  C        done 
        ********************************/

        $scope.isSecurityLocked = function (tab) {

            // check first if security is enabled
            if ($scope.securityEnabled === false) {
                return false
            }

            if (tab.heading === 'HOME') {       // allow always home page view only..
                return false;
            }

            if (tab.heading === 'SCHEDULES' && ($scope.securityEnabled && $scope.SHOMNU.indexOf('e') < 0)) {
              return true;
            }           

            if (tab.heading === 'LIGHT GROUPS') {
                if ($scope.securityEnabled &&
                   ($scope.SHOMNU.indexOf('l') >= 0) &&
                   ($scope.SHOMNU.indexOf('k') >= 0))
                    return false
                else
                    return true
            }

            if (tab.heading === 'VACATION' && ($scope.securityEnabled && $scope.SHOMNU.indexOf('v') < 0)) {
              return true;
            }

            if (tab.heading === 'SYSTEM CONFIGURATION') {
                return false;  // allow this menu always
            }

            return false;
        };
    }
]);