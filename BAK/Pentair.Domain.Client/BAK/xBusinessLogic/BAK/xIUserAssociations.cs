﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pentair.Domain.Client.BusinessLogic
{
    /* TODO: 2b removed 2017-12-13
    public interface IUserAssociations
    {
        /// <summary>
        /// UserInstallationList retrieves a list of pools that the
        /// user has access to.
        /// </summary>
        /// <param name="userId">UID of user to retrieve a list of pools for</param>
        /// <returns>A list of UserInstallations objects</returns>
        List<UserInstallations> UserInstallationList(Guid userId);

        /// <summary>
        /// Remove a user association from a pool
        /// </summary>
        /// <param name="user">user to remove</param>
        /// <param name="property">property to remove</param>
        UserManagementResult RemoveUserFromInstallation(Guid user, Guid property);

        /// <summary>
        /// Remove the invitation for this user
        /// </summary>
        /// <param name="user">Email address of user invite</param>
        /// <param name="property">Property ID of invite</param>
        /// <returns></returns>
        UserManagementResult RemoveUserInvite(string user, Guid property);

        /// <summary>
        /// Change the access rights for a remote pool user
        /// </summary>
        /// <param name="email">users email address</param>
        /// <param name="token">new access rights token</param>
        /// <param name="user">User Id</param>
        /// <param name="property">Property Id</param>
        /// <returns></returns>
        UserManagementResult ChangeUserAccess(string email, string token, Guid user, Guid property);
    }
    */
}
