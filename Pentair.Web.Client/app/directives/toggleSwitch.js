﻿var PWC = PWC || {};

PWC.directive('toggleSwitch', function () {
    'use strict';
    return {
        restrict: 'EA',
        replace: true,
        scope: {
            targetCircuit: '=',
            disabled: '=',
            toggleOnclick: '&'
        },
        template: ' <div class="toggle-switch row" for="shared-{{targetCircuit.SNAME}}" ng-click="clicked()">' +
                     ' <div class="col-sm-3 toggle-label">off</div>' +
                     ' <div class="col-sm-6"><input type="checkbox" ng-model="value" ng-disabled="true" id="shared-{{targetCircuit.SNAME}}" name="check"/>' +
                     ' <span class="toggler">' +
                         ' <span class="toggle"></span>' +
                      '</span></div>' +
                      '<div class="col-sm-3 no-padding toggle-label">on</div>' +
                  '</div>',
        link: function (scope, element, attrs) {
            if (PWC.isEmptyObject(scope.targetCircuit)) {
                return;
            }
            if (!attrs.param) {
                attrs.param = 'STATUS';
            }

            scope.clicked = function () {
                if (!scope.disabled) {
                    var newValue = scope.value ? 'OFF' : 'ON';
                    var params = { obj: scope.targetCircuit, value: newValue, param: attrs.param };
                    scope.toggleOnclick(params);
                }
            };

            //Update UI Value when model Changes
            scope.$watch('targetCircuit.' + attrs.param, function (newVal, oldVal) {
                scope.value = isCircuitOn();
            });

            function isCircuitOn() {
                return (scope.targetCircuit[attrs.param] === 'ON' || scope.targetCircuit[attrs.param] === 'DLYOFF');
            }

        }
    };
});

