﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Pentair.Domain.Simulator.Entity
{
    public class BodySubType
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public string Name { get; set; }
    }
}