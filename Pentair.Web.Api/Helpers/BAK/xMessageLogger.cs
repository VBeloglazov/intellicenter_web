﻿using Pentair.Domain.Client.Entity;
using Pentair.Domain.Client.Infrastructure;
using Pentair.Domain.Client.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Web.Configuration;

namespace Pentair.Web.Api.Helpers
{
    // TODO: VB - 2b removed
    /*
    public static class MessageLogger
    {
        public static bool IsEnabled { get; private set; }
        public static bool IsVerbose { get; private set; }

        static MessageLogger()
        {
            Configuration configSettings = WebConfigurationManager.OpenWebConfiguration("/service");
            
            MessageLogger.IsEnabled = Boolean.Parse(configSettings.AppSettings.Settings["enableLogging"].Value);
            MessageLogger.IsVerbose = Boolean.Parse(configSettings.AppSettings.Settings["logVerbose"].Value);

        } // static MessageLogger()

        public static void LogMessage(
            string action,
            string message,
            int hardwareId,
            Guid userId)
        {
            if (MessageLogger.IsEnabled)
            {
                using (var context = new ApiContext())
                {
                    try
                    {
                        if (null == message)
                        {
                            message = "ERROR";
                        }
                        var messageLogRepo = new Repository<MessageLog, ApiContext>(context);
                        var messageLog = new MessageLog()
                        {
                            Action = action,
                            Message = message,
                            HardwareID = hardwareId,
                            UserID = userId
                        };
                        messageLogRepo.AddItem(messageLog);
                        messageLogRepo.SaveChanges();
                    }
                    // TODO Handle this condition properly
                    catch (Exception ex)
                    {
                        var m = ex.Message;
                    }
                }
            }
        }
    }
    */
}
