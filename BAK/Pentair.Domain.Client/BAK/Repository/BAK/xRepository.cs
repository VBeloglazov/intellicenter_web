﻿using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;

namespace Pentair.Domain.Client.Repository
{
    /* TODO: 2b removed 2017-12-13
    public class Repository<TEntity, TContext> : IRepository<TEntity>
        where TEntity : class 
        where TContext : DbContext
    {
        protected TContext Context;
        private readonly DbSet<TEntity> _entityDbSet;

        public Repository(TContext context)
        {
            Context = context;
            _entityDbSet = Context.Set<TEntity>();
        }

        public IQueryable<TEntity> Items
        {
            get
            {
                return _entityDbSet;
            }
        }

        public TEntity AddItem(TEntity newItem)
        {
            return _entityDbSet.Add(newItem);
        }

        public List<TEntity> ItemList()
        {
            return _entityDbSet.ToList<TEntity>();
        }

        public TEntity FindById(int id)
        {
            return _entityDbSet.Find(id);
        }

        public TEntity RemoveById(int id)
        {
            var toRemove = this.FindById(id);
            if (toRemove != null)
            {
                this.Remove(toRemove);
            }

            return toRemove;
        }

        public TEntity Remove(TEntity toBeRemoved)
        {
            return _entityDbSet.Remove(toBeRemoved);
        }

        public int SaveChanges()
        {
            return Context.SaveChanges();
        }
    }
    */
}