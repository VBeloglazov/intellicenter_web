﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Pentair.Domain.Simulator.Entity
{
    public class Heater
    {
        [Key]
        [MaxLength(16)]
        public string OBJNAM { get; set; }
        public UInt32 OBJREV { get; set; }
        [MaxLength(16)]
        public string OBJTYP { get; set; }
        [MaxLength(16)]
        public string STATIC { get; set; }
        public int LISTORD { get; set; }
        [MaxLength(128)]
        public string HNAME { get; set; }
        [MaxLength(128)]
        public string SNAME { get; set; }
        [MaxLength(16)]
        public string RLY { get; set; }
        [MaxLength(16)]
        public string SUBTYP { get; set; }
        [MaxLength(16)]
        public string BODY { get; set; }
        [MaxLength(16)]
        public string SHARE { get; set; }
        [MaxLength(16)]
        public string COMUART { get; set; }
        [MaxLength(16)]
        public string PARENT { get; set; }
        [MaxLength(16)]
        public string COOL { get; set; }
        public int START { get; set; }
        public int STOP { get; set; }
        public int DLY { get; set; }
        [MaxLength(16)]
        public string STATUS { get; set; }
        [MaxLength(16)]
        public string PERMIT { get; set; }
        public int TIMOUT { get; set; }
        [MaxLength(16)]
        public string READY { get; set; }
    }
}