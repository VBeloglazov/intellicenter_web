﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Pentair.Web.Api
{
    public class WriteHistoryMessageResponse : ResponseCommand
    {
        [JsonProperty("messageNo")]
        public string MessageNo { get; private set; }

        [JsonProperty("timestamp")]
        public string TimeStamp { get; private set; }

        [JsonProperty("error")]
        public string Error { get; private set; }

        public WriteHistoryMessageResponse(
            string messageID,
            string response,
            int messageNo,
            string timestamp,
            string error
            )
            : base(CommandString.WriteHistoryMessageResponse, messageID, response)
        {
            this.MessageNo = messageNo.ToString();
            this.TimeStamp = timestamp;
            this.Error = error;

        } // WriteHistoryMessageResponse()

        /// <summary>
        /// Always returns an object, valid or not. If invalid, check the DeserializationError property value.
        /// </summary>
        public static new WriteHistoryMessageResponse Deserialize(
            string serializedMessage)
        {
            WriteHistoryMessageResponse command = null;

            try
            {
                command = JsonConvert.DeserializeObject<WriteHistoryMessageResponse>(serializedMessage);
                command.IsValid = true;
            }
            catch (Exception e)
            {
                // Deserialization as WriteHistoryMessageResponse failed

                StringBuilder sbError = new StringBuilder();

                sbError.Append("Cannot deserialize as WriteHistoryMessageResponse. " + e.Message);

                // Try to get at least the base message data
                ResponseCommand baseCommand = ResponseCommand.Deserialize(serializedMessage);

                if (!baseCommand.IsValid)
                {
                    sbError.Append("\n" + baseCommand.DeserializationError);
                }

                command = new WriteHistoryMessageResponse(
                    messageID: baseCommand.MessageID,
                    response: ResponseString.BadRequest,
                    messageNo: 0,
                    timestamp: String.Empty,
                    error: sbError.ToString())
                {
                    IsValid = false,
                    DeserializationError = sbError.ToString()
                };
            }

            return command;

        } // Deserialize()

    } // class WriteHistoryMessageResponse
}

