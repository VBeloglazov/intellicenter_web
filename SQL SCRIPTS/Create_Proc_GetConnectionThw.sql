-- =============================================================
--
-- Author:		Vladimir Beloglazov
-- Create date: 04/23/2018
--
-- Creates:
--
--		Table Type:			None
--
--		Stored Procedure:	PGetConnectionThw
--
-- =============================================================

USE PathfinderIntelliCenter
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (
	SELECT * FROM sys.objects
	WHERE object_id = OBJECT_ID(N'dbo.PGetConnectionThw')
	AND type in (N'P', N'PC')
)
DROP PROCEDURE dbo.PGetConnectionThw
GO

-- -------------------------------------------------------------
--
-- Returns [Connections] record field values.
--
-- -------------------------------------------------------------
CREATE PROCEDURE dbo.PGetConnectionThw
	  @installationId	AS int
AS
BEGIN

	SET NOCOUNT OFF

	-- Result set
	SELECT *
	FROM dbo.Connections
	WHERE InstallationId = @installationId
	AND UserId = CAST(0x0 AS uniqueidentifier)

END
GO
