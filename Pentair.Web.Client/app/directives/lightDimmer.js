﻿'use strict';
var PWC = PWC || {};
PWC.directive('lightDimmer', function() {
    return {
        replace: true,
        restrict: 'A',
        scope: {
            ngDisabled: '=',
            targetCircuit: '=',
            range: '=',
            onChange: '&'
        },
        template: "<div class='progress-wrapper dropdown-menu small' ng-click='changer(); $event.stopPropagation()'>" +
            "<div class='dimmer-container'>" +
                    "<div class='child'>" +
                        "<span class='pointer-cursor {{minusClass}}' ng-click='decrease()'></span>" +
                     "</div>" +
                    "<div class='child'>" +
                        "<div round-progress max='100' current=model color='#45CCCE' bgcolor='#EDEBEE' " +
                                "radius='50' semi='isSemi' rounded='rounded' stroke='10' clockwise='clockwise'" +
                                "responsive='responsive' duration='800' animation='easeOutCubic' offset='0'" +
                                "animation-delay='0' on-render='showPreciseCurrent'>" +
                            "<svg>" +
                              "<text y='50%' x='50%' fill='#777' text-anchor='middle' dy='.3em' font-size='20px'>{{model}}%</text>" +
                              "<linearGradient id='gradient' x1='0' x2='0' y1='0' y2='1'>" +
                                    "<stop offset='5%' stop-color='green' />" +
                                    "<stop offset='95%' stop-color='gold' />" +
                                "</linearGradient>" +
                             "</svg>" +
                        "</div>" +
                    "</div>" +
                   "<div class='child'>" +
                       "<span class='pointer-cursor {{plusClass}}' ng-click='increase()'></span>" +
                  "</div>" +
                "</div>" +
                "<h5>{{'BRIGHTNESS'|translate}}</h5>" +
            "</div>",
        link: function($scope, element, attr) {
            var param = "LIMIT"
            $scope.model = $scope.model || {};
            $scope.increment = parseFloat(attr.increment) || 10;
            attr.iconSize = attr.iconSize || 'icon-lg';
            $scope.plusClass = (attr.plusClass || '') + ' icon-paddle-plus ' + attr.iconSize;
            $scope.minusClass = (attr.minusClass || '') + ' icon-paddle-minus ' + attr.iconSize;
            $scope.range = attr.range || { min: 0, max: 100 }

            if ($scope.targetCircuit && $scope.targetCircuit[param]) {
                var val = $scope.targetCircuit[param];
                setRange($scope.targetCircuit);
                setModel(val);
            } else {
                setModel(0)
            }

            $scope.increase = function() {
                var val = parseFloat($scope.model);
                var rounded = roundUpToIncrement(val);
                if (rounded === val) {
                    stepValue(val + $scope.increment);
                } else {
                    stepValue(rounded);
                }
            }

            $scope.decrease = function() {
                var val = parseFloat($scope.model);
                var rounded = roundDownToIncrement(val);
                if (rounded === val) {
                    stepValue(val - $scope.increment);
                } else {
                    stepValue(rounded);
                }
            }

            function setRange(light) {
                if (light) {
                    switch (light["SUBTYP"]) {
                        case "DIMMER":
                            $scope.range = { min: 30, max: 100 }
                            $scope.increment = 10
                            break;
                        case "GLOWT":
                            $scope.range = { min: 50, max: 100 }
                            $scope.increment = 25
                            break;
                    }
                }
            }

            function stepValue(num) {
                $scope.editmode = false;

                if (!$scope.disabled) {
                    if ($scope.targetCircuit && $scope.targetCircuit.OBJNAM) {
                        var value = checkRange(num);
                        $scope.onChange({ objNam: $scope.targetCircuit.OBJNAM, param: param, value: value });
                    }
                }
            };

            function roundUpToIncrement(val) {
                return Math.ceil(val / $scope.increment) * $scope.increment;
            }

            function roundDownToIncrement(val) {
                return Math.floor(val / $scope.increment) * $scope.increment;
            }

            function setModel(val) {
                val = Number(val);

                $scope.model = checkRange(val);
                $scope.text = $scope.model;
            }

            function checkRange(value) {
                if ($scope.range) {
                    if (value < $scope.range.min) {
                        value = $scope.range.min;
                    } else if (value > $scope.range.max) {
                        value = $scope.range.max;
                    }
                }
                return value;
            }

            //Update UI Value when model Changes
            $scope.$watch('targetCircuit.' + param, function(newVal, oldVal) {
                if (newVal && newVal !== oldVal) {
                    setModel(newVal);
                }
            });

        }
    }
});
