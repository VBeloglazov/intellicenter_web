﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Pentair.Domain.Simulator.Entity
{
    public class ValveSemaphore
    {
        [Key]
        public int ID { get; set; }
        public string Name { get; set; }
        public int DelayTimeout { get; set; }
        public string AffectingValveList { get; set; }

        // Foreign Keys
        public string ValveGroupID { get; set; }
        public int NextValveID { get; set; }
        public int? SystemConfigID { get; set; }
    }
}