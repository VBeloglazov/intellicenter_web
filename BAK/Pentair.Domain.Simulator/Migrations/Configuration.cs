namespace Pentair.Domain.Simulator.Migrations
{
    using Pentair.Domain.Simulator.Entity;
    using Pentair.Domain.Simulator.Infrastructure;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<OcpContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        /// <summary>
        /// Seed info for an empty configuration database
        /// </summary>
        /// <param name="context">data set context</param>
        protected override void Seed(OcpContext context)
        {
            try
            {
                //AddCircuitSubTypes(context);
                //AddCircuitStatuses(context);
                //AddLightModes(context);
                //AddBodySubTypes(context);
                //AddChemistryFeedType(context);
                //AddChemistryModes(context);
                //AddChemistryAcidity(context);
                //AddChemistrySubTypes(context);
                //AddClockModes(context);
                //AddClockSources(context);
                //AddDayNightModes(context);
                //AddDstModes(context);
                //AddHeaterSubTypes(context);
                //AddIpModes(context);
                //AddMenuAccessLevels(context);
                //AddMenuListOrder(context);
                //AddPersonalityModuleSubtypes(context);
                //AddPumpListOrderEnums(context);
                //AddPumpSubTypes(context);
                //AddRemoteSubTypes(context);
                //AddScheduleModes(context);
                //AddTemperatureModes(context);
                //AddUserAccessLevels(context);
                //AddValveSubTypes(context);
                //AddObjectTypes(context);
                AddHosts(context);
                AddDefaultConfiguration(context);
            }
            catch
            {

            }
        }

        private void AddHosts(OcpContext context)
        {
            context.Properties.Add(new Property {RemoteHost = "wss://localhost:44300/api/websocket"});
            context.Properties.Add(new Property {RemoteHost = "wss://pentairdev-01.cloudapp.net/service/api/websocket"});
            context.Properties.Add(new Property {RemoteHost = "wss://pentairdev-02.cloudapp.net/service/api/websocket"});
            context.Properties.Add(new Property {RemoteHost = "wss://pentairdev-03.cloudapp.net/service/api/websocket"});
            context.SaveChanges();
        }

        /// <summary>
        /// Seed data for the Circuit Sub Type enumeration
        /// </summary>
        /// <param name="context">data set context</param>
        //private void AddCircuitSubTypes(OcpContext context)
        //{
        //    context.CircuitSubTypes.AddOrUpdate(
        //        r => r.Name,
        //        new CircuitSubType { Name = "FRZ", DisplayName = "Freeze" },
        //        new CircuitSubType { Name = "GENERIC", DisplayName = "Generic" },
        //        new CircuitSubType { Name = "BODY", DisplayName = "Body Filter" },
        //        new CircuitSubType { Name = "CHEM", DisplayName = "Chem Relay" },
        //        new CircuitSubType { Name = "LIGHT", DisplayName = "Light (Generic)" },
        //        new CircuitSubType { Name = "MAGIC1", DisplayName = "Light (Magic Stream 1)" },
        //        new CircuitSubType { Name = "MAGIC2", DisplayName = "Light (Magic Stream 2)" },
        //        new CircuitSubType { Name = "INTELLI", DisplayName = "Light (IntelliBrite)" },
        //        new CircuitSubType { Name = "SAML", DisplayName = "Light (Sam/Sal)" },
        //        new CircuitSubType { Name = "COLORW", DisplayName = "Light (Collor Wheel)" },
        //        new CircuitSubType { Name = "DIMMER", DisplayName = "Light (Dimmer)" },
        //        new CircuitSubType { Name = "MASTER", DisplayName = "Master Cleaner" },
        //        new CircuitSubType { Name = "FLOOR", DisplayName = "Floor Cleaner" },
        //        new CircuitSubType { Name = "INTAKE", DisplayName = "Service Fill" },
        //        new CircuitSubType { Name = "RETURN", DisplayName = "Service Drain" },
        //        new CircuitSubType { Name = "SPILL", DisplayName = "Spillway Valve" },
        //        new CircuitSubType { Name = "INCR", DisplayName = "Pump Increment" },
        //        new CircuitSubType { Name = "DECR", DisplayName = "Pump Decrement" },
        //        new CircuitSubType { Name = "ENABLE", DisplayName = "Heater Enable" },
        //        new CircuitSubType { Name = "BOOST", DisplayName = "Heater Boost" },
        //        new CircuitSubType { Name = "BYPASS", DisplayName = "Heater Bypass" },
        //        new CircuitSubType { Name = "CIRCGRP", DisplayName = "Circuit Group" });
        //    context.SaveChanges();
        //}

        /// <summary>
        /// Seed data for the Circuit Status enumerations
        /// </summary>
        /// <param name="context">data set context</param>
        //private void AddCircuitStatuses(OcpContext context)
        //{
        //    context.CircuitStatuses.AddOrUpdate(
        //        r => r.Name,
        //        new CircuitStatus { Name = "ON" },
        //        new CircuitStatus { Name = "OFF" },
        //        new CircuitStatus { Name = "DLYON" },
        //        new CircuitStatus { Name = "DLYOFF" });
        //    context.SaveChanges();
        //}

        /// <summary>
        /// Seed data for the Light Mode enumerations
        /// </summary>
        /// <param name="context">data set context</param>
        //private void AddLightModes(OcpContext context)
        //{
        //    context.LightModes.AddOrUpdate(
        //        r => r.Name,
        //        new LightMode { Name = "ON", DisplayName = "On" },
        //        new LightMode { Name = "OFF", DisplayName = "Off" },
        //        new LightMode { Name = "ROTATE", DisplayName = "Rotate Start" },
        //        new LightMode { Name = "STOP", DisplayName = "Rotate Stop" },
        //        new LightMode { Name = "SYNC", DisplayName = "Color Sync" },
        //        new LightMode { Name = "SWIM", DisplayName = "Color Swim" },
        //        new LightMode { Name = "HOLD", DisplayName = "Hold / Hold Toggle" },
        //        new LightMode { Name = "RECALL", DisplayName = "Recall" },
        //        new LightMode { Name = "MODE", DisplayName = "Mode Toggle" },
        //        new LightMode { Name = "RESET", DisplayName = "Reset" },
        //        new LightMode { Name = "THUMP", DisplayName = "Thumper Toggle" },
        //        new LightMode { Name = "PARTY", DisplayName = "Party" },
        //        new LightMode { Name = "ROMAN", DisplayName = "Romance" },
        //        new LightMode { Name = "CARIB", DisplayName = "Caribbean" },
        //        new LightMode { Name = "AMERCA", DisplayName = "American" },
        //        new LightMode { Name = "SSET", DisplayName = "Sunset" },
        //        new LightMode { Name = "ROYAL", DisplayName = "Royalty" },
        //        new LightMode { Name = "BLUE", DisplayName = "Blue" },
        //        new LightMode { Name = "GREEN", DisplayName = "Green" },
        //        new LightMode { Name = "LITGRN", DisplayName = "Light Green" },
        //        new LightMode { Name = "RED", DisplayName = "Red" },
        //        new LightMode { Name = "LITRED", DisplayName = "Light red" },
        //        new LightMode { Name = "MAGNTA", DisplayName = "Magenta" },
        //        new LightMode { Name = "AQUA", DisplayName = "Aqua" },
        //        new LightMode { Name = "WHITE", DisplayName = "White" });
        //    context.SaveChanges();
        //}

        /// <summary>
        /// Seed data for the Body Sub Types
        /// </summary>
        /// <param name="context">data set context</param>
        //private void AddBodySubTypes(OcpContext context)
        //{
        //    context.BodySubTypes.AddOrUpdate(
        //        r => r.Name,
        //        new BodySubType { Name = "Pool" },
        //        new BodySubType { Name = "Spa" });
        //    context.SaveChanges();
        //}

        /// <summary>
        /// Seed data for the Chemistry Feed Types
        /// </summary>
        /// <param name="context">data set context</param>
        //private void AddChemistryFeedType(OcpContext context)
        //{
        //    context.ChemistryFeedTypes.AddOrUpdate(
        //        r => r.Name,
        //        new ChemistryFeedType { Name = "VOL" },
        //        new ChemistryFeedType { Name = "TIME" },
        //        new ChemistryFeedType { Name = "SETPT" },
        //        new ChemistryFeedType { Name = "NONE" });
        //    context.SaveChanges();
        //}

        /// <summary>
        /// Seed data for the Chemistry Mode
        /// </summary>
        /// <param name="context">data set context</param>
        //private void AddChemistryModes(OcpContext context)
        //{
        //    context.ChemistryModes.AddOrUpdate(
        //        r => r.Name,
        //        new ChemistryMode { Name = "DOSE" },
        //        new ChemistryMode { Name = "MIX" },
        //        new ChemistryMode { Name = "MONITOR" });
        //    context.SaveChanges();
        //}

        /// <summary>
        /// Seed data for the Chemistry Acidity
        /// </summary>
        /// <param name="context">data set context</param>
        //private void AddChemistryAcidity(OcpContext context)
        //{
        //    context.ChemistryAcidities.AddOrUpdate(
        //        r => r.Name,
        //        new ChemistryAcidity { Name = "ACID" },
        //        new ChemistryAcidity { Name = "BASE" });
        //    context.SaveChanges();
        //}

        /// <summary>
        /// Seed data for the Chemistry Sub Types
        /// </summary>
        /// <param name="context">data set context</param>
        //private void AddChemistrySubTypes(OcpContext context)
        //{
        //    context.ChemistrySubTypes.AddOrUpdate(
        //        r => r.Name,
        //        new ChemistrySubType { Name = "ICHEM", DisplayName = "IntelliChem" },
        //        new ChemistrySubType { Name = "ICHLOR", DisplayName = "IntelliChlor" });
        //    context.SaveChanges();
        //}

        /// <summary>
        /// Seed data for the Clock Mode enumerations
        /// </summary>
        /// <param name="context">data set context</param>
        //private void AddClockModes(OcpContext context)
        //{
        //    context.ClockModes.AddOrUpdate(
        //        r => r.Name,
        //        new ClockMode { Name = "AMPM" },
        //        new ClockMode { Name = "HR24" });
        //    context.SaveChanges();
        //}

        /// <summary>
        /// Seed data for the Clock Source enumerations
        /// </summary>
        /// <param name="ontext">data set context</param>
        //private void AddClockSources(OcpContext context)
        //{
        //    context.ClockSources.AddOrUpdate(
        //        r => r.Name,
        //        new ClockSource { Name = "LOCAL" },
        //        new ClockSource { Name = "URL" });
        //    context.SaveChanges();
        //}

        /// <summary>
        /// Seed data for the Day / Night mode enumerations
        /// </summary>
        /// <param name="ontext">data set context</param>
        //private void AddDayNightModes(OcpContext context)
        //{
        //    context.DayNightModes.AddOrUpdate(
        //        r => r.Name,
        //        new DayNightMode { Name = "DAY" },
        //        new DayNightMode { Name = "NITE" });
        //    context.SaveChanges();
        //}

        /// <summary>
        /// Seed data for clock Daylight Savings modes
        /// </summary>
        /// <param name="context">data set context</param>
        //private void AddDstModes(OcpContext context)
        //{
        //    context.DstModes.AddOrUpdate(
        //    r => r.Name,
        //    new DSTMode { Name = "ON" },
        //    new DSTMode { Name = "OFF" });
        //    context.SaveChanges();
        //}

        /// <summary>
        /// Seed data for the Heater Subtype enumerations
        /// </summary>
        /// <param name="context">data set context</param>
        //private void AddHeaterSubTypes(OcpContext context)
        //{
        //    context.HeaterSubTypes.AddOrUpdate(
        //        r => r.Name,
        //        new HeaterSubType { Name = "HTPMP", DisplayName = "Heater Pump" },
        //        new HeaterSubType { Name = "SOLAR", DisplayName = "Solar" },
        //        new HeaterSubType { Name = "GENERIC", DisplayName = "Generic/Gas" },
        //        new HeaterSubType { Name = "HCOMBO", DisplayName = "Heater Combo" });
        //    context.SaveChanges();
        //}

        /// <summary>
        /// Seed data for IP Mode enumerations
        /// </summary>
        /// <param name="context">data set context</param>
        //private void AddIpModes(OcpContext context)
        //{
        //    context.IpModes.AddOrUpdate(
        //    r => r.Name,
        //    new IPMode { Name = "AUTO" },
        //    new IPMode { Name = "MANUAL" });
        //    context.SaveChanges();
        //}

        /// <summary>
        /// Seed data for the Menu Access Level enumerations
        /// </summary>
        /// <param name="context">data set context</param>
        //private void AddMenuAccessLevels(OcpContext context)
        //{
        //    context.MenuAccessLevels.AddOrUpdate(
        //        r => r.Name,
        //        new MenuAccessLevel { Name = "BASIC" },
        //        new MenuAccessLevel { Name = "ADV" },
        //        new MenuAccessLevel { Name = "SUPER" });
        //    context.SaveChanges();
        //}

        /// <summary>
        /// Seed data for the MenuListOrder enumerations
        /// </summary>
        /// <param name="context"></param>
        //private void AddMenuListOrder(OcpContext context)
        //{
        //    context.MenuListOrders.AddOrUpdate(
        //        r => r.Name,
        //        new MenuListOrder { Name = "FREQ" },
        //        new MenuListOrder { Name = "RECENT" },
        //        new MenuListOrder { Name = "CUSTOM" },
        //        new MenuListOrder { Name = "ALPHA" });
        //    context.SaveChanges();
        //}

        /// <summary>
        /// Seed data for the Personality Module Sub types
        /// </summary>
        /// <param name="context"></param>
        //private void AddPersonalityModuleSubtypes(OcpContext context)
        //{
        //    context.PersonalityModuleSubTypes.AddOrUpdate(
        //        r => r.Name,
        //        new PersonalityModuleSubType { Name = "I5P", DisplayName = "I5P" },
        //        new PersonalityModuleSubType { Name = "I5X", DisplayName = "I5X" },
        //        new PersonalityModuleSubType { Name = "I5PS", DisplayName = "I5PS" },
        //        new PersonalityModuleSubType { Name = "I8P", DisplayName = "I8P" },
        //        new PersonalityModuleSubType { Name = "I8PS", DisplayName = "I8PS" },
        //        new PersonalityModuleSubType { Name = "I10P", DisplayName = "I10P" },
        //        new PersonalityModuleSubType { Name = "I10S", DisplayName = "I10S" },
        //        new PersonalityModuleSubType { Name = "I10D", DisplayName = "I10D" },
        //        new PersonalityModuleSubType { Name = "ANLGEXP", DisplayName = "Analog Expansion" },
        //        new PersonalityModuleSubType { Name = "VALVEXP", DisplayName = "Valve Expansion" },
        //        new PersonalityModuleSubType { Name = "COMLNK", DisplayName = "ComLink Adaptor" },
        //        new PersonalityModuleSubType { Name = "ICHLOR", DisplayName = "Ichlor Expansion" },
        //        new PersonalityModuleSubType { Name = "OCP", DisplayName = "Outdoor Control Panel" },
        //        new PersonalityModuleSubType { Name = "ICP", DisplayName = "Indoor Control Panel" },
        //        new PersonalityModuleSubType { Name = "WCP", DisplayName = "Wireless Control Panel" },
        //        new PersonalityModuleSubType { Name = "XCP", DisplayName = "Expansion Control Panel" },
        //        new PersonalityModuleSubType { Name = "TXRREM", DisplayName = "Transceiver Remote" },
        //        new PersonalityModuleSubType { Name = "TXRCP", DisplayName = "Transceiver Coordinator" });
        //    context.SaveChanges();
        //}

        /// <summary>
        /// Seed data for the Pump List Order enumerations
        /// </summary>
        /// <param name="context"></param>
        //private void AddPumpListOrderEnums(OcpContext context)
        //{
        //    context.PumpListOrders.AddOrUpdate(
        //        r => r.Name,
        //        new PumpListOrder { Name = "HI" },
        //        new PumpListOrder { Name = "RECENT" },
        //        new PumpListOrder { Name = "PRIOR" });
        //    context.SaveChanges();
        //}

        /// <summary>
        /// Seed data for the Pump Sub Type enumerations
        /// </summary>
        /// <param name="context"></param>
        //private void AddPumpSubTypes(OcpContext context)
        //{
        //    context.PumpSubTypes.AddOrUpdate(
        //        r => r.Name,
        //        new PumpSubType { Name = "SINGLE" },
        //        new PumpSubType { Name = "DUAL" },
        //        new PumpSubType { Name = "SPEED" },
        //        new PumpSubType { Name = "FLOW" });
        //    context.SaveChanges();
        //}

        /// <summary>
        /// Seed data for the Remote Sub Types enumerations
        /// </summary>
        /// <param name="context"></param>
        //private void AddRemoteSubTypes(OcpContext context)
        //{
        //    context.RemoteSubTypes.AddOrUpdate(
        //        r => r.Name,
        //        new RemoteSubType { Name = "IS4" },
        //        new RemoteSubType { Name = "IS10" },
        //        new RemoteSubType { Name = "SPACMD" },
        //        new RemoteSubType { Name = "QT" });
        //    context.SaveChanges();
        //}

        /// <summary>
        /// Seed data for the Schedule Modes enumeration
        /// </summary>
        /// <param name="context"></param>
        //private void AddScheduleModes(OcpContext context)
        //{
        //    context.ScheduleModes.AddOrUpdate(
        //        r => r.Name,
        //        new ScheduleMode { Name = "ABSTIM" },
        //        new ScheduleMode { Name = "SRIS" },
        //        new ScheduleMode { Name = "TURNS" },
        //        new ScheduleMode { Name = "TEMP" },
        //        new ScheduleMode { Name = "SSET" });
        //    context.SaveChanges();
        //}

        /// <summary>
        /// Seed data for the Temperature Modes enumerations
        /// </summary>
        /// <param name="context"></param>
        //private void AddTemperatureModes(OcpContext context)
        //{
        //    context.TemperatureModes.AddOrUpdate(
        //        r => r.Name,
        //        new TemperatureMode { Name = "INCR" },
        //        new TemperatureMode { Name = "DECR" },
        //        new TemperatureMode { Name = "HOLD" });
        //    context.SaveChanges();
        //}

        /// <summary>
        /// Seed data for the user access level enumerations
        /// </summary>
        /// <param name="context"></param>
        //private void AddUserAccessLevels(OcpContext context)
        //{
        //    context.UserAccessLevels.AddOrUpdate(
        //        r => r.Name,
        //        new UserAccessLevel { Name = "BASIC" },
        //        new UserAccessLevel { Name = "ADV" },
        //        new UserAccessLevel { Name = "SUPER" });
        //    context.SaveChanges();
        //}

        /// <summary>
        /// Seed data for the valve sub type enumerations
        /// </summary>
        /// <param name="context"></param>
        //private void AddValveSubTypes(OcpContext context)
        //{
        //    context.ValveSubTypes.AddOrUpdate(
        //        r => r.Name,
        //        new ValveSubType { Name = "LEGACY" },
        //        new ValveSubType { Name = "INTELLI" });
        //    context.SaveChanges();
        //}

        //private void AddObjectTypes(OcpContext context)
        //{
        //    context.ObjectTypes.AddOrUpdate(
        //        r => r.Name,
        //        new ObjectType { Name = "SYSTEM" },
        //        new ObjectType { Name = "SYSTIM" },
        //        new ObjectType { Name = "SENSE" },
        //        new ObjectType { Name = "FEATR" },
        //        new ObjectType { Name = "PERMIT" },
        //        new ObjectType { Name = "VACAY" },
        //        new ObjectType { Name = "SERVICE" },
        //        new ObjectType { Name = "CIRCUIT" },
        //        new ObjectType { Name = "CIRCGRP" },
        //        new ObjectType { Name = "VALVE" },
        //        new ObjectType { Name = "VLVCIRC" },
        //        new ObjectType { Name = "VALVEXP" },
        //        new ObjectType { Name = "REMOTE" },
        //        new ObjectType { Name = "REMCIRC" },
        //        new ObjectType { Name = "PUMP" },
        //        new ObjectType { Name = "PMPCIRC" },
        //        new ObjectType { Name = "SCHED" },
        //        new ObjectType { Name = "HTSRC" },
        //        new ObjectType { Name = "BODY" },
        //        new ObjectType { Name = "HEATER" },
        //        new ObjectType { Name = "HCOMBO" },
        //        new ObjectType { Name = "CHEM" },
        //        new ObjectType { Name = "PANEL" },
        //        new ObjectType { Name = "MODULE" },
        //        new ObjectType { Name = "STATUS" },
        //        new ObjectType { Name = "NETWRK" },
        //        new ObjectType { Name = "SUPORT" });
        //    context.SaveChanges();
        //}

        /// <summary>
        /// Default configuration Light objects
        /// </summary>
        /// <param name="context"></param>
        private void AddDefaultLights(OcpContext context)
        {
            context.Circuits.AddOrUpdate(
                r => r.OBJNAM,
                new Circuit { OBJNAM = "C5433", OBJTYP = "CIRCUIT", SUBTYP = "INTELLI", SNAME = "Night Spa", STATUS = "OFF", LISTORD = 1, SHOMNU = "l" },
                new Circuit { OBJNAM = "C5435", OBJTYP = "CIRCUIT", SUBTYP = "MAGIC2", SNAME = "Night Laminar", STATUS = "OFF", LISTORD = 2, SHOMNU = "l" },
                new Circuit { OBJNAM = "C5436", OBJTYP = "CIRCUIT", SUBTYP = "GENERIC", SNAME = "Water Fall", STATUS = "OFF", LISTORD = 1, SHOMNU = "f" },
                new Circuit { OBJNAM = "C5437", OBJTYP = "CIRCUIT", SUBTYP = "FRZ", SNAME = "Freeze Mode", STATUS = "ON", LISTORD = 1, SHOMNU = "s" });
            context.SaveChanges();
        }

        /// <summary>
        /// Default Pool Circuit for configuration
        /// </summary>
        /// <param name="context"></param>
        private void AddDefaultPoolCircuit(OcpContext context)
        {
            context.Circuits.AddOrUpdate(
                r => r.OBJNAM,
                new Circuit { OBJNAM = "C1355", OBJTYP = "CIRCUIT", SUBTYP = "BODY", SNAME = "Pool Circuit", STATUS = "ON", LISTORD = 1, SHOMNU = "s" },
                new Circuit { OBJNAM = "C1356", OBJTYP = "CIRCUIT", SUBTYP = "BODY", SNAME = "Spa Circuit", STATUS = "OFF", LISTORD = 1, SHOMNU = "s" });
            context.SaveChanges();
        }

        /// Default Pool Circuit for configuration
        /// </summary>
        /// <param name="context"></param>
        private void AddDefaultSpaCircuit(OcpContext context)
        {
            context.Circuits.AddOrUpdate(
                r => r.OBJNAM,
                new Circuit { OBJNAM = "C1356", OBJTYP = "CIRCUIT", SUBTYP = "BODY", SNAME = "Spa Circuit", STATUS = "OFF", LISTORD = 1, SHOMNU = "s" });
            context.SaveChanges();
        }

        /// <summary>
        /// Default Heaters for configuration
        /// </summary>
        /// <param name="context"></param>
        //private void AddDefaultHeaters(OcpContext context, SystemConfig systemConfig)
        //{
        //    ObjectType type = context.ObjectTypes.Where(t => t.Name.Equals("HEATER")).FirstOrDefault();
        //    HeaterSubType combo = context.HeaterSubTypes.Where(s => s.Name.Equals("HCOMBO")).FirstOrDefault();
        //    HeaterSubType gas = context.HeaterSubTypes.Where(s => s.Name.Equals("GENERIC")).FirstOrDefault();
        //    HeaterSubType solar = context.HeaterSubTypes.Where(s => s.Name.Equals("SOLAR")).FirstOrDefault();
        //    context.Heaters.AddOrUpdate(
        //        r => r.Name,
        //        new Heater { Name = "H2416", TypeID = type.ID, HeaterSubTypeID = combo.ID, UserName = "Solar Preferred", SystemConfigID = systemConfig.ID },
        //        new Heater { Name = "H2582", TypeID = type.ID, HeaterSubTypeID = gas.ID, UserName = "Pool Heater 1", SystemConfigID = systemConfig.ID },
        //        new Heater { Name = "H6944", TypeID = type.ID, HeaterSubTypeID = solar.ID, UserName = "Pool Solar 1", SystemConfigID = systemConfig.ID });
        //    context.SaveChanges();
        //}

        /// <summary>
        /// Default Heater Groups for the configuration
        /// </summary>
        /// <param name="context"></param>
        //private void AddDefaultHeaterGroups(OcpContext context, SystemConfig systemConfig)
        //{
        //    ObjectType type = context.ObjectTypes.Where(t => t.Name.Equals("HCOMBO")).FirstOrDefault();
        //    Heater parent = context.Heaters.Where(h => h.Name.Equals("H2416")).FirstOrDefault();
        //    Heater solar = context.Heaters.Where(h => h.Name.Equals("H6944")).FirstOrDefault();
        //    Heater gas = context.Heaters.Where(h => h.Name.Equals("H2582")).FirstOrDefault();
        //    context.HeaterGroups.AddOrUpdate(
        //        r => r.Name,
        //        new HeaterGroup { Name = "H2384", TypeID = type.ID, ParentHeaterID = parent.ID, CurrentHeaterID = solar.ID, HeaterAction = 1, SystemConfigID = systemConfig.ID },
        //        new HeaterGroup { Name = "H6243", TypeID = type.ID, ParentHeaterID = parent.ID, CurrentHeaterID = gas.ID, HeaterAction = 2, SystemConfigID = systemConfig.ID });
        //    context.SaveChanges();
        //}

        /// <summary>
        /// Default Heater Affect for configuration
        /// </summary>
        /// <param name="context"></param>
        //private void AddDefaultHeaterAffects(OcpContext context, SystemConfig systemConfig)
        //{
        //    ObjectType type = context.ObjectTypes.Where(t => t.Name.Equals("HTSRC")).FirstOrDefault();
        //    Heater heater = context.Heaters.Where(h => h.Name.Equals("H2416")).FirstOrDefault();
        //    Heater heaterGas = context.Heaters.Where(h => h.Name.Equals("H2582")).FirstOrDefault();
        //    context.HeaterAffects.AddOrUpdate(
        //        r => r.Name,
        //        new HeaterAffect { Name = "H5739", TypeID = type.ID, CurrentHeaterID = heaterGas.ID, TempHigh = 80, SystemConfigID = systemConfig.ID },
        //        new HeaterAffect { Name = "H5823", TypeID = type.ID, CurrentHeaterID = heater.ID, TempHigh = 80, SystemConfigID = systemConfig.ID });
        //    context.SaveChanges();
        //}

        /// <summary>
        /// Default Body for configuration
        /// </summary>
        /// <param name="context"></param>
        private void AddDefaultBody(OcpContext context)
        {
            context.Bodies.AddOrUpdate(
                r => r.OBJNAM,
                new Body { OBJNAM = "B1000", OBJTYP = "BODY", SUBTYP = "POOL", LISTORD = 1, SNAME = "Pool", HEATER = "H1000", MIN = "72^F", MAX = "78^F", TEMP = "75^F", PRIM = "C1355", FILTER = "C1355"},
                new Body { OBJNAM = "B1001", OBJTYP = "BODY", SUBTYP = "SPA", LISTORD = 2, SNAME = "Spa", HEATER = "H1001", MIN = "100^F", MAX = "109^F", TEMP = "108^F", PRIM = "C1356", FILTER = "C1356" }); 
            context.SaveChanges();
            context.SaveChanges();
        }

        /// <summary>
        /// Default Body for configuration
        /// </summary>
        /// <param name="context"></param>
        private void AddDefaultHeater(OcpContext context)
        {
            context.Heaters.AddOrUpdate(
                r => r.OBJNAM,
                new Heater {OBJNAM = "H1000", OBJTYP = "HEATER", STATIC = "ON", LISTORD = 1, SNAME = "Body heater", SUBTYP = "GENERIC", BODY = "B1000", SHARE = "B1001", STATUS="OFF", PERMIT = "ON", TIMOUT = 20, READY = "TRUE"},
                new Heater {OBJNAM = "H1001", OBJTYP = "HEATER", STATIC = "ON", LISTORD = 1, SNAME = "Spa heater", SUBTYP = "GENERIC", BODY = "B1001", SHARE = "B1001", STATUS = "OFF", PERMIT = "ON", TIMOUT = 20, READY = "TRUE" });
            context.SaveChanges();
            context.SaveChanges();
        }

        private void AddDefaultChemistry(OcpContext context)
        {
            context.Chemistries.AddOrUpdate(
                r => r.OBJNAM,
                new Chemistry { OBJNAM = "I2345", OBJTYP = "CHEM", SNAME = "Pool Chlorinator", SUBTYP = "ICHLOR", BODY = "B1000", SHARE = "B1001", READY = "TRUE", SALT = "2450", CALC = 2450, CYACID = 1200 },
                new Chemistry { OBJNAM = "I2572", OBJTYP = "CHEM", SNAME = "Pool Chemistry", SUBTYP = "ICHEM", BODY = "B1000", SHARE = "B1001", READY = "TRUE", SALT = "2450", CALC = 2450, CYACID = 1200 });
            context.SaveChanges();
        }

        /// <summary>
        /// Seed data for the default configuration
        /// </summary>
        /// <param name="context"></param>
        private void AddDefaultConfiguration(OcpContext context)
        {
            //context.SystemConfigs.AddOrUpdate(
            //    r => r.Name,
            //    new SystemConfig { Name = "Default" });
            //context.SaveChanges();
            //SystemConfig defaultConfig = context.SystemConfigs.Where(c => c.Name.Equals("Default")).First();
            AddDefaultLights(context);
            AddDefaultPoolCircuit(context);
            AddDefaultSpaCircuit(context);
            //AddDefaultHeaters(context, defaultConfig);
            //AddDefaultHeaterGroups(context, defaultConfig);
            //AddDefaultHeaterAffects(context, defaultConfig);
            AddDefaultBody(context);
            AddDefaultHeater(context);
            AddDefaultChemistry(context);
        }
    }
}