﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Pentair.Domain.Client.Entity;
using Pentair.Domain.Client.Infrastructure;
using System.ComponentModel.DataAnnotations;


namespace Pentair.Web.Api.Controllers
{

    public class ListUsersViewModel
    {
        [Required]
        public int page { get; set; }
        public int pageSize { get; set; }
        public string sort { get; set; }
        public bool descend { get; set; }
        public string filter { get; set; }
        public string term { get; set; }
        public string sortField { get; set; }

        public ListUsersViewModel()
        {
             page = 1;
             pageSize = 20;
             filter = "all";
             sortField = "email";
             term = "";
             sort = "asc";
        }

    }


    public class ApplicationUsersController : ApiController
    {
        private ApplicationUserContext db = new ApplicationUserContext();

        // GET: api/ApplicationUsers
        public IQueryable<ApplicationUser> GetApplicationUsers(ListUsersViewModel model)
        {

            List<AssociatedUser> userList;
            int totalCount = 0;
            using (var identity = new ApplicationUserManager(
                new UserStore<ApplicationUser>(
                    new ApplicationUserContext())))
            {
                int page = 1;
                int pageSize = 20;
                var filter = "all";
                var sortField = "email";
                var searchTerm = "";
                var sort = "asc";
                if (null != parms.parameters)
                {
                    foreach (var item in parms.parameters)
                    {
                        switch (item.Key)
                        {
                            case "PAGE":
                                int.TryParse(item.Value, out page);
                                break;
                            case "PAGESIZE":
                                int.TryParse(item.Value, out pageSize);
                                break;
                            case "FILTER":
                                filter = item.Value;
                                break;
                            case "SORTFIELD":
                                sortField = item.Value;
                                break;
                            case "SORTDIRECTION":
                                sort = item.Value;
                                break;
                            case "SEARCHTERM":
                                searchTerm = item.Value;
                                break;
                        }
                    }
                }
                SortDirection sortDirection = sort.ToLower().Contains("asc")
                    ? SortDirection.Ascending
                    : SortDirection.Decending;

                var ua = new UserAssociations(_data);
                userList = ua.UserAssociationList(
                    propertyId,
                    identity.Users,
                    pageSize,
                    page - 1,
                    sortField,
                    sortDirection,
                    searchTerm,
                    filter
                    );
                totalCount = ua.TotalCount;
            }

            var users = new List<ParamObject>();
            users.AddRange(from u in userList
                           select new ParamObject()
                           {
                               objnam = u.EmailAddress,
                               parameters = new Dictionary<string, string>()
                    {
                        {"id", u.UserId.ToString()},
                        {"name", u.UserName},
                        {"email", u.EmailAddress},
                        {"level", u.SecurityName},
                        {"status", u.Status}
                    }
                           }
                );

            var response = new UserList(users, totalCount.ToString())
            {
                command = CommandString.ApiResponse
            };
            return JsonConvert.SerializeObject(response);
        }

        // GET: api/ApplicationUsers/5
        [ResponseType(typeof(ApplicationUser))]
        public IHttpActionResult GetApplicationUser(string id)
        {
            ApplicationUser applicationUser = db.ApplicationUsers.Find(id);
            if (applicationUser == null)
            {
                return NotFound();
            }

            return Ok(applicationUser);
        }

        // PUT: api/ApplicationUsers/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutApplicationUser(string id, ApplicationUser applicationUser)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != applicationUser.Id)
            {
                return BadRequest();
            }

            db.Entry(applicationUser).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ApplicationUserExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ApplicationUsers
        [ResponseType(typeof(ApplicationUser))]
        public IHttpActionResult PostApplicationUser(ApplicationUser applicationUser)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.ApplicationUsers.Add(applicationUser);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (ApplicationUserExists(applicationUser.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = applicationUser.Id }, applicationUser);
        }

        // DELETE: api/ApplicationUsers/5
        [ResponseType(typeof(ApplicationUser))]
        public IHttpActionResult DeleteApplicationUser(string id)
        {
            ApplicationUser applicationUser = db.ApplicationUsers.Find(id);
            if (applicationUser == null)
            {
                return NotFound();
            }

            db.ApplicationUsers.Remove(applicationUser);
            db.SaveChanges();

            return Ok(applicationUser);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ApplicationUserExists(string id)
        {
            return db.ApplicationUsers.Count(e => e.Id == id) > 0;
        }
    }
}