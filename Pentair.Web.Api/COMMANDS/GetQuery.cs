﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Pentair.Web.Api
{
    public class GetQuery : CommandBase
    {
        [JsonProperty("queryName")]
        public string QueryName { get; private set; }

        [JsonProperty("arguments")]
        public object Arguments { get; private set; }

        public GetQuery(
            string messageID,
            string queryName,
            Dictionary<string, string> args
            )
            : base(CommandString.GetQuery, messageID)
        {
            this.QueryName = queryName;
            this.Arguments = args;
        }
    
    } // class GetQuery
}
