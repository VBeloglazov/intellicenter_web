﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Pentair.Web.Api
{
    public class ParamListResponse : CommandWithCountResponse
    {
        [JsonProperty("objectList", Required = Required.Default)]
        public List<ParamObject> ObjectList { get; private set; }

        public ParamListResponse(
            string command,
            string messageID,
            string response,
            string totalCount,
            List<ParamObject> objectList
            )
            : base(command, messageID, response, totalCount)
        {
            this.ObjectList = objectList;

        } // ParamListResponse()

        /// <summary>
        /// Always returns an object, valid or not. If invalid, check the DeserializationError property value.
        /// </summary>
        public static new ParamListResponse Deserialize(
            string serializedMessage)
        {
            ParamListResponse command = null;

            try
            {
                command = JsonConvert.DeserializeObject<ParamListResponse>(serializedMessage);
                command.IsValid = true;
            }
            catch (Exception e)
            {
                // Deserialization as ParamListResponse failed

                StringBuilder sbError = new StringBuilder();

                sbError.Append("Cannot deserialize as ParamListResponse. " + e.Message);

                // Try to get at least the base message data
                CommandWithCountResponse baseCommand = CommandWithCountResponse.Deserialize(serializedMessage);

                if (!baseCommand.IsValid)
                {
                    sbError.Append("\n" + baseCommand.DeserializationError);
                }

                command = new ParamListResponse(
                    command: baseCommand.Command,
                    messageID: baseCommand.MessageID,
                    response: ResponseString.BadRequest,
                    totalCount: String.Empty,
                    objectList: null)
                {
                    IsValid = false,
                    DeserializationError = sbError.ToString()
                };
            }

            return command;

        } // Deserialize()

    } // class ParamListResponse
}
