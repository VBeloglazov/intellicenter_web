﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Pentair.Web.Api
{
    public enum EventTypeCodes
    {
        InstallationRegistrationEvent,
        InstallationConnectEvent,
        InstallationDisconnectEvent
    }
    
    public class EventType
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int EventId { get; set; }
        
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
