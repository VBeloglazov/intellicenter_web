﻿/*
--------------------------------------------------------------------------------

    ILogger_WebApi.cs

    Copyright © Pentair Water Pool and Spa, Inc. 2017

--------------------------------------------------------------------------------
*/

using System;

namespace SERVICE_SHARED
{
    public interface ILogger_WebApi
    {
        void LogError(
            string source,
            string eventBriefDescription,
            int installationId,
            Guid userId,
            Guid connectionId,
            string eventFullDescription
        );

        void LogWarning(
            string source,
            string eventBriefDescription,
            int installationId,
            Guid userId,
            Guid connectionId,
            string eventFullDescription,
            bool saveEventLog
        );

        void LogInformation(
            string source,
            string eventBriefDescription,
            int installationId,
            Guid userId,
            Guid connectionId,
            string eventFullDescription,
            bool saveEventLog
        );

        void LogErrorAsync(
            string source,
            string eventBriefDescription,
            int installationId,
            Guid userId,
            Guid connectionId,
            string eventFullDescription
        );

        void LogWarningAsync(
            string source,
            string eventBriefDescription,
            int installationId,
            Guid userId,
            Guid connectionId,
            string eventFullDescription,
            bool saveEventLog
        );

        void LogInformationAsync(
            string source,
            string eventBriefDescription,
            int installationId,
            Guid userId,
            Guid connectionId,
            string eventFullDescription,
            bool saveEventLog
        );

    } // interface ILogger_WebApi
}