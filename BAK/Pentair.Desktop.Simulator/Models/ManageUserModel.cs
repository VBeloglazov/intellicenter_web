﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pentair.Desktop.Simulator.Models
{
    public class ManageUserModel
    {
        public string Email { get; set; }
        public string PermissionGroup { get; set; }
    }
}
