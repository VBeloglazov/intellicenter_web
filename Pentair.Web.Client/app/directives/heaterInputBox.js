var PWC = PWC || {};
PWC.directive('heaterInputBox',['$filter','PropertyModel', function ($filter,PropertyModel) {
    return {
        restrict: 'EA',
        replace: true,
        scope: {
            heaterModel: '=',
            bodyObject: '=',
            onTextInput: '&',
            minValue: '@',
            maxValue: '@',
            maxLength: '@',
            disabled: '='
        },
        template: '<div><span class="heater-temp">{{(heaterModel | translateTemperature).TEMPERATURE}}</span><input ng-numbers-only ng-blur="onExit()" ng-enter="onExit()" ng-show="showInput" ng-model="temp" maxlength="{{maxLength}}" max-value="{{maxValue}}" min-value="{{minValue}}" type="text"></div>',
        link: function (scope, element, attrs) {
            if (!attrs.disabled)
                attrs.disabled = false;
            var oldVal = 0;
            var maxValue = Number(attrs.maxValue || 0);
            var minValue = Number(attrs.minValue || 0);
            var initialUnitOfMeasurment = "";
            scope.maxLength = Number(attrs.maxLength || 0);
            scope.showInput = false;

            //if input box is hidden show box and set focus
            element.on('click', function () {
                if (!scope.disabled && angular.isDefined(scope.showInput) && scope.showInput == false) {
                    initialUnitOfMeasurment = PropertyModel.Objects["_5451"].MODE;
                    oldVal = Number($filter('translateTemperature')(scope.heaterModel).NUMBER) || Number($filter('translateTemperature')(scope.heaterModel.replace(/\D/g, '')).NUMBER);
                    scope.temp = oldVal;

                    scope.showInput = true;
                    scope.$apply(scope.showInput);
                    element[0].lastChild.focus();
                }
            });

            scope.onExit = function () {
                scope.showInput = false;
                var temp = initialUnitOfMeasurment == "ENGLISH" ? (scope.temp - 32) * (5 / 9) : scope.temp;
    
                if (temp< minValue && minValue > 0) {
                    temp = minValue;
                }
                if (temp > maxValue && maxValue > 0) {
                    temp = maxValue;
                }
                if (oldVal != temp) {
                    scope.onTextInput({ obj: scope.bodyObject.OBJNAM, prop: "LOTMP", value: Math.round(temp) });
                    oldVal = temp;
                }
            }

            //update UI value if Model changes
            scope.$watch('heaterModel', function () {
                if (scope.heaterModel) {
                    oldVal = Number(scope.heaterModel) || Number(scope.heaterModel.replace(/\D/g, ''));
                    scope.temp = oldVal;
                }
            });
        }
    };
}]);
