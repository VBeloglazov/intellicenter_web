-- =============================================================
--
-- Author:		Vladimir Beloglazov
-- Create date: 01/26/2018
--
-- Creates:
--
--		Table Type:			None
--
--		Stored Procedure:	PGetInstallationById_GIT
--
-- 2018-04-09 VB: Renamed to PGetInstallationById_GIT to avoid duplication with SP PGetInstallationById_TFS
--                stored in the TFS and used by Web Gateway and RM Services.
--
-- =============================================================

USE PathfinderIntelliCenter
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (
	SELECT * FROM sys.objects
	WHERE object_id = OBJECT_ID(N'dbo.PGetInstallationById_GIT')
	AND type in (N'P', N'PC')
)
DROP PROCEDURE dbo.PGetInstallationById_GIT
GO

-- -------------------------------------------------------------
--
-- Returns [Installations] record field values.
--
-- -------------------------------------------------------------
CREATE PROCEDURE dbo.PGetInstallationById_GIT
	 @installationId AS int
AS
BEGIN

	SET NOCOUNT OFF

	-- Result set
	SELECT *
	FROM dbo.Installations
	WHERE InstallationId = @installationId

END
GO
