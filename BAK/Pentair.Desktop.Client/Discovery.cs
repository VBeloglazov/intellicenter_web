﻿using System;
using System.Diagnostics;
using System.Net;
using System.Web.Services.Description;
using Bonjour;
using System.Collections.Generic;
using System.Net.Sockets;

namespace Pentair.Desktop.Client
{
    public enum ConnectionStatus
    {
        Disabled,
        Enabled,
        Authenticating,
        Connected,
        Error
    }

    public class Discovery : IDisposable
    {
        private Bonjour.DNSSDEventManager _eventManager = null;
        private Bonjour.DNSSDService _service = null;
        private Bonjour.DNSSDService _browser = null;
        private Bonjour.DNSSDService _resolver;
        private const int BufferSize = 1024;
        private readonly byte[] _buffer = new byte[BufferSize];

        private readonly List<PeerData> _services;
        private readonly Queue<PeerData> _resolving;

        public List<PeerData> Services
        {
            get { return _services; }
        }

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public Discovery()
        {
            _services = new List<PeerData>();
            _resolving = new Queue<PeerData>();
        }

        public void Dispose()
        {
            if (null != _resolver)
            {
                _resolver.Stop();
            }
            if (null != _browser)
            {
                _browser.Stop();
            }
            if (null != _resolver)
            {
                _resolver.Stop();
            }
            if (null != _service)
            {
                _service.Stop();
            }
            _eventManager.ServiceRegistered -= ServiceRegistered;
            _eventManager.ServiceFound -= ServiceFound;
            _eventManager.ServiceLost -= ServiceLost;
            _eventManager.ServiceResolved -= ServiceResolved;
            _eventManager.QueryRecordAnswered -= QueryAnswered;
            _eventManager.OperationFailed -= OperationFailed;
        }

        #region Public Methods

        public void StopService()
        {
            Dispose();
        }

        public void StartService()
        {
            try
            {
                _service = new DNSSDService();
                _eventManager = new DNSSDEventManager();
                _eventManager.ServiceRegistered += ServiceRegistered;
                _eventManager.ServiceFound += ServiceFound;
                _eventManager.ServiceLost += ServiceLost;
                _eventManager.ServiceResolved += ServiceResolved;
                _eventManager.QueryRecordAnswered += QueryAnswered;
                _eventManager.OperationFailed += OperationFailed;
                _browser = _service.Browse(0, 0, "_http._tcp", null, _eventManager);
            }
            catch
            {
                throw (new Exception("Discovery service not available"));
            }
        }
        public bool BrowserValid()
        {
            return null != _browser;
        }

        #endregion

        #endregion

        #region Public call back Methods
        /// <summary>
        /// Register callback
        /// called by DNSServices core in response to a registration request
        /// </summary>
        /// <param name="service"></param>
        /// <param name="flags"></param>
        /// <param name="name"></param>
        /// <param name="regType"></param>
        /// <param name="domain"></param>
        public void ServiceRegistered(
            DNSSDService service,
            DNSSDFlags flags,
            String name,
            String regType,
            String domain
            )
        {
            _browser = _service.Browse(0, 0, "_http._tcp", null, _eventManager);
        }

        /// <summary>
        /// Called by DNSServices core in response to a Service Found event
        /// </summary>
        /// <param name="sref"></param>
        /// <param name="flags"></param>
        /// <param name="ifIndex"></param>
        /// <param name="serviceName"></param>
        /// <param name="regType"></param>
        /// <param name="domain"></param>
        public void ServiceFound(
            DNSSDService sref,
            DNSSDFlags flags,
            uint ifIndex,
            String serviceName,
            String regType,
            String domain
            )
        {
            var peer = new PeerData
            {
                InterfaceIndex = ifIndex,
                Name = serviceName,
                Type = regType,
                Domain = domain,
                Address = null,
                Resolved = false
            };

            // Get the details for this service
            if (serviceName.StartsWith("Pentair:"))
            {
                lock (MainWindow.LockData)
                {
                    _resolving.Enqueue(peer);
                    if (null == _resolver)
                    {
                        _resolver = _service.Resolve(0, peer.InterfaceIndex, peer.Name, peer.Type, peer.Domain,
                            _eventManager);
                    }
                }
            }
        }

        /// <summary>
        /// Called by DNSServices core in response to a service lost event
        /// </summary>
        /// <param name="sref"></param>
        /// <param name="flags"></param>
        /// <param name="ifIndex"></param>
        /// <param name="serviceName"></param>
        /// <param name="regType"></param>
        /// <param name="domain"></param>
        public void ServiceLost(
            DNSSDService sref,
            DNSSDFlags flags,
            uint ifIndex,
            String serviceName,
            String regType,
            String domain
            )
        {
            var peer = new PeerData
            {
                InterfaceIndex = ifIndex,
                Name = serviceName,
                Type = regType,
                Domain = domain,
                Address = null
            };

            // Remove the old copy.
            if (_services.Contains(peer))
            {
                lock (MainWindow.LockData)
                {
                    _services.Remove(peer);
                }
            }
        }

        /// <summary>
        /// Called by DNSServices core in response to a Service Resolved event
        /// </summary>
        /// <param name="sref"></param>
        /// <param name="flags"></param>
        /// <param name="ifIndex"></param>
        /// <param name="fullName"></param>
        /// <param name="hostName"></param>
        /// <param name="port"></param>
        /// <param name="txtRecord"></param>
        public void ServiceResolved(
            DNSSDService sref,
            DNSSDFlags flags,
            uint ifIndex,
            String fullName,
            String hostName,
            ushort port,
            TXTRecord txtRecord
            )
        {
            lock (MainWindow.LockData)
            {
                if (null != _resolver)
                {
                    _resolver.Stop();
                    _resolver = null;
                }

                // This is an error condition
                if (0 < _resolving.Count)
                {
                    var next = _resolving.Peek();
                    next.Port = port;
                    next.HostName = hostName;
                    next.FullName = fullName;
                    _resolver = _service.QueryRecord(0, next.InterfaceIndex, next.HostName, DNSSDRRType.kDNSSDType_A,
                        DNSSDRRClass.kDNSSDClass_IN, _eventManager);
                }
            }
        }

        /// <summary>
        /// Called by DNSServices core in response to a Query Answered event
        /// </summary>
        /// <param name="service"></param>
        /// <param name="flags"></param>
        /// <param name="ifIndex"></param>
        /// <param name="fullName"></param>
        /// <param name="rrtype"></param>
        /// <param name="rrclass"></param>
        /// <param name="rdata"></param>
        /// <param name="ttl"></param>
        public void QueryAnswered(
            DNSSDService service,
            DNSSDFlags flags,
            uint ifIndex,
            String fullName,
            DNSSDRRType rrtype,
            DNSSDRRClass rrclass,
            Object rdata,
            uint ttl
            )
        {
            lock (MainWindow.LockData)
            {
                _resolver.Stop();
                _resolver = null;

                // This is an error condition
                if (0 < _resolving.Count)
                {
                    var next = _resolving.Dequeue();
                    uint bits = BitConverter.ToUInt32((Byte[]) rdata, 0);
                    next.Address = new IPAddress(bits);
                    _services.Add(next);
                    if (0 < _resolving.Count)
                    {
                        next = _resolving.Peek();
                        _resolver = _service.Resolve(0, next.InterfaceIndex, next.Name, next.Type,
                            next.Domain,
                            _eventManager);
                    }
                }
            }
        }

        public
            void OperationFailed(DNSSDService service, DNSSDError error)
        {
        }
    }

    #endregion

        #region Helper Classes

    public class DiscoveryEventArgs
    {
        public ConnectionStatus Status { get; private set; }
        public string Message { get; private set; }

        public DiscoveryEventArgs(ConnectionStatus status, string message)
        {
            Status = status;
            Message = message;
        }
    }


    /// <summary>
    /// Class to manage data about a peer on the network
    /// </summary>
    public class PeerData
    {
        public uint InterfaceIndex;
        public String Name;
        public String Type;
        public String Domain;
        public IPAddress Address;
        public int Port;
        public String HostName;
        public String FullName;
        public bool Resolved;

        public override String
            ToString()
        {
            return Name;
        }

        /// <summary>
        /// Override for the Equals operator
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public override bool Equals(object other)
        {
            bool result = false;

            if (other != null)
            {
                if ((object) this == other)
                {
                    result = true;
                }
                else if (other is PeerData)
                {
                    PeerData otherPeerData = (PeerData) other;

                    result = (this.Name == otherPeerData.Name);
                }
            }

            return result;
        }

        /// <summary>
        /// Override for the int operator
        /// </summary>
        /// <returns></returns>
        public override int
            GetHashCode()
        {
            return Name.GetHashCode();
        }
    }

    #endregion
}

