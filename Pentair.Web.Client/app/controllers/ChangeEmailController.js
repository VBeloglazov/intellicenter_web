'use strict';
var PWC = PWC || {};

PWC.controller('ChangeEmailController', [
    '$scope', '$location', 'AppService', 'AuthService', function ($scope, $location, appService, authService) {

        $scope.Authentication = authService.authentication;
        $scope.changeEmailData = {
            oldEmail: authService.authentication.userName,
            newEmail: "",
            confirmEmail: "",
            password:"",
            error: false,
            success: false
        };

        $scope.$parent.Layout = "layout-base";
        $scope.GoToDashboard = function () {
            $location.path("/property");
        };

        $scope.validateOldEmail = false;

        $scope.ChangeEmail = function () {
            var changeEmail = {
                OldEmail: authService.authentication.userName,
                NewEmail: $scope.changeEmailData.newEmail,
                ConfirmEmail: $scope.changeEmailData.confirmEmail,
                Password: $scope.changeEmailData.password
            };
            appService.PostData("Api/Account/ChangeUserName", changeEmail).then(function () {
                authService.authentication['newUserName'] =  $scope.changeEmailData.newEmail;
                resetChangeEmailData();
                $scope.changeEmailData.error = false;
                $scope.changeEmailData.success = true;
                $scope.setUsernameModal = false;
                $scope.confirmUsernameModal = true;
            }, function (res) {
                resetChangeEmailData();
                $scope.changeEmailData.error = true;
                $scope.changeEmailData.success = false;
                $scope.errorMessage = 'There was an error, please try again';
                if(res.Message && res.Message != ''){
                    $scope.errorMessage = res.Message;
                }else if(res && res.error && res.error.Message && res.error.Message != '') {
                    $scope.errorMessage = res.error.Message;
                }
            });
        };


        $scope.validateEmailForm = function () {
            if ($scope.ChangeEmailForm)
                return $scope.ChangeEmailForm  && !$scope.ChangeEmailForm.newEmail.$error.email && $scope.changeEmailData.newEmail &&
                  $scope.verifyConfirmationEmail() && $scope.changeEmailData.password;
            return false;
        };


        $scope.verifyEmail = function () {
            var data = {
                Email: authService.authentication.userName
            };
            appService.PostData("Api/Account/VerifyEmail", data).then(function () {
                $scope.validateOldEmail = true;
            }, function(err) {
                $scope.validateOldEmail = false;
            });
        };


        $scope.verifyConfirmationEmail = function () {
            return !$scope.ChangeEmailForm.confirmEmail.$invalid &&
              $scope.changeEmailData.newEmail == $scope.changeEmailData.confirmEmail;
        };

        $scope.clearOldEmail = function () {
            $scope.changeEmailData.oldEmail = "";
        };


        function resetChangeEmailData() {
            $scope.changeEmailData = {
                oldEmail: authService.authentication.userName,
                newEmail: "",
                confirmEmail: "",
                password:"",
                error: false,
                success: false
            };

        }
    }
]);