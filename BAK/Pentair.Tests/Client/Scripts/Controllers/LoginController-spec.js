﻿describe('LoginController', function () {
    var LoginController;
    var AuthService;
    var AppService;
    var PropertyService;
    var $httpBackend;
    var $scope;
    var $location;
    var $controller;
    var loginData;
    var err;
    var HTTP_SERVICE_URL;
    beforeEach(angular.mock.module('PentairWebClient'));

    beforeEach(inject(function (_$controller_, _AuthService_, _AppService_, _PropertyService_, _$httpBackend_,
                                _$rootScope_, _$location_, _SERVICE_HOST_NAME_) {
        AuthService = _AuthService_;
        AppService = _AppService_;
        PropertyService = _PropertyService_;
        $httpBackend = _$httpBackend_;
        $scope = _$rootScope_.$new();
        $location = _$location_;
        $controller = _$controller_;
        HTTP_SERVICE_URL = 'https://' + _SERVICE_HOST_NAME_;
        var routeParams = {
            current: {
                locals: {
                    installations: SpecHelper.mocks.Installations
                }
            }
        };
        spyOn($location, 'path');
        $scope.PasswordAssistanceFrom = { $setPristine: jasmine.createSpy('$setPristine') }

        LoginController = $controller("LoginController", { $scope: $scope, $route: routeParams });
    }));

    it('should define the following default values', function () {
        expect($scope.AuthService).toBe(AuthService);
        expect($scope.validateOldPassword).toBe(false);
        expect($scope.changePasswordData.error).toBe(false);

        expect($scope.loginData).toEqual({
            userName: "",
            password: "",
            rememberMe: true,
            invalid: false
        });
        expect($scope.changePasswordData).toEqual({
            user: "",
            code: "",
            oldPassword: "",
            newPassword: "",
            confirmPassword: "",
            error: false,
            requestSubmitted: false
        });
        expect($scope.forgotPasswordData).toEqual({
            email: ""
        });
    });

    describe('change Password action', function () {
        beforeEach(function () {
            var routeParams = {
                current: {
                    locals: {
                        resetModel: {
                            User: 'UserName',
                            Code: 'TestCode'
                        }
                    }
                }
            }
        LoginController = $controller("LoginController", { $scope: $scope, $route: routeParams });

        });
        it('should set the username and changepassword code if provided', function () {
            expect($scope.changePasswordData.user).toBe('UserName');
            expect($scope.changePasswordData.code).toBe('TestCode');
        });
    });
    describe('Scope.LogIn success and redirect', function () {
        beforeEach(function () {
            spyOn(AuthService, 'login').and.returnValue({
                then: function (success, error) {
                    success();
                }
            });
            loginData = {
                userName: "User",
                password: "Password",
                rememberMe: true
            };
            $scope.loginData = loginData;

        });

        it('should call AuthService.Login', function () {
            $scope.login();
            expect(AuthService.login).toHaveBeenCalledWith(loginData);
            expect($location.path).toHaveBeenCalledWith("/installations");
        });

    });

    describe('Scope.LogIn error', function () {
        beforeEach(function () {
            err = { error_description: "ERROR" };
            spyOn(AuthService, 'login').and.returnValue({
                then: function (success, error) {
                    error(err);
                }
            });
            loginData = {
                userName: "User",
                password: "Password",
                rememberMe: true
            };
            $scope.loginData = loginData;

        });

        it('should call AuthService.Login and return error', function () {
            $scope.login();
            expect(AuthService.login).toHaveBeenCalledWith(loginData);
            expect($location.path).not.toHaveBeenCalled();
            expect($scope.message).toBe("ERROR");
            expect($scope.loginData.password).toBe("");
        });
    });

    describe('autopopulate last user name', function () {

        it('should set logindata.username to LastName', function () {
            AuthService.authentication.lastUserName = 'LastName';
            LoginController = $controller("LoginController", { $scope: $scope });

            expect($scope.loginData.userName).toBe('LastName');
        });
    });

    describe('validate a passwords minimum requirements', function () {
        it('should require six characters with no capital', function () {
            $scope.changePasswordData.newPassword = 'password';
            expect($scope.validateNewPassword()).toBe(false);
        });
        it('should require six characters with one capital', function () {
            $scope.changePasswordData.newPassword = 'Password';
            expect($scope.validateNewPassword()).toBe(true);
        });
        it('should require six characters with one capital and numbers', function () {
            $scope.changePasswordData.newPassword = 'Password98';
            expect($scope.validateNewPassword()).toBe(true);
        });
        it('should require six characters with no password', function () {
            $scope.changePasswordData.newPassword = '';
            expect($scope.validateNewPassword()).toBe(false);
        });
    });
    describe('verify confirmation password', function () {
        beforeEach(function () {
            $scope.changePasswordData = {
                oldPassword: "",
                newPassword: "abc",
                confirmPassword: "123"
            };
            $scope.ChangePasswordForm = {
                newPassword: { $invalid: true },
                confirmPassword: { $invalid: true }
            }
        });
        it('should be invalid new password', function () {
            expect($scope.verifyConfirmationPassword()).toBe(false);
        });
        it('should be invalid confirm password', function () {
            $scope.changePasswordData.newPassword = 'Password';
            expect($scope.verifyConfirmationPassword()).toBe(false);
        });
        it('should be invalid new password', function () {
            $scope.changePasswordData.confirmPassword = 'Password';
            expect($scope.verifyConfirmationPassword()).toBe(false);
        });
        it('should be valid confirm password', function () {
            spyOn($scope, 'validateNewPassword').and.returnValue(true);
            $scope.ChangePasswordForm.confirmPassword.$invalid = false;
            $scope.changePasswordData.confirmPassword = 'Password';
            $scope.changePasswordData.newPassword = 'Password';
            expect($scope.verifyConfirmationPassword()).toBe(true);
        });
        it('should be invalid confirm password', function () {
            spyOn($scope, 'validateNewPassword').and.returnValue(true);
            $scope.ChangePasswordForm.confirmPassword.$invalid = false;
            $scope.changePasswordData.confirmPassword = 'Password';
            $scope.changePasswordData.newPassword = 'Passwird';
            expect($scope.verifyConfirmationPassword()).toBe(false);
        });
    });
    describe('verify clear old password', function () {
        it('should be clear and validateOldPassword false', function () {
            $scope.changePasswordData.oldPassword = 'password';
            $scope.validateOldPassword = true;
            $scope.clearOldPassword();
            expect($scope.changePasswordData.oldPassword).toBe("");
            expect($scope.validateOldPassword).toBe(false);
        });
    });
    describe('verifyPassword API call', function () {
        beforeEach(function () {
            spyOn(AppService, 'PostData').and.callThrough();
        });
        it('should fail when the password is invalid', function () {
            $httpBackend.expectPOST(HTTP_SERVICE_URL + 'Api/Account/ValidatePassword').respond(403);
            $scope.validateOldPassword = false;
            $scope.verifyPassword();
            $httpBackend.flush();
            expect($scope.validateOldPassword).toBe(false);
        });
        it('should fail when the password is invalid', function () {
            $httpBackend.expectPOST(HTTP_SERVICE_URL + 'Api/Account/ValidatePassword').respond(200);
            $scope.validateOldPassword = false;
            $scope.verifyPassword();
            $httpBackend.flush();
            expect($scope.validateOldPassword).toBe(true);
        });
    });
    describe('verify changePassword form', function () {
        beforeEach(function () {
            $scope.changePasswordFlag = true;
        });
        it('should be invalid new password', function () {
            $scope.validateOldPassword = true;
            spyOn($scope, 'verifyConfirmationPassword').and.returnValue(true);
            spyOn($scope, 'validateNewPassword').and.returnValue(false);
            expect($scope.validateForm()).toBe(false);
        });
        it('should be invalid old password', function () {
            $scope.validateOldPassword = false;
            spyOn($scope, 'verifyConfirmationPassword').and.returnValue(true);
            spyOn($scope, 'validateNewPassword').and.returnValue(true);
            expect($scope.validateForm()).toBe(false);
        });
        it('should be invalid confirmation password', function () {
            $scope.validateOldPassword = true;
            spyOn($scope, 'verifyConfirmationPassword').and.returnValue(false);
            spyOn($scope, 'validateNewPassword').and.returnValue(true);
            expect($scope.validateForm()).toBe(false);
        });
        it('should be a happy form', function () {
            $scope.validateOldPassword = true;
            spyOn($scope, 'verifyConfirmationPassword').and.returnValue(true);
            spyOn($scope, 'validateNewPassword').and.returnValue(true);
            expect($scope.validateForm()).toBe(true);
        });
        describe('forgot Password action', function () {
            it('should not require the old password', function () {
                $scope.changePasswordFlag = false;
                $scope.validateOldPassword = false;
                spyOn($scope, 'verifyConfirmationPassword').and.returnValue(true);
                spyOn($scope, 'validateNewPassword').and.returnValue(true);
                expect($scope.validateForm()).toBe(true);
            });
        });
    });
    describe('changePassword API call', function () {
        beforeEach(function () {
            spyOn(AppService, 'PostData').and.callThrough();
            spyOn(PropertyService, 'Reset');
            spyOn(AuthService, 'logOut');

            $scope.changePasswordData = {
                oldPassword: "password",
                newPassword: "newPassword",
                confirmPassword: "newPassword"
            }
        });

        describe('changePassword Action', function () {
            beforeEach(function () {
                $scope.changePasswordFlag = true;
            });

            it('should pass when the password is changed', function () {

                $httpBackend.expectPOST(HTTP_SERVICE_URL + 'Api/Account/ChangePassword').respond(200);
                expect(AuthService.authentication.passwordChanged).toBe(false);
                expect(AuthService.authentication.lastUserName).toBe("");

                AuthService.passwordChanged = false;
                AuthService.authentication.userName = "userName";

                $scope.ChangePassword();
                $httpBackend.flush();

                expect(AuthService.authentication.passwordChanged).toBe(true);
                expect(PropertyService.Reset).toHaveBeenCalled();
                expect(AuthService.logOut).toHaveBeenCalled();
                expect(AuthService.authentication.lastUserName).toBe("userName");
            });

            it('should set changePasswordError when api call fails', function () {
                $httpBackend.expectPOST(HTTP_SERVICE_URL + 'Api/Account/ChangePassword').respond(500);

                $scope.ChangePassword();
                $httpBackend.flush();

                expect($scope.changePasswordData.error).toBe(true);
            });

            describe('forgot Password action', function () {
                beforeEach(function () {
                    $scope.changePasswordFlag = false;
                    $scope.changePasswordData.oldPassword = '';
                    $scope.changePasswordData.user = 'userName';
                });

                it('should pass when the password is changed', function () {

                    $httpBackend.expectPOST(HTTP_SERVICE_URL + 'Api/Account/ResetPassword').respond(200);
                    expect(AuthService.authentication.passwordChanged).toBe(false);
                    expect(AuthService.authentication.lastUserName).toBe("");

                    AuthService.passwordChanged = false;

                    $scope.ChangePassword();
                    $httpBackend.flush();

                    expect(AuthService.authentication.passwordChanged).toBe(true);
                    expect(PropertyService.Reset).toHaveBeenCalled();
                    expect(AuthService.logOut).toHaveBeenCalled();
                    expect(AuthService.authentication.lastUserName).toBe("userName");
                });

                it('should set changePasswordError when api call fails', function () {
                    $httpBackend.expectPOST(HTTP_SERVICE_URL + 'Api/Account/ResetPassword').respond(500);

                    $scope.ChangePassword();
                    $httpBackend.flush();

                    expect($scope.changePasswordData.error).toBe(true);
                });
            });
        });
    });

    describe('forgotPassword  button', function () {
        it('should go to the passwordAssistance page and set the form to pristine', function () {
            $scope.forgotPassword();
            expect($location.path).toHaveBeenCalledWith('/passwordAssistance');
        });
    });

    describe('password Assistance', function () {
        beforeEach(function () {
            spyOn(AppService, 'PostData').and.callThrough();
            $scope.forgotPasswordData = {
                email: 'unit@test.com'
            }

        });
        describe('Sucess', function () {
            it('should send new password and set success flags', function () {
                $httpBackend.expectPOST(HTTP_SERVICE_URL + 'Api/Account/ForgotPassword').respond(200);
                $scope.passwordAssistance();
                $httpBackend.flush();

                expect($scope.changePasswordData.requestSubmitted).toBe(true);
                expect($scope.changePasswordData.error).toBe(false);
                expect(AppService.PostData).toHaveBeenCalledWith('Api/Account/ForgotPassword', {
                    email: 'unit@test.com'
                });

            });
        });
        describe('Fail', function () {
            it('should send new password and set success flags', function () {
                $httpBackend.expectPOST(HTTP_SERVICE_URL + 'Api/Account/ForgotPassword').respond(500);
                $scope.passwordAssistance();
                expect(AppService.PostData).toHaveBeenCalledWith('Api/Account/ForgotPassword', {
                    email: 'unit@test.com'
                });

                $httpBackend.flush();

                expect($scope.changePasswordData.requestSubmitted).toBe(false);
                expect($scope.changePasswordData.error).toBe(true);
                expect($scope.PasswordAssistanceFrom.$setPristine).toHaveBeenCalled();
            });
        });
    });
});