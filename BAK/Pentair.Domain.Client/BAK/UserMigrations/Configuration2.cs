using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Diagnostics.CodeAnalysis;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Pentair.Domain.Client.Entity;
using Pentair.Domain.Client.Infrastructure;

namespace Pentair.Domain.Client.UserMigrations
{
    [ExcludeFromCodeCoverage]
    internal sealed class Configuration2 : DbMigrationsConfiguration<ApplicationUserContext>
    {
        public Configuration2()
        {
            AutomaticMigrationsEnabled = false;
            AutomaticMigrationDataLossAllowed = false;
            MigrationsDirectory = @"UserMigrations";
        }
    }
}
