﻿using System.Collections.Generic;

namespace Pentair.Domain.Client.Helpers.Commands
{
    public class ReleaseParamList : KeyCommand
    {
        public ReleaseParamList(
            string messageID,
            string condition,
            List<KeyObject> objectList = null
            )
            : base(CommandString.ReleaseParamList, messageID, condition, objectList)
        {
        }

    } // class ReleaseParamList
}