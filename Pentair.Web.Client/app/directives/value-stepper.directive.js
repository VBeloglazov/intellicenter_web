var PWC = PWC || {};
PWC.directive('valueStepper', ['$filter', '$timeout', '$interval', 'PropertyModel', function ($filter, $timeout, $interval, PropertyModel) {
    'use strict';
    return {
        replace: true,
        restrict: 'A',
        scope: {
            targetCircuit: '=',
            parentpump: '=',
            param: '@',
            disabled: '=',
            onChange: '&',
            range: '=',
            displayUom: '='
        },
        template: '<div class="paddles">' +
                    '<span class="pointer-cursor {{minusClass}}" ng-show="!editmode" ng-click="decrease()"></span>' +
                    '<span class="value-stepper-label">' +
                    '<label ng-click="enterEditMode()" ng-hide="editmode" class="{{valueClass}}">{{( text | applyFilter:valueFilter )  || "--"}}' +
                    '<span class="deg"></span>' +
                    ' <span ng-if="isTemp" class="unit">{{sysPref.MODE | unitOfMeasurment }}</span>' +
                    ' <span ng-if="displayUom" >{{displayUom}}</span>' +
                    '</label>' +
                    '<input ng-show="editmode" type="Number" ng-enter ng-blur="setValue()" class="{{valueClass}} no-spinner" ng-model="model" required></span>' +
                    '<span class="pointer-cursor {{plusClass}}" ng-show="!editmode" ng-click="increase()"></span>' +
                  '</div>',
        link: function ($scope, element, attr) {
            var oldVal = 0;
            var isSuperChlor = false;
            var superChlorTimer;
            attr.iconSize = attr.iconSize || 'icon-md';
            attr.precision = parseInt(attr.precision || 0);
            attr.color = attr.color || 'red';

            $scope.isTemp = false;
            $scope.editmode = false;
            $scope.increment = parseFloat(attr.increment) || 1;
            $scope.sysPref = PropertyModel.Objects._5451;

            $scope.forceedge = false // check edge values
            $scope.forceStep = false // 
            $scope.eggtimer = false // check egg timer
            $scope.pumprange = false            
            $scope.affilatedtype = ""

            if (attr.forceedge) {
                if (attr.forceedge === "yes")
                    $scope.forceedge = true
            }
            
            if (attr.minumum) {                              
                $scope.minimum = parseInt(attr.minumum)               
            }
            if (attr.maximum) {
                $scope.maximum = parseInt(attr.maximum)                                
            }
            
            if ($scope.parentpump) {
                $scope.pumprange = true
            }
            
            if (attr.forcestep) {
                if (attr.forcestep === "yes")
                    $scope.forceStep = true
            }

            if (attr.affilatedtype) {                
                $scope.affilatedtype = attr.affilatedtype
            }


            if (attr.eggtimer) {
                if (attr.eggtimer === "yes")
                    $scope.eggtimer = true
            }


            if (attr.color) {
                if (attr.color === "white") {   // 
                    $scope.plusClass = (attr.plusClass || '') + ' icon-paddle-wplus ' + attr.iconSize;
                    $scope.valueClass = (attr.valueClass || '') + ' level-num';
                    $scope.minusClass = (attr.minusClass || '') + ' icon-paddle-wminus ' + attr.iconSize;
                }
                else {
                    $scope.plusClass = (attr.plusClass || '') + ' icon-paddle-plus ' + attr.iconSize;
                    $scope.valueClass = (attr.valueClass || '') + ' level-num';
                    $scope.minusClass = (attr.minusClass || '') + ' icon-paddle-minus ' + attr.iconSize;
                }
            }
            $scope.class = (attr.class || '') + ' row paddles';
            $scope.valueFilter = (attr.valueFilter || 'number:' + attr.precision);
            $scope.running = false; // super chlorinate is running 
            $scope.localTIMOUT = 3600 * 24;   // 96 hours


            if ($scope.valueClass.indexOf('temperature') > -1) {
                $scope.isTemp = true;
            }

            if ($scope.param && $scope.targetCircuit && ($scope.targetCircuit[$scope.param] || $scope.targetCircuit[$scope.param] === 0)) {
                setModel($scope.targetCircuit[$scope.param]);
            } else if ($scope.targetCircuit) {
                if ($scope.targetCircuit.OBJTYP === 'SENSE')
                    setModel($scope.targetCircuit.PROBE);
            }

            if ($scope.param === 'TIMOUT' && $scope.targetCircuit && $scope.targetCircuit.SUBTYP === 'ICHLOR') {
                isSuperChlor = true;
                // over here we display it from seconds to hours when user first see's the page.
                if ($scope.targetCircuit.SUPER === 'OFF') {
                    $scope.text = $scope.targetCircuit.TIMOUT / 3600 + ' HOURS';
                    $scope.model = $scope.targetCircuit.TIMOUT / 3600;
                    $scope.localTIMOUT = $scope.targetCircuit.TIMOUT;
                }

                $scope.$watch('targetCircuit.SUPER', function (newVal, oldVal) {
                    if (newVal) {
                        switch (newVal) {
                            case 'ON':
                                if ($scope.running === false) { // set it if is not running
                                    if (angular.isUndefined(superChlorTimer)) {
                                        superChlorTimer = $interval(updateSuperChlorTime, 1000);
                                        $scope.localTIMOUT = $scope.targetCircuit.TIMOUT;
                                    }
                                }

                                $scope.running = true;
                                break;
                            case 'OFF':
                                //TODO this should come form the simulator
                                // $scope.targetCircuit.TIMOUT = "86400"

                                $interval.cancel(superChlorTimer);
                                superChlorTimer = undefined;
                                $scope.running = false;
                                break;
                        }
                    }
                });
            }

            //Update UI Value when model Changes
            $scope.$watch('targetCircuit.' + $scope.param, function (newVal, oldVal) {
                if ((newVal || newVal === 0) && newVal !== oldVal) {
                    setModel(newVal);
                    if ($scope.disabled) {
                        $scope.editmode = false;
                    }
                }
            });

            $scope.$watch('editmode', function (value) {
                if (value) {
                    $timeout(function () {
                        var SearchInput = angular.element(element.find('input'))[0];
                        SearchInput.focus();
                        SearchInput.select();
                    });
                }
            });

            $scope.enterEditMode = function (e) {
                if (!$scope.disabled) {
                    oldVal = $scope.model;
                    $scope.editmode = true;
                }
            }

            $scope.setValue = function () {
                var rounded = Math.round($scope.model / $scope.increment) * $scope.increment;
                stepValue(rounded);
                $scope.model = oldVal;
            }

            $scope.increase = function () {
                var val = parseFloat($scope.model);
                stepValue(val + getStep($scope.increment));
                /*
                var rounded = roundUpToIncrement(val);            
                if (rounded === val) {
                    stepValue(val + getStep($scope.increment));
                } else {
                    stepValue(rounded);
                }
                */
            }

            $scope.decrease = function () {
                var val = parseFloat($scope.model);
                stepValue(val - getStep($scope.increment));
                 /*
                var rounded = roundDownToIncrement(val);
                if (rounded === val) {
                    stepValue(val - getStep($scope.increment));
                } else {
                    stepValue(rounded);
                }
                */
            }

            function roundUpToIncrement(val) {
                return Math.ceil(val / $scope.increment) * $scope.increment;
            }

            function roundDownToIncrement(val) {
                return Math.floor(val / $scope.increment) * $scope.increment;
            }

            function setModel(val) {
                val = Number(val);

                //SUPER Chlorinate value
                if (isSuperChlor) {
                    switch ($scope.targetCircuit.SUPER) {
                        case 'ON':
                            break;
                        case 'OFF':
                            $scope.model = Number($scope.targetCircuit.TIMOUT) / (60 * 60);
                            $scope.text = $scope.model + ' Hours';
                            $scope.localTIMOUT = $scope.targetCircuit.TIMOUT;
                            break;
                    }
                }
                else if ($scope.param === '') {

                }
                else {
                    $scope.model = checkRange(val);
                    $scope.text = $scope.model;
                }

            }

            function updateSuperChlorTime() {
                var future = new Date();
                var diff;
                /*
                console.log("updateSuperChlorTime : " + $scope.targetCircuit.TIMOUT);
                $scope.targetCircuit.TIMOUT = Number($scope.targetCircuit.TIMOUT) - 1;                
                future.setSeconds(future.getSeconds() + $scope.targetCircuit.TIMOUT);
                */
                console.log("updateSuperChlorTime : " + $scope.localTIMOUT);
                $scope.localTIMOUT = Number($scope.localTIMOUT) - 1;
                future.setSeconds(future.getSeconds() + $scope.localTIMOUT);


                diff = Math.floor((future.getTime() - new Date().getTime()) / 1000);

                if (diff > 0) {
                    $scope.text = getCountDownString(diff);
                    console.log("remain  : " + $scope.text);
                } else {
                    $scope.text = 'Complete';
                }
            }

            function getCountDownString(t) {
                var days, hours, minutes, seconds;
                hours = Math.floor(t / 3600);
                t -= hours * 3600;
                minutes = Math.floor(t / 60) % 60;
                t -= minutes * 60;
                seconds = t % 60;
                return [
                    hours + 'h',
                    minutes + 'm',
                    seconds + 's'
                ].join(' ');
            }

            function stepValue(num) {
                $scope.editmode = false;

                if (!$scope.disabled) {
                    if ($scope.targetCircuit && ($scope.targetCircuit.OBJNAM || $scope.targetCircuit.objectName)) {
                        var value = checkRange(num);
                        $scope.onChange({
                            objNam: $scope.targetCircuit.OBJNAM || $scope.targetCircuit.objectName,
                            param: $scope.param, value: value
                        });
                        $scope.text = value;
                    }
                }
            };

            function getStep(value) {
                value = $scope.increment

                if ($scope.forceStep === false)
                    return value    // return value if range does not exist!


                if ($scope.affilatedtype != "") {
                    // checking affilated pums
                    switch ($scope.affilatedtype) {
                        case "VF":
                        case "VS":
                            if ($scope.parentpump.SETTMP)
                                value = parseInt($scope.parentpump.SETTMP)
                            break
                        case "VFF":
                            if ($scope.parentpump.SETTMPNC)
                                value = parseInt($scope.parentpump.SETTMPNC)
                            break
                    }

                } else {
                    if ($scope.targetCircuit) {
                        switch ($scope.param) {
                            case "PRIMFLO":
                            case "MIN":
                            case "MAX":
                                if ($scope.targetCircuit.SETTMP)
                                    value = parseInt($scope.targetCircuit.SETTMP)
                                break

                            case "MINF":
                            case "MAXF":
                                if ($scope.targetCircuit.SETTMPNC)
                                    value = parseInt($scope.targetCircuit.SETTMPNC)
                                //if ($scope.targetCircuit.SYSTIM)
                                //    value = parseInt($scope.targetCircuit.SYSTIM)
                                break
                        }
                    }
                }

                return value
            };

            function checkEdge(value,min,max){
                if (value > max )
                    value = max

                if (value < min )
                    value = min

                return value

            }
            function getEdgeValue(value) {                

                // checking egg timer
                if ($scope.eggtimer === true) {                    
                    if ($scope.targetCircuit) {
                        if (($scope.targetCircuit.eggTimerHours === 0) && ($scope.targetCircuit.eggTimerMinutes === 0)) {
                            $scope.targetCircuit.eggTimerMinutes = 1
                        }
                    }
                    return value
                }

                // check minimum
                if ($scope.minimum) {
                    if (value < $scope.minimum)
                        value = $scope.minimum
                }
                // check maximum
                if ($scope.maximum) {
                    if (value > $scope.maximum)
                        value = $scope.maximum
                }

                // force edge

                if ($scope.forceedge === false)
                    return value

                if (!$scope.targetCircuit)   // check if exist targetCircuit
                    return value

                if ($scope.param === '')   // no param to test, then return..
                    return value                
                
                if ($scope.affilatedtype != "") {
                    // checking affilated pums
                    switch ($scope.affilatedtype) {
                        case "VS":
                        case "VF":
                            value = checkEdge(value, $scope.parentpump.MIN, $scope.parentpump.MAX)
                            break
                        case "VFF":
                            value = checkEdge(value, $scope.parentpump.MINF, $scope.parentpump.MAXF)
                            break
                    }
                }
                else {
                    switch ($scope.param) {
                        case 'MINF':
                            if (value > $scope.targetCircuit.MAXF)
                                value = $scope.targetCircuit.MAXF
                            break;

                        case 'MIN':
                            if (value > $scope.targetCircuit.MAX)
                                value = $scope.targetCircuit.MAX
                            break;

                        case 'MAXF':
                            if (value < $scope.targetCircuit.MINF)  // value cannot go lower than min
                                value = $scope.targetCircuit.MINF

                            break;
                        case 'MAX':
                            if (value < $scope.targetCircuit.MIN)  // value cannot go lower than min
                                value = $scope.targetCircuit.MIN
                            break;

                        case "PRIMFLO":
                        case 'SPEED':       // value cannot go higher than max
                            value = checkEdge(value, $scope.targetCircuit.MIN, $scope.targetCircuit.MAX)
                            break;
                    }
                }

                return value
            };

            
            
            function checkRange(value) {
                if ($scope.range) {                    
                    if (value < $scope.range.min) {
                        value = $scope.range.min;
                    } else if (value > $scope.range.max) {
                        value = $scope.range.max;
                    }
                }

                // now process local edge
                value = getEdgeValue(value)

                // check 

                return value;
            }
        }
    };
}]);