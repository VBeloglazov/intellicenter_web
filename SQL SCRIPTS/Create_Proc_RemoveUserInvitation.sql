-- =============================================================
--
-- Author:		Vladimir Beloglazov
-- Create date: 04/23/2018
--
-- Creates:
--
--		Table Type:			None
--
--		Stored Procedure:	PRemoveUserInvitation
--
-- =============================================================

USE PathfinderIntelliCenter
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (
	SELECT * FROM sys.objects
	WHERE object_id = OBJECT_ID(N'dbo.PRemoveUserInvitation')
	AND type in (N'P', N'PC')
)
DROP PROCEDURE dbo.PRemoveUserInvitation
GO

CREATE PROCEDURE dbo.PRemoveUserInvitation
	 @invitationId AS uniqueidentifier

AS
BEGIN

	SET NOCOUNT OFF

	DELETE dbo.UserInvitations
	WHERE InvitationId = @invitationId

END
GO
