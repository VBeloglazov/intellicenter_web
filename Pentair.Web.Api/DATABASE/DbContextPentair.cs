﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Web;
using System.Web.Security;
using System.Threading.Tasks;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;

using SERVICE_SHARED;

namespace Pentair.Web.Api
{
    public class DbContextPentair : DbContext
    {
        private const string EVENT_SOURCE = "DbContextPentair";

        #region Construction

        private DbContextPentair()
            : base ("ApiContext")
        {
        } // DbContextPentair()

        /// <summary>
        /// Class factory.
        /// </summary>
        /// <exception cref="Exception"/>
        public static DbContextPentair New()
        {
            try
            {            
                return new DbContextPentair();
            }
            catch(SqlException e)
            {
                throw new Exception(
                    ExceptionManager.CustomizeException(e).ErrorMessage(
                        errorHeader: "SQL Exception",
                        addStackTrace: false // Internal .NET stack trace might be huge in this case and still not helpful
                    )
                );
            }

        } // New()

        #endregion Construction

        #region Public Interface

        /// <summary>
        /// Returns null, if not found.
        /// </summary>
        public Installation GetInstallation(
            int installationId)
        {
            DbRawSqlQuery query = this.Database.SqlQuery(
                elementType: typeof(Installation),
                sql: @"EXEC PGetInstallationById_GIT @installationId",
                parameters:
                    new SqlParameter("@installationId", installationId)
            );

            return query.Cast<Installation>().FirstOrDefault();

        } // GetInstallation()

        /// <summary>
        /// Can return empty list, but never null.
        /// </summary>
        public IEnumerable<AssociatedUser> GetUserAssociationList(
            int propertyId,
            string sortField,
            SortDirection sortDirection,
            string search,
            string filter,
            int startRow,
            int length)
        {
            // TODO: 2b replaced with SP & tested
            this.Database.ExecuteSqlCommand(
                sql: @"EXEC pr_UpdateUserInvitationsStatus @InstallationId, @InviteExpirationHours",
                parameters:
                    new object[]
                    {
                        new SqlParameter("@InstallationId", propertyId),
                        new SqlParameter("@InviteExpirationHours", StaticData.InviteValidAge)
                    }
            );

            string sortFieldDB;
            string searchString;
            string sortDirectionString;

            switch (sortField.ToLower())
            {
                case "name": sortFieldDB = "UserName"; break;
                case "role": sortFieldDB = "SecurityName"; break;
                case "status": sortFieldDB = "[Status]"; break;
                default: sortFieldDB = "EmailAddress"; break;
            }
            searchString = (!String.IsNullOrEmpty(search)) ? "WHERE LOWER(RAWLIST.EmailAddress) LIKE '%" + search.ToLower() + "%'" : String.Empty;
            sortDirectionString = (sortDirection == SortDirection.Ascending) ? "ASC" : "DESC";

            // TODO: 2b replaced with SP & tested
            DbRawSqlQuery query = this.Database.SqlQuery(
                elementType: typeof(AssociatedUser),
                sql: @"EXEC pr_GetUserAssociationList @InstallationId, @Filter, @Search, @SortField, @SortDirection, @StartRow, @NumOfRows",
                parameters:
                    new object[] {
                                    new SqlParameter("@InstallationId", propertyId),
                                    new SqlParameter("@Filter", filter.ToLower()),
                                    new SqlParameter("@Search", searchString),
                                    new SqlParameter("@SortField", sortFieldDB),
                                    new SqlParameter("@SortDirection", sortDirectionString),
                                    new SqlParameter("@StartRow", startRow),
                                    new SqlParameter("@NumOfRows", length)
                                }
            );

            return query.Cast<AssociatedUser>();

        } // GetUserAssociationList()

        public void AddUpdateAssociation(
            Guid userId,
            int installationId,
            string accessToken,
            Guid? invitationId = null)
        {
            this.Database.ExecuteSqlCommand(
                sql: @"EXEC PAddUpdateAssociation @userId, @installationId, @accessToken, @invitationId",
                parameters:
                    new object[]
                    {
                        new SqlParameter("@userId", userId),
                        new SqlParameter("@installationId", installationId),
                        new SqlParameter("@accessToken", accessToken),
                        new SqlParameter("@invitationId", invitationId ?? Guid.Empty)
                    }
            );

        } // AddUpdateAssociation()

        public void RemoveUserAssociation(
            Guid userId,
            int propertyId,
            string userEmail = "")
        {
            this.Database.ExecuteSqlCommand(
                sql: @"EXEC PRemoveAssociation @propertyId, @userId, @tokenSuperAdmin, @tokenAdmin, @userEmail",
                parameters:
                    new object[]
                    {
                        new SqlParameter("@propertyId", propertyId),
                        new SqlParameter("@userId", userId),
                        new SqlParameter("@tokenSuperAdmin", StaticData.SuperAdmin),
                        new SqlParameter("@tokenAdmin", StaticData.Admin),
                        new SqlParameter("@userEmail", userEmail)
                    }
            );

        } // RemoveUserAssociation()

        public UserManagementResult ChangeUserAccess(
            string userEmail,
            string accessToken,
            Guid userId,
            int propertyId)
        {
            try
            {
                               
                DbRawSqlQuery query = this.Database.SqlQuery(
                    elementType: typeof(UserManagementResult),
                    sql: @"EXEC PChangeUserAccess @propertyId, @userId, @userEmail, @tokenNew, @tokenSuperAdmin, @tokenAdmin",
                    parameters:
                        new object[]
                        {
                            new SqlParameter("@propertyId", propertyId),
                            new SqlParameter("@userId", userId),
                            new SqlParameter("@userEmail", userEmail),
                            new SqlParameter("@tokenNew", accessToken),
                            new SqlParameter("@tokenSuperAdmin", StaticData.SuperAdmin),
                            new SqlParameter("@tokenAdmin", StaticData.Admin)
                        }
                );

                return query.Cast<UserManagementResult>().FirstOrDefault();
            }
            catch(Exception e)
            {
                LogManager.LogErrorAsync(
                    source: EVENT_SOURCE + '.' + "ChangeUserAccess",
                    eventBriefDescription: "Updating user access failed",
                    installationId: propertyId,
                    userId: userId,
                    connectionId: Guid.Empty,
                    eventFullDescription:
                        ExceptionManager.CustomizeException(e).ErrorMessage(
                            errorHeader: null,
                            addStackTrace: false // Internal .NET stack trace might be huge in this case and still not helpful
                        )
                );

                return UserManagementResult.UnknownError;
            }

        } // ChangeUserAccess()

        /// <summary>
        /// Returns null, if not found.
        /// </summary>
        public Connection GetConnection(
            Guid connectionId)
        {
            DbRawSqlQuery query = this.Database.SqlQuery(
                elementType: typeof(Connection),
                sql: @"EXEC PGetConnectionById @connectionId",
                parameters:
                    new SqlParameter("@connectionId", connectionId)
            );

            return query.Cast<Connection>().FirstOrDefault();

        } // GetConnection()

        /// <summary>
        /// Returns null, if not found.
        /// </summary>
        public Connection GetConnectionThw(
            int installationId)
        {
            DbRawSqlQuery query = this.Database.SqlQuery(
                elementType: typeof(Connection),
                sql: @"EXEC PGetConnectionThw @installationId",
                parameters:
                    new SqlParameter("@installationId", installationId)
            );

            return query.Cast<Connection>().FirstOrDefault();

        } // GetConnectionThw()

        /// <summary>
        /// Returns new Connection object, or throws on error.
        /// Updates [Installation] record accordingly.
        /// </summary>
        /// <exception cref="Exception" />
        public Connection AddConnectionThw(
            int installationId,
            int eventId,
	        int userId)
        {
            DbRawSqlQuery query = this.Database.SqlQuery(
                elementType: typeof(Connection),
                sql: @"EXEC PAddConnectionThw @hostMachine, @installationId, @eventId, @userId",
                parameters:
                    new object[] {
                        new SqlParameter("@hostMachine", WebApiApplication.HOSTNAME),
                        new SqlParameter("@installationId", installationId),
                        new SqlParameter("@eventId", eventId),
                        new SqlParameter("@userId", userId)
                    }
            );

            return query.Cast<Connection>().FirstOrDefault();

        } // AddConnectionThw()

        /// <summary>
        /// Returns new Connection object, or throws on error.
        /// </summary>
        /// <exception cref="Exception" />
        public Connection AddConnectionClient(
            int installationId,
            Guid userId)
        {
            DbRawSqlQuery query = this.Database.SqlQuery(
                elementType: typeof(Connection),
                sql: @"EXEC PAddConnectionClient @hostMachine, @installationId, @userId",
                parameters:
                    new object[] {
                        new SqlParameter("@hostMachine", WebApiApplication.HOSTNAME),
                        new SqlParameter("@installationId", installationId),
                        new SqlParameter("@userId", userId)
                    }
            );

            return query.Cast<Connection>().FirstOrDefault();

        } // AddConnectionClient()

        /// <summary>
        /// Returns just deleted [Connections] record ID.
        /// </summary>
        public Guid RemoveConnectionThw(
            Guid connectionId,
            int installationId)
        {
            DbRawSqlQuery query = this.Database.SqlQuery(
                elementType: typeof(Guid),
                sql: @"EXEC PRemoveConnectionThw @connectionId, @installationId",
                parameters:
                    new object[] {
                        new SqlParameter("@connectionId", connectionId),
                        new SqlParameter("@installationId", installationId)
                    }
            );

            return query.Cast<Guid>().FirstOrDefault();

        } // RemoveConnectionThw()

        /// <summary>
        /// Returns just deleted [Connections] record ID.
        /// </summary>
        public Guid RemoveConnectionClient(
            Guid connectionId)
        {
            DbRawSqlQuery query = this.Database.SqlQuery(
                elementType: typeof(Guid),
                sql: @"EXEC PRemoveConnectionClient @connectionId",
                parameters:
                    new SqlParameter("@connectionId", connectionId)
            );

            return query.Cast<Guid>().FirstOrDefault();

        } // RemoveConnectionClient()

        public IEnumerable<Connection> GetThwClientConnections(
            int installationId)
        {
            DbRawSqlQuery query = this.Database.SqlQuery(
                elementType: typeof(Connection),
                sql: @"EXEC PGetThwClientConnections @installationId",
                parameters:
                    new SqlParameter("@installationId", installationId)
            );

            return query.Cast<Connection>();

        } // GetThwClientConnections()

        public void CleanupAfterPossibleCrash()
        {
            this.Database.ExecuteSqlCommand(
                sql: @"EXEC PCleanupAfterPossibleCrash @hostMachine",
                parameters:
                    new SqlParameter("@hostMachine", WebApiApplication.HOSTNAME)
            );

        } // CleanupAfterPossibleCrash()

        /// <summary>
        /// Can return empty list, but never null.
        /// </summary>
        public IEnumerable<UserInstallation> UserInstallationListWithDetails(
            Guid userId)
        {
            // TODO: 2b replaced with SP & tested
            DbRawSqlQuery query = this.Database.SqlQuery(
                elementType: typeof(UserInstallation),
                sql: @"EXEC pr_GetUserInstallationListWithDetails @UserId",
                parameters:
                    new SqlParameter("@UserId", userId)
            );

            return query.ToListAsync().Result.Cast<UserInstallation>();

        } // UserInstallationListWithDetails()

        /// <summary>
        /// Can return null.
        /// </summary>
        public UserInstallation UserSingleInstallationWithDetails(
            int installationId,
            Guid userId)
        {
            // TODO: 2b replaced with SP & tested
            DbRawSqlQuery query = this.Database.SqlQuery(
                elementType: typeof(UserInstallation),
                sql: @"EXEC pr_GetUserSingleInstallationWithDetails @InstallationId, @UserId",
                parameters:
                    new object[] {
                        new SqlParameter("@InstallationId", installationId),
                        new SqlParameter("@UserId", userId)
                    }
            );

            return query.ToListAsync().Result.Cast<UserInstallation>().FirstOrDefault();

        } // UserSingleInstallationWithDetails()

        /// <summary>
        /// Can return null.
        /// </summary>
        public InstallationWithDetails GetInstallationWithDetails(
            int installationId)
        {
            DbRawSqlQuery query = this.Database.SqlQuery(
                elementType: typeof(InstallationWithDetails),
                sql: @"EXEC PGetInstallationWithDetails @installationId",
                parameters:
                    new SqlParameter("@installationId", installationId)
            );

            return query.Cast<InstallationWithDetails>().FirstOrDefault();

        } // GetInstallationWithDetails()

        /// <summary>
        /// Updates existing or creates new history records.
        /// </summary>
        public void UpdateHistory(
            int installationId,
            List<Dictionary<string, List<HistoryItem>>> historyObjectList,
            long installationLastHistory)
        {
            // TODO: 2b replaced with SP & tested
            foreach (Dictionary<string, List<HistoryItem>> dic in historyObjectList)
            {
                foreach (KeyValuePair<string, List<HistoryItem>> historyObject in dic)
                {
                    string objName = historyObject.Key;
                    List<HistoryItem> objHistoryItems = historyObject.Value;

                    foreach (HistoryItem historyItem in objHistoryItems)
                    {
                        long revision = Convert.ToInt64(historyItem.Timestamp);

                        // TODO: History having Revision = 0 is an ERROR!
                        if (revision > 0)
                        {
                            UpdateHistoryRecord(
                                installationId: installationId,
                                revision: revision,
                                objName: objName,
                                key: historyItem.Name,
                                value: historyItem.Value
                            );

                            // 2017-10-26 VB: CPU usage here was high. Task.Delay() added to make it better.
                            Task.Delay(1).Wait();
                        }
                        else
                        {
                            LogManager.LogErrorAsync(
                                source: EVENT_SOURCE + '.' + "UpdateHistory",
                                eventBriefDescription: "Invalid history item",
                                installationId: installationId,
                                userId: Guid.Empty,
                                connectionId: Guid.Empty,
                                eventFullDescription:
                                    String.Format("Error: Invalid Revision\n\n({0}, {1}, '{2}', '{3}')",
                                        installationId,
                                        historyItem.Timestamp,
                                        objName,
                                        historyItem.Name
                                    )
                            );
                        }

                    } // foreach (HistoryItem)

                    this.Database.ExecuteSqlCommand(
                        sql: @"EXEC PUpdateInstallationLastHistory @installationId, @lastHistory",
                        parameters:
                            new object[] {
                                new SqlParameter("@installationId", installationId),
                                new SqlParameter("@lastHistory", installationLastHistory)
                            }
                    );

                } // foreach (KeyValuePair)

            } // foreach (Dictionary)

        } // UpdateHistory()

        /// <summary>
        /// Delete's installation's history records within the time interval.
        /// </summary>
        public void ClearHistory(
            int installationId,
            long timeStart,
            long timeEnd)
        {
            if (timeEnd < 1L) timeEnd = 0x7FFFFFFFFFFFFFFF; // T-SQL bigint max

            this.Database.ExecuteSqlCommand(
                sql: @"EXEC PClearHistory @installationId, @timeStart, @timeEnd",
                parameters:
                    new object[] {
                        new SqlParameter("@installationId", installationId),
                        new SqlParameter("@timeStart", timeStart),
                        new SqlParameter("@timeEnd", timeEnd)
                    }
            );

        } // ClearHistory()

        public void AddUserInvitation(
            UserInvitation userInvitation)
        {
            this.Database.ExecuteSqlCommand(
                sql: @"EXEC PAddUserInvitation @invitationId, @userEmail, @installationId, @accessToken",
                parameters:
                    new object[]
                    {
                        new SqlParameter("@invitationId", userInvitation.InvitationId),
                        new SqlParameter("@userEmail", userInvitation.EmailAddress),
                        new SqlParameter("@installationId", userInvitation.InstallationId ?? (object)DBNull.Value),
                        new SqlParameter("@accessToken", userInvitation.AccessToken),
                    }
            );

        } // AddUserInvitation()

        /// <summary>
        /// Returns null, if not found.
        /// </summary>
        public TUserInvitation GetUserInvitation(
            Guid invitationId)
        {
            DbRawSqlQuery query = this.Database.SqlQuery(
                elementType: typeof(TUserInvitation),
                sql: @"EXEC PGetUserInvitation @invitationId",
                parameters:
                    new SqlParameter("@invitationId", invitationId)
            );

            return query.Cast<TUserInvitation>().FirstOrDefault();

        } // GetUserInvitation()

        public TUserInvitation UpdateUserInvitation(
            Guid invitationId)
        {
            DbRawSqlQuery query = this.Database.SqlQuery(
                elementType: typeof(TUserInvitation),
                sql: @"EXEC PUpdateUserInvitation @invitationId",
                parameters:
                    new SqlParameter("@invitationId", invitationId)
            );

            return query.Cast<TUserInvitation>().FirstOrDefault();

        } // UpdateUserInvitation()

        public void RemoveUserInvitation(
            Guid invitationId)
        {
            this.Database.ExecuteSqlCommand(
                sql: @"EXEC PRemoveUserInvitation @invitationId",
                parameters:
                    new SqlParameter("@invitationId", invitationId)
            );

        } // RemoveUserInvitation()

        /// <summary>
        /// Can return empty list, but never null.
        /// </summary>
        public IEnumerable<NotificationRecipient> GetInstallationNotificationRecipients(
            int installationId)
        {
            DbRawSqlQuery query = this.Database.SqlQuery(
                elementType: typeof(NotificationRecipient),
                sql: @"EXEC PGetInstallationNotificationRecipients @installationId",
                parameters:
                    new SqlParameter("@installationId", installationId)
            );

            return query.Cast<NotificationRecipient>();

        } // GetInstallationNotificationRecipients()

        public bool AddInstallationNotificationRecipient(
            NotificationRecipientModel postParameters,
            int installationId)
        {
            DbRawSqlQuery query = this.Database.SqlQuery(
                elementType: typeof(int),
                sql: @"EXEC pr_AddInstallationNotificationRecipient @installationId, @name, @email, @phone",
                parameters:
                    new object[] {
                        new SqlParameter("@installationId", installationId),
                        new SqlParameter("@name", postParameters.Name),
                        new SqlParameter("@email", postParameters.EmailAddress1),
                        new SqlParameter("@phone", postParameters.Phone1),
                    }
            );

            return (query.Cast<int>().FirstOrDefault() > 0); // New RecipientId

        } // AddInstallationNotificationRecipient()

        // TODO: Not in use? 2b removed? 2018-04-05 VB
        /*
        public void UpdateInstallationNotificationRecipient(
            NotificationRecipientModel postParameters,
            int installationId,
            int recipientId)
        {
            // TODO: 2b deleted after testing
            //NotificationRecipient recipient = db.GetNotificationRecipient(recipientId);

            //if (recipient == null)
            //{
            //    return NotFound();
            //}

            //recipient.InstallationId = hardwareId;
            //recipient.Name = postParameters.Name;
            //recipient.EmailAddress1 = postParameters.EmailAddress1;
            //recipient.EmailAddress2 = postParameters.EmailAddress2;
            //recipient.Phone1 = postParameters.Phone1;
            //recipient.Phone2 = postParameters.Phone2;

            //db.SaveChanges();

            // TODO: 2b replaced with SP & tested

            // Update existing record
            this.Database.ExecuteSqlCommand(
                sql: @"UPDATE NotificationRecipients
                       SET  InstallationId = @installationId
                           ,Name = @name
                           ,EmailAddress1 = @email1
	                       ,EmailAddress2 = @email2
	                       ,Phone1 = @phone1
	                       ,Phone2 = @phone2
                       WHERE RecipientId = @recipientId
                      ",
                parameters:
                    new object[] {
                        new SqlParameter("@installationId", installationId),
                        new SqlParameter("@name", postParameters.Name),
                        new SqlParameter("@email1", postParameters.EmailAddress1),
                        new SqlParameter("@email2", postParameters.EmailAddress2),
                        new SqlParameter("@phone1", postParameters.Phone1),
                        new SqlParameter("@phone2", postParameters.Phone2),
                        new SqlParameter("@recipientId", recipientId)
                    }
            );

        } // UpdateInstallationNotificationRecipient()
        */

        public bool DeleteInstallationNotificationRecipients(
            NotificationRecipientList notificationRecipientList)
        {
            StringBuilder sbRecipientIdList = new StringBuilder();
            for (int i=0; i<notificationRecipientList.RecipientId.Count; i++)
            {
                string idTxt = notificationRecipientList.RecipientId[i];

                int recipientId;
                if (!int.TryParse(idTxt, out recipientId))
                {
                    return false;
                }

                sbRecipientIdList.AppendFormat("{0}{1}",
                    recipientId,
                    (i < notificationRecipientList.RecipientId.Count - 1) ? "," : String.Empty
                );
            }

            this.Database.ExecuteSqlCommand(
                sql: @"EXEC PDeleteInstallationNotificationRecipient @recipientIdList",
                parameters:
                    new SqlParameter("@recipientIdList", sbRecipientIdList.ToString())
            );

            return true;

        } // DeleteInstallationNotificationRecipients()

        public List<KeyObject> ClearParam(
            Guid clientId,
            int propertyId)
        {
            DbRawSqlQuery query = this.Database.SqlQuery(
                elementType: typeof(TSubscription),
                sql: String.Format(@"EXEC PClearParam '{0}', {1}", clientId, propertyId)
            );

            IEnumerable<TSubscription> tsubscriptionsDeleted = query.Cast<TSubscription>();

            List<KeyObject> lstCancellations = new List<KeyObject>();

            foreach (TSubscription deleted in tsubscriptionsDeleted)
            {
                if (lstCancellations.FirstOrDefault(o => (o.ObjNam == deleted.ObjNam)) == null)
                {
                    List<string> keys = new List<string>();
                    foreach (TSubscription objSubscription in tsubscriptionsDeleted.Where(o => o.ObjNam == deleted.ObjNam))
                    {
                        keys.Add(objSubscription.ObjParm);
                    }

                    lstCancellations.Add(
                        new KeyObject(
                            objNam: deleted.ObjNam,
                            keys: keys
                        )
                    );
                }
            }

            return lstCancellations;

        } // ClearParam()

        public void ScheduleNotifications(
            Guid clientId,
            int propertyId,
            List<KeyObject> objList)
        {
            DataTable tSubscriptionsToSchedule = new DataTable();

            // Must match dbo.TSubscription
            tSubscriptionsToSchedule.Columns.Add("ClientId", typeof(Guid));
            tSubscriptionsToSchedule.Columns.Add("PropertyId", typeof(int));
            tSubscriptionsToSchedule.Columns.Add("ObjNam", typeof(string));
            tSubscriptionsToSchedule.Columns.Add("ObjParm", typeof(string));

            foreach (KeyObject keyObject in objList)
            {
                if (keyObject != null) // VB: Sometimes clients do that!
                {
                    foreach (string key in keyObject.Keys)
                    {
                        DataRow rSubscriptionsToSchedule = tSubscriptionsToSchedule.NewRow();
                        rSubscriptionsToSchedule["ClientId"] = clientId;
                        rSubscriptionsToSchedule["PropertyId"] = propertyId;
                        rSubscriptionsToSchedule["ObjNam"] = keyObject.ObjNam;
                        rSubscriptionsToSchedule["ObjParm"] = key;

                        tSubscriptionsToSchedule.Rows.Add(rSubscriptionsToSchedule);
                    }
                }
            }

            this.Database.ExecuteSqlCommand(
                sql: @"EXEC PScheduleNotifications @subscriptions",
                parameters:
                    new SqlParameter("@subscriptions", tSubscriptionsToSchedule) { TypeName = "dbo.TSubscription" }
            );

        } // ScheduleNotifications()

        public List<KeyObject> ReleaseParamList(
            Guid clientId,
            int propertyId,
            List<KeyObject> objList)
        {
            // TODO: 2b tested

            DataTable tSubscriptionsToDelete = new DataTable();

            // Must match dbo.TSubscription
            tSubscriptionsToDelete.Columns.Add("ClientId",   typeof(Guid));
            tSubscriptionsToDelete.Columns.Add("PropertyId", typeof(int));
            tSubscriptionsToDelete.Columns.Add("ObjNam",     typeof(string));
            tSubscriptionsToDelete.Columns.Add("ObjParm",    typeof(string));

            foreach (KeyObject keyObject in objList)
            {
                if (keyObject != null) // VB: Sometimes clients do that!
                {
                    foreach (string key in keyObject.Keys)
                    {
                        DataRow rSubscriptionsToDelete = tSubscriptionsToDelete.NewRow();
                        rSubscriptionsToDelete["ClientId"] = clientId;
                        rSubscriptionsToDelete["PropertyId"] = propertyId;
                        rSubscriptionsToDelete["ObjNam"] = keyObject.ObjNam;
                        rSubscriptionsToDelete["ObjParm"] = key;

                        tSubscriptionsToDelete.Rows.Add(rSubscriptionsToDelete);
                    }
                }
            }

            DbRawSqlQuery query = this.Database.SqlQuery(
                elementType: typeof(TSubscription),
                sql: @"EXEC PReleaseParamList @subscriptions",
                parameters:
                    new SqlParameter("@subscriptions", tSubscriptionsToDelete) { TypeName = "dbo.TSubscription" }
            );

            IEnumerable<TSubscription> tsubscriptionsDeleted = query.Cast<TSubscription>();

            List<KeyObject> lstCancellations = new List<KeyObject>();

            foreach (TSubscription deleted in tsubscriptionsDeleted)
            {
                if (lstCancellations.FirstOrDefault(o => (o.ObjNam == deleted.ObjNam)) == null)
                {
                    List<string> keys = new List<string>();
                    foreach (TSubscription objSubscription in tsubscriptionsDeleted.Where(o => o.ObjNam == deleted.ObjNam))
                    {
                        keys.Add(objSubscription.ObjParm);
                    }

                    lstCancellations.Add(
                        new KeyObject(
                            objNam: deleted.ObjNam,
                            keys: keys
                        )
                    );
                }
            }

            return lstCancellations;

        } // ReleaseParamList()

        public void TouchInstallation(
            int installationId,
            long timeNow)
        {
            // TODO: 2b replaced with SP & tested
            this.Database.ExecuteSqlCommand(
                sql: @"EXEC pr_UpdateInstallationLastChange @InstallationId, @TimeNow",
                parameters:
                    new object[] {
                                new SqlParameter("@InstallationId", installationId),
                                new SqlParameter("@TimeNow", timeNow)
                            }
            );

        } // TouchInstallation()

        public long GetThwLastHistoryTime(
            int installationId)
        {
            DbRawSqlQuery query = this.Database.SqlQuery(
                elementType: typeof(long?),
                sql: @"EXEC PGetThwLastHistoryTime @installationId",
                parameters:
                    new SqlParameter("@installationId", installationId)
            );

            long? ret = query.Cast<long?>().FirstOrDefault();

            return ret ?? 0L;

        } // GetThwLastHistoryTime()

        public CreateNewPropertyReturn CreateNewProperty(
            string name,
            Guid userId,
            string accessToken)
        {
            DbRawSqlQuery query = this.Database.SqlQuery(
                elementType: typeof(CreateNewPropertyReturn),
                sql: @"EXEC PCreateNewProperty @name, @userId, @newSecret, @accessToken",
                parameters:
                    new object[] {
                        new SqlParameter("@name", name),
                        new SqlParameter("@userId", userId),
                        new SqlParameter("@newSecret", Membership.GeneratePassword(64, 40)),
                        new SqlParameter("@accessToken", accessToken)
                    }
           );

            return query.Cast<CreateNewPropertyReturn>().FirstOrDefault();

        } // CreateNewProperty()

        public ReRegisterPropertyReturn ReRegisterProperty(
            int installationId,
            Guid userId)
        {
            DbRawSqlQuery query = this.Database.SqlQuery(
                elementType: typeof(ReRegisterPropertyReturn),
                sql: @"EXEC PReRegisterProperty @installationId, @userId, @newSecret",
                parameters:
                    new object[] {
                        new SqlParameter("@installationId", installationId),
                        new SqlParameter("@userId", userId),
                        new SqlParameter("@newSecret", Membership.GeneratePassword(64, 40))
                    }
           );

            return query.Cast<ReRegisterPropertyReturn>().FirstOrDefault();

        } // ReRegisterProperty()

        public void UpdateControlObjects(
            int installationId,
            List<ExtendedParamObject> objList,
            bool deleteNotPresented)
        {
            // Must match dbo.TControlObjects
            DataTable tControlObjectsPresented = new DataTable();
            tControlObjectsPresented.Columns.Add("ObjNam", typeof(string));
            tControlObjectsPresented.Columns.Add("Key",    typeof(string));
            tControlObjectsPresented.Columns.Add("Value",  typeof(string));
            tControlObjectsPresented.PrimaryKey = new DataColumn[] {
                  tControlObjectsPresented.Columns["ObjNam"]
                , tControlObjectsPresented.Columns["Key"]
                , tControlObjectsPresented.Columns["Value"]
            };

            // Must match dbo.TStringList
            DataTable tControlObjectsDeleted = new DataTable();
            tControlObjectsDeleted.Columns.Add("Value", typeof(string));
            tControlObjectsDeleted.PrimaryKey = new DataColumn[] {
                tControlObjectsDeleted.Columns["Value"]
            };

            string newPropertyName = null;

            foreach (ExtendedParamObject extendedParamObject in objList)
            {
                if (extendedParamObject.Created != null)
                {
                    foreach (ParamObject paramObject in extendedParamObject.Created)
                    {
                        foreach (KeyValuePair<string, string> parameter in paramObject.Parameters)
                        {
                            DataRow rControlObjects = tControlObjectsPresented.NewRow();
                            rControlObjects["ObjNam"] = paramObject.ObjNam;
                            rControlObjects["Key"] = parameter.Key;
                            rControlObjects["Value"] = parameter.Value;

                            try
                            {
                                tControlObjectsPresented.Rows.Add(rControlObjects);
                            }
                            catch(ConstraintException)
                            {
                                // VB: We conciously swallow this exception as it only means
                                //     the object was presented more than once in the objList.
                                //     With nice client it should not happen, but even if the
                                //     client is not nice, the show on the server side must go on!
                            }
                        }
                    }
                }

                if (extendedParamObject.Changes != null)
                {
                    foreach (ParamObject paramObject in extendedParamObject.Changes)
                    {
                        foreach (KeyValuePair<string, string> parameter in paramObject.Parameters)
                        {
                            DataRow rControlObjectsPresented = tControlObjectsPresented.NewRow();
                            rControlObjectsPresented["ObjNam"] = paramObject.ObjNam;
                            rControlObjectsPresented["Key"] = parameter.Key;
                            rControlObjectsPresented["Value"] = parameter.Value;

                            try
                            {
                                tControlObjectsPresented.Rows.Add(rControlObjectsPresented);
                            }
                            catch (ConstraintException)
                            {
                                // VB: We conciously swallow this exception as it only means
                                //     the object was presented more than once in the objList.
                                //     With nice client it should not happen, but even if the
                                //     client is not nice, the show on the server side must go on!
                            }

                            if (parameter.Key == "PROPNAME")
                            {
                                newPropertyName = parameter.Value;
                            }
                        }
                    }
                }

                if (extendedParamObject.Deleted != null)
                {
                    foreach (string objName in extendedParamObject.Deleted)
                    {
                        DataRow rControlObjectsDeleted = tControlObjectsDeleted.NewRow();
                        rControlObjectsDeleted["Value"] = objName;

                        try
                        {
                            tControlObjectsDeleted.Rows.Add(rControlObjectsDeleted);
                        }
                        catch (ConstraintException)
                        {
                            // VB: We conciously swallow this exception as it only means
                            //     the object was presented more than once in the objList.
                            //     With nice client it should not happen, but even if the
                            //     client is not nice, the show on the server side must go on!
                        }
                    }
                }

            } // foreach (ExtendedParamObject extendedParamObject)

            // Update [ControlObjects]
            this.Database.ExecuteSqlCommand(
                sql: @"EXEC PUpdateControlObjects @installationId, @deleteNotPresented, @controlObjectsPresented, @controlObjectsDeleted",
                parameters:
                new object[] {
                        new SqlParameter("@installationId",          installationId),
                        new SqlParameter("@deleteNotPresented",      deleteNotPresented),
                        new SqlParameter("@controlObjectsPresented", tControlObjectsPresented) { TypeName = "dbo.TControlObject" },
                        new SqlParameter("@controlObjectsDeleted",   tControlObjectsDeleted)   { TypeName = "dbo.TStringList"    },
                    }
            );

            // Update [Installations]
            if (!String.IsNullOrWhiteSpace(newPropertyName))
            {
                try
                {
                    // Can throw, if UK_Installations_Name_OwnerUserId violation.
                    this.Database.ExecuteSqlCommand(
                        sql: @"EXEC PUpdateInstallationName @installationId, @name",
                        parameters:
                        new object[] {
                        new SqlParameter("@installationId", installationId),
                        new SqlParameter("@name",           newPropertyName)
                    }
                    );
                }
                catch (Exception e)
                {
                    // Log the error, but don't re-throw as the [ControlObjects] update succeeded.
                    LogManager.LogErrorAsync(
                        source: EVENT_SOURCE + '.' + "UpdateControlObjects",
                        eventBriefDescription: "Updating property name failed",
                        installationId: installationId,
                        userId: Guid.Empty,
                        connectionId: Guid.Empty,
                        eventFullDescription:
                            String.Format("New name: {0}\n\n{1}",
                                newPropertyName,
                                ExceptionManager.CustomizeException(e).ErrorMessage(
                                    errorHeader: null,
                                    addStackTrace: false // Internal .NET stack trace might be huge in this case and still not helpful
                                )
                            )
                    );
                }
            }

        } // UpdateControlObjects()

        /// <summary>
        /// Can return null.
        /// </summary>
        public AlertNote GetAlertNote(
            string alertType,
            string alertMessage)
        {
            DbRawSqlQuery query = this.Database.SqlQuery(
                elementType: typeof(AlertNote),
                sql: @"EXEC pr_GetAlertNote @AlertType, @AlertMessage",
                parameters:
                    new object[] {
                        new SqlParameter("@AlertType", alertType),
                        new SqlParameter("@AlertMessage", alertMessage)
                    }
            );

            return query.Cast<AlertNote>().FirstOrDefault();

        } // GetAlertNote()

        /// <summary>
        /// Can return null.
        /// </summary>
        public AlertNotification GetAlertNotification(
            int installationId,
            int alertStatus,
            string alertType,
            string alertMessage)
        {
            DbRawSqlQuery query = this.Database.SqlQuery(
                elementType: typeof(AlertNotification),
                sql: @"EXEC pr_GetAlertNotification @InstallationId, @AlertStatus, @AlertType, @AlertMessage",
                parameters:
                    new object[] {
                        new SqlParameter("@InstallationId", installationId),
                        new SqlParameter("@AlertStatus", alertStatus),
                        new SqlParameter("@AlertType", alertType),
                        new SqlParameter("@AlertMessage", alertMessage)
                    }
            );

            return query.Cast<AlertNotification>().FirstOrDefault();

        } // AlertNotification()


        public IEnumerable<ControlObjectsHistory> GetHistoryRecord(
            int installationId,
            long startTime,
            long endTime)
        {
            // TODO: 2b replaced with SP & tested
            DbRawSqlQuery query = this.Database.SqlQuery(
                elementType: typeof(ControlObjectsHistory),
                sql: @"EXEC pr_GetHistoryRecord @InstallationId, @StartTime, @EndTime",
                parameters:
                    new object[] {
                        new SqlParameter("@InstallationId", installationId),
                        new SqlParameter("@StartTime", startTime),
                        new SqlParameter("@EndTime", endTime)
                    }
            );

            return query.ToListAsync().Result.Cast<ControlObjectsHistory>();

        } // GetHistoryRecord()

        public IEnumerable<PostalCodeInfo> GetPostalCodeInfo(
            string postalCode)
        {
            DbRawSqlQuery query = this.Database.SqlQuery(
                elementType: typeof(PostalCodeInfo),
                sql: @"EXEC pr_GetPostalCodeInfo @PostalCode",
                parameters:
                    new SqlParameter("@PostalCode", postalCode)
            );

            return query.ToListAsync().Result.Cast<PostalCodeInfo>();

        } // GetPostalCodeInfo()

        public IEnumerable<PostalCodeInfo> GetCityInfo(
            string city,
            string state)
        {
            DbRawSqlQuery query = this.Database.SqlQuery(
                elementType: typeof(PostalCodeInfo),
                sql: @"EXEC pr_GetCityInfo @City, @State",
                parameters:
                    new object[]
                    {
                        new SqlParameter("@City", city),
                        new SqlParameter("@State", state)
                    }
            );

            return query.ToListAsync().Result.Cast<PostalCodeInfo>();

        } // GetCityInfo()

        #endregion Public Interface

        #region Privates

        private void UpdateHistoryRecord(
            int installationId,
            long revision,
            string objName,
            string key,
            string value)
        {
            // TODO: 2b replaced with SP & tested
            this.Database.ExecuteSqlCommand(
                sql: @"EXEC pr_UpdateHistoryRecord @InstallationId, @Revision, @ObjName, @Key, @Value",
                parameters:
                    new object[] {
                        new SqlParameter("@InstallationId", installationId),
                        new SqlParameter("@Revision", revision),
                        new SqlParameter("@ObjName", objName),
                        new SqlParameter("@Key", key),
                        new SqlParameter("@Value", value)
                    }
            );

        } // UpdateHistoryRecord()

        #endregion Privates

    } // class DbContextPentair
}
