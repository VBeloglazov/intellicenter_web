/// <reference path="SystemInformationController.js" />
/// <reference path="SystemInformationController.js" />
/// <reference path="SystemInformationController.js" />
'use strict';
var PWC = PWC || {};
PWC.controller('SystemInformationController', [
  '$rootScope', '$scope', '$location', '$filter', '$q','$route', '$http', 'Enumerations', 'PropertyService', 
  'PropertyModel', 'AppService', 'Notification', 'SocketService',
  function ($rootScope, $scope, $location, $filter, $q, $route, $http, Enumerations, propertyService, 
  propertyModel, appService, Notification, socketService) {

    var savedData = {};
    $scope.PreferenceData = {};
    $scope.timeData = {};
    $scope.dataLoaded = false;
    $scope.UoM = 'C';
    $scope.localpropertyModel = propertyModel;
    $scope.locationTimeZone = ""
    $scope.PropertyModel = propertyModel;


    $scope.chems = [];
    $scope.zip_cities = [];
    $scope.zipcitiesEnable = false;
    $scope.searching = false;       // to indicate user search is in progress
    

    // AC 6-1-17 This is controller that gets loaded when we first come to this page by default.
    // Since we unconditionally go here we need to tell the controller to subscribe to the objects it needs over here.
    // On subsequent tab navigation to here, the subscription is handled in the configurations controller.
    socketService.clearSubscriptions();
    propertyService.SubscribeToPage('GENERAL').then(function () {
      savedData = {
        AddressModel: angular.extend({}, propertyModel.Objects['_5451']),
        LocationModel: angular.extend({}, propertyModel.Objects['_C10C']),
        SystemInfo: angular.extend({}, propertyModel.SystemInfo[propertyModel.SelectedInstallation]),
        Source: propertyModel.Objects["_C105"].SOURCE,
        ManOver: propertyModel.Objects["_CFEA"].MANOVR,
        Mode: propertyModel.Objects["_5451"].MODE,
        VacMode: propertyModel.Objects["_5451"].VACFLO, //AVAIL,
        Manht: propertyModel.Objects["_5451"].MANHT,
        PropertyName: '',
        OwnerName: '',
        AirSensors: angular.copy(propertyModel.AirSensors),
        SolarSensors: angular.copy(propertyModel.SolarSensors),
        WaterSensors: angular.copy(propertyModel.WaterSensors)
      };

      $scope.PreferenceData = {
        MODE: propertyModel.Objects["_5451"].MODE,
        MANOVR: propertyModel.Objects["_CFEA"].MANOVR,
        CLK24A: propertyModel.Objects["_C10C"].CLK24A,
        SOURCE: propertyModel.Objects["_C105"].SOURCE,
        AVAIL: propertyModel.Objects["_5451"].AVAIL,
        VACFLO: propertyModel.Objects["_5451"].VACFLO,
        MANHT: propertyModel.Objects["_5451"].MANHT,
        MIN: $filter('translateTime')(propertyModel.Objects["_C10C"].MIN),
        DAY: $filter('translateDate')(propertyModel.Objects["_C10C"].DAY),
        TEMPNC: propertyModel.Objects['_5451'].TEMPNC,
        VALVE: propertyModel.Objects['_5451'].VALVE,
        HEATING: propertyModel.Objects['_5451'].HEATING,
        VER: propertyModel.Objects['_5451'].VER,        
      };

      var clk24 = "AMPM"
      if ($scope.LocationModel && $scope.LocationModel.CLK24A)
          clk24 = $scope.LocationModel.CLK24A

      $scope.timeData = {
        TIMZON: "-8",  //$scope.AddressModel.TIMZON,
        CLK24A: clk24,
        DLSTIM: false, //$scope.LocationModel.DLSTIM !== 'OFF',
        OFFSET: "",
        DAY: "",
        MIN: "",
        SOURCE: propertyModel.Objects["_C105"].SOURCE,
        TimeZone: ''
      };

      if ($scope.LocationModel) {
          if ($scope.LocationModel.DLSTIM) {
              $scope.timeData.DLSTIM = ($scope.LocationModel.DLSTIM !== 'OFF');
          }
      }


      if (propertyModel.Objects["_C10C"] && propertyModel.Objects["_C10C"].TIMZON)
          $scope.timeData.TIMZON = propertyModel.Objects["_C10C"].TIMZON


      $scope.dataLoaded = true;
    });

    $scope.SHOMNUItems = 'vogqkwzactrefluphmnbxiPSC';
    $scope.SHOMNUItemsEditable = 'vogqkwactrefluphmnbxiPSC';
    //TODO Administrator to Pool Builder and Guest to Customer
      //$scope.securityModel = angular.copy(propertyModel.Security);
    $scope.securityModel = propertyModel.Security;
    $scope.securityItemsToDelete = [];
    $scope.accordionStateByObjectNames = {};
    $scope.SHOMNU = propertyModel.ACL.SHOMNU;
    $scope.ACL = propertyModel.ACL;
    $scope.securityEnabled = propertyModel.SecurityEnabled;
      // 
    $scope.securitySupervisor = false;
    $scope.securityGuess = false;
    
    updateChemistry();
      //updateSupervisorSecurity();


    $scope.mapSHOMNUtoAccess = function(shomnu) {
      var accessCheckBoxModel = [];
      if(shomnu === undefined) {
          for(var i=0; i<$scope.SHOMNUItemsEditable.length; i++) {
              accessCheckBoxModel.push(false);
          }
      } else {
        for(var i=0; i<$scope.SHOMNUItemsEditable.length; i++) {
            var containsCharacter = (shomnu.indexOf($scope.SHOMNUItemsEditable.charAt(i)) > -1);
            accessCheckBoxModel.push(containsCharacter);
        }
      }
      return accessCheckBoxModel;
    };

    angular.forEach($scope.securityModel, function(securityModelItem) {
      securityModelItem.confirmPASSWRD = securityModelItem.PASSWRD;
      securityModelItem.accessCheckBoxModel = $scope.mapSHOMNUtoAccess(securityModelItem.SHOMNU);
    });

    $scope.mapAccessToSHOMNU = function(securityItem) {
      var accessCheckBoxModel = securityItem.accessCheckBoxModel;
      var shomnu = '';
      for(var i=0; i<$scope.SHOMNUItemsEditable.length; i++) {
        if(accessCheckBoxModel[i] === true) {
          shomnu += $scope.SHOMNUItemsEditable.charAt(i);
        }
      }
      securityItem.SHOMNU = shomnu;
    };

    $scope.Security = {
      DisplayPIN: false,
      GuestAccessEnable: propertyModel.GuestEnabled,
      SecurityEnable: propertyModel.SecurityEnabled
    };

    $scope.property = propertyModel;
    $scope.airSensors = angular.copy(propertyModel.AirSensors);
    $scope.solarSensors = angular.copy(propertyModel.SolarSensors);
    $scope.waterSensors = angular.copy(propertyModel.WaterSensors);
    console.log(propertyModel.AirSensors);

      // REQUEST NOTIFICATION ON CHANGED.
    propertyService.getUpdateClock();   // UPDATE CLOCK              
    //$scope.airSensors = [
    //  { OBJNAM: 'test1', sName: 'Air', temp: 76 }
    //];
    //
    //$scope.solarSensors = [
    //  { OBJNAM: 'test2', sName: 'Solar', temp: 82 },
    //  { OBJNAM: 'test3', sName: 'Solar 2', temp: 83 },
    //  { OBJNAM: 'test4', sName: 'Solar 3', temp: 81 }
    //];
    //
    //$scope.waterSensors = [
    //  { OBJNAM: 'test5', sName: 'Pool', temp: 78 },
    //  { OBJNAM: 'test6', sName: 'Spa', temp: 79 }
    //];

    //TODO setting to empty until this is live
    //$scope.airSensors = [];
    //$scope.solarSensors = [];
    //$scope.waterSensors = [];

    $scope.errorSave = function(errorMessage) {
      Notification.error({message: 'Failed!' + ' ' + errorMessage, delay: 2500});
    };

    $scope.successSave = function() {
      Notification.success({message: 'Saved!', delay: 1000});
    };

    $scope.successCancel = function() {
      Notification.success({message: 'Cancelled!', delay: 1000});
    };
    $rootScope.forms = {};
    $rootScope.forms.general = {};

    $scope.getMaxSensorLength = function() {
      var lengths = [];
      lengths.push($scope.airSensors.length);
      lengths.push($scope.solarSensors.length);
      lengths.push($scope.waterSensors.length);
      lengths.sort();
      return lengths.pop();
    };

    $scope.getTimes = function(n) {
      return new Array(n);
    }

    $scope.countries = Enumerations.countries;
    $scope.phoneNumberPattern = /^(?!.*911.*\d{4})((\+?1[\/ ]?)?(?![\(\. -]?555.*)\( ?[2-9][0-9]{2} ?\) ?|(\+?1[\.\/ -])?[2-9][0-9]{2}[\.\/ -]?)(?!555.?01..)([2-9][0-9]{2})[\.\/ -]?([0-9]{4})$/;
    $scope.StatusMessageTypeFlags = Enumerations.StatusMessageTypeFlags;
    $scope.Property = propertyService;
    $scope.Model = propertyModel;
    $scope.AddressModel = propertyModel.Objects['_5451'];
    $scope.LocationModel = propertyModel.Objects['_C10C'];
    $scope.timeZones = [];
    //$scope.LocationModel.Address = '';
    $scope.forms = {};
    $scope.$parent.Layout = "layout-base";
    $scope.NotificationRecipients = [];
    $scope.SelectAllRecipients = false;
    $scope.ReciepientsSelected = false;
    $scope.longLatError = 'unresolved';
    $scope.installationNames = {
      PropertyName: '',
      OwnerName: '',
      OwnerPhone: '',
      OwnerEmail: ''
    };

    $rootScope.editMode.general = {};

    $rootScope.editMode.general = {
      LocationInfo: false,
      PersonalInfo: false,
      AdvancedSettings: false,
      NewRecipient: false,
      Security: false
    };

    $scope.NewRecipient = {
      PropertyId: propertyModel.SelectedInstallation.toString(),      
      Name: null,
      Phone1: null,
      EmailAddress1: null,
      Phone2: null,
      EmailAddress2: null,
      UserId: null
    };

    $scope.selectedStatusMessageTypes = {
      Circuit: [],
      Pump: [],
      IntelliChlor: [],
      IntelliChem: [],
      HeaterUltraT: [],
      HeaterMasterT: [],
      HeaterHComboT: [],
    };


    $scope.locationData = {
      LOCX: "",
      LOCY: "",
      Address: "",
      City: "",
      State: "",
      Zip: "",
      Country: ""
    };

    init();

    $scope.$on('ObjectUpdated', function () {
      resetTime();
    });

    $scope.$on('SecurityUpdated', function () {
        resetSecurity();

        //updateSupervisorSecurity();
    });

    $scope.$on('PropertyStatusMessageFlagsUpdated', function () {
      resetStatusMessageFlags();
    });

    $scope.$on('SystemInformationUpdated', function () {
      resetInstallationNames();
    });

    $scope.$on('NotificationRecipientUpdated', function () {
      resetNotificationUsers();
    });

    $scope.$on('SystemConfigUpdated', function () {
      savedData.AddressModel = angular.extend({}, propertyModel.Objects['_5451']);

      if (!$rootScope.editMode.general.PersonalInfo) {
        $scope.AddressModel.NAME = savedData.AddressModel.NAME;
        $scope.installationNames.OwnerName = savedData.AddressModel.NAME;
        $scope.installationNames.OwnerPhone = savedData.AddressModel.PHONE;
        $scope.installationNames.OwnerEmail = savedData.AddressModel.EMAIL;
        $scope.installationNames.PropertyName = savedData.AddressModel.PROPNAME;        
      }
      if (!$rootScope.editMode.general.LocationInfo) {
        $scope.AddressModel.ADDRESS = savedData.AddressModel.ADDRESS;
        $scope.AddressModel.CITY = savedData.AddressModel.CITY;
        $scope.AddressModel.STATE = savedData.AddressModel.STATE;
        $scope.AddressModel.ZIP = savedData.AddressModel.ZIP;
        $scope.AddressModel.COUNTRY = savedData.AddressModel.COUNTRY;
        $scope.AddressModel.TIMZON = savedData.AddressModel.TIMZON;
      }
    });

    $scope.SaveNewRecipient = function () {
      //if ($rootScope.forms.general.newRep.$valid) {
        appService.PostData('Api/NotificationUsers/', $scope.NewRecipient).then(function () {
          propertyService.GetNotificationRecipients();
          $scope.newRecipientEdit = false;
        });
      //}
    };
    
    $scope.isNewRecipientReady = function () {

        if ((!$scope) ||
            (!$scope.NewRecipient) ||
            ( !$scope.NewRecipient.Name ) ||
            ( !$scope.NewRecipient.EmailAddress1 ) )
            
            return false;

        if ( ($scope.NewRecipient.Name.length > 0)  &&
             ($scope.NewRecipient.EmailAddress1.length > 0) 
             
            )
            return true;
        else
            return false;                           
    };



    $scope.SaveInstallationNames = function () {
      savedData.PropertyName = $scope.installationNames.PropertyName;
      savedData.OwnerName = $scope.installationNames.OwnerName;
      savedData.OwnerPhone = $scope.installationNames.OwnerPhone;
      savedData.OwnerEmail = $scope.installationNames.OwnerEmail;
      if ($rootScope.forms.general.names.$valid) {

        var info = {
          PropertyId: $scope.Model.SelectedInstallation,
          Name: $scope.installationNames.PropertyName,
          OwnerName: $scope.installationNames.OwnerName,
          OwnerPhone: $scope.installationNames.OwnerPhone,
          OwnerEmail: $scope.installationNames.OwnerEmail
        };
        propertyService.SaveOwnerInfo(info);
        delete info.OwnerPhone;
        delete info.OwnerEmail;
          /*  as per email from Vlad March-12-2018 removing comand NameChange  
        appService.PostData('Api/Installations/NameChange/', info).then(function () {
          propertyService.GetSystemInformation();
          $rootScope.forms.general.names.$setPristine();
          $scope.successSave();
        });
        */
        propertyService.GetSystemInformation();
        $rootScope.forms.general.names.$setPristine();
        $scope.successSave();

      } else {
        $scope.errorSave('Form invalid.');
      }
    };

    $scope.CancelSaveInstallationNames = function () {
      resetInstallationNames();
      $rootScope.forms.general.names.$setPristine();
      // $rootScope.editMode.general.PersonalInfo = false;
      $scope.successCancel();
    };

    $scope.CancelNewRecipient = function () {
      resetNewRecipient();
      $scope.newRecipientEdit = false;
    };

    $scope.areSelectedRecipientsToRemove = function () {
        var enabled = false;

        angular.forEach($scope.NotificationRecipients, function (item) {
            if (item.Selected) {
                enabled = true  // at least 1 recipient is selected to be removed
            }
        });

        return enabled;
    }

    $scope.RemoveRecipients = function () {
      var deletedUsers = [];
      angular.forEach($scope.NotificationRecipients, function (item) {
        if (item.Selected) {
          deletedUsers.push(item.RecipientId);
        }
      });
      if (deletedUsers.length > 0) {
        var payload = {propertyId: propertyModel.SelectedInstallation, recipientId: deletedUsers};
        appService.DeleteData('Api/NotificationUsers/', payload).then(function () {
          propertyService.GetNotificationRecipients();
          $scope.SelectAllRecipients = false;
        });
      } else {
        $scope.SelectAllRecipients = false;
      }
    };

    $scope.SaveLocationSettings = function () {
        
        $scope.zipcitiesEnable = false; // hide selection

      if ($rootScope.forms.general.location.$valid) {
        saveLongLat();

        var params = {
          ADDRESS: $scope.AddressModel.ADDRESS,
          CITY: $scope.AddressModel.CITY,
          STATE: $scope.AddressModel.STATE,
          ZIP: $scope.AddressModel.ZIP,
          COUNTRY: $scope.AddressModel.COUNTRY,
          TIMZON: $scope.AddressModel.TIMZON.toString()
        };

        propertyService.SaveLocationData(params);

        var params2 = {
          DLSTIM: $scope.timeData.DLSTIM ? 'ON' : 'OFF'
        };

        propertyService.SetParams("_C10C", params2).then(function() {
          propertyService.GetUserSettings();
        }); // DLSTIM is not part of _5451 but of _C10C

        $rootScope.forms.general.location.$setPristine();
        $scope.successSave();
        // $rootScope.editMode.general.LocationInfo = false;
      } else {
        $scope.errorSave('Form invalid.');
      }

        // save
      console.log("fetchinbg longiture and Latitude");
      $scope.FetchLongLat();
    };


    $scope.getCoordinate = function(param){
        var value = "";
        if ($scope.LocationModel) {
            switch(param){
                case "LONGITUDE":            
                        if ($scope.LocationModel.LOCX)
                            value = $scope.LocationModel.LOCX;                    
                    break;

                case "LATITUDE":                
                        if ($scope.LocationModel.LOCX)
                            value = $scope.LocationModel.LOCY;                
                    break;
            }
        }
        return value;
    }

    $scope.FetchLongLat = function () {
      $scope.longLatError = 'unresolved';
      $scope.timeZoneError = "unresolved";
      $scope.timeData.TimeZone = "";
      setTimeZone("fetching...")      
      $scope.LocationModel.LOCX = "";
      $scope.LocationModel.LOCY = "";
      if (!$rootScope.forms.general.location.zip.$invalid) {

      // replacing the service.          
      appService.GetData('Api/zipcodes?zipCode=' + $scope.AddressModel.ZIP).then(function successCallback(response) {

      // propertyService.GetPostalCodeData($scope.AddressModel.ZIP).then(function successCallback(response) {
          if (response) {
            var data = response.data;
            if (data) {
              if (data.Error) {
                $scope.longLatError = data.Error;
                $scope.timeZoneError = data.Error;
              }
              var item = data.item;
              if (item) {
                if (data.item.Longitude && item.Latitude) {
                  $scope.LocationModel.LOCX = item.Longitude;
                  $scope.LocationModel.LOCY = item.Latitude;
                  $scope.longLatError = "";
                }
                if (data.item.TimeZone) {
                  var timeZone = Number(item.TimeZone);
                  timezoneSelected(timeZone * -1);
                  $scope.timeZoneError = "";
                }
              }
            }
          }
        });
      }
    };


    // setting current city
    $scope.setCityItem = function (item) {
        if (item) {
            // set location            
            $scope.LocationModel.LOCX = item.Longitude;
            $scope.LocationModel.LOCY = item.Latitude;
            $scope.longLatError = "";
            // set utc
            if (item.UTC) {
                var timeZone = Number(item.UTC);
                timezoneSelected(timeZone);// * -1);
                //$scope.AddressModel.TIMZON
                $scope.timeZoneError = "";
            }
            // set city
            $scope.AddressModel.CITY = item.City;
            // set state                        
            $scope.AddressModel.STATE = item.State;
            // set zip
            if (item.PostalCode)
                $scope.AddressModel.ZIP = item.PostalCode;
            // set country
            $scope.AddressModel.COUNTRY = item.Country;
            // DST            
            $scope.timeData.DLSTIM = (item.DST === "Y");
        }           
    }

    $scope.selectedCity = function (item) {
        // hide selection
        $scope.zipcitiesEnable = false;
        // set city
        $scope.setCityItem(item);
    }

    $scope.areCitiesAvailable = function () {
        return $scope.zipcitiesEnable;
    };

   
    $scope.getCityStateInfo = function (item) {
        if (item.City && item.State) {
            
            return item.City + "  " + item.State + " , " + item.PostalCode;
        } else {
            return "-"
        }
    }

    $scope.DelayRetrieveCityInfo = function (city, state) {    //
        $scope.zipcitiesEnable = false; // hide selection dropbox
        $scope.searching = true;

        setTimeout(function () { $scope.RetrieveCityInfo(city,state); }, 1000); // 1 seconds
    }
    $scope.RetrieveCityInfo = function (city, state) {

        $scope.zipcitiesEnable = false; // hide selection dropbox
        $scope.searching = true;

        propertyService.GetCityCodeData(city, state).then(function successCallback(response) {
            if (response) {
                if (response.objectList.length > 0) {
                    $scope.zip_cities = [];
                    // select first item
                    var first = true;
                    angular.forEach(response.objectList, function (item) {
                        if (first === true) {
                            $scope.setCityItem(item);
                            first = false;
                        }
                        $scope.zip_cities.push(item)
                    });
                }
                // display cities popup available if more than 2 cities match to zip code
                if (response.objectList.length >= 2)
                    $scope.zipcitiesEnable = true;
                
            }
            $scope.searching = false;
        });        
    }

    $scope.DelayFetchCities = function () {    //
        $scope.zipcitiesEnable = false; // hide selection dropbox
        $scope.searching = true;
        setTimeout(function () { $scope.FetchCities(); }, 1000); // 1 seconds
    }

    $scope.FetchCities = function () {
        setTimeZone("fetching...")
        $scope.zipcitiesEnable = false; // hide selection dropbox
        $scope.searching = true;

        if (!$rootScope.forms.general.location.zip.$invalid) {

           propertyService.GetPostalCodeData($scope.AddressModel.ZIP).then(function successCallback(response) {
               if (response) {
                   if (response.objectList.length > 0) {
                       $scope.zip_cities = [];
                       var first = true;
                       // select first item
                       angular.forEach(response.objectList, function (item) {
                           if (first === true) {
                               $scope.setCityItem(item);
                               first = false;
                           }
                           $scope.zip_cities.push(item)
                       });
                   }
                   // display cities popup available if more than 2 cities match to zip code
                   if (response.objectList.length >= 2) {
                       $scope.zipcitiesEnable = true;
                   }                   
                   }
               $scope.searching = false;
               console.log("searh  [END].")
            });
        }
    };

    $scope.setSearchInProgress = function () {
        $scope.searching = true;
    }
    $scope.isSearchInProgress = function () {
        return $scope.searching;
    }

    $scope.CancelLocationEdit = function () {
      resetLocationData();
      $rootScope.forms.general.location.$setPristine();
      $scope.successCancel();
      // hide selection
      $scope.zipcitiesEnable = false;
      $scope.searching = false;
      // $rootScope.editMode.general.LocationInfo = false;
    };

      // set sensor
    $scope.setSensor = function (value, sensor) {
        //Model.AirSensors[0].CALIB = value - airSensor.SOURCE; Model.AirSensors[0].PROBE = value
        sensor.CALIB = value - sensor.SOURCE;
        sensor.PROBE = value;
        console.log(" --------------- Sensor Name = ", sensor.NAME);
        console.log(" Sensor value CALIB = ", sensor.CALIB);
        console.log(" sensor value  Adjusted = ", value);
        console.log(" sensor value  PROVE  = ", sensor.PROBE);
        return value
    };

    // Time settings Editor
    $scope.timezoneClicked = function (value) {
      value = value.toString().replace('+', '');
      timezoneSelected(Number(value));
    };

    $scope.up = function () {
    

    }
    function updateSecurityArray() {
        if ($scope.securityModel) {
            angular.forEach($scope.securityModel, function (item) {
                // admin user
                if (item.SNAME === "Administrator") {
                    if (item.ENABLE)
                        item.ENABLE = $scope.securitySupervisor;                    
                }
                // Guess user
                if (item.SNAME === "Guest") {
                    if (item.ENABLE) 
                        item.ENABLE =  $scope.securityGuess ;                    
                }
            });
        }
    };

    // Security
    $scope.SaveSecuritySettings = function () {
        var checkparams = $scope.ValidateSecurity()
        //updateSecurityArray()
        switch (checkparams) {
            case 1: // all params are good
                if ($scope.securityItemsToDelete.length > 0) {
                    propertyService.DeleteObject($scope.securityItemsToDelete).then(function () {
                        $scope.securityItemsToDelete = [];
                    });
                }
                propertyService.SaveSecurityTokens($scope.securityModel).then(function () {
                    angular.forEach($scope.securityModel, function (securityModelItem) {
                        securityModelItem.confirmPASSWRD = securityModelItem.PASSWRD;
                        securityModelItem.accessCheckBoxModel = $scope.mapSHOMNUtoAccess(securityModelItem.SHOMNU);
                    });
                    $rootScope.forms.general.security.$setPristine();
                    $scope.successSave();
                });
                break

            case 2: // password does not match
                $scope.errorSave('Password Pin does not match the confirm password.');
                break;

            case 3: // password does not match
                $scope.errorSave('Password already exists.');
                break;

            case 4: // password does not match
                $scope.errorSave('Password Pin must be 4 digits.');
                break;

            case 5: // invalid password 
                $scope.errorSave('Invalid password, please try again.');
                break;

            case 6: // invalid password 
                $scope.errorSave('Invalid password.');
                break;

            case 7: // invalid role name
                $scope.errorSave('Role cannot blank.');
                break;

            case 8: // invalid role name
                $scope.errorSave('Role already exists.');
                break;

            case 0:
            default:
                $scope.errorSave('Form invalid.');
                break;
        }
    };

    $scope.addSecurity = function() {
      var newSecurityModelItem = {
        SHOMNU: '',
        SNAME: '',
        PASSWRD: '',
        confirmPASSWRD: '',
        accessCheckBoxModel: [],
        TIMOUT: 6,
      };
      newSecurityModelItem.accessCheckBoxModel = $scope.mapSHOMNUtoAccess(newSecurityModelItem.SHOMNU);
      $scope.securityModel.push(newSecurityModelItem);
    };

    $scope.deleteRoleFromSecurityModel = function(idx) {
        if ($scope.securityModel[idx].OBJNAM) {
            $scope.objectNamesToDelete = $scope.securityModel[idx].OBJNAM; // selected object to be deleted
            var remove = [];    // item list
            remove[0] = $scope.objectNamesToDelete;
            propertyService.DeleteObject(remove);   // execute command to remove item
            $scope.securityItemsToDelete.push($scope.securityModel[idx].OBJNAM);
        }
        $scope.securityModel.splice(idx, 1);
    };

    $scope.ValidateDuplicatedPassword = function (testitem, skip) {

        var status = 0;   // succesful
        var index = 0
        angular.forEach($scope.securityModel, function (sec) {
            index = index+1
            if (index != skip) {

                if (sec.SNAME != "Guest") {  // check if item is different than guest

                    if (testitem.PASSWRD === sec.PASSWRD)
                        status = 10 // duplicated password

                    if (testitem.SNAME === sec.SNAME)
                        status = 20     // duplicated sname
                }

            }
        });
        return status;
    }
    /**
     * @return {boolean}
     */
    $scope.ValidateSecurity = function () {
      // if ($scope.securityModel[0].ENABLE === 'OFF') {
      //   return true;
        // }

        var status = 1   // succesful
        var  index  = 0
        angular.forEach($scope.securityModel, function(item) {
          index = index+1
          if(item.PASSWRD.match(/^[0-9]+$/) === null)
              status = 6;// invalid password 

          if (item.PASSWRD === "6275")
              status = 5;   // invalid password 

          if (item.PASSWRD.length !== 4 )
              status = 4;   // password does not match
                     
          if (item.PASSWRD !== item.confirmPASSWRD) 
              status = 2;   // password does not match

          if (item.SNAME != "Guest") {  // check if item is different than guest
              if ($scope.ValidateDuplicatedPassword(item, index) === 10)
                  status = 3 // Password already exists

              if ($scope.ValidateDuplicatedPassword(item, index) === 20)
                  status = 8 // duplicated role
          }

          if (item.SNAME === "")
              status = 7        
      });     
      return status;
    };

    $scope.CancelSecuritySettings = function () {
      resetSecurity();
      // $rootScope.editMode.general.Security = false;
      $rootScope.forms.general.security.$setPristine();
      // $rootScope.editMode.general.PersonalInfo = false;
      $scope.successCancel();
    };

    // Preferences
    $scope.SavePreferenceSettings = function () {
      var promises = [];
      if ($scope.ValidateDate() && $scope.ValidateTime()) {
        savedData.Mode = $scope.PreferenceData.MODE;
        savedData.Avail = $scope.PreferenceData.AVAIL;
        savedData.Vacflo = $scope.PreferenceData.VACFLO;
        savedData.ShoMnu = $scope.PreferenceData.SHOMNU;
        savedData.ManOver = $scope.PreferenceData.MANOVR;
        savedData.LocationModel.CLK24A = $scope.PreferenceData.CLK24A;
        savedData.Source = $scope.PreferenceData.SOURCE;
        savedData.TEMPNC = $scope.PreferenceData.TEMPNC;
        savedData.Manht = $scope.PreferenceData.MANHT;

        var params = {
          MODE: $scope.PreferenceData.MODE,
          AVAIL: $scope.PreferenceData.AVAIL,
          VACFLO: $scope.PreferenceData.VACFLO,
          SHOMNU: $scope.PreferenceData.SHOMNU,
          //TEMPNC: $scope.PreferenceData.TEMPNC,
          VALVE: $scope.PreferenceData.VALVE,
          HEATING: $scope.PreferenceData.HEATING,
          MANHT: $scope.PreferenceData.MANHT,
        };
        propertyService.SetParams("_5451", params);
        propertyService.SetParams("_CFEA", {MANOVR: $scope.PreferenceData.MANOVR});
        var parms = {};
        if ($scope.PreferenceData.SOURCE == 'LOCAL') {
          parms = {
            CLK24A: $scope.PreferenceData.CLK24A,
            DAY: $filter('date')(new Date($scope.PreferenceData.DAY), 'MM,dd,yy'),
            TIME: $filter('convertTime')($scope.PreferenceData.MIN)
          };
          propertyService.SetParams("_C10C", parms);
        } else {
          parms = {
            CLK24A: $scope.PreferenceData.CLK24A
          };
          propertyService.SetParams("_C10C", parms);
        }
        propertyService.SetParams("_C105", {SOURCE: $scope.PreferenceData.SOURCE});
        propertyService.GetUserSettings();
        // $rootScope.editMode.general.AdvancedSettings = false;

        savedData.AirSensors = [];
        savedData.WaterSensors = [];
        savedData.SolarSensors = [];
        angular.forEach($scope.airSensors, function(sensor) {            
            //console.log("------------- air sensor calib", sensor.CALIB.toString());
            promises.push(propertyService.SetParams(sensor.OBJNAM , {CALIB: sensor.CALIB.toString()}));
            savedData.AirSensors.push(sensor);          
        })
        angular.forEach($scope.solarSensors, function (sensor) {
            //console.log("------------- solar sensor calib", sensor.CALIB.toString());
            promises.push(propertyService.SetParams(sensor.OBJNAM , {CALIB: sensor.CALIB.toString()}));
          savedData.SolarSensors.push(sensor);
        })
        angular.forEach($scope.waterSensors, function (sensor) {
            //console.log("------------- water sensor calib", sensor.CALIB.toString());
            promises.push(propertyService.SetParams(sensor.OBJNAM , {CALIB: sensor.CALIB.toString()}));
          savedData.WaterSensors.push(sensor)
        })
        $q.all(promises).then(function() {
          $rootScope.forms.general.advanced.$setPristine();
          $scope.successSave();
          propertyService.GetHardwareDefinitionPrecise().then(function (res) {
              console.log("request time")
              propertyService.getUpdateClock();   // UPDATE CLOCK              
          });
        })

      } else {
        $scope.errorSave('Form invalid.');
      }
    };

    $scope.isVacationOn = function () {
        if ($scope.PreferenceData.AVAIL === 'ON')
            return true
        else
            return false
    }

    $scope.DisplayUser = function (item) {        
        
        if (item.name === 'Guest') {
            if (($scope.securityModel[0].ENABLE === "ON") &&
                ($scope.securityModel[1].ENABLE === "ON")) {
                return true;    // return true only if both are enabled.
            }
            
            return false;
        } else {
            return true; // display any other users
        }

    };
    

    function updateSensors() {
      $scope.airSensors = $scope.property.hardwareDefinition.map(function(hPanel) {
        // console.log(hPanel)
        return hPanel.params.OBJLIST
          .filter(function (objectItem) {
            // needed to only grab aux circuits here with the most recent API changes
            if (objectItem.params.CIRCUITS && objectItem.params.CIRCUITS.params.SUBTYP === 'AIR') {
              return objectItem;
            }
          });
      })
      // console.log($scope.airSensors);
    }


    /**
     * @return {boolean}
     */
    $scope.ValidateDate = function () {
      if(!$scope.dataLoaded)
        return true;
      if ($scope.PreferenceData.SOURCE == 'URL') {
        return true;
      }
      return $scope.PreferenceData.DAY.match(/^(\d{1,2})\/(\d{1,2})\/(\d{2,4})$/) != null;
    };

    /**
     * @return {boolean}
     */
    $scope.ValidateTime = function () {
      if(!$scope.dataLoaded)
        return true;
      if ($scope.PreferenceData.SOURCE == 'URL') {
        return true;
      }
      if ($scope.PreferenceData.CLK24A == 'AMPM') {
        return $scope.PreferenceData.MIN.match(/^(\d{1,2}):(\d{2})([ap]m)$/) != null;
      } else {
        return $scope.PreferenceData.MIN.match(/^(\d{1,2}):(\d{2})$/) != null;
      }
    };

    $scope.CancelPreferencesEdit = function () {
      $scope.PreferenceData.MODE = savedData.Mode;
      $scope.PreferenceData.AVAIL = savedData.Avail;
      $scope.PreferenceData.SHOMNU = savedData.ShoMnu;
      $scope.PreferenceData.MANOVR = savedData.ManOver;
      $scope.PreferenceData.CLK24A = savedData.LocationModel.CLK24A;
      $scope.PreferenceData.SOURCE = savedData.Source;
      $scope.PreferenceData.TEMPNC = savedData.TEMPNC;
      $scope.airSensors = savedData.AirSensors;
      $scope.solarSensors = savedData.SolarSensors;
      $scope.waterSensors = savedData.WaterSensors;
      // $rootScope.editMode.general.AdvancedSettings = false;
      $rootScope.forms.general.advanced.$setPristine();
      $scope.successCancel();
    };

    $scope.ToggleSelectAllRecipients = function () {
      $scope.SelectAllRecipients = !$scope.SelectAllRecipients;
      angular.forEach($scope.NotificationRecipients, function (recipient) {
        recipient.Selected = $scope.SelectAllRecipients;
      });
      $scope.UpdateSelectedRecipients();
    };

    $scope.UpdateSelectedRecipients = function () {
      $scope.ReciepientsSelected = false;
      angular.forEach($scope.NotificationRecipients, function (recipient) {
        if (recipient.Selected) {
          $scope.ReciepientsSelected = true
        }
      });
    };

    $scope.ToggleEditMode = function (section) {
      switch (section) {
        case 'LocationInfo':
          resetLocationData();
          break;
        case 'PersonalInfo':
          resetInstallationNames();
          break;
        case 'AdvancedSettings':
          saveSensorsBeforeEdit();
          break;
        case 'NewRecipient':
          $scope.newRecipientEdit = true;
          resetNewRecipient();
          break;
        case 'StatusMessageFlags':
          resetStatusMessageFlags();
          break;
        case 'Security':
          break;
      }
      $rootScope.editMode.general[section] = !$rootScope.editMode.general[section];
    };

    $scope.SaveStatusMessageFlags = function () {
      var args = {
        CIRCUIT: $scope.selectedStatusMessageTypes.Circuit.join(''),
        PUMP: $scope.selectedStatusMessageTypes.Pump.join(''),
        ICHLOR: $scope.selectedStatusMessageTypes.IntelliChlor.join(''),
        ICHEM: $scope.selectedStatusMessageTypes.IntelliChem.join(''),
        ULTRA: $scope.selectedStatusMessageTypes.HeaterUltraT.join(''),
        MASTER: $scope.selectedStatusMessageTypes.HeaterMasterT.join(''),
        HCOMBO: $scope.selectedStatusMessageTypes.HeaterHComboT.join('')
      };
      propertyService.SetCommand('SetStatusMessageFlags', args).then(function () {
        // $rootScope.editMode.general.StatusMessageFlags = false;
        propertyService.GetPropertyStatusMessageFlags();
        $scope.successSave();
      });
    };

    $scope.ClearStatusMessageFlags = function () {
      $scope.selectedStatusMessageTypes = {
        Circuit: [],
        Pump: [],
        IntelliChlor: [],
        IntelliChem: [],
        HeaterUltraT: [],
        HeaterMasterT: [],
        HeaterHComboT: [],
      }
    };

           
    $scope.SendLogsMessage = function (command) {
        propertyService.logsCommands(command);
    };


    $scope.SelectAllStatusMessageFlags = function () {
      $scope.selectedStatusMessageTypes = {
        Circuit: $scope.StatusMessageTypeFlags.Circuit.map(getKey),
        Pump: $scope.StatusMessageTypeFlags.Pump.map(getKey),
        IntelliChlor: $scope.StatusMessageTypeFlags.IntelliChlor.map(getKey),
        IntelliChem: $scope.StatusMessageTypeFlags.IntelliChem.map(getKey),
        HeaterUltraT: $scope.StatusMessageTypeFlags.HeaterUltraT.map(getKey),
        HeaterMasterT: $scope.StatusMessageTypeFlags.HeaterMasterT.map(getKey),
        HeaterHComboT: $scope.StatusMessageTypeFlags.HeaterHComboT.map(getKey)
      }
    };


    $scope.IsDevicePresent = function (name) {
        var count = 0;
       
        switch (name) {
            case 'intellichlor':            
                count = $scope.getCountChemistry('ICHLOR');
                break;

            case 'intellichem':
                count = $scope.getCountChemistry('ICHEM');
                break;

            case 'pumps':
                if ($scope.localpropertyModel.Pumps) {
                    angular.forEach($scope.localpropertyModel.Pumps, function (pump) {                       
                        if (pump.SUBTYP) {
                            if ((pump.SUBTYP === 'SPEED') || (pump.SUBTYP === 'FLOW') || (pump.SUBTYP === 'VSF'))
                                count = 1;
                        }
                    });
                }
                break;

            case 'heaters':     // ULTRA HEATER
                if ($scope.localpropertyModel.Heaters) {
                    angular.forEach($scope.localpropertyModel.Heaters, function (heater) {
                        if (heater.SUBTYP && heater.SUBTYP === 'ULTRA')
                            count = 1
                    });
                }
                break;

            case 'heatersMaster':   // MAX-E-THERM  OR MASTERTEMP HEATER
                if ($scope.localpropertyModel.Heaters) {
                    angular.forEach($scope.localpropertyModel.Heaters, function (heater) {
                        if (heater.SUBTYP && 
                            ((heater.SUBTYP === 'MASTER') || (heater.SUBTYP === 'MAX'))
                            )
                        count = 1
                    });                        
                }
                break;
            case 'heatersHCombo':   // HYBRID HEATER
                if ($scope.localpropertyModel.Heaters) {
                    angular.forEach($scope.localpropertyModel.Heaters, function (heater) {
                        if (heater.SUBTYP && heater.SUBTYP === 'HCOMBO')
                            count = 1
                    });
                }
                break;

            default:
                count = 0;
                break;
       }
       
        if (count > 0)
            return true;
        else
            return false;              
    };

    $scope.getCountChemistry = function (type) {
        var count = 0;
        if ($scope.localpropertyModel.Chemistry) {
            angular.forEach($scope.localpropertyModel.Chemistry, function (chem) {
                if (chem.SUBTYP === type) {
                    count = count + 1;
                }
            });
        }

        return count;
    };

    $scope.getCountChem = function (type) {
        var count = 0;
        angular.forEach($scope.chems, function (chem) {
            if (chem.SUBTYP === 'ICHLOR') {    
                count = count + 1;
            }

            if (chem.SUBTYP === 'ICHEM') {     
                count = count + 1;
            }
        });

        return count;
    };

    $scope.getMaxRoles = function () {
        return 7;
    }

    $scope.getFreeRoles = function () {
        var roles = 0
 
        if ($scope.securityModel) {
            if ($scope.securityModel.length)
                roles = $scope.securityModel.length-2
        }        

        return roles
    }

    $scope.canDisplayRoles = function () {
        var status = false
        if ( !$scope.securityModel || $scope.securityModel.length === 0)  // too early to display
            return status;  

        if ( ($scope.securityModel[1].ENABLE === 'ON') && ( $scope.getFreeRoles() < $scope.getMaxRoles()  )  )
            status= true

        return status
    }
    
      /*********** SECURITY MODE July-18-2018 ********
      VacationMode		,  v        done 
      Support  		    ,  o        NA    
      GeneralSettings		,  g        done  , under testing
      AlertsNotification	,  q        done
      UserPortal		    ,  i        ONLY  ADMIN : THW
      Groups			    ,  k        done 
      Remotes			    ,  w        Removed     *** this option has been removed July-2018 : replace 'w' to 'a' option as per steve recomendation
      AdvancedSettings	,  a        done   , under testing
      Chemistry		    ,  c        done
      Status			    ,  t        done
      DeleteStatus		,  r        Removed    *** this option has been removed July-2018 : replace 'r' to 't' option as per steve recomendation
      Schedules		    ,  e        done
      Features		    ,  f        done
      Lights			    ,  l        done
      Pool			    ,  p        done
      PoolHeatSource		,  h        **
      PoolTemp		    ,  P        **
      Spa			        ,  m        done
      SpaHeatSource		,  n        **
      SpaTemp			    ,  S        **
      History			    ,  u        done
      MultiBodyDrawer		,  b        NA
      ServiceMode		    ,  x        done 
      SystemConfig		,  C        done 
      ********************************/

    $scope.isSecurityEnabled = function () {

        if ($scope.ACL) {    // ckecking if ACL exist
            if ($scope.ACL.OBJNAM) {    // ckecking if objnam exist
                if ($scope.ACL.OBJNAM === "U0000") {   // checking GUEST IS SIGNED
                    return true //security is enabled by GUEST user OR HAS NO RIGTHS ...
                }
            }
        }

        if ($scope.PropertyModel && ($scope.PropertyModel.SecurityEnabled === true))     
            return true
        else
            return false
    }

    $scope.getProbeFormat = function () {
        switch ($scope.PreferenceData.MODE) {
            case "METRIC":                
                return "C";
            case "ENGLISH":
            default:
                return "F";                        
        }
        return "F";
    }

    $scope.isAllowBySecurity = function (section) {

        var status = true

        // check security is not enable
        if ($scope.isSecurityEnabled() === false) {
            return true // no security is required.
        }
                
        switch (section){
            case  'SECTION_SECURITY':                                
                //status = false          // hide this tab is security is enabled
                if ($scope.SHOMNU.indexOf('x') >= 0)
                    status = true
                else
                    status = false
                break

            case 'SECTION_VACATION_MODE':
                if ($scope.SHOMNU.indexOf('v') >= 0)
                    status = true
                else
                    status = false
                break


            case 'SECTION_NOTIFICATIONS':
                if ($scope.SHOMNU.indexOf('q') >= 0)
                    status = true
                else
                    status = false
                break

            case 'SECTION_ADVANCED_SETTINGS':
                if($scope.SHOMNU.indexOf('a') >= 0)
                    status = true
                else
                    status = false
                break

            case 'SECTION_GENERAL_SETTINGS':
                if ($scope.SHOMNU.indexOf('g') >= 0)
                    status = true
                else
                    status = false
                break
                            
            default:

                 break
                
        }

        return status
    }

    $scope.CancelStatusMessageFlagEdit = function () {
      resetStatusMessageFlags();
      // $rootScope.editMode.general.StatusMessageFlags = false;
      $scope.successCancel();
    };

    $scope.filterAirSingleton = function(sensor) {
      return sensor.OBJNAM !== '_A135'
    }
    // Local functions

    function getKey(item) {
      return item.key;
    }

    function resetNewRecipient() {
      $scope.NewRecipient = {
        PropertyId: propertyModel.SelectedInstallation.toString(),
        Name: '',
        Phone1: '',
        EmailAddress1: '',
        Phone2: '',
        EmailAddress2: '',
        UserId: '0'
      };
      if ($scope.forms.newRep) {
        $rootScope.forms.general.newRep.$setPristine();
        $rootScope.forms.general.newRep.$setUntouched();
      }
    }

    function resetSecurity() {
      $scope.securityModel = angular.copy(propertyModel.Security);
      angular.forEach($scope.securityModel, function(securityModelItem) {
        securityModelItem.confirmPASSWRD = securityModelItem.PASSWRD;
        securityModelItem.accessCheckBoxModel = $scope.mapSHOMNUtoAccess(securityModelItem.SHOMNU);
      });
      // $scope.Security.DisplayPIN = false;
    }

    function resetTime() {
        if (!$rootScope.forms.general.advanced.$dirty) {
            if (propertyModel.Objects["_C10C"]) {   // check exist
                $scope.PreferenceData.DAY = $filter('translateDate')(propertyModel.Objects["_C10C"].DAY);
                $scope.PreferenceData.MIN = $filter('translateTime')(propertyModel.Objects["_C10C"].MIN);
            }
      }
    }


    $scope.formatTime = function (value) {
        $scope.PreferenceData.CLK24A = value;
        $scope.localpropertyModel.Objects._C10C.CLK24A = value;        
        $scope.PreferenceData.MIN = $filter('translateTime')(propertyModel.Objects["_C10C"].MIN);
    }

    function resetStatusMessageFlags() {
        $scope.selectedStatusMessageTypes = angular.copy(propertyModel.PropertyStatusMessageFlags); //angular.copy(propertyModel.PropertyStatusMessageFlags, $scope.selectedStatusMessageTypes);
    }

    function saveLongLat() {
      var params = {
        LOCX: $scope.LocationModel.LOCX,
        LOCY: $scope.LocationModel.LOCY,
        ZIP: $scope.AddressModel.ZIP,
        CLK24A: $scope.timeData.CLK24A,
        TIMZON: $scope.AddressModel.TIMZON.toString(),
        DLSTIM: $scope.timeData.DLSTIM ? 'ON' : 'OFF'
      };

      //TODO- this should not need to be set.
      $scope.LocationModel.SNAME = params.SNAME;
      propertyService.SetParams("_C10C", params);
    }

    function resetNotificationUsers() {
        $scope.NotificationRecipients = angular.copy(propertyModel.NotificationRecipients); //angular.copy(propertyModel.NotificationRecipients, $scope.NotificationRecipients);
    }

    function resetInstallationNames() {
        if (propertyModel.SystemInformation.PoolName)
            $scope.installationNames.PropertyName = propertyModel.SystemInformation.PoolName;
        if (propertyModel.Objects['_5451']) {   // check if objects arrives 
            if (propertyModel.Objects['_5451'].NAME)
                $scope.installationNames.OwnerName = propertyModel.Objects['_5451'].NAME;
            if (propertyModel.Objects['_5451'].PHONE)
                $scope.installationNames.OwnerPhone = propertyModel.Objects['_5451'].PHONE;
            if (propertyModel.Objects['_5451'].EMAIL)
                $scope.installationNames.OwnerEmail = propertyModel.Objects['_5451'].EMAIL;
        }
    }

    // Location Editor
    function resetLocationData() {
        
      $scope.LocationModel = angular.copy(savedData.LocationModel);   //angular.copy(savedData.LocationModel, $scope.LocationModel);
      $scope.AddressModel = angular.copy(savedData.AddressModel); //angular.copy(savedData.AddressModel, $scope.AddressModel);
      $scope.timeData.DLSTIM = $scope.LocationModel.DLSTIM !== 'OFF';
      $scope.timeData.TimeZone = $scope.timeZones[Number($scope.AddressModel.TIMZON) + 12].VALUE;
      setTimeZone($scope.timeData.TimeZone)
      
     }


    function setTimeZone(name) {
        $scope.locationTimeZone = name
    }

    function buildLocationModel() {
      savedData.LocationModel = angular.copy($scope.LocationModel); //angular.copy($scope.LocationModel, savedData.LocationModel);
      savedData.AddressModel = angular.copy($scope.AddressModel); //angular.copy($scope.AddressModel, savedData.AddressModel);
    }

    function timezoneSelected(value) {
      $scope.AddressModel.TIMZON = value;
      $scope.timeData.TimeZone = $scope.timeZones[12 + Number(value)].VALUE;
      
      setTimeZone($scope.timeData.TimeZone)
    }

    function init() {
      resetStatusMessageFlags();
      resetNotificationUsers();
      resetInstallationNames();
      processTimeZones();
      buildLocationModel();

      var preference = propertyModel.Objects._5451;
      if (preference) {
          if (preference.MODE.toUpperCase() === "ENGLISH") {
              $scope.UoM = 'F';
          }
      }

      updateChemistry();

    }
    
    
    // chemistry
    function updateChemistry() {
        propertyService.getChemistries().then(function (chemistries) {
            $scope.chems = chemistries.objectList.map(function (item) {
            });    
        });
    }

      // UPDATE SECURITY
    function updateSupervisorSecurity() {
        if ($scope.securityModel) {
            angular.forEach($scope.securityModel, function (item) {
                // admin user
                if (item.SNAME === "Administrator") {
                    if (item.ENABLE) {
                        if (item.ENABLE === 'ON')
                            $scope.securitySupervisor = 'ON'
                        else
                            $scope.securitySupervisor = 'OFF'
                    }
                }
                // Guess user
                if (item.SNAME === "Guest") {
                    if (item.ENABLE) {
                        if (item.ENABLE === 'ON')
                            $scope.securityGuess = 'ON'
                        else
                            $scope.securityGuess = 'OFF'
                    }
                }
            });
        }
    };
      // security

    function updateChemistry() {
        propertyService.getChemistries().then(function (chemistries) {
            $scope.chems = chemistries.objectList.map(function (item) {
            });    
        });
    }


    //sensors

    function processTimeZones() {
      var zones = [];
      for (var i = -12; i <= 12; i++) {
        var zone = "";
        zone = i;
        if (i >= 0) {
          zone = '+' + i;
        }
        var newObject = {
          ZONE: zone,
          VALUE: $filter("translate")("ZT" + zone)
        };
        zones.push(newObject);
      }
      $scope.timeZones = zones;

      if ($scope.AddressModel && $scope.AddressModel.TIMZON)
          $scope.timeData.TimeZone = $scope.timeZones[Number($scope.AddressModel.TIMZON || Enumerations.DefaultSelectedTimezone.ZONE) + 12].VALUE;
      else
          $scope.timeData.TimeZone = $scope.timeZones[Number(Enumerations.DefaultSelectedTimezone.ZONE) + 12].VALUE;

      setTimeZone($scope.timeData.TimeZone)
    }

    function saveSensorsBeforeEdit() {
      savedData.AirSensors = angular.copy($scope.airSensors);
      savedData.SolarSensors = angular.copy($scope.solarSensors);
      savedData.WaterSensors = angular.copy($scope.waterSensors);
    }

  }
]);
