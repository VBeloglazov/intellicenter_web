﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pentair.Web.Api
{
    /// <summary>
    /// Must match PReleaseParamList/TSubscription.
    /// </summary>
    public class TSubscription
    {
        public Guid ClientId { get; set; }
        public int PropertyID { get; set; }
        public string ObjNam { get; set; }
        public string ObjParm { get; set; }

    } // class TSubscription
}