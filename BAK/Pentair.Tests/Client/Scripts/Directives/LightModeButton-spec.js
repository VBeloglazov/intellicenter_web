﻿describe('lightModeButton', function () {
    var $scope, $compile;
    var elm;
    var baseTemplate = '<light-mode-button btn-class="color blue" ng-click="SetLightMode()" light-mode="Blue"></light-mode-button>';
    var spinner;
    var elmScope;
    var $interval;
    beforeEach(module('PentairWebClient'));

    beforeEach(inject(function ($rootScope, _$compile_, _$interval_) {
        $scope = $rootScope.$new();
        $compile = _$compile_;
        $interval = _$interval_;
        $scope.SetLightMode = jasmine.createSpy();

        elm = SpecHelper.createDirective($compile, baseTemplate, $scope);
        spinner = $(elm[0].lastChild);
        elmScope = elm.isolateScope();

        jasmine.clock().install();
    }));

    afterEach(function() {
        jasmine.clock().uninstall();
    });

    describe('show spinner', function () {
        it('should set show spinner to true', function () {
            elm.triggerHandler('click');
            expect(elmScope.showSpinner).toBe(true);
            expect($scope.SetLightMode).toHaveBeenCalled();
        });
        it('should show spinner on click', function () {
            elm.triggerHandler('click');
            $scope.$apply();
            expect(spinner).not.toHaveClass('ng-hide');
        });

        it('should hide spinner after 8 seconds', function () {
            elm.triggerHandler('click');
            $scope.$apply();
            $interval.flush(8001);
            expect(elmScope.showSpinner).toBe(false);
        });
    });


});