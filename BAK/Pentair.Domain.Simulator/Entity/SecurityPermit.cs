﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pentair.Domain.Simulator.Entity
{
    public class SecurityPermit
    {
        [Key]
        public int ID { get; set; }
        public string Name { get; set; }
        public UInt32 LastRevision { get; set; }
        public bool AdvancedPassword { get; set; }
        public bool SupervisorPassword { get; set; }

        // Foreign Keys
        public int TypeID { get; set; }
        public int UserAccessLevelID { get; set; }  // TODO Getter and setter have different functionality
    }
}