﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Pentair.Domain.Client.Helpers.Commands
{
    public class ClearHistoryMessageResponse : CommandWithStartAndEndTime
    {
        public ClearHistoryMessageResponse(
            string messageID,
            string response,
            string timeStart,
            string timeEnd
            )
            : base(CommandString.ClearHistoryMessage, messageID, timeStart, timeEnd)
        {
        } // ClearHistoryMessageResponse()

        /// <summary>
        /// Always returns an object, valid or not. If invalid, check the DeserializationError property value.
        /// </summary>
        public static new ClearHistoryMessageResponse Deserialize(
            string serializedMessage)
        {
            ClearHistoryMessageResponse command = null;

            try
            {
                command = JsonConvert.DeserializeObject<ClearHistoryMessageResponse>(serializedMessage);
                command.IsValid = true;
            }
            catch (Exception e)
            {
                // Deserialization as ClearParamResponse failed

                StringBuilder sbError = new StringBuilder();

                sbError.Append("Cannot deserialize as ClearHistoryMessageResponse. " + e.Message);

                // Try to get at least the base message data
                ResponseCommand baseCommand = ResponseCommand.Deserialize(serializedMessage);

                if (!baseCommand.IsValid)
                {
                    sbError.Append("\n" + baseCommand.DeserializationError);
                }

                command = new ClearHistoryMessageResponse(
                    messageID: baseCommand.MessageID,
                    response: ResponseString.BadRequest,
                    timeStart: null,
                    timeEnd: null)
                {
                    IsValid = false,
                    DeserializationError = sbError.ToString()
                };
            }

            return command;

        } // Deserialize()

    } // class ClearHistoryMessageResponse
}
