﻿describe('InstallationsController', function () {
    var Controller;
    var $scope;
    var $controller;
    var $httpBackend;
    var PropertyService;
    var SocketService;
    var PropertyModel;
    var $location;
    beforeEach(module('PentairWebClient'));

    beforeEach(inject(function (_$controller_, _$rootScope_, _$httpBackend_, _$location_, _PropertyService_, _SocketService_, _PropertyModel_) {
        $scope = _$rootScope_.$new();
        $controller = _$controller_;
        $httpBackend = _$httpBackend_;
        PropertyService = _PropertyService_;
        SocketService = _SocketService_;
        PropertyModel = _PropertyModel_;
        $location = _$location_;
        PropertyService.Ready = true;

        PropertyService.Ready = false;
        var routeParams = {
            current: {
                locals: {
                    installations: { data: SpecHelper.mocks.Installations }
                }
            }
        }
        Controller = $controller("InstallationsController", { $scope: $scope, $route: routeParams });
        spyOn(PropertyModel, 'Clear');
        spyOn(PropertyService, 'Connect');
    }));
    describe('default vars', function () {
        it('should have the followinf default values', function () {
            expect($scope.Property).toBe(PropertyService);
        });
    });

    describe('Property Service Ready', function () {
        beforeEach(function () {
            PropertyModel.Ready = true;
            Controller = $controller("InstallationsController", { $scope: $scope });
        });
        describe('Init', function () {
            it('should not restart if property service is loaded/ready', function () {
                expect(PropertyModel.Clear).not.toHaveBeenCalled();
            });
        });
    });

    describe('Property Service Not Ready and Socket OPEN', function () {
        beforeEach(function () {
            PropertyModel.Ready = false;
            SocketService.socket = { status: 'Connected' };
            PropertyModel.Installations = [
                {
                    "InstallationId": "1f92ba9c-077a-4cac-bdd2-1dee3579911a",
                    "PoolName": "Dashons pool 1",
                    "OnLine": true
                }
            ];
            sessionStorage.selectedInstallation = '{"property": "CACHED","name": "Dashons pool 1","connected": true}';

            Controller = $controller("InstallationsController", { $scope: $scope });
        });

        describe('Init', function () {
            it('should reset the property Service and auto connect to the users only installation', function () {
                expect(PropertyModel.Clear).toHaveBeenCalled();
            });

            it('should Reload from cache', function () {
                expect(PropertyModel.SelectedInstallation.property).toBe("CACHED");
            });
        });
    });

    describe('Property Service Not Ready and Socket empty', function () {
        beforeEach(function () {
            PropertyModel.Ready = false;
            SocketService.socket = {};
            PropertyModel.Installations = [
                {
                    "InstallationId": "1f92ba9c-077a-4cac-bdd2-1dee3579911a",
                    "PoolName": "Dashons pool 1",
                    "OnLine": true
                }
            ];
            sessionStorage.selectedInstallation = '';

            Controller = $controller("InstallationsController", { $scope: $scope });
        });

        describe('Init', function () {
            it('should reset the property Service and auto connect to the users only installation', function () {
                expect(PropertyModel.Clear).toHaveBeenCalled();
                expect(PropertyModel.SelectedInstallation).toBe("1f92ba9c-077a-4cac-bdd2-1dee3579911a");
                expect(PropertyService.Connect).toHaveBeenCalled();
            });

        });
    });

    //describe('HidePropertySelector', function () {
    //    var routeParams;
    //    beforeEach(function () {
    //        PropertyModel.Ready = false;
    //        SocketService.socket = {};
    //        PropertyModel.Installations = [
    //            {
    //                "Installation": "1f92ba9c-077a-4cac-bdd2-1dee3579911a",
    //                "PoolName": "Dashons pool 1",
    //                "OnLine": true
    //            }
    //        ];
    //        sessionStorage.selectedInstallation = '';

    //        routeParams = {
    //            current: {
    //                controller: "LoginController",
    //                locals: {
    //                    installations: { data: SpecHelper.mocks.Installations }
    //                }
    //            }
    //        }
    //    });
    //    it('should hideproperty selector if on the  login Page', function () {
    //        routeParams.current.controller = "LoginController";
    //        Controller = $controller("InstallationsController", { $scope: $scope, $route: routeParams });
    //        expect($scope.HidePropertySelector()).toBe(true);
    //    });
    //    it('should hideproperty selector if on the installations Page', function () {
    //        routeParams.current.controller = "InstallationsController";
    //        Controller = $controller("InstallationsController", { $scope: $scope, $route: routeParams });
    //        expect($scope.HidePropertySelector()).toBe(true);
    //    });
    //    it('should hideproperty selector if on the acceptInvite Page', function () {
    //        routeParams.current.controller = "AcceptInviteController";
    //        Controller = $controller("InstallationsController", { $scope: $scope, $route: routeParams });
    //        expect($scope.HidePropertySelector()).toBe(true);
    //    });
    //});

});