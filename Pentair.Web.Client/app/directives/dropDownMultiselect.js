﻿'use strict';
var PWC = PWC || {};
PWC.directive('dropdownMultiselect', function () {
    return {
        restrict: 'A',
        scope: {
            options: '=',
            label: '@',
            onSelectionsChanged: '=',
            model: '=dropdownMultiselect',
            ngDisabled: '='
        },
        template: "<div on-click-outside='handelClickOutside(event)' class='btn-group' data-ng-class='{open: open}' dropdown>" +
                    "<button dropdown-toggle class='btn drodown-btn' ng-disabled='ngDisabled'>" +
                        "<span class='pull-right chevron'><span class='glyphicon glyphicon-chevron-right'></span></span>" +
                        "{{label}} {{numSelected()}}" +
                    "</button>" +
                 "<ul class='dropdown-menu'>" +
                    "<li class='noselect'><a class='' ng-click='toggleAll($event)'><span class='left check' ng-class='isAllChecked()'>&nbsp</span><strong>All {{label}} Messages</strong></a></li>" +
                     //"<li><a data-ng-click='deselectAll($event);'`><i class='glyphicon glyphicon-remove-sign'></i>  Uncheck All</a></li>" +
                     //"<li class='divider'></li>" +
                     "<li class='noselect' ng-repeat='option in options'> <a ng-click='setSelectedItem($event)'><span class='left check' ng-class='isChecked(option.key)'>&nbsp</span>{{option.value | translate}}</a></li>" +
                 "</ul>" +
             "</div>",
        controller: function ($scope) {
            $scope.model = $scope.model || [];

            $scope.handelClickOutside = function (event) {
                if ($scope.open) {
                    $scope.open = false;
                    if ($scope.onSelectionsChanged) {
                        $scope.onSelectionsChanged($scope.model);
                    }
                    $scope.$apply();
                }
            };

            $scope.toggleDropdown = function () {
                if (!$scope.ngDisabled) {
                    if ($scope.open) {
                        $scope.open = false;
                        if ($scope.onSelectionsChanged) {
                            $scope.onSelectionsChanged($scope.model);
                        }
                    }
                    else {
                        $scope.open = true;
                    }
                } else {
                    $scope.open = false;
                }
            };

            $scope.toggleAll = function (event) {
                event.stopPropagation();

                if (!$scope.ngDisabled) {
                    if ($scope.isAllChecked()) {
                        $scope.deselectAll();
                    } else {
                        $scope.selectAll();
                    }
                }
            }

            $scope.selectAll = function () {
                $scope.model = pluck($scope.options, 'key');
            };
            $scope.deselectAll = function () {
                $scope.model = [];
            };
            $scope.setSelectedItem = function (event) {
                event.stopPropagation();

                if (!$scope.ngDisabled) {
                    var key = this.option.key;
                    var existingPosition = $scope.model.indexOf(key);
                    if (existingPosition > -1) {
                        $scope.model.splice(existingPosition, 1);
                    } else {
                        $scope.model.push(key);
                    }
                }
                return false;
            };
            $scope.isChecked = function (key) {
                if ($scope.model.indexOf(key) > -1) {
                    return 'glyphicon glyphicon-ok';
                }
                return false;
            };

            $scope.isAllChecked = function () {
                if ($scope.model.length === $scope.options.length) {
                    return 'glyphicon glyphicon-ok';
                }
                return false;
            }
            $scope.numSelected = function () {
                if ($scope.isAllChecked()) {
                    return '(All)'
                } else {
                    return '(' + $scope.model.length + ':' + $scope.options.length + ')';
                }
            }
            function pluck(arr, key) {
                return arr.map(function (e) { return e[key]; });
            }
        }
    }
});