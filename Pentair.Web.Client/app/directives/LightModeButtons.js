﻿'use strict';
var PWC = PWC || {};
PWC.directive('lightModeButton', function ($interval) {
    return {
        restrict: 'EA',
        replace: true,
        transclude: true,
        scope: {
            disabled: '@',
            lightMode: '@',
            btnClass: '@'
        },
        template: '<div><button class="{{btnClass}}">{{lightMode}}</button> <div ng-show="showSpinner" class="spinner"/></div>',
        link: function (scope, element, attrs) {
            if (!attrs.disabled) { attrs.disabled = false; }
            scope.disabled = attrs.disabled;
            scope.showSpinner = false;
            element.on('click', function (e) {
                e.stopPropagation();
                if (!scope.disabled) {
                    scope.showSpinner = true;

                    $interval(function () {
                        scope.showSpinner = false;
                    }, 8000);
                }
            });

        }
    };
});
