/**
 * Created by Adam Carnagey on 5/11/16.
 */
'use strict';
describe('ConfigureBodiesController', function () {
  var $scope, $location, $interpolate, PropertyService, propertyModel, Enumerations, $filter,
    ConfigureBodiesController, $route, $controller, messageProcessor, appService, $httpBackend, secretEmptyKey,
    deferred, $q;

  beforeEach(function () {
    module('PentairWebClient');
    // Provide will help us create fake implementations for our dependencies
    module(function ($provide) {
      // Fake PropertyService Implementation returning a promise
      $provide.value('PropertyService', {
        SetParams: function () {
          return {
            then: function (callback) {
              return callback([{some: 'thing'}]);
            }
          };
        },
        getUpdatedBody: function () {
          return {
            then: function (callback) {
              return callback([{someOther: 'thingy'}]);
            }
          };
        }
      });
      return null;
    });
  });

  beforeEach(function () {

    // When Angular Injects the PropertyService dependency,
    // it will use the implementation we provided above
    inject(function (_$controller_, _$httpBackend_, _$rootScope_, _PropertyService_, _PropertyModel_,
                     _$location_, _$filter_, _$q_, _SocketService_, _$route_, _MessageProcessor_, _AppService_, _$interpolate_,
                     _Enumerations_) {
      $scope = _$rootScope_.$new();
      PropertyService = _PropertyService_;
      propertyModel = _PropertyModel_;
      $location = _$location_;
      $filter = _$filter_;
      $controller = _$controller_;
      $route = _$route_;
      messageProcessor = _MessageProcessor_;
      $httpBackend = _$httpBackend_;
      appService = _AppService_;
      $interpolate = _$interpolate_;
      Enumerations = _Enumerations_;
      secretEmptyKey = '[$empty$]';
      $scope.$parent.Layout = 'layout-base';
      $scope.property = propertyModel;
      $scope.accordionStateByObjectNames = {};
      $q = _$q_;
      deferred = $q.defer();

      propertyModel.Bodies = [{
        "OBJNAM": "BCD5C",
        "SNAME": "Spa!",
        "TEMP": "75",
        "HITMP": "104",
        "LOTMP": "98",
        "LISTORD": "2",
        "OBJTYP": "BODY",
        "SUBTYP": "SPA",
        "HEATER": "H6246",
        "SHARE": "B7A8E",
        "PRIM": "C868B",
        "SEC": "CE0B1",
        "ACT1": "C868B",
        "ACT2": "CE0B1",
        "ACT3": "C868B",
        "ACT4": "C868B",
        "VOL": "61",
        "HEATERS_debug": [{
          "OBJNAM": "H6246",
          "OBJTYP": "HEATER",
          "SNAME": "Gas Heater 1",
          "PERMIT": "PERMIT",
          "STATUS": "INCR",
          "SUBTYP": "GENERIC",
          "READY": "READY",
          "BODY": "B7A8E",
          "SHARE": "BCD5C",
          "TIMOUT": "0",
          "HTMODE": "GENERIC",
          "SHOMNU": "had"
        }],
        "STATUS": "ON",
        "HTSRC": "000FF",
        "MANUAL": "ON",
        "LSTTMP": "75"
      }, {
        "OBJNAM": "B7A8E",
        "SNAME": "Pool",
        "TEMP": "75",
        "HITMP": "100",
        "LOTMP": "82",
        "LISTORD": "1",
        "OBJTYP": "BODY",
        "SUBTYP": "POOL",
        "HEATER": "H6246",
        "SHARE": "BCD5C",
        "PRIM": "00000",
        "SEC": "00000",
        "ACT1": "00000",
        "ACT2": "00000",
        "ACT3": "00000",
        "ACT4": "00000",
        "VOL": "0",
        "HEATERS_debug": [{
          "OBJNAM": "H6246",
          "OBJTYP": "HEATER",
          "SNAME": "Gas Heater 1",
          "PERMIT": "PERMIT",
          "STATUS": "INCR",
          "SUBTYP": "GENERIC",
          "READY": "READY",
          "BODY": "B7A8E",
          "SHARE": "BCD5C",
          "TIMOUT": "0",
          "HTMODE": "GENERIC",
          "SHOMNU": "had"
        }],
        "STATUS": "OVRON",
        "HTSRC": "H6246",
        "MANUAL": "MANUAL",
        "LSTTMP": "75"
      }];

      propertyModel.Circuits = [{
        "OBJNAM": "_FEA2",
        "OBJTYP": "CIRCUIT",
        "STATUS": "OFF",
        "SUBTYP": "FRZ",
        "Delay": {"VISIBLE": false, "MESSAGE": ""}
      }, {
        "OBJNAM": "C868B",
        "OBJTYP": "CIRCUIT",
        "SNAME": "Spa",
        "SHOMNU": "csrepvhzmobw",
        "STATUS": "ON",
        "SUBTYP": "SPA",
        "USAGE": "USAGE",
        "ACT": "ACT",
        "FREEZE": "OFF",
        "Delay": {"VISIBLE": false, "MESSAGE": ""},
        "MODE": "MODE",
        "LISTORD": "-1",
        "LIMIT": "LIMIT",
        "USE": "USE",
        "MANUAL": "OFF",
        "FEATR": "ON",
        "DNTSTP": "OFF",
        "CHILD": "CHILD"
      }, {
        "OBJNAM": "CE0B1",
        "OBJTYP": "CIRCUIT",
        "SNAME": "Pool",
        "SHOMNU": "csrepvhzmobw",
        "STATUS": "OVRON",
        "SUBTYP": "POOL",
        "USAGE": "USAGE",
        "ACT": "ACT",
        "FREEZE": "OFF",
        "Delay": {"VISIBLE": false, "MESSAGE": ""},
        "MODE": "MODE",
        "LISTORD": "-1",
        "LIMIT": "LIMIT",
        "USE": "USE",
        "MANUAL": "OFF",
        "FEATR": "ON",
        "DNTSTP": "OFF",
        "CHILD": "CHILD"
      }];

      ConfigureBodiesController = $controller('ConfigureBodiesController', {$scope: $scope});

    });
  });

  // describe('Initial Values', function () {
  //   it('should expect the following initial values ', function () {
  //     expect($scope.cBodies).toEqual([{
  //       "OBJNAM": "BCD5C",
  //       "SNAME": "Spa!",
  //       "TEMP": "75",
  //       "HITMP": "104",
  //       "LOTMP": "98",
  //       "LISTORD": "2",
  //       "OBJTYP": "BODY",
  //       "SUBTYP": "SPA",
  //       "HEATER": "H6246",
  //       "SHARE": "B7A8E",
  //       "PRIM": "C868B",
  //       "SEC": "CE0B1",
  //       "ACT1": "C868B",
  //       "ACT2": "CE0B1",
  //       "ACT3": "C868B",
  //       "ACT4": "C868B",
  //       "VOL": "61",
  //       "HEATERS_debug": [{
  //         "OBJNAM": "H6246",
  //         "OBJTYP": "HEATER",
  //         "SNAME": "Gas Heater 1",
  //         "PERMIT": "PERMIT",
  //         "STATUS": "INCR",
  //         "SUBTYP": "GENERIC",
  //         "READY": "READY",
  //         "BODY": "B7A8E",
  //         "SHARE": "BCD5C",
  //         "TIMOUT": "0",
  //         "HTMODE": "GENERIC",
  //         "SHOMNU": "had"
  //       }],
  //       "STATUS": "ON",
  //       "HTSRC": "000FF",
  //       "MANUAL": "ON",
  //       "LSTTMP": "75"
  //     }, {
  //       "OBJNAM": "B7A8E",
  //       "SNAME": "Pool",
  //       "TEMP": "75",
  //       "HITMP": "100",
  //       "LOTMP": "82",
  //       "LISTORD": "1",
  //       "OBJTYP": "BODY",
  //       "SUBTYP": "POOL",
  //       "HEATER": "H6246",
  //       "SHARE": "BCD5C",
  //       "PRIM": "00000",
  //       "SEC": "00000",
  //       "ACT1": "00000",
  //       "ACT2": "00000",
  //       "ACT3": "00000",
  //       "ACT4": "00000",
  //       "VOL": "0",
  //       "HEATERS_debug": [{
  //         "OBJNAM": "H6246",
  //         "OBJTYP": "HEATER",
  //         "SNAME": "Gas Heater 1",
  //         "PERMIT": "PERMIT",
  //         "STATUS": "INCR",
  //         "SUBTYP": "GENERIC",
  //         "READY": "READY",
  //         "BODY": "B7A8E",
  //         "SHARE": "BCD5C",
  //         "TIMOUT": "0",
  //         "HTMODE": "GENERIC",
  //         "SHOMNU": "had"
  //       }],
  //       "STATUS": "OVRON",
  //       "HTSRC": "H6246",
  //       "MANUAL": "MANUAL",
  //       "LSTTMP": "75"
  //     }]);
  //   })
  // });
  //
  // describe('Cancelling out of a body edit', function () {
  //   beforeEach(function () {
  //     $scope.setEditMode($scope.panels[0].bodies[0]);
  //     $scope.panels[0].bodies[0].sName = 'new name';
  //   });
  //   it('should reset the body to the initial values', function () {
  //     $scope.cancelEdit(0, 0);
  //     expect($scope.panels[0].bodies[0].editMode).toBeFalsy();
  //     expect($scope.panels[0].bodies[0].sName).toBe('Spa!');
  //   });
  // });
  //
  // describe('Saving a single body from a panel', function () {
  //   beforeEach(function () {
  //     $scope.setEditMode($scope.panels[0].bodies[0]);
  //     $scope.panels[0].bodies[0].sName = 'new name';
  //
  //     // Jasmine spy over the SetParams service.
  //     // Since we provided a fake response already we can just call through.
  //     spyOn(PropertyService, 'SetParams').and.callThrough();
  //     spyOn(PropertyService, 'getUpdatedBody').and.callThrough();
  //   });
  //   it('should post to aux update to Server if form is valid', function () {
  //     $scope.saveBody($scope.panels[0].bodies[0]);
  //
  //     expect(PropertyService.SetParams.calls.all()[0].args[0]).toBe('BCD5C');
  //     expect(PropertyService.SetParams.calls.all()[0].args[1].SNAME).toBe('new name');
  //     expect(PropertyService.SetParams).toHaveBeenCalled();
  //     expect(PropertyService.getUpdatedBody).toHaveBeenCalled();
  //
  //     expect($scope.panels[0].bodies[0].editMode).toBeFalsy();
  //   });
  // });
  //
  // describe('show circuit sname from object name', function () {
  //   it('should show sname from object name', function () {
  //     expect($scope.showCircuitSNameFromObjectName('C868B')).toBe('Spa');
  //   });
  // });

});
