﻿using System.Collections.Generic;

using Newtonsoft.Json;

namespace Pentair.Web.Api
{
    public class KeyObject
    {
        [JsonProperty("objnam")]
        public string ObjNam { get; private set; }

        [JsonProperty("keys")]
        public List<string> Keys { get; private set; }

        public KeyObject(
            string objNam,
            List<string> keys)
        {
            this.ObjNam = objNam;
            this.Keys = keys;
        }

    } // class KeyObject
}