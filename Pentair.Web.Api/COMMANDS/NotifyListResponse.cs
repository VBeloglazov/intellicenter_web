﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Pentair.Web.Api
{
    public class NotifyListResponse : ParamCommandWithTimeSpanResponse
    {
        public NotifyListResponse(
            string messageID,
            string response,
            string timeNow,
            string timeSince,
            List<ParamObject> objectList
            )
            : base(CommandString.NotifyList, messageID, response, timeNow, timeSince, objectList)
        {
        } // NotifyListResponse()

        /// <summary>
        /// Always returns an object, valid or not. If invalid, check the DeserializationError property value.
        /// </summary>
        public static new NotifyListResponse Deserialize(
            string serializedMessage)
        {
            NotifyListResponse command = null;

            try
            {
                command = JsonConvert.DeserializeObject<NotifyListResponse>(serializedMessage);
                command.IsValid = true;
            }
            catch (Exception e)
            {
                // Deserialization as NotifyListResponse failed

                StringBuilder sbError = new StringBuilder();

                sbError.Append("Cannot deserialize as NotifyListResponse. " + e.Message);

                // Try to get at least the base message data
                ParamCommandWithTimeSpanResponse baseCommand = ParamCommandWithTimeSpanResponse.Deserialize(serializedMessage);

                if (!baseCommand.IsValid)
                {
                    sbError.Append("\n" + baseCommand.DeserializationError);
                }

                command = new NotifyListResponse(
                    messageID: baseCommand.MessageID,
                    response: ResponseString.BadRequest,
                    timeNow: String.Empty,
                    timeSince: String.Empty,
                    objectList: null)
                {
                    IsValid = false,
                    DeserializationError = sbError.ToString()
                };
            }

            return command;

        } // Deserialize()

    } // class NotifyListResponse
}