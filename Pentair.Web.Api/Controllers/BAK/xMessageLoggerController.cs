﻿using System;
using System.Net.Http;
using System.Web.Http;
using System.Net;
using Pentair.Domain.Client.Helpers.Commands;
using Pentair.Web.Api.Helpers;

using SERVICE_SHARED;

namespace Pentair.Web.Api.Controllers
{
    // TODO: VB - 2b removed?

    public class MessageLoggerController : ApiController
    {
        private const string EVENT_SOURCE = "MessageLoggerController";

        [AllowAnonymous]
        public HttpResponseMessage Post(PostParameters postParameters)
        {
            try
            {
                var userId = User.Identity.GetOwinUserId();
                int hardwareId;
                if (!int.TryParse(postParameters.HardwareId, out hardwareId))
                {
                    hardwareId = -1;
                }

                LogManager.LogWarningAsync(
                    source: EVENT_SOURCE,
                    eventBriefDescription: "MessageLoggerController - in use!",
                    installationId: hardwareId,
                    userId: userId,
                    connectionId: Guid.Empty,
                    eventFullDescription:
                        String.Format("{0}\n\n{1}",
                            postParameters.Action,
                            postParameters.Message ?? "MessageLogger endpoint null message."
                        ),
                    saveEventLog: true
                );

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch(Exception e)
            {
                LogManager.LogErrorAsync(
                    source: EVENT_SOURCE,
                    eventBriefDescription: "MessageLoggerController - in use, but failed!",
                    installationId: 0,
                    userId: Guid.Empty,
                    connectionId: Guid.Empty,
                    eventFullDescription:
                        ExceptionManager.CustomizeException(e).ErrorMessage(
                            errorHeader: null,
                            addStackTrace: true
                        )
                );

                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }
    }
}