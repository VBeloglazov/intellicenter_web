﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using Newtonsoft.Json;
using Pentair.Domain.Client.Helpers;
using Pentair.Domain.Client.Helpers.Commands;
using Pentair.Domain.Client.Infrastructure;
using Pentair.Web.Api.Helpers;

namespace Pentair.Desktop.Simulator.BusinessLogic
{
    public class MessageProcessor
    {
        #region Private Properties
        private readonly string[] _messageQueue = new string[1024];
        #endregion

        #region Public Properties
        public int Property { get; set; }
        public int Configuration { get; set; }
        #endregion

        #region Virtual Methods

        protected virtual ISimulatorContext dBase()
        {
            return new SimulatorContext();
        }
        #endregion

        #region Events and Delegates
        public event OnConfigurationChange OnConfigurationChangeEvent;
        public delegate void OnConfigurationChange(Object sender, string e);
        public event OnDataChange OnDataChangeEvent;
        public delegate void OnDataChange(Object sender, EventArgs e);
        public event LogMessage LogMessageEvent;
        public delegate void LogMessage(string message);
        public event OnCloseRequest OnCloseRequestEvent;
        public delegate void OnCloseRequest(Object sender);

        #endregion

        #region Public Methods
        /// <summary>
        /// Update the system clock
        /// </summary>
        /// <param name="value"></param>
        public void UpdateSystemClock(DateTime value)
        {
            if (Configuration > 0)
            {
                var lastChange = Globals.CurrentTime() - 1;
                using (var data = dBase())
                {
                    var p = new Simulator.BusinessLogic.BusinessLogic(data, Configuration);
                    p.UpdateSystemClock(value);
                    var response = p.GetNotifications();
                    if (null != response.ObjectList && response.ObjectList.Count > 0)
                    {
                        if (OnConfigurationChangeEvent != null)
                            OnConfigurationChangeEvent (this, JsonConvert.SerializeObject(response));
                    }
                    var history = p.GetHistory(0);
                    if (null != history.ObjectList && history.ObjectList.Count > 0)
                    {
                        var msg = JsonConvert.SerializeObject(history);
                        if (null != OnConfigurationChangeEvent)
                        {
                            OnConfigurationChangeEvent(this, JsonConvert.SerializeObject(history));
                        }
                    }
                    var notifications = p.GetWarningAndAlertChanges(lastChange.ToString());
                    if (null != notifications.ObjectList && notifications.ObjectList.Count > 0 &&
                        null != OnConfigurationChangeEvent)
                    {
                        if (notifications.ObjectList[0].Changes.Count > 0 ||
                            notifications.ObjectList[0].Deleted.Count > 0)
                        {
                            OnConfigurationChangeEvent(this, JsonConvert.SerializeObject(notifications));
                        }
                    }
                    if (null != OnDataChangeEvent)
                    {
                        OnDataChangeEvent(this, new EventArgs());
                    }
                }
            }
        }

        public void SendMessage(string message)
        {
            if (null != OnConfigurationChangeEvent)
            {
                OnConfigurationChangeEvent(this, message);
            }
        }

        /// <summary>
        /// Clear all subscriptions for the given client on the given property
        /// </summary>
        /// <param name="client">Client Id</param>
        public void ClearSubscriptions(Guid client)
        {
            using (var data = dBase())
            {
                var subscription = (from c in data.Subscriptions
                                    where c.ClientId == client &&
                                          c.PropertyId == Property
                                    select c).ToList();
                foreach (var item in subscription)
                {
                    data.Subscriptions.Remove(item);
                }
                data.SaveChanges();
            }
        }

        /// <summary>
        /// Parse and determine the incomming message.
        /// </summary>
        /// <param name="message">Message to process</param>
        /// <param name="client"></param>
        /// <param name="property"></param>
        /// <returns></returns>
        public string ProcessMessage(string message, Guid client)
        {
            // Parse the message type
            var messageType = WebSocketMessage.ParseMessageType(message, EndpointType.Server);

            switch (messageType.CommandType)
            {
                case CommandType.CreateObject:
                    var result = CreateObject(message, messageType);
                    if (null != OnDataChangeEvent)
                    {
                        OnDataChangeEvent(this, new EventArgs());
                    }
                    return (result);
                //break;
                case CommandType.GetParamList:
                    return (GetParms(message, messageType));
                //break;
                case CommandType.RequestParamList:
                    return (RequestParams(message, messageType, client, Configuration));
                //break;

                case CommandType.ClearParam:
                    using (var data = dBase())
                    {
                        var p = new BusinessLogic(data, Configuration);
                        p.ClearParamList(client, Configuration);
                    }
                    break;

                case CommandType.ReleaseParamList:
                    using (var data = dBase())
                    {
                        var m = JsonConvert.DeserializeObject<ReleaseParamList>(message);
                        var s = m.ObjectList.SelectMany(l => l.Keys, (l, o) =>
                            (from t in data.Subscriptions
                             where t.ClientId == client && t.ObjNam == l.ObjNam && t.ObjParm == o
                             select t).FirstOrDefault())
                            .Where(i => null != i).ToList();
                        foreach (var item in s)
                        {
                            data.Subscriptions.Remove(item);
                        }
                        data.SaveChanges();
                    }
                    break;

                case CommandType.SetParamList:
                    var rqst = JsonConvert.DeserializeObject<SetParamListResponse>(message);

                    SetParamListResponse spResponse;

                    using (var data = dBase())
                    {
                        var lastSent = data.Properties.FirstOrDefault(x => x.PropertyId == Configuration);
                        var parser = new BusinessLogic(data, Configuration);

                        try
                        {
                            parser.SetParamList(rqst);
                            var configChanges = parser.GetConfigChanges();
                            if (null != OnDataChangeEvent)
                            {
                                OnDataChangeEvent(this, new EventArgs());
                            }
                            if (null != configChanges.ObjectList &&
                                configChanges.ObjectList.Count > 0 &&
                                configChanges.ObjectList[0].Changes.Count > 0 &&
                                null != OnConfigurationChangeEvent)
                            {
                                OnConfigurationChangeEvent(this, JsonConvert.SerializeObject(configChanges));
                            }

                            var notifications = parser.GetNotifications();
                            if (null != notifications.ObjectList && notifications.ObjectList.Count > 0 &&
                                null != OnConfigurationChangeEvent)
                            {
                                OnConfigurationChangeEvent(this, JsonConvert.SerializeObject(notifications));
                            }

                            spResponse = new SetParamListResponse(
                                messageID: rqst.MessageID,
                                response: ResponseString.OK,
                                objectList: null
                            );
                        }
                        catch (Exception ex)
                        {
                            MessageLogger.LogMessage("Set Parameters", ex.Message, Configuration, Guid.Empty);

                            spResponse = new SetParamListResponse(
                                messageID: rqst.MessageID,
                                response: ResponseString.InternalServerError,
                                objectList: null
                            );
                        }
                    }

                    return JsonConvert.SerializeObject(spResponse);

                case CommandType.ReadParam:
                    var rd = JsonConvert.DeserializeObject<ReadParam>(message);
                    using (var data = dBase())
                    {
                        var bm = new BusinessLogic(data, Configuration);

                        var rsp = bm.GetConfigChanges(rd.TimeSince);

                        if (OnConfigurationChangeEvent != null)
                        {
                            OnConfigurationChangeEvent(this, JsonConvert.SerializeObject(rsp));
                        }
                    }
                    break;

                case CommandType.GetQuery:
                    return GetQueryCommand(message);

                case CommandType.SetCommand:
                    return SetCommand(message);

                case CommandType.ReadHistoryList:
                    var rh = JsonConvert.DeserializeObject<ReadHistory>(message);
                    using (var data = dBase())
                    {
                        var bm = new BusinessLogic(data, Configuration);
                        {
                            var rsp = bm.GetHistory(Convert.ToInt64(rh.TimeStart));
                        }
                    }
                    break;

                default:
                    if (null != OnCloseRequestEvent)
                    {
                        OnCloseRequestEvent(this);
                    }

                    if (null != LogMessageEvent)
                    {
                        LogMessageEvent(
                            String.Format("Unknown Message: {0}", message));
                    }
                    break;
            }
            return null;
        }

        public void CloseSocket()
        {
            if (null != OnCloseRequestEvent)
            {
                OnCloseRequestEvent(this);
            }
        }
        #endregion

        #region Private Methods

        private string CreateObject(string message, MessageType type)
        {
            string response = null;
            string request = null;
            try
            {
                var r = JsonConvert.DeserializeObject<CreateObjectFromParam>(message);
                if (r.ObjectList.Count > 1)
                {
                    response = JsonConvert.SerializeObject(
                        new ObjectCreatedResponse(
                            messageID: r.MessageID,
                            response: ResponseString.BadRequest,
                            objNam: String.Empty
                        )
                    );
                }
                else
                {
                    using (var db = new SimulatorContext())
                    {
                        foreach (var obj in r.ObjectList)
                        {
                            var bl = new Simulator.BusinessLogic.BusinessLogic(db, Configuration);

                            var objnam = bl.CreateObject(obj.ObjTyp);

                            if (string.IsNullOrEmpty(objnam))
                            {
                                response = JsonConvert.SerializeObject(
                                    new ObjectCreatedResponse(
                                        r.MessageID,
                                        ResponseString.BadRequest,
                                        objNam: String.Empty
                                    )
                                );
                            }
                            else
                            {
                                var parmObject = new List<ParamObject>()
                                    {
                                        new ParamObject(objnam, r.ObjectList[0].Parameters)
                                    };

                                bl.SetParamList(
                                    new SetParamListResponse(
                                        messageID: String.Empty,
                                        response: String.Empty,
                                        objectList: parmObject
                                    )
                                );

                                response = JsonConvert.SerializeObject(
                                    new ObjectCreatedResponse(
                                        r.MessageID,
                                        ResponseString.Created,
                                        objnam
                                    )
                                );

                                var changes = bl.GetConfigChanges();
                                if (null != changes.ObjectList &&
                                    changes.ObjectList.Count > 0 &&
                                    changes.ObjectList[0].Changes.Count > 0)
                                {
                                    SendMessage(response);
                                    response = JsonConvert.SerializeObject(changes);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageLogger.LogMessage(
                    "Create object",
                    string.Format("Error creating object {0}, error: {1}", request, ex.Message), Configuration,
                    Guid.Empty
                );
            }
            return response;
        }

        private string GetParms(
            string message,
            MessageType type)
        {
            GetParamListResponse response = null;

            try
            {
                var request = JsonConvert.DeserializeObject<GetParamList>(message);

                using (var data = dBase())
                {
                    var bm = new BusinessLogic(data, Configuration);

                    response = new GetParamListResponse(
                            messageID: request.MessageID,
                            response: ResponseString.OK,
                            objectList: bm.GetParamList(request)
                        );
                }
            }
            catch
            {
                response = new GetParamListResponse(
                    messageID: String.Empty,
                    response: ResponseString.BadRequest,
                    objectList: new List<ParamObject>()
                );
            }

            return JsonConvert.SerializeObject(response);
        
        } // GetParms()

        private string SetCommand(string message)
        {
            var lastChange = Globals.CurrentTime() - 1;
            
            var request = JsonConvert.DeserializeObject<SetCommand>(message);

            ResponseCommand response = null;
            
            switch (request.Method.ToUpper())
            {
                case "CLEARSTATUSMESSAGES":
                    using (var data = new SimulatorContext())
                    {
                        var bm = new BusinessLogic(data, Configuration);

                        if (bm.ClearStatusMessages())
                        {
                            response = new ResponseCommand(
                                CommandString.SetCommand, request.MessageID, ResponseString.OK
                            );

                            var changes = bm.GetNotifications();
                            if (null != changes.ObjectList && changes.ObjectList.Count > 0 &&
                                null != OnConfigurationChangeEvent)
                            {
                                OnConfigurationChangeEvent(this, JsonConvert.SerializeObject(changes));
                            }
                            var alerts = bm.GetWarningAndAlertChanges(lastChange.ToString());
                            if (null != alerts.ObjectList && alerts.ObjectList.Count > 0 &&
                                null != OnConfigurationChangeEvent)
                            {
                                OnConfigurationChangeEvent(this, JsonConvert.SerializeObject(alerts));
                            }
                        }

                        if (null != OnDataChangeEvent)
                        {
                            OnDataChangeEvent(this, new EventArgs());
                        }
                    }
                    break;

                case "SETSTATUSMESSAGEFLAGS":
                    var args = request.Arguments;

                    using (var data = new SimulatorContext())
                    {
                        var bm = new BusinessLogic(data, Configuration);
                        if (bm.SetStatusMessageMasks(args))
                        {
                            response = new ResponseCommand(
                                CommandString.SetCommand, request.MessageID, ResponseString.OK
                            );
                        }
                    }
                    break;

                case "SETBODYSTATE":
                    using (var data = new SimulatorContext())
                    {
                        var bm = new BusinessLogic(data, Configuration);

                        if (bm.SetBodyState(request.Arguments))
                        {
                            response = new ResponseCommand(
                                CommandString.SetCommand, request.MessageID, ResponseString.OK
                            );

                            var changes = bm.GetNotifications();
                            if (null != changes.ObjectList && changes.ObjectList.Count > 0 &&
                                null != OnConfigurationChangeEvent)
                            {
                                OnConfigurationChangeEvent(this, JsonConvert.SerializeObject(changes));
                            }
                            var alerts = bm.GetWarningAndAlertChanges(lastChange.ToString());
                            if (null != alerts.ObjectList && alerts.ObjectList.Count > 0 &&
                                null != OnConfigurationChangeEvent)
                            {
                                if (alerts.ObjectList[0].Changes.Count > 0 ||
                                    alerts.ObjectList[0].Deleted.Count > 0)
                                {
                                    OnConfigurationChangeEvent(this, JsonConvert.SerializeObject(alerts));
                                }
                            }
                            if (null != OnDataChangeEvent)
                            {
                                OnDataChangeEvent(this, new EventArgs());
                            }
                        }
                    }
                    break;

                case "MASTEROFF":
                    using (var data = new SimulatorContext())
                    {
                        var bm = new BusinessLogic(data, Configuration);
                        bm.MasterOff();
                        var notifications = bm.GetNotifications();
                        if (null != notifications.ObjectList && notifications.ObjectList.Count > 0 &&
                            null != OnConfigurationChangeEvent)
                        {
                            OnConfigurationChangeEvent(this, JsonConvert.SerializeObject(notifications));
                        }

                        response = new ResponseCommand(
                            CommandString.SetCommand, request.MessageID, ResponseString.OK
                        );
                    }
                    break;

                case "CREATELIGHTGROUP":
                    using (var data = new SimulatorContext())
                    {
                        var bm = new BusinessLogic(data, Configuration);

                        var rsp = new SetCommandResponse(
                            request.MessageID,
                            ResponseString.OK,
                            bm.CreateLightGrouping(request.Arguments)
                        );

                        if (null != rsp.ObjectList && rsp.ObjectList.ToString().Length > 0 &&
                            null != OnConfigurationChangeEvent)
                        {
                            OnConfigurationChangeEvent(this, JsonConvert.SerializeObject(rsp));
                        }
                        var notifications = bm.GetNotifications();
                        if (null != notifications.ObjectList && notifications.ObjectList.Count > 0 &&
                            null != OnConfigurationChangeEvent)
                        {
                            OnConfigurationChangeEvent(this, JsonConvert.SerializeObject(notifications));
                        }
                        var configChanges = bm.GetConfigChanges();
                        if (null != OnDataChangeEvent)
                        {
                            OnDataChangeEvent(this, new EventArgs());
                        }
                        if (null != configChanges.ObjectList &&
                            configChanges.ObjectList.Count > 0 &&
                            configChanges.ObjectList[0].Changes.Count > 0 &&
                            null != OnConfigurationChangeEvent)
                        {
                            OnConfigurationChangeEvent(this, JsonConvert.SerializeObject(configChanges));
                        }

                        return null;
                    }

                case "EDITLIGHTGROUP":
                    using (var data = new SimulatorContext())
                    {
                        var bm = new BusinessLogic(data, Configuration);

                        var rsp = new SetCommandResponse(
                            request.MessageID,
                            ResponseString.OK,
                            bm.SaveLightGrouping(request.Arguments)
                        );

                        if (null != rsp.ObjectList && rsp.ObjectList.ToString().Length > 0 &&
                            null != OnConfigurationChangeEvent)
                        {
                            OnConfigurationChangeEvent(this, JsonConvert.SerializeObject(rsp));
                        }
                        var notifications = bm.GetNotifications();
                        if (null != notifications.ObjectList && notifications.ObjectList.Count > 0 &&
                            null != OnConfigurationChangeEvent)
                        {
                            OnConfigurationChangeEvent(this, JsonConvert.SerializeObject(notifications));
                        }
                        var configChanges = bm.GetConfigChanges();
                        if (null != OnDataChangeEvent)
                        {
                            OnDataChangeEvent(this, new EventArgs());
                        }
                        if (null != configChanges.ObjectList &&
                            configChanges.ObjectList.Count > 0 &&
                            configChanges.ObjectList[0].Changes.Count > 0 &&
                            null != OnConfigurationChangeEvent)
                        {
                            OnConfigurationChangeEvent(this, JsonConvert.SerializeObject(configChanges));
                        }

                        return null;
                    }

                case "DELETELIGHTGROUP":
                    using (var data = new SimulatorContext())
                    {
                        var bm = new BusinessLogic(data, Configuration);

                        if (bm.DeleteLightGrouping(request.Arguments))
                        {
                            var rsp = new SetCommandResponse(
                                request.MessageID,
                                ResponseString.OK,
                                null
                            );

                            if (null != OnConfigurationChangeEvent)
                            {
                                OnConfigurationChangeEvent(this, JsonConvert.SerializeObject(rsp));
                            }
                            var notifications = bm.GetNotifications();
                            if (null != notifications.ObjectList && notifications.ObjectList.Count > 0 &&
                                null != OnConfigurationChangeEvent)
                            {
                                OnConfigurationChangeEvent(this, JsonConvert.SerializeObject(notifications));
                            }
                            var configChanges = bm.GetConfigChanges();
                            if (null != OnDataChangeEvent)
                            {
                                OnDataChangeEvent(this, new EventArgs());
                            }
                            if (null != configChanges.ObjectList &&
                                configChanges.ObjectList.Count > 0 &&
                                configChanges.ObjectList[0].Changes.Count > 0 &&
                                null != OnConfigurationChangeEvent)
                            {
                                OnConfigurationChangeEvent(this, JsonConvert.SerializeObject(configChanges));
                            }
                        }

                        return null;
                    }

            } // switch (request.Method.ToUpper())

            if (response == null)
                response = new ResponseCommand(CommandString.SetCommand, request.MessageID, ResponseString.BadRequest);

            return JsonConvert.SerializeObject(response);
        
        } // SetCommand()

        private string GetQueryCommand(string message)
        {
            GetQuery request = JsonConvert.DeserializeObject<GetQuery>(message);

            string response = null;

            switch (request.QueryName.ToUpper())
            {
                case "GETSTATUSMESSAGEFLAGS":
                    using (var data = new SimulatorContext())
                    {
                        var bm = new BusinessLogic(data, Configuration);

                        var rsp = new SendQueryWithDictionary(
                            request.MessageID,
                            ResponseString.OK,
                            bm.GetStatusMessageMasks()
                        );

                        response = JsonConvert.SerializeObject(rsp);
                    }
                    break;

                case "GETCONFIGURATION":
                    using (var data = new SimulatorContext())
                    {
                        var bm = new BusinessLogic(data, Configuration);

                        var rsp = new SendQueryWithParamList(
                            request.MessageID,
                            ResponseString.OK,
                            bm.GetConfiguration()
                        );

                        response = JsonConvert.SerializeObject(rsp);
                    }
                    break;

                case "GETVALIDLIGHTGROUPINGS":
                    using (var data = new SimulatorContext())
                    {
                        var bm = new BusinessLogic(data, Configuration);

                        var rsp = new SendQueryWithParamList(
                            request.MessageID,
                            ResponseString.OK,
                            bm.GetCompatibleLightCircuits(request.Arguments)
                        );

                        response = JsonConvert.SerializeObject(rsp);
                    }
                    break;

                default:
                    response = JsonConvert.SerializeObject(
                        new ResponseCommand(CommandString.Invalid, request.MessageID, ResponseString.BadRequest)
                    );
                    break;

            } // switch (request.QueryName.ToUpper())

            return response;

        } // GetQueryCommand()

        private string GetNotifications(string message)
        {
            var request = JsonConvert.DeserializeObject<ReadStatusMessage>(message);

            WriteStatusMessage rsp = null;

            using (var data = dBase())
            {
                var bm = new BusinessLogic(data, Configuration);
                rsp = bm.GetWarningsAndAlerts(String.Empty, request.TimeSince);
            }

            return JsonConvert.SerializeObject(rsp);
        }

        private string RequestParams(string message, MessageType type, Guid client, int property)
        {
            var errRsp = String.Format(
                        "{{\"command\": \"{0}\",\"messageID\": \"{1}\"}}",
                        CommandString.ErrorMessage,
                        type.MessageID);

            try
            {
                var request = JsonConvert.DeserializeObject<RequestParamList>(message);
                using (var data = dBase())
                {
                    var parser = new BusinessLogic(data, Configuration);

                    var response = new ResponseNotifyList(
                        request.MessageID,
                        ResponseString.OK,
                        String.Empty,
                        String.Empty,
                        parser.RequestParamList(request, client, property).ObjectList
                    );

                    return (response.ObjectList != null && response.ObjectList.Count > 0)
                        ? JsonConvert.SerializeObject(response)
                        : null;
                }
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return errRsp;
            }
        }
        #endregion
    }
    #region Helper Classes

    public class ClientSocketEventArgs
    {
        public ConnectionStatus Status { get; private set; }
        public string Message { get; private set; }
        public ClientSocketEventArgs(ConnectionStatus status, string message)
        {
            Status = status;
            Message = message;
        }
    }
    #endregion
}
