﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Pentair.Web.Api
{
    public class ServerCommandRemoveThwClients : ServerCommand
    {
        [JsonProperty("installationId", Required=Required.Always)]
        public int InstallationId { get; private set; }

        public ServerCommandRemoveThwClients(
            int installationId
            )
            : base(
                command: CommandString.ServerRemoveThwClients,
                messageID: new Guid().ToString()
            )
        {
            this.InstallationId = installationId;

        } // ServerCommandRemoveThwClients()

        public override void Handle()
        {
            WebSocketClient.RemoveThwClients(
                installationId: this.InstallationId,
                removeThw: false, // Clients only
                caller: "ServerCommandRemoveThwClients",
                reason: "THW disconnected"
            );

        } // Handle()

        /// <summary>
        /// Always returns an object, valid or not. If invalid, check the DeserializationError property value.
        /// </summary>
        public static new ServerCommandRemoveThwClients Deserialize(
            string serializedMessage)
        {
            ServerCommandRemoveThwClients command = null;

            try
            {
                command = JsonConvert.DeserializeObject<ServerCommandRemoveThwClients>(serializedMessage);
                command.IsValid = true;
            }
            catch (Exception e)
            {
                // Deserialization as ServerCommandRemoveThwClients failed

                StringBuilder sbError = new StringBuilder();

                sbError.Append("Cannot deserialize as ServerCommandRemoveThwClients. " + e.Message);

                // Try to get at least the base message data
                ServerCommand baseCommand = ServerCommand.Deserialize(serializedMessage);

                if (!baseCommand.IsValid)
                {
                    sbError.Append("\n" + baseCommand.DeserializationError);
                }

                command = new ServerCommandRemoveThwClients(
                    installationId: -1)
                {
                    IsValid = false,
                    DeserializationError = sbError.ToString()
                };
            }

            return command;

        } // Deserialize()

    } // class ServerCommandRemoveThwClients
}
