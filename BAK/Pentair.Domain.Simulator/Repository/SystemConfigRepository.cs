﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pentair.Domain.Simulator.Entity;
using Pentair.Domain.Simulator.Repository;
using Pentair.Domain.Simulator.Infrastructure;

namespace Pentair.Domain.Simulator.Repository
{
    public class SystemConfigRepository : Repository<SystemConfig, OcpContext>, ISystemConfigRepository
    {
        public SystemConfigRepository(OcpContext context) : base(context) 
        {
        }

        public SystemConfig GetDefault()
        {
            return Items.First(c => c.Name.Equals("Default"));
        }
    }
}
