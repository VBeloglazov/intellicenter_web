﻿'use strict';
var PWC = PWC || {};
///DEPRECATE!
PWC.directive('panelAccordion', [
    function () {
        return {
            restrict: 'EA',
            transclude: true,
            replace: true,
            scope: {
                onlyOneExpanded: '@',
                itemSelected: '&'
            },
            template: '<div class="panelAccordion" ng-transclude></div>',
            controller: [
                '$scope', function (scope) {
                    var collapsiblePanels = [];

                    this.openPanel = function (panelToOpen) {
                        if (scope.onlyOneExpanded) {
                            angular.forEach(collapsiblePanels, function (panel) {
                                panel.isOpen = false;
                                panel.styleIcon = 'unselected';
                            });
                            panelToOpen.isOpen = true;
                            panelToOpen.styleIcon = 'selected';
                        }
                    }

                    this.addCollapsiblePanel = function (collapsiblePanel) {
                        collapsiblePanels.push(collapsiblePanel);
                    }

                    this.itemSelected = function (item) {
                        scope.itemSelected({ objNam: item });
                    }
                }
            ]
        };
    }
]).directive('collapsiblePanel', [
    function () {
        return {
            require: '^panelAccordion',
            restrict: 'EA',
            transclude: true,
            replace: true,
            scope: {
                relayName: '@',
                circuit: '@',
                type: '@',
                defaultOpen: '@',
                typeIcon: '@',
                item: '@',
                selectedPanel: '=',
                parent: '@'
            },
            link: function (scope, element, attrs, panelAccordionController) {
                if (scope.defaultOpen == 'true') {
                    scope.isOpen = true;
                    scope.styleIcon = 'selected';
                } else {
                    scope.isOpen = false;
                    scope.styleIcon = 'unselected';
                }

                panelAccordionController.addCollapsiblePanel(scope);

                scope.toggleTab = function (panel) {
                    if (!panel.isOpen) {
                        panelAccordionController.openPanel(panel);
                        panelAccordionController.itemSelected(scope.item);
                    }
                }

                scope.$watch('type', function(val) {
                    if (scope.type) {
                        switch (scope.type) {
                            case 'LIGHT':
                            case 'MAGIC1':
                            case 'MAGIC2':
                            case 'INTELLI':
                            case 'SAML':
                            case 'COLORW':
                            case 'DIMMER':
                                scope.staticIcon = 'light';
                                break;
                            case 'BODY':
                                scope.staticIcon = 'pool';
                                break;
                            default:
                                scope.staticIcon = 'generic';
                        }
                    } else {
                        scope.staticIcon = 'generic';
                    }
                });

                scope.$watch('selectedPanel', function (val) {
                    if (scope.isOpen) {
                        if (val == scope.parent) {
                            panelAccordionController.itemSelected(scope.item);
                        }
                    }
                });
            },
            template: '<div class="collapsiblePanel">' +
                        '<div class="tab circuit icon {{styleIcon}} {{staticIcon}}" ng-click="toggleTab(this)">' +
                            '<div style="padding-top: 2px; width:250px; white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">{{relayName}}{{circuit}}</div>' +
                            '<div class="content" ng-show="isOpen" ng-transclude>' +
                            '</div>' +
                        '</div>' +
                    '</div>'
        };
    }
]);