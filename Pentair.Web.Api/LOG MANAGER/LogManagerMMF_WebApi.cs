/*
--------------------------------------------------------------------------------

    LogManagerMMF_WebApi.cs

    Copyright � Pentair Water Pool and Spa, Inc. 2014

--------------------------------------------------------------------------------
*/

using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using System.IO;
using System.IO.MemoryMappedFiles;

namespace SERVICE_SHARED
{
    public class LogManagerMMF_WebApi : ILogger_WebApi
    {
        private const int DESCRIPTION_LENGTH_MAX = 1024;
        private const string TRUNCATED_DESCRIPTION_FOOTER = "... (truncated)";

        private string EventSourceName { get; set; }

        private long logFileSizeMax;
        private long logMinBytesToFlush;
        private string eventLogName;
        private string mapLogName;

        private readonly Pool_ByteBuffer poolByteBuffer = new Pool_ByteBuffer(
            eventSourceNamePrefix: AppDomain.CurrentDomain.FriendlyName + "_",
            bufferSize: 1024,
            capacity: 500,
            caller: "LogManagerMMF_WebApi"
        );

        private string DayLogPathFormat;

        private object lockFile = new object();

        private int iDayLogCount = -1;
        private DateTime dtDayLogPrev = DateTime.MinValue;
        private bool bStopped = false;
        private long byteBuffered = 0L;
        private long posCurrent = 0L;
        private string dayLogPathCurrent;

        private MemoryMappedFile map = null;
        private MemoryMappedViewAccessor accessor = null;

        private StringBuilder sbEntry = new StringBuilder(capacity: 8192); // Pre-allocated buffer

        #region Public Interface

        public void Initialize(
            string logDirectory,
            long logFileSizeMax,
            long logMinBytesToFlush,
            string eventLogName,
            string eventSourceName)
        {
            this.logFileSizeMax = logFileSizeMax;
            this.logMinBytesToFlush = logMinBytesToFlush;
            this.eventLogName = eventLogName;
            this.EventSourceName = eventSourceName;
            this.mapLogName = eventSourceName;

            this.DayLogPathFormat = String.Format("{0}/{{0:yyyy-MM-dd}}_log_{{1:000}}.csv",
                logDirectory
            );

            DirectoryInfo dirInfo = new DirectoryInfo(logDirectory);

            if (!dirInfo.Exists)
            {
                try
                {
                    dirInfo.Create();
                }
                catch (Exception e)
                {
                    SaveEventLog(
                        source: this.EventSourceName,
                        eventBriefDescription: "Cannot create Log directory",
                        installationIdTxt: String.Empty,
                        userIdTxt: String.Empty,
                        connectionIdTxt: String.Empty,
                        eventFullDescription: String.Format("Directory name: '{0}'\n\nError: {1}", logDirectory, e.Message),
                        typeEntry: EventLogEntryType.Error
                    );

                    return;
                }
            }

            SaveEventLog(
                source: this.EventSourceName,
                eventBriefDescription: String.Format("Logs: '{0}'", dirInfo.FullName),
                installationIdTxt: String.Empty,
                userIdTxt: String.Empty,
                connectionIdTxt: String.Empty,
                eventFullDescription: String.Empty,
                typeEntry: EventLogEntryType.Information
            );

            // Find the last already existing log file to continue with it
            string sFileNameTmpPrev;
            string sFileNameTmp = String.Empty;
            while (true)
            {
                iDayLogCount++;

                sFileNameTmpPrev = sFileNameTmp;

                sFileNameTmp = String.Format(this.DayLogPathFormat,
                    DateTime.Now,
                    iDayLogCount
                );

                if (!File.Exists(sFileNameTmp))
                {
                    string fileCurrent;
                    if (String.IsNullOrEmpty(sFileNameTmpPrev))
                    {
                        fileCurrent = sFileNameTmp;
                    }
                    else
                    {
                        fileCurrent = sFileNameTmpPrev;
                        iDayLogCount--;
                    }

                    SetDayLogPath(fileCurrent);

                    dtDayLogPrev = DateTime.Now;

                    break;
                }
            }

        } // Initialize()

        public void LogError(
            string source,
            string eventBriefDescription,
            int installationId,
            Guid userId,
            Guid connectionId,
            string eventFullDescription)
        {
            try
            {
                DoLoggingAction(
                    DateTime.Now,
                    eventFullDescription,
                    (dtEntry) => LogEntry(
                        dtEntry, source, eventBriefDescription, installationId, userId, connectionId, eventFullDescription,
                        EventLogEntryType.Error
                    )
                );
            }
            catch (Exception e)
            {
                HandleLoggingError("LogError", e);
            }

        } // LogError()

        public void LogWarning(
            string source,
            string eventBriefDescription,
            int installationId,
            Guid userId,
            Guid connectionId,
            string eventFullDescription,
            bool saveEventLog = false)
        {
            try
            {
                DoLoggingAction(
                    DateTime.Now,
                    eventFullDescription,
                    (dtEntry) => LogEntry(
                        dtEntry, source, eventBriefDescription, installationId, userId, connectionId, eventFullDescription,
                        EventLogEntryType.Warning,
                        saveEventLog
                    )
                );
            }
            catch (Exception e)
            {
                HandleLoggingError("LogWarning", e);
            }

        } // LogWarning()

        public void LogInformation(
            string source,
            string eventBriefDescription,
            int installationId,
            Guid userId,
            Guid connectionId,
            string eventFullDescription,
            bool saveEventLog = false)
        {
            try
            {
                DoLoggingAction(
                    DateTime.Now,
                    eventFullDescription,
                    (dtEntry) => LogEntry(
                        dtEntry, source, eventBriefDescription, installationId, userId, connectionId, eventFullDescription,
                        EventLogEntryType.Information,
                        saveEventLog
                    )
                );
            }
            catch (Exception e)
            {
                HandleLoggingError("LogInformation", e);
            }

        } // LogInformation()

        public void LogErrorAsync(
            string source,
            string eventBriefDescription,
            int installationId,
            Guid userId,
            Guid connectionId,
            string eventFullDescription)
        {
            try
            {
                DoLoggingAction(
                    DateTime.Now,
                    eventFullDescription,
                    (dtEntry) => LogEntry(
                        dtEntry, source, eventBriefDescription, installationId, userId, connectionId, eventFullDescription,
                        EventLogEntryType.Error
                    )
                );
            }
            catch (Exception e)
            {
                HandleLoggingError("LogErrorAsync", e);
            }

        } // LogErrorAsync()

        public void LogWarningAsync(
            string source,
            string eventBriefDescription,
            int installationId,
            Guid userId,
            Guid connectionId,
            string eventFullDescription,
            bool saveEventLog = false)
        {
            try
            {
                DoLoggingAction(
                    DateTime.Now,
                    eventFullDescription,
                    (dtEntry) => LogEntry(
                        dtEntry, source, eventBriefDescription, installationId, userId, connectionId, eventFullDescription,
                        EventLogEntryType.Warning,
                        saveEventLog
                    )
                );
            }
            catch (Exception e)
            {
                HandleLoggingError("LogWarningAsync", e);
            }

        } // LogWarningAsync()

        public void LogInformationAsync(
            string source,
            string eventBriefDescription,
            int installationId,
            Guid userId,
            Guid connectionId,
            string eventFullDescription,
            bool saveEventLog = false)
        {
            try
            {
                DoLoggingAction(
                    DateTime.Now,
                    eventFullDescription,
                    (dtEntry) => LogEntry(dtEntry, source, eventBriefDescription, installationId, userId, connectionId, eventFullDescription, EventLogEntryType.Information, saveEventLog)
                );
            }
            catch (Exception e)
            {
                HandleLoggingError("LogInformationAsync", e);
            }

        } // LogInformationAsync()

        /// <summary>
        /// Takes final message as a parameter. Can be null.
        /// </summary>
        public void Stop(
            string source,
            string eventBriefDescription,
            int installationId,
            Guid userId,
            Guid connectionId,
            string eventFullDescription,
            EventLogEntryType typeEntry)
        {
            try
            {
                DoLoggingAction(
                    DateTime.Now,
                    eventFullDescription,
                    (dtEntry) =>
                    {
                        if (!(String.IsNullOrEmpty(eventBriefDescription) && String.IsNullOrEmpty(eventFullDescription)))
                        {
                            LogEntry(dtEntry, source, eventBriefDescription, installationId, userId, connectionId, eventFullDescription, typeEntry, saveEventLog: true);
                        }

                        if (map != null)
                        {
                            CloseFile();
                        }

                        bStopped = true;
                    }
                );
            }
            catch (Exception e)
            {
                HandleLoggingError("Stop", e);
            }

        } // Stop()

        #endregion Public Interface

        #region Implementation

        /// <summary>
        /// Locks lockFile.
        /// </summary>
        private void DoLoggingAction(
            DateTime dtEntry,
            string messageToLog,
            Action<DateTime> action)
        {
            try
            {
                LockWithTimeout.DoWithLock(
                    lockObj: this.lockFile,
                    timeoutMSec: 100,
                    action: () =>
                    {
                        try
                        {
                            if (!bStopped)
                            {
                                action(dtEntry);
                            }
                        }
                        catch (Exception e)
                        {
                            SaveEventLog(
                                source: this.EventSourceName,
                                eventBriefDescription: "Logging error",
                                installationIdTxt: String.Empty,
                                userIdTxt: String.Empty,
                                connectionIdTxt: String.Empty,
                                eventFullDescription: String.Format("Error:\n{0}\n\nMessage lost:\n{1}", e.Message, messageToLog),
                                typeEntry: EventLogEntryType.Error
                            );
                        }
                    }
                );
            }
            catch(Exception e)
            {
                SaveEventLog(
                    source: this.EventSourceName,
                    eventBriefDescription: "Logging error",
                    installationIdTxt: String.Empty,
                    userIdTxt: String.Empty,
                    connectionIdTxt: String.Empty,
                    eventFullDescription: String.Format("Error:\n{0}\n\nMessage lost:\n{1}", e.Message, messageToLog),
                    typeEntry: EventLogEntryType.Error
                );
            }

        } // DoReportingAction()

        /// <summary>
        /// Not thread safe.
        /// </summary>
        private void LogEntry(
            DateTime dtEntry,
            string source,
            string eventBriefDescription,
            int installationId,
            Guid userId,
            Guid connectionId,
            string eventFullDescription,
            EventLogEntryType typeEntry,
            bool saveEventLog = false)
        {
            string installationIdTxt = (installationId > 0) ? installationId.ToString() : String.Empty;
            string userIdTxt = (userId.CompareTo(Guid.Empty) != 0) ? userId.ToString() : String.Empty;
            string connectionIdTxt = (connectionId.CompareTo(Guid.Empty) != 0) ? connectionId.ToString() : String.Empty;

            // Replace desired double quotes
            string descriptionCSV = eventFullDescription.Replace("\"", "\"\"");

            // TODO: Single quotes, backslash, etc.?

            if (typeEntry != EventLogEntryType.Error && descriptionCSV.Length > DESCRIPTION_LENGTH_MAX)
            {
                // Truncate message

                descriptionCSV =
                    descriptionCSV.Substring(0, DESCRIPTION_LENGTH_MAX - TRUNCATED_DESCRIPTION_FOOTER.Length) + TRUNCATED_DESCRIPTION_FOOTER;
            }

            sbEntry.Clear();
            sbEntry.AppendFormat("{0:HH:mm:ss.fff},{1},{2},{3},{4},{5},{6},\"{7}\"\n",
                dtEntry,
                GetEntryTypeTxt(typeEntry),
                source,
                eventBriefDescription,
                installationIdTxt,
                userIdTxt,
                connectionIdTxt,
                descriptionCSV
            );

            string entry = sbEntry.ToString();

            bool bNewFile = false;

            // Check if the day is new
            DateTime dtNow = DateTime.Now;
            if (dtNow.Date != dtDayLogPrev.Date)
            {
                iDayLogCount = 0;
                bNewFile = true;
            }

            dtDayLogPrev = dtNow;

            // Check the file size
            if (posCurrent + entry.Length > this.logFileSizeMax)
            {
                iDayLogCount++;
                bNewFile = true;
            }

            if (bNewFile)
            {
                SetDayLogPath(
                    String.Format(this.DayLogPathFormat,
                        DateTime.Now,
                        iDayLogCount
                    )
                );
            }

            if (byteBuffered + entry.Length > this.logMinBytesToFlush)
            {
                accessor.Flush();
                byteBuffered = 0L;
            }

            WriteEntry(entry);

            if (typeEntry == EventLogEntryType.Error || saveEventLog)
            {
                SaveEventLog(
                    source,
                    eventBriefDescription,
                    installationIdTxt,
                    userIdTxt,
                    connectionIdTxt,
                    eventFullDescription,
                    typeEntry
                );
            }

        } // LogEntry()

        private void WriteEntry(
            string entry)
        {
            Pool_ByteBuffer_Item poolByteBufferItem = null;
            try
            {
                poolByteBufferItem = poolByteBuffer.Pop("LogManagerMMF_WebApi.WriteEntry()");

                if (poolByteBufferItem != null)
                {
                    int entryPos = 0;
                    while (entryPos < entry.Length)
                    {
                        int entryCount = entry.Length - entryPos;

                        int count = (entryCount < poolByteBufferItem.BufferSize)
                            ? entryCount
                            : poolByteBufferItem.BufferSize;

                        for (int i = 0; i < count; i++)
                        {
                            poolByteBufferItem.Buffer[i + poolByteBufferItem.Offset] = (byte)entry[entryPos + i];
                        }

                        accessor.WriteArray(posCurrent, poolByteBufferItem.Buffer, 0, count);

                        posCurrent += count;
                        byteBuffered += count;

                        entryPos += count;
                    }
                }
                else
                {
                    SaveEventLog(
                        source: this.EventSourceName,
                        eventBriefDescription: "Writing entry failed",
                        installationIdTxt: String.Empty,
                        userIdTxt: String.Empty,
                        connectionIdTxt: String.Empty,
                        eventFullDescription: String.Format("Entry lost:\n{0}", entry),
                        typeEntry: EventLogEntryType.Error
                    );
                }
            }
            finally
            {
                if (poolByteBufferItem != null)
                {
                    poolByteBuffer.Push(poolByteBufferItem, "LogManagerMMF_WebApi.WriteEntry()");
                }
            }

        } // WriteEntry()

        private void HandleLoggingError(
            string source,
            Exception e)
        {
            if (e != null)
            {
                SaveEventLog(
                    source: this.EventSourceName,
                    eventBriefDescription: "Logging error",
                    installationIdTxt: String.Empty,
                    userIdTxt: String.Empty,
                    connectionIdTxt: String.Empty,
                    eventFullDescription: ExceptionManager.CustomizeException(e).ErrorMessage(String.Format("({0})", source)),
                    typeEntry: EventLogEntryType.Error
                );
            }

        } // HandleLoggingError()

        private void SaveEventLog(
            string source,
            string eventBriefDescription,
            string installationIdTxt,
            string userIdTxt,
            string connectionIdTxt,
            string eventFullDescription,
            EventLogEntryType typeEntry)
        {
            try
            {
                if (!String.IsNullOrWhiteSpace(eventFullDescription))
                {
                    // The description could arrive within d-quotas to be saved to a single CSV cell.
                    // Not needed for the EventLog. Remove leading and tailing d-quotas, if both presented.
                    if (eventFullDescription[0] == '\"' && eventFullDescription[eventFullDescription.Length - 1] == '\"')
                    {
                        eventFullDescription = eventFullDescription.Substring(1, eventFullDescription.Length - 2);
                    }
                }

                // Requires administrator's privileges.
                // Otherwise throws System.Security.SecurityException.
                if (!EventLog.SourceExists(source))
                {
                    EventLog.CreateEventSource(source, this.eventLogName);
                }

                string logEntry = String.Format("{0}{1}{2}{3}{4}",
                    eventBriefDescription,
                    !String.IsNullOrEmpty(installationIdTxt) ? "\nInstallationId: " + installationIdTxt : String.Empty,
                    !String.IsNullOrEmpty(userIdTxt) ? "\nUserId: " + userIdTxt : String.Empty,
                    !String.IsNullOrEmpty(connectionIdTxt) ? "\nConnectionId: " + connectionIdTxt : String.Empty,
                    !String.IsNullOrEmpty(eventFullDescription) ? "\n\n" + eventFullDescription : String.Empty
                );

                int lengthMax = (typeEntry != EventLogEntryType.Error) ? DESCRIPTION_LENGTH_MAX : 20000; // Max = 32766
                if (logEntry.Length > lengthMax)
                {
                    // Truncate message

                    logEntry = logEntry.Substring(0, length: lengthMax - TRUNCATED_DESCRIPTION_FOOTER.Length)
                        + TRUNCATED_DESCRIPTION_FOOTER;
                }

                EventLog.WriteEntry(source, logEntry, typeEntry);
            }
            catch(Exception e)
            {
                try
                {
                    EventLog.WriteEntry(
                        this.eventLogName,
                        String.Format("Writing to EventLog failed\n\nError:\n{0}", e.Message),
                        EventLogEntryType.Error
                    );
                }
                catch
                {
                    // No action. Check if the log file is full... (manually ;)
                }
            }

        } // SaveEventLog()

        /// <summary>
        /// Not thread safe. Caller shall use lockFile.
        /// </summary>
        private void SetDayLogPath(
            string sDayLogPath)
        {
            if (map != null)
            {
                CloseFile();
            }

            byteBuffered = 0;

            bool bNewFile;
            if (!File.Exists(sDayLogPath))
            {
                // New file
                FileStream fs = File.Create(sDayLogPath);
                fs.Close();
                posCurrent = 0L;

                bNewFile = true;
            }
            else
            {
                // Existing file
                FileStream fs = new FileStream(sDayLogPath, FileMode.Open);
                posCurrent = fs.Length;
                fs.Close();

                bNewFile = false;
            }

            FileStream fileStream = new FileStream(sDayLogPath, FileMode.Open, FileAccess.ReadWrite, FileShare.Read);

            map = MemoryMappedFile.CreateFromFile(
                    fileStream: fileStream, 
                    mapName: this.mapLogName, 
                    capacity: this.logFileSizeMax, 
                    access: MemoryMappedFileAccess.ReadWrite, 
                    memoryMappedFileSecurity: null, 
                    inheritability: HandleInheritability.None, 
                    leaveOpen: false
                    );
            //path: sDayLogPath,
            //mode: FileMode.Open,
            //mapName: this.mapLogName,
            //capacity: this.logFileSizeMax
            //);

            accessor = (map != null) ? map.CreateViewAccessor() : null;

            dayLogPathCurrent = sDayLogPath;

            // Header
            if (bNewFile)
            {
                WriteEntry("Time,Category,Source,Event,THW ID,User ID,Connection ID,Description\n");
            }

        } // SetDayLogPath()

        private void CloseFile()
        {
            accessor.Flush();
            accessor.Dispose();
            map.Dispose();

            // Truncate file to the actual size
            FileStream fs = new FileStream(dayLogPathCurrent, FileMode.Open);
            fs.SetLength(posCurrent);
            fs.Close();

        } // CloseFile()

        private string GetEntryTypeTxt(
            EventLogEntryType type)
        {
            switch (type)
            {
                case EventLogEntryType.Error: return "ERRR";
                case EventLogEntryType.Warning: return "WARN";
                case EventLogEntryType.Information: return "INFO";
                default: return type.ToString();
            }

        } // GetEntryTypeTxt()

        #endregion Implementation

    } // class LogManagerMMF_WebApi
}
