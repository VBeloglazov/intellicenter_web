﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Pentair.Domain.Client.Helpers.Commands
{
    public class SuccessCommandResponse : ResponseCommand
    {
        public SuccessCommandResponse(
            string messageID
            )
            : base(CommandString.SuccessMessage, messageID, ResponseString.OK)
        {
        } // SuccessCommandResponse()

        /// <summary>
        /// Always returns an object, valid or not. If invalid, check the DeserializationError property value.
        /// </summary>
        public static new SuccessCommandResponse Deserialize(
            string serializedMessage)
        {
            SuccessCommandResponse command = null;

            try
            {
                command = JsonConvert.DeserializeObject<SuccessCommandResponse>(serializedMessage);
                command.IsValid = true;
            }
            catch (Exception e)
            {
                // Deserialization as SuccessCommandResponse failed

                StringBuilder sbError = new StringBuilder();

                sbError.Append("Cannot deserialize as SuccessCommandResponse. " + e.Message);

                // Try to get at least the base message data
                ResponseCommand baseCommand = ResponseCommand.Deserialize(serializedMessage);

                if (!baseCommand.IsValid)
                {
                    sbError.Append("\n" + baseCommand.DeserializationError);
                }

                command = new SuccessCommandResponse(
                    messageID: baseCommand.MessageID)
                {
                    IsValid = false,
                    DeserializationError = sbError.ToString()
                };
            }

            return command;

        } // Deserialize()

    } // class SuccessCommandResponse
}