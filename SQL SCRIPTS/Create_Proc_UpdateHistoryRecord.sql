USE [PathfinderIntellicenter]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (
	SELECT * FROM sys.objects
	WHERE object_id = OBJECT_ID(N'dbo.pr_UpdateHistoryRecord')
	AND type in (N'P', N'PC')
)
DROP PROCEDURE dbo.pr_UpdateHistoryRecord
GO


-- =============================================
-- Author			:	Wayne Hui
-- Create date		:	02/13/2019
-- Parameters		:
--	@InstallationId	:	Device ID
--	@Revision		:	Revision Number
--	@ObjName		:	Object Type Name
--	@Key			:	Value Type
--	@Value			:	Type Value
-- Description		:	Updates the object and create history record
--
-- Modified By			Date		Description
-- =============================================
CREATE PROCEDURE [dbo].[pr_UpdateHistoryRecord]
	@InstallationId		INT
	, @Revision			BIGINT
	, @ObjName			NVARCHAR(16)
	, @Key				NVARCHAR(16)
	, @Value			NVARCHAR(256)
AS
BEGIN

	SET NOCOUNT OFF

----TEST
--DECLARE
--	@InstallationId		INT
--	, @Revision			BIGINT
--	, @ObjName			NVARCHAR(16)
--	, @Key				NVARCHAR(16)
--	, @Value			NVARCHAR(256)

--SET @InstallationId		= 3948
--SET @Revision			= 1550084580
--SET @ObjName			= '_A135'
--SET @Key				= 'PROBE'
--SET @Value				= '31'

----END TEST

	---- Update the value for this row
--	UPDATE ControlObjects
	UPDATE ControlObjectsHistory
	SET Value = @value
	WHERE 
		INSTALLATION = @InstallationId
		AND Revision = @Revision
		AND OBJNAM = @ObjName
		AND [Key] = @Key

	
	---- Insert new row into history
--	INSERT INTO ControlObjects
	INSERT INTO ControlObjectsHistory
	(
		INSTALLATION		
		,Revision		
		,OBJNAM				
		,[Key]				
		,[Value]
		--,Deleted
		--,ParamType
		,CreateDateTime		
		,UpdateDateTime
	)
	SELECT
		@installationId	
		,@revision			
		,@objName			
		,@key				
		,@value		
		--,0				
		--,'History'	
		,GETUTCDATE()			-- CreateDateTime	TODO: 2b converted to UTC later
		,GETUTCDATE()			-- UpdateDateTime	TODO: 2b converted to UTC later
	WHERE 
		NOT EXISTS 
		(
			SELECT 1 FROM ControlObjects
			WHERE INSTALLATION = @installationId
			AND Revision = @revision
			AND OBJNAM = @objName
			AND [Key] = @key
		)


END

GO


--GRANT EXECUTE ON [dbo].[pr_UpdateHistoryRecord] TO PUBLIC
--GO
