﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;

namespace Pentair.Desktop.Simulator.Views
{
    /// <summary>
    /// Interaction logic for TreeView.xaml
    /// </summary>
    public partial class TreeView : UserControl
    {
        public TreeView()
        {
            InitializeComponent();
        }

        private void DgOnDataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            Dispatcher.BeginInvoke(DispatcherPriority.Render, new Action(InitializeComponent));
        }

    }
}
