﻿'use strict';
describe('DashboardController', function () {
    var DashboardController;
    var AppService;
    var $scope;
    var SocketService;
    var $controller;
    var AuthService;
    var $q;
    var mockInstallations;
    var $route;
    var LocalStorageService;
    var Enumerations;
    var PropertyService;
    var $location;
    var propertyModel;
    var messageProcessor;


    beforeEach(module('PentairWebClient'));

    beforeEach(inject(function (_$controller_, _AppService_, _$rootScope_, _SocketService_, _AuthService_, _$q_, _$route_, _LocalStorageService_, _Enumerations_, _PropertyService_, _PropertyModel_, _MessageProcessor_, _$location_) {
        AppService = _AppService_;
        $scope = _$rootScope_.$new();
        SocketService = _SocketService_;
        $controller = _$controller_;
        AuthService = _AuthService_;
        LocalStorageService = _LocalStorageService_;
        Enumerations = _Enumerations_;
        $q = _$q_;
        $route = _$route_;
        PropertyService = _PropertyService_;
        $location = _$location_;
        propertyModel = _PropertyModel_;
        messageProcessor = _MessageProcessor_;
        messageProcessor.ProcessNotification(JSON.parse(SpecHelper.JSONdata.Bodies));

        mockInstallations = [
                 {
                     "property": "1f92ba9c-077a-4cac-bdd2-1dee3579911a",
                     "name": "hewitts pool 1",
                     "connected": false
                 },
                 {
                     "property": "c077a376-e4f5-40b9-a495-ace172297e1c",
                     "name": "Hewitt Pool 4",
                     "connected": true
                 },
                 {
                     "property": "f467e58f-307f-43f4-a9dc-df7bc3828a88",
                     "name": "Hewitt Pool 2",
                     "connected": false
                 }
        ];
        $route.current = {
            locals: { 'installations': mockInstallations }
        };

        DashboardController = $controller("DashboardController", { $scope: $scope });

        spyOn($location, 'path').and.returnValue('/property');
    }));
});


describe('DashboardController autoConnect to single property', function () {
    var DashboardController;
    var $controller;
    var $q;
    var $route;
    var $scope;
    var mockInstallation;
    var PropertyService;
    beforeEach(module('PentairWebClient'));

    beforeEach(inject(function (_$controller_, _$rootScope_, _$q_, _$route_, _PropertyService_) {
        $scope = _$rootScope_.$new();
        $controller = _$controller_;
        $q = _$q_;
        $route = _$route_;
        PropertyService = _PropertyService_;
        mockInstallation = [
            {
                "property": "1f92ba9c-077a-4cac-bdd2-1dee3579911a",
                "name": "hewitts pool 1",
                "connected": false
            }
        ];

        $route.current = {
            locals: { 'installations': mockInstallation }
        };
        DashboardController = $controller("DashboardController", { $scope: $scope });
        // spyOn($scope, 'Connect');
    }));

    xit('should automatically connect to property if user only has one property', function () {
        expect(PropertyService.Installations).toEqual(mockInstallation);
        expect(PropertyService.Installations.length).toEqual(1);
        expect(PropertyService.SelectedInstallation).toBe(mockInstallation[0].property);

        //Spy not working
        // expect($scope.Connect).toHaveBeenCalled();
    });

});
