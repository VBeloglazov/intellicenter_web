﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;

namespace Pentair.Desktop.Simulator.Views
{
    /// <summary>
    /// Interaction logic for MessagePopup.xaml
    /// </summary>
    public partial class MessagePopup : UserControl
    {
        public bool HasFocus { get; private set; }

        internal DispatcherTimer HideTimer;

        public MessagePopup()
        {
            InitializeComponent();
            HasFocus = false;
            HideTimer = new DispatcherTimer { Interval = TimeSpan.FromSeconds(2), IsEnabled = false };
            HideTimer.Tick += HideTimerCallback;
        }

        internal void StartTimer()
        {
            HideTimer.Interval = TimeSpan.FromSeconds(2);
            HideTimer.Start();
        }

        internal void StopTimer()
        {
            HideTimer.Stop();
        }

        private void HideTimerCallback(Object source, EventArgs e)
        {
            HideTimer.Stop();
            Application.Current.Dispatcher.Invoke(HidePopup);
        }

        private void HidePopup()
        {
            if (!HasFocus)
            {
                HideTimer.Stop();
                MessageDetailPopup.IsOpen = false;
            }
            else
            {
                StartTimer();
            }
        }

        private void OnHideDetail(object sender, MouseEventArgs e)
        {
            HasFocus = false;
        }
        private void OnShowDetail(object sender, MouseEventArgs e)
        {
            HasFocus = true;
        }
    }
}
