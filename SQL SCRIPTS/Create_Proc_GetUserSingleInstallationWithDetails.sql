USE [PathfinderIntellicenter]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (
	SELECT * FROM sys.objects
	WHERE object_id = OBJECT_ID(N'dbo.pr_GetUserSingleInstallationWithDetails')
	AND type in (N'P', N'PC')
)
DROP PROCEDURE dbo.pr_GetUserSingleInstallationWithDetails
GO


-- =============================================
-- Author			:	Wayne Hui
-- Create date		:	02/13/2019
-- Parameters		:
--	@InstallationId	:	Device ID
--  @UserId			:	User Guid
-- Description		:	Get a single user installation detail
--
-- Modified By			Date		Description
-- =============================================
CREATE PROCEDURE [dbo].[pr_GetUserSingleInstallationWithDetails]
	@InstallationId	INT
	, @UserId		NVARCHAR(36)
AS
BEGIN

	SET NOCOUNT OFF

----TEST
--DECLARE
--	@InstallationId	INT
--	, @UserId		NVARCHAR(36)

--SET @InstallationId	= 3948
--SET @UserId			= '1a1c200c-d52b-467f-808c-ee3862ae6994'

----END TEST


	SELECT
		I.InstallationId [InstallationId]
		,A.AccessToken [AccessToken]
		,I.Name [PoolName]
		,(SELECT CASE WHEN EXISTS (SELECT 1 FROM [Connections] WHERE InstallationId = I.InstallationId AND CONVERT(VARCHAR(36), UserId) = '00000000-0000-0000-0000-000000000000') THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END) [OnLine]
		,I.UpdateDateTime [LastStatusChange]
		,(SELECT COUNT(*) FROM Associations WHERE InstallationId = I.InstallationId AND AccessToken IN ('UFFFF', 'UFFFE')) TotalAdmins
		,(SELECT TOP 1 Value FROM [ControlObjects] WHERE INSTALLATION = I.InstallationId AND OBJNAM = '_5451' AND [Key] = 'ADDRESS' ORDER BY Revision DESC) [Address]
		,(SELECT TOP 1 Value FROM [ControlObjects] WHERE INSTALLATION = I.InstallationId AND OBJNAM = '_5451' AND [Key] = 'CITY'    ORDER BY Revision DESC) [City]
		,(SELECT TOP 1 Value FROM [ControlObjects] WHERE INSTALLATION = I.InstallationId AND OBJNAM = '_5451' AND [Key] = 'STATE'	ORDER BY Revision DESC) [State]
		,(SELECT TOP 1 Value FROM [ControlObjects] WHERE INSTALLATION = I.InstallationId AND OBJNAM = '_5451' AND [Key] = 'ZIP'     ORDER BY Revision DESC) [Zip]
		,(SELECT TOP 1 Value FROM [ControlObjects] WHERE INSTALLATION = I.InstallationId AND OBJNAM = '_5451' AND [Key] = 'COUNTRY' ORDER BY Revision DESC) [Country]
		,(SELECT TOP 1 Value FROM [ControlObjects] WHERE INSTALLATION = I.InstallationId AND OBJNAM = '_5451' AND [Key] = 'TIMZON'  ORDER BY Revision DESC) [TimeZone]
		,(SELECT TOP 1 Value FROM [ControlObjects] WHERE INSTALLATION = I.InstallationId AND OBJNAM = '_5451' AND [Key] = 'NAME'    ORDER BY Revision DESC) [OwnerName]
		,(SELECT TOP 1 Value FROM [ControlObjects] WHERE INSTALLATION = I.InstallationId AND OBJNAM = '_5451' AND [Key] = 'PHONE'   ORDER BY Revision DESC) [Phone]
		,(SELECT TOP 1 Value FROM [ControlObjects] WHERE INSTALLATION = I.InstallationId AND OBJNAM = '_5451' AND [Key] = 'EMAIL'   ORDER BY Revision DESC) [Email]
	FROM 
		Installations I 
		JOIN Associations A 
			ON A.InstallationId = I.InstallationId
	WHERE 
		I.InstallationId = @InstallationId
		AND CONVERT(VARCHAR(36), A.UserId) = @UserId

		
END

GO


--GRANT EXECUTE ON [dbo].[pr_GetUserSingleInstallationWithDetails] TO PUBLIC
--GO
