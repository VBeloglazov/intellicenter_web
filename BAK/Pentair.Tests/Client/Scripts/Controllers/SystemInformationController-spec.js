﻿'use strict';
describe('SystemInformationController', function () {
  var propertyService;
  var $location;
  var $filter;
  var $scope;
  var $rootScope;
  var propertyModel;
  var systemInformationController;
  var $route;
  var $controller;
  var messageProcessor;
  var appService;
  var $httpBackend;
  var $compile;
  beforeEach(module('PentairWebClient'));


  beforeEach(inject(function (_$controller_, _$httpBackend_, _$rootScope_, _PropertyService_, _PropertyModel_,
                              _$location_, _$filter_, _$q_, _$route_, _MessageProcessor_, _AppService_, _$compile_) {
    $scope = _$rootScope_.$new();
    $rootScope = _$rootScope_;
    propertyService = _PropertyService_;
    propertyModel = _PropertyModel_;
    $location = _$location_;
    $filter = _$filter_;
    $controller = _$controller_;
    $q = _$q_;
    $route = _$route_;
    messageProcessor = _MessageProcessor_;
    $httpBackend = _$httpBackend_;
    appService = _AppService_;
    $compile = _$compile_;

    propertyModel.MessageQueue = angular.extend({id1: "test"}, SpecHelper.messageQueue.LocationRequest);
    propertyModel.Installations = [
      {
        "InstallationId": "1",
        "PoolName": "Dev pool",
        "OnLine": true
      }
    ];
    propertyModel.Objects["UFFFE"] = {"OBJNAM": "UFFFE", "SNAME": "Administrator", "ENABLE": "ON", "PASSWRD": "4647"};
    propertyModel.SystemInformation = {"PoolName": "Dev pool", "OwnerName": "John Doe"};
    propertyModel.SelectedInstallation = "1";
    propertyModel.SystemInfo[propertyModel.SelectedInstallation] = {ADDRESS: "Address", OWNER: "1"};
    propertyModel.NotificationRecipients = [
      {
        "RecipientId": 1,
        "InstallationId": 1,
        "Name": "Test Guy",
        "EmailAddress1": "test@email.com",
        "EmailAddress2": "",
        "Phone1": "312-555-2231",
        "Phone2": "",
        "CreateDateTime": "2015-11-10T12:48:15.857",
        "UpdateDateTime": "2015-11-10T12:48:15.857"
      }
    ];
    propertyModel.PropertyStatusMessageFlags = {
      Circuit: [],
      Pump: [],
      IntelliChlor: [],
      IntelliChem: [],
      HeaterUltraT: []
    };
    messageProcessor.ProcessNotification(JSON.parse(SpecHelper.JSONdata.PageSubscriptions.SystemInformation));
    systemInformationController = $controller("SystemInformationController", {$scope: $scope});
    spyOn($location, 'path').and.returnValue('/systemInformation');
    $httpBackend.expectGET('/app/templates/systemInformation.html').respond(200);

  }));

  // describe('Selected Pool Name', function () {
  //   it('should display the proper pool name', function () {
  //     // expect($scope.installationNames.PropertyName.valueOf(''));
  //   });
  // });

  // describe('Initial Values', function () {
  //   it('should expect the following intial values ', function () {
  //     expect($scope.countries.length).toBe(3);
  //     expect($scope.StatusMessageTypeFlags).toBeDefined();
  //     expect($scope.phoneNumberPattern.toString()).toBeDefined();
  //     expect($scope.Property).toBeDefined();
  //     expect($scope.Model).toBeDefined();
  //     expect($scope.LocationModel.TIMZON).toBe('-5');
  //     expect($scope.timeZones.length).toBe(0);
  //     expect($scope.forms).toBeDefined();
  //     expect($scope.NotificationRecipients.length).toBe(0);
  //     expect($scope.SelectAllRecipients).toBe(false);
  //     expect($scope.ReciepientsSelected).toBe(false);
  //     expect($scope.longLatError).toBe('unresolved');
  //     expect($scope.installationNames.PropertyName).toBe('');
      // expect($scope.EditMode.LocationInfo).toBe(false);
      // expect($scope.EditMode.PersonalInfo).toBe(false);
      // expect($scope.EditMode.AdvancedSettings).toBe(false);
      // expect($scope.EditMode.NewRecipient).toBe(false);
      // expect($scope.NewRecipient).toEqual({
      //   PropertyId: '1',
      //   Name: null,
      //   Phone1: null,
      //   EmailAddress1: null,
      //   Phone2: null,
      //   EmailAddress2: null,
      //   UserId: null
      // });
      // expect($scope.selectedStatusMessageTypes).toEqual({
      //   Circuit: [],
      //   Pump: [],
      //   IntelliChlor: [],
      //   IntelliChem: [],
      //   HeaterUltraT: []
      // });
      // expect($scope.locationData).toEqual({
      //   LOCX: "",
      //   LOCY: "",
      //   Address: "",
      //   City: "",
      //   State: "",
      //   Zip: "",
      //   Country: ""
      // });
      // expect($scope.PreferenceData.MODE).toBe('ENGLISH');
      // expect($scope.PreferenceData.MANOVR).toBe('OFF');
      // expect($scope.PreferenceData.CLK24A).toBe('AMPM');
      //
      // expect($scope.timeData).toEqual({
      //   TIMZON: '-5',
      //   CLK24A: 'AMPM',
      //   DLSTIM: false,
      //   OFFSET: '',
      //   DAY: '',
      //   MIN: '',
      //   SOURCE: 'LOCAL',
      //   TimeZone: 'Eastern Standard Time'
      // });
  //
  //   });
  // });

  describe('SaveNewRecipient', function () {
    var deferred;
    beforeEach(function () {
      deferred = $q.defer();
      $scope.NewRecipient.Name = "Unit Test";
      spyOn(appService, 'PostData').and.callFake(function () {
        return {
          then: function (callback) {
            callback({things: 'and stuff'})
          }
        };
      });
      spyOn(appService, 'GetData').and.callFake(function () {
        return {
          then: function (callback) {
            callback({things: 'and stuff'})
          }
        };
      });
      $scope.forms = {
        newRep: {$valid: true}
      };
    });
    // it('should post to Notification Users if valid', function () {
    //   $scope.EditMode.NewRecipient = true;
    //   $scope.SaveNewRecipient();
    //   //$scope.$apply(deferred.resolve());
    //
    //
    //   expect(appService.PostData.calls.all()[0].args[0]).toBe('Api/NotificationUsers/');
    //   expect(appService.PostData.calls.all()[0].args[1].PropertyId).toBe('1');
    //   expect(appService.PostData.calls.all()[0].args[1].Name).toBe('Unit Test');
    //   expect(appService.GetData).toHaveBeenCalledWith('Api/NotificationUsers/1');
    //   expect($scope.EditMode.NewRecipient).toBe(false);
    // });
    // it('should Not post to Notification Users if not valid', function () {
    //   $scope.EditMode.NewRecipient = true;
    //   $scope.forms.newRep.$valid = false;
    //
    //   $scope.SaveNewRecipient();
    //
    //   expect(appService.PostData).not.toHaveBeenCalled();
    //   expect($scope.EditMode.NewRecipient).toBe(true);
    // });
  });


  describe('SaveInstallationNames', function () {
    var deferred;
    beforeEach(function () {
      deferred = $q.defer();
      $scope.installationNames.PropertyName = "Unit Test";
      $scope.installationNames.OwnerName = "Unit Guy";
      spyOn(appService, 'GetData').and.callFake(function () {
        return {
          then: function (callback) {
            callback({things: 'and stuff'})
          }
        };
      });
      spyOn(appService, 'PostData').and.callFake(function () {
        return {
          then: function (callback) {
            callback({things: 'and stuff'})
          }
        };
      });
      $scope.forms = {
        newRep: {$valid: true}
      };
      spyOn(propertyService, 'FetchInstallations');
      spyOn(propertyService, 'GetSystemInformation');
      $scope.forms = {
        names: {$valid: true}
      };
    });
    // it('should post to name change to Server if form is valid', function () {
    //   $scope.EditMode.PersonalInfo = true;
    //
    //   $scope.SaveInstallationNames();
    //
    //   expect(appService.PostData.calls.all()[0].args[0]).toBe('Api/Installations/NameChange/');
    //   expect(appService.PostData.calls.all()[0].args[1].Name).toBe('Unit Test');
    //   expect(appService.PostData.calls.all()[0].args[1].OwnerName).toBe('Unit Guy');
    //
    //   expect(propertyService.GetSystemInformation).toHaveBeenCalled();
    //
    //   expect($scope.EditMode.PersonalInfo).toBe(false);
    // });
    // it('should Not post to name Change to server if form is not valid', function () {
    //   $scope.EditMode.PersonalInfo = true;
    //   $scope.forms.names.$valid = false;
    //
    //   $scope.SaveInstallationNames();
    //
    //   expect(propertyService.FetchInstallations).not.toHaveBeenCalled();
    //   expect(appService.PostData).not.toHaveBeenCalled();
    //   expect($scope.EditMode.PersonalInfo).toBe(true);
    // });
  });

  describe('CancelSaveInstallationNames', function () {
    beforeEach(function () {
      $scope.installationNames.PropertyName = "Unit Test";
      $scope.installationNames.OwnerName = "Unit Guy";
      $scope.EditMode.PersonalInfo = true;
    });
    // it('should reset installation Name data to the initial values', function () {
    //   $scope.CancelSaveInstallationNames();
    //   expect($scope.installationNames.PropertyName).toBe('Dev pool');
    //   expect($scope.installationNames.OwnerName).toBe('John Doe');
    //   expect($scope.EditMode.PersonalInfo).toBe(false);
    // });
  });


  describe('CancelNewRecipient', function () {
    beforeEach(function () {
      $scope.NewRecipient.Name = "Unit Guy";
      $scope.NewRecipient.EmailAddress1 = "test@gmail.com";
      $scope.NewRecipient.Phone1 = "111-111-1111";
      $scope.EditMode.NewRecipient = true;
      var element = angular.element(
        '<form name="forms.newRep">' +
        '<input ng-model="model.somenum" name="somenum" integer />' +
        '</form>'
      );
      $scope.model = {somenum: null};
      $compile(element)($scope);
    });
    // it('should reset add new recipient form data to the initial values', function () {
    //   $scope.CancelNewRecipient();
    //   expect($scope.NewRecipient.Name).toBe('');
    //   expect($scope.NewRecipient.EmailAddress1).toBe('');
    //   expect($scope.NewRecipient.Phone1).toBe('');
    //   expect($scope.EditMode.NewRecipient).toBe(false);
    //   $scope.CancelNewRecipient();
    // });
  });

  describe('CancelLocationEdit', function () {
    beforeEach(function () {
      $scope.LocationModel.Address = '111 Main Street';
      $scope.LocationModel.City = 'Detroit';
      $scope.LocationModel.State = 'MI';
      $scope.LocationModel.ZIP = '48234';
      $scope.EditMode.LocationInfo = true;
    });
    // it('should reset location data to the initial values', function () {
    //   $scope.CancelLocationEdit();
    //   expect($scope.AddressModel.ADDRESS).toBe('Address');
    //   expect($scope.AddressModel.CITY).toBe('Chicago');
    //   expect($scope.AddressModel.STATE).toBe('IL');
    //   expect($scope.AddressModel.ZIP).toBe('60654');
    //   expect($scope.EditMode.LocationInfo).toBe(false);
    // });
  });

  describe('RemoveRecipients', function () {
    beforeEach(function () {
      spyOn(appService, 'GetData').and.callFake(function () {
        return {
          then: function (callback) {
            callback({things: 'and stuff'})
          }
        };
      });
      spyOn(appService, 'DeleteData').and.callFake(function () {
        return {
          then: function (callback) {
            callback({things: 'and stuff'})
          }
        };
      });
      $scope.SelectAllRecipients = true;
    });
    // it('should call appservice.DeleteData with the selected recipient Ids', function () {
    //   $scope.NotificationRecipients = [{
    //     RecipientId: 1,
    //     Selected: false
    //   }, {
    //     RecipientId: 2,
    //     Selected: true
    //   }, {
    //     RecipientId: 3,
    //     Selected: true
    //   }];
    //
    //   $scope.RemoveRecipients();
    //
    //   expect(appService.DeleteData.calls.all()[0].args[0]).toBe('Api/NotificationUsers/');
    //   expect(appService.DeleteData.calls.all()[0].args[1].recipientId).toEqual([2, 3]);
    //   expect(appService.GetData).toHaveBeenCalledWith('Api/NotificationUsers/1');
    //   expect($scope.SelectAllRecipients).toBe(false);
    // });

    // it('should Not call appservice.DeleteData if no recipient Ids are selected', function () {
    //   $scope.NotificationRecipients = [{
    //     RecipientId: 1,
    //     Selected: false
    //   }, {
    //     RecipientId: 2,
    //     Selected: false
    //   }, {
    //     RecipientId: 3,
    //     Selected: false
    //   }];
    //
    //   $scope.RemoveRecipients();
    //
    //   expect(appService.DeleteData).not.toHaveBeenCalled();
    //   expect(appService.GetData).not.toHaveBeenCalled();
    //   expect($scope.SelectAllRecipients).toBe(false);
    // });
  });


  describe('SaveLocationSettings', function () {
    beforeEach(function () {
      $scope.LocationModel.Address = '111 Main Street';
      $scope.LocationModel.City = 'Detroit';
      $scope.LocationModel.State = 'MI';
      $scope.LocationModel.ZIP = '48234';
      $scope.EditMode.LocationInfo = true;

      spyOn(propertyService, 'SetParams').and.callFake(function () {
        return {
          then: function (callback) {
            callback({things: 'and stuff'})
          }
        };
      });
      spyOn(propertyService, 'GetUserSettings').and.callFake(function () {
        return {
          then: function (callback) {
            callback({things: 'and stuff'})
          }
        };
      });
      $scope.forms = {
        location: {$valid: true, zip: {$invalid: false}}
      };
    });
    // it('should post to Notification Users if valid', function () {
    //   $scope.EditMode.NewRecipient = true;
    //   $scope.SaveLocationSettings();
    //   expect(propertyService.SetParams.calls.all()[0].args[0]).toBe('_C10C');
    //   expect(propertyService.SetParams.calls.all()[0].args[1]).toEqual({
    //     CLK24A: 'AMPM',
    //     DLSTIM: 'OFF',
    //     LOCX: '41.895485',
    //     LOCY: '87.634880',
    //     TIMZON: '-5',
    //     ZIP: '60654'
    //   });
    //   expect(propertyService.SetParams.calls.all()[1].args[0]).toBe('_5451');
    //   expect(propertyService.SetParams.calls.all()[1].args[1]).toEqual({
    //     ADDRESS: 'Address',
    //     CITY: 'Chicago',
    //     COUNTRY: undefined,
    //     STATE: 'IL',
    //     TIMZON: '-5',
    //     ZIP: '60654'
    //   });
    //   expect(propertyService.GetUserSettings).toHaveBeenCalled();
    //   expect($scope.EditMode.LocationInfo).toBe(false);
    // });
    // it('should Not post to Notification Users if not valid', function () {
    //   $scope.EditMode.NewRecipient = true;
    //   $scope.forms.location.$valid = false;
    //
    //   $scope.SaveLocationSettings();
    //
    //   expect(propertyService.SetParams).not.toHaveBeenCalled();
    //   expect(propertyService.GetUserSettings).not.toHaveBeenCalled();
    //   expect($scope.EditMode.LocationInfo).toBe(true);
    // });
  });

  describe('FetchLongLat', function () {
    beforeEach(function () {
      $scope.longLatError = 'Error';
      $scope.timeZoneError = 'Error';
      $scope.LocationModel.LOCX = '1';
      $scope.LocationModel.LOCY = '2';
      $scope.AddressModel.ZIP = '48234';
      spyOn(appService, 'GetData').and.callFake(function (endpoint) {
        return {
          then: function (callback) {
            var result = {};
            if (endpoint.indexOf('48234') >= 0) {
              result = {
                data: {
                  item: {
                    Latitude: 'Y',
                    Longitude: 'X',
                    TimeZone: '5'
                  }
                }
              };
            } else {
              result = {
                data: {
                  Error: 'ServiceError'
                }
              };
            }
            callback(result)
          }
        };
      });
      $scope.forms = {
        location: {$valid: true, zip: {$invalid: false}}
      };
    });

    // it('should fetch lat long and timezone from the server and populate if found', function () {
    //
    //   $scope.FetchLongLat();
    //
    //   expect(appService.GetData.calls.all()[0].args[0]).toBe('Api/zipcodes?zipCode=48234');
    //
    //   expect($scope.LocationModel.LOCY).toBe('Y');
    //   expect($scope.LocationModel.LOCX).toBe('X');
    //   expect($scope.timeData.TimeZone).toBe('Eastern Standard Time');
    //   expect($scope.longLatError).toBe('');
    // });
    //
    // it('should fetch lat long and timezone from the server and show error if an error is returned from service', function () {
    //   $scope.AddressModel.ZIP = 'abc';
    //   $scope.FetchLongLat();
    //
    //   expect(appService.GetData.calls.all()[0].args[0]).toBe('Api/zipcodes?zipCode=abc');
    //
    //   expect($scope.LocationModel.LOCY).toBe('');
    //   expect($scope.LocationModel.LOCX).toBe('');
    //   expect($scope.timeData.TimeZone).toBe('');
    //   expect($scope.longLatError).toBe('ServiceError');
    //   expect($scope.timeZoneError).toBe('ServiceError');
    // });

  });

  describe('timezoneClicked', function () {
    beforeEach(function () {
      $scope.timeData.TimeZone = 'Western TimeZone';
    });
    // it('should populate the timezone with the correct string value', function () {
    //   $scope.timezoneClicked(-5);
    //   expect($scope.timeData.TimeZone).toBe('Eastern Standard Time');
    // });
  });

  describe('SavePreferenceSettings', function () {
    beforeEach(function () {
      $scope.PreferenceData.MODE = 'METRIC';
      $scope.PreferenceData.MANOVR = 'OFF';
      $scope.PreferenceData.CLK24A = '24HR';
      $scope.PreferenceData.MIN = '1:23';
      $scope.PreferenceData.DAY = '1/1/11';
      $scope.EditMode.AdvancedSettings = true;
      spyOn(propertyService, 'SetParams');
    });
    // it('should save prefrence data', function () {
    //   $scope.SavePreferenceSettings();
    //
    //   expect($scope.EditMode.AdvancedSettings).toBe(false);
    //
    //   expect(propertyService.SetParams.calls.all()[0].args[0]).toBe('_5451');
    //   expect(propertyService.SetParams.calls.all()[0].args[1].MODE).toBe('METRIC');
    //
    //   expect(propertyService.SetParams.calls.all()[1].args[0]).toBe('_CFEA');
    //   expect(propertyService.SetParams.calls.all()[1].args[1].MANOVR).toBe('OFF');
    //
    //   expect(propertyService.SetParams.calls.all()[2].args[0]).toBe('_C10C');
    //   expect(propertyService.SetParams.calls.all()[2].args[1].CLK24A).toBe('24HR');
    // });
  });

  describe('SaveBadPreferenceSettings', function () {
    beforeEach(function () {
      $scope.PreferenceData.MODE = 'METRIC';
      $scope.PreferenceData.MANOVR = 'OFF';
      $scope.PreferenceData.CLK24A = 'AMPM';
      $scope.PreferenceData.MIN = '1:23';
      $scope.PreferenceData.DAY = '1/1/11';
      $scope.EditMode.AdvancedSettings = true;
      spyOn(propertyService, 'SetParams');
    });

    // it('should save prefrence data', function () {
    //   $scope.SavePreferenceSettings();
    //
    //   expect($scope.EditMode.AdvancedSettings).toBe(true);
    // });
  });

  describe('SaveGoodRemotePreferenceSettings', function () {
    beforeEach(function () {
      $scope.PreferenceData.SOURCE = 'URL';
      $scope.PreferenceData.MODE = 'METRIC';
      $scope.PreferenceData.MANOVR = 'OFF';
      $scope.PreferenceData.CLK24A = 'AMPM';
      $scope.PreferenceData.MIN = '1:23';
      $scope.PreferenceData.DAY = '1/1/11';
      $scope.EditMode.AdvancedSettings = true;
      spyOn(propertyService, 'SetParams');
    });
    // it('should save prefrence data', function () {
    //   $scope.SavePreferenceSettings();
    //
    //   expect($scope.EditMode.AdvancedSettings).toBe(false);
    //
    //   expect(propertyService.SetParams.calls.all()[0].args[0]).toBe('_5451');
    //   expect(propertyService.SetParams.calls.all()[0].args[1].MODE).toBe('METRIC');
    //
    //   expect(propertyService.SetParams.calls.all()[1].args[0]).toBe('_CFEA');
    //   expect(propertyService.SetParams.calls.all()[1].args[1].MANOVR).toBe('OFF');
    //
    //   expect(propertyService.SetParams.calls.all()[2].args[0]).toBe('_C10C');
    //   expect(propertyService.SetParams.calls.all()[2].args[1].CLK24A).toBe('AMPM');
    // });
  });

  describe('CancelPreferencesEdit', function () {
    beforeEach(function () {
      $scope.PreferenceData.MODE = 'TEST';
      $scope.PreferenceData.MANOVR = 'TEST1';
      $scope.PreferenceData.CLK24A = 'TEST2';
      $scope.EditMode.AdvancedSettings = true;
    });
    // it('should reset Preference data to the initial values', function () {
    //   $scope.CancelPreferencesEdit();
    //   expect($scope.PreferenceData.MODE).toBe('ENGLISH');
    //   expect($scope.PreferenceData.MANOVR).toBe('OFF');
    //   expect($scope.PreferenceData.CLK24A).toBe('AMPM');
    //   expect($scope.EditMode.LocationInfo).toBe(false);
    // });
  });

  describe('ToggleSelectAllRecipients', function () {
    beforeEach(function () {
      $scope.NotificationRecipients = [{
        RecipientId: 1,
        Selected: false
      }, {
        RecipientId: 2,
        Selected: false
      }];
      $scope.SelectAllRecipients = false;
    });
    // it('should mark all notification recipients as selected', function () {
    //   $scope.ToggleSelectAllRecipients();
    //   expect($scope.SelectAllRecipients).toBe(true);
    //   expect($scope.NotificationRecipients[0].Selected).toBe(true);
    //   expect($scope.NotificationRecipients[1].Selected).toBe(true);
    // });
  });

  describe('UpdateSelectedRecipients', function () {
    beforeEach(function () {

      $scope.ReciepientsSelected = false
    });
    // it('should show that at least one recipient was selected', function () {
    //   $scope.NotificationRecipients = [{
    //     RecipientId: 1,
    //     Selected: true
    //   }, {
    //     RecipientId: 2,
    //     Selected: false
    //   }];
    //
    //   $scope.UpdateSelectedRecipients();
    //   expect($scope.ReciepientsSelected).toBe(true);
    // });
    // it('should show that no recipient was selected', function () {
    //   $scope.NotificationRecipients = [{
    //     RecipientId: 1,
    //     Selected: false
    //   }, {
    //     RecipientId: 2,
    //     Selected: false
    //   }];
    //   $scope.UpdateSelectedRecipients();
    //   expect($scope.ReciepientsSelected).toBe(false);
    // });
  });

  describe('ToggleEditMode', function () {
    beforeEach(function () {

    });
    // it('should mark enter into location edit mode and reset location model', function () {
    //   $scope.EditMode.LocationInfo = false;
    //   $scope.LocationModel.Address = '111 Main Street';
    //
    //   $scope.ToggleEditMode('LocationInfo');
    //   expect($scope.LocationModel.Address).toBe('');
    //   expect($scope.EditMode.LocationInfo).toBe(true);
    // });
    //
    // it('should mark enter into personalInfo edit mode and reset personalInfo model', function () {
    //   $scope.EditMode.PersonalInfo = false;
    //   $scope.installationNames.PropertyName = 'new Name';
    //
    //   $scope.ToggleEditMode('PersonalInfo');
    //   expect($scope.installationNames.PropertyName).toBe('Dev pool');
    //   expect($scope.EditMode.PersonalInfo).toBe(true);
    // });
    //
    // it('should mark enter into AdvancedSettings edit mode and reset AdvancedSettings model', function () {
    //   $scope.EditMode.AdvancedSettings = false;
    //
    //   $scope.ToggleEditMode('AdvancedSettings');
    //   expect($scope.EditMode.AdvancedSettings).toBe(true);
    // });
    //
    // it('should mark enter into NewRecipient edit mode and reset NewRecipient model', function () {
    //   $scope.EditMode.NewRecipient = false;
    //   $scope.NewRecipient.Name = 'new Name';
    //
    //   $scope.ToggleEditMode('NewRecipient');
    //   expect($scope.NewRecipient.Name).toBe('');
    //   expect($scope.EditMode.NewRecipient).toBe(true);
    // });


    // it('should mark enter into StatusMessageFlags edit mode and reset StatusMessageFlags model', function () {
    //   $scope.EditMode.StatusMessageFlags = false;
    //   $scope.selectedStatusMessageTypes.Circuit = ['a', 'm', 'e'];
    //
    //   $scope.ToggleEditMode('StatusMessageFlags');
    //   expect($scope.selectedStatusMessageTypes.Circuit).toEqual([]);
    //   expect($scope.EditMode.StatusMessageFlags).toBe(true);
    // });
  });

  describe('SaveStatusMessageFlags', function () {
    beforeEach(function () {
      $scope.selectedStatusMessageTypes.Circuit = ['a', 'm', 'e'];
      $scope.selectedStatusMessageTypes.Pump = ['b', 'm', 'e'];
      $scope.selectedStatusMessageTypes.IntelliChlor = ['c', 'm', 'e'];
      $scope.selectedStatusMessageTypes.IntelliChem = ['d', 'm', 'e'];
      $scope.selectedStatusMessageTypes.HeaterUltraT = ['e', 'm', 'e'];
      $scope.EditMode.StatusMessageFlags = true;
      spyOn(appService, 'GetData').and.callFake(function () {
        return {
          then: function (callback) {
            callback({things: 'and stuff'})
          }
        };
      });
      spyOn(propertyService, 'SetCommand').and.callFake(function () {
        return {
          then: function (callback) {
            callback({things: 'and stuff'})
          }
        };
      });
    });
    // it('should send and save status message flags', function () {
    //   $scope.SaveStatusMessageFlags();
    //
    //   expect($scope.EditMode.StatusMessageFlags).toBe(false);
    //
    //   expect(propertyService.SetCommand.calls.all()[0].args[0]).toBe('SetStatusMessageFlags');
    //   expect(propertyService.SetCommand.calls.all()[0].args[1].CIRCUIT).toBe('ame');
    //   expect(propertyService.SetCommand.calls.all()[0].args[1].PUMP).toBe('bme');
    //   expect(propertyService.SetCommand.calls.all()[0].args[1].ICHLOR).toBe('cme');
    //   expect(propertyService.SetCommand.calls.all()[0].args[1].ICHEM).toBe('dme');
    //   expect(propertyService.SetCommand.calls.all()[0].args[1].HEATER).toBe('eme');
    // });
  });


  describe('ClearStatusMessageFlags', function () {
    beforeEach(function () {
      $scope.selectedStatusMessageTypes.Circuit = ['a', 'm', 'e'];
      $scope.selectedStatusMessageTypes.Pump = ['b', 'm', 'e'];
      $scope.selectedStatusMessageTypes.IntelliChlor = ['c', 'm', 'e'];
      $scope.selectedStatusMessageTypes.IntelliChem = ['d', 'm', 'e'];
      $scope.selectedStatusMessageTypes.HeaterUltraT = ['e', 'm', 'e'];
    });
    // it('should clear all statusMessgeFlags', function () {
    //   $scope.ClearStatusMessageFlags();
    //   expect($scope.selectedStatusMessageTypes).toEqual({
    //     Circuit: [],
    //     Pump: [],
    //     IntelliChlor: [],
    //     IntelliChem: [],
    //     HeaterUltraT: []
    //   });
    // });
  });

  describe('SelectAllStatusMessageFlags', function () {
    beforeEach(function () {
      $scope.NotificationRecipients = [{
        RecipientId: 1,
        Selected: false
      }, {
        RecipientId: 2,
        Selected: false
      }];
      $scope.SelectAllRecipients = false;
    });
    // it('should mark all status messages as selected', function () {
    //   $scope.SelectAllStatusMessageFlags();
    //
    //   expect($scope.selectedStatusMessageTypes.Circuit).toEqual(['f']);
    //   expect($scope.selectedStatusMessageTypes.Pump).toEqual(['w', 'f', 'e', 'v', 'i', 'o', 'u', 'b', 'p', 'a']);
    //   expect($scope.selectedStatusMessageTypes.IntelliChlor).toEqual(['l', 'o', 'c', 'e', 'd', 'v', 'i', 'f']);
    //   expect($scope.selectedStatusMessageTypes.IntelliChem).toEqual(['e', 'h', 'o', 'm', 'a', 'c', 'b', 'd', 'f', 'g', 'i', 'j', 'k', 'l', 'n', 'p']);
    //   expect($scope.selectedStatusMessageTypes.HeaterUltraT).toEqual(['h', 'l', 'w', 'r', 'o', 's', 'f', 't', 'b', 'p', 'g']);
    // });
  });

  describe('CancelStatusMessageFlagEdit', function () {
    beforeEach(function () {
      $scope.selectedStatusMessageTypes.Circuit = ['a', 'm', 'e'];
      $scope.selectedStatusMessageTypes.Pump = ['b', 'm', 'e'];
      $scope.selectedStatusMessageTypes.IntelliChlor = ['c', 'm', 'e'];
      $scope.selectedStatusMessageTypes.IntelliChem = ['d', 'm', 'e'];
      $scope.selectedStatusMessageTypes.HeaterUltraT = ['e', 'm', 'e'];

      $scope.EditMode.StatusMessageFlags = true;
    });
    // it('should reset Preference data to the initial values', function () {
    //   $scope.CancelStatusMessageFlagEdit();
    //
    //   expect($scope.selectedStatusMessageTypes).toEqual({
    //     Circuit: [],
    //     Pump: [],
    //     IntelliChlor: [],
    //     IntelliChem: [],
    //     HeaterUltraT: []
    //   });
    //   expect($scope.EditMode.StatusMessageFlags).toBe(false);
    // });
  });

  describe('Trigger scope events', function () {
    // it('should trigger the events', function () {
    //   $rootScope.$broadcast('ObjectUpdated');
    //   $rootScope.$broadcast('SecurityUpdated');
    //   $rootScope.$broadcast('PropertyStatusMessageFlagsUpdated');
    //   $rootScope.$broadcast('SystemInformationUpdated');
    //   $rootScope.$broadcast('NotificationRecipientUpdated');
    //   $rootScope.$broadcast('SystemConfigUpdated');
    // });
  });

  describe('security setting funtionality', function () {
    beforeEach(function () {

    });
    // it('should cancel out of a edit', function () {
    //   $scope.CancelSecuritySettings();
    // });
    // it('should save the settings', function () {
    //   $scope.SaveSecuritySettings();
    //   $scope.Security.SecurityEnable = true;
    //   $scope.SaveSecuritySettings();
    //   $scope.ToggleEditMode('Security')
    // });
  });

});
