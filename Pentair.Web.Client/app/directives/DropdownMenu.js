var PWC = PWC || {};
PWC.directive('dropdownMenu', [function () {
    return {
        restrict: 'A',
        replace: true,
        scope: {
            dropdownMenu: '=',
            dropdownModel: '=',
            dropdownOnchange: '&',
            defaultValue: '=',
            disabled: '='
        },
        controller: [
            '$scope', '$element', '$attrs', '$window', '$location', function (scope, $element, $attrs, $window, $location) {
                var bodyEl = angular.element($window.document);

                scope.labelField = $attrs.labelField || 'name';
                scope.valueField = $attrs.valueField || 'value';
                scope.limitTo = $attrs.limitTo || 20;
                scope.showAlternativeSelector = $attrs.showAlternativeSelector || false;
                scope.selected = scope.defoldToggleSwitchaultValue;

                //Update the UI value if model changes
                scope.$watch('dropdownModel + dropdownMenu', function () {
                    //if more than 7 Installations show Alternative Selector
                    if (scope.dropdownMenu.length > 7 && scope.dropdownMenu[0].hasOwnProperty('PoolName')) {
                        scope.showAlternativeSelector = true;
                    }
                    for (var i = 0; i < scope.dropdownMenu.length; i++) {
                        switch (scope.dropdownModel || '0') {
                            case scope.dropdownMenu[i][scope.valueField]:
                                scope.selected = scope.dropdownMenu[i];
                                break;
                            case '0':
                                scope.selected = scope.defaultValue;
                                break;
                        }
                    }
                });

                scope.GoToChangeInstallationsGrid = function () {
                    $location.path("/installationGrid");
                }
                this.select = function (selected) {
                    scope.dropdownModel = selected[scope.valueField];
                    if (scope.dropdownOnchange)
                        scope.dropdownOnchange({ selection: scope.dropdownModel });
                };


                $element.bind('click', elemClick);


                function elemClick(event) {
                    if (event)
                        event.stopPropagation();

                    if (!scope.disabled)
                        $element.toggleClass('active');

                    if ($element.hasClass('active')) {
                        bodyEl.on('click', onBodyClick);
                    } else {
                        bodyEl.off('click', onBodyClick);
                    }
                }


                function onBodyClick(evt) {
                    if (evt.target === $element[0]) return;
                    return evt.target !== $element[0] && elemClick();
                }
            }
        ],
        template: "<div class='wrap-dd-select'>" +
            "<span class='selected'>{{selected[labelField]}}</span>" +
            "<ul class='dropdown'style='line-height:1; max-height:240px; overflow-y:auto; font-size:18px;'>" +
            "<li ng-if='showAlternativeSelector'><a ng-click='GoToChangeInstallationsGrid()' href=''>Search/sort pool systems </a>" +
            "</li><li ng-repeat='item in dropdownMenu |limitTo:limitTo' class='dropdown-item' dropdown-menu-item='item' dropdown-item-label='labelField'>" +
            "</li></ul></div>"
    };
}
]).directive('dropdownMenuItem', [
    function () {
        return {
            require: '^dropdownMenu',
            replace: true,
            scope: {
                dropdownItemLabel: '=',
                dropdownMenuItem: '=',
            },
            link: function (scope, element, attrs, dropdownMenuCtrl) {
                scope.selectItem = function () {
                    dropdownMenuCtrl.select(scope.dropdownMenuItem);
                };

                scope.dropdownItemClass = scope.dropdownMenuItem.OnLine == false ? 'notConnected' : 'connected';
            },
            template: "<li ng-class='{divider: dropdownMenuItem.divider}'>\n    <a href='' class='dropdown-item {{dropdownItemClass}}'\n        ng-if='!dropdownMenuItem.divider'\n        ng-href='{{dropdownMenuItem.href}}'\n        ng-click='selectItem()'>\n        {{dropdownMenuItem[dropdownItemLabel]}}\n    </a>\n</li>"
        };
    }
]);
