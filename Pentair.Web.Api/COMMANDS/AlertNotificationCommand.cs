﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;

using Newtonsoft.Json;

namespace Pentair.Web.Api
{
    // 1.0.0.14
    public class AlertNotificationCommand : CommandBase
    {
        /// <summary>
        /// Server should never receive 2 and 3, shall be logged as error.
        /// </summary>
        public enum Category
        {
            Error = 0,
            Warning = 1,
            Information = 2,
            OK = 3
        }

        [JsonProperty("MODE", Required = Required.Always, Order = 10)]
        public Category Mode { get; private set; }

        [JsonProperty("OBJNAM", Required = Required.Always, Order = 11)]
        public string ObjName { get; private set; }

        [JsonProperty("OBJTYP", Required = Required.Always, Order = 12)]
        public string ObjType { get; private set; }

        [JsonProperty("PARENT", Required = Required.Always, Order = 13)]
        public string Parent { get; private set; }

        /// <summary>
        /// Alert short description like "Communication lost", etc.
        /// </summary>
        [JsonProperty("SNAME", Required = Required.Always, Order = 14)]
        public string Alert { get; private set; }

        [JsonProperty("TIME", Required = Required.Always, Order = 15)]
        public long TimeStamp { get; private set; }

        public AlertNotificationCommand(
            string command,
            string messageID,
            int mode,
            string objName,
            string objType,
            string parent,
            string alert,
            long timeStamp
            )
            : base(command, messageID)
        {
            this.Mode = (Category)mode;
            this.ObjName = objName;
            this.ObjType = objType;
            this.Parent = parent;
            this.Alert = alert;
            this.TimeStamp = timeStamp;

        } // AlertNotificationCommand()

        /// <exception cref="Exception" />
        public static AlertNotificationCommand AlertNotificationCommandFactory(
            CommandType commandType,
            string serializedMessage)
        {
            AlertNotificationCommand alertNotificationCommand;

            switch(commandType)
            {
                case CommandType.AlertOn:
                    alertNotificationCommand = AlertNotificationOn.Deserialize(serializedMessage);
                    break;

                case CommandType.AlertOff:
                    alertNotificationCommand = AlertNotificationOff.Deserialize(serializedMessage);
                    break;

                default:
                    throw new Exception("Unsupported alert notification type: " + commandType);

            } // switch (commandType)

            return alertNotificationCommand;

        } // AlertNotificationCommandFactory()

        /// <summary>
        /// Always returns an object, valid or not. If invalid, check the DeserializationError property value.
        /// </summary>
        public static new AlertNotificationCommand Deserialize(
            string serializedMessage)
        {
            AlertNotificationCommand command = null;

            try
            {
                command = JsonConvert.DeserializeObject<AlertNotificationCommand>(serializedMessage);
                command.IsValid = true;
            }
            catch (Exception e)
            {
                // Deserialization as AlertNotificationCommand failed

                StringBuilder sbError = new StringBuilder();

                sbError.Append("Cannot deserialize as AlertNotificationCommand. " + e.Message);

                // Try to get at least the base message data
                CommandBase commandBase = CommandBase.Deserialize(serializedMessage);

                if (!commandBase.IsValid)
                {
                    sbError.Append("\n" + commandBase.DeserializationError);
                }

                command = new AlertNotificationCommand(
                    command: commandBase.Command,
                    messageID: commandBase.MessageID,
                    mode: 0, // Error
                    objName: String.Empty,
                    objType: String.Empty,
                    parent: String.Empty,
                    alert: String.Empty,
                    timeStamp: 0L)
                {
                    IsValid = false,
                    DeserializationError = sbError.ToString()
                };
            }

            return command;

        } // Deserialize()

    } // class AlertNotificationCommand
}