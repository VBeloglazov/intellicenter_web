﻿describe('ScheduleController', function () {
    var Controller;
    var $scope;
    var $controller;
    var $httpBackend;
    var propertyService;
    var authService;
    var enumerations;
    var $filter;
    var body;
    var $location;
    var propertyModel;
    var messageProcessor;
    var deferred;
    beforeEach(module('PentairWebClient'));

    beforeEach(inject(function (_$controller_, _$rootScope_, _$httpBackend_, _$filter_, _$location_, _$q_, _PropertyService_, _AuthService_, _Enumerations_, _PropertyModel_, _MessageProcessor_) {
        $scope = _$rootScope_.$new();
        $controller = _$controller_;
        $httpBackend = _$httpBackend_;
        enumerations = _Enumerations_;
        authService = _AuthService_;
        propertyService = _PropertyService_;
        propertyModel = _PropertyModel_;
        propertyModel.Objects["_C10C"] = { OBJNAM: "_C10C", OBJTYP: "SYSTEM", TIMZON: "-4", MIN: "10:00" }
        $location = _$location_;
        $filter = _$filter_;
        messageProcessor = _MessageProcessor_;
        $q = _$q_;
        deferred = $q.defer();

        messageProcessor.ProcessNotification(JSON.parse(SpecHelper.JSONdata.Bodies));

        messageProcessor.ProcessNotification(JSON.parse(SpecHelper.JSONdata.Circuits));

        Controller = $controller("ScheduleController", { $scope: $scope });
    }));


    describe('Initial Variables', function () {
        var date;
        beforeEach(function () {
            date = new Date();
        });

        it('should have the folloing initial state', function () {
            expect($scope.Property).toBe(propertyService);
            expect($scope.Authentication).toBe(authService);
            expect($scope.Enumerations).toBe(enumerations);
            // edit mode is no longer a thing because of PPA-2
            //expect($scope.EditMode).toBe(false);
        });
    });

    describe('Edit Mode', function () {
        beforeEach(function () {
            $scope.AddSchedule();
            body = propertyModel.Bodies[0];
        });
        // TODO for some reason this isn't equalling even though it appears to be the same
        // might go back here and do each field compare separately to track it down
        // it('should create a new schedule object', function () {
        //     expect($scope.EditMode).toBe(true);
        //     expect($scope.SelectedSchedule).toEqual({
        //         OBJNAM: null,
        //         OBJTYP: 'SCHED',
        //         SNAME: '',
        //         LISTORD: '',
        //         CIRCUIT: '',
        //         DAY: '',
        //         START: 'ABSTIM',
        //         TIME: '',
        //         STOP: 'ABSTIM',
        //         SINGLE: 'OFF',
        //         TIMOUT: '',
        //         GROUP: '',
        //         HEATER: '',
        //         HITMP: '',
        //         LOTMP: '',
        //         SMTSRT: 'OFF',
        //         days: ['M', 'T', 'W', 'R', 'F', 'A', 'U' ],
        //         heatMode: '',
        //         startTime: jasmine.any(Date),
        //         stopTime: jasmine.any(Date),
        //         bodyObject: {},
        //         body: false,
        //         smartStart: false,
        //         deleteFlag: false,
        //         open: true
        //     });
        // });
        // TODO the structure has changed for TypeChanged
        // like {"OBJNAM":"C64BC","OBJTYP":"CIRCUIT","HNAME":"Pool","SNAME":"Pool","SHOMNU":"csrepvhzmobw","STATUS":"OFF","SUBTYP":"POOL","SOURCE":"","TIMOUT":"0","USAGE":"USAGE","ACT":"ACT","FREEZE":"ON","FEATR":"ON","LISTORD":"-1","DNTSTP":"OFF","TIME":"720","BODY":"BAE32","RLY":"r0005","Delay":{"VISIBLE":false,"MESSAGE":""},"MODE":"MODE","LIMIT":"LIMIT","USE":"USE","MANUAL":"OFF","CHILD":"CHILD","$$hashKey":"object:399"}"
        // it('should copy the body object to the Schedule', function () {
        //     $scope.CircuitSelected(body);
        //     expect($scope.SelectedSchedule.bodyObject).toEqual(body);
        // });
        // it('should not make a refrence to the body object', function () {
        //     $scope.CircuitSelected(body.OBJNAM);
        //     expect($scope.SelectedSchedule.bodyObject).not.toBe(body);
        // });
        //
        // it('should not copy a object that is not a body', function () {
        //     $scope.CircuitSelected(propertyModel.Circuits[0].OBJNAM);
        //     expect($scope.SelectedSchedule.bodyObject).toEqual({});
        // });

        it('should set Schedule.Days to accept new list of days', function () {
            $scope.TypeChanged();
            expect($scope.SelectedSchedule.days).toEqual($scope.getCurrentDayLetter());
        });

        it('should cancel edit', function () {
            $scope.CancelEdit();
            // edit mode is no longer a thing because of PPA-2
            //expect($scope.EditMode).toBe(false);
        })
    });

    //describe('Edit schedule', function () {
    //    beforeEach(function () {
    //        propertyModel.Schedules = SpecHelper.mocks.Schedules;
    //        $rootScope.$broadcast('SchedulesUpdated', '');
    //        $scope.SetEditMode(SpecHelper.mocs.schedule1)
    //    });
    //    it('select the correct schedule for editing', function () {
    //        expect($scope.SelectedSchedule.OBJNAM).toBe('S1000');
    //    });

    //    it('should set Schedule.Days to accept new list of days', function () {
    //        $scope.TypeChanged();
    //        expect($scope.SelectedSchedule.days).toEqual([]);
    //    });

    //});

    //describe('Create New Schedule', function () {
    //    it('should reset errors and reset modal', function () {
    //        $scope.CreateNewSchedule();
    //        expect($scope.Modal).toBe('ADD');
    //    });
    //});

    //describe('Edit Schedule', function () {
    //    describe('Schedule item found', function () {
    //        beforeEach(function () {
    //            $scope.Schedule.ObjNam = 'S1001';
    //            $scope.EditSchedule(SpecHelper.mocks.Schedules.S1000);
    //        });

    //        it('should Populate Scope schedule with Schedule objects', function () {
    //            expect($scope.Schedule.ObjNam).toBe('S1000');
    //            expect($scope.Schedule.Circuit).toBe('B1000');
    //            expect($scope.Schedule.Days).toEqual(['M']);
    //            //expect($scope.Schedule.StartTime.getHours()).toBe(11);
    //            //expect($scope.Schedule.StopTime.getHours()).toBe(13);
    //            expect($scope.Schedule.Type).toBe(1);
    //            expect($scope.Schedule.StopMode).toBe('ABSTIM');
    //            expect($scope.Schedule.StartMode).toBe('ABSTIM');
    //            expect($scope.Schedule.Group).toBe('1');
    //            expect($scope.Schedule.Status).toBe('ON');
    //            expect($scope.Schedule.Body.OBJNAM).toBe('B1000');
    //        });

    //        it('should set Modal to EDIT', function () {
    //            expect($scope.Modal).toBe('EDIT');
    //        });

    //        it('should Split the days into and array', function () {
    //            SpecHelper.mocks.Schedules.S1000.DAY = 'MT';
    //            $scope.EditSchedule(SpecHelper.mocks.Schedules.S1000);
    //            expect($scope.Schedule.Days).toEqual(['M', 'T']);
    //        });


    //    });
    //    describe('Schedule not item found', function () {
    //        it('should not show edit modal if scheduled item is not found', function () {
    //            SpecHelper.mocks.Schedules.S1000.CIRCUIT = 'NOT-FOUND';
    //            $scope.EditSchedule(SpecHelper.mocks.Schedules.S1000);
    //            expect($scope.Modal).toBe('');
    //        });
    //});
    //});

    //describe('Check All Days', function () {
    //    it('should Set Schedule days to all days', function () {
    //        $scope.checkAllDays();
    //        expect($scope.Schedule.Days).toEqual(['M', 'T', 'W', 'R', 'F', 'A', 'U']);
    //    });
    //});

    //describe('Set temp', function () {
    //    it('should Set value of object to 55 ', function () {
    //        $scope.Schedule.Body = { HITMP: '44' };
    //        $scope.SetTemp($scope.Schedule.Body, 'HITMP', '55');
    //        expect($scope.Schedule.Body.HITMP).toBe('55');
    //    });
    //    it('should not set the value to 55 if Prop is not found', function () {
    //        $scope.Schedule.Body = { HITMP: '44' };
    //        expect($scope.Schedule.Body.HITMP).toBe('44');
    //    });
    //});

    //describe('Increase temp', function () {
    //    it('should Increase the string value by 1', function () {
    //        $scope.Schedule.Body = { OBJNAM: 'MOCK', HITMP: '44F' };
    //        $scope.IncreaseTemp($scope.Schedule.Body, 'HITMP', 99);
    //        expect($scope.Schedule.Body.HITMP).toBe(45);
    //    });
    //    it('should Increase the Number value by 1', function () {
    //        $scope.Schedule.Body = { OBJNAM: 'MOCK', HITMP: '44' };
    //        $scope.IncreaseTemp($scope.Schedule.Body, 'HITMP', 99);
    //        expect($scope.Schedule.Body.HITMP).toBe(45);
    //    });

    //    it('should not Increase the value by 1 if prop is not found', function () {
    //        $scope.Schedule.Body = { HITMP: '44' };

    //        $scope.IncreaseTemp($scope.Schedule.Body, 'HIMP', 90);
    //        expect($scope.Schedule.Body.HITMP).toBe('44');
    //        expect($scope.Schedule.Body.HIMP).not.toBeDefined();
    //    });
    //    it('should keep the value at the max 99 if the value was already 99', function () {
    //        $scope.Schedule.Body = { OBJNAM: 'MOCK', HITMP: '99' };
    //        $scope.IncreaseTemp($scope.Schedule.Body, 'HITMP', 99);
    //        expect($scope.Schedule.Body.HITMP).toBe('99');
    //    });

    //    it('should keep the value at 99 if no body object is provided', function () {
    //        $scope.Schedule.Body = { OBJNAM: 'MOCK', HITMP: '99' };
    //        $scope.IncreaseTemp(null, 'HITMP', 99);
    //        expect($scope.Schedule.Body.HITMP).toBe('99');
    //    });

    //});

    //describe('Decrease temp', function () {
    //    it('should Increase the string value by 1', function () {
    //        $scope.Schedule.Body = { OBJNAM: 'MOCK', HITMP: '44F' };
    //        $scope.DecreaseTemp($scope.Schedule.Body, 'HITMP', 40);
    //        expect($scope.Schedule.Body.HITMP).toBe(43);
    //    });
    //    it('should Increase the Number value by 1', function () {
    //        $scope.Schedule.Body = { OBJNAM: 'MOCK', HITMP: '44' };
    //        $scope.DecreaseTemp($scope.Schedule.Body, 'HITMP', 40);
    //        expect($scope.Schedule.Body.HITMP).toBe(43);
    //    });

    //    it('should not Increase the value by 1 if prop is not found', function () {
    //        $scope.Schedule.Body = { HITMP: '45' };

    //        $scope.DecreaseTemp($scope.Schedule.Body, 'HIMP', 40);
    //        expect($scope.Schedule.Body.HITMP).toBe('45');
    //        expect($scope.Schedule.Body.HIMP).not.toBeDefined();
    //    });
    //    it('should keep the value at the min 40 if the value was already 99', function () {
    //        $scope.Schedule.Body = { OBJNAM: 'MOCK', HITMP: '40' };
    //        $scope.DecreaseTemp($scope.Schedule.Body, 'HITMP', 40);
    //        expect($scope.Schedule.Body.HITMP).toBe('40');
    //    });

    //    it('should keep the value at 40 if body is null', function () {
    //        $scope.Schedule.Body = { OBJNAM: 'MOCK', HITMP: '40' };
    //        $scope.DecreaseTemp(null, 'HITMP', 40);
    //        expect($scope.Schedule.Body.HITMP).toBe('40');
    //    });
    //});
    //describe('Save Schedule', function () {
    //    var createDeferred;
    //    var saveDefrred;
    //    beforeEach(function () {
    //        createDeferred = $q.defer();
    //        saveDefrred = $q.defer();

    //        spyOn(propertyService, 'CreateNewObject').and.returnValue(createDeferred.promise);
    //        spyOn(propertyService, 'SetParams').and.returnValue(saveDefrred.promise);
    //    });

    //    describe('Existing Schedule', function () {;
    //        beforeEach(function () {
    //            $scope.Modal = "EDIT";
    //            $scope.Schedule.ObjNam = "S1000";
    //        });
    //        describe('Success', function () {
    //            beforeEach(function () {
    //                $scope.SaveSchedule("EDIT");
    //                createDeferred.resolve();
    //                saveDefrred.resolve();
    //                $scope.$apply();
    //            });
    //            it('should reset Modal and call SetParams on edit', function () {
    //                expect(propertyService.SetParams).toHaveBeenCalled();
    //                expect(propertyService.SetParams.mostRecentCall.args[0]).toBe('S1000');
    //            });
    //        });
    //        describe('ERROR', function () {
    //            it('should reset Modal but not set error flag if no Schedule OBJNAM is provided', function () {
    //                $scope.Schedule.ObjNam = null;
    //                $scope.SaveSchedule("EDIT");
    //                expect(propertyService.SetParams).not.toHaveBeenCalled();
    //            });
    //        });
    //    });

    //    describe('New Schedule', function () {
    //        beforeEach(function () {
    //            $scope.Modal = "ADD";
    //            $scope.SaveSchedule("ADD");
    //        });

    //        describe('Success', function () {
    //            beforeEach(function () {
    //                createDeferred.resolve({ objectList: [{ objnam: 'S2200' }] });
    //            });
    //            it('should reset Modal and call CreateNewObject', function () {
    //                expect(propertyService.CreateNewObject).toHaveBeenCalled();
    //            });
            
    //        });
    //        describe('Create schedule object ERROR', function () {
    //            beforeEach(function () {
    //                createDeferred.reject();
    //                $scope.$apply();
    //            });
    //            it('should reset Modal but set ScheduleError and not call SaveSchedule if CreateNewObject failed', function () {
    //                expect(propertyService.SetParams).not.toHaveBeenCalled();
    //                expect(propertyService.CreateNewObject).toHaveBeenCalled();
    //            });
    //        });


    //        describe('Save schedule ERROR', function () {
    //            beforeEach(function () {
    //                createDeferred.resolve({ objectList: [{ objnam: 'S3200' }] });
    //                saveDefrred.reject();
    //                $scope.$apply();
    //            });
    //            it('should reset Modal but set ScheduleError and if SaveSchedule  failed', function () {
    //               // expect(propertyService.CreateNewObject).toHaveBeenCalled();
    //               // expect(propertyService.SetParams.mostRecentCall.args[0]).toBe('S3200');
    //               // expect(propertyService.SetParams).toHaveBeenCalled();
    //            });
    //        });
    //    });
    //});

    //describe('Sync stop Time', function () {

    //    beforeEach(function () {
    //        $scope.Schedule.StopTime = new Date();
    //    });
    //    it('should set stop time 2 hours ahead of start time if pristine', function () {
    //        var date = new Date();
    //        date.setHours(12);
    //        $scope.Schedule.StopTime.setHours(22);
    //        $scope.SyncStopTime(date);
    //        expect($scope.Schedule.StopTime.getHours()).toBe(14);
    //    });

    //    it('should not set stop time 2 hours ahead of start time if pristine', function () {
    //        var date = new Date();
    //        date.setHours(12);
    //        $scope.Schedule.StopTime.setHours(22);

    //        $scope.StopTimeChanged();
    //        $scope.SyncStopTime(date);
    //        expect($scope.Schedule.StopTime.getHours()).toBe(22);
    //    });
    //});

    //describe('Validate new Schedule', function () {
    //    beforeEach(function () {
    //        $scope.Schedule = {
    //            ObjNam: 'S1000',
    //            Body: {},
    //            Circuit: 'B1000',
    //            Type: '',
    //            Group: '1',
    //            Status: 'ON',
    //            Days: ['f'],
    //            StartTime: jasmine.any(Date),
    //            StopTime: jasmine.any(Date),
    //            StartMode: 'ABSTIM',
    //            StopMode: 'ABSTIM'
    //        }
    //    });
    //    it('should return true if schedule has days and circuit ', function () {
    //        expect($scope.ValidateNewSchedule()).toBe(true);
    //    });

    //    it('should return true if schedule has no Days', function () {
    //        $scope.Schedule.Days = [];
    //        expect($scope.ValidateNewSchedule()).toBe(false);
    //    });

    //    it('should return true if schedule has no Circuit', function () {
    //        $scope.Schedule.Circuit = '';
    //        expect($scope.ValidateNewSchedule()).toBe(false);
    //    });
    //});

    //describe('Toggle Use Circuit', function () {
    //    beforeEach(function () {
    //        $scope.Schedule.UseCurrentHeatMode = true;
    //        $scope.Schedule.UseCurrentTemp = true;
    //        propertyModel.Objects.B1000.LOTMP = '99';
    //        propertyModel.Objects.B1000.HEATER = 'H1000';
    //        $scope.Schedule.Body = { HEATER: 'H1001', LOTMP: '55' };
    //        $scope.Schedule.Circuit = 'B1000';
    //    });

    //    it('should set Heater to current heater when toggled on', function () {
    //        $scope.ToggleUseCurrent('HeatMode');
    //        expect($scope.Schedule.Body.HEATER).toBe('H1000');
    //    });
    //    it('should not modify current heater when toggled off', function () {
    //        $scope.Schedule.UseCurrentHeatMode = false;
    //        $scope.ToggleUseCurrent('HeatMode');
    //        expect($scope.Schedule.Body.HEATER).toBe('H1001');
    //    });


    //    it('should set temp to current temp when toggled on', function () {
    //        $scope.ToggleUseCurrent('Temp');
    //        expect($scope.Schedule.Body.LOTMP).toBe('99');
    //    });
    //    it('should not modify current temp when toggled off', function () {
    //        $scope.Schedule.UseCurrentTemp = false;
    //        $scope.ToggleUseCurrent('Temp');
    //        expect($scope.Schedule.Body.LOTMP).toBe('55');
    //    });

    //});
});

