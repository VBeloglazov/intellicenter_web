﻿using System;
using System.Configuration;
using System.Web.Configuration;
using System.Web.Http;
using System.Threading.Tasks;

using Microsoft.AspNet.SignalR;

using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Microsoft.Owin.Security.OAuth;

using Owin;

using SERVICE_SHARED;

namespace Pentair.Web.Api
{
    public class Startup
    {
        private const string EVENT_SOURCE = "Startup";

        public static OAuthBearerAuthenticationOptions OAuthBearerOptions { get; private set; }

        public void Configuration(
            IAppBuilder app)
        {
            var config = new HttpConfiguration();

            ConfigureOAuth(app);

            WebApiConfig.Register(config);
            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
            app.UseWebApi(config);

            ConfigureSignalR(app);

        } // Configuration()

        private void ConfigureOAuth(
            IAppBuilder app)
        {
            var oAuthServerOptions = new OAuthAuthorizationServerOptions
            {
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/login"),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(1),
                Provider = new SimpleAuthorizationServerProvider()
            };

            var oAuthBearerOptions = new OAuthBearerAuthenticationOptions
            {
                Provider = new QueryStringOAuthBearerProvider("access-token")
            };

            // Configure the db context and user manager to use a single instance per request
            app.CreatePerOwinContext(ApplicationUserContext.Create);
            app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);

            OAuthBearerOptions = new OAuthBearerAuthenticationOptions();
            app.UseOAuthBearerAuthentication(OAuthBearerOptions);
            app.UseOAuthAuthorizationServer(oAuthServerOptions);
            app.UseOAuthBearerAuthentication(oAuthBearerOptions);

        } // ConfigureOAuth()

        private void ConfigureSignalR(
            IAppBuilder app)
        {
            try
            {
                Configuration configSettings = WebConfigurationManager.OpenWebConfiguration("/service");

                bool isESB;
                if (!System.Boolean.TryParse(configSettings.AppSettings.Settings["isESB"].Value, out isESB))
                {
                    LogManager.LogErrorAsync(
                        source: EVENT_SOURCE,
                        eventBriefDescription: "Cannot parse config setting",
                        installationId: 0,
                        userId: Guid.Empty,
                        connectionId: Guid.Empty,
                        eventFullDescription:
                            String.Format("Name: \"isESB\"\nType: boolean\nConfig value: \"{0}\"",
                                configSettings.AppSettings.Settings["isESB"].Value
                            )
                    );

                    return;
                }

                if (isESB)
                {
                    EsbClientConnection.IsEsbRequired = true;

                    int port;
                    if (!System.Int32.TryParse(configSettings.AppSettings.Settings["redisPort"].Value, out port))
                    {
                        LogManager.LogErrorAsync(
                            source: EVENT_SOURCE,
                            eventBriefDescription: "Cannot parse config setting",
                            installationId: 0,
                            userId: Guid.Empty,
                            connectionId: Guid.Empty,
                            eventFullDescription:
                                String.Format("Name: \"redisPort\"\nType: integer\nConfig value: \"{0}\"",
                                    configSettings.AppSettings.Settings["redisPort"].Value
                                )
                        );

                        return;
                    }

                    GlobalHost.DependencyResolver.UseRedis(
                        configSettings.AppSettings.Settings["redisServer"].Value,
                        port,
                        "",
                        "Pentair"
                    );

                    app.MapSignalR<EsbConnection>("/signalr");

                    LogManager.LogInformationAsync(
                        source: EVENT_SOURCE,
                        eventBriefDescription: "SignalR mapped",
                        installationId: 0,
                        userId: Guid.Empty,
                        connectionId: Guid.Empty,
                        eventFullDescription:
                            String.Format("Mapped to: {0}:{1}",
                                configSettings.AppSettings.Settings["redisServer"].Value,
                                configSettings.AppSettings.Settings["redisPort"].Value
                            ),
                        saveEventLog: true
                    );

                    EsbClientConnection.ConnectEsbAsync().Wait(15000);
                }
                else
                {
                    EsbClientConnection.IsEsbRequired = false;

                    LogManager.LogInformationAsync(
                        source: EVENT_SOURCE,
                        eventBriefDescription: "ESB not configured",
                        installationId: 0,
                        userId: Guid.Empty,
                        connectionId: Guid.Empty,
                        eventFullDescription: String.Empty,
                        saveEventLog: true
                    );

                    return;
                }
            }
            catch (Exception e)
            {
                LogManager.LogErrorAsync(
                    EVENT_SOURCE,
                    eventBriefDescription: "SignalR configuring failed",
                    installationId: 0,
                    userId: Guid.Empty,
                    connectionId: Guid.Empty,
                    eventFullDescription:
                    ExceptionManager.CustomizeException(e).ErrorMessage(errorHeader: null, addStackTrace: true)
                );
            }

        } // ConfigureSignalR()

    } // class Startup
}