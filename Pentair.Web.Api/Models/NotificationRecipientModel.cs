﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pentair.Web.Api
{
    public class NotificationRecipientModel
    {
        [Required]
        public string UserId { get; set; }

        [Required]
        public string PropertyId { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        [EmailAddress]
        public string EmailAddress1 { get; set; }

        [EmailAddress]
        public string EmailAddress2 { get; set; }

        [Phone]
        public string Phone1 { get; set; }

        [Phone]
        public string Phone2 { get; set; }
    
    } // class NotificationRecipientModel

    public class NotificationRecipientList
    {
        [Required]
        public string PropertyId { get; set; }

        [Required]
        public List<string> RecipientId { get; set; } 
    
    } // class NotificationRecipientList
}