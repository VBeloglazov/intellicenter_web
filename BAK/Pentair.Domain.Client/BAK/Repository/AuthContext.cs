﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;
using Pentair.Domain.Client.Entity;

namespace Pentair.Domain.Client.Repository
{
    class AuthContext : IdentityDbContext<ApplicationUser>
    {
        public AuthContext()
            : base("ApiContext")
        {
        }
    }
}
