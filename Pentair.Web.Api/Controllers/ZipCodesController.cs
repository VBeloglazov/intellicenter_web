﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;

namespace Pentair.Web.Api
{
    public class ZipCodesController : ApiController
    {
        [Authorize]
        public async Task<IHttpActionResult> Get(string zipCode)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://api.zip-codes.com/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.GetAsync("ZipCodesAPI.svc/1.0/GetZipCodeDetails/" + zipCode + "?key=SRCMEA74212H8YZH5R85");
                if (response.IsSuccessStatusCode)
                {
                    dynamic latLongTime = await response.Content.ReadAsAsync<dynamic>();
                    return Ok(latLongTime);
                }
                else
                {
                    return NotFound();
                }
            }
        }
    }

    public class LatLongTimeZone
    {
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string TimeZone { get; set; }
    }

}
