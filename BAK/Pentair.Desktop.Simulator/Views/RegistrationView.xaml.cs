﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Windows;
using System.Windows.Controls;
using Microsoft.Win32;
using Pentair.Domain.Client.Entity;

namespace Pentair.Desktop.Simulator.Views
{
    /// <summary>
    /// Interaction logic for Registration.xaml
    /// </summary>
    public partial class RegistrationView : UserControl, INotifyPropertyChanged
    {
        private PropertyConfigurations _configuration = PropertyConfigurations.SharedPoolAndSpa;
        private string _customConfiguration = "";
        private string _propertyName;
        private string _propertyId;
        private string _secret;
        private string _userId;
        private bool _register;
        private bool _reRegister;

        public string PropertyName
        {
            get { return _propertyName; }
            set
            {
                _propertyName = value;
                SetProperty("PropertyName");
            }
        }

        public string UserName
        {
            get { return _userId; }
            set
            {
                _userId = value;
                SetProperty("UserName");
            }
        }

        public string PropertyId
        {
            get { return _propertyId; }
            set
            {
                _propertyId = value;
                SetProperty("PropertyId");
            }
        }

        public string Secret
        {
            get { return _secret; }
            set
            {
                _secret = value;
                SetProperty("Secret");
            }
        }
        public bool NewRegistration { get; set; }
        public bool HasMoved { get; set; }
        public bool CreateUser { get; set; }
        public Dictionary<string, string> HostList { get; set; }
        public DataSet ConfigurationList { get; set; }

        public bool ReRegister
        {
            get { return _reRegister; }
            set
            {
                _reRegister = value;
                SetProperty("ReRegister");
            }
        }

        public bool Register
        {
            get { return _register; }
            set
            {
                _register = value;
                SetProperty("Register");
            }
        }
        public string Password { get { return txtPassword.Password; } }
        public PropertyConfigurations Configuration { get { return _configuration; } }
        public String ConfigFile { get { return _customConfiguration; } }
        #region Events
        public delegate void ChangeProperty(object sender, Property e);
        public event PropertyChangedEventHandler PropertyChanged;

        public static readonly RoutedEvent RegisterPropertyEvent =
            EventManager.RegisterRoutedEvent("RegisterProperty",
                RoutingStrategy.Bubble,
                typeof (RoutedEventHandler),
                typeof(RegistrationView));

        public event RoutedEventHandler RegisterProperty
        {
            add { AddHandler(RegisterPropertyEvent, value); }
            remove { RemoveHandler(RegisterPropertyEvent, value); }
        }

        public static readonly RoutedEvent GetRegisteredPropertiesEvent =
            EventManager.RegisterRoutedEvent("GetRegisteredProperties",
                RoutingStrategy.Bubble,
                typeof (RoutedEventHandler),
                typeof (RegistrationView));

        public event RoutedEventHandler GetRegisteredProperties
        {
            add { AddHandler(GetRegisteredPropertiesEvent, value); }
            remove { RemoveHandler(GetRegisteredPropertiesEvent, value); }
        }

        #endregion

        #region Constructor
        /// <summary>
        /// Consttructor
        /// </summary>
        public RegistrationView()
        {
            DataContext = this;
            Register = ReRegister = false;
            InitializeComponent();
            _customConfiguration = null;
            SelectedHost.ItemsSource = Globals.Hosts.Keys;
            SelectedHost.SelectedIndex = 3;
            SelectedConfiguration.ItemsSource = Globals.Configurations.Values;
            SelectedConfiguration.SelectedIndex = 0;
        }
        #endregion

        #region Event Handlers
        public void RegisterButton_Click(object sender, RoutedEventArgs e)
        {
            _configuration = (PropertyConfigurations) SelectedConfiguration.SelectedIndex;
            _propertyId = PropertyIdText.Text;
            if (SelectedConfiguration.SelectedIndex == (int) PropertyConfigurations.Import)
            {
                var dlg = new OpenFileDialog();
                dlg.DefaultExt = "txt";
                dlg.Filter = "Json files (*.json)|*.json|Text Files (*.txt)|*.txt|All files (*.*)|*.*";
                var result = dlg.ShowDialog();
                if (result != true)
                {
                    MessageBox.Show("No file selected", "Error creating custom configuration", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                _customConfiguration = dlg.FileName;
            }
            RaiseEvent(new RoutedEventArgs(RegistrationView.RegisterPropertyEvent, this));
        }

        public void GetPropertiesButton_Click(object sender, RoutedEventArgs e)
        {
            RaiseEvent(new RoutedEventArgs(RegistrationView.GetRegisteredPropertiesEvent, this));
        }

        public void CheckEnables(object sender, RoutedEventArgs e)
        {
            var userCredentials = !String.IsNullOrEmpty(UserName) &&
                                  !String.IsNullOrEmpty(txtPassword.Password);
            Register = userCredentials;
            ReRegister = userCredentials;
        }

        internal void SetProperty(string property)
        {
            if (null != PropertyChanged)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }


        #endregion
    }
}
