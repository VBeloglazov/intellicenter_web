/// <reference path="LightGroupsController.js" />
/// <reference path="ScheduleController.js" />
'use strict';
var PWC = PWC || {};
PWC.controller('LightGroupsController', [
    '$scope', '$location', '$filter', 'PropertyService', 'AuthService', 'Enumerations', 'PropertyModel', 'Notification',
    function ($scope, $location, $filter, propertyService, authService, Enumerations, propertyModel, Notification) {
        $scope.SelectedLightgroup = null;
        $scope.LightGroups = [];
        $scope.ConfirmDelete = false;
        $scope.SelectAllLights = false;
        $scope.ValidCircuits = [];
        $scope.AvailableCircuits = [];
        $scope.Available = [];
        $scope.AvailableColors = Enumerations.lightColorGroup.types;

        init();

        $scope.$on('CircuitUpdate', function (event, message) {
            refreshList();
        });

        $scope.errorSave = function(errorMessage) {
          Notification.error({message: 'Failed!' + ' ' + errorMessage, delay: 1000});
        };

        $scope.successSave = function() {
          Notification.success({message: 'Saved!', delay: 1000});
        };

        $scope.successCancel = function() {
          Notification.success({message: 'Cancelled!', delay: 1000});
        };

        $scope.SetAllLightsSelected = function (value) {
            $scope.SelectAllLights = value;
            if ($scope.SelectedLightgroup.LIGHTS) {
                angular.forEach($scope.SelectedLightgroup.LIGHTS, function (light) {
                    light.deleteFlag = value;
                });
            }
        };

        $scope.SetLightSelected = function (index) {
            if ($scope.SelectedLightgroup.LIGHTS) {
                $scope.SelectedLightgroup.LIGHTS[index].deleteFlag = !$scope.SelectedLightgroup.LIGHTS[index].deleteFlag;
            }
        };

        $scope.AddLightgroup = function () {
            $scope.SelectedLightgroup = CreateNewLightGroup();
            $scope.LightGroups.push($scope.SelectedLightgroup);
            $scope.AvailableCircuits = $filter('lightCircuits')(propertyModel.Objects);
        };

        $scope.AddLight = function () {
            $scope.SelectedLightgroup.LIGHTS.push(CreateLightcircuit());
            // now filter the available circuits.
            FilterAvailableLights()
        };

        $scope.CircuitSelected = function (circuit, index) {
            if (circuit) {
                $scope.SelectedLightgroup.LIGHTS[index] = buildLight(circuit.OBJNAM);
                if ($scope.SelectedLightgroup.LIGHTS.length === 1) {
                    buildValidLightsList(circuit);
                } else {
                    FilterAvailableLights();
                }
            }
        };

        $scope.ColorSelected = function (color,circuit, index) {
            if (color.key && circuit.USE) {
                circuit.USE = color.key;
                circuit.SUBTYP = color.SUBTYP;
            }
        };

        $scope.getColorSelected = function (circuit) {
            var name  = circuit.USE
            if (circuit) {                                
                angular.forEach($scope.AvailableColors, function (color) {
                    if (circuit.USE === color.key) {
                        name = color.value
                        //var name1 = $scope.getColorName(color.value) // with the code get name
                    }
                });                
            }
            return name;
        };

        $scope.SetEditMode = function (group) {
            $scope.SelectAllLights = false;
            if (group) {
                $scope.AvailableCircuits = [];
                var obj = propertyModel.Objects[group.OBJNAM];
                if (!obj) {
                    $scope.AvailableCircuits = $filter('lightCircuits')(propertyModel.Objects);
                } else {
                    buildValidLightsList(group.LIGHTS[0]);
                }
                $scope.SelectedLightgroup = group;
            }
        };

        $scope.CancelEdit = function () {
            //$scope.SelectedLightgroup = null;
            $scope.LightGroups = [];
            refreshList();
            $scope.successCancel();
        };

        $scope.DeleteLightGroups = function () {
            $scope.ConfirmDelete = true;
        };

        $scope.DeleteLightCircuits2 = function (index) {
            if ($scope.SelectedLightgroup && $scope.SelectedLightgroup.LIGHTS)  // check if exist
                $scope.SelectedLightgroup.LIGHTS.splice(index, 1);
            FilterAvailableLights()
        };

        $scope.DeleteLightCircuits = function (index) {
            var lights = [];
            $scope.SelectAllLights = false;
            angular.forEach($scope.SelectedLightgroup.LIGHTS, function (light) {
                if (!light.deleteFlag) {
                    lights.push(light);
                }
            });
            $scope.SelectedLightgroup.LIGHTS = lights;
            if ($scope.SelectedLightgroup.LIGHTS.length < 1) {
                $scope.SelectedLightgroup.LIGHTS.push(CreateLightcircuit());
                $scope.AvailableCircuits = $filter('lightCircuits')(propertyModel.Objects);
            } else {
                FilterAvailableLights();
            }

        }

        $scope.DeleteCircuitFlag = function () {
            var enable = $scope.SelectAllLights;
            if ($scope.SelectedLightgroup) {
                for (var i = 0; i < $scope.SelectedLightgroup.LIGHTS.length; i++) {
                    if ($scope.SelectedLightgroup.LIGHTS[i].deleteFlag == true) {
                        enable = true;
                    }
                }
            }
            return enable;
        };

        $scope.ConfirmDeleteLightGroups = function () {
            var objNames = [];
            $scope.LightGroups.forEach(function (group) {
                if (group.deleteFlag) {
                    objNames.push(group.OBJNAM);
                    // the refresh trigger is picking up from here so needed to delete from here too
                    delete propertyModel.Objects[group.OBJNAM];
                }
            });
            propertyService.DeleteObject(objNames);
            $scope.LightGroups = [];
            propertyService.RefreshLightGroups();
            $scope.ConfirmDelete = false;
        };

        $scope.ValidateNewGroup = function () {
            if ($scope.SelectedLightgroup && $scope.SelectedLightgroup.LIGHTS) {
                return $scope.SelectedLightgroup.SNAME !== '' && $scope.ValidLights();
            }
            return false;
        };

        $scope.RemainingLights = function () {
            if ($scope.AvailableCircuits)
                return $scope.AvailableCircuits.length > 0 && $scope.ValidLights() && canAddMoreLights();
            else
                return $scope.ValidLights();
        };

        $scope.ValidLights = function () {
            var valid = true;
            if ($scope.SelectedLightgroup) {
                for (var i = 0; i < $scope.SelectedLightgroup.LIGHTS.length; i++) {
                    if ($scope.SelectedLightgroup.LIGHTS[i].OBJNAM === 'new') {
                        valid = false;
                    }
                }
            }
            return valid;
        };

        $scope.ExpandItem = function (event, group) {
            if (group) {
                $scope.SelectedLightgroup = group;
                $scope.SetEditMode(group);
                $scope.LightGroups.forEach(function (g) {
                    if (g.OBJNAM === group.OBJNAM) {
                        g.open = !g.open;
                    }
                });
            }
            event.stopPropagation();
            FilterAvailableLights()
        };

        $scope.CreateLightGroup = function () {
            // check disable egg timer
            var disableEggTimer = ($scope.SelectedLightgroup.disableEggTimer) ? 'ON' : 'OFF';
            // computer eggtimer 
            var time = $scope.SelectedLightgroup.eggTimerMinutes;
            time += ($scope.SelectedLightgroup.eggTimerHours * 60);
            time = time + ''

            var args = {
                SNAME: $scope.SelectedLightgroup.SNAME,
                TIME: time,
                DNTSTP: disableEggTimer,
                OBJTYP: 'LITSHO',
                OBJLIST: []
            };
            for (var i = 0; i < $scope.SelectedLightgroup.LIGHTS.length; i++) {
                var PARAMS = {                                        
                    DLY: $scope.SelectedLightgroup.LIGHTS[i].DELAY.toString(),
                    USE: $scope.getColorCode($scope.SelectedLightgroup.LIGHTS[i].USE)     
                };
                args.OBJLIST.push({ objnam: $scope.SelectedLightgroup.LIGHTS[i].OBJNAM, params: PARAMS });
            }

            propertyService.CreateGroup(args);
            $scope.LightGroups = [];
            propertyService.RefreshLightGroups();
            $scope.successSave();
        };

        $scope.getColorCode = function(color){
            var value = "1";            
            switch (color) {
                case 'White':
                case 'WHITER':  
                    value = "0";
                    break;
                case 'Lightgreen':
                case 'LITGRN':  
                    value = "1";
                    break;
                case 'Green':
                case 'GREEN':   
                    value = "2";
                    break;
                case 'Cyan':
                case 'CYAN':    
                    value = "3";
                    break;
                case 'Blue':
                case 'BLUE':    
                    value = "4";
                    break;
                case 'Lavender':
                case 'LAVANDER':
                    value = "5";
                    break;
                case 'Magenta':
                case 'MAGNTA':  
                    value = "6";
                    break;
                case 'LightMagenta':
                case 'MAGNTAR':
                    value = "7";
                    break;
                default:
                    value = "1";
                    break;
            }
            return  value;
        }

        $scope.getColorName = function (code) {
            var value = "1";
            switch (color) {
                case '0':
                    value = "WHITER";
                    break;
                case '1':
                    value = "LITGRN";
                    break;
                case '2':
                    value = "GREEN";
                    break;
                case '3':
                    value = "CYAN";
                    break;
                case '4':
                    value = "BLUE";
                    break;
                case '5':
                    value = "LAVANDER";
                    break;
                case '6':
                    value = "MAGNTA";
                    break;
                case '7':
                    value = "MAGNTAR";
                    break;
                default:
                    value = "WHITER";
                    break;
            }
            return value;
        }

        $scope.SaveCircuitGroup = function () {
            // check disable egg timer
            var disableEggTimer = ($scope.SelectedLightgroup.disableEggTimer) ? 'ON' : 'OFF';
            // computer eggtimer 
            var time = $scope.SelectedLightgroup.eggTimerMinutes
            time += ($scope.SelectedLightgroup.eggTimerHours * 60);

            var args = {
                objnam: $scope.SelectedLightgroup.OBJNAM,
                SNAME: $scope.SelectedLightgroup.SNAME,
                TIME: time.toString(),
                DNTSTP: disableEggTimer,
                OBJLIST: []
            };
            for (var i = 0; i < $scope.SelectedLightgroup.LIGHTS.length; i++) {
                var parameters = {
                    DLY: $scope.SelectedLightgroup.LIGHTS[i].DELAY.toString(),
                    USE: $scope.getColorCode($scope.SelectedLightgroup.LIGHTS[i].USE),                    

                }
                args.OBJLIST.push({objnam:$scope.SelectedLightgroup.LIGHTS[i].OBJNAM, params:parameters});

            }

            propertyService.EditGroup(args);
            $scope.LightGroups = [];
            propertyService.RefreshLightGroups();
            $scope.successSave();
        };

        $scope.GroupDeletes = function () {
            var i = 0;
            angular.forEach($scope.LightGroups, function (item) {
                if (item.deleteFlag) {
                    i++;
                }
            });
            return i > 0;
        };

        $scope.SetDeleteFlag = function (group) {
            if (group) {
                group.deleteFlag = !group.deleteFlag;
            }
        };

        $scope.isGlobriteWhite = function(circuit) { // check if this circuit is GloBrite White
            if (!circuit)
                return false;

            if (circuit.SUBTYP && circuit.SUBTYP == "GLOWT")
                return true;
            else
                return false;
        }

        // Set default circuit group
        function getDefaultNameCircuitGroup()  {
            
            for (var i = 0; i < $scope.LightGroups.length; i++) { 
                var name = 'Light Group ' + (i+1)           
                var available = false;
                $scope.LightGroups.forEach(function (group){
                    if (group.SNAME === name)
                        available =true
                });
                if (available===false){
                    return name;
                }
            }
            
            return 'Light Group ' + ($scope.LightGroups.length + 1)
        }

        function CreateNewLightGroup() {
            var group = {
                OBJNAM: 'new',
                OBJTYP: 'CIRCUIT',
                SUBTYP: 'LITSHO',
                USE: 'WHITER',
                SNAME: getDefaultNameCircuitGroup(),
                LISTORD: '1',
                SHOMNU: 'le',
                FREEZE: 'OFF',                
                LIGHTS: [],
                deleteFlag: false,
                open: true,
                eggTimerHours: 12,
                eggTimerMinutes: 0,
                disableEggTimer: false,
            };
            
            
            group.LIGHTS.push(CreateLightcircuit());
            return group;
        }

        function CreateLightcircuit() {
            var circuit = {
                OBJNAM: 'new',
                OBJTYP: 'CIRCUIT',
                USE: 'WHITER',
                DELAY: '0',
                SUBTYP: "LIGHT",
                SNAME: "Select a Light", //getDefaultNameCircuitGroup(),
                deleteFlag: false
            };
            return circuit;
        }

        function refreshList() {
            $scope.LightGroups = [];
            $filter('lightGroupCircuits')(propertyModel.Objects).forEach(function (group) {
                var lightGroup = CreateNewLightGroup();
                if (group.OBJNAM) {
                    lightGroup.OBJNAM = group.OBJNAM;
                }
                if (group.OBJTYP) {
                    lightGroup.OBJTYP = group.OBJTYP;
                }
                if (group.SUBTYP) {
                    lightGroup.SUBTYP = group.SUBTYP;
                }
                if (group.SNAME) {
                    lightGroup.SNAME = group.SNAME;
                }
                if (group.LISTORD) {
                    lightGroup.LISTORD = group.LISTORD;
                }
                if (group.SHOMNU) {
                    lightGroup.SHOMNU = group.SHOMNU;
                }
                if (group.FREEZE) {
                    lightGroup.FREEZE = group.FREEZE;
                }
                if (group.USE) {    // color
                    lightGroup.USE = group.USE;
                }
                if (group.TIME) {
                    lightGroup.eggTimerHours = Math.floor(group.TIME / 60);
                    lightGroup.eggTimerMinutes = group.TIME % 60;
                }
                if (group.DNTSTP) {
                    lightGroup.disableEggTimer = (group.DNTSTP === 'ON') ? true : false;
                }

                lightGroup.open = false;
                if (group.CHILD) {
                    lightGroup.LIGHTS = [];
                    var children = group.CHILD.split(' ');
                    var index = 0;
                    angular.forEach(children, function (child) {                                               
                        var light = buildLight(child.trim())
                        // SETTING DELAY
                        if (group.OBJLIST && group.OBJLIST[index] && group.OBJLIST[index].params) {
                            if (group.OBJLIST[index].params.DLY)    // add delay if exist
                                light.DELAY = group.OBJLIST[index].params.DLY
                            if (group.OBJLIST[index].params.USE)    // add color if exist
                                light.USE = group.OBJLIST[index].params.USE
                            if (group.OBJLIST[index].params.SUBTYP)    // add subtyp if exist
                                light.SUBTYP = group.OBJLIST[index].params.SUBTYP

                        }
                        lightGroup.LIGHTS.push(light);
                        index++
                    });
                }
                $scope.LightGroups.push(lightGroup);
            });
        }

        function buildLight(circuitName) {
            var light = CreateLightcircuit();
            if (propertyModel.Objects[circuitName]) {
                var circuit = propertyModel.Objects[circuitName];
                light.OBJNAM = circuit.OBJNAM;
                light.OBJTYP = circuit.OBJTYP;
                light.SNAME = circuit.SNAME;
                light.DELAY = 0;         // COLOR SWIM DELAY
                light.USE = "WHITER";    // COLOR SET 
                light.SUBTYP = circuit.SUBTYP;                
            }
            return light;
        }

        function buildValidLightsList(lightCircuit) {
    /*
            propertyService.GetCompatableLightCircuits(lightCircuit.OBJNAM).then(function (result) {
                if (result && result.answer) {
                    $scope.ValidCircuits = [];
                    result.answer.forEach(function (circuit) {
                        $scope.ValidCircuits.push(propertyModel.Objects[circuit.objnam]);
                    });
                    FilterAvailableLights();
                }
            });
*/
            // get all lights
            $scope.ValidCircuits = $filter('lightCircuits')(propertyModel.Objects);
            // remove currently selected 
            FilterAvailableLights();
        }

        
        function addSubtypes(subtypes, type) {  // adding unique subtypes
            var exist = false
            angular.forEach(subtypes, function (item) { // check if does not exist
                if (item === type)
                    exist = true
            });
            if (exist === false) {
                subtypes.push(type)
            }            
        }

        function ItemInList(list, search) { // check if seach is in list
            var status = false
            angular.forEach(list, function (item) {
                if (item === search)
                    status = true;
            });
            return status;
        }

        function IsColoredLight(color) {            
            var colored = Enumerations.coloredlights            
            var exist = ItemInList(colored, color) // check if at least 1 is in the current list
            return exist
        }

        function IsNonColoredLight(color) {            
            var nonColored = Enumerations.noncoloredlights
            var exist = ItemInList(nonColored, color) // check if at least 1 is in the current list
            return exist
        }

        function IsColoredItemsOnly(list) {
            var exist = false   // check if at least 1 colored item is in the current list
            
            angular.forEach(list, function (item) {                
                if (IsColoredLight(item))
                   exist = true                
            })
            return exist
        }

        function IsNonColoredItemsOnly(list) {
            var exist = false   // check if at least 1 colored item is in the current list

            angular.forEach(list, function (item) {
                if (IsNonColoredLight(item))
                    exist = true
            })
            return exist
        }
        
        function checkRules_1_types(list_types, item) {
            var status = false;
            if (ItemInList(list_types, "GLOWT")) {      // 1 GLOWT	only allow : LIGHT, GLOWT, DIMMER
                switch (item.SUBTYP) {
                    case "LIGHT":
                    case "GLOWT":
                    case "DIMMER":
                        status = true; // $scope.AvailableCircuits.push(item)
                        break;
                }
            }else if (ItemInList(list_types, "LIGHT")) {      // 1 LIGHT only allow GLOWT 
                switch (item.SUBTYP) {                 
                    case "GLOWT":                 
                        status = true; //$scope.AvailableCircuits.push(item)
                        break;
                }
            }else if (ItemInList(list_types, "DIMMER")) {      // 1 DIMMER only allow GLOWT 
                switch (item.SUBTYP) {
                    case "DIMMER":
                    case "GLOWT":
                        status = true; //$scope.AvailableCircuits.push(item)
                        break;
                }
            }
            return status;
        }

        function checkRules_2_types(list_types, item) {
            var status = false;
            if (ItemInList(list_types, "GLOWT") && ItemInList(list_types, "DIMMER")) {     
                switch (item.SUBTYP) {                    
                    case "GLOWT":
                    case "DIMMER":
                        status = true; //$scope.AvailableCircuits.push(item)
                        break;
                }
            }else if (ItemInList(list_types, "LIGHT") && ItemInList(list_types, "GLOWT")) {      
                switch (item.SUBTYP) {
                    case "LIGHT":
                    case "GLOWT":                    
                        status = true; //$scope.AvailableCircuits.push(item)                        
                        break;
                }
            }
            return status;
        }

        function canAddMoreLights() {   // check if is possible to add more lights            
            $scope.Available = [];
            var list_types = [];
            var status = false;
            angular.forEach($scope.ValidCircuits, function (light) {
                var used = false;
                for (var i = 0; i < $scope.SelectedLightgroup.LIGHTS.length; i++) {
                    if ($scope.SelectedLightgroup.LIGHTS[i].OBJNAM === light.OBJNAM) {
                        used = true;
                        var subtype = $scope.SelectedLightgroup.LIGHTS[i].SUBTYP
                        addSubtypes(list_types, subtype)
                    }
                }
                if (!used) {
                    $scope.Available.push(light)
                }
            });

            if ($scope.SelectedLightgroup.LIGHTS.length === 0){  // if non are selected then return entired list                
                return true;
            }

            // CHECKING COLORED 
            if (IsColoredItemsOnly(list_types) === true) {  // filter all colored items
                angular.forEach($scope.Available, function (item) {
                    if (IsColoredLight(item.SUBTYP))
                        status = true
                })                
            }
            // CHECKING NON COLORED     
            if (IsNonColoredItemsOnly(list_types) === true) {  // filter all non colored items
                angular.forEach($scope.Available, function (item) {
                    if (ItemInList(list_types, item.SUBTYP)) {    // first add items are in the current list : most common case scenario
                        status = true;
                    }
                    else {
                        var same = false
                        if (list_types.length === 1) {
                            same = checkRules_1_types(list_types, item);
                        }
                        else if (list_types.length === 2) {
                            same = checkRules_2_types(list_types, item);
                        }

                        if (same === true) // adding if status is true
                            status = true
                    }
                })                
            }
            return status;
        }

        function FilterAvailableLights() {
            $scope.AvailableCircuits = [];
            $scope.Available = [];   
            var list_types = [];            
            angular.forEach($scope.ValidCircuits, function (light) {
                var used = false;
                for (var i = 0; i < $scope.SelectedLightgroup.LIGHTS.length; i++) {
                    if ($scope.SelectedLightgroup.LIGHTS[i].OBJNAM === light.OBJNAM) {
                        used = true;
                        var subtype = $scope.SelectedLightgroup.LIGHTS[i].SUBTYP
                        addSubtypes(list_types, subtype)
                    }           
                }
                if (!used) {                  
                    $scope.Available.push(light)      //$scope.AvailableCircuits.push(light);
                }
            });
                
            if (($scope.SelectedLightgroup.LIGHTS.length === 0) ||  // if non are selected then return entired list
                ($scope.SelectedLightgroup.LIGHTS.length === 1)     // or is only 1 light
                ) {    
                $scope.AvailableCircuits = $scope.Available;
                return;
            }

            // CHECKING COLORED 
            if (IsColoredItemsOnly(list_types) === true) {  // filter all colored items
                angular.forEach($scope.Available, function (item) {
                    if (IsColoredLight(item.SUBTYP))
                        $scope.AvailableCircuits.push(item)
                })                
                return
            }
            // CHECKING NON COLORED     
            if (IsNonColoredItemsOnly(list_types) === true) {  // filter all non colored items
                angular.forEach($scope.Available, function (item) {                    
                    if (ItemInList(list_types, item.SUBTYP)) {    // first add items are in the current list : most common case scenario
                        $scope.AvailableCircuits.push(item)                        
                    }
                    else {
                        var status = false
                        if (list_types.length === 1) {
                            status = checkRules_1_types(list_types, item);
                        }
                        else if (list_types.length === 2) {
                            status = checkRules_2_types(list_types, item);
                        }

                        if (status === true) // adding if status is true
                            $scope.AvailableCircuits.push(item)
                    }                    
                })
                return
            }

            // otherwise 
            $scope.AvailableCircuits = $scope.Available;
        }

        function init() {
            refreshList();
        }
    }
]);
