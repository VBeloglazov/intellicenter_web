﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.AspNet.Identity.EntityFramework;

namespace Pentair.Web.Api
{
    class AuthContext : IdentityDbContext<ApplicationUser>
    {
        public AuthContext()
            : base("ApiContext")
        {
        } // AuthContext()

    } // class AuthContext
}
