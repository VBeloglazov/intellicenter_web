﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Pentair.Web.Api
{
    public class ReleaseParamListResponse : ResponseCommand
    {
        public ReleaseParamListResponse(
            string messageID,
            string response
            )
            : base(CommandString.ReleaseParamList, messageID, response)
        {
        } // ReleaseParamListResponse()

        /// <summary>
        /// Always returns an object, valid or not. If invalid, check the DeserializationError property value.
        /// </summary>
        public static new ReleaseParamListResponse Deserialize(
            string serializedMessage)
        {
            ReleaseParamListResponse command = null;

            try
            {
                command = JsonConvert.DeserializeObject<ReleaseParamListResponse>(serializedMessage);
                command.IsValid = true;
            }
            catch (Exception e)
            {
                // Deserialization as ReleaseParamListResponse failed

                StringBuilder sbError = new StringBuilder();

                sbError.Append("Cannot deserialize as ReleaseParamListResponse. " + e.Message);

                // Try to get at least the base message data
                ResponseCommand baseCommand = ResponseCommand.Deserialize(serializedMessage);

                if (!baseCommand.IsValid)
                {
                    sbError.Append("\n" + baseCommand.DeserializationError);
                }

                command = new ReleaseParamListResponse(
                    messageID: baseCommand.MessageID,
                    response: ResponseString.BadRequest)
                {
                    IsValid = false,
                    DeserializationError = sbError.ToString()
                };
            }

            return command;

        } // Deserialize()

    } // class ReleaseParamListResponse
}
