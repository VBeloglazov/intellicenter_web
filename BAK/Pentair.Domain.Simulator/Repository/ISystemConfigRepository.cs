﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pentair.Domain.Simulator.Entity;

namespace Pentair.Domain.Simulator.Repository
{
    public interface ISystemConfigRepository : IRepository<SystemConfig>
    {
        SystemConfig GetDefault();
    }
}
