-- =============================================================
--
-- Author:		Vladimir Beloglazov
-- Create date: 01/26/2018
--
-- Creates:
--
--		Table Type:			None
--
--		Stored Procedure:	PCleanupAfterPossibleCrash
--
-- 2018-03-16 VB: [Installations] redesign.
--
-- =============================================================

USE PathfinderIntelliCenter
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (
	SELECT * FROM sys.objects
	WHERE object_id = OBJECT_ID(N'dbo.PCleanupAfterPossibleCrash')
	AND type in (N'P', N'PC')
)
DROP PROCEDURE dbo.PCleanupAfterPossibleCrash
GO

-- -------------------------------------------------------------
--
-- No return.
--
-- -------------------------------------------------------------
CREATE PROCEDURE dbo.PCleanupAfterPossibleCrash
	@hostMachine AS nvarchar(max)

AS
BEGIN

	SET NOCOUNT OFF

	-- Remove all [Subscriptions] records belonging to disconnected THWs

	DELETE Subscriptions
	FROM dbo.Subscriptions S
	INNER JOIN dbo.Connections C ON C.InstallationId = S.PropertyId
	WHERE C.HostMachine = @hostMachine

	-- Remove all [Connections] records belonging to this server

	DELETE FROM Connections
	WHERE HostMachine = @hostMachine

END
GO
