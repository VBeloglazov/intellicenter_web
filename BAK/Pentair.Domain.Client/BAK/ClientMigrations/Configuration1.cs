using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Data.Entity.Migrations.Infrastructure;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Pentair.Domain.Client.Infrastructure;
using Pentair.Domain.Client.Repository;

namespace Pentair.Domain.Client.ClientMigrations
{
    [ExcludeFromCodeCoverage]
    internal sealed class Configuration1 : DbMigrationsConfiguration<ApiContext>
    {
        /// <summary>
        /// Identity context used by the .NET framework
        /// </summary>
        public Configuration1()
        {
            AutomaticMigrationsEnabled = false;
            AutomaticMigrationDataLossAllowed = false;
            MigrationsDirectory = @"ClientMigrations";
        }
    }
}
