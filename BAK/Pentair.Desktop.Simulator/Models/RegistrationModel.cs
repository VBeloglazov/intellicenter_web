﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Windows;
using System.Windows.Controls;
using Pentair.Domain.Client.Entity;
using Pentair.Domain.Client.Infrastructure;

namespace Pentair.Desktop.Simulator.Models
{
    public class RegistrationModel : INotifyPropertyChanged
    {
        #region Public Properties
        public string PropertyName { get; set; }
        public string UserName { get; set; }
        public int PropertyId { get; set; }
        public string Secret { get; set; }
        public bool NewRegistration { get; set; }
        public bool HasMoved { get; set; }
        public bool CreateUser { get; set; }
        public Dictionary<string, string> HostList { get; set; }
        public DataSet ConfigurationList { get; set; }

        #endregion

        #region Private Properties

        private MainViewModel _data;
        #endregion

        #region Events and Delegates
        public event ChangeProperty ChangePropertyEvent;
        public delegate void ChangeProperty(object sender, Property e);
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region Constructor

        public RegistrationModel(MainViewModel data)
        {
            _data = data;
        }
        #endregion

        #region Public Methods
        internal void SetProperty(Property property)
        {
            if (null == property)
            {
                return;
            }
            PropertyName = property.Name;
            PropertyId = property.PropertyId;
            Secret = property.Secret;
            if (null != ChangePropertyEvent)
            {
                ChangePropertyEvent(this, property);
            }
            PropertyChanged(this, new PropertyChangedEventArgs(null));
        }

        #endregion
    }
}
