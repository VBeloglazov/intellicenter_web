﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Machine.Specifications;
using Pentair.Domain.Client.BusinessLogic;
using Pentair.Domain.Client.DataLayer;
using Pentair.Domain.Client.Entity;
using Pentair.Domain.Client.Helpers;
using Pentair.Domain.Client.Helpers.Commands;
using Pentair.Domain.Client.Infrastructure;

namespace Pentair.Tests.Domain
{
    [Subject("UserAssociations")]
    public class ObjectManagerSpec
    {
        #region Database Records

        private static int property1 = 1;

        public class TestContent
        {
            public IApiContext Data { get; private set; }

            public TestContent()
            {
                Data = new FakeContext()
                {
                    ControlObjects =
                    {
                        new ControlObject()
                        {
                            INSTALLATION = property1,
                            OBJNAM = StaticData.Guest,
                            //OBJTYP = "PERMIT",
                            //SNAME = "Guest",
                            //SHOMNU = "slb",
                            //LISTORD = 1,
                            //VER = 1.0f
                        },
                        new ControlObject()
                        {
                            INSTALLATION = property1,
                            OBJNAM = StaticData.Admin,
                            //OBJTYP = "PERMIT",
                            //SNAME = "Admin",
                            //SHOMNU = "slb",
                            //LISTORD = 1,
                            //VER = 1.0f
                        }

                    },
                };
            }
        }

        #endregion

        #region Tests

        //public class WhenNewPermissionsAreAddedToAnInstallation
        //{
        //    private static ObjectManager subject;
        //    private static TestContent data = new TestContent();
        //    private static int originalCount = data.Data.ControlObjects.Count();

        //    private Establish context = () => subject = new ObjectManager(data.Data, property1);

            //private Because of = () =>
            //    subject.UpdateObject(new ParamObject()
            //    {
            //        objnam = "u1000",
            //        parameters = new Dictionary<string, string>()
            //        {
            //            {"OBJTYP", "PERMIT"},
            //            {"SNAME", "Children"},
            //            {"SHOMNU", "slb"},
            //            {"LISTORD", "1"},
            //            {"VER", "1.0"}
            //        }
            //    });

        //    private It TheNumberOfControlObjectsShouldIncrease = () => data.Data.ControlObjects.Count().ShouldEqual(originalCount + 1);
        //}

        public class WhenThePermissionsChangeOnAnExistingPermtObject
        {
            private static ObjectManager subject;
            private static TestContent data = new TestContent();
            private static int originalCount = data.Data.ControlObjects.Count();

            private Establish context = () => subject = new ObjectManager(data.Data, property1);

            //private Because of = () =>
            //    subject.UpdateObject(new ParamObject()
            //    {
            //        objnam = "U0000",
            //        parameters = new Dictionary<string, string>()
            //        {
            //            {"SNAME", "Children"},
            //            {"SHOMNU", "aslb"},
            //            {"LISTORD", "1"},
            //            {"VER", "1.0"}
            //        }
            //    });

            private It TheNumberOfControlObjectsShouldRemainTheSame = () => data.Data.ControlObjects.Count().ShouldEqual(originalCount);
        }

        #endregion
    }
}
