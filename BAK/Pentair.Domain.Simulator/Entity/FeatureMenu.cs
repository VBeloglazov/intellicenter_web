﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pentair.Domain.Simulator.Entity
{
    public class FeatureMenu
    {
        [Key]
        public int ID { get; set; }
        public string Name { get; set; }
        public UInt32 LastRevision { get; set; }
        public int BadgeCount { get; set; }
        public int Reset { get; set; }
        public IList<Circuit> CircuitList { get; set; }

        // Foreign Keys
        public int TypeID { get; set; }
        public int MenuListOrderID { get; set; }
        public int MenuAccessLevelID { get; set; }
    }
}