﻿using System;
using System.Collections.Generic;

using Newtonsoft.Json;

namespace Pentair.Web.Api
{
    public class NewParamObject
    {
        [JsonProperty("objtyp")]
        public string ObjTyp { get; private set; }

        [JsonProperty("params")]
        public Dictionary<string, object> Parameters { get; private set; }

        public NewParamObject(
            string objTyp)
        {
            this.ObjTyp = objTyp;
        }

    } // class NewParamObject
}