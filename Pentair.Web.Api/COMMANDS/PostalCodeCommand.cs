﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Pentair.Web.Api
{
    public class PostalCodeCommand : CommandBase
    {
        [JsonProperty("postalCode")]
        public string PostalCode { get; private set; }

        public PostalCodeCommand(
            string command,
            string messageID,
            string postalCode
            )
            : base(CommandString.ReadPostalCodeInfoMessage, messageID)
        {
            this.PostalCode = postalCode;
        }

    } // class PostalCodeCommand
}
