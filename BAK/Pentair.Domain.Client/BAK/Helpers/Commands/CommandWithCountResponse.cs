﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Pentair.Domain.Client.Helpers.Commands
{
    public class CommandWithCountResponse : ResponseCommand
    {
        [JsonProperty("totalCount", Required = Required.Always)]
        public string TotalCount { get; private set; }

        public CommandWithCountResponse(
            string command,
            string messageID,
            string response,
            string totalCount
            )
            : base(command, messageID, response)
        {
            this.TotalCount = totalCount;

        } // CommandWithCountResponse()

        /// <summary>
        /// Always returns an object, valid or not. If invalid, check the DeserializationError property value.
        /// </summary>
        public static new CommandWithCountResponse Deserialize(
            string serializedMessage)
        {
            CommandWithCountResponse command = null;

            try
            {
                command = JsonConvert.DeserializeObject<CommandWithCountResponse>(serializedMessage);
                command.IsValid = true;
            }
            catch (Exception e)
            {
                // Deserialization as CommandWithCountResponse failed

                StringBuilder sbError = new StringBuilder();

                sbError.Append("Cannot deserialize as CommandWithCountResponse. " + e.Message);

                // Try to get at least the base message data
                ResponseCommand baseCommand = ResponseCommand.Deserialize(serializedMessage);

                if (!baseCommand.IsValid)
                {
                    sbError.Append("\n" + baseCommand.DeserializationError);
                }

                command = new CommandWithCountResponse(
                    command: baseCommand.Command,
                    messageID: baseCommand.MessageID,
                    response: ResponseString.BadRequest,
                    totalCount: String.Empty)
                {
                    IsValid = false,
                    DeserializationError = sbError.ToString()
                };
            }

            return command;

        } // Deserialize()

    } // class CommandWithCountResponse
}