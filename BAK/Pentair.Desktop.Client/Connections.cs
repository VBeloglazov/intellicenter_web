﻿using System.Collections.Generic;
using System.Windows.Documents;
using Newtonsoft.Json;
namespace Pentair.Desktop.Client
{
    public class Connections
    {
        [JsonProperty("connections")] 
        public List<Connection> connections = new List<Connection>();
    }

    public class Connection
    {
        public string name { get; set; }
        public string address { get; set; }
        public int port { get; set; }
    }
}
