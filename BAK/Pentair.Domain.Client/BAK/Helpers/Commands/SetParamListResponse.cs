﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Pentair.Domain.Client.Helpers.Commands
{
    public class SetParamListResponse : ParamCommandResponse
    {
        public SetParamListResponse(
            string messageID,
            string response,
            List<ParamObject> objectList
            )
            : base(CommandString.SetParamList, messageID, response, objectList)
        {
        } // SetParamListResponse()

        /// <summary>
        /// Always returns an object, valid or not. If invalid, check the DeserializationError property value.
        /// </summary>
        public static new SetParamListResponse Deserialize(
            string serializedMessage)
        {
            SetParamListResponse command = null;

            try
            {
                command = JsonConvert.DeserializeObject<SetParamListResponse>(serializedMessage);
                command.IsValid = true;
            }
            catch (Exception e)
            {
                // Deserialization as SetParamListResponse failed

                StringBuilder sbError = new StringBuilder();

                sbError.Append("Cannot deserialize as SetParamListResponse. " + e.Message);

                // Try to get at least the base message data
                ParamCommandResponse baseCommand = ParamCommandResponse.Deserialize(serializedMessage);

                if (!baseCommand.IsValid)
                {
                    sbError.Append("\n" + baseCommand.DeserializationError);
                }

                command = new SetParamListResponse(
                    messageID: baseCommand.MessageID,
                    response: ResponseString.BadRequest,
                    objectList: null)
                {
                    IsValid = false,
                    DeserializationError = sbError.ToString()
                };
            }

            return command;

        } // Deserialize()

    } // class SetParamListResponse
}