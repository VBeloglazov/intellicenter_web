﻿using System.Runtime.InteropServices;
using System.Windows;
using Pentair.Desktop.Simulator.Models;

namespace Pentair.Desktop.Simulator.Views
{
    /// <summary>
    /// Interaction logic for CircuitEditor.xaml
    /// </summary>
    public partial class CircuitEditor : Window
    {
        #region Private Properties

        private EditModel _data;
        #endregion

        #region Constructor

        public CircuitEditor(EditModel data)
        {
            _data = data;
            DataContext = this._data;
            _data.objnam = _data.Object.OBJNAM;
            _data.objtyp = _data.Object.OBJTYP;
            InitializeComponent();
        }

        #endregion

        #region Public Methods

        #endregion

        #region Events

        private void OnLoad(object sender, RoutedEventArgs e)
        {


        }

        private void cancel_click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }


        private void save_click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        #endregion

        #region Private methods
        #endregion
    }
}
