﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pentair.Domain.Simulator.Entity
{
    public class HeaterAffect
    {
        [Key]
        public int ID { get; set; }
        public string Name { get; set; }
        public UInt32 LastRevision { get; set; }
        public int TempHigh { get; set; }
        public int TempLow { get; set; }
        public bool Status { get; set; }

        // Foreign Keys
        public int TypeID { get; set; }
        public int ParentObjectID { get; set; }
        public int CurrentHeaterID { get; set; }
        public int TempModeID { get; set; }
        public int? SystemConfigID { get; set; }
    }
}