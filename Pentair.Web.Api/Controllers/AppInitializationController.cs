﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

using SERVICE_SHARED;

namespace Pentair.Web.Api
{
    public class AppInitializationController : ApiController
    {
        private const string EVENT_SOURCE = "AppInitializationController";

        [HttpGet, AllowAnonymous]
        public void Init()
        {
            if (this.Request.Headers.UserAgent.ToString().Contains("Initialization Preload"))
            {
                LogManager.LogInformationAsync(
                    source: EVENT_SOURCE,
                    eventBriefDescription: "API Initialization",
                    installationId: 0,
                    userId: Guid.Empty,
                    connectionId: Guid.Empty,
                    eventFullDescription: String.Empty,
                    saveEventLog: true
                );
            }

        } // Init()

    } // class AppInitializationController
}