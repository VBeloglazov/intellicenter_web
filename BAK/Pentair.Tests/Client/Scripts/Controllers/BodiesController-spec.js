﻿/* TODO: 2b removed
'use strict';
describe('DashboardController', function () {
    var DashboardController;
    var AppService;
    var $scope;
    var SocketService;
    var $controller;
    var AuthService;
    var $q;
    var mockInstallations;
    var $route;
    var LocalStorageService;
    var Enumerations;
    var PropertyService;
    var $location;
    var propertyModel;
    var messageProcessor;


    beforeEach(module('PentairWebClient'));

    beforeEach(inject(function (_$controller_, _AppService_, _$rootScope_, _SocketService_, _AuthService_, _$q_,
                                _$route_, _LocalStorageService_, _Enumerations_, _PropertyService_, _PropertyModel_,
                                _MessageProcessor_, _$location_) {
        AppService = _AppService_;
        $scope = _$rootScope_.$new();
        SocketService = _SocketService_;
        $controller = _$controller_;
        AuthService = _AuthService_;
        LocalStorageService = _LocalStorageService_;
        Enumerations = _Enumerations_;
        $q = _$q_;
        $route = _$route_;
        PropertyService = _PropertyService_;
        $location = _$location_;
        propertyModel = _PropertyModel_;
        messageProcessor = _MessageProcessor_;
        messageProcessor.ProcessNotification(JSON.parse(SpecHelper.JSONdata.Bodies));

        DashboardController = $controller("BodiesController", { $scope: $scope });
    }));


    describe('Get list of bodies', function () {

        beforeEach(function () {
        });

        it('should create an array of single bodies', function () {
            expect($scope.SingleBodies.length).toBe(2);
        });

        it('should create an array of shared bodies', function () {
            expect($scope.SharedBodies.length).toBe(1);
            expect($scope.SharedBodies[0].parent.OBJNAM).toBe('B1002');
            expect($scope.SharedBodies[0].child.OBJNAM).toBe('B1003');
        });
    });



});
*/